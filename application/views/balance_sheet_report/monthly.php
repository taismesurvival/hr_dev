<style>
    table { border-collapse: collapse; }
    tr.border-top { border-top: solid thin; }
    tr.border-bottom { border-bottom: solid thin; }
    tr.bold { font-weight: bold; }
    * { font-size: 12px; font-family: Lucida Sans Unicode; }
</style>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/slider-pips/jquery-ui.js"></script>

<button type="button" id="download_excel_file">Download Excel Report</button>
<button type="button" id="download_pdf_file">Print or Download PDF Report</button>
<div id="table_wrapper">
    <table style='width:100%;'>
        <tr>
            <td style='font-size:20px;text-align:center;color:blue;'><?php echo $company_name; ?></td>
        </tr>
        <tr>
            <td style='font-size:16px;text-align:center;color:blue;'>Outlets List</td>
        </tr>
        <tr>
            <td style='font-size:16px;text-align:center;color:blue;'>
                <?php foreach($outlets AS $item) {
                    echo $item.'<br/>';
                } ?>
            </td>
        </tr>
        <tr>
            <td style='text-align:center;font-size:18px;'>Balance Sheet (monthly)</td>
        </tr>
        <tr>
            <td style='font-size:16px;text-align:center;'><?php echo $starting_month; ?> - <?php echo $ending_month; ?></td>
        </tr>

    </table>
    <table style='width:100%;'>
        <thead>
        <tr class='border-top'>
            <th colspan="2" style='width:20%;  text-align:center;'>Accounts</th>
            <th style='width:5%;  text-align:center;'><?php echo date('F', strtotime($starting_month)); ?></th>
            <th style='width:5%;  text-align:center;'><?php echo date('F', strtotime($starting_month . ' +1 month')); ?></th>
            <?php for($i=1; $i < 11; $i++) { ?>
                <th style='width:5%;  text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>'><?php echo date('F', strtotime($starting_month.' +'.($i+1).' months')); ?></th>
            <?php } ?>
        </tr>
        </thead>
        <tbody id="accounts_table">
        <?php foreach($accounts AS $item) {

            if ($item['code'] == 20000) {
                ?>
                <tr id="current_asset" style="font-weight: bold;">
                    <td></td>
                    <td>2, TOTAL CURRENT ASSETS</td>
                    <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
                    <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month . ' +1 month'))); ?>"></td>
                    <?php for($i=1; $i < 11; $i++) { ?>
                        <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime($starting_month.' +'.($i+1).' months'))); ?>"></td>
                    <?php } ?>
                </tr>
                <tr id="asset" style="font-weight: bold;">
                    <td></td>
                    <td>3, TOTAL ASSETS (1+2)</td>
                    <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
                    <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month . ' +1 month'))); ?>"></td>
                    <?php for($i=1; $i < 11; $i++) { ?>
                        <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime($starting_month.' +'.($i+1).' months'))); ?>"></td>
                    <?php } ?>
                </tr>
                <?php
            }

            if ($item['code'] == 30000) {
                ?>
                <tr id="non_current_liability" style="font-weight: bold;">
                    <td></td>
                    <td>5, TOTAL NON-CURRENT LIABILITY</td>
                    <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
                    <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month . ' +1 month'))); ?>"></td>
                    <?php for($i=1; $i < 11; $i++) { ?>
                        <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime($starting_month.' +'.($i+1).' months'))); ?>"></td>
                    <?php } ?>
                </tr>
                <tr id="liability" style="font-weight: bold;">
                    <td></td>
                    <td>6, TOTAL LIABILITY (4+5)</td>
                    <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
                    <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month . ' +1 month'))); ?>"></td>
                    <?php for($i=1; $i < 11; $i++) { ?>
                        <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime($starting_month.' +'.($i+1).' months'))); ?>"></td>
                    <?php } ?>
                </tr>
                <?php
            }

            if ($item['code'] == 12000) {
                ?>
                <tr id="non_current_asset" style="font-weight: bold;">
                    <td></td>
                    <td>1, TOTAL NON-CURRENT ASSET</td>
                    <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
                    <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month . ' +1 month'))); ?>"></td>
                    <?php for($i=1; $i < 11; $i++) { ?>
                        <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime($starting_month.' +'.($i+1).' months'))); ?>"></td>
                    <?php } ?>
                </tr>
                <?php
            }

            if ($item['code'] == 22000) {
                ?>
                <tr id="current_liability" style="font-weight: bold;">
                    <td></td>
                    <td>4, TOTAL CURRENT LIABILITY</td>
                    <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
                    <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month . ' +1 month'))); ?>"></td>
                    <?php for($i=1; $i < 11; $i++) { ?>
                        <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime($starting_month.' +'.($i+1).' months'))); ?>"></td>
                    <?php } ?>
                </tr>
                <?php
            }

            $padding = $item['level']*15;

            $monthly_report = [];
            foreach ($item['monthly_report'] AS $record) {
                if ($item['type'] == 'asset') {

                    if ($record['type'] == 'debit') {
                        $monthly_report[$record['document_month']] += $record['amount'];
                    } else {
                        $monthly_report[$record['document_month']] -= $record['amount'];
                    }
                } else {

                    if ($record['type'] == 'credit') {
                        $monthly_report[$record['document_month']] += $record['amount'];
                    } else {
                        $monthly_report[$record['document_month']] -= $record['amount'];
                    }

                }
            }
        $class_item = '';
        if (!isset($item['children'])) {
            $class_item = $item['type'];

            if ($class_item == 'asset') {
                $class_item = $item['code'] >= 12000 ? 'current_asset' : 'non_current_asset';
            }

            if ($class_item == 'liability') {
                $class_item = $item['code'] < 22000 ? 'current_liability' : 'non_current_liability';
            }
        }
            ?>
            <tr <?php if (($report_type == 'hidden' && ($item['level'] == 4 || $item['level'] == 5)) || ($report_type == 'hidden' && $item['level'] == 3 && $item['code'] >= 30000)) { ?> style="display:none;" <?php } ?> class="<?php echo $class_item; ?> <?php echo $item['id']; ?> account_item" data-level="<?php echo $item['level']; ?>" data-fatherid="<?php echo $item['father_id']; ?>">
                <td style="padding-left: <?php echo $padding; ?>px;"><?php echo $item['code']; ?></td>
                <td style="padding-left: <?php echo $padding; ?>px;"><?php echo $item['name']; ?></td>

                <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"><?php echo isset($monthly_report[strtolower(date('n', strtotime($starting_month)))]) ? $monthly_report[strtolower(date('n', strtotime($starting_month)))] > 0 ? $monthly_report[strtolower(date('n', strtotime($starting_month)))] : '('.abs($monthly_report[strtolower(date('n', strtotime($starting_month)))]).')' : '0'; ?></td>
                <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month . ' +1 month'))); ?>"><?php echo isset($monthly_report[strtolower(date('n', strtotime($starting_month . ' +1 month')))]) ? $monthly_report[strtolower(date('n', strtotime($starting_month . ' +1 month')))] > 0 ? $monthly_report[strtolower(date('n', strtotime($starting_month . ' +1 month')))] : '('.abs($monthly_report[strtolower(date('n', strtotime($starting_month . ' +1 month')))]).')' : '0'; ?></td>
                <?php

                $ending_balance = 0;
                for($i=1; $i < 11; $i++) {

                    $this_month = $monthly_report[strtolower(date('n', strtotime($starting_month.' +'.($i+1).' months')))];

                    if (isset($this_month)) {

                        if ($item['code'] == 30510 || $item['code'] == 30520){
                            $ending_balance = $this_month;
                        } else {
                            $ending_balance += $this_month;
                        }
                    }

                    ?>
                    <td style='width:5%;  text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime($starting_month.' +'.($i+1).' months'))); ?>"><?php echo $ending_balance >= 0 ? $ending_balance : '(' . abs($ending_balance) . ')'; ?></td>
                <?php } ?>
            </tr>
        <?php } ?>
        <tr id="equity" style="font-weight: bold;">
            <td></td>
            <td>7, TOTAL EQUITY</td>
            <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
            <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month . ' +1 month'))); ?>"></td>
            <?php for($i=1; $i < 11; $i++) { ?>
                <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime($starting_month.' +'.($i+1).' months'))); ?>"></td>
            <?php } ?>
        </tr>
        <tr id="equity_and_liability" style="font-weight: bold;">
            <td></td>
            <td>8, TOTAL EQUITY AND LIABILITY (6+7)</td>
            <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
            <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month . ' +1 month'))); ?>"></td>
            <?php for($i=1; $i < 11; $i++) { ?>
                <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime($starting_month.' +'.($i+1).' months'))); ?>"></td>
            <?php } ?>
        </tr>
        </tbody>
    </table>
</div>
<script>

    jQuery(document).ready(function () {
        var total_january_non_current_asset = 0;
        var total_february_non_current_asset = 0;
        var total_march_non_current_asset = 0;
        var total_april_non_current_asset = 0;
        var total_may_non_current_asset = 0;
        var total_june_non_current_asset = 0;
        var total_july_non_current_asset = 0;
        var total_august_non_current_asset = 0;
        var total_september_non_current_asset = 0;
        var total_october_non_current_asset = 0;
        var total_november_non_current_asset = 0;
        var total_december_non_current_asset = 0;

        $('#accounts_table .non_current_asset').each(function() {
            var el = $(this);

            var january = el.find('.january').html();
            var january_negative_value = january.match(/\(([^)]+)\)/);

            if (january_negative_value != null) {
                total_january_non_current_asset -= parseFloat(january_negative_value[1]);
            } else {
                total_january_non_current_asset += parseFloat(january);
            }

            var february = el.find('.february').html();
            var february_negative_value = february.match(/\(([^)]+)\)/);

            if (february_negative_value != null) {
                total_february_non_current_asset -= parseFloat(february_negative_value[1]);
            } else {
                total_february_non_current_asset += parseFloat(february);
            }

            var march = el.find('.march').html();
            var march_negative_value = march.match(/\(([^)]+)\)/);

            if (march_negative_value != null) {
                total_march_non_current_asset -= parseFloat(march_negative_value[1]);
            } else {
                total_march_non_current_asset += parseFloat(march);
            }

            var april = el.find('.april').html();
            var april_negative_value = april.match(/\(([^)]+)\)/);

            if (april_negative_value != null) {
                total_april_non_current_asset -= parseFloat(april_negative_value[1]);
            } else {
                total_april_non_current_asset += parseFloat(april);
            }

            var may = el.find('.may').html();
            var may_negative_value = may.match(/\(([^)]+)\)/);

            if (may_negative_value != null) {
                total_may_non_current_asset -= parseFloat(may_negative_value[1]);
            } else {
                total_may_non_current_asset += parseFloat(may);
            }

            var june = el.find('.june').html();
            var june_negative_value = june.match(/\(([^)]+)\)/);

            if (june_negative_value != null) {
                total_june_non_current_asset -= parseFloat(june_negative_value[1]);
            } else {
                total_june_non_current_asset += parseFloat(june);
            }

            var july = el.find('.july').html();
            var july_negative_value = july.match(/\(([^)]+)\)/);

            if (july_negative_value != null) {
                total_july_non_current_asset -= parseFloat(july_negative_value[1]);
            } else {
                total_july_non_current_asset += parseFloat(july);
            }

            var august = el.find('.august').html();
            var august_negative_value = august.match(/\(([^)]+)\)/);

            if (august_negative_value != null) {
                total_august_non_current_asset -= parseFloat(august_negative_value[1]);
            } else {
                total_august_non_current_asset += parseFloat(august);
            }

            var september = el.find('.september').html();
            var september_negative_value = september.match(/\(([^)]+)\)/);

            if (september_negative_value != null) {
                total_september_non_current_asset -= parseFloat(september_negative_value[1]);
            } else {
                total_september_non_current_asset += parseFloat(september);
            }

            var october = el.find('.october').html();
            var october_negative_value = october.match(/\(([^)]+)\)/);

            if (october_negative_value != null) {
                total_october_non_current_asset -= parseFloat(october_negative_value[1]);
            } else {
                total_october_non_current_asset += parseFloat(october);
            }

            var november = el.find('.november').html();
            var november_negative_value = november.match(/\(([^)]+)\)/);

            if (november_negative_value != null) {
                total_november_non_current_asset -= parseFloat(november_negative_value[1]);
            } else {
                total_november_non_current_asset += parseFloat(november);
            }

            var december = el.find('.december').html();
            var december_negative_value = december.match(/\(([^)]+)\)/);

            if (december_negative_value != null) {
                total_december_non_current_asset -= parseFloat(december_negative_value[1]);
            } else {
                total_december_non_current_asset += parseFloat(december);
            }

        });
        $('#non_current_asset .january').html(total_january_non_current_asset >= 0 ? total_january_non_current_asset : '('+Math.abs(total_january_non_current_asset)+')');
        $('#non_current_asset .february').html(total_february_non_current_asset >= 0 ? total_february_non_current_asset : '('+Math.abs(total_february_non_current_asset)+')');
        $('#non_current_asset .march').html(total_march_non_current_asset >= 0 ? total_march_non_current_asset : '('+Math.abs(total_march_non_current_asset)+')');
        $('#non_current_asset .april').html(total_april_non_current_asset >= 0 ? total_april_non_current_asset : '('+Math.abs(total_april_non_current_asset)+')');
        $('#non_current_asset .may').html(total_may_non_current_asset >= 0 ? total_may_non_current_asset : '('+Math.abs(total_may_non_current_asset)+')');
        $('#non_current_asset .june').html(total_june_non_current_asset >= 0 ? total_june_non_current_asset : '('+Math.abs(total_june_non_current_asset)+')');
        $('#non_current_asset .july').html(total_july_non_current_asset >= 0 ? total_july_non_current_asset : '('+Math.abs(total_july_non_current_asset)+')');
        $('#non_current_asset .august').html(total_august_non_current_asset >= 0 ? total_august_non_current_asset : '('+Math.abs(total_august_non_current_asset)+')');
        $('#non_current_asset .september').html(total_september_non_current_asset >= 0 ? total_september_non_current_asset : '('+Math.abs(total_september_non_current_asset)+')');
        $('#non_current_asset .october').html(total_october_non_current_asset >= 0 ? total_october_non_current_asset : '('+Math.abs(total_october_non_current_asset)+')');
        $('#non_current_asset .november').html(total_november_non_current_asset >= 0 ? total_november_non_current_asset : '('+Math.abs(total_november_non_current_asset)+')');
        $('#non_current_asset .december').html(total_december_non_current_asset >= 0 ? total_december_non_current_asset : '('+Math.abs(total_december_non_current_asset)+')');

        // Current asset
        var total_january_current_asset = 0;
        var total_february_current_asset = 0;
        var total_march_current_asset = 0;
        var total_april_current_asset = 0;
        var total_may_current_asset = 0;
        var total_june_current_asset = 0;
        var total_july_current_asset = 0;
        var total_august_current_asset = 0;
        var total_september_current_asset = 0;
        var total_october_current_asset = 0;
        var total_november_current_asset = 0;
        var total_december_current_asset = 0;

        $('#accounts_table .current_asset').each(function() {
            var el = $(this);

            var january = el.find('.january').html();
            var january_negative_value = january.match(/\(([^)]+)\)/);

            if (january_negative_value != null) {
                total_january_current_asset -= parseFloat(january_negative_value[1]);
            } else {
                total_january_current_asset += parseFloat(january);
            }

            var february = el.find('.february').html();
            var february_negative_value = february.match(/\(([^)]+)\)/);

            if (february_negative_value != null) {
                total_february_current_asset -= parseFloat(february_negative_value[1]);
            } else {
                total_february_current_asset += parseFloat(february);
            }

            var march = el.find('.march').html();
            var march_negative_value = march.match(/\(([^)]+)\)/);

            if (march_negative_value != null) {
                total_march_current_asset -= parseFloat(march_negative_value[1]);
            } else {
                total_march_current_asset += parseFloat(march);
            }

            var april = el.find('.april').html();
            var april_negative_value = april.match(/\(([^)]+)\)/);

            if (april_negative_value != null) {
                total_april_current_asset -= parseFloat(april_negative_value[1]);
            } else {
                total_april_current_asset += parseFloat(april);
            }

            var may = el.find('.may').html();
            var may_negative_value = may.match(/\(([^)]+)\)/);

            if (may_negative_value != null) {
                total_may_current_asset -= parseFloat(may_negative_value[1]);
            } else {
                total_may_current_asset += parseFloat(may);
            }

            var june = el.find('.june').html();
            var june_negative_value = june.match(/\(([^)]+)\)/);

            if (june_negative_value != null) {
                total_june_current_asset -= parseFloat(june_negative_value[1]);
            } else {
                total_june_current_asset += parseFloat(june);
            }

            var july = el.find('.july').html();
            var july_negative_value = july.match(/\(([^)]+)\)/);

            if (july_negative_value != null) {
                total_july_current_asset -= parseFloat(july_negative_value[1]);
            } else {
                total_july_current_asset += parseFloat(july);
            }

            var august = el.find('.august').html();
            var august_negative_value = august.match(/\(([^)]+)\)/);

            if (august_negative_value != null) {
                total_august_current_asset -= parseFloat(august_negative_value[1]);
            } else {
                total_august_current_asset += parseFloat(august);
            }

            var september = el.find('.september').html();
            var september_negative_value = september.match(/\(([^)]+)\)/);

            if (september_negative_value != null) {
                total_september_current_asset -= parseFloat(september_negative_value[1]);
            } else {
                total_september_current_asset += parseFloat(september);
            }

            var october = el.find('.october').html();
            var october_negative_value = october.match(/\(([^)]+)\)/);

            if (october_negative_value != null) {
                total_october_current_asset -= parseFloat(october_negative_value[1]);
            } else {
                total_october_current_asset += parseFloat(october);
            }

            var november = el.find('.november').html();
            var november_negative_value = november.match(/\(([^)]+)\)/);

            if (november_negative_value != null) {
                total_november_current_asset -= parseFloat(november_negative_value[1]);
            } else {
                total_november_current_asset += parseFloat(november);
            }

            var december = el.find('.december').html();
            var december_negative_value = december.match(/\(([^)]+)\)/);

            if (december_negative_value != null) {
                total_december_current_asset -= parseFloat(december_negative_value[1]);
            } else {
                total_december_current_asset += parseFloat(december);
            }

        });
        var january_asset = total_january_non_current_asset + total_january_current_asset;

        $('#current_asset .january').html(total_january_current_asset >= 0 ? total_january_current_asset : '('+Math.abs(total_january_current_asset)+')');

        $('#asset .january').html(january_asset >= 0 ? january_asset : '('+Math.abs(january_asset)+')');

        var february_asset = total_february_non_current_asset + total_february_current_asset;

        $('#current_asset .february').html(total_february_current_asset >= 0 ? total_february_current_asset : '('+Math.abs(total_february_current_asset)+')');

        $('#asset .february').html(february_asset >= 0 ? february_asset : '('+Math.abs(february_asset)+')');

        var march_asset = total_march_non_current_asset + total_march_current_asset;

        $('#current_asset .march').html(total_march_current_asset >= 0 ? total_march_current_asset : '('+Math.abs(total_march_current_asset)+')');

        $('#asset .march').html(march_asset >= 0 ? march_asset : '('+Math.abs(march_asset)+')');

        var april_asset = total_april_non_current_asset + total_april_current_asset;

        $('#current_asset .april').html(total_april_current_asset >= 0 ? total_april_current_asset : '('+Math.abs(total_april_current_asset)+')');

        $('#asset .april').html(april_asset >= 0 ? april_asset : '('+Math.abs(april_asset)+')');

        var may_asset = total_may_non_current_asset + total_may_current_asset;

        $('#current_asset .may').html(total_may_current_asset >= 0 ? total_may_current_asset : '('+Math.abs(total_may_current_asset)+')');

        $('#asset .may').html(may_asset >= 0 ? may_asset : '('+Math.abs(may_asset)+')');

        var june_asset = total_june_non_current_asset + total_june_current_asset;

        $('#current_asset .june').html(total_june_current_asset >= 0 ? total_june_current_asset : '('+Math.abs(total_june_current_asset)+')');

        $('#asset .june').html(june_asset >= 0 ? june_asset : '('+Math.abs(june_asset)+')');

        var july_asset = total_july_non_current_asset + total_july_current_asset;

        $('#current_asset .july').html(total_july_current_asset >= 0 ? total_july_current_asset : '('+Math.abs(total_july_current_asset)+')');

        $('#asset .july').html(july_asset >= 0 ? july_asset : '('+Math.abs(july_asset)+')');

        var august_asset = total_august_non_current_asset + total_august_current_asset;

        $('#current_asset .august').html(total_august_current_asset >= 0 ? total_august_current_asset : '('+Math.abs(total_august_current_asset)+')');

        $('#asset .august').html(august_asset >= 0 ? august_asset : '('+Math.abs(august_asset)+')');

        var september_asset = total_september_non_current_asset + total_september_current_asset;

        $('#current_asset .september').html(total_september_current_asset >= 0 ? total_september_current_asset : '('+Math.abs(total_september_current_asset)+')');

        $('#asset .september').html(september_asset >= 0 ? september_asset : '('+Math.abs(september_asset)+')');

        var october_asset = total_october_non_current_asset + total_october_current_asset;

        $('#current_asset .october').html(total_october_current_asset >= 0 ? total_october_current_asset : '('+Math.abs(total_october_current_asset)+')');

        $('#asset .october').html(october_asset >= 0 ? october_asset : '('+Math.abs(october_asset)+')');

        var november_asset = total_november_non_current_asset + total_november_current_asset;

        $('#current_asset .november').html(total_november_current_asset >= 0 ? total_november_current_asset : '('+Math.abs(total_november_current_asset)+')');

        $('#asset .november').html(november_asset >= 0 ? november_asset : '('+Math.abs(november_asset)+')');

        var december_asset = total_december_non_current_asset + total_december_current_asset;

        $('#current_asset .december').html(total_december_current_asset >= 0 ? total_december_current_asset : '('+Math.abs(total_december_current_asset)+')');

        $('#asset .december').html(december_asset >= 0 ? december_asset : '('+Math.abs(december_asset)+')');

        // Current liability
        var total_january_current_liability = 0;
        var total_february_current_liability = 0;
        var total_march_current_liability = 0;
        var total_april_current_liability = 0;
        var total_may_current_liability = 0;
        var total_june_current_liability = 0;
        var total_july_current_liability = 0;
        var total_august_current_liability = 0;
        var total_september_current_liability = 0;
        var total_october_current_liability = 0;
        var total_november_current_liability = 0;
        var total_december_current_liability = 0;

        $('#accounts_table .current_liability').each(function() {
            var el = $(this);

            var january = el.find('.january').html();
            var january_negative_value = january.match(/\(([^)]+)\)/);

            if (january_negative_value != null) {
                total_january_current_liability -= parseFloat(january_negative_value[1]);
            } else {
                total_january_current_liability += parseFloat(january);
            }

            var february = el.find('.february').html();
            var february_negative_value = february.match(/\(([^)]+)\)/);

            if (february_negative_value != null) {
                total_february_current_liability -= parseFloat(february_negative_value[1]);
            } else {
                total_february_current_liability += parseFloat(february);
            }

            var march = el.find('.march').html();
            var march_negative_value = march.match(/\(([^)]+)\)/);

            if (march_negative_value != null) {
                total_march_current_liability -= parseFloat(march_negative_value[1]);
            } else {
                total_march_current_liability += parseFloat(march);
            }

            var april = el.find('.april').html();
            var april_negative_value = april.match(/\(([^)]+)\)/);

            if (april_negative_value != null) {
                total_april_current_liability -= parseFloat(april_negative_value[1]);
            } else {
                total_april_current_liability += parseFloat(april);
            }

            var may = el.find('.may').html();
            var may_negative_value = may.match(/\(([^)]+)\)/);

            if (may_negative_value != null) {
                total_may_current_liability -= parseFloat(may_negative_value[1]);
            } else {
                total_may_current_liability += parseFloat(may);
            }

            var june = el.find('.june').html();
            var june_negative_value = june.match(/\(([^)]+)\)/);

            if (june_negative_value != null) {
                total_june_current_liability -= parseFloat(june_negative_value[1]);
            } else {
                total_june_current_liability += parseFloat(june);
            }

            var july = el.find('.july').html();
            var july_negative_value = july.match(/\(([^)]+)\)/);

            if (july_negative_value != null) {
                total_july_current_liability -= parseFloat(july_negative_value[1]);
            } else {
                total_july_current_liability += parseFloat(july);
            }

            var august = el.find('.august').html();
            var august_negative_value = august.match(/\(([^)]+)\)/);

            if (august_negative_value != null) {
                total_august_current_liability -= parseFloat(august_negative_value[1]);
            } else {
                total_august_current_liability += parseFloat(august);
            }

            var september = el.find('.september').html();
            var september_negative_value = september.match(/\(([^)]+)\)/);

            if (september_negative_value != null) {
                total_september_current_liability -= parseFloat(september_negative_value[1]);
            } else {
                total_september_current_liability += parseFloat(september);
            }

            var october = el.find('.october').html();
            var october_negative_value = october.match(/\(([^)]+)\)/);

            if (october_negative_value != null) {
                total_october_current_liability -= parseFloat(october_negative_value[1]);
            } else {
                total_october_current_liability += parseFloat(october);
            }

            var november = el.find('.november').html();
            var november_negative_value = november.match(/\(([^)]+)\)/);

            if (november_negative_value != null) {
                total_november_current_liability -= parseFloat(november_negative_value[1]);
            } else {
                total_november_current_liability += parseFloat(november);
            }

            var december = el.find('.december').html();
            var december_negative_value = december.match(/\(([^)]+)\)/);

            if (december_negative_value != null) {
                total_december_current_liability -= parseFloat(december_negative_value[1]);
            } else {
                total_december_current_liability += parseFloat(december);
            }


        });

        $('#current_liability .january').html(total_january_current_liability >= 0 ? total_january_current_liability : '('+Math.abs(total_january_current_liability)+')');
        $('#current_liability .february').html(total_february_current_liability >= 0 ? total_february_current_liability : '('+Math.abs(total_february_current_liability)+')');
        $('#current_liability .march').html(total_march_current_liability >= 0 ? total_march_current_liability : '('+Math.abs(total_march_current_liability)+')');
        $('#current_liability .april').html(total_april_current_liability >= 0 ? total_april_current_liability : '('+Math.abs(total_april_current_liability)+')');
        $('#current_liability .may').html(total_may_current_liability >= 0 ? total_may_current_liability : '('+Math.abs(total_may_current_liability)+')');
        $('#current_liability .june').html(total_june_current_liability >= 0 ? total_june_current_liability : '('+Math.abs(total_june_current_liability)+')');
        $('#current_liability .july').html(total_july_current_liability >= 0 ? total_july_current_liability : '('+Math.abs(total_july_current_liability)+')');
        $('#current_liability .august').html(total_august_current_liability >= 0 ? total_august_current_liability : '('+Math.abs(total_august_current_liability)+')');
        $('#current_liability .september').html(total_september_current_liability >= 0 ? total_september_current_liability : '('+Math.abs(total_september_current_liability)+')');
        $('#current_liability .october').html(total_october_current_liability >= 0 ? total_october_current_liability : '('+Math.abs(total_october_current_liability)+')');
        $('#current_liability .november').html(total_november_current_liability >= 0 ? total_november_current_liability : '('+Math.abs(total_november_current_liability)+')');
        $('#current_liability .december').html(total_december_current_liability >= 0 ? total_december_current_liability : '('+Math.abs(total_december_current_liability)+')');

        // Non-current liability
        var total_january_non_current_liability = 0;
        var total_february_non_current_liability = 0;
        var total_march_non_current_liability = 0;
        var total_april_non_current_liability = 0;
        var total_may_non_current_liability = 0;
        var total_june_non_current_liability = 0;
        var total_july_non_current_liability = 0;
        var total_august_non_current_liability = 0;
        var total_september_non_current_liability = 0;
        var total_october_non_current_liability = 0;
        var total_november_non_current_liability = 0;
        var total_december_non_current_liability = 0;

        $('#accounts_table .non_current_liability').each(function() {
            var el = $(this);

            var january = el.find('.january').html();
            var january_negative_value = january.match(/\(([^)]+)\)/);

            if (january_negative_value != null) {
                total_january_non_current_liability -= parseFloat(january_negative_value[1]);
            } else {
                total_january_non_current_liability += parseFloat(january);
            }

            var february = el.find('.february').html();
            var february_negative_value = february.match(/\(([^)]+)\)/);

            if (february_negative_value != null) {
                total_february_non_current_liability -= parseFloat(february_negative_value[1]);
            } else {
                total_february_non_current_liability += parseFloat(february);
            }

            var march = el.find('.march').html();
            var march_negative_value = march.match(/\(([^)]+)\)/);

            if (march_negative_value != null) {
                total_march_non_current_liability -= parseFloat(march_negative_value[1]);
            } else {
                total_march_non_current_liability += parseFloat(march);
            }

            var april = el.find('.april').html();
            var april_negative_value = april.match(/\(([^)]+)\)/);

            if (april_negative_value != null) {
                total_april_non_current_liability -= parseFloat(april_negative_value[1]);
            } else {
                total_april_non_current_liability += parseFloat(april);
            }

            var may = el.find('.may').html();
            var may_negative_value = may.match(/\(([^)]+)\)/);

            if (may_negative_value != null) {
                total_may_non_current_liability -= parseFloat(may_negative_value[1]);
            } else {
                total_may_non_current_liability += parseFloat(may);
            }

            var june = el.find('.june').html();
            var june_negative_value = june.match(/\(([^)]+)\)/);

            if (june_negative_value != null) {
                total_june_non_current_liability -= parseFloat(june_negative_value[1]);
            } else {
                total_june_non_current_liability += parseFloat(june);
            }

            var july = el.find('.july').html();
            var july_negative_value = july.match(/\(([^)]+)\)/);

            if (july_negative_value != null) {
                total_july_non_current_liability -= parseFloat(july_negative_value[1]);
            } else {
                total_july_non_current_liability += parseFloat(july);
            }

            var august = el.find('.august').html();
            var august_negative_value = august.match(/\(([^)]+)\)/);

            if (august_negative_value != null) {
                total_august_non_current_liability -= parseFloat(august_negative_value[1]);
            } else {
                total_august_non_current_liability += parseFloat(august);
            }

            var september = el.find('.september').html();
            var september_negative_value = september.match(/\(([^)]+)\)/);

            if (september_negative_value != null) {
                total_september_non_current_liability -= parseFloat(september_negative_value[1]);
            } else {
                total_september_non_current_liability += parseFloat(september);
            }

            var october = el.find('.october').html();
            var october_negative_value = october.match(/\(([^)]+)\)/);

            if (october_negative_value != null) {
                total_october_non_current_liability -= parseFloat(october_negative_value[1]);
            } else {
                total_october_non_current_liability += parseFloat(october);
            }

            var november = el.find('.november').html();
            var november_negative_value = november.match(/\(([^)]+)\)/);

            if (november_negative_value != null) {
                total_november_non_current_liability -= parseFloat(november_negative_value[1]);
            } else {
                total_november_non_current_liability += parseFloat(november);
            }

            var december = el.find('.december').html();
            var december_negative_value = december.match(/\(([^)]+)\)/);

            if (december_negative_value != null) {
                total_december_non_current_liability -= parseFloat(december_negative_value[1]);
            } else {
                total_december_non_current_liability += parseFloat(december);
            }

        });
        var january_liability = total_january_non_current_liability + total_january_current_liability;

        $('#non_current_liability .january').html(total_january_non_current_liability >= 0 ? total_january_non_current_liability : '('+Math.abs(total_january_non_current_liability)+')');

        $('#liability .january').html(january_liability >= 0 ? january_liability : '('+Math.abs(january_liability)+')');

        var february_liability = total_february_non_current_liability + total_february_current_liability;

        $('#non_current_liability .february').html(total_february_non_current_liability >= 0 ? total_february_non_current_liability : '('+Math.abs(total_february_non_current_liability)+')');

        $('#liability .february').html(february_liability >= 0 ? february_liability : '('+Math.abs(february_liability)+')');

        var march_liability = total_march_non_current_liability + total_march_current_liability;

        $('#non_current_liability .march').html(total_march_non_current_liability >= 0 ? total_march_non_current_liability : '('+Math.abs(total_march_non_current_liability)+')');

        $('#liability .march').html(march_liability >= 0 ? march_liability : '('+Math.abs(march_liability)+')');

        var april_liability = total_april_non_current_liability + total_april_current_liability;

        $('#non_current_liability .april').html(total_april_non_current_liability >= 0 ? total_april_non_current_liability : '('+Math.abs(total_april_non_current_liability)+')');

        $('#liability .april').html(april_liability >= 0 ? april_liability : '('+Math.abs(april_liability)+')');

        var may_liability = total_may_non_current_liability + total_may_current_liability;

        $('#non_current_liability .may').html(total_may_non_current_liability >= 0 ? total_may_non_current_liability : '('+Math.abs(total_may_non_current_liability)+')');

        $('#liability .may').html(may_liability >= 0 ? may_liability : '('+Math.abs(may_liability)+')');

        var june_liability = total_june_non_current_liability + total_june_current_liability;

        $('#non_current_liability .june').html(total_june_non_current_liability >= 0 ? total_june_non_current_liability : '('+Math.abs(total_june_non_current_liability)+')');

        $('#liability .june').html(june_liability >= 0 ? june_liability : '('+Math.abs(june_liability)+')');

        var july_liability = total_july_non_current_liability + total_july_current_liability;

        $('#non_current_liability .july').html(total_july_non_current_liability >= 0 ? total_july_non_current_liability : '('+Math.abs(total_july_non_current_liability)+')');

        $('#liability .july').html(july_liability >= 0 ? july_liability : '('+Math.abs(july_liability)+')');

        var august_liability = total_august_non_current_liability + total_august_current_liability;

        $('#non_current_liability .august').html(total_august_non_current_liability >= 0 ? total_august_non_current_liability : '('+Math.abs(total_august_non_current_liability)+')');

        $('#liability .august').html(august_liability >= 0 ? august_liability : '('+Math.abs(august_liability)+')');

        var september_liability = total_september_non_current_liability + total_september_current_liability;

        $('#non_current_liability .september').html(total_september_non_current_liability >= 0 ? total_september_non_current_liability : '('+Math.abs(total_september_non_current_liability)+')');

        $('#liability .september').html(september_liability >= 0 ? september_liability : '('+Math.abs(september_liability)+')');

        var october_liability = total_october_non_current_liability + total_october_current_liability;

        $('#non_current_liability .october').html(total_october_non_current_liability >= 0 ? total_october_non_current_liability : '('+Math.abs(total_october_non_current_liability)+')');

        $('#liability .october').html(october_liability >= 0 ? october_liability : '('+Math.abs(october_liability)+')');

        var november_liability = total_november_non_current_liability + total_november_current_liability;

        $('#non_current_liability .november').html(total_november_non_current_liability >= 0 ? total_november_non_current_liability : '('+Math.abs(total_november_non_current_liability)+')');

        $('#liability .november').html(november_liability >= 0 ? november_liability : '('+Math.abs(november_liability)+')');

        var december_liability = total_december_non_current_liability + total_december_current_liability;

        $('#non_current_liability .december').html(total_december_non_current_liability >= 0 ? total_december_non_current_liability : '('+Math.abs(total_december_non_current_liability)+')');

        $('#liability .december').html(december_liability >= 0 ? december_liability : '('+Math.abs(december_liability)+')');

        // Equity
        var total_january_equity = 0;
        var total_february_equity = 0;
        var total_march_equity = 0;
        var total_april_equity = 0;
        var total_may_equity = 0;
        var total_june_equity = 0;
        var total_july_equity = 0;
        var total_august_equity = 0;
        var total_september_equity = 0;
        var total_october_equity = 0;
        var total_november_equity = 0;
        var total_december_equity = 0;

        $('#accounts_table .equity').each(function() {
            var el = $(this);

            var january = el.find('.january').html();
            var january_negative_value = january.match(/\(([^)]+)\)/);

            if (january_negative_value != null) {
                total_january_equity -= parseFloat(january_negative_value[1]);
            } else {
                total_january_equity += parseFloat(january);
            }

            var february = el.find('.february').html();
            var february_negative_value = february.match(/\(([^)]+)\)/);

            if (february_negative_value != null) {
                total_february_equity -= parseFloat(february_negative_value[1]);
            } else {
                total_february_equity += parseFloat(february);
            }

            var march = el.find('.march').html();
            var march_negative_value = march.match(/\(([^)]+)\)/);

            if (march_negative_value != null) {
                total_march_equity -= parseFloat(march_negative_value[1]);
            } else {
                total_march_equity += parseFloat(march);
            }

            var april = el.find('.april').html();
            var april_negative_value = april.match(/\(([^)]+)\)/);

            if (april_negative_value != null) {
                total_april_equity -= parseFloat(april_negative_value[1]);
            } else {
                total_april_equity += parseFloat(april);
            }

            var may = el.find('.may').html();
            var may_negative_value = may.match(/\(([^)]+)\)/);

            if (may_negative_value != null) {
                total_may_equity -= parseFloat(may_negative_value[1]);
            } else {
                total_may_equity += parseFloat(may);
            }

            var june = el.find('.june').html();
            var june_negative_value = june.match(/\(([^)]+)\)/);

            if (june_negative_value != null) {
                total_june_equity -= parseFloat(june_negative_value[1]);
            } else {
                total_june_equity += parseFloat(june);
            }

            var july = el.find('.july').html();
            var july_negative_value = july.match(/\(([^)]+)\)/);

            if (july_negative_value != null) {
                total_july_equity -= parseFloat(july_negative_value[1]);
            } else {
                total_july_equity += parseFloat(july);
            }

            var august = el.find('.august').html();
            var august_negative_value = august.match(/\(([^)]+)\)/);

            if (august_negative_value != null) {
                total_august_equity -= parseFloat(august_negative_value[1]);
            } else {
                total_august_equity += parseFloat(august);
            }

            var september = el.find('.september').html();
            var september_negative_value = september.match(/\(([^)]+)\)/);

            if (september_negative_value != null) {
                total_september_equity -= parseFloat(september_negative_value[1]);
            } else {
                total_september_equity += parseFloat(september);
            }

            var october = el.find('.october').html();
            var october_negative_value = october.match(/\(([^)]+)\)/);

            if (october_negative_value != null) {
                total_october_equity -= parseFloat(october_negative_value[1]);
            } else {
                total_october_equity += parseFloat(october);
            }

            var november = el.find('.november').html();
            var november_negative_value = november.match(/\(([^)]+)\)/);

            if (november_negative_value != null) {
                total_november_equity -= parseFloat(november_negative_value[1]);
            } else {
                total_november_equity += parseFloat(november);
            }

            var december = el.find('.december').html();
            var december_negative_value = december.match(/\(([^)]+)\)/);

            if (december_negative_value != null) {
                total_december_equity -= parseFloat(december_negative_value[1]);
            } else {
                total_december_equity += parseFloat(december);
            }

        });
        var january_equity_and_liability = total_january_equity + january_liability;

        $('#equity .january').html(total_january_equity >= 0 ? total_january_equity : '('+Math.abs(total_january_equity)+')');

        $('#equity_and_liability .january').html(january_equity_and_liability >= 0 ? january_equity_and_liability : '('+Math.abs(january_equity_and_liability)+')');

        var february_equity_and_liability = total_february_equity + february_liability;

        $('#equity .february').html(total_february_equity >= 0 ? total_february_equity : '('+Math.abs(total_february_equity)+')');

        $('#equity_and_liability .february').html(february_equity_and_liability >= 0 ? february_equity_and_liability : '('+Math.abs(february_equity_and_liability)+')');

        var march_equity_and_liability = total_march_equity + march_liability;

        $('#equity .march').html(total_march_equity >= 0 ? total_march_equity : '('+Math.abs(total_march_equity)+')');

        $('#equity_and_liability .march').html(march_equity_and_liability >= 0 ? march_equity_and_liability : '('+Math.abs(march_equity_and_liability)+')');

        var april_equity_and_liability = total_april_equity + april_liability;

        $('#equity .april').html(total_april_equity >= 0 ? total_april_equity : '('+Math.abs(total_april_equity)+')');

        $('#equity_and_liability .april').html(april_equity_and_liability >= 0 ? april_equity_and_liability : '('+Math.abs(april_equity_and_liability)+')');

        var may_equity_and_liability = total_may_equity + may_liability;

        $('#equity .may').html(total_may_equity >= 0 ? total_may_equity : '('+Math.abs(total_may_equity)+')');

        $('#equity_and_liability .may').html(may_equity_and_liability >= 0 ? may_equity_and_liability : '('+Math.abs(may_equity_and_liability)+')');

        var june_equity_and_liability = total_june_equity + june_liability;

        $('#equity .june').html(total_june_equity >= 0 ? total_june_equity : '('+Math.abs(total_june_equity)+')');

        $('#equity_and_liability .june').html(june_equity_and_liability >= 0 ? june_equity_and_liability : '('+Math.abs(june_equity_and_liability)+')');

        var july_equity_and_liability = total_july_equity + july_liability;

        $('#equity .july').html(total_july_equity >= 0 ? total_july_equity : '('+Math.abs(total_july_equity)+')');

        $('#equity_and_liability .july').html(july_equity_and_liability >= 0 ? july_equity_and_liability : '('+Math.abs(july_equity_and_liability)+')');

        var august_equity_and_liability = total_august_equity + august_liability;

        $('#equity .august').html(total_august_equity >= 0 ? total_august_equity : '('+Math.abs(total_august_equity)+')');

        $('#equity_and_liability .august').html(august_equity_and_liability >= 0 ? august_equity_and_liability : '('+Math.abs(august_equity_and_liability)+')');

        var september_equity_and_liability = total_september_equity + september_liability;

        $('#equity .september').html(total_september_equity >= 0 ? total_september_equity : '('+Math.abs(total_september_equity)+')');

        $('#equity_and_liability .september').html(september_equity_and_liability >= 0 ? september_equity_and_liability : '('+Math.abs(september_equity_and_liability)+')');

        var october_equity_and_liability = total_october_equity + october_liability;

        $('#equity .october').html(total_october_equity >= 0 ? total_october_equity : '('+Math.abs(total_october_equity)+')');

        $('#equity_and_liability .october').html(october_equity_and_liability >= 0 ? october_equity_and_liability : '('+Math.abs(october_equity_and_liability)+')');

        var november_equity_and_liability = total_november_equity + november_liability;

        $('#equity .november').html(total_november_equity >= 0 ? total_november_equity : '('+Math.abs(total_november_equity)+')');

        $('#equity_and_liability .november').html(november_equity_and_liability >= 0 ? november_equity_and_liability : '('+Math.abs(november_equity_and_liability)+')');

        var december_equity_and_liability = total_december_equity + december_liability;

        $('#equity .december').html(total_december_equity >= 0 ? total_december_equity : '('+Math.abs(total_december_equity)+')');

        $('#equity_and_liability .december').html(december_equity_and_liability >= 0 ? december_equity_and_liability : '('+Math.abs(december_equity_and_liability)+')');

        $('#accounts_table tr').each(function() {
            var el = $(this);

            if (el.data('level') == 5) {
                var father_id = el.data('fatherid');

                var father_amount_january = $('.'+father_id).find('.january').html();
                var father_amount_january_negative_value = father_amount_january.match(/\(([^)]+)\)/);
                if (father_amount_january_negative_value != null) {
                    father_amount_january = -parseFloat(father_amount_january_negative_value[1]);
                } else {
                    father_amount_january = parseFloat(father_amount_january);
                }

                var january_amount = el.find('.january').html();
                var january_negative_value = january_amount.match(/\(([^)]+)\)/);

                if (january_negative_value != null) {
                    father_amount_january -= parseFloat(january_negative_value[1]);
                } else {
                    father_amount_january += parseFloat(january_amount);
                }

                $('.'+father_id).find('.january').html(father_amount_january >= 0 ? father_amount_january : '('+Math.abs(father_amount_january)+')');

                var father_id = el.data('fatherid');

                var father_amount_february = $('.'+father_id).find('.february').html();
                var father_amount_february_negative_value = father_amount_february.match(/\(([^)]+)\)/);
                if (father_amount_february_negative_value != null) {
                    father_amount_february = -parseFloat(father_amount_february_negative_value[1]);
                } else {
                    father_amount_february = parseFloat(father_amount_february);
                }

                var february_amount = el.find('.february').html();
                var february_negative_value = february_amount.match(/\(([^)]+)\)/);

                if (february_negative_value != null) {
                    father_amount_february -= parseFloat(february_negative_value[1]);
                } else {
                    father_amount_february += parseFloat(february_amount);
                }

                $('.'+father_id).find('.february').html(father_amount_february >= 0 ? father_amount_february : '('+Math.abs(father_amount_february)+')');

                var father_id = el.data('fatherid');

                var father_amount_march = $('.'+father_id).find('.march').html();
                var father_amount_march_negative_value = father_amount_march.match(/\(([^)]+)\)/);
                if (father_amount_march_negative_value != null) {
                    father_amount_march = -parseFloat(father_amount_march_negative_value[1]);
                } else {
                    father_amount_march = parseFloat(father_amount_march);
                }

                var march_amount = el.find('.march').html();
                var march_negative_value = march_amount.match(/\(([^)]+)\)/);

                if (march_negative_value != null) {
                    father_amount_march -= parseFloat(march_negative_value[1]);
                } else {
                    father_amount_march += parseFloat(march_amount);
                }

                $('.'+father_id).find('.march').html(father_amount_march >= 0 ? father_amount_march : '('+Math.abs(father_amount_march)+')');

                var father_id = el.data('fatherid');

                var father_amount_april = $('.'+father_id).find('.april').html();
                var father_amount_april_negative_value = father_amount_april.match(/\(([^)]+)\)/);
                if (father_amount_april_negative_value != null) {
                    father_amount_april = -parseFloat(father_amount_april_negative_value[1]);
                } else {
                    father_amount_april = parseFloat(father_amount_april);
                }

                var april_amount = el.find('.april').html();
                var april_negative_value = april_amount.match(/\(([^)]+)\)/);

                if (april_negative_value != null) {
                    father_amount_april -= parseFloat(april_negative_value[1]);
                } else {
                    father_amount_april += parseFloat(april_amount);
                }

                $('.'+father_id).find('.april').html(father_amount_april >= 0 ? father_amount_april : '('+Math.abs(father_amount_april)+')');

                var father_id = el.data('fatherid');

                var father_amount_may = $('.'+father_id).find('.may').html();
                var father_amount_may_negative_value = father_amount_may.match(/\(([^)]+)\)/);
                if (father_amount_may_negative_value != null) {
                    father_amount_may = -parseFloat(father_amount_may_negative_value[1]);
                } else {
                    father_amount_may = parseFloat(father_amount_may);
                }

                var may_amount = el.find('.may').html();
                var may_negative_value = may_amount.match(/\(([^)]+)\)/);

                if (may_negative_value != null) {
                    father_amount_may -= parseFloat(may_negative_value[1]);
                } else {
                    father_amount_may += parseFloat(may_amount);
                }

                $('.'+father_id).find('.may').html(father_amount_may >= 0 ? father_amount_may : '('+Math.abs(father_amount_may)+')');

                var father_id = el.data('fatherid');

                var father_amount_june = $('.'+father_id).find('.june').html();
                var father_amount_june_negative_value = father_amount_june.match(/\(([^)]+)\)/);
                if (father_amount_june_negative_value != null) {
                    father_amount_june = -parseFloat(father_amount_june_negative_value[1]);
                } else {
                    father_amount_june = parseFloat(father_amount_june);
                }

                var june_amount = el.find('.june').html();
                var june_negative_value = june_amount.match(/\(([^)]+)\)/);

                if (june_negative_value != null) {
                    father_amount_june -= parseFloat(june_negative_value[1]);
                } else {
                    father_amount_june += parseFloat(june_amount);
                }

                $('.'+father_id).find('.june').html(father_amount_june >= 0 ? father_amount_june : '('+Math.abs(father_amount_june)+')');

                var father_id = el.data('fatherid');

                var father_amount_july = $('.'+father_id).find('.july').html();
                var father_amount_july_negative_value = father_amount_july.match(/\(([^)]+)\)/);
                if (father_amount_july_negative_value != null) {
                    father_amount_july = -parseFloat(father_amount_july_negative_value[1]);
                } else {
                    father_amount_july = parseFloat(father_amount_july);
                }

                var july_amount = el.find('.july').html();
                var july_negative_value = july_amount.match(/\(([^)]+)\)/);

                if (july_negative_value != null) {
                    father_amount_july -= parseFloat(july_negative_value[1]);
                } else {
                    father_amount_july += parseFloat(july_amount);
                }

                $('.'+father_id).find('.july').html(father_amount_july >= 0 ? father_amount_july : '('+Math.abs(father_amount_july)+')');

                var father_id = el.data('fatherid');

                var father_amount_august = $('.'+father_id).find('.august').html();
                var father_amount_august_negative_value = father_amount_august.match(/\(([^)]+)\)/);
                if (father_amount_august_negative_value != null) {
                    father_amount_august = -parseFloat(father_amount_august_negative_value[1]);
                } else {
                    father_amount_august = parseFloat(father_amount_august);
                }

                var august_amount = el.find('.august').html();
                var august_negative_value = august_amount.match(/\(([^)]+)\)/);

                if (august_negative_value != null) {
                    father_amount_august -= parseFloat(august_negative_value[1]);
                } else {
                    father_amount_august += parseFloat(august_amount);
                }

                $('.'+father_id).find('.august').html(father_amount_august >= 0 ? father_amount_august : '('+Math.abs(father_amount_august)+')');

                var father_id = el.data('fatherid');

                var father_amount_september = $('.'+father_id).find('.september').html();
                var father_amount_september_negative_value = father_amount_september.match(/\(([^)]+)\)/);
                if (father_amount_september_negative_value != null) {
                    father_amount_september = -parseFloat(father_amount_september_negative_value[1]);
                } else {
                    father_amount_september = parseFloat(father_amount_september);
                }

                var september_amount = el.find('.september').html();
                var september_negative_value = september_amount.match(/\(([^)]+)\)/);

                if (september_negative_value != null) {
                    father_amount_september -= parseFloat(september_negative_value[1]);
                } else {
                    father_amount_september += parseFloat(september_amount);
                }

                $('.'+father_id).find('.september').html(father_amount_september >= 0 ? father_amount_september : '('+Math.abs(father_amount_september)+')');

                var father_id = el.data('fatherid');

                var father_amount_october = $('.'+father_id).find('.october').html();
                var father_amount_october_negative_value = father_amount_october.match(/\(([^)]+)\)/);
                if (father_amount_october_negative_value != null) {
                    father_amount_october = -parseFloat(father_amount_october_negative_value[1]);
                } else {
                    father_amount_october = parseFloat(father_amount_october);
                }

                var october_amount = el.find('.october').html();
                var october_negative_value = october_amount.match(/\(([^)]+)\)/);

                if (october_negative_value != null) {
                    father_amount_october -= parseFloat(october_negative_value[1]);
                } else {
                    father_amount_october += parseFloat(october_amount);
                }

                $('.'+father_id).find('.october').html(father_amount_october >= 0 ? father_amount_october : '('+Math.abs(father_amount_october)+')');

                var father_id = el.data('fatherid');

                var father_amount_november = $('.'+father_id).find('.november').html();
                var father_amount_november_negative_value = father_amount_november.match(/\(([^)]+)\)/);
                if (father_amount_november_negative_value != null) {
                    father_amount_november = -parseFloat(father_amount_november_negative_value[1]);
                } else {
                    father_amount_november = parseFloat(father_amount_november);
                }

                var november_amount = el.find('.november').html();
                var november_negative_value = november_amount.match(/\(([^)]+)\)/);

                if (november_negative_value != null) {
                    father_amount_november -= parseFloat(november_negative_value[1]);
                } else {
                    father_amount_november += parseFloat(november_amount);
                }

                $('.'+father_id).find('.november').html(father_amount_november >= 0 ? father_amount_november : '('+Math.abs(father_amount_november)+')');

                var father_id = el.data('fatherid');

                var father_amount_december = $('.'+father_id).find('.december').html();
                var father_amount_december_negative_value = father_amount_december.match(/\(([^)]+)\)/);
                if (father_amount_december_negative_value != null) {
                    father_amount_december = -parseFloat(father_amount_december_negative_value[1]);
                } else {
                    father_amount_december = parseFloat(father_amount_december);
                }

                var december_amount = el.find('.december').html();
                var december_negative_value = december_amount.match(/\(([^)]+)\)/);

                if (december_negative_value != null) {
                    father_amount_december -= parseFloat(december_negative_value[1]);
                } else {
                    father_amount_december += parseFloat(december_amount);
                }

                $('.'+father_id).find('.december').html(father_amount_december >= 0 ? father_amount_december : '('+Math.abs(father_amount_december)+')');
            }

        });


        $('#accounts_table tr').each(function() {
            var el = $(this);

            if (el.data('level') == 4) {
                var father_id = el.data('fatherid');

                var father_amount_january = $('.'+father_id).find('.january').html();
                var father_amount_january_negative_value = father_amount_january.match(/\(([^)]+)\)/);
                if (father_amount_january_negative_value != null) {
                    father_amount_january = -parseFloat(father_amount_january_negative_value[1]);
                } else {
                    father_amount_january = parseFloat(father_amount_january);
                }

                var january_amount = el.find('.january').html();
                var january_negative_value = january_amount.match(/\(([^)]+)\)/);

                if (january_negative_value != null) {
                    father_amount_january -= parseFloat(january_negative_value[1]);
                } else {
                    father_amount_january += parseFloat(january_amount);
                }

                $('.'+father_id).find('.january').html(father_amount_january >= 0 ? father_amount_january : '('+Math.abs(father_amount_january)+')');

                var father_id = el.data('fatherid');

                var father_amount_february = $('.'+father_id).find('.february').html();
                var father_amount_february_negative_value = father_amount_february.match(/\(([^)]+)\)/);
                if (father_amount_february_negative_value != null) {
                    father_amount_february = -parseFloat(father_amount_february_negative_value[1]);
                } else {
                    father_amount_february = parseFloat(father_amount_february);
                }

                var february_amount = el.find('.february').html();
                var february_negative_value = february_amount.match(/\(([^)]+)\)/);

                if (february_negative_value != null) {
                    father_amount_february -= parseFloat(february_negative_value[1]);
                } else {
                    father_amount_february += parseFloat(february_amount);
                }

                $('.'+father_id).find('.february').html(father_amount_february >= 0 ? father_amount_february : '('+Math.abs(father_amount_february)+')');

                var father_id = el.data('fatherid');

                var father_amount_march = $('.'+father_id).find('.march').html();
                var father_amount_march_negative_value = father_amount_march.match(/\(([^)]+)\)/);
                if (father_amount_march_negative_value != null) {
                    father_amount_march = -parseFloat(father_amount_march_negative_value[1]);
                } else {
                    father_amount_march = parseFloat(father_amount_march);
                }

                var march_amount = el.find('.march').html();
                var march_negative_value = march_amount.match(/\(([^)]+)\)/);

                if (march_negative_value != null) {
                    father_amount_march -= parseFloat(march_negative_value[1]);
                } else {
                    father_amount_march += parseFloat(march_amount);
                }

                $('.'+father_id).find('.march').html(father_amount_march >= 0 ? father_amount_march : '('+Math.abs(father_amount_march)+')');

                var father_id = el.data('fatherid');

                var father_amount_april = $('.'+father_id).find('.april').html();
                var father_amount_april_negative_value = father_amount_april.match(/\(([^)]+)\)/);
                if (father_amount_april_negative_value != null) {
                    father_amount_april = -parseFloat(father_amount_april_negative_value[1]);
                } else {
                    father_amount_april = parseFloat(father_amount_april);
                }

                var april_amount = el.find('.april').html();
                var april_negative_value = april_amount.match(/\(([^)]+)\)/);

                if (april_negative_value != null) {
                    father_amount_april -= parseFloat(april_negative_value[1]);
                } else {
                    father_amount_april += parseFloat(april_amount);
                }

                $('.'+father_id).find('.april').html(father_amount_april >= 0 ? father_amount_april : '('+Math.abs(father_amount_april)+')');

                var father_id = el.data('fatherid');

                var father_amount_may = $('.'+father_id).find('.may').html();
                var father_amount_may_negative_value = father_amount_may.match(/\(([^)]+)\)/);
                if (father_amount_may_negative_value != null) {
                    father_amount_may = -parseFloat(father_amount_may_negative_value[1]);
                } else {
                    father_amount_may = parseFloat(father_amount_may);
                }

                var may_amount = el.find('.may').html();
                var may_negative_value = may_amount.match(/\(([^)]+)\)/);

                if (may_negative_value != null) {
                    father_amount_may -= parseFloat(may_negative_value[1]);
                } else {
                    father_amount_may += parseFloat(may_amount);
                }

                $('.'+father_id).find('.may').html(father_amount_may >= 0 ? father_amount_may : '('+Math.abs(father_amount_may)+')');

                var father_id = el.data('fatherid');

                var father_amount_june = $('.'+father_id).find('.june').html();
                var father_amount_june_negative_value = father_amount_june.match(/\(([^)]+)\)/);
                if (father_amount_june_negative_value != null) {
                    father_amount_june = -parseFloat(father_amount_june_negative_value[1]);
                } else {
                    father_amount_june = parseFloat(father_amount_june);
                }

                var june_amount = el.find('.june').html();
                var june_negative_value = june_amount.match(/\(([^)]+)\)/);

                if (june_negative_value != null) {
                    father_amount_june -= parseFloat(june_negative_value[1]);
                } else {
                    father_amount_june += parseFloat(june_amount);
                }

                $('.'+father_id).find('.june').html(father_amount_june >= 0 ? father_amount_june : '('+Math.abs(father_amount_june)+')');

                var father_id = el.data('fatherid');

                var father_amount_july = $('.'+father_id).find('.july').html();
                var father_amount_july_negative_value = father_amount_july.match(/\(([^)]+)\)/);
                if (father_amount_july_negative_value != null) {
                    father_amount_july = -parseFloat(father_amount_july_negative_value[1]);
                } else {
                    father_amount_july = parseFloat(father_amount_july);
                }

                var july_amount = el.find('.july').html();
                var july_negative_value = july_amount.match(/\(([^)]+)\)/);

                if (july_negative_value != null) {
                    father_amount_july -= parseFloat(july_negative_value[1]);
                } else {
                    father_amount_july += parseFloat(july_amount);
                }

                $('.'+father_id).find('.july').html(father_amount_july >= 0 ? father_amount_july : '('+Math.abs(father_amount_july)+')');

                var father_id = el.data('fatherid');

                var father_amount_august = $('.'+father_id).find('.august').html();
                var father_amount_august_negative_value = father_amount_august.match(/\(([^)]+)\)/);
                if (father_amount_august_negative_value != null) {
                    father_amount_august = -parseFloat(father_amount_august_negative_value[1]);
                } else {
                    father_amount_august = parseFloat(father_amount_august);
                }

                var august_amount = el.find('.august').html();
                var august_negative_value = august_amount.match(/\(([^)]+)\)/);

                if (august_negative_value != null) {
                    father_amount_august -= parseFloat(august_negative_value[1]);
                } else {
                    father_amount_august += parseFloat(august_amount);
                }

                $('.'+father_id).find('.august').html(father_amount_august >= 0 ? father_amount_august : '('+Math.abs(father_amount_august)+')');

                var father_id = el.data('fatherid');

                var father_amount_september = $('.'+father_id).find('.september').html();
                var father_amount_september_negative_value = father_amount_september.match(/\(([^)]+)\)/);
                if (father_amount_september_negative_value != null) {
                    father_amount_september = -parseFloat(father_amount_september_negative_value[1]);
                } else {
                    father_amount_september = parseFloat(father_amount_september);
                }

                var september_amount = el.find('.september').html();
                var september_negative_value = september_amount.match(/\(([^)]+)\)/);

                if (september_negative_value != null) {
                    father_amount_september -= parseFloat(september_negative_value[1]);
                } else {
                    father_amount_september += parseFloat(september_amount);
                }

                $('.'+father_id).find('.september').html(father_amount_september >= 0 ? father_amount_september : '('+Math.abs(father_amount_september)+')');

                var father_id = el.data('fatherid');

                var father_amount_october = $('.'+father_id).find('.october').html();
                var father_amount_october_negative_value = father_amount_october.match(/\(([^)]+)\)/);
                if (father_amount_october_negative_value != null) {
                    father_amount_october = -parseFloat(father_amount_october_negative_value[1]);
                } else {
                    father_amount_october = parseFloat(father_amount_october);
                }

                var october_amount = el.find('.october').html();
                var october_negative_value = october_amount.match(/\(([^)]+)\)/);

                if (october_negative_value != null) {
                    father_amount_october -= parseFloat(october_negative_value[1]);
                } else {
                    father_amount_october += parseFloat(october_amount);
                }

                $('.'+father_id).find('.october').html(father_amount_october >= 0 ? father_amount_october : '('+Math.abs(father_amount_october)+')');

                var father_id = el.data('fatherid');

                var father_amount_november = $('.'+father_id).find('.november').html();
                var father_amount_november_negative_value = father_amount_november.match(/\(([^)]+)\)/);
                if (father_amount_november_negative_value != null) {
                    father_amount_november = -parseFloat(father_amount_november_negative_value[1]);
                } else {
                    father_amount_november = parseFloat(father_amount_november);
                }

                var november_amount = el.find('.november').html();
                var november_negative_value = november_amount.match(/\(([^)]+)\)/);

                if (november_negative_value != null) {
                    father_amount_november -= parseFloat(november_negative_value[1]);
                } else {
                    father_amount_november += parseFloat(november_amount);
                }

                $('.'+father_id).find('.november').html(father_amount_november >= 0 ? father_amount_november : '('+Math.abs(father_amount_november)+')');

                var father_id = el.data('fatherid');

                var father_amount_december = $('.'+father_id).find('.december').html();
                var father_amount_december_negative_value = father_amount_december.match(/\(([^)]+)\)/);
                if (father_amount_december_negative_value != null) {
                    father_amount_december = -parseFloat(father_amount_december_negative_value[1]);
                } else {
                    father_amount_december = parseFloat(father_amount_december);
                }

                var december_amount = el.find('.december').html();
                var december_negative_value = december_amount.match(/\(([^)]+)\)/);

                if (december_negative_value != null) {
                    father_amount_december -= parseFloat(december_negative_value[1]);
                } else {
                    father_amount_december += parseFloat(december_amount);
                }

                $('.'+father_id).find('.december').html(father_amount_december >= 0 ? father_amount_december : '('+Math.abs(father_amount_december)+')');
            }

        });

        $('#accounts_table tr').each(function() {
            var el = $(this);

            if (el.data('level') == 3) {
                var father_id = el.data('fatherid');

                var father_amount_january = $('.'+father_id).find('.january').html();
                var father_amount_january_negative_value = father_amount_january.match(/\(([^)]+)\)/);
                if (father_amount_january_negative_value != null) {
                    father_amount_january = -parseFloat(father_amount_january_negative_value[1]);
                } else {
                    father_amount_january = parseFloat(father_amount_january);
                }

                var january_amount = el.find('.january').html();
                var january_negative_value = january_amount.match(/\(([^)]+)\)/);

                if (january_negative_value != null) {
                    father_amount_january -= parseFloat(january_negative_value[1]);
                } else {
                    father_amount_january += parseFloat(january_amount);
                }

                $('.'+father_id).find('.january').html(father_amount_january >= 0 ? father_amount_january : '('+Math.abs(father_amount_january)+')');

                var father_id = el.data('fatherid');

                var father_amount_february = $('.'+father_id).find('.february').html();
                var father_amount_february_negative_value = father_amount_february.match(/\(([^)]+)\)/);
                if (father_amount_february_negative_value != null) {
                    father_amount_february = -parseFloat(father_amount_february_negative_value[1]);
                } else {
                    father_amount_february = parseFloat(father_amount_february);
                }

                var february_amount = el.find('.february').html();
                var february_negative_value = february_amount.match(/\(([^)]+)\)/);

                if (february_negative_value != null) {
                    father_amount_february -= parseFloat(february_negative_value[1]);
                } else {
                    father_amount_february += parseFloat(february_amount);
                }

                $('.'+father_id).find('.february').html(father_amount_february >= 0 ? father_amount_february : '('+Math.abs(father_amount_february)+')');

                var father_id = el.data('fatherid');

                var father_amount_march = $('.'+father_id).find('.march').html();
                var father_amount_march_negative_value = father_amount_march.match(/\(([^)]+)\)/);
                if (father_amount_march_negative_value != null) {
                    father_amount_march = -parseFloat(father_amount_march_negative_value[1]);
                } else {
                    father_amount_march = parseFloat(father_amount_march);
                }

                var march_amount = el.find('.march').html();
                var march_negative_value = march_amount.match(/\(([^)]+)\)/);

                if (march_negative_value != null) {
                    father_amount_march -= parseFloat(march_negative_value[1]);
                } else {
                    father_amount_march += parseFloat(march_amount);
                }

                $('.'+father_id).find('.march').html(father_amount_march >= 0 ? father_amount_march : '('+Math.abs(father_amount_march)+')');

                var father_id = el.data('fatherid');

                var father_amount_april = $('.'+father_id).find('.april').html();
                var father_amount_april_negative_value = father_amount_april.match(/\(([^)]+)\)/);
                if (father_amount_april_negative_value != null) {
                    father_amount_april = -parseFloat(father_amount_april_negative_value[1]);
                } else {
                    father_amount_april = parseFloat(father_amount_april);
                }

                var april_amount = el.find('.april').html();
                var april_negative_value = april_amount.match(/\(([^)]+)\)/);

                if (april_negative_value != null) {
                    father_amount_april -= parseFloat(april_negative_value[1]);
                } else {
                    father_amount_april += parseFloat(april_amount);
                }

                $('.'+father_id).find('.april').html(father_amount_april >= 0 ? father_amount_april : '('+Math.abs(father_amount_april)+')');

                var father_id = el.data('fatherid');

                var father_amount_may = $('.'+father_id).find('.may').html();
                var father_amount_may_negative_value = father_amount_may.match(/\(([^)]+)\)/);
                if (father_amount_may_negative_value != null) {
                    father_amount_may = -parseFloat(father_amount_may_negative_value[1]);
                } else {
                    father_amount_may = parseFloat(father_amount_may);
                }

                var may_amount = el.find('.may').html();
                var may_negative_value = may_amount.match(/\(([^)]+)\)/);

                if (may_negative_value != null) {
                    father_amount_may -= parseFloat(may_negative_value[1]);
                } else {
                    father_amount_may += parseFloat(may_amount);
                }

                $('.'+father_id).find('.may').html(father_amount_may >= 0 ? father_amount_may : '('+Math.abs(father_amount_may)+')');

                var father_id = el.data('fatherid');

                var father_amount_june = $('.'+father_id).find('.june').html();
                var father_amount_june_negative_value = father_amount_june.match(/\(([^)]+)\)/);
                if (father_amount_june_negative_value != null) {
                    father_amount_june = -parseFloat(father_amount_june_negative_value[1]);
                } else {
                    father_amount_june = parseFloat(father_amount_june);
                }

                var june_amount = el.find('.june').html();
                var june_negative_value = june_amount.match(/\(([^)]+)\)/);

                if (june_negative_value != null) {
                    father_amount_june -= parseFloat(june_negative_value[1]);
                } else {
                    father_amount_june += parseFloat(june_amount);
                }

                $('.'+father_id).find('.june').html(father_amount_june >= 0 ? father_amount_june : '('+Math.abs(father_amount_june)+')');

                var father_id = el.data('fatherid');

                var father_amount_july = $('.'+father_id).find('.july').html();
                var father_amount_july_negative_value = father_amount_july.match(/\(([^)]+)\)/);
                if (father_amount_july_negative_value != null) {
                    father_amount_july = -parseFloat(father_amount_july_negative_value[1]);
                } else {
                    father_amount_july = parseFloat(father_amount_july);
                }

                var july_amount = el.find('.july').html();
                var july_negative_value = july_amount.match(/\(([^)]+)\)/);

                if (july_negative_value != null) {
                    father_amount_july -= parseFloat(july_negative_value[1]);
                } else {
                    father_amount_july += parseFloat(july_amount);
                }

                $('.'+father_id).find('.july').html(father_amount_july >= 0 ? father_amount_july : '('+Math.abs(father_amount_july)+')');

                var father_id = el.data('fatherid');

                var father_amount_august = $('.'+father_id).find('.august').html();
                var father_amount_august_negative_value = father_amount_august.match(/\(([^)]+)\)/);
                if (father_amount_august_negative_value != null) {
                    father_amount_august = -parseFloat(father_amount_august_negative_value[1]);
                } else {
                    father_amount_august = parseFloat(father_amount_august);
                }

                var august_amount = el.find('.august').html();
                var august_negative_value = august_amount.match(/\(([^)]+)\)/);

                if (august_negative_value != null) {
                    father_amount_august -= parseFloat(august_negative_value[1]);
                } else {
                    father_amount_august += parseFloat(august_amount);
                }

                $('.'+father_id).find('.august').html(father_amount_august >= 0 ? father_amount_august : '('+Math.abs(father_amount_august)+')');

                var father_id = el.data('fatherid');

                var father_amount_september = $('.'+father_id).find('.september').html();
                var father_amount_september_negative_value = father_amount_september.match(/\(([^)]+)\)/);
                if (father_amount_september_negative_value != null) {
                    father_amount_september = -parseFloat(father_amount_september_negative_value[1]);
                } else {
                    father_amount_september = parseFloat(father_amount_september);
                }

                var september_amount = el.find('.september').html();
                var september_negative_value = september_amount.match(/\(([^)]+)\)/);

                if (september_negative_value != null) {
                    father_amount_september -= parseFloat(september_negative_value[1]);
                } else {
                    father_amount_september += parseFloat(september_amount);
                }

                $('.'+father_id).find('.september').html(father_amount_september >= 0 ? father_amount_september : '('+Math.abs(father_amount_september)+')');

                var father_id = el.data('fatherid');

                var father_amount_october = $('.'+father_id).find('.october').html();
                var father_amount_october_negative_value = father_amount_october.match(/\(([^)]+)\)/);
                if (father_amount_october_negative_value != null) {
                    father_amount_october = -parseFloat(father_amount_october_negative_value[1]);
                } else {
                    father_amount_october = parseFloat(father_amount_october);
                }

                var october_amount = el.find('.october').html();
                var october_negative_value = october_amount.match(/\(([^)]+)\)/);

                if (october_negative_value != null) {
                    father_amount_october -= parseFloat(october_negative_value[1]);
                } else {
                    father_amount_october += parseFloat(october_amount);
                }

                $('.'+father_id).find('.october').html(father_amount_october >= 0 ? father_amount_october : '('+Math.abs(father_amount_october)+')');

                var father_id = el.data('fatherid');

                var father_amount_november = $('.'+father_id).find('.november').html();
                var father_amount_november_negative_value = father_amount_november.match(/\(([^)]+)\)/);
                if (father_amount_november_negative_value != null) {
                    father_amount_november = -parseFloat(father_amount_november_negative_value[1]);
                } else {
                    father_amount_november = parseFloat(father_amount_november);
                }

                var november_amount = el.find('.november').html();
                var november_negative_value = november_amount.match(/\(([^)]+)\)/);

                if (november_negative_value != null) {
                    father_amount_november -= parseFloat(november_negative_value[1]);
                } else {
                    father_amount_november += parseFloat(november_amount);
                }

                $('.'+father_id).find('.november').html(father_amount_november >= 0 ? father_amount_november : '('+Math.abs(father_amount_november)+')');

                var father_id = el.data('fatherid');

                var father_amount_december = $('.'+father_id).find('.december').html();
                var father_amount_december_negative_value = father_amount_december.match(/\(([^)]+)\)/);
                if (father_amount_december_negative_value != null) {
                    father_amount_december = -parseFloat(father_amount_december_negative_value[1]);
                } else {
                    father_amount_december = parseFloat(father_amount_december);
                }

                var december_amount = el.find('.december').html();
                var december_negative_value = december_amount.match(/\(([^)]+)\)/);

                if (december_negative_value != null) {
                    father_amount_december -= parseFloat(december_negative_value[1]);
                } else {
                    father_amount_december += parseFloat(december_amount);
                }

                $('.'+father_id).find('.december').html(father_amount_december >= 0 ? father_amount_december : '('+Math.abs(father_amount_december)+')');
            }

        });

        $('#accounts_table tr').each(function() {
            var el = $(this);

            if (el.data('level') == 2) {
                var father_id = el.data('fatherid');

                var father_amount_january = $('.'+father_id).find('.january').html();
                var father_amount_january_negative_value = father_amount_january.match(/\(([^)]+)\)/);
                if (father_amount_january_negative_value != null) {
                    father_amount_january = -parseFloat(father_amount_january_negative_value[1]);
                } else {
                    father_amount_january = parseFloat(father_amount_january);
                }

                var january_amount = el.find('.january').html();
                var january_negative_value = january_amount.match(/\(([^)]+)\)/);

                if (january_negative_value != null) {
                    father_amount_january -= parseFloat(january_negative_value[1]);
                } else {
                    father_amount_january += parseFloat(january_amount);
                }

                $('.'+father_id).find('.january').html(father_amount_january >= 0 ? father_amount_january : '('+Math.abs(father_amount_january)+')');

                var father_id = el.data('fatherid');

                var father_amount_february = $('.'+father_id).find('.february').html();
                var father_amount_february_negative_value = father_amount_february.match(/\(([^)]+)\)/);
                if (father_amount_february_negative_value != null) {
                    father_amount_february = -parseFloat(father_amount_february_negative_value[1]);
                } else {
                    father_amount_february = parseFloat(father_amount_february);
                }

                var february_amount = el.find('.february').html();
                var february_negative_value = february_amount.match(/\(([^)]+)\)/);

                if (february_negative_value != null) {
                    father_amount_february -= parseFloat(february_negative_value[1]);
                } else {
                    father_amount_february += parseFloat(february_amount);
                }

                $('.'+father_id).find('.february').html(father_amount_february >= 0 ? father_amount_february : '('+Math.abs(father_amount_february)+')');

                var father_id = el.data('fatherid');

                var father_amount_march = $('.'+father_id).find('.march').html();
                var father_amount_march_negative_value = father_amount_march.match(/\(([^)]+)\)/);
                if (father_amount_march_negative_value != null) {
                    father_amount_march = -parseFloat(father_amount_march_negative_value[1]);
                } else {
                    father_amount_march = parseFloat(father_amount_march);
                }

                var march_amount = el.find('.march').html();
                var march_negative_value = march_amount.match(/\(([^)]+)\)/);

                if (march_negative_value != null) {
                    father_amount_march -= parseFloat(march_negative_value[1]);
                } else {
                    father_amount_march += parseFloat(march_amount);
                }

                $('.'+father_id).find('.march').html(father_amount_march >= 0 ? father_amount_march : '('+Math.abs(father_amount_march)+')');

                var father_id = el.data('fatherid');

                var father_amount_april = $('.'+father_id).find('.april').html();
                var father_amount_april_negative_value = father_amount_april.match(/\(([^)]+)\)/);
                if (father_amount_april_negative_value != null) {
                    father_amount_april = -parseFloat(father_amount_april_negative_value[1]);
                } else {
                    father_amount_april = parseFloat(father_amount_april);
                }

                var april_amount = el.find('.april').html();
                var april_negative_value = april_amount.match(/\(([^)]+)\)/);

                if (april_negative_value != null) {
                    father_amount_april -= parseFloat(april_negative_value[1]);
                } else {
                    father_amount_april += parseFloat(april_amount);
                }

                $('.'+father_id).find('.april').html(father_amount_april >= 0 ? father_amount_april : '('+Math.abs(father_amount_april)+')');

                var father_id = el.data('fatherid');

                var father_amount_may = $('.'+father_id).find('.may').html();
                var father_amount_may_negative_value = father_amount_may.match(/\(([^)]+)\)/);
                if (father_amount_may_negative_value != null) {
                    father_amount_may = -parseFloat(father_amount_may_negative_value[1]);
                } else {
                    father_amount_may = parseFloat(father_amount_may);
                }

                var may_amount = el.find('.may').html();
                var may_negative_value = may_amount.match(/\(([^)]+)\)/);

                if (may_negative_value != null) {
                    father_amount_may -= parseFloat(may_negative_value[1]);
                } else {
                    father_amount_may += parseFloat(may_amount);
                }

                $('.'+father_id).find('.may').html(father_amount_may >= 0 ? father_amount_may : '('+Math.abs(father_amount_may)+')');

                var father_id = el.data('fatherid');

                var father_amount_june = $('.'+father_id).find('.june').html();
                var father_amount_june_negative_value = father_amount_june.match(/\(([^)]+)\)/);
                if (father_amount_june_negative_value != null) {
                    father_amount_june = -parseFloat(father_amount_june_negative_value[1]);
                } else {
                    father_amount_june = parseFloat(father_amount_june);
                }

                var june_amount = el.find('.june').html();
                var june_negative_value = june_amount.match(/\(([^)]+)\)/);

                if (june_negative_value != null) {
                    father_amount_june -= parseFloat(june_negative_value[1]);
                } else {
                    father_amount_june += parseFloat(june_amount);
                }

                $('.'+father_id).find('.june').html(father_amount_june >= 0 ? father_amount_june : '('+Math.abs(father_amount_june)+')');

                var father_id = el.data('fatherid');

                var father_amount_july = $('.'+father_id).find('.july').html();
                var father_amount_july_negative_value = father_amount_july.match(/\(([^)]+)\)/);
                if (father_amount_july_negative_value != null) {
                    father_amount_july = -parseFloat(father_amount_july_negative_value[1]);
                } else {
                    father_amount_july = parseFloat(father_amount_july);
                }

                var july_amount = el.find('.july').html();
                var july_negative_value = july_amount.match(/\(([^)]+)\)/);

                if (july_negative_value != null) {
                    father_amount_july -= parseFloat(july_negative_value[1]);
                } else {
                    father_amount_july += parseFloat(july_amount);
                }

                $('.'+father_id).find('.july').html(father_amount_july >= 0 ? father_amount_july : '('+Math.abs(father_amount_july)+')');

                var father_id = el.data('fatherid');

                var father_amount_august = $('.'+father_id).find('.august').html();
                var father_amount_august_negative_value = father_amount_august.match(/\(([^)]+)\)/);
                if (father_amount_august_negative_value != null) {
                    father_amount_august = -parseFloat(father_amount_august_negative_value[1]);
                } else {
                    father_amount_august = parseFloat(father_amount_august);
                }

                var august_amount = el.find('.august').html();
                var august_negative_value = august_amount.match(/\(([^)]+)\)/);

                if (august_negative_value != null) {
                    father_amount_august -= parseFloat(august_negative_value[1]);
                } else {
                    father_amount_august += parseFloat(august_amount);
                }

                $('.'+father_id).find('.august').html(father_amount_august >= 0 ? father_amount_august : '('+Math.abs(father_amount_august)+')');

                var father_id = el.data('fatherid');

                var father_amount_september = $('.'+father_id).find('.september').html();
                var father_amount_september_negative_value = father_amount_september.match(/\(([^)]+)\)/);
                if (father_amount_september_negative_value != null) {
                    father_amount_september = -parseFloat(father_amount_september_negative_value[1]);
                } else {
                    father_amount_september = parseFloat(father_amount_september);
                }

                var september_amount = el.find('.september').html();
                var september_negative_value = september_amount.match(/\(([^)]+)\)/);

                if (september_negative_value != null) {
                    father_amount_september -= parseFloat(september_negative_value[1]);
                } else {
                    father_amount_september += parseFloat(september_amount);
                }

                $('.'+father_id).find('.september').html(father_amount_september >= 0 ? father_amount_september : '('+Math.abs(father_amount_september)+')');

                var father_id = el.data('fatherid');

                var father_amount_october = $('.'+father_id).find('.october').html();
                var father_amount_october_negative_value = father_amount_october.match(/\(([^)]+)\)/);
                if (father_amount_october_negative_value != null) {
                    father_amount_october = -parseFloat(father_amount_october_negative_value[1]);
                } else {
                    father_amount_october = parseFloat(father_amount_october);
                }

                var october_amount = el.find('.october').html();
                var october_negative_value = october_amount.match(/\(([^)]+)\)/);

                if (october_negative_value != null) {
                    father_amount_october -= parseFloat(october_negative_value[1]);
                } else {
                    father_amount_october += parseFloat(october_amount);
                }

                $('.'+father_id).find('.october').html(father_amount_october >= 0 ? father_amount_october : '('+Math.abs(father_amount_october)+')');

                var father_id = el.data('fatherid');

                var father_amount_november = $('.'+father_id).find('.november').html();
                var father_amount_november_negative_value = father_amount_november.match(/\(([^)]+)\)/);
                if (father_amount_november_negative_value != null) {
                    father_amount_november = -parseFloat(father_amount_november_negative_value[1]);
                } else {
                    father_amount_november = parseFloat(father_amount_november);
                }

                var november_amount = el.find('.november').html();
                var november_negative_value = november_amount.match(/\(([^)]+)\)/);

                if (november_negative_value != null) {
                    father_amount_november -= parseFloat(november_negative_value[1]);
                } else {
                    father_amount_november += parseFloat(november_amount);
                }

                $('.'+father_id).find('.november').html(father_amount_november >= 0 ? father_amount_november : '('+Math.abs(father_amount_november)+')');

                var father_id = el.data('fatherid');

                var father_amount_december = $('.'+father_id).find('.december').html();
                var father_amount_december_negative_value = father_amount_december.match(/\(([^)]+)\)/);
                if (father_amount_december_negative_value != null) {
                    father_amount_december = -parseFloat(father_amount_december_negative_value[1]);
                } else {
                    father_amount_december = parseFloat(father_amount_december);
                }

                var december_amount = el.find('.december').html();
                var december_negative_value = december_amount.match(/\(([^)]+)\)/);

                if (december_negative_value != null) {
                    father_amount_december -= parseFloat(december_negative_value[1]);
                } else {
                    father_amount_december += parseFloat(december_amount);
                }

                $('.'+father_id).find('.december').html(father_amount_december >= 0 ? father_amount_december : '('+Math.abs(father_amount_december)+')');
            }

        });



        $('.account_item').each(function() {
            var el = $(this);

            if (parseFloat(el.find('.january').html()) == 0
                && parseFloat(el.find('.february').html()) == 0
                && parseFloat(el.find('.march').html()) == 0
                && parseFloat(el.find('.april').html()) == 0
                && parseFloat(el.find('.may').html()) == 0
                && parseFloat(el.find('.june').html()) == 0
                && parseFloat(el.find('.july').html()) == 0
                && parseFloat(el.find('.august').html()) == 0
                && parseFloat(el.find('.september').html()) == 0
                && parseFloat(el.find('.october').html()) == 0
                && parseFloat(el.find('.november').html()) == 0
                && parseFloat(el.find('.december').html()) == 0
                ) {

                el.hide();
            }
        });

        $('.january, .february, .march, .april, .may, .june, .july, .august, .september, .october, .november, .december').each(function() {
            var el = $(this);

            var html_amount = el.html();
            var check_rackets = html_amount.match(/\(([^)]+)\)/);

            if (check_rackets != null) {

                var round_amount = numberWithCommas(parseFloat(check_rackets[1]).toFixed(2));

                el.html('('+round_amount+')');
            } else {
                el.html(numberWithCommas(parseFloat(html_amount).toFixed(2)));
            }
        });

        function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }

    });

    $(document).ready(function() {
        $("#download_excel_file").click(function(e) {
            e.preventDefault();

            //getting data from our table
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('table_wrapper');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');

            var a = document.createElement('a');
            a.href = data_type + ', ' + table_html;
            a.download = 'BS_monthly_report.xls';
            a.click();
        });

        $('body').mouseover(function () {
            $("#download_excel_file").show();
            $("#download_pdf_file").show();
        });

        $("#download_pdf_file").click(function () {
            $("#download_excel_file").hide();
            $("#download_pdf_file").hide();
            window.print();
        });
    });
</script>