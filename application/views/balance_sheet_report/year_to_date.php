<style>
    table { border-collapse: collapse; }
    tr.border-top { border-top: solid thin; }
    tr.border-bottom { border-bottom: solid thin; }
    tr.bold { font-weight: bold; }
    * { font-size: 12px; font-family: Lucida Sans Unicode; }
</style>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/slider-pips/jquery-ui.js"></script>
<button type="button" id="download_excel_file">Download Excel Report</button>
<button type="button" id="download_pdf_file">Print or Download PDF Report</button>
<div id="table_wrapper">
    <table style='width:100%;'>
        <tr>
            <td style='font-size:20px;text-align:center;color:blue;'><?php echo $company_name; ?></td>
        </tr>
        <tr>
            <td style='font-size:16px;text-align:center;color:blue;'>Outlets List</td>
        </tr>
        <tr>
            <td style='font-size:16px;text-align:center;color:blue;'>
                <?php foreach($outlets AS $item) {
                    echo $item.'<br/>';
                } ?>
            </td>
        </tr>
        <tr>
            <td style='text-align:center;font-size:18px;'>Balance Sheet Statement</td>
        </tr>
    </table>
    <table style='width:100%;'>
        <thead>
            <tr class='border-top'>
                <th colspan="2" style='width:20%;  text-align:center;'>Accounts</th>
                <th style='width:7%;  text-align:center;'><?php echo $starting_month; ?> - <?php echo $ending_month; ?></th>
                <th style='width:7%; text-align:center;'><?php echo $starting_month_1year_ago; ?> - <?php echo $ending_month_1year_ago; ?></th>
                <th style='width:7%;  text-align:center;'>Difference %</th>
            </tr>
        </thead>
        <tbody id="accounts_table">
            <?php foreach($accounts AS $item) {

                if ($item['code'] == 20000) {
                    ?>
                    <tr id="current_asset" style="font-weight: bold;">
                        <td></td>
                        <td>2, TOTAL CURRENT ASSETS</td>
                        <td style="text-align: center;" class="selected_period"></td>
                        <td style="text-align: center;" class="year_to_date"></td>
                        <td style="text-align: center;" class="percentage_difference"></td>
                    </tr>
                    <tr id="asset" style="font-weight: bold;">
                        <td></td>
                        <td>3, TOTAL ASSETS (1+2)</td>
                        <td style="text-align: center;" class="selected_period"></td>
                        <td style="text-align: center;" class="year_to_date"></td>
                        <td style="text-align: center;" class="percentage_difference"></td>
                    </tr>
                    <?php
                }

                if ($item['code'] == 30000) {
                    ?>
                    <tr id="non_current_liability" style="font-weight: bold;">
                        <td></td>
                        <td>5, TOTAL NON-CURRENT LIABILITY</td>
                        <td style="text-align: center;" class="selected_period"></td>
                        <td style="text-align: center;" class="year_to_date"></td>
                        <td style="text-align: center;" class="percentage_difference"></td>
                    </tr>
                    <tr id="liability" style="font-weight: bold;">
                        <td></td>
                        <td>6, TOTAL LIABILITY (4+5)</td>
                        <td style="text-align: center;" class="selected_period"></td>
                        <td style="text-align: center;" class="year_to_date"></td>
                        <td style="text-align: center;" class="percentage_difference"></td>
                    </tr>
                    <?php
                }

                if ($item['code'] == 12000) {
                    ?>
                    <tr id="non_current_asset" style="font-weight: bold;">
                        <td></td>
                        <td>1, TOTAL NON-CURRENT ASSET</td>
                        <td style="text-align: center;" class="selected_period"></td>
                        <td style="text-align: center;" class="year_to_date"></td>
                        <td style="text-align: center;" class="percentage_difference"></td>
                    </tr>
                    <?php
                }

                if ($item['code'] == 22000) {
                    ?>
                    <tr id="current_liability" style="font-weight: bold;">
                        <td></td>
                        <td>4, TOTAL CURRENT LIABILITY</td>
                        <td style="text-align: center;" class="selected_period"></td>
                        <td style="text-align: center;" class="year_to_date"></td>
                        <td style="text-align: center;" class="percentage_difference"></td>
                    </tr>
                    <?php
                }

                $padding = $item['level']*15;

                $selected_period = 0;

                foreach ($item['selected_period'] AS $selected) {
                    if ($item['type'] == 'asset') {

                        if ($selected['type'] == 'debit') {
                            $selected_period += $selected['amount'];
                        } else {
                            $selected_period -= $selected['amount'];
                        }
                    } else {

                        if ($selected['type'] == 'credit') {
                            $selected_period += $selected['amount'];
                        } else {
                            $selected_period -= $selected['amount'];
                        }

                    }
                }

                $year_to_date = 0;
                foreach ($item['year_to_date'] AS $to_date) {
                    if ($item['type'] == 'asset') {

                        if ($to_date['type'] == 'debit') {
                            $year_to_date += $to_date['amount'];
                        } else {
                            $year_to_date -= $to_date['amount'];
                        }
                    } else {

                        if ($to_date['type'] == 'credit') {
                            $year_to_date += $to_date['amount'];
                        } else {
                            $year_to_date -= $to_date['amount'];
                        }

                    }
                }
            $class_item = '';
            if (!isset($item['children'])) {
                $class_item = $item['type'];

                if ($class_item == 'asset') {
                    $class_item = $item['code'] >= 12000 ? 'current_asset' : 'non_current_asset';
                }

                if ($class_item == 'liability') {
                    $class_item = $item['code'] < 22000 ? 'current_liability' : 'non_current_liability';
                }
            }

                ?>
            <tr <?php if (($report_type == 'hidden' && ($item['level'] == 4 || $item['level'] == 5)) || ($report_type == 'hidden' && $item['level'] == 3 && $item['code'] >= 30000)) { ?> style="display:none;" <?php } ?> class="<?php echo $class_item; ?> <?php echo $item['id']; ?> account_item" data-level="<?php echo $item['level']; ?>" data-fatherid="<?php echo $item['father_id']; ?>">
                <td style="padding-left: <?php echo $padding; ?>px;"><?php echo $item['code']; ?></td>
                <td style="padding-left: <?php echo $padding; ?>px;"><?php echo $item['name']; ?></td>
                <td class="selected_period" style='text-align:center;'><?php echo $selected_period >= 0 ? $selected_period : '('.abs($selected_period).')'; ?></td>
                <td class="year_to_date" style='text-align:center;'><?php echo $year_to_date >= 0 ? $year_to_date : '('.abs($year_to_date).')'; ?></td>
                <td class="percentage_difference" style='text-align:center;'></td>
            </tr>
            <?php } ?>
            <tr id="equity" style="font-weight: bold;">
                <td></td>
                <td>7, TOTAL EQUITY</td>
                <td style="text-align: center;" class="selected_period"></td>
                <td style="text-align: center;" class="year_to_date"></td>
                <td style="text-align: center;" class="percentage_difference"></td>
            </tr>
            <tr id="equity_and_liability" style="font-weight: bold;">
                <td></td>
                <td>8, TOTAL EQUITY AND LIABILITY (6+7)</td>
                <td style="text-align: center;" class="selected_period"></td>
                <td style="text-align: center;" class="year_to_date"></td>
                <td style="text-align: center;" class="percentage_difference"></td>
            </tr>
        </tbody>
    </table>
</div>
<script>

    jQuery(document).ready(function () {
        var total_selected_period_non_current_asset = 0;
        var total_year_to_date_non_current_asset = 0;

        $('#accounts_table .non_current_asset').each(function() {
            var el = $(this);

            var selected_amount = el.find('.selected_period').html();
            var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

            var year_to_date_amount = el.find('.year_to_date').html();
            var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

            if (selected_negative_value != null) {
                total_selected_period_non_current_asset -= parseFloat(selected_negative_value[1]);
            } else {
                total_selected_period_non_current_asset += parseFloat(selected_amount);
            }

            if (year_to_date_negative_value != null) {
                total_year_to_date_non_current_asset -= parseFloat(year_to_date_negative_value[1]);
            } else {
                total_year_to_date_non_current_asset += parseFloat(year_to_date_amount);
            }

        });
        $('#non_current_asset .selected_period').html(total_selected_period_non_current_asset >= 0 ? total_selected_period_non_current_asset : '('+Math.abs(total_selected_period_non_current_asset)+')');
        $('#non_current_asset .year_to_date').html(total_year_to_date_non_current_asset >= 0 ? total_year_to_date_non_current_asset : '('+Math.abs(total_year_to_date_non_current_asset)+')');

        // Current asset
        var total_selected_period_current_asset = 0;
        var total_year_to_date_current_asset = 0;

        $('#accounts_table .current_asset').each(function() {
            var el = $(this);

            var selected_amount = el.find('.selected_period').html();
            var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

            var year_to_date_amount = el.find('.year_to_date').html();
            var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

            if (selected_negative_value != null) {
                total_selected_period_current_asset -= parseFloat(selected_negative_value[1]);
            } else {
                total_selected_period_current_asset += parseFloat(selected_amount);
            }

            if (year_to_date_negative_value != null) {
                total_year_to_date_current_asset -= parseFloat(year_to_date_negative_value[1]);
            } else {
                total_year_to_date_current_asset += parseFloat(year_to_date_amount);
            }

        });
        var selected_period_asset = total_selected_period_non_current_asset + total_selected_period_current_asset;
        var year_to_date_asset = total_year_to_date_non_current_asset + total_year_to_date_current_asset;

        $('#current_asset .selected_period').html(total_selected_period_current_asset >= 0 ? total_selected_period_current_asset : '('+Math.abs(total_selected_period_current_asset)+')');
        $('#current_asset .year_to_date').html(total_year_to_date_current_asset >= 0 ? total_year_to_date_current_asset : '('+Math.abs(total_year_to_date_current_asset)+')');

        $('#asset .selected_period').html(selected_period_asset >= 0 ? selected_period_asset : '('+Math.abs(selected_period_asset)+')');
        $('#asset .year_to_date').html(year_to_date_asset >= 0 ? year_to_date_asset : '('+Math.abs(year_to_date_asset)+')');

        // Current liability
        var total_selected_period_current_liability = 0;
        var total_year_to_date_current_liability = 0;

        $('#accounts_table .current_liability').each(function() {
            var el = $(this);

            var selected_amount = el.find('.selected_period').html();
            var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

            var year_to_date_amount = el.find('.year_to_date').html();
            var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

            if (selected_negative_value != null) {
                total_selected_period_current_liability -= parseFloat(selected_negative_value[1]);
            } else {
                total_selected_period_current_liability += parseFloat(selected_amount);
            }

            if (year_to_date_negative_value != null) {
                total_year_to_date_current_liability -= parseFloat(year_to_date_negative_value[1]);
            } else {
                total_year_to_date_current_liability += parseFloat(year_to_date_amount);
            }

        });

        $('#current_liability .selected_period').html(total_selected_period_current_liability >= 0 ? total_selected_period_current_liability : '('+Math.abs(total_selected_period_current_liability)+')');
        $('#current_liability .year_to_date').html(total_year_to_date_current_liability >= 0 ? total_year_to_date_current_liability : '('+Math.abs(total_year_to_date_current_liability)+')');

        // Non-current liability
        var total_selected_period_non_current_liability = 0;
        var total_year_to_date_non_current_liability = 0;

        $('#accounts_table .non_current_liability').each(function() {
            var el = $(this);

            var selected_amount = el.find('.selected_period').html();
            var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

            var year_to_date_amount = el.find('.year_to_date').html();
            var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

            if (selected_negative_value != null) {
                total_selected_period_non_current_liability -= parseFloat(selected_negative_value[1]);
            } else {
                total_selected_period_non_current_liability += parseFloat(selected_amount);
            }

            if (year_to_date_negative_value != null) {
                total_year_to_date_non_current_liability -= parseFloat(year_to_date_negative_value[1]);
            } else {
                total_year_to_date_non_current_liability += parseFloat(year_to_date_amount);
            }

        });
        var selected_period_liability = total_selected_period_non_current_liability + total_selected_period_current_liability;
        var year_to_date_liability = total_year_to_date_non_current_liability + total_year_to_date_current_liability;

        $('#non_current_liability .selected_period').html(total_selected_period_non_current_liability >= 0 ? total_selected_period_non_current_liability : '('+Math.abs(total_selected_period_non_current_liability)+')');
        $('#non_current_liability .year_to_date').html(total_year_to_date_non_current_liability >= 0 ? total_year_to_date_non_current_liability : '('+Math.abs(total_year_to_date_non_current_liability)+')');

        $('#liability .selected_period').html(selected_period_liability >= 0 ? selected_period_liability : '('+Math.abs(selected_period_liability)+')');
        $('#liability .year_to_date').html(year_to_date_liability >= 0 ? year_to_date_liability : '('+Math.abs(year_to_date_liability)+')');


        // Equity
        var total_selected_period_equity = 0;
        var total_year_to_date_equity = 0;

        $('#accounts_table .equity').each(function() {
            var el = $(this);

            var selected_amount = el.find('.selected_period').html();
            var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

            var year_to_date_amount = el.find('.year_to_date').html();
            var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

            if (selected_negative_value != null) {
                total_selected_period_equity -= parseFloat(selected_negative_value[1]);
            } else {
                total_selected_period_equity += parseFloat(selected_amount);
            }

            if (year_to_date_negative_value != null) {
                total_year_to_date_equity -= parseFloat(year_to_date_negative_value[1]);
            } else {
                total_year_to_date_equity += parseFloat(year_to_date_amount);
            }

        });
        var selected_period_equity_and_liability = total_selected_period_equity + selected_period_liability;
        var year_to_date_equity_and_liability = total_year_to_date_equity + year_to_date_liability;

        $('#equity .selected_period').html(total_selected_period_equity >= 0 ? total_selected_period_equity : '('+Math.abs(total_selected_period_equity)+')');
        $('#equity .year_to_date').html(total_year_to_date_equity >= 0 ? total_year_to_date_equity : '('+Math.abs(total_year_to_date_equity)+')');

        $('#equity_and_liability .selected_period').html(selected_period_equity_and_liability >= 0 ? selected_period_equity_and_liability : '('+Math.abs(selected_period_equity_and_liability)+')');
        $('#equity_and_liability .year_to_date').html(year_to_date_equity_and_liability >= 0 ? year_to_date_equity_and_liability : '('+Math.abs(year_to_date_equity_and_liability)+')');

        $('#accounts_table tr').each(function() {
            var el = $(this);

            if (el.data('level') == 5) {
                var father_id = el.data('fatherid');

                var father_amount_selected = $('.'+father_id).find('.selected_period').html();
                var father_amount_selected_negative_value = father_amount_selected.match(/\(([^)]+)\)/);
                if (father_amount_selected_negative_value != null) {
                    father_amount_selected = -parseFloat(father_amount_selected_negative_value[1]);
                } else {
                    father_amount_selected = parseFloat(father_amount_selected);
                }

                var father_amount_year_to_date = $('.'+father_id).find('.year_to_date').html();
                var father_amount_year_to_date_negative_value = father_amount_year_to_date.match(/\(([^)]+)\)/);
                if (father_amount_year_to_date_negative_value != null) {
                    father_amount_year_to_date = -parseFloat(father_amount_year_to_date_negative_value[1]);
                } else {
                    father_amount_year_to_date = parseFloat(father_amount_year_to_date);
                }

                var selected_amount = el.find('.selected_period').html();
                var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

                var year_to_date_amount = el.find('.year_to_date').html();
                var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

                if (selected_negative_value != null) {
                    father_amount_selected -= parseFloat(selected_negative_value[1]);
                } else {
                    father_amount_selected += parseFloat(selected_amount);
                }

                if (year_to_date_negative_value != null) {
                    father_amount_year_to_date -= parseFloat(year_to_date_negative_value[1]);
                } else {
                    father_amount_year_to_date += parseFloat(year_to_date_amount);
                }

                $('.'+father_id).find('.selected_period').html(father_amount_selected >= 0 ? father_amount_selected : '('+Math.abs(father_amount_selected)+')');
                $('.'+father_id).find('.year_to_date').html(father_amount_year_to_date >= 0 ? father_amount_year_to_date : '('+Math.abs(father_amount_year_to_date)+')');
            }

        });

        $('#accounts_table tr').each(function() {
            var el = $(this);

            if (el.data('level') == 4) {
                var father_id = el.data('fatherid');

                var father_amount_selected = $('.'+father_id).find('.selected_period').html();
                var father_amount_selected_negative_value = father_amount_selected.match(/\(([^)]+)\)/);
                if (father_amount_selected_negative_value != null) {
                    father_amount_selected = -parseFloat(father_amount_selected_negative_value[1]);
                } else {
                    father_amount_selected = parseFloat(father_amount_selected);
                }

                var father_amount_year_to_date = $('.'+father_id).find('.year_to_date').html();
                var father_amount_year_to_date_negative_value = father_amount_year_to_date.match(/\(([^)]+)\)/);
                if (father_amount_year_to_date_negative_value != null) {
                    father_amount_year_to_date = -parseFloat(father_amount_year_to_date_negative_value[1]);
                } else {
                    father_amount_year_to_date = parseFloat(father_amount_year_to_date);
                }

                var selected_amount = el.find('.selected_period').html();
                var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

                var year_to_date_amount = el.find('.year_to_date').html();
                var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

                if (selected_negative_value != null) {
                    father_amount_selected -= parseFloat(selected_negative_value[1]);
                } else {
                    father_amount_selected += parseFloat(selected_amount);
                }

                if (year_to_date_negative_value != null) {
                    father_amount_year_to_date -= parseFloat(year_to_date_negative_value[1]);
                } else {
                    father_amount_year_to_date += parseFloat(year_to_date_amount);
                }

                $('.'+father_id).find('.selected_period').html(father_amount_selected >= 0 ? father_amount_selected : '('+Math.abs(father_amount_selected)+')');
                $('.'+father_id).find('.year_to_date').html(father_amount_year_to_date >= 0 ? father_amount_year_to_date : '('+Math.abs(father_amount_year_to_date)+')');
            }

        });

        $('#accounts_table tr').each(function() {
            var el = $(this);

            if (el.data('level') == 3) {
                var father_id = el.data('fatherid');

                var father_amount_selected = $('.'+father_id).find('.selected_period').html();
                var father_amount_selected_negative_value = father_amount_selected.match(/\(([^)]+)\)/);
                if (father_amount_selected_negative_value != null) {
                    father_amount_selected = -parseFloat(father_amount_selected_negative_value[1]);
                } else {
                    father_amount_selected = parseFloat(father_amount_selected);
                }

                var father_amount_year_to_date = $('.'+father_id).find('.year_to_date').html();
                var father_amount_year_to_date_negative_value = father_amount_year_to_date.match(/\(([^)]+)\)/);
                if (father_amount_year_to_date_negative_value != null) {
                    father_amount_year_to_date = -parseFloat(father_amount_year_to_date_negative_value[1]);
                } else {
                    father_amount_year_to_date = parseFloat(father_amount_year_to_date);
                }

                var selected_amount = el.find('.selected_period').html();
                var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

                var year_to_date_amount = el.find('.year_to_date').html();
                var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

                if (selected_negative_value != null) {
                    father_amount_selected -= parseFloat(selected_negative_value[1]);
                } else {
                    father_amount_selected += parseFloat(selected_amount);
                }

                if (year_to_date_negative_value != null) {
                    father_amount_year_to_date -= parseFloat(year_to_date_negative_value[1]);
                } else {
                    father_amount_year_to_date += parseFloat(year_to_date_amount);
                }

                $('.'+father_id).find('.selected_period').html(father_amount_selected >= 0 ? father_amount_selected : '('+Math.abs(father_amount_selected)+')');
                $('.'+father_id).find('.year_to_date').html(father_amount_year_to_date >= 0 ? father_amount_year_to_date : '('+Math.abs(father_amount_year_to_date)+')');
            }

        });

        $('#accounts_table tr').each(function() {
            var el = $(this);

            if (el.data('level') == 2) {
                var father_id = el.data('fatherid');

                var father_amount_selected = $('.'+father_id).find('.selected_period').html();
                var father_amount_selected_negative_value = father_amount_selected.match(/\(([^)]+)\)/);
                if (father_amount_selected_negative_value != null) {
                    father_amount_selected = -parseFloat(father_amount_selected_negative_value[1]);
                } else {
                    father_amount_selected = parseFloat(father_amount_selected);
                }

                var father_amount_year_to_date = $('.'+father_id).find('.year_to_date').html();
                var father_amount_year_to_date_negative_value = father_amount_year_to_date.match(/\(([^)]+)\)/);
                if (father_amount_year_to_date_negative_value != null) {
                    father_amount_year_to_date = -parseFloat(father_amount_year_to_date_negative_value[1]);
                } else {
                    father_amount_year_to_date = parseFloat(father_amount_year_to_date);
                }

                var selected_amount = el.find('.selected_period').html();
                var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

                var year_to_date_amount = el.find('.year_to_date').html();
                var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

                if (selected_negative_value != null) {
                    father_amount_selected -= parseFloat(selected_negative_value[1]);
                } else {
                    father_amount_selected += parseFloat(selected_amount);
                }

                if (year_to_date_negative_value != null) {
                    father_amount_year_to_date -= parseFloat(year_to_date_negative_value[1]);
                } else {
                    father_amount_year_to_date += parseFloat(year_to_date_amount);
                }

                $('.'+father_id).find('.selected_period').html(father_amount_selected >= 0 ? father_amount_selected : '('+Math.abs(father_amount_selected)+')');
                $('.'+father_id).find('.year_to_date').html(father_amount_year_to_date >= 0 ? father_amount_year_to_date : '('+Math.abs(father_amount_year_to_date)+')');
            }

        });

        $('.selected_period').each(function() {
            var el = $(this);

            var html_amount = el.html();

            var year_to_date = el.closest('tr').find('.year_to_date').html();

            if (parseFloat(html_amount) == 0 && parseFloat(year_to_date) == 0) {

                el.closest('.account_item').hide();
            }
        });

        // Update percentage
        $('#accounts_table .percentage_difference').each(function() {
            var el = $(this);

            var selected_amount = el.closest('tr').find('.selected_period').html();
            var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

            var year_to_date_amount = el.closest('tr').find('.year_to_date').html();
            var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

            if (selected_negative_value != null) {
                selected_amount = -parseFloat(selected_negative_value[1]);
            }

            if (year_to_date_negative_value != null) {
                year_to_date_amount = -parseFloat(year_to_date_negative_value[1]);
            }

            var percentage_diff = selected_amount/year_to_date_amount*100;

            el.find('.percentage_difference').html(percentage_diff >= 0 ? percentage_diff : '('+Math.abs(percentage_diff)+')');
        });

        $('.selected_period, .year_to_date, .percentage_difference').each(function() {
            var el = $(this);

            var html_amount = el.html();
            var check_rackets = html_amount.match(/\(([^)]+)\)/);

            if (check_rackets != null) {

                var round_amount = numberWithCommas(parseFloat(check_rackets[1]).toFixed(2));

                el.html('('+round_amount+')');
            } else {
                el.html(numberWithCommas(parseFloat(html_amount).toFixed(2)));
            }
        });

        function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }
    });

    $(document).ready(function() {
        $("#download_excel_file").click(function(e) {
            e.preventDefault();

            //getting data from our table
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('table_wrapper');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');

            var a = document.createElement('a');
            a.href = data_type + ', ' + table_html;
            a.download = 'BS_year_to_date_report.xls';
            a.click();
        });

        $('body').mouseover(function () {
            $("#download_excel_file").show();
            $("#download_pdf_file").show();
        });

        $("#download_pdf_file").click(function () {
            $("#download_excel_file").hide();
            $("#download_pdf_file").hide();
            window.print();
        });
    });
</script>