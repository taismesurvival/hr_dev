<!DOCTYPE html>
<html>
<head>
    <title>Primary Site Fixed</title>
    <script src="//cdn.jsdelivr.net/g/jquery@1.11.0"></script>
    <script src="//cdn.jsdelivr.net/jquery.cookie/1.4.0/jquery.cookie.min.js"></script>
</head>
<body>

<script>
    var is_safari = navigator.userAgent.indexOf("Safari") > -1; // safari detection

    if ((/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) || is_safari) {
        if (!$.cookie('fixed')) {
            $.cookie('fixed', 'fixed', { expires: 365, path: '/' }); // 1 year
            window.location.replace("https://accounts.smesurvival.com/system/_safari_fix.html");
        }
    }
</script>
<iframe src="https://accounts.smesurvival.com/system/balance_sheet_report/monthly_view?token=<?php echo $token; ?>" width="100%" height="1000" frameborder="2"></iframe>

</body>
</html>
