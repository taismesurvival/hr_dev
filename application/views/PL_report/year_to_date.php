<style>
    table { border-collapse: collapse; }
    tr.border-top { border-top: solid thin; }
    tr.border-bottom { border-bottom: solid thin; }
    tr.bold { font-weight: bold; }
    * { font-size: 12px; font-family: Lucida Sans Unicode; }
</style>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/slider-pips/jquery-ui.js"></script>
<button type="button" id="download_excel_file">Download Excel Report</button>
<button type="button" id="download_pdf_file">Print or Download PDF Report</button>
<div id="table_wrapper">
    <table style='width:100%;'>

        <tr>
            <td style='font-size:20px;text-align:center;color:blue;'><?php echo $company_name; ?></td>
        </tr>
        <tr>
            <td style='font-size:16px;text-align:center;color:blue;'>Outlets List</td>
        </tr>
        <tr>
            <td style='font-size:16px;text-align:center;color:blue;'>
                <?php foreach($outlets AS $item) {
                    echo $item.'<br/>';
                } ?>
            </td>
        </tr>
        <tr>
            <td style='text-align:center;font-size:18px;'>Profit/Loss Statement</td>
        </tr>
    </table>
    <table style='width:100%;'>
        <thead>
            <tr class='border-top'>
                <th colspan="2" style='width:20%;  text-align:center;'>Accounts</th>
                <th style='width:7%;  text-align:center;'><?php echo $starting_month; ?> - <?php echo $ending_month; ?></th>
                <th style='width:7%; text-align:center;'><?php echo $starting_month_1year_ago; ?> - <?php echo $ending_month_1year_ago; ?></th>
                <th style='width:7%;  text-align:center;'>Difference %</th>
            </tr>
        </thead>
        <tbody id="accounts_table">
            <?php foreach($accounts AS $item) {

                if ($item['code'] == 50000) {
                    ?>
                    <tr id="income" style="font-weight: bold;">
                        <td></td>
                        <td>1, TOTAL SALES</td>
                        <td style="text-align: center;" class="selected_period"></td>
                        <td style="text-align: center;" class="year_to_date"></td>
                        <td style="text-align: center;" class="percentage_difference"></td>
                    </tr>
                    <?php
                }

                if ($item['code'] == 50200) {
                    ?>
                    <tr id="cost_of_sales_purchases" style="font-weight: bold;">
                        <td></td>
                        <td>2, TOTAL PURCHASES</td>
                        <td style="text-align: center;" class="selected_period"></td>
                        <td style="text-align: center;" class="year_to_date"></td>
                        <td style="text-align: center;" class="percentage_difference"></td>
                    </tr>
                    <tr id="contribution_margin" style="font-weight: bold;">
                        <td></td>
                        <td>3, CONTRIBUTION MARGIN (1-2)</td>
                        <td style="text-align: center;" class="selected_period"></td>
                        <td style="text-align: center;" class="year_to_date"></td>
                        <td style="text-align: center;" class="percentage_difference"></td>
                    </tr>
                    <?php
                }

                if ($item['code'] == 60000) {
                    ?>
                    <tr id="cost_of_sales_expenses" style="font-weight: bold;">
                        <td></td>
                        <td>4, TOTAL DIRECT & VARIABLE EXPENSES</td>
                        <td style="text-align: center;" class="selected_period"></td>
                        <td style="text-align: center;" class="year_to_date"></td>
                        <td style="text-align: center;" class="percentage_difference"></td>
                    </tr>
                    <tr id="gross_profit" style="font-weight: bold;">
                        <td></td>
                        <td>5, GROSS PROFIT (3-4)</td>
                        <td style="text-align: center;" class="selected_period"></td>
                        <td style="text-align: center;" class="year_to_date"></td>
                        <td style="text-align: center;" class="percentage_difference"></td>
                    </tr>
                    <?php
                }

                if ($item['code'] == 60700) {
                    ?>
                    <tr id="expenses" style="font-weight: bold;">
                        <td></td>
                        <td>6, TOTAL OPERATING EXPENSES</td>
                        <td style="text-align: center;" class="selected_period"></td>
                        <td style="text-align: center;" class="year_to_date"></td>
                        <td style="text-align: center;" class="percentage_difference"></td>
                    </tr>
                    <tr id="operating_profit_bftax" style="font-weight: bold;">
                        <td></td>
                        <td>7, OPERATING PROFIT BEFORE TAX (5-6)</td>
                        <td style="text-align: center;" class="selected_period"></td>
                        <td style="text-align: center;" class="year_to_date"></td>
                        <td style="text-align: center;" class="percentage_difference"></td>
                    </tr>
                    <?php
                }

                if ($item['code'] == 70630) {
                    ?>
                    <tr id="expenses_tax" style="font-weight: bold;">
                        <td></td>
                        <td>8, INCOME TAX</td>
                        <td style="text-align: center;" class="selected_period"></td>
                        <td style="text-align: center;" class="year_to_date"></td>
                        <td style="text-align: center;" class="percentage_difference"></td>
                    </tr>
                    <tr id="operating_profit_aftax" style="font-weight: bold;">
                        <td></td>
                        <td>9, PROFIT AFTER TAX (7-8)</td>
                        <td style="text-align: center;" class="selected_period"></td>
                        <td style="text-align: center;" class="year_to_date"></td>
                        <td style="text-align: center;" class="percentage_difference"></td>
                    </tr>
                    <?php
                }

                $padding = $item['level']*15;

                $selected_period = 0;

                foreach ($item['selected_period'] AS $selected) {
                    if ($item['type'] == 'income' OR $item['type'] == 'other_income') {

                        if ($selected['type'] == 'credit') {
                            $selected_period += $selected['amount'];
                        } else {
                            $selected_period -= $selected['amount'];
                        }
                    } else {

                        if ($selected['type'] == 'debit') {
                            $selected_period += $selected['amount'];
                        } else {
                            $selected_period -= $selected['amount'];
                        }

                    }
                }

                $year_to_date = 0;
                foreach ($item['year_to_date'] AS $to_date) {
                    if ($item['type'] == 'income' OR $item['type'] == 'other_income') {

                        if ($to_date['type'] == 'credit') {
                            $year_to_date += $to_date['amount'];
                        } else {
                            $year_to_date -= $to_date['amount'];
                        }
                    } else {

                        if ($to_date['type'] == 'debit') {
                            $year_to_date += $to_date['amount'];
                        } else {
                            $year_to_date -= $to_date['amount'];
                        }

                    }
                }

            $class_item = '';
                if (!isset($item['children'])) {

                    $class_item = $item['type'];

                    if ($class_item == 'cost_of_sales') {
                        $class_item = $item['code'] >= 50200 ? 'cost_of_sales_expenses' : 'cost_of_sales_purchases';
                    }

                    if ($class_item == 'expenses') {
                        $class_item = $item['code'] >= 60700 ? 'expenses_tax' : 'expenses';
                    }
                }

                ?>
            <tr <?php if ($report_type == 'hidden' && $item['level'] == 3 && ($item['code'] <= 50100 || $item['code'] >= 50191)) { ?> style="display:none;" <?php } ?> class="<?php echo $class_item; ?> <?php echo $item['id']; ?> account_item" data-level="<?php echo $item['level']; ?>" data-fatherid="<?php echo $item['father_id']; ?>">
                <td style="padding-left: <?php echo $padding; ?>px;"><?php echo $item['code']; ?></td>
                <td style="padding-left: <?php echo $padding; ?>px;"><?php echo $item['name']; ?></td>
                <td class="selected_period" style='text-align:center;'><?php echo $selected_period >= 0 ? $selected_period : '('.abs($selected_period).')'; ?></td>
                <td class="year_to_date" style='text-align:center;'><?php echo $year_to_date >= 0 ? $year_to_date : '('.abs($year_to_date).')'; ?></td>
                <td style="text-align: center;" class="percentage_difference"></td>
            </tr>
            <?php } ?>

            <tr id="other_income" style="font-weight: bold;">
                <td></td>
                <td>10, TOTAL OTHER INCOME</td>
                <td style="text-align: center;" class="selected_period"></td>
                <td style="text-align: center;" class="year_to_date"></td>
                <td style="text-align: center;" class="percentage_difference"></td>
            </tr>
            <tr id="net_profit" style="font-weight: bold;">
                <td></td>
                <td>11, NET PROFIT (9+10)</td>
                <td style="text-align: center;" class="selected_period"></td>
                <td style="text-align: center;" class="year_to_date"></td>
                <td style="text-align: center;" class="percentage_difference"></td>
            </tr>
        </tbody>
    </table>
</div>
<script>

    jQuery(document).ready(function () {
        var total_selected_period = 0;
        var total_year_to_date = 0;

        $('#accounts_table .income').each(function() {
            var el = $(this);

            var selected_amount = el.find('.selected_period').html();
            var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

            var year_to_date_amount = el.find('.year_to_date').html();
            var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

            if (selected_negative_value != null) {
                total_selected_period -= parseFloat(selected_negative_value[1]);
            } else {
                total_selected_period += parseFloat(selected_amount);
            }

            if (year_to_date_negative_value != null) {
                total_year_to_date -= parseFloat(year_to_date_negative_value[1]);
            } else {
                total_year_to_date += parseFloat(year_to_date_amount);
            }

        });
        $('#income .selected_period').html(total_selected_period);
        $('#income .year_to_date').html(total_year_to_date);

        // Contribution margin
        var total_selected_period_cost_of_sales_purchases = 0;
        var total_year_to_date_cost_of_sales_purchases = 0;

        $('#accounts_table .cost_of_sales_purchases').each(function() {
            var el = $(this);

            var selected_amount = el.find('.selected_period').html();
            var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

            var year_to_date_amount = el.find('.year_to_date').html();
            var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

            if (selected_negative_value != null) {
                total_selected_period_cost_of_sales_purchases -= parseFloat(selected_negative_value[1]);
            } else {
                total_selected_period_cost_of_sales_purchases += parseFloat(selected_amount);
            }

            if (year_to_date_negative_value != null) {
                total_year_to_date_cost_of_sales_purchases -= parseFloat(year_to_date_negative_value[1]);
            } else {
                total_year_to_date_cost_of_sales_purchases += parseFloat(year_to_date_amount);
            }

        });
        var selected_period_contribution_margin = total_selected_period - total_selected_period_cost_of_sales_purchases;
        var year_to_date_contribution_margin = total_year_to_date - total_year_to_date_cost_of_sales_purchases;

        $('#cost_of_sales_purchases .selected_period').html(total_selected_period_cost_of_sales_purchases >= 0 ? total_selected_period_cost_of_sales_purchases : '('+Math.abs(total_selected_period_cost_of_sales_purchases)+')');
        $('#cost_of_sales_purchases .year_to_date').html(total_year_to_date_cost_of_sales_purchases >= 0 ? total_year_to_date_cost_of_sales_purchases : '('+Math.abs(total_year_to_date_cost_of_sales_purchases)+')');

        $('#contribution_margin .selected_period').html(selected_period_contribution_margin >= 0 ? selected_period_contribution_margin : '('+Math.abs(selected_period_contribution_margin)+')');
        $('#contribution_margin .year_to_date').html(year_to_date_contribution_margin >= 0 ? year_to_date_contribution_margin : '('+Math.abs(year_to_date_contribution_margin)+')');

        // Gross profit
        var total_selected_period_cost_of_sales_expenses = 0;
        var total_year_to_date_cost_of_sales_expenses = 0;

        $('#accounts_table .cost_of_sales_expenses').each(function() {
            var el = $(this);

            var selected_amount = el.find('.selected_period').html();
            var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

            var year_to_date_amount = el.find('.year_to_date').html();
            var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

            if (selected_negative_value != null) {
                total_selected_period_cost_of_sales_expenses -= parseFloat(selected_negative_value[1]);
            } else {
                total_selected_period_cost_of_sales_expenses += parseFloat(selected_amount);
            }

            if (year_to_date_negative_value != null) {
                total_year_to_date_cost_of_sales_expenses -= parseFloat(year_to_date_negative_value[1]);
            } else {
                total_year_to_date_cost_of_sales_expenses += parseFloat(year_to_date_amount);
            }

        });
        var selected_period_gross_profit = selected_period_contribution_margin - total_selected_period_cost_of_sales_expenses;
        var year_to_date_gross_profit = year_to_date_contribution_margin - total_year_to_date_cost_of_sales_expenses;

        $('#cost_of_sales_expenses .selected_period').html(total_selected_period_cost_of_sales_expenses >= 0 ? total_selected_period_cost_of_sales_expenses : '('+Math.abs(total_selected_period_cost_of_sales_expenses)+')');
        $('#cost_of_sales_expenses .year_to_date').html(total_year_to_date_cost_of_sales_expenses >= 0 ? total_year_to_date_cost_of_sales_expenses : '('+Math.abs(total_year_to_date_cost_of_sales_expenses)+')');

        $('#gross_profit .selected_period').html(selected_period_gross_profit >= 0 ? selected_period_gross_profit : '('+Math.abs(selected_period_gross_profit)+')');
        $('#gross_profit .year_to_date').html(year_to_date_gross_profit >= 0 ? year_to_date_gross_profit : '('+Math.abs(year_to_date_gross_profit)+')');

        // Operating profit before tax
        var total_selected_period_expenses = 0;
        var total_year_to_date_expenses = 0;

        $('#accounts_table .expenses').each(function() {
            var el = $(this);

            var selected_amount = el.find('.selected_period').html();
            var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

            var year_to_date_amount = el.find('.year_to_date').html();
            var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

            if (selected_negative_value != null) {
                total_selected_period_expenses -= parseFloat(selected_negative_value[1]);
            } else {
                total_selected_period_expenses += parseFloat(selected_amount);
            }

            if (year_to_date_negative_value != null) {
                total_year_to_date_expenses -= parseFloat(year_to_date_negative_value[1]);
            } else {
                total_year_to_date_expenses += parseFloat(year_to_date_amount);
            }

        });

        var selected_period_operating_profit_bftax = selected_period_gross_profit - total_selected_period_expenses;
        var year_to_date_operating_profit_bftax = year_to_date_gross_profit - total_year_to_date_expenses;

        $('#expenses .selected_period').html(total_selected_period_expenses >= 0 ? total_selected_period_expenses : '('+Math.abs(total_selected_period_expenses)+')');
        $('#expenses .year_to_date').html(total_year_to_date_expenses >= 0 ? total_year_to_date_expenses : '('+Math.abs(total_year_to_date_expenses)+')');

        $('#operating_profit_bftax .selected_period').html(selected_period_operating_profit_bftax >= 0 ? selected_period_operating_profit_bftax : '('+Math.abs(selected_period_operating_profit_bftax)+')');
        $('#operating_profit_bftax .year_to_date').html(year_to_date_operating_profit_bftax >= 0 ? year_to_date_operating_profit_bftax : '('+Math.abs(year_to_date_operating_profit_bftax)+')');

        // Operating profit after tax
        var total_selected_period_expenses_tax = 0;
        var total_year_to_date_expenses_tax = 0;

        $('#accounts_table .expenses_tax').each(function() {
            var el = $(this);

            var selected_amount = el.find('.selected_period').html();
            var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

            var year_to_date_amount = el.find('.year_to_date').html();
            var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

            if (selected_negative_value != null) {
                total_selected_period_expenses_tax -= parseFloat(selected_negative_value[1]);
            } else {
                total_selected_period_expenses_tax += parseFloat(selected_amount);
            }

            if (year_to_date_negative_value != null) {
                total_year_to_date_expenses_tax -= parseFloat(year_to_date_negative_value[1]);
            } else {
                total_year_to_date_expenses_tax += parseFloat(year_to_date_amount);
            }

        });

        var selected_period_operating_profit_aftax = selected_period_operating_profit_bftax - total_selected_period_expenses_tax;
        var year_to_date_operating_profit_aftax = year_to_date_operating_profit_bftax - total_year_to_date_expenses_tax;

        $('#expenses_tax .selected_period').html(total_selected_period_expenses_tax >= 0 ? total_selected_period_expenses_tax : '('+Math.abs(total_selected_period_expenses_tax)+')');
        $('#expenses_tax .year_to_date').html(total_year_to_date_expenses_tax >= 0 ? total_year_to_date_expenses_tax : '('+Math.abs(total_year_to_date_expenses_tax)+')');

        $('#operating_profit_aftax .selected_period').html(selected_period_operating_profit_aftax >= 0 ? selected_period_operating_profit_aftax : '('+Math.abs(selected_period_operating_profit_aftax)+')');
        $('#operating_profit_aftax .year_to_date').html(year_to_date_operating_profit_aftax >= 0 ? year_to_date_operating_profit_aftax : '('+Math.abs(year_to_date_operating_profit_aftax)+')');


        // Operating profit after tax
        var total_selected_period_other_income = 0;
        var total_year_to_date_other_income = 0;

        $('#accounts_table .other_income').each(function() {
            var el = $(this);

            var selected_amount = el.find('.selected_period').html();
            var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

            var year_to_date_amount = el.find('.year_to_date').html();
            var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

            if (selected_negative_value != null) {
                total_selected_period_other_income -= parseFloat(selected_negative_value[1]);
            } else {
                total_selected_period_other_income += parseFloat(selected_amount);
            }

            if (year_to_date_negative_value != null) {
                total_year_to_date_other_income -= parseFloat(year_to_date_negative_value[1]);
            } else {
                total_year_to_date_other_income += parseFloat(year_to_date_amount);
            }

        });

        var selected_period_net_profit = parseFloat(selected_period_operating_profit_aftax) + parseFloat(total_selected_period_other_income);
        var year_to_date_net_profit = parseFloat(year_to_date_operating_profit_aftax) + parseFloat(total_year_to_date_other_income);

        $('#other_income .selected_period').html(total_selected_period_other_income >= 0 ? total_selected_period_other_income : '('+Math.abs(total_selected_period_other_income)+')');
        $('#other_income .year_to_date').html(total_year_to_date_other_income >= 0 ? total_year_to_date_other_income : '('+Math.abs(total_year_to_date_other_income)+')');

        $('#net_profit .selected_period').html(selected_period_net_profit >= 0 ? selected_period_net_profit : '('+Math.abs(selected_period_net_profit)+')');
        $('#net_profit .year_to_date').html(year_to_date_net_profit >= 0 ? year_to_date_net_profit : '('+Math.abs(year_to_date_net_profit)+')');


        $('#accounts_table tr').each(function() {
            var el = $(this);

            if (el.data('level') == 3) {
                var father_id = el.data('fatherid');

                var father_amount_selected = $('.'+father_id).find('.selected_period').html();
                var father_amount_selected_negative_value = father_amount_selected.match(/\(([^)]+)\)/);
                if (father_amount_selected_negative_value != null) {
                    father_amount_selected = -parseFloat(father_amount_selected_negative_value[1]);
                } else {
                    father_amount_selected = parseFloat(father_amount_selected);
                }

                var father_amount_year_to_date = $('.'+father_id).find('.year_to_date').html();
                var father_amount_year_to_date_negative_value = father_amount_year_to_date.match(/\(([^)]+)\)/);
                if (father_amount_year_to_date_negative_value != null) {
                    father_amount_year_to_date = -parseFloat(father_amount_year_to_date_negative_value[1]);
                } else {
                    father_amount_year_to_date = parseFloat(father_amount_year_to_date);
                }

                var selected_amount = el.find('.selected_period').html();
                var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

                var year_to_date_amount = el.find('.year_to_date').html();
                var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

                if (selected_negative_value != null) {
                    father_amount_selected -= parseFloat(selected_negative_value[1]);
                } else {
                    father_amount_selected += parseFloat(selected_amount);
                }

                if (year_to_date_negative_value != null) {
                    father_amount_year_to_date -= parseFloat(year_to_date_negative_value[1]);
                } else {
                    father_amount_year_to_date += parseFloat(year_to_date_amount);
                }

                $('.'+father_id).find('.selected_period').html(father_amount_selected >= 0 ? father_amount_selected : '('+Math.abs(father_amount_selected)+')');
                $('.'+father_id).find('.year_to_date').html(father_amount_year_to_date >= 0 ? father_amount_year_to_date : '('+Math.abs(father_amount_year_to_date)+')');
            }

        });

        $('#accounts_table tr').each(function() {
            var el = $(this);

            if (el.data('level') == 2) {
                var father_id = el.data('fatherid');

                var father_amount_selected = $('.'+father_id).find('.selected_period').html();
                var father_amount_selected_negative_value = father_amount_selected.match(/\(([^)]+)\)/);
                if (father_amount_selected_negative_value != null) {
                    father_amount_selected = -parseFloat(father_amount_selected_negative_value[1]);
                } else {
                    father_amount_selected = parseFloat(father_amount_selected);
                }

                var father_amount_year_to_date = $('.'+father_id).find('.year_to_date').html();
                var father_amount_year_to_date_negative_value = father_amount_year_to_date.match(/\(([^)]+)\)/);
                if (father_amount_year_to_date_negative_value != null) {
                    father_amount_year_to_date = -parseFloat(father_amount_year_to_date_negative_value[1]);
                } else {
                    father_amount_year_to_date = parseFloat(father_amount_year_to_date);
                }

                var selected_amount = el.find('.selected_period').html();
                var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

                var year_to_date_amount = el.find('.year_to_date').html();
                var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

                if (selected_negative_value != null) {
                    father_amount_selected -= parseFloat(selected_negative_value[1]);
                } else {
                    father_amount_selected += parseFloat(selected_amount);
                }

                if (year_to_date_negative_value != null) {
                    father_amount_year_to_date -= parseFloat(year_to_date_negative_value[1]);
                } else {
                    father_amount_year_to_date += parseFloat(year_to_date_amount);
                }

                $('.'+father_id).find('.selected_period').html(father_amount_selected >= 0 ? father_amount_selected : '('+Math.abs(father_amount_selected)+')');
                $('.'+father_id).find('.year_to_date').html(father_amount_year_to_date >= 0 ? father_amount_year_to_date : '('+Math.abs(father_amount_year_to_date)+')');
            }

        });

        $('.selected_period').each(function() {
            var el = $(this);

            var html_amount = el.html();

            var year_to_date = el.closest('tr').find('.year_to_date').html();

            if (parseFloat(html_amount) == 0 && parseFloat(year_to_date) == 0) {

                el.closest('.account_item').hide();
            }
        });

        // Update percentage
        $('#accounts_table .percentage_difference').each(function() {
            var el = $(this);

            var selected_amount = el.closest('tr').find('.selected_period').html();
            var selected_negative_value = selected_amount.match(/\(([^)]+)\)/);

            var year_to_date_amount = el.closest('tr').find('.year_to_date').html();
            var year_to_date_negative_value = year_to_date_amount.match(/\(([^)]+)\)/);

            if (selected_negative_value != null) {
                selected_amount = -parseFloat(selected_negative_value[1]);
            }

            if (year_to_date_negative_value != null) {
                year_to_date_amount = -parseFloat(year_to_date_negative_value[1]);
            }

            var percentage_diff = selected_amount/year_to_date_amount*100;

            el.find('.percentage_difference').html(percentage_diff >= 0 ? percentage_diff : '('+Math.abs(percentage_diff)+')');
        });

        $('.selected_period, .year_to_date, .percentage_difference').each(function() {
            var el = $(this);

            var html_amount = el.html();
            var check_rackets = html_amount.match(/\(([^)]+)\)/);

            if (check_rackets != null) {

                var round_amount = numberWithCommas(parseFloat(check_rackets[1]).toFixed(2));

                el.html('('+round_amount+')');
            } else {
                el.html(numberWithCommas(parseFloat(html_amount).toFixed(2)));
            }
        });

        function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }
    });

    $(document).ready(function() {
        $("#download_excel_file").click(function(e) {
            e.preventDefault();

            //getting data from our table
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('table_wrapper');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');

            var a = document.createElement('a');
            a.href = data_type + ', ' + table_html;
            a.download = 'PL_year_to_date_report.xls';
            a.click();
        });

        $('body').mouseover(function () {
            $("#download_excel_file").show();
            $("#download_pdf_file").show();
        });

        $("#download_pdf_file").click(function () {
            $("#download_excel_file").hide();
            $("#download_pdf_file").hide();
            window.print();
        });
    });
</script>