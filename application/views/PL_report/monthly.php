<style>
    table { border-collapse: collapse; }
    tr.border-top { border-top: solid thin; }
    tr.border-bottom { border-bottom: solid thin; }
    tr.bold { font-weight: bold; }
    * { font-size: 12px; font-family: Lucida Sans Unicode; }
</style>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/slider-pips/jquery-ui.js"></script>
<button type="button" id="download_excel_file">Download Excel Report</button>
<button type="button" id="download_pdf_file">Print or Download PDF Report</button>
<div id="table_wrapper">
    <table style='width:100%;'>
        <tr>
            <td style='font-size:20px;text-align:center;color:blue;'><?php echo $company_name; ?></td>
        </tr>
        <tr>
            <td style='font-size:16px;text-align:center;color:blue;'>Outlets List</td>
        </tr>
        <tr>
            <td style='font-size:16px;text-align:center;color:blue;'>
                <?php foreach($outlets AS $item) {
                    echo $item.'<br/>';
                } ?>
            </td>
        </tr>
        <tr>
            <td style='text-align:center;font-size:18px;'>Profit/Loss Statement (monthly)</td>
        </tr>
        <tr>
            <td style='font-size:16px;text-align:center;'><?php echo $starting_month; ?> - <?php echo $ending_month; ?></td>
        </tr>
    </table>
    <table style='width:100%;'>
        <thead>
            <tr class='border-top'>
                <th colspan="2" style='width:20%;  text-align:center;'>Accounts</th>
                <th style='width:5%;  text-align:center;'><?php echo date('F', strtotime($starting_month)); ?></th>
                <th style='width:5%;  text-align:center;'><?php echo date('F', strtotime('+1 month', strtotime($starting_month))); ?></th>
                <?php for($i=1; $i<11; $i++) { ?>
                    <th style='width:5%;  text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>'><?php echo date('F', strtotime('+'.($i+1).' months', strtotime($starting_month))); ?></th>
                <?php } ?>
                <th style='width:5%;  text-align:center;'>FY</th>
            </tr>
        </thead>
        <tbody id="accounts_table">
            <?php foreach($accounts AS $item) {

                if ($item['code'] == 50000) {
                    ?>
                    <tr id="income" style="font-weight: bold;">
                        <td></td>
                        <td>1, TOTAL SALES</td>
                        <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
                        <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime('+1 month', strtotime($starting_month)))); ?>"></td>
                        <?php for($i=1; $i<11; $i++) { ?>
                            <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime('+'.($i+1).' months', strtotime($starting_month)))); ?>"></td>
                        <?php } ?>
                        <td style='text-align:center; font-weight: bold;' class="FY_total"></td>
                    </tr>
                    <?php
                }

                if ($item['code'] == 50200) {
                    ?>
                    <tr id="cost_of_sales_purchases" style="font-weight: bold;">
                        <td></td>
                        <td>2, TOTAL PURCHASES</td>
                        <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
                        <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime('+1 month', strtotime($starting_month)))); ?>"></td>
                        <?php for($i=1; $i<11; $i++) { ?>
                            <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime('+'.($i+1).' months', strtotime($starting_month)))); ?>"></td>
                        <?php } ?>
                        <td style='text-align:center; font-weight: bold;' class="FY_total"></td>
                    </tr>
                    <tr id="contribution_margin" style="font-weight: bold;">
                        <td></td>
                        <td>3, CONTRIBUTION MARGIN (1-2)</td>
                        <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
                        <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime('+1 month', strtotime($starting_month)))); ?>"></td>
                        <?php for($i=1; $i<11; $i++) { ?>
                            <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime('+'.($i+1).' months', strtotime($starting_month)))); ?>"></td>
                        <?php } ?>
                        <td style='text-align:center; font-weight: bold;' class="FY_total"></td>
                    </tr>
                    <?php
                }

                if ($item['code'] == 60000) {
                    ?>
                    <tr id="cost_of_sales_expenses" style="font-weight: bold;">
                        <td></td>
                        <td>4, TOTAL DIRECT & VARIABLE EXPENSES</td>
                        <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
                        <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime('+1 month', strtotime($starting_month)))); ?>"></td>
                        <?php for($i=1; $i<11; $i++) { ?>
                            <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime('+'.($i+1).' months', strtotime($starting_month)))); ?>"></td>
                        <?php } ?>
                        <td style='text-align:center; font-weight: bold;' class="FY_total"></td>
                    </tr>
                    <tr id="gross_profit" style="font-weight: bold;">
                        <td></td>
                        <td>5, GROSS PROFIT (3-4)</td>
                        <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
                        <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime('+1 month', strtotime($starting_month)))); ?>"></td>
                        <?php for($i=1; $i<11; $i++) { ?>
                            <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime('+'.($i+1).' months', strtotime($starting_month)))); ?>"></td>
                        <?php } ?>
                        <td style='text-align:center; font-weight: bold;' class="FY_total"></td>
                    </tr>
                    <?php
                }

                if ($item['code'] == 60700) {
                    ?>
                    <tr id="expenses" style="font-weight: bold;">
                        <td></td>
                        <td>6, TOTAL OPERATING EXPENSES</td>
                        <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
                        <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime('+1 month', strtotime($starting_month)))); ?>"></td>
                        <?php for($i=1; $i<11; $i++) { ?>
                            <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime('+'.($i+1).' months', strtotime($starting_month)))); ?>"></td>
                        <?php } ?>
                        <td style='text-align:center; font-weight: bold;' class="FY_total"></td>
                    </tr>
                    <tr id="operating_profit_bftax" style="font-weight: bold;">
                        <td></td>
                        <td>7, OPERATING PROFIT BEFORE TAX (5-6)</td>
                        <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
                        <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime('+1 month', strtotime($starting_month)))); ?>"></td>
                        <?php for($i=1; $i<11; $i++) { ?>
                            <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime('+'.($i+1).' months', strtotime($starting_month)))); ?>"></td>
                        <?php } ?>
                        <td style='text-align:center; font-weight: bold;' class="FY_total"></td>
                    </tr>
                    <?php
                }

                if ($item['code'] == 70630) {
                    ?>
                    <tr id="expenses_tax" style="font-weight: bold;">
                        <td></td>
                        <td>8, INCOME TAX</td>
                        <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
                        <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime('+1 month', strtotime($starting_month)))); ?>"></td>
                        <?php for($i=1; $i<11; $i++) { ?>
                            <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime('+'.($i+1).' months', strtotime($starting_month)))); ?>"></td>
                        <?php } ?>
                        <td style='text-align:center; font-weight: bold;' class="FY_total"></td>
                    </tr>
                    <tr id="operating_profit_aftax" style="font-weight: bold;">
                        <td></td>
                        <td>9, PROFIT AFTER TAX (7-8)</td>
                        <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
                        <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime('+1 month', strtotime($starting_month)))); ?>"></td>
                        <?php for($i=1; $i<11; $i++) { ?>
                            <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime('+'.($i+1).' months', strtotime($starting_month)))); ?>"></td>
                        <?php } ?>
                        <td style='text-align:center; font-weight: bold;' class="FY_total"></td>
                    </tr>
                    <?php
                }

                $padding = $item['level']*15;

                $monthly_report = [];
                foreach ($item['monthly_report'] AS $record) {
                    if ($item['type'] == 'income' OR $item['type'] == 'other_income') {

                        if ($record['type'] == 'credit') {
                            $monthly_report[$record['document_month']] += $record['amount'];
                        } else {
                            $monthly_report[$record['document_month']] -= $record['amount'];
                        }
                    } else {

                        if ($record['type'] == 'debit') {
                            $monthly_report[$record['document_month']] += $record['amount'];
                        } else {
                            $monthly_report[$record['document_month']] -= $record['amount'];
                        }

                    }
                }
            $class_item = '';
            if (!isset($item['children'])) {
                $class_item = $item['type'];

                if ($class_item == 'cost_of_sales') {
                    $class_item = $item['code'] >= 50200 ? 'cost_of_sales_expenses' : 'cost_of_sales_purchases';
                }

                if ($class_item == 'expenses') {
                    $class_item = $item['code'] >= 60700 ? 'expenses_tax' : 'expenses';
                }
            }

                ?>
            <tr <?php if ($report_type == 'hidden' && $item['level'] == 3 && ($item['code'] <= 50100 || $item['code'] >= 50191)) { ?> style="display:none;" <?php } ?> class="<?php echo $class_item; ?> <?php echo $item['id']; ?> account_item" data-level="<?php echo $item['level']; ?>" data-fatherid="<?php echo $item['father_id']; ?>">
                <td style="padding-left: <?php echo $padding; ?>px;"><?php echo $item['code']; ?></td>
                <td style="padding-left: <?php echo $padding; ?>px;"><?php echo $item['name']; ?></td>

                <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"><?php echo isset($monthly_report[strtolower(date('n', strtotime($starting_month)))]) ? $monthly_report[strtolower(date('n', strtotime($starting_month)))] > 0 ? $monthly_report[strtolower(date('n', strtotime($starting_month)))] : '('.abs($monthly_report[strtolower(date('n', strtotime($starting_month)))]).')' : '0'; ?></td>
                <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime('+1 month', strtotime($starting_month)))); ?>"><?php echo isset($monthly_report[strtolower(date('n', strtotime('+1 month', strtotime($starting_month))))]) ? $monthly_report[strtolower(date('n', strtotime('+1 month', strtotime($starting_month))))] > 0 ? $monthly_report[strtolower(date('n', strtotime('+1 month', strtotime($starting_month))))] : '('.abs($monthly_report[strtolower(date('n', strtotime('+1 month', strtotime($starting_month))))]).')' : '0'; ?></td>
                <?php for($i=1; $i<11; $i++) { ?>
                    <td style='width:5%;  text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime('+'.($i+1).' months', strtotime($starting_month)))); ?>"><?php echo isset($monthly_report[strtolower(date('n', strtotime('+'.($i+1).' months', strtotime($starting_month))))]) ? $monthly_report[strtolower(date('n', strtotime('+'.($i+1).' months', strtotime($starting_month))))] > 0 ? $monthly_report[strtolower(date('n', strtotime('+'.($i+1).' months', strtotime($starting_month))))] : '('.abs($monthly_report[strtolower(date('n', strtotime('+'.($i+1).' months', strtotime($starting_month))))]).')' : '0'; ?></td>
                <?php } ?>
                <td style='text-align:center; font-weight: bold;' class="FY_total"></td>

            </tr>
            <?php } ?>
            <tr id="other_income" style="font-weight: bold;">
                <td></td>
                <td>10, TOTAL OTHER INCOME</td>
                <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
                <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime('+1 month', strtotime($starting_month)))); ?>"></td>
                <?php for($i=1; $i<11; $i++) { ?>
                    <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime('+'.($i+1).' months', strtotime($starting_month)))); ?>"></td>
                <?php } ?>
                <td style='text-align:center; font-weight: bold;' class="FY_total"></td>
            </tr>
            <tr id="net_profit" style="font-weight: bold;">
                <td></td>
                <td>11, NET PROFIT (9+10)</td>
                <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime($starting_month))); ?>"></td>
                <td style='text-align:center;' class="<?php echo strtolower(date('F', strtotime('+1 month', strtotime($starting_month)))); ?>"></td>
                <?php for($i=1; $i<11; $i++) { ?>
                    <td style='text-align:center; <?php echo strtotime($starting_month.' +'.($i+1).' months') > strtotime($ending_month) ? "display:none;" : "" ?>' class="<?php echo strtolower(date('F', strtotime('+'.($i+1).' months', strtotime($starting_month)))); ?>"></td>
                <?php } ?>
                <td style='text-align:center; font-weight: bold;' class="FY_total"></td>
            </tr>
        </tbody>
    </table>
</div>
<script>

    jQuery(document).ready(function () {
        var january_total = 0;
        var february_total = 0;
        var march_total = 0;
        var april_total = 0;
        var may_total = 0;
        var june_total = 0;
        var july_total = 0;
        var august_total = 0;
        var september_total = 0;
        var october_total = 0;
        var november_total = 0;
        var december_total = 0;

        $('#accounts_table .income').each(function() {
            var el = $(this);

            var january = el.find('.january').html();
            var january_negative_value = january.match(/\(([^)]+)\)/);

            var february = el.find('.february').html();
            var february_negative_value = february.match(/\(([^)]+)\)/);

            var march = el.find('.march').html();
            var march_negative_value = march.match(/\(([^)]+)\)/);

            var april = el.find('.april').html();
            var april_negative_value = april.match(/\(([^)]+)\)/);

            var may = el.find('.may').html();
            var may_negative_value = may.match(/\(([^)]+)\)/);

            var june = el.find('.june').html();
            var june_negative_value = june.match(/\(([^)]+)\)/);

            var july = el.find('.july').html();
            var july_negative_value = july.match(/\(([^)]+)\)/);

            var august = el.find('.august').html();
            var august_negative_value = august.match(/\(([^)]+)\)/);

            var september = el.find('.september').html();
            var september_negative_value = september.match(/\(([^)]+)\)/);

            var october = el.find('.october').html();
            var october_negative_value = october.match(/\(([^)]+)\)/);

            var november = el.find('.november').html();
            var november_negative_value = november.match(/\(([^)]+)\)/);

            var december = el.find('.december').html();
            var december_negative_value = december.match(/\(([^)]+)\)/);

            if (january_negative_value != null) {
                january_total -= parseFloat(january_negative_value[1]);
            } else {
                january_total += parseFloat(january);
            }

            if (february_negative_value != null) {
                february_total -= parseFloat(february_negative_value[1]);
            } else {
                february_total += parseFloat(february);
            }

            if (march_negative_value != null) {
                march_total -= parseFloat(march_negative_value[1]);
            } else {
                march_total += parseFloat(march);
            }

            if (april_negative_value != null) {
                april_total -= parseFloat(april_negative_value[1]);
            } else {
                april_total += parseFloat(april);
            }

            if (may_negative_value != null) {
                may_total -= parseFloat(may_negative_value[1]);
            } else {
                may_total += parseFloat(may);
            }

            if (june_negative_value != null) {
                june_total -= parseFloat(june_negative_value[1]);
            } else {
                june_total += parseFloat(june);
            }

            if (july_negative_value != null) {
                july_total -= parseFloat(july_negative_value[1]);
            } else {
                july_total += parseFloat(july);
            }

            if (august_negative_value != null) {
                august_total -= parseFloat(august_negative_value[1]);
            } else {
                august_total += parseFloat(august);
            }

            if (september_negative_value != null) {
                september_total -= parseFloat(september_negative_value[1]);
            } else {
                september_total += parseFloat(september);
            }

            if (october_negative_value != null) {
                october_total -= parseFloat(october_negative_value[1]);
            } else {
                october_total += parseFloat(october);
            }

            if (november_negative_value != null) {
                november_total -= parseFloat(november_negative_value[1]);
            } else {
                november_total += parseFloat(november);
            }

            if (december_negative_value != null) {
                december_total -= parseFloat(december_negative_value[1]);
            } else {
                december_total += parseFloat(december);
            }

        });
        $('#income .january').html(january_total);
        $('#income .february').html(february_total);
        $('#income .march').html(march_total);
        $('#income .april').html(april_total);
        $('#income .may').html(may_total);
        $('#income .june').html(june_total);
        $('#income .july').html(july_total);
        $('#income .august').html(august_total);
        $('#income .september').html(september_total);
        $('#income .october').html(october_total);
        $('#income .november').html(november_total);
        $('#income .december').html(december_total);

        // Contribution margin
        var january_total_cost_of_sales_purchases = 0;
        var february_total_cost_of_sales_purchases = 0;
        var march_total_cost_of_sales_purchases = 0;
        var april_total_cost_of_sales_purchases = 0;
        var may_total_cost_of_sales_purchases = 0;
        var june_total_cost_of_sales_purchases = 0;
        var july_total_cost_of_sales_purchases = 0;
        var august_total_cost_of_sales_purchases = 0;
        var september_total_cost_of_sales_purchases = 0;
        var october_total_cost_of_sales_purchases = 0;
        var november_total_cost_of_sales_purchases = 0;
        var december_total_cost_of_sales_purchases = 0;

        $('#accounts_table .cost_of_sales_purchases').each(function() {
            var el = $(this);

            var january = el.find('.january').html();
            var january_negative_value = january.match(/\(([^)]+)\)/);

            if (january_negative_value != null) {
                january_total_cost_of_sales_purchases -= parseFloat(january_negative_value[1]);
            } else {
                january_total_cost_of_sales_purchases += parseFloat(january);
            }

            var february = el.find('.february').html();
            var february_negative_value = february.match(/\(([^)]+)\)/);

            if (february_negative_value != null) {
                february_total_cost_of_sales_purchases -= parseFloat(february_negative_value[1]);
            } else {
                february_total_cost_of_sales_purchases += parseFloat(february);
            }

            var march = el.find('.march').html();
            var march_negative_value = march.match(/\(([^)]+)\)/);

            if (march_negative_value != null) {
                march_total_cost_of_sales_purchases -= parseFloat(march_negative_value[1]);
            } else {
                march_total_cost_of_sales_purchases += parseFloat(march);
            }

            var april = el.find('.april').html();
            var april_negative_value = april.match(/\(([^)]+)\)/);

            if (april_negative_value != null) {
                april_total_cost_of_sales_purchases -= parseFloat(april_negative_value[1]);
            } else {
                april_total_cost_of_sales_purchases += parseFloat(april);
            }

            var may = el.find('.may').html();
            var may_negative_value = may.match(/\(([^)]+)\)/);

            if (may_negative_value != null) {
                may_total_cost_of_sales_purchases -= parseFloat(may_negative_value[1]);
            } else {
                may_total_cost_of_sales_purchases += parseFloat(may);
            }

            var june = el.find('.june').html();
            var june_negative_value = june.match(/\(([^)]+)\)/);

            if (june_negative_value != null) {
                june_total_cost_of_sales_purchases -= parseFloat(june_negative_value[1]);
            } else {
                june_total_cost_of_sales_purchases += parseFloat(june);
            }

            var july = el.find('.july').html();
            var july_negative_value = july.match(/\(([^)]+)\)/);

            if (july_negative_value != null) {
                july_total_cost_of_sales_purchases -= parseFloat(july_negative_value[1]);
            } else {
                july_total_cost_of_sales_purchases += parseFloat(july);
            }

            var august = el.find('.august').html();
            var august_negative_value = august.match(/\(([^)]+)\)/);

            if (august_negative_value != null) {
                august_total_cost_of_sales_purchases -= parseFloat(august_negative_value[1]);
            } else {
                august_total_cost_of_sales_purchases += parseFloat(august);
            }

            var september = el.find('.september').html();
            var september_negative_value = september.match(/\(([^)]+)\)/);

            if (september_negative_value != null) {
                september_total_cost_of_sales_purchases -= parseFloat(september_negative_value[1]);
            } else {
                september_total_cost_of_sales_purchases += parseFloat(september);
            }

            var october = el.find('.october').html();
            var october_negative_value = october.match(/\(([^)]+)\)/);

            if (october_negative_value != null) {
                october_total_cost_of_sales_purchases -= parseFloat(october_negative_value[1]);
            } else {
                october_total_cost_of_sales_purchases += parseFloat(october);
            }

            var november = el.find('.november').html();
            var november_negative_value = november.match(/\(([^)]+)\)/);

            if (november_negative_value != null) {
                november_total_cost_of_sales_purchases -= parseFloat(november_negative_value[1]);
            } else {
                november_total_cost_of_sales_purchases += parseFloat(november);
            }

            var december = el.find('.december').html();
            var december_negative_value = december.match(/\(([^)]+)\)/);

            if (december_negative_value != null) {
                december_total_cost_of_sales_purchases -= parseFloat(december_negative_value[1]);
            } else {
                december_total_cost_of_sales_purchases += parseFloat(december);
            }

        });
        var january_contribution_margin = january_total - january_total_cost_of_sales_purchases;

        $('#cost_of_sales_purchases .january').html(january_total_cost_of_sales_purchases >= 0 ? january_total_cost_of_sales_purchases : '('+Math.abs(january_total_cost_of_sales_purchases)+')');

        $('#contribution_margin .january').html(january_contribution_margin >= 0 ? january_contribution_margin : '('+Math.abs(january_contribution_margin)+')');

        var february_contribution_margin = february_total - february_total_cost_of_sales_purchases;

        $('#cost_of_sales_purchases .february').html(february_total_cost_of_sales_purchases >= 0 ? february_total_cost_of_sales_purchases : '('+Math.abs(february_total_cost_of_sales_purchases)+')');

        $('#contribution_margin .february').html(february_contribution_margin >= 0 ? february_contribution_margin : '('+Math.abs(february_contribution_margin)+')');

        var march_contribution_margin = march_total - march_total_cost_of_sales_purchases;

        $('#cost_of_sales_purchases .march').html(march_total_cost_of_sales_purchases >= 0 ? march_total_cost_of_sales_purchases : '('+Math.abs(march_total_cost_of_sales_purchases)+')');

        $('#contribution_margin .march').html(march_contribution_margin >= 0 ? march_contribution_margin : '('+Math.abs(march_contribution_margin)+')');

        var april_contribution_margin = april_total - april_total_cost_of_sales_purchases;

        $('#cost_of_sales_purchases .april').html(april_total_cost_of_sales_purchases >= 0 ? april_total_cost_of_sales_purchases : '('+Math.abs(april_total_cost_of_sales_purchases)+')');

        $('#contribution_margin .april').html(april_contribution_margin >= 0 ? april_contribution_margin : '('+Math.abs(april_contribution_margin)+')');

        var may_contribution_margin = may_total - may_total_cost_of_sales_purchases;

        $('#cost_of_sales_purchases .may').html(may_total_cost_of_sales_purchases >= 0 ? may_total_cost_of_sales_purchases : '('+Math.abs(may_total_cost_of_sales_purchases)+')');

        $('#contribution_margin .may').html(may_contribution_margin >= 0 ? may_contribution_margin : '('+Math.abs(may_contribution_margin)+')');

        var june_contribution_margin = june_total - june_total_cost_of_sales_purchases;

        $('#cost_of_sales_purchases .june').html(june_total_cost_of_sales_purchases >= 0 ? june_total_cost_of_sales_purchases : '('+Math.abs(june_total_cost_of_sales_purchases)+')');

        $('#contribution_margin .june').html(june_contribution_margin >= 0 ? june_contribution_margin : '('+Math.abs(june_contribution_margin)+')');

        var july_contribution_margin = july_total - july_total_cost_of_sales_purchases;

        $('#cost_of_sales_purchases .july').html(july_total_cost_of_sales_purchases >= 0 ? july_total_cost_of_sales_purchases : '('+Math.abs(july_total_cost_of_sales_purchases)+')');

        $('#contribution_margin .july').html(july_contribution_margin >= 0 ? july_contribution_margin : '('+Math.abs(july_contribution_margin)+')');

        var august_contribution_margin = august_total - august_total_cost_of_sales_purchases;

        $('#cost_of_sales_purchases .august').html(august_total_cost_of_sales_purchases >= 0 ? august_total_cost_of_sales_purchases : '('+Math.abs(august_total_cost_of_sales_purchases)+')');

        $('#contribution_margin .august').html(august_contribution_margin >= 0 ? august_contribution_margin : '('+Math.abs(august_contribution_margin)+')');

        var september_contribution_margin = september_total - september_total_cost_of_sales_purchases;

        $('#cost_of_sales_purchases .september').html(september_total_cost_of_sales_purchases >= 0 ? september_total_cost_of_sales_purchases : '('+Math.abs(september_total_cost_of_sales_purchases)+')');

        $('#contribution_margin .september').html(september_contribution_margin >= 0 ? september_contribution_margin : '('+Math.abs(september_contribution_margin)+')');

        var october_contribution_margin = october_total - october_total_cost_of_sales_purchases;

        $('#cost_of_sales_purchases .october').html(october_total_cost_of_sales_purchases >= 0 ? october_total_cost_of_sales_purchases : '('+Math.abs(october_total_cost_of_sales_purchases)+')');

        $('#contribution_margin .october').html(october_contribution_margin >= 0 ? october_contribution_margin : '('+Math.abs(october_contribution_margin)+')');

        var november_contribution_margin = november_total - november_total_cost_of_sales_purchases;

        $('#cost_of_sales_purchases .november').html(november_total_cost_of_sales_purchases >= 0 ? november_total_cost_of_sales_purchases : '('+Math.abs(november_total_cost_of_sales_purchases)+')');

        $('#contribution_margin .november').html(november_contribution_margin >= 0 ? november_contribution_margin : '('+Math.abs(november_contribution_margin)+')');

        var december_contribution_margin = december_total - december_total_cost_of_sales_purchases;

        $('#cost_of_sales_purchases .december').html(december_total_cost_of_sales_purchases >= 0 ? december_total_cost_of_sales_purchases : '('+Math.abs(december_total_cost_of_sales_purchases)+')');

        $('#contribution_margin .december').html(december_contribution_margin >= 0 ? december_contribution_margin : '('+Math.abs(december_contribution_margin)+')');

        // Gross profit
        var january_total_cost_of_sales_expenses = 0;
        var february_total_cost_of_sales_expenses = 0;
        var march_total_cost_of_sales_expenses = 0;
        var april_total_cost_of_sales_expenses = 0;
        var may_total_cost_of_sales_expenses = 0;
        var june_total_cost_of_sales_expenses = 0;
        var july_total_cost_of_sales_expenses = 0;
        var august_total_cost_of_sales_expenses = 0;
        var september_total_cost_of_sales_expenses = 0;
        var october_total_cost_of_sales_expenses = 0;
        var november_total_cost_of_sales_expenses = 0;
        var december_total_cost_of_sales_expenses = 0;

        $('#accounts_table .cost_of_sales_expenses').each(function() {
            var el = $(this);

            var january = el.find('.january').html();
            var january_negative_value = january.match(/\(([^)]+)\)/);

            if (january_negative_value != null) {
                january_total_cost_of_sales_expenses -= parseFloat(january_negative_value[1]);
            } else {
                january_total_cost_of_sales_expenses += parseFloat(january);
            }

            var february = el.find('.february').html();
            var february_negative_value = february.match(/\(([^)]+)\)/);

            if (february_negative_value != null) {
                february_total_cost_of_sales_expenses -= parseFloat(february_negative_value[1]);
            } else {
                february_total_cost_of_sales_expenses += parseFloat(february);
            }

            var march = el.find('.march').html();
            var march_negative_value = march.match(/\(([^)]+)\)/);

            if (march_negative_value != null) {
                march_total_cost_of_sales_expenses -= parseFloat(march_negative_value[1]);
            } else {
                march_total_cost_of_sales_expenses += parseFloat(march);
            }

            var april = el.find('.april').html();
            var april_negative_value = april.match(/\(([^)]+)\)/);

            if (april_negative_value != null) {
                april_total_cost_of_sales_expenses -= parseFloat(april_negative_value[1]);
            } else {
                april_total_cost_of_sales_expenses += parseFloat(april);
            }

            var may = el.find('.may').html();
            var may_negative_value = may.match(/\(([^)]+)\)/);

            if (may_negative_value != null) {
                may_total_cost_of_sales_expenses -= parseFloat(may_negative_value[1]);
            } else {
                may_total_cost_of_sales_expenses += parseFloat(may);
            }

            var june = el.find('.june').html();
            var june_negative_value = june.match(/\(([^)]+)\)/);

            if (june_negative_value != null) {
                june_total_cost_of_sales_expenses -= parseFloat(june_negative_value[1]);
            } else {
                june_total_cost_of_sales_expenses += parseFloat(june);
            }

            var july = el.find('.july').html();
            var july_negative_value = july.match(/\(([^)]+)\)/);

            if (july_negative_value != null) {
                july_total_cost_of_sales_expenses -= parseFloat(july_negative_value[1]);
            } else {
                july_total_cost_of_sales_expenses += parseFloat(july);
            }

            var august = el.find('.august').html();
            var august_negative_value = august.match(/\(([^)]+)\)/);

            if (august_negative_value != null) {
                august_total_cost_of_sales_expenses -= parseFloat(august_negative_value[1]);
            } else {
                august_total_cost_of_sales_expenses += parseFloat(august);
            }

            var september = el.find('.september').html();
            var september_negative_value = september.match(/\(([^)]+)\)/);

            if (september_negative_value != null) {
                september_total_cost_of_sales_expenses -= parseFloat(september_negative_value[1]);
            } else {
                september_total_cost_of_sales_expenses += parseFloat(september);
            }

            var october = el.find('.october').html();
            var october_negative_value = october.match(/\(([^)]+)\)/);

            if (october_negative_value != null) {
                october_total_cost_of_sales_expenses -= parseFloat(october_negative_value[1]);
            } else {
                october_total_cost_of_sales_expenses += parseFloat(october);
            }

            var november = el.find('.november').html();
            var november_negative_value = november.match(/\(([^)]+)\)/);

            if (november_negative_value != null) {
                november_total_cost_of_sales_expenses -= parseFloat(november_negative_value[1]);
            } else {
                november_total_cost_of_sales_expenses += parseFloat(november);
            }

            var december = el.find('.december').html();
            var december_negative_value = december.match(/\(([^)]+)\)/);

            if (december_negative_value != null) {
                december_total_cost_of_sales_expenses -= parseFloat(december_negative_value[1]);
            } else {
                december_total_cost_of_sales_expenses += parseFloat(december);
            }

        });
        var january_gross_profit = january_contribution_margin - january_total_cost_of_sales_expenses;

        $('#cost_of_sales_expenses .january').html(january_total_cost_of_sales_expenses >= 0 ? january_total_cost_of_sales_expenses : '('+Math.abs(january_total_cost_of_sales_expenses)+')');

        $('#gross_profit .january').html(january_gross_profit >= 0 ? january_gross_profit : '('+Math.abs(january_gross_profit)+')');

        var february_gross_profit = february_contribution_margin - february_total_cost_of_sales_expenses;

        $('#cost_of_sales_expenses .february').html(february_total_cost_of_sales_expenses >= 0 ? february_total_cost_of_sales_expenses : '('+Math.abs(february_total_cost_of_sales_expenses)+')');

        $('#gross_profit .february').html(february_gross_profit >= 0 ? february_gross_profit : '('+Math.abs(february_gross_profit)+')');

        var march_gross_profit = march_contribution_margin - march_total_cost_of_sales_expenses;

        $('#cost_of_sales_expenses .march').html(march_total_cost_of_sales_expenses >= 0 ? march_total_cost_of_sales_expenses : '('+Math.abs(march_total_cost_of_sales_expenses)+')');

        $('#gross_profit .march').html(march_gross_profit >= 0 ? march_gross_profit : '('+Math.abs(march_gross_profit)+')');

        var april_gross_profit = april_contribution_margin - april_total_cost_of_sales_expenses;

        $('#cost_of_sales_expenses .april').html(april_total_cost_of_sales_expenses >= 0 ? april_total_cost_of_sales_expenses : '('+Math.abs(april_total_cost_of_sales_expenses)+')');

        $('#gross_profit .april').html(april_gross_profit >= 0 ? april_gross_profit : '('+Math.abs(april_gross_profit)+')');

        var may_gross_profit = may_contribution_margin - may_total_cost_of_sales_expenses;

        $('#cost_of_sales_expenses .may').html(may_total_cost_of_sales_expenses >= 0 ? may_total_cost_of_sales_expenses : '('+Math.abs(may_total_cost_of_sales_expenses)+')');

        $('#gross_profit .may').html(may_gross_profit >= 0 ? may_gross_profit : '('+Math.abs(may_gross_profit)+')');

        var june_gross_profit = june_contribution_margin - june_total_cost_of_sales_expenses;

        $('#cost_of_sales_expenses .june').html(june_total_cost_of_sales_expenses >= 0 ? june_total_cost_of_sales_expenses : '('+Math.abs(june_total_cost_of_sales_expenses)+')');

        $('#gross_profit .june').html(june_gross_profit >= 0 ? june_gross_profit : '('+Math.abs(june_gross_profit)+')');

        var july_gross_profit = july_contribution_margin - july_total_cost_of_sales_expenses;

        $('#cost_of_sales_expenses .july').html(july_total_cost_of_sales_expenses >= 0 ? july_total_cost_of_sales_expenses : '('+Math.abs(july_total_cost_of_sales_expenses)+')');

        $('#gross_profit .july').html(july_gross_profit >= 0 ? july_gross_profit : '('+Math.abs(july_gross_profit)+')');

        var august_gross_profit = august_contribution_margin - august_total_cost_of_sales_expenses;

        $('#cost_of_sales_expenses .august').html(august_total_cost_of_sales_expenses >= 0 ? august_total_cost_of_sales_expenses : '('+Math.abs(august_total_cost_of_sales_expenses)+')');

        $('#gross_profit .august').html(august_gross_profit >= 0 ? august_gross_profit : '('+Math.abs(august_gross_profit)+')');

        var september_gross_profit = september_contribution_margin - september_total_cost_of_sales_expenses;

        $('#cost_of_sales_expenses .september').html(september_total_cost_of_sales_expenses >= 0 ? september_total_cost_of_sales_expenses : '('+Math.abs(september_total_cost_of_sales_expenses)+')');

        $('#gross_profit .september').html(september_gross_profit >= 0 ? september_gross_profit : '('+Math.abs(september_gross_profit)+')');

        var october_gross_profit = october_contribution_margin - october_total_cost_of_sales_expenses;

        $('#cost_of_sales_expenses .october').html(october_total_cost_of_sales_expenses >= 0 ? october_total_cost_of_sales_expenses : '('+Math.abs(october_total_cost_of_sales_expenses)+')');

        $('#gross_profit .october').html(october_gross_profit >= 0 ? october_gross_profit : '('+Math.abs(october_gross_profit)+')');

        var november_gross_profit = november_contribution_margin - november_total_cost_of_sales_expenses;

        $('#cost_of_sales_expenses .november').html(november_total_cost_of_sales_expenses >= 0 ? november_total_cost_of_sales_expenses : '('+Math.abs(november_total_cost_of_sales_expenses)+')');

        $('#gross_profit .november').html(november_gross_profit >= 0 ? november_gross_profit : '('+Math.abs(november_gross_profit)+')');

        var december_gross_profit = december_contribution_margin - december_total_cost_of_sales_expenses;

        $('#cost_of_sales_expenses .december').html(december_total_cost_of_sales_expenses >= 0 ? december_total_cost_of_sales_expenses : '('+Math.abs(december_total_cost_of_sales_expenses)+')');

        $('#gross_profit .december').html(december_gross_profit >= 0 ? december_gross_profit : '('+Math.abs(december_gross_profit)+')');

        // Operating profit before tax
        var january_total_expenses = 0;
        var february_total_expenses = 0;
        var march_total_expenses = 0;
        var april_total_expenses = 0;
        var may_total_expenses = 0;
        var june_total_expenses = 0;
        var july_total_expenses = 0;
        var august_total_expenses = 0;
        var september_total_expenses = 0;
        var october_total_expenses = 0;
        var november_total_expenses = 0;
        var december_total_expenses = 0;

        $('#accounts_table .expenses').each(function() {
            var el = $(this);

            var january = el.find('.january').html();
            var january_negative_value = january.match(/\(([^)]+)\)/);

            if (january_negative_value != null) {
                january_total_expenses -= parseFloat(january_negative_value[1]);
            } else {
                january_total_expenses += parseFloat(january);
            }

            var february = el.find('.february').html();
            var february_negative_value = february.match(/\(([^)]+)\)/);

            if (february_negative_value != null) {
                february_total_expenses -= parseFloat(february_negative_value[1]);
            } else {
                february_total_expenses += parseFloat(february);
            }

            var march = el.find('.march').html();
            var march_negative_value = march.match(/\(([^)]+)\)/);

            if (march_negative_value != null) {
                march_total_expenses -= parseFloat(march_negative_value[1]);
            } else {
                march_total_expenses += parseFloat(march);
            }

            var april = el.find('.april').html();
            var april_negative_value = april.match(/\(([^)]+)\)/);

            if (april_negative_value != null) {
                april_total_expenses -= parseFloat(april_negative_value[1]);
            } else {
                april_total_expenses += parseFloat(april);
            }

            var may = el.find('.may').html();
            var may_negative_value = may.match(/\(([^)]+)\)/);

            if (may_negative_value != null) {
                may_total_expenses -= parseFloat(may_negative_value[1]);
            } else {
                may_total_expenses += parseFloat(may);
            }

            var june = el.find('.june').html();
            var june_negative_value = june.match(/\(([^)]+)\)/);

            if (june_negative_value != null) {
                june_total_expenses -= parseFloat(june_negative_value[1]);
            } else {
                june_total_expenses += parseFloat(june);
            }

            var july = el.find('.july').html();
            var july_negative_value = july.match(/\(([^)]+)\)/);

            if (july_negative_value != null) {
                july_total_expenses -= parseFloat(july_negative_value[1]);
            } else {
                july_total_expenses += parseFloat(july);
            }

            var august = el.find('.august').html();
            var august_negative_value = august.match(/\(([^)]+)\)/);

            if (august_negative_value != null) {
                august_total_expenses -= parseFloat(august_negative_value[1]);
            } else {
                august_total_expenses += parseFloat(august);
            }

            var september = el.find('.september').html();
            var september_negative_value = september.match(/\(([^)]+)\)/);

            if (september_negative_value != null) {
                september_total_expenses -= parseFloat(september_negative_value[1]);
            } else {
                september_total_expenses += parseFloat(september);
            }

            var october = el.find('.october').html();
            var october_negative_value = october.match(/\(([^)]+)\)/);

            if (october_negative_value != null) {
                october_total_expenses -= parseFloat(october_negative_value[1]);
            } else {
                october_total_expenses += parseFloat(october);
            }

            var november = el.find('.november').html();
            var november_negative_value = november.match(/\(([^)]+)\)/);

            if (november_negative_value != null) {
                november_total_expenses -= parseFloat(november_negative_value[1]);
            } else {
                november_total_expenses += parseFloat(november);
            }

            var december = el.find('.december').html();
            var december_negative_value = december.match(/\(([^)]+)\)/);

            if (december_negative_value != null) {
                december_total_expenses -= parseFloat(december_negative_value[1]);
            } else {
                december_total_expenses += parseFloat(december);
            }

        });

        var january_operating_profit_bftax = january_gross_profit - january_total_expenses;

        $('#expenses .january').html(january_total_expenses >= 0 ? january_total_expenses : '('+Math.abs(january_total_expenses)+')');

        $('#operating_profit_bftax .january').html(january_operating_profit_bftax >= 0 ? january_operating_profit_bftax : '('+Math.abs(january_operating_profit_bftax)+')');

        var february_operating_profit_bftax = february_gross_profit - february_total_expenses;

        $('#expenses .february').html(february_total_expenses >= 0 ? february_total_expenses : '('+Math.abs(february_total_expenses)+')');

        $('#operating_profit_bftax .february').html(february_operating_profit_bftax >= 0 ? february_operating_profit_bftax : '('+Math.abs(february_operating_profit_bftax)+')');

        var march_operating_profit_bftax = march_gross_profit - march_total_expenses;

        $('#expenses .march').html(march_total_expenses >= 0 ? march_total_expenses : '('+Math.abs(march_total_expenses)+')');

        $('#operating_profit_bftax .march').html(march_operating_profit_bftax >= 0 ? march_operating_profit_bftax : '('+Math.abs(march_operating_profit_bftax)+')');

        var april_operating_profit_bftax = april_gross_profit - april_total_expenses;

        $('#expenses .april').html(april_total_expenses >= 0 ? april_total_expenses : '('+Math.abs(april_total_expenses)+')');

        $('#operating_profit_bftax .april').html(april_operating_profit_bftax >= 0 ? april_operating_profit_bftax : '('+Math.abs(april_operating_profit_bftax)+')');

        var may_operating_profit_bftax = may_gross_profit - may_total_expenses;

        $('#expenses .may').html(may_total_expenses >= 0 ? may_total_expenses : '('+Math.abs(may_total_expenses)+')');

        $('#operating_profit_bftax .may').html(may_operating_profit_bftax >= 0 ? may_operating_profit_bftax : '('+Math.abs(may_operating_profit_bftax)+')');

        var june_operating_profit_bftax = june_gross_profit - june_total_expenses;

        $('#expenses .june').html(june_total_expenses >= 0 ? june_total_expenses : '('+Math.abs(june_total_expenses)+')');

        $('#operating_profit_bftax .june').html(june_operating_profit_bftax >= 0 ? june_operating_profit_bftax : '('+Math.abs(june_operating_profit_bftax)+')');

        var july_operating_profit_bftax = july_gross_profit - july_total_expenses;

        $('#expenses .july').html(july_total_expenses >= 0 ? july_total_expenses : '('+Math.abs(july_total_expenses)+')');

        $('#operating_profit_bftax .july').html(july_operating_profit_bftax >= 0 ? july_operating_profit_bftax : '('+Math.abs(july_operating_profit_bftax)+')');

        var august_operating_profit_bftax = august_gross_profit - august_total_expenses;

        $('#expenses .august').html(august_total_expenses >= 0 ? august_total_expenses : '('+Math.abs(august_total_expenses)+')');

        $('#operating_profit_bftax .august').html(august_operating_profit_bftax >= 0 ? august_operating_profit_bftax : '('+Math.abs(august_operating_profit_bftax)+')');

        var september_operating_profit_bftax = september_gross_profit - september_total_expenses;

        $('#expenses .september').html(september_total_expenses >= 0 ? september_total_expenses : '('+Math.abs(september_total_expenses)+')');

        $('#operating_profit_bftax .september').html(september_operating_profit_bftax >= 0 ? september_operating_profit_bftax : '('+Math.abs(september_operating_profit_bftax)+')');

        var october_operating_profit_bftax = october_gross_profit - october_total_expenses;

        $('#expenses .october').html(october_total_expenses >= 0 ? october_total_expenses : '('+Math.abs(october_total_expenses)+')');

        $('#operating_profit_bftax .october').html(october_operating_profit_bftax >= 0 ? october_operating_profit_bftax : '('+Math.abs(october_operating_profit_bftax)+')');

        var november_operating_profit_bftax = november_gross_profit - november_total_expenses;

        $('#expenses .november').html(november_total_expenses >= 0 ? november_total_expenses : '('+Math.abs(november_total_expenses)+')');

        $('#operating_profit_bftax .november').html(november_operating_profit_bftax >= 0 ? november_operating_profit_bftax : '('+Math.abs(november_operating_profit_bftax)+')');

        var december_operating_profit_bftax = december_gross_profit - december_total_expenses;

        $('#expenses .december').html(december_total_expenses >= 0 ? december_total_expenses : '('+Math.abs(december_total_expenses)+')');

        $('#operating_profit_bftax .december').html(december_operating_profit_bftax >= 0 ? december_operating_profit_bftax : '('+Math.abs(december_operating_profit_bftax)+')');

        // Operating profit after tax
        var january_total_expenses_tax = 0;
        var february_total_expenses_tax = 0;
        var march_total_expenses_tax = 0;
        var april_total_expenses_tax = 0;
        var may_total_expenses_tax = 0;
        var june_total_expenses_tax = 0;
        var july_total_expenses_tax = 0;
        var august_total_expenses_tax = 0;
        var september_total_expenses_tax = 0;
        var october_total_expenses_tax = 0;
        var november_total_expenses_tax = 0;
        var december_total_expenses_tax = 0;

        $('#accounts_table .expenses_tax').each(function() {
            var el = $(this);

            var january = el.find('.january').html();
            var january_negative_value = january.match(/\(([^)]+)\)/);

            if (january_negative_value != null) {
                january_total_expenses_tax -= parseFloat(january_negative_value[1]);
            } else {
                january_total_expenses_tax += parseFloat(january);
            }

            var february = el.find('.february').html();
            var february_negative_value = february.match(/\(([^)]+)\)/);

            if (february_negative_value != null) {
                february_total_expenses_tax -= parseFloat(february_negative_value[1]);
            } else {
                february_total_expenses_tax += parseFloat(february);
            }

            var march = el.find('.march').html();
            var march_negative_value = march.match(/\(([^)]+)\)/);

            if (march_negative_value != null) {
                march_total_expenses_tax -= parseFloat(march_negative_value[1]);
            } else {
                march_total_expenses_tax += parseFloat(march);
            }

            var april = el.find('.april').html();
            var april_negative_value = april.match(/\(([^)]+)\)/);

            if (april_negative_value != null) {
                april_total_expenses_tax -= parseFloat(april_negative_value[1]);
            } else {
                april_total_expenses_tax += parseFloat(april);
            }

            var may = el.find('.may').html();
            var may_negative_value = may.match(/\(([^)]+)\)/);

            if (may_negative_value != null) {
                may_total_expenses_tax -= parseFloat(may_negative_value[1]);
            } else {
                may_total_expenses_tax += parseFloat(may);
            }

            var june = el.find('.june').html();
            var june_negative_value = june.match(/\(([^)]+)\)/);

            if (june_negative_value != null) {
                june_total_expenses_tax -= parseFloat(june_negative_value[1]);
            } else {
                june_total_expenses_tax += parseFloat(june);
            }

            var july = el.find('.july').html();
            var july_negative_value = july.match(/\(([^)]+)\)/);

            if (july_negative_value != null) {
                july_total_expenses_tax -= parseFloat(july_negative_value[1]);
            } else {
                july_total_expenses_tax += parseFloat(july);
            }

            var august = el.find('.august').html();
            var august_negative_value = august.match(/\(([^)]+)\)/);

            if (august_negative_value != null) {
                august_total_expenses_tax -= parseFloat(august_negative_value[1]);
            } else {
                august_total_expenses_tax += parseFloat(august);
            }

            var september = el.find('.september').html();
            var september_negative_value = september.match(/\(([^)]+)\)/);

            if (september_negative_value != null) {
                september_total_expenses_tax -= parseFloat(september_negative_value[1]);
            } else {
                september_total_expenses_tax += parseFloat(september);
            }

            var october = el.find('.october').html();
            var october_negative_value = october.match(/\(([^)]+)\)/);

            if (october_negative_value != null) {
                october_total_expenses_tax -= parseFloat(october_negative_value[1]);
            } else {
                october_total_expenses_tax += parseFloat(october);
            }

            var november = el.find('.november').html();
            var november_negative_value = november.match(/\(([^)]+)\)/);

            if (november_negative_value != null) {
                november_total_expenses_tax -= parseFloat(november_negative_value[1]);
            } else {
                november_total_expenses_tax += parseFloat(november);
            }

            var december = el.find('.december').html();
            var december_negative_value = december.match(/\(([^)]+)\)/);

            if (december_negative_value != null) {
                december_total_expenses_tax -= parseFloat(december_negative_value[1]);
            } else {
                december_total_expenses_tax += parseFloat(december);
            }

        });

        var january_operating_profit_aftax = january_operating_profit_bftax - january_total_expenses_tax;

        $('#expenses_tax .january').html(january_total_expenses_tax >= 0 ? january_total_expenses_tax : '('+Math.abs(january_total_expenses_tax)+')');

        $('#operating_profit_aftax .january').html(january_operating_profit_aftax >= 0 ? january_operating_profit_aftax : '('+Math.abs(january_operating_profit_aftax)+')');

        var february_operating_profit_aftax = february_operating_profit_bftax - february_total_expenses_tax;

        $('#expenses_tax .february').html(february_total_expenses_tax >= 0 ? february_total_expenses_tax : '('+Math.abs(february_total_expenses_tax)+')');

        $('#operating_profit_aftax .february').html(february_operating_profit_aftax >= 0 ? february_operating_profit_aftax : '('+Math.abs(february_operating_profit_aftax)+')');

        var march_operating_profit_aftax = march_operating_profit_bftax - march_total_expenses_tax;

        $('#expenses_tax .march').html(march_total_expenses_tax >= 0 ? march_total_expenses_tax : '('+Math.abs(march_total_expenses_tax)+')');

        $('#operating_profit_aftax .march').html(march_operating_profit_aftax >= 0 ? march_operating_profit_aftax : '('+Math.abs(march_operating_profit_aftax)+')');

        var april_operating_profit_aftax = april_operating_profit_bftax - april_total_expenses_tax;

        $('#expenses_tax .april').html(april_total_expenses_tax >= 0 ? april_total_expenses_tax : '('+Math.abs(april_total_expenses_tax)+')');

        $('#operating_profit_aftax .april').html(april_operating_profit_aftax >= 0 ? april_operating_profit_aftax : '('+Math.abs(april_operating_profit_aftax)+')');

        var may_operating_profit_aftax = may_operating_profit_bftax - may_total_expenses_tax;

        $('#expenses_tax .may').html(may_total_expenses_tax >= 0 ? may_total_expenses_tax : '('+Math.abs(may_total_expenses_tax)+')');

        $('#operating_profit_aftax .may').html(may_operating_profit_aftax >= 0 ? may_operating_profit_aftax : '('+Math.abs(may_operating_profit_aftax)+')');

        var june_operating_profit_aftax = june_operating_profit_bftax - june_total_expenses_tax;

        $('#expenses_tax .june').html(june_total_expenses_tax >= 0 ? june_total_expenses_tax : '('+Math.abs(june_total_expenses_tax)+')');

        $('#operating_profit_aftax .june').html(june_operating_profit_aftax >= 0 ? june_operating_profit_aftax : '('+Math.abs(june_operating_profit_aftax)+')');

        var july_operating_profit_aftax = july_operating_profit_bftax - july_total_expenses_tax;

        $('#expenses_tax .july').html(july_total_expenses_tax >= 0 ? july_total_expenses_tax : '('+Math.abs(july_total_expenses_tax)+')');

        $('#operating_profit_aftax .july').html(july_operating_profit_aftax >= 0 ? july_operating_profit_aftax : '('+Math.abs(july_operating_profit_aftax)+')');

        var august_operating_profit_aftax = august_operating_profit_bftax - august_total_expenses_tax;

        $('#expenses_tax .august').html(august_total_expenses_tax >= 0 ? august_total_expenses_tax : '('+Math.abs(august_total_expenses_tax)+')');

        $('#operating_profit_aftax .august').html(august_operating_profit_aftax >= 0 ? august_operating_profit_aftax : '('+Math.abs(august_operating_profit_aftax)+')');

        var september_operating_profit_aftax = september_operating_profit_bftax - september_total_expenses_tax;

        $('#expenses_tax .september').html(september_total_expenses_tax >= 0 ? september_total_expenses_tax : '('+Math.abs(september_total_expenses_tax)+')');

        $('#operating_profit_aftax .september').html(september_operating_profit_aftax >= 0 ? september_operating_profit_aftax : '('+Math.abs(september_operating_profit_aftax)+')');

        var october_operating_profit_aftax = october_operating_profit_bftax - october_total_expenses_tax;

        $('#expenses_tax .october').html(october_total_expenses_tax >= 0 ? october_total_expenses_tax : '('+Math.abs(october_total_expenses_tax)+')');

        $('#operating_profit_aftax .october').html(october_operating_profit_aftax >= 0 ? october_operating_profit_aftax : '('+Math.abs(october_operating_profit_aftax)+')');

        var november_operating_profit_aftax = november_operating_profit_bftax - november_total_expenses_tax;

        $('#expenses_tax .november').html(november_total_expenses_tax >= 0 ? november_total_expenses_tax : '('+Math.abs(november_total_expenses_tax)+')');

        $('#operating_profit_aftax .november').html(november_operating_profit_aftax >= 0 ? november_operating_profit_aftax : '('+Math.abs(november_operating_profit_aftax)+')');

        var december_operating_profit_aftax = december_operating_profit_bftax - december_total_expenses_tax;

        $('#expenses_tax .december').html(december_total_expenses_tax >= 0 ? december_total_expenses_tax : '('+Math.abs(december_total_expenses_tax)+')');

        $('#operating_profit_aftax .december').html(december_operating_profit_aftax >= 0 ? december_operating_profit_aftax : '('+Math.abs(december_operating_profit_aftax)+')');

        // Net profit
        var january_total_other_income = 0;
        var february_total_other_income = 0;
        var march_total_other_income = 0;
        var april_total_other_income = 0;
        var may_total_other_income = 0;
        var june_total_other_income = 0;
        var july_total_other_income = 0;
        var august_total_other_income = 0;
        var september_total_other_income = 0;
        var october_total_other_income = 0;
        var november_total_other_income = 0;
        var december_total_other_income = 0;

        $('#accounts_table .other_income').each(function() {
            var el = $(this);

            var january = el.find('.january').html();
            var january_negative_value = january.match(/\(([^)]+)\)/);

            if (january_negative_value != null) {
                january_total_other_income -= parseFloat(january_negative_value[1]);
            } else {
                january_total_other_income += parseFloat(january);
            }

            var february = el.find('.february').html();
            var february_negative_value = february.match(/\(([^)]+)\)/);

            if (february_negative_value != null) {
                february_total_other_income -= parseFloat(february_negative_value[1]);
            } else {
                february_total_other_income += parseFloat(february);
            }

            var march = el.find('.march').html();
            var march_negative_value = march.match(/\(([^)]+)\)/);

            if (march_negative_value != null) {
                march_total_other_income -= parseFloat(march_negative_value[1]);
            } else {
                march_total_other_income += parseFloat(march);
            }

            var april = el.find('.april').html();
            var april_negative_value = april.match(/\(([^)]+)\)/);

            if (april_negative_value != null) {
                april_total_other_income -= parseFloat(april_negative_value[1]);
            } else {
                april_total_other_income += parseFloat(april);
            }

            var may = el.find('.may').html();
            var may_negative_value = may.match(/\(([^)]+)\)/);

            if (may_negative_value != null) {
                may_total_other_income -= parseFloat(may_negative_value[1]);
            } else {
                may_total_other_income += parseFloat(may);
            }

            var june = el.find('.june').html();
            var june_negative_value = june.match(/\(([^)]+)\)/);

            if (june_negative_value != null) {
                june_total_other_income -= parseFloat(june_negative_value[1]);
            } else {
                june_total_other_income += parseFloat(june);
            }

            var july = el.find('.july').html();
            var july_negative_value = july.match(/\(([^)]+)\)/);

            if (july_negative_value != null) {
                july_total_other_income -= parseFloat(july_negative_value[1]);
            } else {
                july_total_other_income += parseFloat(july);
            }

            var august = el.find('.august').html();
            var august_negative_value = august.match(/\(([^)]+)\)/);

            if (august_negative_value != null) {
                august_total_other_income -= parseFloat(august_negative_value[1]);
            } else {
                august_total_other_income += parseFloat(august);
            }

            var september = el.find('.september').html();
            var september_negative_value = september.match(/\(([^)]+)\)/);

            if (september_negative_value != null) {
                september_total_other_income -= parseFloat(september_negative_value[1]);
            } else {
                september_total_other_income += parseFloat(september);
            }

            var october = el.find('.october').html();
            var october_negative_value = october.match(/\(([^)]+)\)/);

            if (october_negative_value != null) {
                october_total_other_income -= parseFloat(october_negative_value[1]);
            } else {
                october_total_other_income += parseFloat(october);
            }

            var november = el.find('.november').html();
            var november_negative_value = november.match(/\(([^)]+)\)/);

            if (november_negative_value != null) {
                november_total_other_income -= parseFloat(november_negative_value[1]);
            } else {
                november_total_other_income += parseFloat(november);
            }

            var december = el.find('.december').html();
            var december_negative_value = december.match(/\(([^)]+)\)/);

            if (december_negative_value != null) {
                december_total_other_income -= parseFloat(december_negative_value[1]);
            } else {
                december_total_other_income += parseFloat(december);
            }

        });

        var january_net_profit = january_operating_profit_aftax + january_total_other_income;

        $('#other_income .january').html(january_total_other_income >= 0 ? january_total_other_income : '('+Math.abs(january_total_other_income)+')');

        $('#net_profit .january').html(january_net_profit >= 0 ? january_net_profit : '('+Math.abs(january_net_profit)+')');

        var february_net_profit = february_operating_profit_aftax + february_total_other_income;

        $('#other_income .february').html(february_total_other_income >= 0 ? february_total_other_income : '('+Math.abs(february_total_other_income)+')');

        $('#net_profit .february').html(february_net_profit >= 0 ? february_net_profit : '('+Math.abs(february_net_profit)+')');

        var march_net_profit = march_operating_profit_aftax + march_total_other_income;

        $('#other_income .march').html(march_total_other_income >= 0 ? march_total_other_income : '('+Math.abs(march_total_other_income)+')');

        $('#net_profit .march').html(march_net_profit >= 0 ? march_net_profit : '('+Math.abs(march_net_profit)+')');

        var april_net_profit = april_operating_profit_aftax + april_total_other_income;

        $('#other_income .april').html(april_total_other_income >= 0 ? april_total_other_income : '('+Math.abs(april_total_other_income)+')');

        $('#net_profit .april').html(april_net_profit >= 0 ? april_net_profit : '('+Math.abs(april_net_profit)+')');

        var may_net_profit = may_operating_profit_aftax + may_total_other_income;

        $('#other_income .may').html(may_total_other_income >= 0 ? may_total_other_income : '('+Math.abs(may_total_other_income)+')');

        $('#net_profit .may').html(may_net_profit >= 0 ? may_net_profit : '('+Math.abs(may_net_profit)+')');

        var june_net_profit = june_operating_profit_aftax + june_total_other_income;

        $('#other_income .june').html(june_total_other_income >= 0 ? june_total_other_income : '('+Math.abs(june_total_other_income)+')');

        $('#net_profit .june').html(june_net_profit >= 0 ? june_net_profit : '('+Math.abs(june_net_profit)+')');

        var july_net_profit = july_operating_profit_aftax + july_total_other_income;

        $('#other_income .july').html(july_total_other_income >= 0 ? july_total_other_income : '('+Math.abs(july_total_other_income)+')');

        $('#net_profit .july').html(july_net_profit >= 0 ? july_net_profit : '('+Math.abs(july_net_profit)+')');

        var august_net_profit = august_operating_profit_aftax + august_total_other_income;

        $('#other_income .august').html(august_total_other_income >= 0 ? august_total_other_income : '('+Math.abs(august_total_other_income)+')');

        $('#net_profit .august').html(august_net_profit >= 0 ? august_net_profit : '('+Math.abs(august_net_profit)+')');

        var september_net_profit = september_operating_profit_aftax + september_total_other_income;

        $('#other_income .september').html(september_total_other_income >= 0 ? september_total_other_income : '('+Math.abs(september_total_other_income)+')');

        $('#net_profit .september').html(september_net_profit >= 0 ? september_net_profit : '('+Math.abs(september_net_profit)+')');

        var october_net_profit = october_operating_profit_aftax + october_total_other_income;

        $('#other_income .october').html(october_total_other_income >= 0 ? october_total_other_income : '('+Math.abs(october_total_other_income)+')');

        $('#net_profit .october').html(october_net_profit >= 0 ? october_net_profit : '('+Math.abs(october_net_profit)+')');

        var november_net_profit = november_operating_profit_aftax + november_total_other_income;

        $('#other_income .november').html(november_total_other_income >= 0 ? november_total_other_income : '('+Math.abs(november_total_other_income)+')');

        $('#net_profit .november').html(november_net_profit >= 0 ? november_net_profit : '('+Math.abs(november_net_profit)+')');

        var december_net_profit = december_operating_profit_aftax + december_total_other_income;

        $('#other_income .december').html(december_total_other_income >= 0 ? december_total_other_income : '('+Math.abs(december_total_other_income)+')');

        $('#net_profit .december').html(december_net_profit >= 0 ? december_net_profit : '('+Math.abs(december_net_profit)+')');

        $('#accounts_table tr').each(function() {
            var el = $(this);

            if (el.data('level') == 3) {
                var father_id = el.data('fatherid');

                var father_amount_january = $('.'+father_id).find('.january').html();
                var father_amount_january_negative_value = father_amount_january.match(/\(([^)]+)\)/);
                if (father_amount_january_negative_value != null) {
                    father_amount_january = -parseFloat(father_amount_january_negative_value[1]);
                } else {
                    father_amount_january = parseFloat(father_amount_january);
                }

                var january_amount = el.find('.january').html();
                var january_negative_value = january_amount.match(/\(([^)]+)\)/);

                if (january_negative_value != null) {
                    father_amount_january -= parseFloat(january_negative_value[1]);
                } else {
                    father_amount_january += parseFloat(january_amount);
                }

                $('.'+father_id).find('.january').html(father_amount_january >= 0 ? father_amount_january : '('+Math.abs(father_amount_january)+')');

                var father_id = el.data('fatherid');

                var father_amount_february = $('.'+father_id).find('.february').html();
                var father_amount_february_negative_value = father_amount_february.match(/\(([^)]+)\)/);
                if (father_amount_february_negative_value != null) {
                    father_amount_february = -parseFloat(father_amount_february_negative_value[1]);
                } else {
                    father_amount_february = parseFloat(father_amount_february);
                }

                var february_amount = el.find('.february').html();
                var february_negative_value = february_amount.match(/\(([^)]+)\)/);

                if (february_negative_value != null) {
                    father_amount_february -= parseFloat(february_negative_value[1]);
                } else {
                    father_amount_february += parseFloat(february_amount);
                }

                $('.'+father_id).find('.february').html(father_amount_february >= 0 ? father_amount_february : '('+Math.abs(father_amount_february)+')');

                var father_id = el.data('fatherid');

                var father_amount_march = $('.'+father_id).find('.march').html();
                var father_amount_march_negative_value = father_amount_march.match(/\(([^)]+)\)/);
                if (father_amount_march_negative_value != null) {
                    father_amount_march = -parseFloat(father_amount_march_negative_value[1]);
                } else {
                    father_amount_march = parseFloat(father_amount_march);
                }

                var march_amount = el.find('.march').html();
                var march_negative_value = march_amount.match(/\(([^)]+)\)/);

                if (march_negative_value != null) {
                    father_amount_march -= parseFloat(march_negative_value[1]);
                } else {
                    father_amount_march += parseFloat(march_amount);
                }

                $('.'+father_id).find('.march').html(father_amount_march >= 0 ? father_amount_march : '('+Math.abs(father_amount_march)+')');

                var father_id = el.data('fatherid');

                var father_amount_april = $('.'+father_id).find('.april').html();
                var father_amount_april_negative_value = father_amount_april.match(/\(([^)]+)\)/);
                if (father_amount_april_negative_value != null) {
                    father_amount_april = -parseFloat(father_amount_april_negative_value[1]);
                } else {
                    father_amount_april = parseFloat(father_amount_april);
                }

                var april_amount = el.find('.april').html();
                var april_negative_value = april_amount.match(/\(([^)]+)\)/);

                if (april_negative_value != null) {
                    father_amount_april -= parseFloat(april_negative_value[1]);
                } else {
                    father_amount_april += parseFloat(april_amount);
                }

                $('.'+father_id).find('.april').html(father_amount_april >= 0 ? father_amount_april : '('+Math.abs(father_amount_april)+')');

                var father_id = el.data('fatherid');

                var father_amount_may = $('.'+father_id).find('.may').html();
                var father_amount_may_negative_value = father_amount_may.match(/\(([^)]+)\)/);
                if (father_amount_may_negative_value != null) {
                    father_amount_may = -parseFloat(father_amount_may_negative_value[1]);
                } else {
                    father_amount_may = parseFloat(father_amount_may);
                }

                var may_amount = el.find('.may').html();
                var may_negative_value = may_amount.match(/\(([^)]+)\)/);

                if (may_negative_value != null) {
                    father_amount_may -= parseFloat(may_negative_value[1]);
                } else {
                    father_amount_may += parseFloat(may_amount);
                }

                $('.'+father_id).find('.may').html(father_amount_may >= 0 ? father_amount_may : '('+Math.abs(father_amount_may)+')');

                var father_id = el.data('fatherid');

                var father_amount_june = $('.'+father_id).find('.june').html();
                var father_amount_june_negative_value = father_amount_june.match(/\(([^)]+)\)/);
                if (father_amount_june_negative_value != null) {
                    father_amount_june = -parseFloat(father_amount_june_negative_value[1]);
                } else {
                    father_amount_june = parseFloat(father_amount_june);
                }

                var june_amount = el.find('.june').html();
                var june_negative_value = june_amount.match(/\(([^)]+)\)/);

                if (june_negative_value != null) {
                    father_amount_june -= parseFloat(june_negative_value[1]);
                } else {
                    father_amount_june += parseFloat(june_amount);
                }

                $('.'+father_id).find('.june').html(father_amount_june >= 0 ? father_amount_june : '('+Math.abs(father_amount_june)+')');

                var father_id = el.data('fatherid');

                var father_amount_july = $('.'+father_id).find('.july').html();
                var father_amount_july_negative_value = father_amount_july.match(/\(([^)]+)\)/);
                if (father_amount_july_negative_value != null) {
                    father_amount_july = -parseFloat(father_amount_july_negative_value[1]);
                } else {
                    father_amount_july = parseFloat(father_amount_july);
                }

                var july_amount = el.find('.july').html();
                var july_negative_value = july_amount.match(/\(([^)]+)\)/);

                if (july_negative_value != null) {
                    father_amount_july -= parseFloat(july_negative_value[1]);
                } else {
                    father_amount_july += parseFloat(july_amount);
                }

                $('.'+father_id).find('.july').html(father_amount_july >= 0 ? father_amount_july : '('+Math.abs(father_amount_july)+')');

                var father_id = el.data('fatherid');

                var father_amount_august = $('.'+father_id).find('.august').html();
                var father_amount_august_negative_value = father_amount_august.match(/\(([^)]+)\)/);
                if (father_amount_august_negative_value != null) {
                    father_amount_august = -parseFloat(father_amount_august_negative_value[1]);
                } else {
                    father_amount_august = parseFloat(father_amount_august);
                }

                var august_amount = el.find('.august').html();
                var august_negative_value = august_amount.match(/\(([^)]+)\)/);

                if (august_negative_value != null) {
                    father_amount_august -= parseFloat(august_negative_value[1]);
                } else {
                    father_amount_august += parseFloat(august_amount);
                }

                $('.'+father_id).find('.august').html(father_amount_august >= 0 ? father_amount_august : '('+Math.abs(father_amount_august)+')');

                var father_id = el.data('fatherid');

                var father_amount_september = $('.'+father_id).find('.september').html();
                var father_amount_september_negative_value = father_amount_september.match(/\(([^)]+)\)/);
                if (father_amount_september_negative_value != null) {
                    father_amount_september = -parseFloat(father_amount_september_negative_value[1]);
                } else {
                    father_amount_september = parseFloat(father_amount_september);
                }

                var september_amount = el.find('.september').html();
                var september_negative_value = september_amount.match(/\(([^)]+)\)/);

                if (september_negative_value != null) {
                    father_amount_september -= parseFloat(september_negative_value[1]);
                } else {
                    father_amount_september += parseFloat(september_amount);
                }

                $('.'+father_id).find('.september').html(father_amount_september >= 0 ? father_amount_september : '('+Math.abs(father_amount_september)+')');

                var father_id = el.data('fatherid');

                var father_amount_october = $('.'+father_id).find('.october').html();
                var father_amount_october_negative_value = father_amount_october.match(/\(([^)]+)\)/);
                if (father_amount_october_negative_value != null) {
                    father_amount_october = -parseFloat(father_amount_october_negative_value[1]);
                } else {
                    father_amount_october = parseFloat(father_amount_october);
                }

                var october_amount = el.find('.october').html();
                var october_negative_value = october_amount.match(/\(([^)]+)\)/);

                if (october_negative_value != null) {
                    father_amount_october -= parseFloat(october_negative_value[1]);
                } else {
                    father_amount_october += parseFloat(october_amount);
                }

                $('.'+father_id).find('.october').html(father_amount_october >= 0 ? father_amount_october : '('+Math.abs(father_amount_october)+')');

                var father_id = el.data('fatherid');

                var father_amount_november = $('.'+father_id).find('.november').html();
                var father_amount_november_negative_value = father_amount_november.match(/\(([^)]+)\)/);
                if (father_amount_november_negative_value != null) {
                    father_amount_november = -parseFloat(father_amount_november_negative_value[1]);
                } else {
                    father_amount_november = parseFloat(father_amount_november);
                }

                var november_amount = el.find('.november').html();
                var november_negative_value = november_amount.match(/\(([^)]+)\)/);

                if (november_negative_value != null) {
                    father_amount_november -= parseFloat(november_negative_value[1]);
                } else {
                    father_amount_november += parseFloat(november_amount);
                }

                $('.'+father_id).find('.november').html(father_amount_november >= 0 ? father_amount_november : '('+Math.abs(father_amount_november)+')');

                var father_id = el.data('fatherid');

                var father_amount_december = $('.'+father_id).find('.december').html();
                var father_amount_december_negative_value = father_amount_december.match(/\(([^)]+)\)/);
                if (father_amount_december_negative_value != null) {
                    father_amount_december = -parseFloat(father_amount_december_negative_value[1]);
                } else {
                    father_amount_december = parseFloat(father_amount_december);
                }

                var december_amount = el.find('.december').html();
                var december_negative_value = december_amount.match(/\(([^)]+)\)/);

                if (december_negative_value != null) {
                    father_amount_december -= parseFloat(december_negative_value[1]);
                } else {
                    father_amount_december += parseFloat(december_amount);
                }

                $('.'+father_id).find('.december').html(father_amount_december >= 0 ? father_amount_december : '('+Math.abs(father_amount_december)+')');
            }

        });

        $('#accounts_table tr').each(function() {
            var el = $(this);

            if (el.data('level') == 2) {
                var father_id = el.data('fatherid');

                var father_amount_january = $('.'+father_id).find('.january').html();
                var father_amount_january_negative_value = father_amount_january.match(/\(([^)]+)\)/);
                if (father_amount_january_negative_value != null) {
                    father_amount_january = -parseFloat(father_amount_january_negative_value[1]);
                } else {
                    father_amount_january = parseFloat(father_amount_january);
                }

                var january_amount = el.find('.january').html();
                var january_negative_value = january_amount.match(/\(([^)]+)\)/);

                if (january_negative_value != null) {
                    father_amount_january -= parseFloat(january_negative_value[1]);
                } else {
                    father_amount_january += parseFloat(january_amount);
                }

                $('.'+father_id).find('.january').html(father_amount_january >= 0 ? father_amount_january : '('+Math.abs(father_amount_january)+')');

                var father_id = el.data('fatherid');

                var father_amount_february = $('.'+father_id).find('.february').html();
                var father_amount_february_negative_value = father_amount_february.match(/\(([^)]+)\)/);
                if (father_amount_february_negative_value != null) {
                    father_amount_february = -parseFloat(father_amount_february_negative_value[1]);
                } else {
                    father_amount_february = parseFloat(father_amount_february);
                }

                var february_amount = el.find('.february').html();
                var february_negative_value = february_amount.match(/\(([^)]+)\)/);

                if (february_negative_value != null) {
                    father_amount_february -= parseFloat(february_negative_value[1]);
                } else {
                    father_amount_february += parseFloat(february_amount);
                }

                $('.'+father_id).find('.february').html(father_amount_february >= 0 ? father_amount_february : '('+Math.abs(father_amount_february)+')');

                var father_id = el.data('fatherid');

                var father_amount_march = $('.'+father_id).find('.march').html();
                var father_amount_march_negative_value = father_amount_march.match(/\(([^)]+)\)/);
                if (father_amount_march_negative_value != null) {
                    father_amount_march = -parseFloat(father_amount_march_negative_value[1]);
                } else {
                    father_amount_march = parseFloat(father_amount_march);
                }

                var march_amount = el.find('.march').html();
                var march_negative_value = march_amount.match(/\(([^)]+)\)/);

                if (march_negative_value != null) {
                    father_amount_march -= parseFloat(march_negative_value[1]);
                } else {
                    father_amount_march += parseFloat(march_amount);
                }

                $('.'+father_id).find('.march').html(father_amount_march >= 0 ? father_amount_march : '('+Math.abs(father_amount_march)+')');

                var father_id = el.data('fatherid');

                var father_amount_april = $('.'+father_id).find('.april').html();
                var father_amount_april_negative_value = father_amount_april.match(/\(([^)]+)\)/);
                if (father_amount_april_negative_value != null) {
                    father_amount_april = -parseFloat(father_amount_april_negative_value[1]);
                } else {
                    father_amount_april = parseFloat(father_amount_april);
                }

                var april_amount = el.find('.april').html();
                var april_negative_value = april_amount.match(/\(([^)]+)\)/);

                if (april_negative_value != null) {
                    father_amount_april -= parseFloat(april_negative_value[1]);
                } else {
                    father_amount_april += parseFloat(april_amount);
                }

                $('.'+father_id).find('.april').html(father_amount_april >= 0 ? father_amount_april : '('+Math.abs(father_amount_april)+')');

                var father_id = el.data('fatherid');

                var father_amount_may = $('.'+father_id).find('.may').html();
                var father_amount_may_negative_value = father_amount_may.match(/\(([^)]+)\)/);
                if (father_amount_may_negative_value != null) {
                    father_amount_may = -parseFloat(father_amount_may_negative_value[1]);
                } else {
                    father_amount_may = parseFloat(father_amount_may);
                }

                var may_amount = el.find('.may').html();
                var may_negative_value = may_amount.match(/\(([^)]+)\)/);

                if (may_negative_value != null) {
                    father_amount_may -= parseFloat(may_negative_value[1]);
                } else {
                    father_amount_may += parseFloat(may_amount);
                }

                $('.'+father_id).find('.may').html(father_amount_may >= 0 ? father_amount_may : '('+Math.abs(father_amount_may)+')');

                var father_id = el.data('fatherid');

                var father_amount_june = $('.'+father_id).find('.june').html();
                var father_amount_june_negative_value = father_amount_june.match(/\(([^)]+)\)/);
                if (father_amount_june_negative_value != null) {
                    father_amount_june = -parseFloat(father_amount_june_negative_value[1]);
                } else {
                    father_amount_june = parseFloat(father_amount_june);
                }

                var june_amount = el.find('.june').html();
                var june_negative_value = june_amount.match(/\(([^)]+)\)/);

                if (june_negative_value != null) {
                    father_amount_june -= parseFloat(june_negative_value[1]);
                } else {
                    father_amount_june += parseFloat(june_amount);
                }

                $('.'+father_id).find('.june').html(father_amount_june >= 0 ? father_amount_june : '('+Math.abs(father_amount_june)+')');

                var father_id = el.data('fatherid');

                var father_amount_july = $('.'+father_id).find('.july').html();
                var father_amount_july_negative_value = father_amount_july.match(/\(([^)]+)\)/);
                if (father_amount_july_negative_value != null) {
                    father_amount_july = -parseFloat(father_amount_july_negative_value[1]);
                } else {
                    father_amount_july = parseFloat(father_amount_july);
                }

                var july_amount = el.find('.july').html();
                var july_negative_value = july_amount.match(/\(([^)]+)\)/);

                if (july_negative_value != null) {
                    father_amount_july -= parseFloat(july_negative_value[1]);
                } else {
                    father_amount_july += parseFloat(july_amount);
                }

                $('.'+father_id).find('.july').html(father_amount_july >= 0 ? father_amount_july : '('+Math.abs(father_amount_july)+')');

                var father_id = el.data('fatherid');

                var father_amount_august = $('.'+father_id).find('.august').html();
                var father_amount_august_negative_value = father_amount_august.match(/\(([^)]+)\)/);
                if (father_amount_august_negative_value != null) {
                    father_amount_august = -parseFloat(father_amount_august_negative_value[1]);
                } else {
                    father_amount_august = parseFloat(father_amount_august);
                }

                var august_amount = el.find('.august').html();
                var august_negative_value = august_amount.match(/\(([^)]+)\)/);

                if (august_negative_value != null) {
                    father_amount_august -= parseFloat(august_negative_value[1]);
                } else {
                    father_amount_august += parseFloat(august_amount);
                }

                $('.'+father_id).find('.august').html(father_amount_august >= 0 ? father_amount_august : '('+Math.abs(father_amount_august)+')');

                var father_id = el.data('fatherid');

                var father_amount_september = $('.'+father_id).find('.september').html();
                var father_amount_september_negative_value = father_amount_september.match(/\(([^)]+)\)/);
                if (father_amount_september_negative_value != null) {
                    father_amount_september = -parseFloat(father_amount_september_negative_value[1]);
                } else {
                    father_amount_september = parseFloat(father_amount_september);
                }

                var september_amount = el.find('.september').html();
                var september_negative_value = september_amount.match(/\(([^)]+)\)/);

                if (september_negative_value != null) {
                    father_amount_september -= parseFloat(september_negative_value[1]);
                } else {
                    father_amount_september += parseFloat(september_amount);
                }

                $('.'+father_id).find('.september').html(father_amount_september >= 0 ? father_amount_september : '('+Math.abs(father_amount_september)+')');

                var father_id = el.data('fatherid');

                var father_amount_october = $('.'+father_id).find('.october').html();
                var father_amount_october_negative_value = father_amount_october.match(/\(([^)]+)\)/);
                if (father_amount_october_negative_value != null) {
                    father_amount_october = -parseFloat(father_amount_october_negative_value[1]);
                } else {
                    father_amount_october = parseFloat(father_amount_october);
                }

                var october_amount = el.find('.october').html();
                var october_negative_value = october_amount.match(/\(([^)]+)\)/);

                if (october_negative_value != null) {
                    father_amount_october -= parseFloat(october_negative_value[1]);
                } else {
                    father_amount_october += parseFloat(october_amount);
                }

                $('.'+father_id).find('.october').html(father_amount_october >= 0 ? father_amount_october : '('+Math.abs(father_amount_october)+')');

                var father_id = el.data('fatherid');

                var father_amount_november = $('.'+father_id).find('.november').html();
                var father_amount_november_negative_value = father_amount_november.match(/\(([^)]+)\)/);
                if (father_amount_november_negative_value != null) {
                    father_amount_november = -parseFloat(father_amount_november_negative_value[1]);
                } else {
                    father_amount_november = parseFloat(father_amount_november);
                }

                var november_amount = el.find('.november').html();
                var november_negative_value = november_amount.match(/\(([^)]+)\)/);

                if (november_negative_value != null) {
                    father_amount_november -= parseFloat(november_negative_value[1]);
                } else {
                    father_amount_november += parseFloat(november_amount);
                }

                $('.'+father_id).find('.november').html(father_amount_november >= 0 ? father_amount_november : '('+Math.abs(father_amount_november)+')');

                var father_id = el.data('fatherid');

                var father_amount_december = $('.'+father_id).find('.december').html();
                var father_amount_december_negative_value = father_amount_december.match(/\(([^)]+)\)/);
                if (father_amount_december_negative_value != null) {
                    father_amount_december = -parseFloat(father_amount_december_negative_value[1]);
                } else {
                    father_amount_december = parseFloat(father_amount_december);
                }

                var december_amount = el.find('.december').html();
                var december_negative_value = december_amount.match(/\(([^)]+)\)/);

                if (december_negative_value != null) {
                    father_amount_december -= parseFloat(december_negative_value[1]);
                } else {
                    father_amount_december += parseFloat(december_amount);
                }

                $('.'+father_id).find('.december').html(father_amount_december >= 0 ? father_amount_december : '('+Math.abs(father_amount_december)+')');
            }

        });

        $('#accounts_table .FY_total').each(function() {
            var FY_total = 0;
            var el = $(this);

            var january = el.closest('tr').find('.january').html();
            var january_negative_value = january.match(/\(([^)]+)\)/);

            if (january_negative_value != null) {
                FY_total -= parseFloat(january_negative_value[1]);
            } else {
                FY_total += parseFloat(january);
            }

            var february = el.closest('tr').find('.february').html();
            var february_negative_value = february.match(/\(([^)]+)\)/);

            if (february_negative_value != null) {
                FY_total -= parseFloat(february_negative_value[1]);
            } else {
                FY_total += parseFloat(february);
            }

            var march = el.closest('tr').find('.march').html();
            var march_negative_value = march.match(/\(([^)]+)\)/);

            if (march_negative_value != null) {
                FY_total -= parseFloat(march_negative_value[1]);
            } else {
                FY_total += parseFloat(march);
            }

            var april = el.closest('tr').find('.april').html();
            var april_negative_value = april.match(/\(([^)]+)\)/);

            if (april_negative_value != null) {
                FY_total -= parseFloat(april_negative_value[1]);
            } else {
                FY_total += parseFloat(april);
            }

            var may = el.closest('tr').find('.may').html();
            var may_negative_value = may.match(/\(([^)]+)\)/);

            if (may_negative_value != null) {
                FY_total -= parseFloat(may_negative_value[1]);
            } else {
                FY_total += parseFloat(may);
            }

            var june = el.closest('tr').find('.june').html();
            var june_negative_value = june.match(/\(([^)]+)\)/);

            if (june_negative_value != null) {
                FY_total -= parseFloat(june_negative_value[1]);
            } else {
                FY_total += parseFloat(june);
            }

            var july = el.closest('tr').find('.july').html();
            var july_negative_value = july.match(/\(([^)]+)\)/);

            if (july_negative_value != null) {
                FY_total -= parseFloat(july_negative_value[1]);
            } else {
                FY_total += parseFloat(july);
            }

            var august = el.closest('tr').find('.august').html();
            var august_negative_value = august.match(/\(([^)]+)\)/);

            if (august_negative_value != null) {
                FY_total -= parseFloat(august_negative_value[1]);
            } else {
                FY_total += parseFloat(august);
            }

            var september = el.closest('tr').find('.september').html();
            var september_negative_value = september.match(/\(([^)]+)\)/);

            if (september_negative_value != null) {
                FY_total -= parseFloat(september_negative_value[1]);
            } else {
                FY_total += parseFloat(september);
            }

            var october = el.closest('tr').find('.october').html();
            var october_negative_value = october.match(/\(([^)]+)\)/);

            if (october_negative_value != null) {
                FY_total -= parseFloat(october_negative_value[1]);
            } else {
                FY_total += parseFloat(october);
            }

            var november = el.closest('tr').find('.november').html();
            var november_negative_value = november.match(/\(([^)]+)\)/);

            if (november_negative_value != null) {
                FY_total -= parseFloat(november_negative_value[1]);
            } else {
                FY_total += parseFloat(november);
            }

            var december = el.closest('tr').find('.december').html();
            var december_negative_value = december.match(/\(([^)]+)\)/);

            if (december_negative_value != null) {
                FY_total -= parseFloat(december_negative_value[1]);
            } else {
                FY_total += parseFloat(december);
            }
            el.html(FY_total >= 0 ? FY_total : '('+Math.abs(FY_total)+')');
        });

        $('.account_item').each(function() {
            var el = $(this);

            if (parseFloat(el.find('.january').html()) == 0
                && parseFloat(el.find('.february').html()) == 0
                && parseFloat(el.find('.march').html()) == 0
                && parseFloat(el.find('.april').html()) == 0
                && parseFloat(el.find('.may').html()) == 0
                && parseFloat(el.find('.june').html()) == 0
                && parseFloat(el.find('.july').html()) == 0
                && parseFloat(el.find('.august').html()) == 0
                && parseFloat(el.find('.september').html()) == 0
                && parseFloat(el.find('.october').html()) == 0
                && parseFloat(el.find('.november').html()) == 0
                && parseFloat(el.find('.december').html()) == 0
                ) {

                el.hide();
            }
        });

        $('.january, .february, .march, .april, .may, .june, .july, .august, .september, .october, .november, .december, .FY_total').each(function() {
            var el = $(this);

            var html_amount = el.html();
            var check_rackets = html_amount.match(/\(([^)]+)\)/);

            if (check_rackets != null) {

                var round_amount = numberWithCommas(parseFloat(check_rackets[1]).toFixed(2));

                el.html('('+round_amount+')');
            } else {
                el.html(numberWithCommas(parseFloat(html_amount).toFixed(2)));
            }
        });

        function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }
    });

    $(document).ready(function() {
        $("#download_excel_file").click(function(e) {
            e.preventDefault();

            //getting data from our table
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('table_wrapper');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');

            var a = document.createElement('a');
            a.href = data_type + ', ' + table_html;
            a.download = 'PL_monthly_report.xls';
            a.click();
        });

        $('body').mouseover(function () {
            $("#download_excel_file").show();
            $("#download_pdf_file").show();
        });

        $("#download_pdf_file").click(function () {
            $("#download_excel_file").hide();
            $("#download_pdf_file").hide();
            window.print();
        });
    });
</script>