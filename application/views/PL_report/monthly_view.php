<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    <?= $title ?>
</h3>
<div class="page-bar" <?php echo $hide_bar == TRUE ? 'style="display:none;"' : ''; ?>>
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo base_url("PL_report/monthly_view"); ?>">P&L (monthly)</a>
            <i class="fa fa-angle-right"></i>
        </li>
</div>
<!-- END PAGE HEADER-->

<div class="portlet box <?php echo $hide_bar != TRUE ? 'blue' : ''; ?> ">
    <div class="portlet-title" <?php echo $hide_bar == TRUE ? 'style="display:none;"' : ''; ?>>
        <div class="caption">
            <i class="fa fa-money"></i> P&L (monthly)
        </div>

    </div>
    <div class="portlet-body">
        <form role="form" method='post' class='form-horizontal' id="filter_form" data-parsley-validate target="_blank">
            <div class="row form-group">
                <div class="col-md-12">
                    <table class="dataTable table table-bordered table-hover">
                        <tbody>
                        <tr>
                            <td>
                                Select Financial Period <span style="color:red;" class="required">*</span>
                            </td>
                            <td colspan="2">
                                <div>
                                    <select class="form-control" name="financial_period_id" data-parsley-required="true" data-parsley-required-message="Please select period to view report">
                                        <option value="">Please Select</option>
                                        <?php foreach ($financial_period AS $item) { ?>
                                        <option value="<?php echo $item['id']; ?>"><?php echo 'From '.$item['starting_date'].' to '.$item['ending_date'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Select Outlets <span style="color:red;" class="required">*</span>
                            </td>
                            <td colspan="2">
                                <div>
                                    <select name="outlet_list[]" id='pre-selected-options' multiple='multiple' data-parsley-required="true" data-parsley-required-message="Please select outlets to view report">
                                        <?php for ($i = 0; $i < count($project_list); $i++) {
                                            ?>
                                            <option value="<?php echo $project_list[$i]['id']; ?>"><?php echo $project_list[$i]['name'].' ('.$project_list[$i]['description'].')'; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Select Type of Report <span style="color:red;" class="required">*</span>
                            </td>
                            <td colspan="2">
                                <div>
                                    <input type="radio" name="report_type" value="summary" checked="checked"><?php echo form_label('Summary', 'report_type');?>
                                    <input type="radio" name="report_type" value="detail"><?php echo form_label('Detail', 'report_type');?>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>


            <div class='row form-group'>
                <div class="col-md-12">
                    <input class="btn btn-primary form-control" type='submit' value="Submit">
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    jQuery(document).ready(function () {

        // run pre selected options
        $('#pre-selected-options').multiSelect();
    });
</script>