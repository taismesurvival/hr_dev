<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Bank and Cash Account
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo base_url('company') ?>">Company Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Bank and Cash Account</a>
        </li>
</div>
<!-- END PAGE HEADER -->
<!-- BEGIN ALERT BOX -->
<?php if (isset($flash_message)) { ?>
    <div class="row margin-bottom-20">
        <div class="col-xs-12">
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END ALERT BOX -->

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Bank and Cash Account</span>
                </div>
                <a href="#" class="bank_account-add btn btn-md blue pull-right"><i class="fa fa-plus"></i> Add New</a>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-4 col-xs-12" style="margin-bottom: 20px;">
                        <span data-toggle="tooltip" data-original-title="This is default account for making Payment (Invoices)" class="caption-subject font-dark bold uppercase">Default Payment Account: </span>

                    </div>
                    <div class="col-md-8 col-xs-12">
                        <span class="caption-subject font-dark bold uppercase"><?php echo $default_account; ?></span>

                    </div>

                    <div class="col-md-12">

                        <!-- Begin: life time stats -->
                        <div class="table-container">
                            <!-- Begin: life time stats -->
                            <div class="clearfix mt15">

                                <form method="get" class="form" role="form">
                                    <div class="row">
                                        <div class="col-md-3 col-xs-12">
                                            <label>Keyword</label>
                                            <div class="form-group">
                                                <input type="text" name="key_word" class="form-control" value="<?php echo $search['key_word'] ?>" placeholder="Login ID">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 text-right">
                                            <a href="<?php echo base_url('bank_account') ?>" class=" btn btn-md grey-salsa">Reset</a>
                                            <button type="submit" class="btn btn-md green"><i class="icon-magnifier font-white"></i>Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <table style="margin-top: 20px;" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="4%" class="text-right">No</th>
                                    <th width="8%">Name</th>
                                    <th width="8%">Number</th>
                                    <th width="8%">Account Code</th>
                                    <th width="8%">Type</th>
                                    <th width="25%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php for ($i = 0; $i < count($accounts); $i++) {
                                        ?>
                                    <tr role="row" class="heading">
                                        <td class="text-right"><?php echo $pagination['offset']+$i ?></td>
                                        <td class="id hidden"><?php echo $accounts[$i]['id'] ?></td>
                                        <td class="name"><?php echo $accounts[$i]['name'] ?></td>
                                        <td class="number"><?php echo $accounts[$i]['number']; ?></td>
                                        <td class="account_code"><?php echo $accounts[$i]['account_code']; ?></td>
                                        <td class="type"><?php echo $accounts[$i]['type']; ?></td>
                                        <td>
                                            <a class="bank_account-edit btn btn-md blue"><i class="fa fa-pencil"></i> Edit</a>
                                            <a href="<?php echo base_url() ?>bank_account/set_default?account_id=<?php echo $accounts[$i]['id'] ?>" class="bank_account-set_default btn btn-md blue"><i class="fa fa-building"></i> Set Default</a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
                <nav class="pagination-wrap clearfix">
                    <div class="pull-left mt10">
                        <?php if ($pagination['total'] != 0) { ?>
                        <label class="form-control-static"><?php echo $pagination['total'] ?> Items in total, Display
                            <?php echo $pagination['offset'] ?> ~
                            <?php echo $pagination['limit'] ?></label>
                        <?php } ?>
                    </div>
                    <?php if (!empty($pagination) && $pagination['total'] > $pagination['items_per_page']) { ?>
                    <div class="pull-right">
                        <ul class="pagination no-margin">
                            <li <?php if ($pagination['page'] - 1 <= 0) { ?> class="disabled" <?php } ?>>

                            <a href="<?php if ($pagination['page'] - 1 <= 0){ ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] - 1 ?><?php } ?>">
                                Previous
                            </a>
                            </li>
                            <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                            <li>
                                <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=1"> 1 </a>
                            </li>
                            <?php } ?>

                            <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                            <li><a href="javascript:;">...</a></li>
                            <?php } ?>

                            <?php for ($p=($pagination['min_display']); $p <= $pagination['max_display']; $p++) { ?>
                            <?php if ($p > 0) { ?>
                            <li <?php if ($pagination['page'] == $p) { ?> class="active" <?php } ?>>
                            <a href="<?php if ($pagination['page'] != $p) { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $p ?><?php } else { ?>javascript:;<?php } ?>"> <?php echo $p ?> </a>
                            </li>
                            <?php } ?>
                            <?php } ?>

                            <?php if (($pagination['page'] + $pagination['adj']) < $pagination['total_page']) { ?>
                            <li><a href="javascript:;">...</a></li>
                            <?php } ?>

                            <?php if ($pagination['page'] + $pagination['adj'] < $pagination['total_page']) { ?>
                            <li>
                                <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['total_page'] ?>"> <?php echo $pagination['total_page'] ?> </a>
                            </li>
                            <?php } ?>

                            <li <?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> class="disabled" <?php } ?>>
                            <a href="<?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] + 1 ?><?php } ?>">
                                Next
                            </a>
                            </li>
                        </ul>
                    </div>
                    <?php } ?>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="bank_account-edit-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="bank_account-modal-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">

                <div id="show_error_message" class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span id="error_message">Financial Period is updated successfully</span>
                </div>

                <?php echo form_open_multipart(base_url('bank_account/edit'), 'class="form-horizontal" method="post" data-parsley-validate id="bank_account-edit_form"'); ?>
                <div class="form-body">
                    <div class="form-group">
                        <?php echo form_label('Name <span style="color:red;" class="required">*</span>', 'name', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input('name', set_value('name', ''), 'class="form-control" id="modal_name" data-parsley-required="true" data-parsley-required-message="Account Name is required field"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Number', 'number', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input('number', set_value('number', ''), 'class="form-control number" id="modal_number"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Type <span style="color:red;" class="required">*</span>', 'type', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_dropdown('type', ['' => 'Please Select', 'bank' => 'Bank Account', 'cash' => 'Cash on Hand'], set_select('type'), 'class="form-control" id="modal_type" data-parsley-required="true" data-parsley-required-message="Account Type is required field"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Account Code', 'account_code', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input('account_code', set_value('account_code', ''), 'class="form-control" id="modal_account_code" disabled'); ?>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_id">
                <button  type="button" class="btn btn-md blue" id="bank_account_submit">Submit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {

        $('[data-toggle="tooltip"]').tooltip();

        $('.date-picker').datepicker({
            format: "yyyy-mm-dd"
        });

        $("#modal_number").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $("#bank_account_submit").on("click",function(){

            $('#show_error_message').hide();
            $("#bank_account-edit_form").submit();
            return;
        });

        $(".bank_account-edit").on("click",function(){

            $("#modal_name").parsley().reset();
            $("#modal_type").parsley().reset();

            var el = $(this);
            var name = el.closest('tr').find('.name').html();
            var number = el.closest('tr').find('.number').html();
            var account_code = el.closest('tr').find('.account_code').html();
            var type = el.closest('tr').find('.type').html();
            var id = el.closest('tr').find('.id').html();

            $('#show_error_message').hide();

            $('#modal_name').val(name);
            $('#modal_number').val(number);
            $('#modal_account_code').val(account_code);
            $('#modal_type').val(type);
            $('#modal_id').val(id);
            $('#modal_type').attr("disabled", "disabled");

            $('#bank_account-modal-title').html('Edit Account');

            $('#bank_account-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $(".bank_account-add").on("click",function(){

            $("#modal_name").parsley().reset();
            $("#modal_type").parsley().reset();

            $('#modal_type').prop("disabled", false);
            $('#modal_name').val('');
            $('#modal_number').val('');
            $('#modal_type').val('');
            $('#modal_account_code').val('');
            $('#modal_id').val('');
            $('#show_error_message').hide();

            $('#bank_account-modal-title').html('Add Account');

            $('#bank_account-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });
    });
</script>
