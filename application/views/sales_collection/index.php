<style>
    .choose_account {cursor: pointer;}

    .ms-container .ms-selectable, .ms-container .ms-selection {
        width: 40% !important;
    }
</style>
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Sales Collection
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo base_url('sales_collection_submit') ?>">Sales Collection Submit History</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Sales Collection</a>
        </li>
</div>
<!-- END PAGE HEADER -->
<!-- BEGIN ALERT BOX -->
<?php if (isset($flash_message)) { ?>
    <div class="row margin-bottom-20">
        <div class="col-xs-12">
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END ALERT BOX -->

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Sales Collection</span>
                </div>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-12">

                        <!-- Begin: life time stats -->
                        <div class="table-container">
                            <!-- Begin: life time stats -->
                            <div class="clearfix mt15">

                                <form method="post" class="form" role="form" data-parsley-validate enctype="multipart/form-data">
                                    <div class="row">

                                        <div class="col-xs-12">
                                            <div class="row">
                                                <?php echo form_label('Bank Account', 'description', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <select id="bank_account_id" name="bank_account_id" class="form-control" data-parsley-required="true" data-parsley-required-message="Account is required field">
                                                        <?php echo $bank_accounts_selector; ?>

                                                    </select>
                                                </div>
                                                <?php echo form_label('Payment Method', 'description', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <select id="payment_method" name="payment_method" class="form-control" data-parsley-required="true" data-parsley-required-message="Payment Method is required field">
                                                        <option selected value="">Please Select</option>
                                                        <option value="cash">Cash</option>
                                                        <option value="visa_and_master">Visa and Master Card</option>
                                                        <option value="nets">NETS</option>
                                                        <option value="american_express">American Express</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12" style="margin-top: 10px;">
                                            <div class="row">
                                                <?php echo form_label('Outlets', 'description', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                                <select name="outlet_list[]" id='pre-selected-options' multiple='multiple' data-parsley-required="true" data-parsley-required-message="Outlets is required field">
                                                    <?php for ($i = 0; $i < count($project_list); $i++) {
                                                        ?>
                                                        <option value="<?php echo $project_list[$i]['id']; ?>"><?php echo $project_list[$i]['name'].' ('.$project_list[$i]['description'].')'; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-xs-12" style="margin-top: 10px;">
                                            <div class="row">

                                                <?php echo form_label('Month', 'description', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <div class="input-group date month-picker" data-date-format="yyyy-mm"
                                                          data-date-viewmode="years">
                                                        <input id="selected_month" value="" name="selected_month" type="text" class="form-control" data-parsley-required="true" data-parsley-required-message="Month is required">

                                                        <div class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-xs-12 text-right" style="margin-top: 10px;">
                                            <button type="submit" class="btn btn-md green"><i class="icon-magnifier font-white"></i>Show Outstanding AR</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <?php if (!empty($payments)) { ?>
                            <table style="margin-top: 20px;" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="4%" class="text-right">No</th>
                                    <th width="8%">Document Date</th>
                                    <th width="15%">Outlet</th>
                                    <th width="8%">Description</th>
                                    <th width="8%">Cash in Bank</th>
                                    <th width="8%">Bank Charge</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $total_cash_in_bank = 0;
                                    $total_bank_charge = 0;

                                    for ($i = 0; $i < count($payments); $i++) {

                                        $cash_in_bank = $payments[$i]['amount']*(100-$default_bank_charge_rate)/100;

                                        $bank_charge = $payments[$i]['amount']-$cash_in_bank;

                                        $total_cash_in_bank += $cash_in_bank;
                                        $total_bank_charge += $bank_charge;

                                        ?>
                                    <tr role="row" class="trans">
                                        <td class="text-right">
                                            <input type="checkbox" name="payment_select[]" class="payment_select" value="<?php echo $payments[$i]['id'] ?>">
                                        </td>
                                        <td class="document_date"><?php echo $payments[$i]['document_date']; ?></td>
                                        <td class="project_id hidden"><?php echo $payments[$i]['project_id']; ?></td>
                                        <td class="GL_transaction_id hidden"><?php echo $payments[$i]['id']; ?></td>
                                        <td class="account_code hidden"><?php echo $payments[$i]['account_code']; ?></td>
                                        <td class="Gl_transaction_amount hidden"><?php echo $payments[$i]['amount']; ?></td>
                                        <td><?php echo $payments[$i]['project_name']; ?> (<?php echo $payments[$i]['project_code']; ?>)</td>
                                        <td class="description"><?php echo $payments[$i]['description']; ?></td>
                                        <td class="amount"><?php echo number_format($cash_in_bank,2); ?></td>
                                        <td class="bank_charge"><?php echo number_format($bank_charge,2); ?></td>
                                    </tr>
                                    <?php } ?>
                                    <tr role="row">
                                        <td colspan="4" class="text-center" style="font-weight: bold;">Total</td>
                                        <td id="total_cash_in_bank"><?php echo number_format($total_cash_in_bank,2);; ?></td>
                                        <td id="total_bank_charge"><?php echo number_format($total_bank_charge,2);; ?></td>
                                    </tr>
                                </tbody>
                            </table>

                            <table style="margin-top: 20px;" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="8%" class="text-right"></th>
                                    <th width="8%">Total Cash in Bank</th>
                                    <th width="8%">Total Bank Charge</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr role="row">
                                        <td style="font-weight: bold;">Chosen Sales</td>
                                        <td id="chosen_total_cash_in_bank">0.00</td>
                                        <td id="chosen_total_bank_charge">0.00</td>
                                    </tr>

                                    <tr role="row">
                                        <td style="font-weight: bold;">Bank Statements</td>
                                        <td id="bank_statement_total_cash_in_bank"><?php echo number_format($total_bank_statements,2);; ?></td>
                                        <td id="bank_statement_total_bank_charge"></td>
                                    </tr>
                                    <tr role="row">
                                        <td style="font-weight: bold;">Adjustment</td>
                                        <td id="adjustment_total_cash_in_bank"></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php } ?>

                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
                <?php if (!empty($payments)) { ?>
                <div class="text-center">
                    <small id="required_selected_trans" style="color:red; display:none;">Please select sales to submit</small>
                    <a id="pay_invoices" class="btn btn-md blue"><i class="fa fa-pencil"></i> Submit</a>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {

        function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }

        $('.month-picker').datepicker({
            format: "yyyy-mm",
            viewMode: "months",
            minViewMode: "months"
        });

        var payment_method = '<?php echo $search['payment_method']; ?>';
        var bank_account_id = '<?php echo $search['bank_account_id']; ?>';
        var file_name = '<?php echo $search['file_name']; ?>';
        var selected_month = '<?php echo $search['selected_month']; ?>';
        var type = '<?php echo $search['type']; ?>';

        $('#payment_method').val(payment_method);
        $('#bank_account_id').val(bank_account_id);
        $('#selected_month').val(selected_month);
        $('#file_name').val(file_name);
        $('#type').val(type);

        var values = '<?php echo $search['outlet_string']; ?>';
        $.each(values.split(","), function(i,e){
            $("#pre-selected-options option[value='" + e + "']").prop("selected", true);
        });

        $('#pre-selected-options').multiSelect();

        $('input:checkbox.payment_select').on('click', function () {

            var el = $(this);

            var chosen_total_cash_in_bank = parseFloat($('#chosen_total_cash_in_bank').html().replace(/,/g, ""));
            var chosen_total_bank_charge = parseFloat($('#chosen_total_bank_charge').html().replace(/,/g, ""));

            var amount = parseFloat(el.closest('.trans').find('.amount').html().replace(/,/g, ""));

            var bank_charge = parseFloat(el.closest('.trans').find('.bank_charge').html().replace(/,/g, ""));

            if($(this).is(':checked')){

                el.closest('.trans').find('td').css('background-color', 'yellow');

                chosen_total_cash_in_bank += amount;
                chosen_total_bank_charge += bank_charge;


            } else {

                el.closest('.trans').find('td').attr('style', '');

                chosen_total_cash_in_bank -= amount;
                chosen_total_bank_charge -= bank_charge;


            }

            $('#chosen_total_cash_in_bank').html(numberWithCommas(chosen_total_cash_in_bank.toFixed(2)));
            $('#chosen_total_bank_charge').html(numberWithCommas(chosen_total_bank_charge.toFixed(2)));

            var bank_statement_total_cash_in_bank = parseFloat($('#bank_statement_total_cash_in_bank').html().replace(/,/g, ""));

            var adjustment_total_cash_in_bank = bank_statement_total_cash_in_bank - chosen_total_cash_in_bank;

            $('#adjustment_total_cash_in_bank').html(numberWithCommas(adjustment_total_cash_in_bank.toFixed(2)));

        });

        $('#pay_invoices').on('click', function () {

            $(this).prop("disabled", true);

            var transactions = [];
            $('input:checkbox.payment_select:checked').each(function() {

                var el = $(this);
                var transaction = {};
                transaction.GL_transaction_id = el.closest('.trans').find('.GL_transaction_id').html();
                transaction.account_code = el.closest('.trans').find('.account_code').html();
                transaction.project_id = el.closest('.trans').find('.project_id').html();
                transaction.sales_date = el.closest('.trans').find('.document_date').html();
                transaction.GL_transaction_amount = el.closest('.trans').find('.Gl_transaction_amount').html();

                transactions.push(transaction);

            });

            var payment_method = $('#payment_method').val();

            var selected_month = $('#selected_month').val();

            var file_name = $('#file_name').val();

            var bank_account_id = $('#bank_account_id').val();
            var type = $('#bank_account_id').val();

            var chosen_total_cash_in_bank = parseFloat($('#chosen_total_cash_in_bank').html().replace(/,/g, ""));
            var chosen_total_bank_charge = parseFloat($('#chosen_total_bank_charge').html().replace(/,/g, ""));

            var bank_statement_total_cash_in_bank = parseFloat($('#bank_statement_total_cash_in_bank').html().replace(/,/g, ""));

            var adjustment_total_cash_in_bank = parseFloat($('#adjustment_total_cash_in_bank').html().replace(/,/g, ""));


            if (chosen_total_cash_in_bank == 0) {
                $('#chosen_total_cash_in_bank').closest('tr').css('color', 'red');
                $('#required_selected_trans').show();
                $(this).prop("disabled", false);
                return;
            } else {
                $('#chosen_total_cash_in_bank').closest('tr').css('color', 'black');
                $('#required_selected_trans').hide();
            }

            $.ajax({
                method: 'POST',
                url: "<?php echo base_url(); ?>sales_collection/submit",
                data: {
                    bank_account_id: bank_account_id,
                    type: type,
                    payment_method: payment_method,
                    bank_in_amount: bank_statement_total_cash_in_bank.toFixed(2),
                    payable_amount: (chosen_total_cash_in_bank+chosen_total_bank_charge).toFixed(2),
                    bank_charge_amount: (chosen_total_bank_charge-adjustment_total_cash_in_bank).toFixed(2),
                    document_date: selected_month,
                    bank_statement_file: file_name,
                    transactions: transactions
                },
                dataType: 'json'
            }).done(function() {

                window.location = "<?php echo base_url(); ?>sales_collection_submit";
            });


        });

    });
</script>
