<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Outlet
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url('company') ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo base_url('company') ?>">Company Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Payment Method</a>
        </li>
</div>
<!-- END PAGE HEADER -->
<!-- BEGIN ALERT BOX -->
<?php if (isset($flash_message)) { ?>
    <div class="row margin-bottom-20">
        <div class="col-xs-12">
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END ALERT BOX -->

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Payment Method</span>
                </div>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-12">

                        <!-- Begin: life time stats -->
                        <div class="table-container">

                            <div class="clearfix mt15">

                                <form method="get" class="form" role="form">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <?php echo form_label('Outlet', 'outlet', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <select name="outlet" class="form-control" id="outlet_id">
                                                        <?php echo $project_selector; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 text-right">
                                            <a href="<?php echo base_url('HGW_payment_method_mapping') ?>" class=" btn btn-md grey-salsa">Reset</a>
                                            <button type="submit" class="btn btn-md green"><i class="icon-magnifier font-white"></i>Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <!-- Begin: life time stats -->
                            <table style="margin-top: 20px;" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="7%" class="text-right">No</th>
                                    <th width="11%">HGW Payment Method Name</th>
                                    <th width="11%">Mapped Payment Method Name</th>
                                    <th width="10%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php for ($i = 0; $i < count($payment_methods); $i++) { ?>
                                    <tr role="row" class="heading">
                                        <td class="text-right"><?php echo $i+1 ?></td>
                                        <td class="id hidden"><?php echo $payment_methods[$i]['id'] ?></td>
                                        <td class="HGW_payment_method_id hidden"><?php echo $payment_methods[$i]['HGW_payment_method_id'] ?></td>
                                        <td class="payment_method_id hidden"><?php echo $payment_methods[$i]['payment_method_id'] ?></td>
                                        <td class="HGW_payment_method_name"><?php echo $payment_methods[$i]['HGW_payment_method_name']; ?></td>
                                        <td class="payment_method_name"><?php echo $payment_methods[$i]['payment_method_name']; ?></td>
                                        <td>
                                            <a class="payment_method-edit btn btn-md blue"><i class="fa fa-pencil"></i> Map</a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="payment_method-edit-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="payment_method-modal-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">

                <div id="show_error_message" class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span id="error_message"></span>
                </div>

                <?php echo form_open_multipart(base_url('HGW_payment_method_mapping/edit'), 'class="form-horizontal" method="post" data-parsley-validate id="payment_method-edit_form"'); ?>

                <div class="form-group">
                    <?php echo form_label('HGW Payment Method Name', 'HGW_payment_method_name', array('class' => 'col-md-6 col-lg-5 control-label')); ?>

                    <div class="col-md-6 col-lg-7 form-group">
                        <?php echo form_input('HGW_payment_method_name', set_value('HGW_payment_method_name', ''), 'class="form-control modal_HGW_payment_method_name" disabled'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-6 col-lg-5 control-label">Select Payment Method</label>

                    <div class="col-md-6 col-lg-7 form-group">
                        <select name="payment_method_id" class="form-control" id="modal_payment_method_id">
                            <?php echo $payments; ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_id">
                <input type="hidden" name="HGW_payment_method_name" class="modal_HGW_payment_method_name">
                <input type="hidden" name="HGW_payment_method_id" id="modal_HGW_payment_method_id">
                <input type="hidden" name="outlet_id" id="modal_outlet_id">
                <button  type="button" class="btn btn-md blue" id="payment_method_submit">Submit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {

        $("#payment_method_submit").on("click",function(){

            $('#show_error_message').hide();
            $("#payment_method-edit_form").submit();
        });

        $(".payment_method-edit").on("click",function(){

            var el = $(this);
            var HGW_payment_method_name = el.closest('tr').find('.HGW_payment_method_name').html();
            var id = el.closest('tr').find('.id').html();
            var HGW_payment_method_id = el.closest('tr').find('.HGW_payment_method_id').html();
            var outlet_id = $('#outlet_id').val();
            var payment_method_id = el.closest('tr').find('.payment_method_id').html();

            $('#show_error_message').hide();

            $('.modal_HGW_payment_method_name').val(HGW_payment_method_name);
            $('#modal_id').val(id);
            $('#modal_HGW_payment_method_id').val(HGW_payment_method_id);
            $('#modal_outlet_id').val(outlet_id);
            $('#modal_payment_method_id').val(payment_method_id);

            $('#payment_method-modal-title').html('Map HGW Payment Method');

            $('#payment_method-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });
    });
</script>
