<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Accounting System</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>


    <link href="<?php echo base_url('public/assets/global/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/global/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/global/plugins/uniform/css/uniform.default.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css"/>

    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/select2/select2.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/datatables/plugins/integration/bootstrap/3/dataTables.bootstrap.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/datatables-yadcf/jquery.dataTables.yadcf.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/bootstrap-datepicker/css/datepicker3.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/bootstrap-datetimepicker/css/datetimepicker.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/typeahead/typeahead.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/ion.rangeslider/css/ion.rangeSlider.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/ion.rangeslider/css/ion.rangeSlider.Metronic.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/slider-pips/jquery-ui-slider-pips.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/bootstrap-summernote/summernote.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/admin/pages/css/jquery.treegrid.css') ?>">

    <!-- END PAGE LEVEL STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo base_url('public/assets/global/css/components.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/global/css/plugins.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/global/css/fix.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/admin/layout/css/layout.css') ?>" rel="stylesheet" type="text/css"/>
    <link id="style_color" href="<?php echo base_url('public/assets/admin/layout/css/themes/default.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/admin/layout/css/custom.css') ?>" rel="stylesheet" type="text/css"/>
    <!-- plus a jQuery UI theme, here I use "flick" -->
    <link rel="stylesheet" href="<?php echo base_url('public/assets/global/plugins/slider-pips/jquery-ui.css') ?>">
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="<?php echo base_url('favicon.ico') ?>"/>
    <link href="<?php echo base_url('public/assets/global/css/multi-select.css') ?>" rel="stylesheet" type="text/css"/>

    <script src="<?php echo base_url('public/assets/global/plugins/jquery-1.11.0.min.js') ?>" type="text/javascript"></script>

    <script type="text/javascript">
        var base_url = '<?= base_url()?>';


    </script>
    <style type="text/css">
        table {
            color: #333333;
            border-collapse: collapse;
            table-layout: fixed;
            word-wrap: break-word;
            margin: 20px auto;
        }
        th{
            text-align: center;
        }
        th, td{
            border: 1px solid #666666;
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .borderless{
            border: none;
        }
        input{
            text-align: right;
        }
        .check_bold{
            background: #cccccc; font-weight: bold; text-align: right;
            padding-right: 3px;
            -webkit-box-sizing:  border-box; /* Safari/Chrome, other WebKit */
            -moz-box-sizing:  border-box;    /* Firefox, other Gecko */
            box-sizing:  border-box;
        }
        .td_input{
            background: #dce6f1;
        }
        .td_input input{
            width: 70px;
            padding-right: 3px;
            -webkit-box-sizing:  border-box; /* Safari/Chrome, other WebKit */
            -moz-box-sizing:  border-box;    /* Firefox, other Gecko */
            box-sizing:  border-box;
        }
    </style>
</head>
<body>

    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="<?php echo base_url(); ?>">
                    <div class="row logo-default" style="color: #999999;" >Accounting System</div>
                </a>
                <div class="menu-toggler sidebar-toggler">
                </div>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN HORIZANTAL MENU -->
            <!-- DOC: Remove "hor-menu-light" class to have a horizontal menu with theme background instead of white background -->
            <!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) in the responsive menu below along with sidebar menu. So the horizontal menu has 2 seperate versions -->

            <!-- END HEADER SEARCH BOX -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse">
            </a>
            <div style="position:absolute;top:15px;margin-left:50%;margin-right:50%;width:400px;color:white">
                <?php echo $current_company_data['name']; ?>
            </div>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                        <span class="username username-hide-on-mobile">
                        <?php
                        $adminData = $this->session->userData("user_data");
                        if(isset($adminData['username'])){
                            echo $adminData["username"];
                        }else{
                            echo $adminData["code"]." - ".$adminData["full_name"];
                        }
                        ?>
                        </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo base_url('user/change_password'); ?>">
                                    <i class="icon-lock"></i>Change Login Password </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url("AuthController/logout/admin"); ?>">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>

                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->

    <div class="page-content-wrapper">
        <div style="padding: 70px 20px 10px 20px;background-color: #fff;">
            <!-- BEGIN ALERT BOX -->
            <?php if (isset($flash_message)) { ?>
                <div class="row margin-bottom-20">
                    <div class="col-xs-12">
                        <div class="alert alert-success">
                            <button class="close" data-close="alert"></button>
                            <?php echo $flash_message; ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <!-- END ALERT BOX -->
            <div id="show_error_message" class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <span id="error_message">Financial Period is updated successfully</span>
            </div>
            <table>
                <thead>
                <tr style="margin-bottom: 10px;">
                    <th colspan="14" class="borderless" style="float:left;"><a href="<?php echo base_url("sales_input/select_outlet"); ?>" type="button" class="btn btn-md grey-salsa text-right">Back</a></th>
                </tr>
                <tr style="margin-bottom: 10px;">
                    <th colspan="1" class="borderless" style=" text-align: left;">		Sales Date:</th>
                    <th colspan="3" class="borderless td_input"><input style="width:100%;" id="sales_date" type="text" class="date-picker">
                        <small id="required_payroll_month" style="color:red; display:none;">Please select Sales Date</small>
                        <small id="required_date_payroll_month" style="color:red; display:none;">Please select date in the last 48 hours</small>
                    </th>
                    <th colspan="3" class="borderless hidden_date">Suggested GST Amount: </th>
                    <th colspan="2" id="suggested_GST_amount" class="borderless hidden_date"></th>
                    <th class="borderless"></th>
                </tr>
                <tr class="hidden_date">
                    <th colspan="10" style="padding-top: 10px; padding-bottom: 10px;">SALES INFORMATION</th>
                    <th class="borderless"></th>
                </tr>
                <tr class="hidden_date">
                    <th style="padding:0px 5px;">No.</th>
                    <th style="padding:0px 5px;">Dept/outlet code</th>
                    <th style=" width: 70px;"><?php echo $sales_names['40010']; ?></th>
                    <th style=" width: 70px;"><?php echo $sales_names['40020']; ?></th>
                    <th style=" width: 70px;"><?php echo $sales_names['40030']; ?></th>
                    <th style=" width: 70px;"><?php echo $sales_names['40510']; ?></th>
                    <th style=" width: 70px;">Service Charge</th>
                    <th style=" width: 70px;">Delivery</th>
                    <th style=" width: 70px;">GST</th>
                    <th style=" width: 70px;">Rounding</th>
                    <th style=" width: 70px;">Sales Total</th>
                </tr>
                </thead>
                <tbody id="sales_table" class="hidden_date">
                <?php foreach ($project_list AS $key => $project) { ?>
                    <tr>
                        <td class="project_id hidden"><?php echo $project['id']; ?></td>
                        <td style="text-align: center;padding:0px 5px;"><?php echo $key+1; ?></td>
                        <td style="padding:0px 5px;"><?php echo $project['name']; ?></td>
                        <td class="td_input"><input class="number_input food_amount"></td>
                        <td class="td_input"><input class="number_input beverage_amount"></td>
                        <td class="td_input"><input class="number_input liquor_amount"></td>
                        <td class="td_input"><input class="number_input others_amount"></td>
                        <td class="td_input"><input class="number_input service_charge_amount"></td>
                        <td class="td_input"><input class="number_input delivery_amount"></td>
                        <td class="td_input"><input class="number_input GST_amount"></td>
                        <td class="td_input"><input class="number_input_negative rounding_amount"></td>
                        <td class="check_bold sales_total"></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>


            <table class="hidden_date">
                <thead>
                <tr>
                    <th colspan="15" style="padding-top: 10px; padding-bottom: 10px;">COLLECTION INFORMATION</th>
                    <th class="borderless"></th>
                </tr>
                <tr>
                    <th style="padding:0px 5px;">No.</th>
                    <th style="padding:0px 5px;">Dept/outlet code</th>
                    <th style=" width: 70px;">Discount</th>
                    <th style=" width: 70px;"><?php echo $debtor_names['14005']; ?></th>
                    <th style=" width: 70px;"><?php echo $debtor_names['14002']; ?></th>
                    <th style=" width: 70px;"><?php echo $debtor_names['14004']; ?></th>
                    <th style=" width: 70px;"><?php echo $debtor_names['14014']; ?></th>
                    <th style=" width: 70px;"><?php echo $debtor_names['14013']; ?></th>
                    <th style=" width: 70px;"><?php echo $debtor_names['14003']; ?></th>
                    <th style=" width: 70px;"><?php echo $debtor_names['14001']; ?></th>
                    <th style=" width: 70px;"><?php echo $debtor_names['14008']; ?></th>
                    <th style=" width: 70px;"><?php echo $debtor_names['14017']; ?></th>
                    <th style=" width: 70px;"><?php echo $debtor_names['14018']; ?></th>
                    <th style=" width: 70px;"><?php echo $debtor_names['14019']; ?></th>
                    <th style=" width: 70px;">Collection Total</th>
                </tr>
                </thead>
                <tbody id="collection_table">
                <?php foreach ($project_list AS $key => $project) { ?>
                    <tr class="project<?php echo $project['id']; ?>">
                        <td style="text-align: center;padding:0px 5px;"><?php echo $key+1; ?></td>
                        <td style="padding:0px 5px;"><?php echo $project['name']; ?></td>
                        <td class="td_input"><input class="number_input discount_amount"></td>
                        <td class="td_input"><input class="number_input cash_amount"></td>
                        <td class="td_input"><input class="number_input visa_card_amount"></td>
                        <td class="td_input"><input class="number_input nets_amount"></td>
                        <td class="td_input"><input class="number_input internal_voucher_amount"></td>
                        <td class="td_input"><input class="number_input external_voucher_amount"></td>
                        <td class="td_input"><input class="number_input master_card_amount"></td>
                        <td class="td_input"><input class="number_input american_express_amount"></td>
                        <td class="td_input"><input class="number_input corporate_amount"></td>
                        <td class="td_input"><input class="number_input ubereats_amount"></td>
                        <td class="td_input"><input class="number_input deliveroo_amount"></td>
                        <td class="td_input"><input class="number_input foodpanda_amount"></td>
                        <td class="check_bold collection_total"></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>

            <div style="text-align: right;" class="hidden_date">
                <button id="submit_sales" type="button" class="btn btn-md blue text-right">Submit</button>

                <button id="all_zero" type="button" class="btn btn-md blue text-right">All Zero</button>

            </div>
        </div>
    </div>

    <div class="modal-backdrop in" style="display: none; z-index: 99999;">
        <div clas="modal in" style="position: absolute;top: 0;left: 0;bottom: 0;right: 0;margin: auto;background-color: rgba(255, 255, 255, 0.8);background-image: url(<?= base_url('public/assets/admin/layout/img/wait.gif') ?>);background-repeat: no-repeat;background-position: center;background-size: 360;z-index: 2000;">

        </div>
    </div>

    <script>

    </script>

    <script src="<?php echo base_url('public/assets/global/plugins/jquery-migrate-1.2.1.min.js') ?>" type="text/javascript"></script>
    <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
    <script src="<?php echo base_url('public/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/global/plugins/bootstrap/js/bootstrap.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/global/plugins/jquery.blockui.min.js') ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>public/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>public/assets/global/plugins/datatables-yadcf/jquery.dataTables.yadcf.js"></script>
    <script src="<?php echo base_url('public/assets/global/plugins/jquery.cokie.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/global/plugins/uniform/jquery.uniform.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') ?>" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/jquery-validation/js/additional-methods.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/select2/select2.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/parsley/parsley.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/parsley/parsley.remote.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/bootbox/bootbox.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/typeahead/handlebars.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/typeahead/typeahead.bundle.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/slider-pips/jquery-ui-slider-pips.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/slider-pips/jquery.blockUI.js') ?>"></script>
    <script src="<?php echo base_url('public/assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url();?>public/assets/global/plugins/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>public/assets/global/plugins/ckeditor/ckeditor.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <script src="<?php echo base_url('public/assets/global/scripts/metronic.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/admin/layout/scripts/layout.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/admin/pages/scripts/component-picker.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/admin/pages/scripts/component-form-tool.js') ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/admin/includes/js/moment.js') ?>"></script>
    <script  type="text/javascript" src="<?php echo base_url('public/assets/admin/includes/js/datetime-moment.js') ?>"></script>
    <script  type="text/javascript" src="<?php echo base_url('public/assets/admin/pages/scripts/jquery.treegrid.min.js') ?>"></script>

    <!-- 20160929 vernhui IRAS -->
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/jquery.formatCurrency-1.4.0.pack.js') ?>"></script>

    <script src="<?php echo base_url('public/assets/global/scripts/jquery.multi-select.js') ?>"></script>

    <script>
        function confirmLink(m,u) {
            if ( confirm(m) ) {
                window.location = u;
            }
        }
        jQuery(document).ready(function(){

            var editable = '<?php echo $editable; ?>';
            var locked_financial_month = '<?php echo $locked_next_financial_month; ?>';

            $('.hidden_date').hide();

            $('.date-picker').datepicker({
                format: "yyyy-mm-dd"
            });

            $(".number_input").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

            $(".number_input_negative").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 189]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

            $('#show_error_message').hide();

            $("#sales_date").on("change",function(){
                var el = $(this);

                var project_id = $("#sales_table .project_id").html();
                $("#suggested_GST_amount").html('');


                if (el.val() != '') {

                    $('.hidden_date').show();

                    $.ajax({
                        method: 'POST',
                        url: "<?php echo base_url(); ?>sales_input/get_data",
                        data: {
                            sales_date: el.val(),
                            project_id: project_id
                        },
                        dataType: 'json'
                    }).done(function(res) {

                        var total_sales = 0;

                        if(res != null) {

                            if (res.food_amount != null) {
                                $(".food_amount").val(numberWithCommas(parseFloat(res.food_amount).toFixed(2)));
                                total_sales += parseFloat(res.food_amount);
                            }

                            if (res.beverage_amount != null) {
                                $(".beverage_amount").val(numberWithCommas(parseFloat(res.beverage_amount).toFixed(2)));
                                total_sales += parseFloat(res.beverage_amount);
                            }

                            if (res.liquor_amount != null) {
                                $(".liquor_amount").val(numberWithCommas(parseFloat(res.liquor_amount).toFixed(2)));
                                total_sales += parseFloat(res.liquor_amount);
                            }

                            if (res.others_amount != null) {
                                $(".others_amount").val(numberWithCommas(parseFloat(res.others_amount).toFixed(2)));
                                total_sales += parseFloat(res.others_amount);
                            }

                            if (res.GST_amount != null) {
                                $(".GST_amount").val(numberWithCommas(parseFloat(res.GST_amount).toFixed(2)));
                                total_sales += parseFloat(res.GST_amount);
                            }

                            if (res.service_charge_amount != null) {
                                $(".service_charge_amount").val(numberWithCommas(parseFloat(res.service_charge_amount).toFixed(2)));
                                total_sales += parseFloat(res.service_charge_amount);
                            }

                            if (res.delivery_amount != null) {
                                $(".delivery_amount").val(numberWithCommas(parseFloat(res.delivery_amount).toFixed(2)));
                                total_sales += parseFloat(res.delivery_amount);
                            }

                            if (res.adjustment_amount != null) {
                                $(".rounding_amount").val(numberWithCommas(parseFloat(res.adjustment_amount).toFixed(2)));
                                total_sales += parseFloat(res.adjustment_amount);
                            }

                            $(".sales_total").html(numberWithCommas(parseFloat(total_sales).toFixed(2)));

                            $(".collection_total").html(numberWithCommas(parseFloat(total_sales).toFixed(2)));

                            if (res.discount_amount != null) {
                                $(".discount_amount").val(numberWithCommas(parseFloat(res.discount_amount).toFixed(2)));
                            }

                            if (res.cash_amount != null) {
                                $(".cash_amount").val(numberWithCommas(parseFloat(res.cash_amount).toFixed(2)));
                            }

                            if (res.visa_card_amount != null) {
                                $(".visa_card_amount").val(numberWithCommas(parseFloat(res.visa_card_amount).toFixed(2)));
                            }

                            if (res.nets_amount != null) {
                                $(".nets_amount").val(numberWithCommas(parseFloat(res.nets_amount).toFixed(2)));
                            }

                            if (res.internal_voucher_amount != null) {
                                $(".internal_voucher_amount").val(numberWithCommas(parseFloat(res.internal_voucher_amount).toFixed(2)));
                            }

                            if (res.external_voucher_amount != null) {
                                $(".external_voucher_amount").val(numberWithCommas(parseFloat(res.external_voucher_amount).toFixed(2)));
                            }

                            if (res.master_card_amount != null) {
                                $(".master_card_amount").val(numberWithCommas(parseFloat(res.master_card_amount).toFixed(2)));
                            }

                            if (res.american_express_amount != null) {
                                $(".american_express_amount").val(numberWithCommas(parseFloat(res.american_express_amount).toFixed(2)));
                            }

                            if (res.corporate_amount != null) {
                                $(".corporate_amount").val(numberWithCommas(parseFloat(res.corporate_amount).toFixed(2)));
                            }

                            if (res.foodpanda_amount != null) {
                                $(".foodpanda_amount").val(numberWithCommas(parseFloat(res.foodpanda_amount).toFixed(2)));
                            }

                            if (res.ubereats_amount != null) {
                                $(".ubereats_amount").val(numberWithCommas(parseFloat(res.ubereats_amount).toFixed(2)));
                            }

                            if (res.deliveroo_amount != null) {
                                $(".deliveroo_amount").val(numberWithCommas(parseFloat(res.deliveroo_amount).toFixed(2)));
                            }

                            if (editable == 'cannot') {
                                $('#sales_table input, #collection_table input').prop("disabled", true);
                                $('#submit_sales').hide();
                                $('#all_zero').hide();
                            }
                        } else {

                            $('#sales_table input, #collection_table input').val('');
                            $('.sales_total, .collection_total').html('');

                            if (new Date(locked_financial_month) > new Date(el.val())) {

                                $('#sales_table input, #collection_table input').prop("disabled", true);
                                $('#submit_sales').hide();
                                $('#all_zero').hide();

                            } else {

                                $('#sales_table input, #collection_table input').prop("disabled", false);
                                $('#submit_sales').show();
                                $('#all_zero').show();
                            }
                        }

                    });

                    if (new Date(locked_financial_month) > new Date(el.val())) {

                        $('#sales_table input, #collection_table input').prop("disabled", true);
                        $('#submit_sales').hide();
                        $('#all_zero').hide();

                    }

                } else {
                    location.reload();
                }
            });

            $('#all_zero').on("click",function(){

                $('#sales_table input').val('0.00');

                $('#collection_table input').val('0.00');

                $('.sales_total').html('0.00');

                $('.collection_total').html('0.00');

            });

            $('#submit_sales').on("click",function(){

                $(this).prop("disabled", true);

                var sales_date = $('#sales_date').val();

                var date_error = false;
                if (sales_date == '') {
                    $('#required_payroll_month').show();
                    date_error = true;
                } else {
                    $('#required_payroll_month').hide();


                    if (editable == 'cannot') {
                        $.ajax({
                            async: false,
                            method: 'POST',
                            url: "<?php echo base_url(); ?>sales_input/check_date",
                            data: {
                                sales_date: sales_date
                            },
                            dataType: 'json'
                        }).done(function (res) {

                            if (res.error == true) {
                                date_error = true;
                                $('#required_date_payroll_month').show();
                                $(this).prop("disabled", false);
                                return;
                            } else {
                                $('#required_date_payroll_month').hide();
                            }

                        });
                    }

                }
                if (date_error == true) {
                    $(this).prop("disabled", false);
                    return;
                }

                var error = false;

                var outlets = [];

                $('#sales_table tr').each(function() {

                    var outlet = {};
                    var el = $(this);

                    var sales_total = el.find('.sales_total').html().replace(/,/g, "");
                    var project_id = el.find('.project_id').html();

                    outlet.project_id = project_id;

                    var collection_total = $('#collection_table .project' + project_id).find('.collection_total').html().replace(/,/g, "");

                    if (parseFloat(sales_total) != parseFloat(collection_total)) {
                        error = true;
                        $('#collection_table .project'+project_id).find('.collection_total').css('color', 'red');
                        el.find('.sales_total').css('color', 'red');
                    } else {
                        $('#collection_table .project'+project_id).find('.collection_total').css('color', '#333333');
                        el.find('.sales_total').css('color', '#333333');
                    }

                    var food_amount = el.find('.food_amount').val();
                    if (food_amount != '') {
                        outlet.food_amount = parseFloat(food_amount.replace(/,/g, ""));
                    }

                    var beverage_amount = el.find('.beverage_amount').val();
                    if (beverage_amount != '') {
                        outlet.beverage_amount = parseFloat(beverage_amount.replace(/,/g, ""));
                    }

                    var liquor_amount = el.find('.liquor_amount').val();
                    if (liquor_amount != '') {
                        outlet.liquor_amount = parseFloat(liquor_amount.replace(/,/g, ""));
                    }

                    var others_amount = el.find('.others_amount').val();
                    if (others_amount != '') {
                        outlet.others_amount = parseFloat(others_amount.replace(/,/g, ""));
                    }

                    var GST_amount = el.find('.GST_amount').val();
                    if (GST_amount != '') {
                        outlet.GST_amount = parseFloat(GST_amount.replace(/,/g, ""));
                    }

                    var service_charge_amount = el.find('.service_charge_amount').val();
                    if (service_charge_amount != '') {
                        outlet.service_charge_amount = parseFloat(service_charge_amount.replace(/,/g, ""));
                    }

                    var delivery_amount = el.find('.delivery_amount').val();
                    if (delivery_amount != '') {
                        outlet.delivery_amount = parseFloat(delivery_amount.replace(/,/g, ""));
                    }

                    var rounding_amount = el.find('.rounding_amount').val();
                    if (rounding_amount != '') {
                        outlet.rounding_amount = parseFloat(rounding_amount.replace(/,/g, ""));
                    }

                    var discount_amount = $('#collection_table .project'+project_id).find('.discount_amount').val();
                    if (discount_amount != '') {
                        outlet.discount_amount = parseFloat(discount_amount.replace(/,/g, ""));
                    }

                    var cash_amount = $('#collection_table .project'+project_id).find('.cash_amount').val();
                    if (cash_amount != '') {
                        outlet.cash_amount = parseFloat(cash_amount.replace(/,/g, ""));
                    }

                    var visa_card_amount = $('#collection_table .project'+project_id).find('.visa_card_amount').val();
                    if (visa_card_amount != '') {
                        outlet.visa_card_amount = parseFloat(visa_card_amount.replace(/,/g, ""));
                    }

                    var nets_amount = $('#collection_table .project'+project_id).find('.nets_amount').val();
                    if (nets_amount != '') {
                        outlet.nets_amount = parseFloat(nets_amount.replace(/,/g, ""));
                    }

                    var internal_voucher_amount = $('#collection_table .project'+project_id).find('.internal_voucher_amount').val();
                    if (internal_voucher_amount != '') {
                        outlet.internal_voucher_amount = parseFloat(internal_voucher_amount.replace(/,/g, ""));
                    }

                    var external_voucher_amount = $('#collection_table .project'+project_id).find('.external_voucher_amount').val();
                    if (external_voucher_amount != '') {
                        outlet.external_voucher_amount = parseFloat(external_voucher_amount.replace(/,/g, ""));
                    }

                    var master_card_amount = $('#collection_table .project'+project_id).find('.master_card_amount').val();
                    if (master_card_amount != '') {
                        outlet.master_card_amount = parseFloat(master_card_amount.replace(/,/g, ""));
                    }

                    var american_express_amount = $('#collection_table .project'+project_id).find('.american_express_amount').val();
                    if (american_express_amount != '') {
                        outlet.american_express_amount = parseFloat(american_express_amount.replace(/,/g, ""));
                    }

                    var corporate_amount = $('#collection_table .project'+project_id).find('.corporate_amount').val();
                    if (corporate_amount != '') {
                        outlet.corporate_amount = parseFloat(corporate_amount.replace(/,/g, ""));
                    }

                    var ubereats_amount = $('#collection_table .project'+project_id).find('.ubereats_amount').val();
                    if (ubereats_amount != '') {
                        outlet.ubereats_amount = parseFloat(ubereats_amount.replace(/,/g, ""));
                    }

                    var deliveroo_amount = $('#collection_table .project'+project_id).find('.deliveroo_amount').val();
                    if (deliveroo_amount != '') {
                        outlet.deliveroo_amount = parseFloat(deliveroo_amount.replace(/,/g, ""));
                    }

                    var foodpanda_amount = $('#collection_table .project'+project_id).find('.foodpanda_amount').val();
                    if (foodpanda_amount != '') {
                        outlet.foodpanda_amount = parseFloat(foodpanda_amount.replace(/,/g, ""));
                    }

                    outlets.push(outlet);
                });

                if (error == true) {

                    $('#show_error_message').show();
                    $('#error_message').html('Total Sales and Total Collection should be equal');
                    $(this).prop("disabled", false);
                    return;

                } else {

                    $('#show_error_message').hide();
                }

                //doing

                $.ajax({
                    method: 'POST',
                    url: "<?php echo base_url(); ?>sales_input/submit_sales",
                    data: {
                        outlets: outlets,
                        sales_date: sales_date
                    },
                    dataType: 'json'
                }).done(function(res) {

                    if (res.success == true) {
                        window.location.reload();
                    }
                });
            });

            function numberWithCommas(x) {
                var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return parts.join(".");
            }

            $("#sales_table input").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));

                } else {
                    el.val('0.00');
                }

                var food_amount = $('.food_amount').val();
                food_amount = food_amount != '' ? parseFloat(food_amount.replace(/,/g, "")) : 0;

                var beverage_amount = $('.beverage_amount').val();
                beverage_amount = beverage_amount != '' ? parseFloat(beverage_amount.replace(/,/g, "")) : 0;

                var liquor_amount = $('.liquor_amount').val();
                liquor_amount = liquor_amount != '' ? parseFloat(liquor_amount.replace(/,/g, "")) : 0;

                var others_amount = $('.others_amount').val();
                others_amount = others_amount != '' ? parseFloat(others_amount.replace(/,/g, "")) : 0;

                var service_charge_amount = $('.service_charge_amount').val();
                service_charge_amount = service_charge_amount != '' ? parseFloat(service_charge_amount.replace(/,/g, "")) : 0;

                var delivery_amount = $('.delivery_amount').val();
                delivery_amount = delivery_amount != '' ? parseFloat(delivery_amount.replace(/,/g, "")) : 0;

                var discount_amount = $('.discount_amount').val();
                discount_amount = discount_amount != '' ? parseFloat(discount_amount.replace(/,/g, "")) : 0;

                var total_sales = food_amount+beverage_amount+liquor_amount+others_amount+service_charge_amount+delivery_amount-discount_amount;
                var calculated_GST = total_sales*7/100;

                $('#suggested_GST_amount').html(numberWithCommas(calculated_GST.toFixed(2)));

                var tr = el.closest('tr');

                var total_amount = 0;
                tr.find('input').each(function() {
                    var el1 = $(this);
                    var amount = el1.val().replace(/,/g, "");

                    if (amount != '') {
                        total_amount += parseFloat(amount);
                    }
                });

                tr.find('.sales_total').html(numberWithCommas(total_amount.toFixed(2)));
            });

            $("#collection_table input").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));

                } else {
                    el.val('0.00');
                }

                var food_amount = $('.food_amount').val();
                food_amount = food_amount != '' ? parseFloat(food_amount.replace(/,/g, "")) : 0;

                var beverage_amount = $('.beverage_amount').val();
                beverage_amount = beverage_amount != '' ? parseFloat(beverage_amount.replace(/,/g, "")) : 0;

                var liquor_amount = $('.liquor_amount').val();
                liquor_amount = liquor_amount != '' ? parseFloat(liquor_amount.replace(/,/g, "")) : 0;

                var others_amount = $('.others_amount').val();
                others_amount = others_amount != '' ? parseFloat(others_amount.replace(/,/g, "")) : 0;

                var service_charge_amount = $('.service_charge_amount').val();
                service_charge_amount = service_charge_amount != '' ? parseFloat(service_charge_amount.replace(/,/g, "")) : 0;

                var delivery_amount = $('.delivery_amount').val();
                delivery_amount = delivery_amount != '' ? parseFloat(delivery_amount.replace(/,/g, "")) : 0;

                var discount_amount = $('.discount_amount').val();
                discount_amount = discount_amount != '' ? parseFloat(discount_amount.replace(/,/g, "")) : 0;

                var total_sales = food_amount+beverage_amount+liquor_amount+others_amount+service_charge_amount+delivery_amount-discount_amount;
                var calculated_GST = total_sales*7/100;

                $('#suggested_GST_amount').html(numberWithCommas(calculated_GST.toFixed(2)));

                var tr = el.closest('tr');

                var total_amount = 0;
                tr.find('input').each(function() {
                    var el1 = $(this);
                    var amount = el1.val().replace(/,/g, "");

                    if (amount != '') {
                        total_amount += parseFloat(amount);
                    }
                });

                tr.find('.collection_total').html(numberWithCommas(total_amount.toFixed(2)));
            });

            $("#selLimit").on("change",function(){
                <?php
                if(isset($sort_by) && isset($order)){
                $sort_parameter['sort_by'] = $sort_by;
                $sort_parameter['page'] = 1;
                $sort_parameter['order'] = $order;
                ?>
                window.location.href = "<?php echo $base_url_pagination?>&limit="+$(this).val();
                <?php
                }
                ?>

            });

            // 20160127 vernhui Enable user to change password for their payslips
            window.close_modal_password = function(msg){
                $('#home_page_modal').modal('hide');
                bootbox.alert(msg);
            };
        });
    </script>
    <script type="text/javascript">
        $('.tree').treegrid({
            initialState: 'collapsed'
        });

        $('body').on("submit",function(){

            $(".modal-backdrop.in").show();

            setInterval(function(){
                $(".modal-backdrop.in").hide();
            }, 1500);

        });

        $.ajaxSetup({
            beforeSend: function() {
                $(".modal-backdrop.in").show();
            },
            complete: function() {
                $(".modal-backdrop.in").hide();
            }
        });
    </script>
</body>
</html>