<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Accounting System</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>


    <link href="<?php echo base_url('public/assets/global/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/global/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/global/plugins/uniform/css/uniform.default.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css"/>

    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/select2/select2.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/datatables/plugins/integration/bootstrap/3/dataTables.bootstrap.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/datatables-yadcf/jquery.dataTables.yadcf.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/bootstrap-datepicker/css/datepicker3.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/bootstrap-datetimepicker/css/datetimepicker.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/typeahead/typeahead.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/ion.rangeslider/css/ion.rangeSlider.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/ion.rangeslider/css/ion.rangeSlider.Metronic.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/slider-pips/jquery-ui-slider-pips.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/bootstrap-summernote/summernote.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/admin/pages/css/jquery.treegrid.css') ?>">

    <!-- END PAGE LEVEL STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo base_url('public/assets/global/css/components.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/global/css/plugins.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/global/css/fix.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/admin/layout/css/layout.css') ?>" rel="stylesheet" type="text/css"/>
    <link id="style_color" href="<?php echo base_url('public/assets/admin/layout/css/themes/default.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/admin/layout/css/custom.css') ?>" rel="stylesheet" type="text/css"/>
    <!-- plus a jQuery UI theme, here I use "flick" -->
    <link rel="stylesheet" href="<?php echo base_url('public/assets/global/plugins/slider-pips/jquery-ui.css') ?>">
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="<?php echo base_url('favicon.ico') ?>"/>
    <link href="<?php echo base_url('public/assets/global/css/multi-select.css') ?>" rel="stylesheet" type="text/css"/>

    <script src="<?php echo base_url('public/assets/global/plugins/jquery-1.11.0.min.js') ?>" type="text/javascript"></script>

    <script type="text/javascript">
        var base_url = '<?= base_url()?>';


    </script>
    <style type="text/css">
        table {
            color: #333333;
            border-collapse: collapse;
            table-layout: fixed;
            word-wrap: break-word;
            margin: 20px auto;
        }
        th{
            text-align: center;
        }
        th, td{
            border: 1px solid #666666;
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .borderless{
            border: none;
        }
        input{
            text-align: right;
        }
        .check_bold{
            background: #cccccc; font-weight: bold; text-align: right;
            padding-right: 3px;
            -webkit-box-sizing:  border-box; /* Safari/Chrome, other WebKit */
            -moz-box-sizing:  border-box;    /* Firefox, other Gecko */
            box-sizing:  border-box;
        }
        .td_input{
            background: #dce6f1;
        }
        .td_input input{
            width: 70px;
            padding-right: 3px;
            -webkit-box-sizing:  border-box; /* Safari/Chrome, other WebKit */
            -moz-box-sizing:  border-box;    /* Firefox, other Gecko */
            box-sizing:  border-box;
        }
    </style>
</head>
<body>

<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="<?php echo base_url(); ?>">
                <div class="row logo-default" style="color: #999999;" >Accounting System</div>
            </a>
            <div class="menu-toggler sidebar-toggler">
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN HORIZANTAL MENU -->
        <!-- DOC: Remove "hor-menu-light" class to have a horizontal menu with theme background instead of white background -->
        <!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) in the responsive menu below along with sidebar menu. So the horizontal menu has 2 seperate versions -->

        <!-- END HEADER SEARCH BOX -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
           data-target=".navbar-collapse">
        </a>
        <div style="position:absolute;top:15px;margin-left:50%;margin-right:50%;width:400px;color:white">
            <?php echo $current_company_data['name']; ?>
        </div>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">

                <!-- BEGIN USER LOGIN DROPDOWN -->
                <li class="dropdown dropdown-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                        <span class="username username-hide-on-mobile">
                        <?php
                        $adminData = $this->session->userData("user_data");
                        if(isset($adminData['username'])){
                            echo $adminData["username"];
                        }else{
                            echo $adminData["code"]." - ".$adminData["full_name"];
                        }
                        ?>
                        </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo base_url('user/change_password'); ?>">
                                <i class="icon-lock"></i>Change Login Password </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url("AuthController/logout/admin"); ?>">
                                <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul>
                </li>

                <!-- END USER LOGIN DROPDOWN -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->

<div class="page-content-wrapper">
    <div style="padding: 70px 20px 10px 20px;background-color: #fff;"><!-- BEGIN PAGE HEADER-->
        <h3 class="page-title">
            Sales Input
        </h3>

        <?php if (isset($flash_message)) { ?>
        <div class="row margin-bottom-20">
            <div class="col-xs-12">
                <div class="alert alert-success">
                    <button class="close" data-close="alert"></button>
                    <?php echo $flash_message; ?>
                </div>
            </div>
        </div>
        <?php } ?>

        <div class="row">
            <div class="col-xs-12">
                <div class="portlet box blue hoki">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-tasks"></i>
                                Outlet Selection
                        </div>
                    </div>
                    <div class="portlet-body" style="">
                        <div class="tiles">
                        <?php foreach ($project_list AS $item) { ?>

                            <a class="tile  bg-green-seagreen" href="<?php echo base_url("sales_input/index/".$item['id']) ?>">
                                <div class="tile-body">
                                    <i class="fa fa-list"></i>
                                </div>
                                <div class="tile-object text-center">
                                    Sales Input<br/>
                                    <?php echo $item['name'].' ('.$item['code'].')'; ?>
                                </div>
                            </a>

                            <a class="tile  bg-yellow-gold" href="<?php echo base_url("sales_report/index/".$item['id']) ?>">
                                <div class="tile-body">
                                    <i class="fa fa-list"></i>
                                </div>
                                <div class="tile-object text-center">
                                    Sales Report<br/>
                                    <?php echo $item['name'].' ('.$item['code'].')'; ?>
                                </div>
                            </a>
                        <?php } ?>

                            <a class="tile  bg-red-flamingo" href="<?php echo base_url("salary_import/") ?>">
                                <div class="tile-body">
                                    <i class="fa fa-sitemap"></i>
                                </div>
                                <div class="tile-object text-center">
                                    Salary Import
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- END SCRIPT -->

    </div>
</div>

<script>

</script>

<script src="<?php echo base_url('public/assets/global/plugins/jquery-migrate-1.2.1.min.js') ?>" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<?php echo base_url('public/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/assets/global/plugins/bootstrap/js/bootstrap.min.js')?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/assets/global/plugins/jquery.blockui.min.js') ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url() ?>public/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>public/assets/global/plugins/datatables-yadcf/jquery.dataTables.yadcf.js"></script>
<script src="<?php echo base_url('public/assets/global/plugins/jquery.cokie.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/assets/global/plugins/uniform/jquery.uniform.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') ?>" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/jquery-validation/js/additional-methods.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/select2/select2.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/parsley/parsley.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/parsley/parsley.remote.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/bootbox/bootbox.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/typeahead/handlebars.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/typeahead/typeahead.bundle.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/slider-pips/jquery-ui-slider-pips.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/slider-pips/jquery.blockUI.js') ?>"></script>
<script src="<?php echo base_url('public/assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript') ?>"></script>
<script type="text/javascript" src="<?php echo base_url();?>public/assets/global/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>public/assets/global/plugins/ckeditor/ckeditor.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script src="<?php echo base_url('public/assets/global/scripts/metronic.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/assets/admin/layout/scripts/layout.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/assets/admin/pages/scripts/component-picker.js') ?>" type="text/javascript"></script>
<script src="<?php echo base_url('public/assets/admin/pages/scripts/component-form-tool.js') ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url('public/assets/admin/includes/js/moment.js') ?>"></script>
<script  type="text/javascript" src="<?php echo base_url('public/assets/admin/includes/js/datetime-moment.js') ?>"></script>
<script  type="text/javascript" src="<?php echo base_url('public/assets/admin/pages/scripts/jquery.treegrid.min.js') ?>"></script>

<!-- 20160929 vernhui IRAS -->
<script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/jquery.formatCurrency-1.4.0.pack.js') ?>"></script>

<script src="<?php echo base_url('public/assets/global/scripts/jquery.multi-select.js') ?>"></script>

<script>
    function confirmLink(m,u) {
        if ( confirm(m) ) {
            window.location = u;
        }
    }
    jQuery(document).ready(function() {
        Metronic.init(); // init metronic core components
        Layout.init(); // init current layout
        ComponentsPickers.init();
        ComponentsFormTools.init();
        //ComponentsIonSliders.init();
        //$('.summernote').summernote({height: 300});
    });
</script>
<script type="text/javascript">
    $('.tree').treegrid({
        initialState: 'collapsed'
    });
</script>
</body>
</html>