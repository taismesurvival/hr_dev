<style>
    table { border-collapse: collapse; }
    tr.border-top { border-top: solid thin; }
    tr.border-bottom { border-bottom: solid thin; }
    tr.bold { font-weight: bold; }
    * { font-size: 14px; font-family: Lucida Sans Unicode; }
</style>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/slider-pips/jquery-ui.js"></script>
<button type="button" id="download_excel_file">Download Excel Report</button>
<button type="button" id="download_pdf_file">Print or Download PDF Report</button>
<div id="table_wrapper">
    <table style='width:100%;'>

        <tr>
            <td style='text-align:left;'>
                <table style='color:blue;width:100%;'>
                    <tr>
                        <td style='font-size:20px;text-align:center;'><?php echo $company_name; ?></td>
                    </tr>
                    <tr>
                        <td style='font-size:16px;text-align:center;'><?php echo $ending_month; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style='text-align:center;font-size:18px;'>General Listing Report</td>
        </tr>
    </table>
    <table style='width:100%;'>
        <thead>
        <tr class='border-top'>
            <th style='width:5%;'>Reference No</th>
            <th style='width:5%;'>Account Code</th>
            <th style='width:5%; text-align:center;'>Account Name</th>
            <th style='width:5%; text-align:center;'>Debit</th>
            <th style='width:5%; text-align:center;'>Credit</th>
            <th style='width:5%; text-align:center;'>Outlet</th>
        </tr>
        </thead>
        <tbody id="accounts_table">
        <?php foreach ($GL_entries_data AS $key => $item) { ?>

            <?php if ($GL_entries_data[$key]['GL_entry_id'] != $GL_entries_data[$key-1]['GL_entry_id']) { ?>

                <tr class='border-top' style="font-weight: bold;">
                    <td style='text-align:center;'><?php echo $item['description']; ?></td>
                    <td style='text-align:center;'><?php echo $item['document_date']; ?></td>
                    <td style='text-align:center;'><?php echo $item['type']; ?></td>
                    <td colspan="3"></td>
                </tr>

            <?php } ?>
            <tr class="credit_debit">
                <td style='text-align:center;'><?php echo 'GE'.$item['GL_entry_id']; ?></td>
                <td style='text-align:center;'><?php echo $item['account_code']; ?></td>
                <td style='text-align:center;'><?php echo $item['account_name']; ?></td>
                <td style='text-align:center;' class="debit"><?php echo $item['GL_type'] == 'debit' ? $item['amount'] : ''; ?></td>
                <td class="credit" style='text-align:center;'><?php echo $item['GL_type'] == 'credit' ? $item['amount'] : ''; ?></td>
                <td style='text-align:center;'><?php echo $item['project_name'].' ('.$item['project_code'].')'; ?></td>
            </tr>
        <?php } ?>

        <?php foreach ($GL_taxable_data AS $key => $item) { ?>

            <tr class='border-top' style="font-weight: bold;">
                <td style='text-align:center;'><?php echo $item['description']; ?></td>
                <td style='text-align:center;'><?php echo $item['document_date']; ?></td>
                <td style='text-align:center;'><?php echo $item['supplier_name']; ?></td>
                <td style='text-align:center;'><?php echo $item['refference_no']; ?></td>
                <td colspan="2"></td>
            </tr>

            <?php foreach ($item['project_split'] AS $project) { ?>
            <tr class="credit_debit">
                <td style='text-align:center;'><?php echo 'GT'.$key; ?></td>
                <td style='text-align:center;'><?php echo $project['account_code']; ?></td>
                <td style='text-align:center;'><?php echo $project['account_name']; ?></td>
                <td style='text-align:center;' class="debit"><?php echo $project['GL_type'] == 'debit' ? $project['amount'] : ''; ?></td>
                <td class="credit" style='text-align:center;'><?php echo $project['GL_type'] == 'credit' ? $project['amount'] : ''; ?></td>
                <td style='text-align:center;'><?php echo $project['project_name'].' ('.$project['project_code'].')'; ?></td>
            </tr>
        <?php }} ?>

        <?php foreach ($purchases_data AS $key => $item) { ?>

            <tr class='border-top' style="font-weight: bold;">
                <td style='text-align:center;'><?php echo $item['description']; ?></td>
                <td style='text-align:center;'><?php echo $item['document_date']; ?></td>
                <td style='text-align:center;'><?php echo $item['supplier_name']; ?></td>
                <td style='text-align:center;'><?php echo $item['refference_no']; ?></td>
                <td colspan="2"></td>
            </tr>

            <?php foreach ($item['project_split'] AS $project) { ?>
                <tr class="credit_debit">
                    <td style='text-align:center;'><?php echo 'PE'.$key; ?></td>
                    <td style='text-align:center;'><?php echo $project['account_code']; ?></td>
                    <td style='text-align:center;'><?php echo $project['account_name']; ?></td>
                    <td style='text-align:center;' class="debit"><?php echo $project['GL_type'] == 'debit' ? $project['amount'] : ''; ?></td>
                    <td class="credit" style='text-align:center;'><?php echo $project['GL_type'] == 'credit' ? $project['amount'] : ''; ?></td>
                    <td style='text-align:center;'><?php echo $project['project_name'].' ('.$project['project_code'].')'; ?></td>
                </tr>
            <?php }} ?>

        <?php foreach ($credit_notes_data AS $key => $item) { ?>

            <tr class='border-top' style="font-weight: bold;">
                <td style='text-align:center;'><?php echo $item['description']; ?></td>
                <td style='text-align:center;'><?php echo $item['document_date']; ?></td>
                <td style='text-align:center;'><?php echo $item['supplier_name']; ?></td>
                <td style='text-align:center;'><?php echo $item['refference_no']; ?></td>
                <td colspan="2"></td>
            </tr>

            <?php foreach ($item['project_split'] AS $project) { ?>
                <tr class="credit_debit">
                    <td style='text-align:center;'><?php echo 'CR'.$key; ?></td>
                    <td style='text-align:center;'><?php echo $project['account_code']; ?></td>
                    <td style='text-align:center;'><?php echo $project['account_name']; ?></td>
                    <td style='text-align:center;' class="debit"><?php echo $project['GL_type'] == 'debit' ? $project['amount'] : ''; ?></td>
                    <td class="credit" style='text-align:center;'><?php echo $project['GL_type'] == 'credit' ? $project['amount'] : ''; ?></td>
                    <td style='text-align:center;'><?php echo $project['project_name'].' ('.$project['project_code'].')'; ?></td>
                </tr>
            <?php }} ?>

        <?php foreach ($bank_recons_data AS $key => $item) { ?>

            <tr class='border-top' style="font-weight: bold;">
                <td style='text-align:center;'><?php echo $item['description']; ?></td>
                <td style='text-align:center;'><?php echo $item['document_date']; ?></td>
                <td style='text-align:center;'><?php echo $item['refference_no']; ?></td>
                <td colspan="3"></td>
            </tr>

            <?php foreach ($item['project_split'] AS $project) { ?>
                <tr class="credit_debit">
                    <td style='text-align:center;'><?php echo 'BR'.$key; ?></td>
                    <td style='text-align:center;'><?php echo $project['account_code']; ?></td>
                    <td style='text-align:center;'><?php echo $project['account_name']; ?></td>
                    <td style='text-align:center;' class="debit"><?php echo $project['GL_type'] == 'debit' ? $project['amount'] : ''; ?></td>
                    <td class="credit" style='text-align:center;'><?php echo $project['GL_type'] == 'credit' ? $project['amount'] : ''; ?></td>
                    <td style='text-align:center;'><?php echo $project['project_name'].' ('.$project['project_code'].')'; ?></td>
                </tr>
            <?php }} ?>

        <?php foreach ($sales_collection_submits_data AS $key => $item) { ?>

            <tr class='border-top' style="font-weight: bold;">
                <td style='text-align:center;'><?php echo $item['payment_method']; ?></td>
                <td style='text-align:center;'><?php echo $item['document_date']; ?></td>
                <td colspan="4"></td>
            </tr>

            <?php foreach ($item['project_split'] AS $project) { ?>
                <tr class="credit_debit">
                    <td style='text-align:center;'><?php echo 'SC'.$key; ?></td>
                    <td style='text-align:center;'><?php echo $project['account_code']; ?></td>
                    <td style='text-align:center;'><?php echo $project['account_name']; ?></td>
                    <td style='text-align:center;' class="debit"><?php echo $project['GL_type'] == 'debit' ? $project['amount'] : ''; ?></td>
                    <td class="credit" style='text-align:center;'><?php echo $project['GL_type'] == 'credit' ? $project['amount'] : ''; ?></td>
                    <td style='text-align:center;'><?php echo $project['project_name'].' ('.$project['project_code'].')'; ?></td>
                </tr>
            <?php }} ?>

        </tbody>
        <tbody>
        <tr id="accounts_total">
            <td colspan="3" style='text-align:right;font-weight: bold;font-size: 15px;' class='border-top'>Grand Total: </td>
            <td style='text-align:center;' class="debit"></td>
            <td style='text-align:center;' class="credit"></td>
            <td></td>
        </tr>
        </tbody>
    </table>
</div>
<script>

    jQuery(document).ready(function () {

        var debit_total = 0;
        $('#accounts_table .credit_debit').each(function() {
            var el = $(this);

            var trans = el.find('.debit').html();

            if (trans != '') {
                debit_total += parseFloat(trans);
            }
        });

        $('#accounts_total .debit').html((debit_total.toFixed(2)));

        var credit_total = 0;
        $('#accounts_table .credit_debit').each(function() {
            var el = $(this);

            var trans = el.find('.credit').html();

            if (trans != '') {
                credit_total += parseFloat(trans);
            }
        });

        $('#accounts_total .credit').html((credit_total.toFixed(2)));

        $('.debit, .credit').each(function() {
            var el = $(this);

            var html_amount = el.html();

            if (html_amount != '') {
                el.html(numberWithCommas(parseFloat(html_amount).toFixed(2)));
            }
        });

        function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }
    });

    $(document).ready(function() {
        $("#download_excel_file").click(function(e) {
            e.preventDefault();

            //getting data from our table
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('table_wrapper');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');

            var a = document.createElement('a');
            a.href = data_type + ', ' + table_html;
            a.download = 'general_listing_report.xls';
            a.click();
        });

        $('body').mouseover(function () {
            $("#download_excel_file").show();
            $("#download_pdf_file").show();
        });

        $("#download_pdf_file").click(function () {
            $("#download_excel_file").hide();
            $("#download_pdf_file").hide();
            window.print();
        });
    });
</script>