<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Bank Statement Upload
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Bank Statement Upload</a>
        </li>
</div>
<!-- END PAGE HEADER -->
<!-- BEGIN ALERT BOX -->
<?php if (isset($flash_message)) { ?>
    <div class="row margin-bottom-20">
        <div class="col-xs-12">
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END ALERT BOX -->

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Bank Statement Upload</span>
                </div>
                <a href="#" class="user-add btn btn-md blue pull-right"><i class="fa fa-plus"></i> Add New</a>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-12">

                        <!-- Begin: life time stats -->
                        <div class="table-container">
                            <!-- Begin: life time stats -->
                            <div class="clearfix mt15">

                                <form method="get" class="form" role="form">
                                    <div class="row">
                                        <div class="col-md-3 col-xs-12">
                                            <label>Month</label>
                                            <div class="form-group">
                                                <div class="input-group date month-picker" data-date-format="yyyy-mm"
                                                     data-date-viewmode="years">
                                                    <input name="month" type="text" class="form-control" value="<?php echo $search['month'] ?>" placeholder="Month">

                                                    <div class="input-group-btn">
                                                        <button class="btn default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-12">
                                            <label>Type</label>
                                            <div class="form-group">
                                                <?php echo form_dropdown('type', ['' => 'Please Select', 'posb' => 'POSB', 'manual' => 'Manual', 'uob' => 'UOB', 'dbs' => 'DBS', 'ocbc' => 'OCBC', 'hsbc' => 'HSBC'], set_value('type', $search['type']), 'class="form-control"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 text-right">
                                            <a href="<?php echo base_url('bank_statement_upload') ?>" class=" btn btn-md grey-salsa">Reset</a>
                                            <button type="submit" class="btn btn-md green"><i class="icon-magnifier font-white"></i>Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <table style="margin-top: 20px;" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="4%" class="text-right">No</th>
                                    <th width="8%">Month</th>
                                    <th width="8%">Type</th>
                                    <th width="8%">Bank Account</th>
                                    <th width="8%">File Name</th>
                                    <th width="8%">Uploaded At</th>
                                    <th width="8%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php for ($i = 0; $i < count($uploads); $i++) {

                                    switch ($uploads[$i]['type']) {
                                        case 'uob':
                                            $type = 'UOB';
                                            break;
                                        case 'posb':
                                            $type = 'POSB';
                                            break;
                                        case 'manual':
                                            $type = 'Manual';
                                            break;
                                        case 'dbs':
                                            $type = 'DBS';
                                            break;
                                        case 'ocbc':
                                            $type = 'OCBC';
                                            break;
                                        case 'hsbc':
                                            $type = 'HSBC';
                                            break;
                                    }

                                    ?>
                                    <tr role="row" class="heading">
                                        <td class="text-right"><?php echo $pagination['offset']+$i ?></td>
                                        <td class="id hidden"><?php echo $uploads[$i]['id'] ?></td>
                                        <td class="month"><?php echo $uploads[$i]['month']; ?></td>
                                        <td><?php echo $type; ?></td>
                                        <td><?php echo $uploads[$i]['name'].' ';echo !empty($uploads[$i]['number']) ? '('.$uploads[$i]['number'].')' : ''; ?></td>
                                        <td class="type hidden"><?php echo $uploads[$i]['type']; ?></td>
                                        <td class="file_name"><a href="<?php echo $url.$uploads[$i]['file_name'] ?>"><?php echo $uploads[$i]['file_name']; ?></a></td>
                                        <td class="uploaded_at"><?php echo $uploads[$i]['uploaded_at']; ?></td>
                                        <td>
                                            <a href="#" class="btn btn-md red user-delete"><i class="fa fa-trash-o"></i> Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
                <nav class="pagination-wrap clearfix">
                    <div class="pull-left mt10">
                        <?php if ($pagination['total'] != 0) { ?>
                            <label class="form-control-static"><?php echo $pagination['total'] ?> Items in total, Display
                                <?php echo $pagination['offset'] ?> ~
                                <?php echo $pagination['limit'] ?></label>
                        <?php } ?>
                    </div>
                    <?php if (!empty($pagination) && $pagination['total'] > $pagination['items_per_page']) { ?>
                        <div class="pull-right">
                            <ul class="pagination no-margin">
                                <li <?php if ($pagination['page'] - 1 <= 0) { ?> class="disabled" <?php } ?>>

                                    <a href="<?php if ($pagination['page'] - 1 <= 0){ ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] - 1 ?><?php } ?>">
                                        Previous
                                    </a>
                                </li>
                                <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                                    <li>
                                        <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=1"> 1 </a>
                                    </li>
                                <?php } ?>

                                <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                                    <li><a href="javascript:;">...</a></li>
                                <?php } ?>

                                <?php for ($p=($pagination['min_display']); $p <= $pagination['max_display']; $p++) { ?>
                                    <?php if ($p > 0) { ?>
                                        <li <?php if ($pagination['page'] == $p) { ?> class="active" <?php } ?>>
                                            <a href="<?php if ($pagination['page'] != $p) { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $p ?><?php } else { ?>javascript:;<?php } ?>"> <?php echo $p ?> </a>
                                        </li>
                                    <?php } ?>
                                <?php } ?>

                                <?php if (($pagination['page'] + $pagination['adj']) < $pagination['total_page']) { ?>
                                    <li><a href="javascript:;">...</a></li>
                                <?php } ?>

                                <?php if ($pagination['page'] + $pagination['adj'] < $pagination['total_page']) { ?>
                                    <li>
                                        <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['total_page'] ?>"> <?php echo $pagination['total_page'] ?> </a>
                                    </li>
                                <?php } ?>

                                <li <?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> class="disabled" <?php } ?>>
                                    <a href="<?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] + 1 ?><?php } ?>">
                                        Next
                                    </a>
                                </li>
                            </ul>
                        </div>
                    <?php } ?>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="user-edit-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="user-modal-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">

                <div id="show_error_message" class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span id="error_message">Financial Period is updated successfully</span>
                </div>

                <?php echo form_open_multipart(base_url('bank_statement_upload/edit'), 'class="form-horizontal" method="post" data-parsley-validate enctype="multipart/form-data" id="user-edit_form"'); ?>
                <div class="form-body">
                    <div class="form-group">
                        <?php echo form_label('File Upload <span style="color:red;" class="required">*</span>', 'file_name', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">

                            <input id="modal_file_name" type="file" name="file_name" data-parsley-required="true" data-parsley-required-message="File Upload is required">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-5 col-lg-4">

                        </div>
                        <div class="col-md-7 col-lg-8 form-group" id="file_uploaded"></div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Month <span style="color:red;" class="required">*</span>', 'month', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <div class="input-group date month-picker" data-date-format="yyyy-mm"
                                 data-date-viewmode="years">
                                <input value="" name="month" type="text" class="form-control" placeholder="Month" id="modal_month" data-parsley-required="true" data-parsley-required-message="Month is required field" data-parsley-duplicate="duplicate">

                                <div class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Type <span style="color:red;" class="required">*</span>', 'type', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_dropdown('type', ['' => 'Please Select', 'posb' => 'POSB', 'manual' => 'Manual', 'uob' => 'UOB', 'dbs' => 'DBS', 'ocbc' => 'OCBC', 'hsbc' => 'HSBC'], set_select('type'), 'class="form-control" id="modal_type" data-parsley-required="true" data-parsley-required-message="Type is required field"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Bank Account <span style="color:red;" class="required">*</span>', 'description', array('class' => 'col-md-5 col-lg-4 control-label')); ?>
                        <div class="col-md-7 col-lg-8 form-group">
                            <select id="modal_bank_account_id" name="bank_account_id" class="form-control" data-parsley-required="true" data-parsley-required-message="Account is required field" data-parsley-duplicate="duplicate">
                                <?php echo $bank_accounts_selector; ?>

                            </select>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_id">
                <button  type="button" class="btn btn-md blue" id="user_submit">Submit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<div class="modal" id="user-delete-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Delete File Upload</h4>
            </div>
            <div class="modal-body">

                <p class="text-center">Can you confirm to delete File Upload?</p>

                <?php echo form_open_multipart(base_url('bank_statement_upload/delete'), 'class="form-horizontal" method="post" data-parsley-validate'); ?>


            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_delete_id">
                <button  type="submit" class="btn btn-md red">Delete</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {

        window.ParsleyValidator.addValidator('duplicate',
            function (value, requirement) {
                var modal_month = $('#modal_month').val();
                var modal_bank_account_id = $('#modal_bank_account_id').val();
                var error_message = '';

                var response = false;
                $.ajax({
                    async:false,
                    method: 'POST',
                    url: "<?php echo base_url(); ?>bank_statement_upload/check_add",
                    data: {
                        month: modal_month,
                        bank_account_id: modal_bank_account_id
                    },
                    dataType: 'json'
                }).done(function(res) {

                    response = res.error;
                    error_message = res.message;
                });

                window.ParsleyValidator.addMessage('en', 'duplicate', error_message);
                return response == false;
            }, 32
        );

        $('.month-picker').datepicker({
            format: "yyyy-mm",
            viewMode: "months",
            minViewMode: "months"
        });

        $("#user_submit").on("click",function(){
            $(this).prop("disabled", true);

            $('#show_error_message').hide();
            $("#user-edit_form").submit();
        });

        $(".user-delete").on("click",function(){
            var el = $(this);
            var id = el.closest('tr').find('.id').html();
            $('#modal_delete_id').val(id);

            $('#user-delete-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $(".user-add").on("click",function(){

            $("#modal_type").parsley().reset();
            $("#modal_file_name").parsley().reset();
            $("#modal_month").parsley().reset();

            $('#modal_type').val('');
            $('#file_uploaded').html('');
            $('#modal_file_name').val('');
            $('#modal_month').val('');
            $('#show_error_message').hide();
            $('#modal_id').val('');

            $('#user-modal-title').html('Add File Upload');

            $('#user-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });
    });
</script>
