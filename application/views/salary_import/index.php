<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Accounting System</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <meta content="" name="author"/>


    <link href="<?php echo base_url('public/assets/global/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/global/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/global/plugins/uniform/css/uniform.default.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css"/>

    <!-- BEGIN PAGE LEVEL STYLES -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/select2/select2.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/datatables/plugins/integration/bootstrap/3/dataTables.bootstrap.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/datatables-yadcf/jquery.dataTables.yadcf.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/bootstrap-datepicker/css/datepicker3.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/bootstrap-datetimepicker/css/datetimepicker.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/typeahead/typeahead.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/ion.rangeslider/css/ion.rangeSlider.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/ion.rangeslider/css/ion.rangeSlider.Metronic.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/slider-pips/jquery-ui-slider-pips.css') ?>"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/bootstrap-summernote/summernote.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/admin/pages/css/jquery.treegrid.css') ?>">

    <!-- END PAGE LEVEL STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link href="<?php echo base_url('public/assets/global/css/components.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/global/css/plugins.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/global/css/fix.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/admin/layout/css/layout.css') ?>" rel="stylesheet" type="text/css"/>
    <link id="style_color" href="<?php echo base_url('public/assets/admin/layout/css/themes/default.css') ?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url('public/assets/admin/layout/css/custom.css') ?>" rel="stylesheet" type="text/css"/>
    <!-- plus a jQuery UI theme, here I use "flick" -->
    <link rel="stylesheet" href="<?php echo base_url('public/assets/global/plugins/slider-pips/jquery-ui.css') ?>">
    <!-- END THEME STYLES -->
    <link rel="shortcut icon" href="<?php echo base_url('favicon.ico') ?>"/>
    <link href="<?php echo base_url('public/assets/global/css/multi-select.css') ?>" rel="stylesheet" type="text/css"/>

    <script src="<?php echo base_url('public/assets/global/plugins/jquery-1.11.0.min.js') ?>" type="text/javascript"></script>

    <script type="text/javascript">
        var base_url = '<?= base_url()?>';


    </script>
    <style type="text/css">
        @font-face {
            font-family: 'hgw-OpenSansSemibold';
            font-style: normal;
            font-weight: 600;
            src: url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-OpenSans-Semibold.eot');
            /* IE9 Compat Modes */
            src: url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-OpenSans-Semibold.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */ url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-OpenSans-Semibold.woff') format('woff'), /* Modern Browsers */ url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-OpenSans-Semibold.ttf') format('truetype'), /* Safari, Android, iOS */ url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-OpenSans-Semibold.svg#OpenSansSemibold') format('svg');
            /* Legacy iOS */
        }

        table {
            color: #333333;
            border-collapse: collapse;
            table-layout: fixed;
            word-wrap: break-word;
            margin: 20px auto;
        }
        th{
            text-align: center;
        }
        th, td{
            border: 1px solid #666666;
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .borderless{
            border: none;
        }
        input{
            text-align: right;
        }
        .check_bold{
            background: #cccccc; font-weight: bold; text-align: right;
            padding-right: 3px;
            -webkit-box-sizing:  border-box; /* Safari/Chrome, other WebKit */
            -moz-box-sizing:  border-box;    /* Firefox, other Gecko */
            box-sizing:  border-box;
        }
        .td_input{
            background: #dce6f1;
        }
        .td_input input{
            width: 70px;
            padding-right: 3px;
            -webkit-box-sizing:  border-box; /* Safari/Chrome, other WebKit */
            -moz-box-sizing:  border-box;    /* Firefox, other Gecko */
            box-sizing:  border-box;
        }

        .nav-tabs_sme {
            border-bottom: 1px solid #f1f1f1;
            background-color: #fafafa;
            *zoom: 1;
        }
        .nav-tabs_sme:before,
        .nav-tabs_sme:after {
            display: table;
            content: "";
            line-height: 0;
        }
        .nav-tabs_sme:after {
            clear: both;
        }
        .nav-tabs_sme li {
            float: left;
            padding-right: 40px;
        }
        .nav-tabs_sme li:last-child {
            padding-right: 0;
        }
        .nav-tabs_sme li a {
            text-transform: uppercase;
            font-size: 12px;
            color: #777777;
            display: block;
            border-bottom: 4px solid transparent;
            padding: 21px 0 18px;
            text-decoration: none !important;
        }
        .nav-tabs_sme li a:hover {
            color: #d11f25;
        }
        .nav-tabs_sme li.active a {
            color: #d11f25;
            font-family: "hgw-OpenSansSemibold", Arial, sans-serif;
            border-bottom-color: #d11f25;
        }

        .nav-tabs_sme-mobile {
            display: none;
            cursor: pointer;
            text-align: center;
            color: #777777;
            background-color: #fafafa;
            line-height: 40px;
            height: 60px;
            padding: 10px 0;
            border-bottom: 1px solid #f1f1f1;
        }
        .nav-tabs_sme-mobile .nvm-text {
            text-transform: uppercase;
        }
        .nav-tabs_sme-mobile .nvm-arrow {
            display: inline-block;
            vertical-align: top;
            margin: 15px 0 0 8px;
            border-left: 6px solid transparent;
            border-right: 6px solid transparent;
            border-top: 6px solid #999999;
        }

        @media screen and (min-width: 641px) {
            .nav-tabs_sme {
                display: block !important;
            }
        }
        @media screen and (max-width: 640px) {
            .nav-tabs_sme-wrap {
                position: relative;
            }

            .nav-tabs_sme-mobile {
                display: block;
            }

            .nav-tabs_sme {
                background: rgba(255, 255, 255, 0.96);
                border: none;
                display: none;
                position: absolute;
                top: 100%;
                left: 0;
                width: 100%;
                padding: 0 !important;
                z-index: 5;
            }

            .nav-tabs_sme li {
                float: none;
                width: 100%;
                padding-right: 0;
                border-bottom: 1px solid #dddddd;
                text-align: center;
            }

            .nav-tabs_sme li a {
                font-size: 14px;
                border: none;
                padding: 20px 0;
            }

            .nav-tabs_sme li.active {
                display: none;
            }
        }



        @font-face {
            font-family: 'hgw-RobotoSlabLight';
            font-style: normal;
            font-weight: 300;
            src: url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-RobotoSlab-Light.eot');
            /* IE9 Compat Modes */
            src: url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-RobotoSlab-Light.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */ url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-RobotoSlab-Light.woff') format('woff'), /* Modern Browsers */ url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-RobotoSlab-Light.ttf') format('truetype'), /* Safari, Android, iOS */ url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-RobotoSlab-Light.svg#RobotoSlabLight') format('svg');
            /* Legacy iOS */
        }

        .hgw_title {
            font-size: 34px;
            font-family: "hgw-RobotoSlabLight", Arial, sans-serif;
        }

        .top-pane {
            min-height: 115px;
            padding: 40px 0 0;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .mss-space {
            padding-left: 50px !important;
            padding-right: 50px !important;
        }

        .mss-line {
            border-bottom: 1px solid #f1f1f1;
        }
    </style>
</head>
<body>

    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top" <?php echo $hide_bar == TRUE ? 'style="display:none;"' : ''; ?>>
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="<?php echo base_url(); ?>">
                    <div class="row logo-default" style="color: #999999;" >Accounting System</div>
                </a>
                <div class="menu-toggler sidebar-toggler">
                </div>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN HORIZANTAL MENU -->
            <!-- DOC: Remove "hor-menu-light" class to have a horizontal menu with theme background instead of white background -->
            <!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) in the responsive menu below along with sidebar menu. So the horizontal menu has 2 seperate versions -->

            <!-- END HEADER SEARCH BOX -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
               data-target=".navbar-collapse">
            </a>
            <div style="position:absolute;top:15px;margin-left:50%;margin-right:50%;width:400px;color:white">
                <?php echo $current_company_data['name']; ?>
            </div>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">

                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                        <span class="username username-hide-on-mobile">
                        <?php
                        $adminData = $this->session->userData("user_data");
                        if(isset($adminData['username'])){
                            echo $adminData["username"];
                        }else{
                            echo $adminData["code"]." - ".$adminData["full_name"];
                        }
                        ?>
                        </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo base_url('user/change_password'); ?>">
                                    <i class="icon-lock"></i>Change Login Password </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url("AuthController/logout/admin"); ?>">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>

                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->

    <div class="page-content-wrapper">
        <div style="<?php echo $hide_bar == TRUE ? 'padding: 0px !important;' : 'padding: 70px 20px 10px 20px;'; ?> background-color: #fff;">
            <?php if ($hide_bar == TRUE) { ?>
            <div class="top-pane mss-space mss-line">
                <span class="hgw_title">Accounting</span>
            </div>
            <div class="nav-tabs_sme-wrap">
                <div class="nav-tabs_sme-mobile">
                    <span class="nvm-text">Salary Input <i class="nvm-arrow"></i></span>
                </div>
                <ul class="nav-tabs_sme mss-space" style="list-style: none;">
                    <li class="active">
                        <a href="<?php echo base_url(); ?>salary_import/index">Salary Input</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>PL_report/year_to_date_view">P&L (year-to-date)</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>PL_report/monthly_view">P&L (monthly)</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>balance_sheet_report/year_to_date_view">BS (year-to-date)</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>balance_sheet_report/monthly_view">BS (monthly)</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>aging_report/AP_view">Aging Report</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url(); ?>gst_report/view">GST Report</a>
                    </li>
                </ul>
            </div>
            <?php } ?>
            <!-- BEGIN ALERT BOX -->
            <?php if (isset($flash_message)) { ?>
                <div class="row margin-bottom-20">
                    <div class="col-xs-12">
                        <div class="alert alert-success">
                            <button class="close" data-close="alert"></button>
                            <?php echo $flash_message; ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <!-- END ALERT BOX -->
            <div id="show_error_message" class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <span id="error_message">Financial Period is updated successfully</span>
            </div>
            <div style="overflow: auto; padding: 10px;">
                <table>
                    <thead>
                    <?php if ($hide_bar != TRUE) { ?>
                    <tr style="margin-bottom: 10px;">
                        <th colspan="14" class="borderless" style="float:left;"><a href="<?php echo base_url("sales_input/select_outlet"); ?>" type="button" class="btn btn-md grey-salsa text-right">Back</a></th>
                    </tr>
                    <?php } ?>
                    <tr style="margin-bottom: 10px;">
                        <th colspan="1" class="borderless" style=" text-align: left;">		Payroll Month:</th>
                        <th colspan="3" class="borderless td_input"><input style="width:100%;" id="payroll_month" type="text" class="month-picker">
                            <small id="required_payroll_month" style="color:red; display:none;">Please select Payroll Month</small>
                        </th>
                        <th class="borderless"></th>
                    </tr>
                    <tr class="hidden_date">
                        <th colspan="13" style="padding-top: 10px; padding-bottom: 10px;">SALARY INFORMATION</th>
                        <th class="borderless"></th>
                    </tr>
                    <tr class="hidden_date">
                        <th style="padding:0px 5px;">No.</th>
                        <th style="padding:0px 5px;">Dept/outlet code</th>
                        <th style="padding:0px 5px; ">Code type</th>
                        <th style=" width: 70px;">Basic salary</th>
                        <th style=" width: 70px;">Bonus</th>
                        <th style=" width: 70px;">Gross Wage (exclude bonus)</th>
                        <th style=" width: 70px;">Net pay</th>
                        <th style=" width: 70px;">EE CPF</th>
                        <th style=" width: 70px;">ER CPF</th>
                        <th style=" width: 70px;">Ethnic</th>
                        <th style=" width: 70px;">SDL</th>
                        <th style=" width: 70px;">FWL</th>
                        <th style=" width: 70px;">Pay Items (Non-Taxable)</th>
                    </tr>
                    </thead>
                    <tbody id="outlets_table" class="hidden_date">
                    <?php foreach ($project_list AS $key => $project) { ?>
                        <tr class="non_director">
                            <td class="project_id hidden"><?php echo $project['id']; ?></td>
                            <td style="text-align: center;padding:0px 5px;"><?php echo $key+1; ?></td>
                            <td style="padding:0px 5px;"><?php echo $project['name']; ?></td>
                            <td class="project_type" style="padding:0px 5px;"><?php echo $project['type'] == 'outlet' ? 'DIR' : ($project['type'] == 'operating' ? 'IDR' : 'MGT'); ?></td>
                            <td class="td_input"><input class="number_input basic_salary"></td>
                            <td class="td_input"><input class="number_input bonus"></td>
                            <td class="td_input"><input class="number_input gross_wage"></td>
                            <td class="td_input"><input class="number_input net_pay"></td>
                            <td class="td_input"><input class="number_input EE_CPF"></td>
                            <td class="td_input"><input class="number_input ER_CPF"></td>
                            <td class="td_input"><input class="number_input ethnic"></td>
                            <td class="td_input"><input class="number_input SDL"></td>
                            <td class="td_input"><input class="number_input FWL"></td>
                            <td class="td_input"><input class="number_input pay_items"></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                    <tbody id="total_outlets" class="hidden_date">
                        <tr>
                            <td class="borderless"></td>
                            <td colspan="2" style="padding:0px 5px; text-align: center; font-weight: bold;">CO TOTAL</td>
                            <td class="check_bold basic_salary"></td>
                            <td class="check_bold bonus"></td>
                            <td class="check_bold gross_wage"></td>
                            <td class="check_bold net_pay"></td>
                            <td class="check_bold EE_CPF"></td>
                            <td class="check_bold ER_CPF"></td>
                            <td class="check_bold ethnic"></td>
                            <td class="check_bold SDL"></td>
                            <td class="check_bold FWL"></td>
                            <td class="check_bold pay_items"></td>
                            <td class="borderless"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div style="overflow: auto; padding: 10px;">
                <table class="hidden_date">
                    <thead>
                    <tr>
                        <th colspan="4" class="borderless"></th>
                        <th colspan="3" style="padding-top: 10px; padding-bottom: 10px;">Salary Deduction</th>
                        <th colspan="3" style="padding-top: 10px; padding-bottom: 10px;">Medical Claims</th>
                        <th colspan="3" style="padding-top: 10px; padding-bottom: 10px;">Others</th>
                        <th class="borderless"></th>
                    </tr>
                    <tr>
                        <th style="padding:0px 5px;">No.</th>
                        <th style="padding:0px 5px;">Dept/outlet code</th>
                        <th style="padding:0px 5px; ">Code type</th>
                        <th style=" width: 70px;">Pay Items (Non-Taxable)</th>
                        <th style=" width: 70px;">AMT</th>
                        <td style=" width: 70px; text-align: center;"><b>GST</b><br> (if any)</td>
                        <th style=" width: 70px;">TOTAL</th>
                        <th style=" width: 70px;">AMT</th>
                        <td style=" width: 70px; text-align: center;"><b>GST</b><br> (if any)</td>
                        <th style=" width: 70px;">TOTAL</th>
                        <th style=" width: 70px;">AMT</th>
                        <td style=" width: 70px; text-align: center;"><b>GST</b><br> (if any)</td>
                        <th style=" width: 70px;">TOTAL</th>
                    </tr>
                    </thead>
                    <tbody id="pay_items">
                    <?php foreach ($project_list AS $key => $project) { ?>
                        <tr class="project<?php echo $project['id']; ?>">
                            <td style="text-align: center;padding:0px 5px;"><?php echo $key+1; ?></td>
                            <td style="padding:0px 5px;"><?php echo $project['name']; ?></td>
                            <td style="padding:0px 5px;"><?php echo $project['type'] == 'outlet' ? 'DIR' : ($project['type'] == 'operating' ? 'IDR' : 'MGT'); ?></td>
                            <td class="check_bold pay_items"></td>
                            <td class="td_input">
                                <input class="number_input salary_amount">
                                <small class="required_salary_amount" style="color:red; display:none;">Salary Amount is required field</small>
                            </td>
                            <td class="td_input"><input class="number_input salary_GST">
                                <small class="required_salary_GST" style="color:red; display:none;">Salary GST is required field</small>
                            </td>
                            <td class="check_bold salary_total"></td>
                            <td class="td_input">
                                <input class="number_input medical_amount">
                                <small class="required_medical_amount" style="color:red; display:none;">Medical Amount is required field</small>
                            </td>
                            <td class="td_input">
                                <input class="number_input medical_GST">
                                <small class="required_medical_GST" style="color:red; display:none;">Medical GST is required field</small>
                            </td>
                            <td class="check_bold medical_total"></td>
                            <td class="td_input"><input class="number_input others_amount">
                                <small class="required_others_amount" style="color:red; display:none;">Others Amount is required field</small>
                            </td>
                            <td class="td_input"><input class="number_input others_GST">
                                <small class="required_others_GST" style="color:red; display:none;">Others GST is required field</small>
                            </td>
                            <td class="check_bold others_total"></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div style="overflow: auto; padding: 10px;">
                <table class="hidden_date">
                    <thead>
                    <tr>
                        <th style="width: 200px;">Mode</th>
                        <th rowspan="2" style="width: 200px;">Date</th>
                        <th rowspan="2" style="width: 200px;">Amount</th>
                        <th style="width: 200px;">Ref No</th>
                        <th style="width: 200px;">Bank Account</th>
                    </tr>
                    </thead>
                    <tbody id="payment_mode">
                        <tr>
                            <td class="td_input">
                                <select style="width: 180px; margin-left: 3px" class="payment_mode_selector">
                                    <option selected value="">Please Select</option>
                                    <option value="GIRO">GIRO</option>
                                    <option value="cheque">CHEQUE</option>
                                    <option value="cash">CASH</option>
                                </select>
                                <small class="required_payment_mode_selector" style="color:red; display:none;">Payment Type is required field</small>
                            </td>
                            <td class="td_input">
                                <input class="pay_date date-picker1" type="text" style="width: 180px; margin-left: 3px">
                                <small class="required_pay_date" style="color:red; display:none;">Pay Date is required field</small>
                            </td>
                            <td class="td_input">
                                <input class="amount number_input" name="" style="width: 180px; margin-left: 3px">
                                <small class="required_amount" style="color:red; display:none;">Amount is required field</small>
                            </td>
                            <td class="td_input"><input class="ref_no" type="text" style="width: 180px; margin-left: 3px">
                                <small class="required_ref_no" style="color:red; display:none;">Reference No is required field</small>
                            </td>
                            <td class="td_input">
                                <select style="width: 180px; margin-left: 3px" class="bank_account_selector">
                                    <?php echo $bank_accounts_selector; ?>
                                </select>

                                <select style="width: 180px; margin-left: 3px" class="cash_account_selector">
                                    <?php echo $cash_accounts_selector; ?>
                                </select>
                                <small class="required_account" style="color:red; display:none;">Bank Account is required field</small>
                            </td>
                            <td style="text-align: center;width: 5%;">
                                <a class="btn btn-md red payment_mode-delete"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_input">
                                <select style="width: 180px; margin-left: 3px" class="payment_mode_selector">
                                    <option selected value="">Please Select</option>
                                    <option value="GIRO">GIRO</option>
                                    <option value="cheque">CHEQUE</option>
                                    <option value="cash">CASH</option>
                                </select>
                                <small class="required_payment_mode_selector" style="color:red; display:none;">Payment Type is required field</small>
                            </td>
                            <td class="td_input">
                                <input class="pay_date date-picker1" type="text" style="width: 180px; margin-left: 3px">
                                <small class="required_pay_date" style="color:red; display:none;">Pay Date is required field</small>
                            </td>
                            <td class="td_input">
                                <input class="amount number_input" name="" style="width: 180px; margin-left: 3px">
                                <small class="required_amount" style="color:red; display:none;">Amount is required field</small>
                            </td>
                            <td class="td_input"><input class="ref_no" type="text" style="width: 180px; margin-left: 3px">
                                <small class="required_ref_no" style="color:red; display:none;">Reference No is required field</small>
                            </td>
                            <td class="td_input">
                                <select style="width: 180px; margin-left: 3px" class="bank_account_selector">
                                    <?php echo $bank_accounts_selector; ?>
                                </select>

                                <select style="width: 180px; margin-left: 3px" class="cash_account_selector">
                                    <?php echo $cash_accounts_selector; ?>
                                </select>
                                <small class="required_account" style="color:red; display:none;">Bank Account is required field</small>
                            </td>
                            <td style="text-align: center;width: 5%;">
                                <a class="btn btn-md red payment_mode-delete"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_input">
                                <select style="width: 180px; margin-left: 3px" class="payment_mode_selector">
                                    <option selected value="">Please Select</option>
                                    <option value="GIRO">GIRO</option>
                                    <option value="cheque">CHEQUE</option>
                                    <option value="cash">CASH</option>
                                </select>
                                <small class="required_payment_mode_selector" style="color:red; display:none;">Payment Type is required field</small>
                            </td>
                            <td class="td_input">
                                <input class="pay_date date-picker1" type="text" style="width: 180px; margin-left: 3px">
                                <small class="required_pay_date" style="color:red; display:none;">Pay Date is required field</small>
                            </td>
                            <td class="td_input">
                                <input class="amount number_input" name="" style="width: 180px; margin-left: 3px">
                                <small class="required_amount" style="color:red; display:none;">Amount is required field</small>
                            </td>
                            <td class="td_input"><input class="ref_no" type="text" style="width: 180px; margin-left: 3px">
                                <small class="required_ref_no" style="color:red; display:none;">Reference No is required field</small>
                            </td>
                            <td class="td_input">
                                <select style="width: 180px; margin-left: 3px" class="bank_account_selector">
                                    <?php echo $bank_accounts_selector; ?>
                                </select>

                                <select style="width: 180px; margin-left: 3px" class="cash_account_selector">
                                    <?php echo $cash_accounts_selector; ?>
                                </select>
                                <small class="required_account" style="color:red; display:none;">Bank Account is required field</small>
                            </td>
                            <td style="text-align: center;width: 5%;">
                                <a class="btn btn-md red payment_mode-delete"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <td id="add_payment_mode" colspan="5" class="check_bold" style="padding-right: 10px; text-align: center; cursor: pointer;"><i class="fa fa-plus"></i> Add New</td>
                        </tr>
                        <tr>
                            <th>TOTAL</th>
                            <td class="borderless"></td>
                            <td class="check_bold" id="total_payment" style="padding-right: 10px;"></td>
                            <td class="borderless"></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="hidden_date" style="text-align: right;">
                <button id="submit_salary" type="button" class="btn btn-md blue text-right">Submit</button>
            </div>
        </div>
    </div>

    <div class="modal-backdrop in" style="display: none; z-index: 99999;">
        <div clas="modal in" style="position: absolute;top: 0;left: 0;bottom: 0;right: 0;margin: auto;background-color: rgba(255, 255, 255, 0.8);background-image: url(<?= base_url('public/assets/admin/layout/img/wait.gif') ?>);background-repeat: no-repeat;background-position: center;background-size: 360;z-index: 2000;">

        </div>
    </div>

    <script>

    </script>

    <script src="<?php echo base_url('public/assets/global/plugins/jquery-migrate-1.2.1.min.js') ?>" type="text/javascript"></script>
    <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
    <script src="<?php echo base_url('public/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/global/plugins/bootstrap/js/bootstrap.min.js')?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/global/plugins/jquery.blockui.min.js') ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>public/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>public/assets/global/plugins/datatables/plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>public/assets/global/plugins/datatables-yadcf/jquery.dataTables.yadcf.js"></script>
    <script src="<?php echo base_url('public/assets/global/plugins/jquery.cokie.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/global/plugins/uniform/jquery.uniform.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') ?>" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/jquery-validation/js/additional-methods.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/select2/select2.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/parsley/parsley.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/parsley/parsley.remote.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/bootbox/bootbox.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/typeahead/handlebars.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/typeahead/typeahead.bundle.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/ion.rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/slider-pips/jquery-ui-slider-pips.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/slider-pips/jquery.blockUI.js') ?>"></script>
    <script src="<?php echo base_url('public/assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url();?>public/assets/global/plugins/tinymce/tinymce.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>public/assets/global/plugins/ckeditor/ckeditor.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <script src="<?php echo base_url('public/assets/global/scripts/metronic.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/admin/layout/scripts/layout.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/admin/pages/scripts/component-picker.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/assets/admin/pages/scripts/component-form-tool.js') ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url('public/assets/admin/includes/js/moment.js') ?>"></script>
    <script  type="text/javascript" src="<?php echo base_url('public/assets/admin/includes/js/datetime-moment.js') ?>"></script>
    <script  type="text/javascript" src="<?php echo base_url('public/assets/admin/pages/scripts/jquery.treegrid.min.js') ?>"></script>

    <!-- 20160929 vernhui IRAS -->
    <script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/jquery.formatCurrency-1.4.0.pack.js') ?>"></script>

    <script src="<?php echo base_url('public/assets/global/scripts/jquery.multi-select.js') ?>"></script>

    <script>
        function confirmLink(m,u) {
            if ( confirm(m) ) {
                window.location = u;
            }
        }
        jQuery(document).ready(function() {
            Metronic.init(); // init metronic core components
            Layout.init(); // init current layout
            ComponentsPickers.init();
            ComponentsFormTools.init();
            //ComponentsIonSliders.init();
            //$('.summernote').summernote({height: 300});
        });
        $(document).ready(function(){

            $('.hidden_date').hide();

            $(".number_input").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

            $('#show_error_message').hide();

            $('#payment_mode tr .date-picker1').datepicker({
                format: "yyyy-mm-dd"
            });

            $('#payment_mode .payment_mode-delete').on("click",function(){
                var el = $(this);
                el.closest('tr').remove();
            });

            $("#payroll_month").on("change",function(){
                var el = $(this);
                if (el.val() != '') {

                    $('.hidden_date').show();

                    $.ajax({
                        method: 'POST',
                        url: "<?php echo base_url(); ?>salary_import/get_data",
                        data: {
                            payroll_month: el.val()
                        },
                        dataType: 'json'
                    }).done(function(res) {

                        if(res.salary_import.length > 0) {
                            // Assign values
                            var total_basic_salary = 0;
                            var total_bonus = 0;
                            var total_gross_wage = 0;
                            var total_net_pay = 0;
                            var total_EE_CPF = 0;
                            var total_ER_CPF = 0;
                            var total_ethnic = 0;
                            var total_SDL = 0;
                            var total_FWL = 0;
                            var total_pay_items = 0;

                            $.each(res.salary_import, function (index, value) {

                                $('#outlets_table .non_director').each(function () {
                                    var el1 = $(this);
                                    var project_id = el1.find('.project_id').html();

                                    if (parseFloat(project_id) == parseFloat(value.project_id)) {

                                        if (value.basic_salary != null) {
                                            el1.find('.basic_salary').val(numberWithCommas(parseFloat(value.basic_salary).toFixed(2)));
                                            total_basic_salary += parseFloat(value.basic_salary);
                                        }

                                        if (value.bonus != null) {
                                            el1.find('.bonus').val(numberWithCommas(parseFloat(value.bonus).toFixed(2)));
                                            total_bonus += parseFloat(value.bonus);
                                        }

                                        if (value.gross_wage != null) {
                                            el1.find('.gross_wage').val(numberWithCommas(parseFloat(value.gross_wage).toFixed(2)));
                                            total_gross_wage += parseFloat(value.gross_wage);
                                        }

                                        if (value.net_pay != null) {
                                            el1.find('.net_pay').val(numberWithCommas(parseFloat(value.net_pay).toFixed(2)));
                                            total_net_pay += parseFloat(value.net_pay);
                                        }

                                        if (value.EE_CPF != null) {
                                            el1.find('.EE_CPF').val(numberWithCommas(parseFloat(value.EE_CPF).toFixed(2)));
                                            total_EE_CPF += parseFloat(value.EE_CPF);
                                        }

                                        if (value.ER_CPF != null) {
                                            el1.find('.ER_CPF').val(numberWithCommas(parseFloat(value.ER_CPF).toFixed(2)));
                                            total_ER_CPF += parseFloat(value.ER_CPF);
                                        }

                                        if (value.ethnic != null) {
                                            el1.find('.ethnic').val(numberWithCommas(parseFloat(value.ethnic).toFixed(2)));
                                            total_ethnic += parseFloat(value.ethnic);
                                        }

                                        if (value.SDL != null) {
                                            el1.find('.SDL').val(numberWithCommas(parseFloat(value.SDL).toFixed(2)));
                                            total_SDL += parseFloat(value.SDL);
                                        }

                                        if (value.FWL != null) {
                                            el1.find('.FWL').val(numberWithCommas(parseFloat(value.FWL).toFixed(2)));
                                            total_FWL += parseFloat(value.FWL);
                                        }

                                        if (value.pay_items != null) {
                                            el1.find('.pay_items').val(numberWithCommas(parseFloat(value.pay_items).toFixed(2)));
                                            total_pay_items += parseFloat(value.pay_items);
                                        }

                                    }
                                });

                                $("#total_outlets .basic_salary").html(numberWithCommas(total_basic_salary.toFixed(2)));
                                $("#total_outlets .bonus").html(numberWithCommas(total_bonus.toFixed(2)));
                                $("#total_outlets .gross_wage").html(numberWithCommas(total_gross_wage.toFixed(2)));
                                $("#total_outlets .net_pay").html(numberWithCommas(total_net_pay.toFixed(2)));
                                $("#total_outlets .EE_CPF").html(numberWithCommas(total_EE_CPF.toFixed(2)));
                                $("#total_outlets .ER_CPF").html(numberWithCommas(total_ER_CPF.toFixed(2)));
                                $("#total_outlets .ethnic").html(numberWithCommas(total_ethnic.toFixed(2)));
                                $("#total_outlets .SDL").html(numberWithCommas(total_SDL.toFixed(2)));
                                $("#total_outlets .FWL").html(numberWithCommas(total_FWL.toFixed(2)));
                                $("#total_outlets .pay_items").html(numberWithCommas(total_pay_items.toFixed(2)));

                                var project_id1 = value.project_id;
                                var this_ele = $('#pay_items .project' + project_id1);

                                var total_salary = 0;
                                if (value.salary_amount != null) {
                                    this_ele.find('.salary_amount').val(numberWithCommas(parseFloat(value.salary_amount).toFixed(2)));
                                    total_salary += parseFloat(value.salary_amount);
                                }

                                if (value.salary_GST != null) {
                                    this_ele.find('.salary_GST').val(numberWithCommas(parseFloat(value.salary_GST).toFixed(2)));
                                    total_salary += parseFloat(value.salary_GST);
                                }

                                this_ele.find('.salary_total').html(numberWithCommas(total_salary.toFixed(2)));

                                var total_medical = 0;
                                if (value.medical_amount != null) {
                                    this_ele.find('.medical_amount').val(numberWithCommas(parseFloat(value.medical_amount).toFixed(2)));
                                    total_medical += parseFloat(value.medical_amount);
                                }

                                if (value.medical_GST != null) {
                                    this_ele.find('.medical_GST').val(numberWithCommas(parseFloat(value.medical_GST).toFixed(2)));
                                    total_medical += parseFloat(value.medical_GST);
                                }

                                this_ele.find('.medical_total').html(numberWithCommas(total_medical.toFixed(2)));

                                var total_others = 0;
                                if (value.others_amount != null) {
                                    this_ele.find('.others_amount').val(numberWithCommas(parseFloat(value.others_amount).toFixed(2)));
                                    total_others += parseFloat(value.others_amount);
                                }

                                if (value.others_GST != null) {
                                    this_ele.find('.others_GST').val(numberWithCommas(parseFloat(value.others_GST).toFixed(2)));
                                    total_others += parseFloat(value.others_GST);
                                }

                                this_ele.find('.others_total').html(numberWithCommas(total_others.toFixed(2)));

                                if (value.pay_items != null) {
                                    this_ele.find('.pay_items').html(numberWithCommas(parseFloat(value.pay_items).toFixed(2)));
                                }

                            });

                            $("#payment_mode").html('');

                            var total_payment = 0;
                            $.each(res.salary_payment, function (index, value) {
                                var html = '<tr>';
                                html += '<td class="td_input">';
                                html += '<select style="width: 180px; margin-left: 3px" class="payment_mode_selector">';
                                html += '<option selected value="">Please Select</option>';
                                html += '<option value="GIRO">GIRO</option>';
                                html += '<option value="cheque">CHEQUE</option>';
                                html += '<option value="cash">CASH</option>';
                                html += '</select>';
                                html += '<small class="required_payment_mode_selector" style="color:red; display:none;">Payment Type is required field</small></td>';
                                html += '<td class="td_input"><input class="pay_date date-picker" type="text" style="width: 180px; margin-left: 3px"><small class="required_pay_date" style="color:red; display:none;">Pay Date is required field</small></td>';
                                html += '<td class="td_input"><input class="amount number_input" name="" style="width: 180px; margin-left: 3px"><small class="required_amount" style="color:red; display:none;">Amount is required field</small></td>';
                                html += '<td class="td_input"><input class="ref_no" type="text" style="width: 180px; margin-left: 3px"><small class="required_ref_no" style="color:red; display:none;">Reference No is required field</small></td>';
                                html += '<td class="td_input">';
                                html += '<select style="width: 180px; margin-left: 3px" class="bank_account_selector">';
                                html += '<?php echo $bank_accounts_selector; ?>';
                                html += '</select>';

                                html += '<select style="width: 180px; margin-left: 3px" class="cash_account_selector">';
                                html += '<?php echo $cash_accounts_selector; ?>';
                                html += '</select>';
                                html += '<small class="required_account" style="color:red; display:none;">Bank Account is required field</small></td>';
                                html += '<td style="text-align: center;width: 5%;">';
                                html += '</td>';
                                html += '</tr>';

                                $("#payment_mode").append(html);

                                total_payment += parseFloat(value.amount);

                                $('#payment_mode tr:last-child .payment_mode_selector').val(value.payment_mode);
                                $('#payment_mode tr:last-child .pay_date').val(value.pay_date);
                                $('#payment_mode tr:last-child .amount').val(numberWithCommas(parseFloat(value.amount).toFixed(2)));
                                $('#payment_mode tr:last-child .ref_no').val(value.ref_no);
                                $('#payment_mode tr:last-child .bank_account_selector').val(value.bank_account_id);
                                $('#payment_mode tr:last-child .cash_account_selector').val(value.bank_account_id);

                                if ($('#payment_mode tr:last-child .bank_account_selector').val() == null) {
                                    $('#payment_mode tr:last-child .bank_account_selector').hide();
                                } else {
                                    $('#payment_mode tr:last-child .cash_account_selector').hide();
                                }
                            });

                            $("input, select").prop("disabled", true);
                            $("#payroll_month").prop("disabled", false);

                            $("#add_payment_mode").hide();
                            $("#submit_salary").hide();

                            $("#total_payment").html(numberWithCommas(total_payment.toFixed(2)));

                        } else {

                            $("input, select").prop("disabled", false);
                            $("#payroll_month").prop("disabled", false);

                            $("#submit_salary").show();
                            $("#outlets_table input, #pay_items input").val('');
                            $("#add_payment_mode").show();
                            $("#pay_items .pay_items, #pay_items .salary_total, #pay_items .medical_total, #pay_items .others_total, #total_payment").html('');

                            $("#total_outlets .basic_salary").html('');
                            $("#total_outlets .bonus").html('');
                            $("#total_outlets .gross_wage").html('');
                            $("#total_outlets .net_pay").html('');
                            $("#total_outlets .EE_CPF").html('');
                            $("#total_outlets .ER_CPF").html('');
                            $("#total_outlets .ethnic").html('');
                            $("#total_outlets .SDL").html('');
                            $("#total_outlets .FWL").html('');
                            $("#total_outlets .pay_items").html('');

                            $("#payment_mode").html('');

                            for (var i = 0; i < 3; i++) {
                                var html = '<tr>';
                                html += '<td class="td_input">';
                                html += '<select style="width: 180px; margin-left: 3px" class="payment_mode_selector">';
                                html += '<option selected value="">Please Select</option>';
                                html += '<option value="GIRO">GIRO</option>';
                                html += '<option value="cheque">CHEQUE</option>';
                                html += '<option value="cash">CASH</option>';
                                html += '</select>';
                                html += '<small class="required_payment_mode_selector" style="color:red; display:none;">Payment Type is required field</small></td>';
                                html += '<td class="td_input"><input class="pay_date date-picker" type="text" style="width: 180px; margin-left: 3px"><small class="required_pay_date" style="color:red; display:none;">Pay Date is required field</small></td>';
                                html += '<td class="td_input"><input class="amount number_input" name="" style="width: 180px; margin-left: 3px"><small class="required_amount" style="color:red; display:none;">Amount is required field</small></td>';
                                html += '<td class="td_input"><input class="ref_no" type="text" style="width: 180px; margin-left: 3px"><small class="required_ref_no" style="color:red; display:none;">Reference No is required field</small></td>';
                                html += '<td class="td_input">';
                                html += '<select style="width: 180px; margin-left: 3px" class="bank_account_selector">';
                                html += '<?php echo $bank_accounts_selector; ?>';
                                html += '</select>';

                                html += '<select style="width: 180px; margin-left: 3px" class="cash_account_selector">';
                                html += '<?php echo $cash_accounts_selector; ?>';
                                html += '</select>';
                                html += '<small class="required_account" style="color:red; display:none;">Bank Account is required field</small></td>';
                                html += '<td style="text-align: center;width: 5%;">';
                                html += '<a class="btn btn-md red payment_mode-delete"><i class="fa fa-trash-o"></i></a>';
                                html += '</td>';
                                html += '</tr>';

                                $("#payment_mode").append(html);

                                $('#payment_mode tr:last-child .bank_account_selector').hide();
                                $('#payment_mode tr:last-child .cash_account_selector').hide();

                                $('#payment_mode tr:last-child .date-picker').datepicker({
                                    format: "yyyy-mm-dd"
                                });

                                $('#payment_mode tr:last-child .payment_mode-delete').on("click", function () {
                                    var el = $(this);
                                    el.closest('tr').remove();
                                });

                                $("#payment_mode tr:last-child .payment_mode_selector").on("change", function () {
                                    var el = $(this);
                                    if (el.val() != '') {

                                        if (el.val() == 'GIRO' || el.val() == 'cheque') {
                                            el.closest('tr').find(".bank_account_selector").show();
                                            el.closest('tr').find(".cash_account_selector").hide();

                                            if (el.val() == 'GIRO') {
                                                el.closest('tr').find(".ref_no").val('WGI');
                                            } else {
                                                el.closest('tr').find(".ref_no").val('WCH');
                                            }

                                        } else {
                                            el.closest('tr').find(".ref_no").val('WCA');
                                            el.closest('tr').find(".bank_account_selector").hide();
                                            el.closest('tr').find(".cash_account_selector").show();
                                        }

                                    } else {
                                        el.closest('tr').find(".ref_no").val('');
                                        el.closest('tr').find(".bank_account_selector").hide();
                                        el.closest('tr').find(".cash_account_selector").hide();
                                    }

                                    el.closest('tr').find(".bank_account_selector").val('');
                                    el.closest('tr').find(".cash_account_selector").val('');
                                });

                                $("#payment_mode tr:last-child .amount").on("change", function () {
                                    var el = $(this);
                                    var amount = parseFloat(el.val());
                                    if (amount != 0 && el.val() != '') {

                                        el.val(numberWithCommas(amount.toFixed(2)));

                                    } else {
                                        el.val('');
                                    }

                                    var total_amount = 0;
                                    $('#payment_mode .amount').each(function () {
                                        var el1 = $(this);
                                        var amount = el1.val().replace(/,/g, "");

                                        if (amount != '') {
                                            total_amount += parseFloat(amount);
                                        }
                                    });

                                    $('#total_payment').html(numberWithCommas(total_amount.toFixed(2)));
                                });

                            }
                        }

                    });

                } else {
                    location.reload();
                }
            });


            $('#submit_salary').on("click",function(){
                $(this).prop("disabled", true);

                var payroll_month = $('#payroll_month').val();
                
                if (payroll_month == '') {
                    $('#required_payroll_month').show();
                    $(this).prop("disabled", false);
                    return;
                } else {
                    $('#required_payroll_month').hide();
                }

                var non_director = [];
                var payment_modes = [];
                var error = false;
                var error1 = false;
                var error2 = false;

                $('#outlets_table .non_director').each(function() {

                    var outlet = {};
                    var checking = 0;
                    var total = 0;

                    var el1 = $(this);

                    var project_id = el1.find('.project_id').html();
                    outlet.project_id = project_id;

                    var project_type = el1.find('.project_type').html();
                    outlet.project_type = project_type;

                    var basic_salary = el1.find('.basic_salary').val();
                    if (basic_salary != '') {
                        outlet.basic_salary = parseFloat(basic_salary.replace(/,/g, ""));
                    }

                    var bonus = el1.find('.bonus').val();
                    if (bonus != '') {
                        outlet.bonus = parseFloat(bonus.replace(/,/g, ""));
                        checking  += parseFloat(bonus.replace(/,/g, ""));
                        total += parseFloat(bonus.replace(/,/g, ""));
                    }

                    var gross_wage = el1.find('.gross_wage').val();
                    if (gross_wage != '') {
                        outlet.gross_wage = parseFloat(gross_wage.replace(/,/g, ""));
                        checking  += parseFloat(gross_wage.replace(/,/g, ""));
                        total += parseFloat(gross_wage.replace(/,/g, ""));
                    }

                    var net_pay = el1.find('.net_pay').val();
                    if (net_pay != '') {
                        outlet.net_pay = parseFloat(net_pay.replace(/,/g, ""));
                        checking  -= parseFloat(net_pay.replace(/,/g, ""));
                        total += parseFloat(net_pay.replace(/,/g, ""));
                    }

                    var EE_CPF = el1.find('.EE_CPF').val();
                    if (EE_CPF != '') {
                        outlet.EE_CPF = parseFloat(EE_CPF.replace(/,/g, ""));
                        checking  -= parseFloat(EE_CPF.replace(/,/g, ""));
                        total += parseFloat(EE_CPF.replace(/,/g, ""));
                    }

                    var ER_CPF = el1.find('.ER_CPF').val();
                    if (ER_CPF != '') {
                        outlet.ER_CPF = parseFloat(ER_CPF.replace(/,/g, ""));
                        total += parseFloat(ER_CPF.replace(/,/g, ""));
                    }

                    var ethnic = el1.find('.ethnic').val();
                    if (ethnic != '') {
                        outlet.ethnic = parseFloat(ethnic.replace(/,/g, ""));
                        checking  -= parseFloat(ethnic.replace(/,/g, ""));
                        total += parseFloat(ethnic.replace(/,/g, ""));
                    }

                    var SDL = el1.find('.SDL').val();
                    if (SDL != '') {
                        outlet.SDL = parseFloat(SDL.replace(/,/g, ""));
                        total += parseFloat(SDL.replace(/,/g, ""));
                    }

                    var FWL = el1.find('.FWL').val();
                    if (FWL != '') {
                        outlet.FWL = parseFloat(FWL.replace(/,/g, ""));
                        total += parseFloat(FWL.replace(/,/g, ""));
                    }

                    var pay_items = el1.find('.pay_items').val();
                    if (pay_items != '') {
                        outlet.pay_items = parseFloat(pay_items.replace(/,/g, ""));
                        checking  += parseFloat(pay_items.replace(/,/g, ""));
                        total += parseFloat(pay_items.replace(/,/g, ""));
                    }


                    var total_pay_items = 0;
                    var salary_amount = $('#pay_items .project'+project_id).find('.salary_amount').val();
                    if (salary_amount != '') {
                        outlet.salary_amount = parseFloat(salary_amount.replace(/,/g, ""));
                        total_pay_items += parseFloat(salary_amount.replace(/,/g, ""));
                    }

                    var salary_GST = $('#pay_items .project'+project_id).find('.salary_GST').val();
                    if (salary_GST != '') {
                        outlet.salary_GST = parseFloat(salary_GST.replace(/,/g, ""));
                        total_pay_items += parseFloat(salary_GST.replace(/,/g, ""));
                    }

                    var medical_amount = $('#pay_items .project'+project_id).find('.medical_amount').val();
                    if (medical_amount != '') {
                        outlet.medical_amount = parseFloat(medical_amount.replace(/,/g, ""));
                        total_pay_items += parseFloat(medical_amount.replace(/,/g, ""));
                    }

                    var medical_GST = $('#pay_items .project'+project_id).find('.medical_GST').val();
                    if (medical_GST != '') {
                        outlet.medical_GST = parseFloat(medical_GST.replace(/,/g, ""));
                        total_pay_items += parseFloat(medical_GST.replace(/,/g, ""));
                    }

                    var others_amount = $('#pay_items .project'+project_id).find('.others_amount').val();
                    if (others_amount != '') {
                        outlet.others_amount = parseFloat(others_amount.replace(/,/g, ""));
                        total_pay_items += parseFloat(others_amount.replace(/,/g, ""));
                    }

                    var others_GST = $('#pay_items .project'+project_id).find('.others_GST').val();
                    if (others_GST != '') {
                        outlet.others_GST = parseFloat(others_GST.replace(/,/g, ""));
                        total_pay_items += parseFloat(others_GST.replace(/,/g, ""));
                    }

                    if (parseFloat(checking.toFixed(2)) != 0) {

                        error = true;
                        el1.css('color', 'red');

                    } else {
                        el1.css('color', '#333333');
                    }

                    if (total == 0  && project_type == 'DIR') {

                        error2 = true;
                        el1.css('color', 'red');

                    } else {
                        el1.css('color', '#333333');
                    }

                    if (pay_items != '' && total_pay_items != parseFloat(pay_items.replace(/,/g, ""))) {
                        error1 = true;
                        $('#pay_items .project'+project_id).css('color', 'red');
                    } else {
                        $('#pay_items .project'+project_id).css('color', '#333333');
                    }

                    non_director.push(outlet);


                });

                if (error == true) {

                    $('#show_error_message').show();
                    $('#error_message').html('Total Bonus, Gross Wage and Pay Items should be equal to Net Pay, EE CPF and Ethnic');
                    $(this).prop("disabled", false);
                    return;

                } else {

                    $('#show_error_message').hide();
                }

                if (error2 == true) {

                    $('#show_error_message').show();
                    $('#error_message').html('Please input salary data for all outlets before submitting');
                    $(this).prop("disabled", false);
                    return;

                } else {

                    $('#show_error_message').hide();
                }

                if (error1 == true) {

                    $('#show_error_message').show();
                    $('#error_message').html('Total Salary, Medical and Others should be equal to Total Pay Items');
                    $(this).prop("disabled", false);
                    return;

                } else {

                    $('#show_error_message').hide();
                }

                var payment_error = false;

                $('#payment_mode tr').each(function() {

                    var payment_item = {};

                    var el1 = $(this);

                    var payment_mode_type = el1.find('.payment_mode_selector').val();
                    if (payment_mode_type == '') {
                        el1.find('.required_payment_mode_selector').show();
                        payment_error = true;
                    } else {
                        el1.find('.required_payment_mode_selector').hide();
                        payment_item.payment_mode = payment_mode_type;
                    }

                    var pay_date = el1.find('.pay_date').val();
                    if (pay_date == '') {
                        el1.find('.required_pay_date').show();
                        payment_error = true;
                    } else {
                        el1.find('.required_pay_date').hide();
                        payment_item.pay_date = pay_date;

                    }

                    var amount = el1.find('.amount').val();
                    if (amount == '') {
                        el1.find('.required_amount').show();
                        payment_error = true;
                    } else {
                        el1.find('.required_amount').hide();
                        payment_item.amount = parseFloat(amount.replace(/,/g, ""));

                    }

                    var ref_no = el1.find('.ref_no').val();
                    if (amount == '') {
                        el1.find('.required_ref_no').show();
                        payment_error = true;
                    } else {
                        el1.find('.required_ref_no').hide();
                        payment_item.ref_no = ref_no;

                    }

                    var bank_account_selector = el1.find('.bank_account_selector').val();
                    var cash_account_selector = el1.find('.cash_account_selector').val();
                    if (bank_account_selector == '' && cash_account_selector == '') {
                        el1.find('.required_account').show();
                        payment_error = true;
                    } else {
                        el1.find('.required_account').hide();
                        payment_item.bank_account_id = bank_account_selector != '' ? bank_account_selector : cash_account_selector;

                    }


                    payment_modes.push(payment_item);


                });

                if (payment_error == true) {
                    $(this).prop("disabled", false);
                    return;
                }

                var net_pay = $('#total_outlets .net_pay').html();
                var total_payment = $('#total_payment').html();

                if (net_pay != total_payment || net_pay == '' || total_payment == '') {

                    $('#show_error_message').show();
                    $('#error_message').html('Total Net Pay should be equal to Total Payment');

                    $('#total_outlets .net_pay').css('color', 'red');
                    $('#total_payment').css('color', 'red');
                    $(this).prop("disabled", false);
                    return;

                } else {

                    $('#show_error_message').hide();
                    $('#total_outlets .net_pay').css('color', '#333333');
                    $('#total_payment').css('color', '#333333');
                }


                //doing

                $.ajax({
                    method: 'POST',
                    url: "<?php echo base_url(); ?>salary_import/get_salary",
                    data: {
                        payroll_month: payroll_month,
                        non_director: non_director,
                        payment_modes: payment_modes
                    },
                    dataType: 'json'
                }).done(function(res) {
                    if (res.success == true) {
                        window.location.reload();
                    }
                });
            });

            $("#payment_mode .amount").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));

                } else {
                    el.val('');
                }

                var total_amount = 0;
                $('#payment_mode .amount').each(function() {
                    var el1 = $(this);
                    var amount = el1.val().replace(/,/g, "");

                    if (amount != '') {
                        total_amount += parseFloat(amount);
                    }
                });

                $('#total_payment').html(numberWithCommas(total_amount.toFixed(2)));
            });

            $("#pay_items input").prop( "disabled", true );

            $(".bank_account_selector").hide();
            $(".cash_account_selector").hide();

            function numberWithCommas(x) {
                var parts = x.toString().split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                return parts.join(".");
            }

            function calculate_total(name) {

                var total_amount = 0;
                $('#outlets_table .'+name).each(function() {
                    var el1 = $(this);
                    var amount = el1.val().replace(/,/g, "");

                    if (amount != '') {
                        total_amount += parseFloat(amount);
                    }
                });

                $('#total_outlets .'+name).html(numberWithCommas(total_amount.toFixed(2)));
            }

            $("#payment_mode .payment_mode_selector").on("change",function(){
                var el = $(this);
                if (el.val() != '') {

                    if (el.val() == 'GIRO' || el.val() == 'cheque') {
                        el.closest('tr').find(".bank_account_selector").show();
                        el.closest('tr').find(".cash_account_selector").hide();

                        if (el.val() == 'GIRO') {
                            el.closest('tr').find(".ref_no").val('WGI');
                        } else {
                            el.closest('tr').find(".ref_no").val('WCH');
                        }

                    } else {
                        el.closest('tr').find(".ref_no").val('WCA');
                        el.closest('tr').find(".bank_account_selector").hide();
                        el.closest('tr').find(".cash_account_selector").show();
                    }

                } else {
                    el.closest('tr').find(".ref_no").val('');
                    el.closest('tr').find(".bank_account_selector").hide();
                    el.closest('tr').find(".cash_account_selector").hide();
                }

                el.closest('tr').find(".bank_account_selector").val('');
                el.closest('tr').find(".cash_account_selector").val('');
            });

            $("#outlets_table .basic_salary").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));

                } else {
                    el.val('');
                }

                calculate_total('basic_salary');
            });

            $("#add_payment_mode").on("click",function(){

                var html = '<tr>';
                html += '<td class="td_input">';
                html += '<select style="width: 180px; margin-left: 3px" class="payment_mode_selector">';
                html += '<option selected value="">Please Select</option>';
                html += '<option value="GIRO">GIRO</option>';
                html += '<option value="cheque">CHEQUE</option>';
                html += '<option value="cash">CASH</option>';
                html += '</select>';
                html += '<small class="required_payment_mode_selector" style="color:red; display:none;">Payment Type is required field</small></td>';
                html += '<td class="td_input"><input class="pay_date date-picker" type="text" style="width: 180px; margin-left: 3px"><small class="required_pay_date" style="color:red; display:none;">Pay Date is required field</small></td>';
                html += '<td class="td_input"><input class="amount number_input" name="" style="width: 180px; margin-left: 3px"><small class="required_amount" style="color:red; display:none;">Amount is required field</small></td>';
                html += '<td class="td_input"><input class="ref_no" type="text" style="width: 180px; margin-left: 3px"><small class="required_ref_no" style="color:red; display:none;">Reference No is required field</small></td>';
                html += '<td class="td_input">';
                html += '<select style="width: 180px; margin-left: 3px" class="bank_account_selector">';
                html += '<?php echo $bank_accounts_selector; ?>';
                html += '</select>';

                html += '<select style="width: 180px; margin-left: 3px" class="cash_account_selector">';
                html += '<?php echo $cash_accounts_selector; ?>';
                html += '</select>';
                html += '<small class="required_account" style="color:red; display:none;">Bank Account is required field</small></td>';
                html += '<td style="text-align: center;width: 5%;">';
                html += '<a class="btn btn-md red payment_mode-delete"><i class="fa fa-trash-o"></i></a>';
                html += '</td>';
                html += '</tr>';

                $("#payment_mode").append(html);

                $('#payment_mode tr:last-child .bank_account_selector').hide();
                $('#payment_mode tr:last-child .cash_account_selector').hide();

                $('#payment_mode tr:last-child .date-picker').datepicker({
                    format: "yyyy-mm-dd"
                });

                $('#payment_mode tr:last-child .payment_mode-delete').on("click",function(){
                    var el = $(this);
                    el.closest('tr').remove();
                });

                $("#payment_mode tr:last-child .payment_mode_selector").on("change",function(){
                    var el = $(this);
                    if (el.val() != '') {

                        if (el.val() == 'GIRO' || el.val() == 'cheque') {
                            el.closest('tr').find(".bank_account_selector").show();
                            el.closest('tr').find(".cash_account_selector").hide();

                            if (el.val() == 'GIRO') {
                                el.closest('tr').find(".ref_no").val('WGI');
                            } else {
                                el.closest('tr').find(".ref_no").val('WCH');
                            }

                        } else {
                            el.closest('tr').find(".ref_no").val('WCA');
                            el.closest('tr').find(".bank_account_selector").hide();
                            el.closest('tr').find(".cash_account_selector").show();
                        }

                    } else {
                        el.closest('tr').find(".ref_no").val('');
                        el.closest('tr').find(".bank_account_selector").hide();
                        el.closest('tr').find(".cash_account_selector").hide();
                    }

                    el.closest('tr').find(".bank_account_selector").val('');
                    el.closest('tr').find(".cash_account_selector").val('');
                });

                $("#payment_mode tr:last-child .amount").on("change",function(){
                    var el = $(this);
                    var amount = parseFloat(el.val());
                    if (amount != 0 && el.val() != '') {

                        el.val(numberWithCommas(amount.toFixed(2)));

                    } else {
                        el.val('');
                    }

                    var total_amount = 0;
                    $('#payment_mode .amount').each(function() {
                        var el1 = $(this);
                        var amount = el1.val().replace(/,/g, "");

                        if (amount != '') {
                            total_amount += parseFloat(amount);
                        }
                    });

                    $('#total_payment').html(numberWithCommas(total_amount.toFixed(2)));
                });
            });

            $("#outlets_table .bonus").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));

                } else {
                    el.val('');
                }

                calculate_total('bonus');
            });

            $("#outlets_table .gross_wage").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));

                } else {
                    el.val('');
                }

                calculate_total('gross_wage');
            });

            $("#outlets_table .net_pay").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));

                } else {
                    el.val('');
                }

                calculate_total('net_pay');
            });

            $("#outlets_table .EE_CPF").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));

                } else {
                    el.val('');
                }

                calculate_total('EE_CPF');
            });

            $("#outlets_table .ER_CPF").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));

                } else {
                    el.val('');
                }

                calculate_total('ER_CPF');
            });

            $("#outlets_table .ethnic").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));

                } else {
                    el.val('');
                }

                calculate_total('ethnic');
            });

            $("#outlets_table .SDL").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));

                } else {
                    el.val('');
                }

                calculate_total('SDL');
            });

            $("#outlets_table .FWL").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));

                } else {
                    el.val('');
                }

                calculate_total('FWL');
            });

            $("#outlets_table .non_director .pay_items").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                var project_id = el.closest('tr').find('.project_id').html();
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));

                    $('#pay_items .project'+project_id).find('.pay_items').html(numberWithCommas(amount.toFixed(2)));

                    $('#pay_items .project'+project_id+' input').prop( "disabled", false );

                } else {
                    el.val('');
                    $('#pay_items .project'+project_id+' input').prop( "disabled", true );
                    $('#pay_items .project'+project_id+' input').val('');
                    $('#pay_items .project'+project_id+' .salary_total').html('');
                    $('#pay_items .project'+project_id+' .medical_total').html('');
                    $('#pay_items .project'+project_id+' .others_total').html('');
                    $('#pay_items .project'+project_id).find('.pay_items').html('');
                }

                calculate_total('pay_items');
            });

            $("#pay_items .salary_amount").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                var total_amount = 0;
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));
                    total_amount = amount;

                } else {
                    el.val('');
                }

                var other_amount = el.closest('tr').find('.salary_GST').val().replace(/,/g, "");

                if (other_amount != '') {
                    total_amount += parseFloat(other_amount);
                }

                el.closest('tr').find('.salary_total').html(numberWithCommas(total_amount.toFixed(2)));
            });

            $("#pay_items .salary_GST").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                var total_amount = 0;
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));
                    total_amount = amount;

                } else {
                    el.val('');
                }

                var other_amount = el.closest('tr').find('.salary_amount').val().replace(/,/g, "");

                if (other_amount != '') {
                    total_amount += parseFloat(other_amount);
                }

                el.closest('tr').find('.salary_total').html(numberWithCommas(total_amount.toFixed(2)));
            });

            $("#pay_items .medical_amount").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                var total_amount = 0;
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));
                    total_amount = amount;

                } else {
                    el.val('');
                }

                var other_amount = el.closest('tr').find('.medical_GST').val().replace(/,/g, "");

                if (other_amount != '') {
                    total_amount += parseFloat(other_amount);
                }

                el.closest('tr').find('.medical_total').html(numberWithCommas(total_amount.toFixed(2)));
            });

            $("#pay_items .medical_GST").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                var total_amount = 0;
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));
                    total_amount = amount;

                } else {
                    el.val('');
                }

                var other_amount = el.closest('tr').find('.medical_amount').val().replace(/,/g, "");

                if (other_amount != '') {
                    total_amount += parseFloat(other_amount);
                }

                el.closest('tr').find('.medical_total').html(numberWithCommas(total_amount.toFixed(2)));
            });

            $("#pay_items .others_amount").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                var total_amount = 0;
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));
                    total_amount = amount;

                } else {
                    el.val('');
                }

                var other_amount = el.closest('tr').find('.others_GST').val().replace(/,/g, "");

                if (other_amount != '') {
                    total_amount += parseFloat(other_amount);
                }

                el.closest('tr').find('.others_total').html(numberWithCommas(total_amount.toFixed(2)));
            });

            $("#pay_items .others_GST").on("change",function(){
                var el = $(this);
                var amount = parseFloat(el.val());
                var total_amount = 0;
                if (amount != 0 && el.val() != '') {

                    el.val(numberWithCommas(amount.toFixed(2)));
                    total_amount = amount;

                } else {
                    el.val('');
                }

                var other_amount = el.closest('tr').find('.others_amount').val().replace(/,/g, "");

                if (other_amount != '') {
                    total_amount += parseFloat(other_amount);
                }

                el.closest('tr').find('.others_total').html(numberWithCommas(total_amount.toFixed(2)));
            });

            $('.month-picker').datepicker({
                format: "yyyy-mm",
                viewMode: "months",
                minViewMode: "months"
            });

            $('.month-picker').on('changeDate', function(ev){
                $(this).datepicker('hide');
            });

            $('.month-picker').on('hide.bs.modal', function(ev){
                ev.stopPropogation();
            });

            $("#selLimit").on("change",function(){
                <?php
                if(isset($sort_by) && isset($order)){
                $sort_parameter['sort_by'] = $sort_by;
                $sort_parameter['page'] = 1;
                $sort_parameter['order'] = $order;
                ?>
                window.location.href = "<?php echo $base_url_pagination?>&limit="+$(this).val();
                <?php
                }
                ?>

            });

            // 20160127 vernhui Enable user to change password for their payslips
            window.close_modal_password = function(msg){
                $('#home_page_modal').modal('hide');
                bootbox.alert(msg);
            };
        });
    </script>
    <script type="text/javascript">
        $('.tree').treegrid({
            initialState: 'collapsed'
        });

        $('body').on("submit",function(){

            $(".modal-backdrop.in").show();

            setInterval(function(){
                $(".modal-backdrop.in").hide();
            }, 1500);

        });

        $.ajaxSetup({
            beforeSend: function() {
                $(".modal-backdrop.in").show();
            },
            complete: function() {
                $(".modal-backdrop.in").hide();
            }
        });

        $(".nav-tabs_sme-mobile").click(function(){
            var d=$(this);
            var e=d.parents(".nav-tabs_sme-wrap");
            if(e.find(".nav-tabs_sme").hasClass("nav-tap-open")) {
                e.find(".nav-tabs_sme").removeClass("nav-tap-open");
                e.find(".nav-tabs_sme").slideUp()
            } else {
                e.find(".nav-tabs_sme").addClass("nav-tap-open");
                e.find(".nav-tabs_sme").slideDown();
            }
        });
        $(document).mouseup(function(g){
            var f=$(".user-info .ui-detail");
            if(!f.is(g.target)&&f.has(g.target).length===0) {
                f.find(".ui-selectmenu-button").removeClass("ui-selectmenu-button-open");
                f.find(".ui-selectmenu-menu").removeClass("ui-front ui-selectmenu-open")
            }
            var d=$(".nav-tabs_sme-wrap");
            var h=$(".nav-tabs_sme-mobile");
            if(h.is(":visible")&&d.length&&!d.is(g.target)&&d.has(g.target).length===0) {
                d.find(".nav-tabs_sme").removeClass("nav-tap-open").slideUp();
            }
        });
    </script>
</body>
</html>