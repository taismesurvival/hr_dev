<style>
    .choose_account {cursor: pointer;}
    #table_select_project td {font-size: 14px;}
    #accounts_list td {font-size: 14px;line-height: 2.8;}
</style>
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Sales Collection Submit History
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Sales Collection Submit History</a>
        </li>
</div>
<!-- END PAGE HEADER -->
<!-- BEGIN ALERT BOX -->
<?php if (isset($flash_message)) { ?>
    <div class="row margin-bottom-20">
        <div class="col-xs-12">
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END ALERT BOX -->

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Sales Collection Submit History</span>
                </div>
                <a href="<?php echo base_url('sales_collection') ?>" class="btn btn-md blue pull-right"><i class="fa fa-plus"></i> Sales Collection</a>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-12">

                        <!-- Begin: life time stats -->
                        <div class="table-container">
                            <!-- Begin: life time stats -->
                            <div class="clearfix mt15">

                                <form method="get" class="form" role="form">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <?php echo form_label('Payment Method', 'description', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <select id="payment_method" name="payment_method" class="form-control">
                                                        <option selected value="">Please Select</option>
                                                        <option value="cash">Cash</option>
                                                        <option value="visa_and_master">Visa and Master Card</option>
                                                        <option value="nets">NETS</option>
                                                        <option value="american_express">American Express</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="row">
                                                <?php echo form_label('Document Date', 'keyword', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <input type="text" name="document_date_from" class="form-control date-picker" value="<?php echo $search['document_date_from'] ?>" placeholder="Starting Date">
                                                </div>

                                                <?php echo form_label('~', 'description', array('class' => 'col-md-2 col-lg-1 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <input type="text" name="document_date_to" class="form-control date-picker" value="<?php echo $search['document_date_to'] ?>" placeholder="Ending Date">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 text-right">
                                            <a href="<?php echo base_url('sales_collection_submit') ?>" class=" btn btn-md grey-salsa">Reset</a>
                                            <button type="submit" class="btn btn-md green"><i class="icon-magnifier font-white"></i>Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <table style="margin-top: 20px;" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="4%" class="text-right">No</th>
                                    <th width="8%">Payment Method</th>
                                    <th width="8%">Bank In Amount</th>
                                    <th width="8%">Document Date</th>
                                    <th width="8%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php for ($i = 0; $i < count($GL_entries); $i++) {

                                        switch ($GL_entries[$i]['payment_method']) {

                                            case 'cash': $payment_method = 'Cash';
                                            break;

                                            case 'visa_and_master': $payment_method = 'Visa and Master Card';
                                                break;

                                            case 'nets': $payment_method = 'NETS';
                                                break;

                                            case 'american_express': $payment_method = 'American Express';
                                                break;

                                        }
                                        ?>
                                    <tr role="row" class="heading">
                                        <td class="text-right"><?php echo $pagination['offset']+$i ?></td>
                                        <td class="GL_entry_id hidden"><?php echo $GL_entries[$i]['id'] ?></td>
                                        <td><?php echo $payment_method; ?></td>
                                        <td><?php echo number_format($GL_entries[$i]['bank_in_amount'], 2); ?></td>
                                        <td><?php echo $GL_entries[$i]['document_date']; ?></td>
                                        <td>
                                            <?php if (empty($GL_entries[$i]['clear_date']) && date('Y-m', strtotime($GL_entries[$i]['document_date'])) > $locked_financial_month) { ?>
                                            <a href="#" class="btn btn-md red GL_entry-delete"><i class="fa fa-trash-o"></i> Delete</a>
                                            <?php } ?>
                                            <a href="#" class="btn btn-md yellow GL_entry-view"><i class="fa fa-eye"></i> View</a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
                <nav class="pagination-wrap clearfix">
                    <div class="pull-left mt10">
                        <?php if ($pagination['total'] != 0) { ?>
                        <label class="form-control-static"><?php echo $pagination['total'] ?> Items in total, Display
                            <?php echo $pagination['offset'] ?> ~
                            <?php echo $pagination['limit'] ?></label>
                        <?php } ?>
                    </div>
                    <?php if (!empty($pagination) && $pagination['total'] > $pagination['items_per_page']) { ?>
                    <div class="pull-right">
                        <ul class="pagination no-margin">
                            <li <?php if ($pagination['page'] - 1 <= 0) { ?> class="disabled" <?php } ?>>

                            <a href="<?php if ($pagination['page'] - 1 <= 0){ ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] - 1 ?><?php } ?>">
                                Previous
                            </a>
                            </li>
                            <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                            <li>
                                <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=1"> 1 </a>
                            </li>
                            <?php } ?>

                            <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                            <li><a href="javascript:;">...</a></li>
                            <?php } ?>

                            <?php for ($p=($pagination['min_display']); $p <= $pagination['max_display']; $p++) { ?>
                            <?php if ($p > 0) { ?>
                            <li <?php if ($pagination['page'] == $p) { ?> class="active" <?php } ?>>
                            <a href="<?php if ($pagination['page'] != $p) { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $p ?><?php } else { ?>javascript:;<?php } ?>"> <?php echo $p ?> </a>
                            </li>
                            <?php } ?>
                            <?php } ?>

                            <?php if (($pagination['page'] + $pagination['adj']) < $pagination['total_page']) { ?>
                            <li><a href="javascript:;">...</a></li>
                            <?php } ?>

                            <?php if ($pagination['page'] + $pagination['adj'] < $pagination['total_page']) { ?>
                            <li>
                                <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['total_page'] ?>"> <?php echo $pagination['total_page'] ?> </a>
                            </li>
                            <?php } ?>

                            <li <?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> class="disabled" <?php } ?>>
                            <a href="<?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] + 1 ?><?php } ?>">
                                Next
                            </a>
                            </li>
                        </ul>
                    </div>
                    <?php } ?>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="GL_entry-delete-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Delete Submission</h4>
            </div>
            <div class="modal-body">

                <p class="text-center">Can you confirm to delete the Submission</p>

                <?php echo form_open_multipart(base_url('sales_collection_submit/delete'), 'class="form-horizontal" method="post" data-parsley-validate'); ?>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_delete_id">
                <button  type="submit" class="btn btn-md red">Delete</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {

        $('.date-picker').datepicker({
            format: "yyyy-mm-dd"
        });

        var payment_method = '<?php echo $search['payment_method']; ?>';

        $('#payment_method').val(payment_method);

        $(".GL_entry-delete").on("click",function(){
            var el = $(this);
            var GL_entry_id = el.closest('tr').find('.GL_entry_id').html();
            $('#modal_delete_id').val(GL_entry_id);

            $('#GL_entry-delete-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });
    });
</script>
