<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Company Dashboard
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Company Dashboard</a>
        </li>
</div>

<?php if (isset($flash_message)) { ?>
<div class="row margin-bottom-20">
    <div class="col-xs-12">
        <div class="alert alert-success">
            <button class="close" data-close="alert"></button>
            <?php echo $flash_message; ?>
        </div>
    </div>
</div>
<?php } ?>

<div class="row">
    <div class="col-xs-12">
        <div class="portlet box blue hoki">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-tasks"></i>
                        Company Dashboard
                </div>
            </div>
            <div class="portlet-body" style="">
                <div class="tiles">
                    <a class="tile  bg-blue-steel" href="<?php echo base_url("company/profile") ?>">
                        <div class="tile-body">
                            <i class="fa fa-building"></i>
                        </div>
                        <div class="tile-object text-center">
                            Company Information
                        </div>
                    </a>
                    <a class="tile  bg-green-seagreen" href="<?php echo base_url("financial_period") ?>">
                        <div class="tile-body">
                            <i class="fa fa-list"></i>
                        </div>
                        <div class="tile-object text-center">
                            Financial Periods
                        </div>
                    </a>
                    <a class="tile  bg-red-flamingo" href="<?php echo base_url("user") ?>">
                        <div class="tile-body">
                            <i class="fa fa-sitemap"></i>
                        </div>
                        <div class="tile-object text-center">
                            Users
                        </div>
                    </a>
                    <a class="tile  bg-purple-studio" href="<?php echo base_url("project") ?>">
                        <div class="tile-body">
                            <i class="fa fa-credit-card"></i>
                        </div>
                        <div class="tile-object text-center">
                            Outlets
                        </div>
                    </a>
                    <a class="tile  bg-yellow-gold" href="<?php echo base_url("bank_account") ?>">
                        <div class="tile-body">
                            <i class="fa fa-credit-card"></i>
                        </div>
                        <div class="tile-object text-center">
                            Bank & Cash
                        </div>
                    </a>
                    <a class="tile  bg-green" href="<?php echo base_url("payment_method") ?>">
                        <div class="tile-body">
                            <i class="fa fa-credit-card"></i>
                        </div>
                        <div class="tile-object text-center">
                            Payment Method
                        </div>
                    </a>
                    <a class="tile  bg-green" href="<?php echo base_url("HGW_payment_method_mapping") ?>">
                        <div class="tile-body">
                            <i class="fa fa-credit-card"></i>
                        </div>
                        <div class="tile-object text-center">
                            HGW Mapping
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <?php foreach ($unmaping_payment_methods AS $unmap) {
            $HGW_payment_methods = join(', ', $unmap['HGW_payment_method']);
            ?>
        <p style="color: red; font-size: 16px; font-weight: bold;">HGW Unmapped: <span><?php echo 'Outlet: '.$unmap['outlet_name'].' ('.$unmap['outlet_code'].') HGW Unmapped Payment Methods: '.$HGW_payment_methods; ?></span></p>

        <?php  } ?>
        <p style="color: red; font-size: 14px;">Last Bank Recons Submit: <span><?php echo 'Bank Account: '.$clear_bank_recons['name'].' ('.$clear_bank_recons['number'].') Month: '.$clear_bank_recons['month']; ?></span></p>
        <p style="color: red; font-size: 14px;">Accounting Period locked as at: <span><?php echo $company['locked_financial_month'] ?></span></p>
        <p style="color: red; font-size: 14px;">Next Automatic Lock: <span><?php echo $locked_month.' will be locked on '.$next_auto_lock; ?></span></p>
    </div>
</div>


<!-- END SCRIPT -->

