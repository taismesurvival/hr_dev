<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Company Information
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo base_url("company"); ?>">Company Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Company Information</a>

        </li>
</div>
<!-- END PAGE HEADER-->

<div class="row">
    <div class="col-md-12">
        <div class="portlet light bordered">
            <div class="portlet-body">
                <?php echo form_open_multipart($action, 'class="form-horizontal" method="post" data-parsley-validate id="company_form"'); ?>
                    <div class="form-body">
                        <div class="form-group">
                            <?php echo form_label('Company Name <span style="color:red;" class="required">*</span>', 'name', array('class' => 'col-md-3 col-lg-2 control-label')); ?>

                            <div class="col-md-9 col-lg-10 form-group">
                                <?php echo form_input('name', set_value('name', isset($company) ? $company['name'] : ''), 'class="form-control" data-parsley-required="true" data-parsley-required-message="Company Name is required field"'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Company Code', 'company_id', array('class' => 'col-md-3 col-lg-2 control-label')); ?>

                            <div class="col-md-9 col-lg-10 form-group">
                                <?php echo form_input('company_id', set_value('company_id', isset($company) ? $company['company_id'] : ''), 'id="mask_code_1" class="form-control" data-parsley-required="true" disabled'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Company Address', 'company_address', array('class' => 'col-md-3 col-lg-2 control-label')); ?>

                            <div class="col-md-9 col-lg-10 form-group">
                                <?php echo form_input('company_address', set_value('company_address', isset($company) ? $company['company_address'] : ''), 'class="form-control"'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Postal Code', 'postal_code', array('class' => 'col-md-3 col-lg-2 control-label')); ?>

                            <div class="col-md-9 col-lg-10 form-group">
                                <?php echo form_input('postal_code', set_value('postal_code', isset($company) ? $company['postal_code'] : ''), 'class="form-control"'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Email <span style="color:red;" class="required">*</span>', 'email', array('class' => 'col-md-3 col-lg-2 control-label')); ?>

                            <div class="col-md-9 col-lg-10 form-group">
                                <?php echo form_input('email', set_value('email', isset($company) ? $company['email'] : ''), 'class="form-control" data-parsley-type="email" data-parsley-required="true" data-parsley-required-message="Company Email is required field"'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Phone', 'phone', array('class' => 'col-md-3 col-lg-2 control-label')); ?>

                            <div class="col-md-9 col-lg-10 form-group">
                                <?php echo form_input('phone', set_value('phone', isset($company) ? $company['phone'] : ''), 'class="form-control"'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Fax', 'fax', array('class' => 'col-md-3 col-lg-2 control-label')); ?>

                            <div class="col-md-9 col-lg-10 form-group">
                                <?php echo form_input('fax', set_value('fax', isset($company) ? $company['fax'] : ''), 'class="form-control"'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('GST setting', 'GST_setting', array('class' => 'col-md-3 col-lg-2 control-label', 'data-toggle' => "tooltip", 'data-original-title' => "Switch On or Off for GST setting, Be careful, this one will affect too many things")); ?>

                            <div class="col-md-9 col-lg-10 form-group">
                                <input type="radio" name="GST_setting" value="active" <?php echo $company['GST_setting'] == 'active' ? 'checked="checked"' : ''; ?>><?php echo form_label('Active', 'GST_setting');?>
                                <input type="radio" name="GST_setting" value="inactive" <?php echo $company['GST_setting'] == 'inactive' ? 'checked="checked"' : ''; ?>><?php echo form_label('Inactive', 'GST_setting');?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('Company Register Number', 'company_register_no', array('class' => 'col-md-3 col-lg-2 control-label', 'data-toggle' => "tooltip", 'data-original-title' => "Company Register No will appear in Restaurant Bills")); ?>

                            <div class="col-md-9 col-lg-10 form-group">
                                <?php echo form_input('company_register_no', set_value('company_register_no', isset($company) ? $company['company_register_no'] : ''), 'class="form-control"'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php echo form_label('GST Company Register Number', 'GST_company_register_no', array('class' => 'col-md-3 col-lg-2 control-label', 'data-toggle' => "tooltip", 'data-original-title' => "GST Register No will appear in Restaurant Bills")); ?>

                            <div class="col-md-9 col-lg-10 form-group">
                                <?php echo form_input('GST_company_register_no', set_value('GST_company_register_no', isset($company) ? $company['GST_company_register_no'] : ''), 'class="form-control"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions text-right">
                        <a href="<?php echo base_url("company"); ?>" class="btn btn-md grey-salsa">Cancel</a>
                        <button id="company_submit" type="button" class="btn btn-md blue">Submit</button>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>


<script>
    jQuery(document).ready(function () {

        $('[data-toggle="tooltip"]').tooltip();

        $("#company_submit").on("click", function () {

            $('#show_error_message').hide();
            $("#company_form").submit();
            return;
        });
    });
</script>