<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Outlet
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url('company') ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo base_url('company') ?>">Company Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Outlet</a>
        </li>
</div>
<!-- END PAGE HEADER -->
<!-- BEGIN ALERT BOX -->
<?php if (isset($flash_message)) { ?>
    <div class="row margin-bottom-20">
        <div class="col-xs-12">
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END ALERT BOX -->

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Outlet</span>
                </div>
                <a href="#" class="project-add btn btn-md blue pull-right"><i class="fa fa-plus"></i> Add New</a>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-12">

                        <!-- Begin: life time stats -->
                        <div class="table-container">
                            <div class="clearfix mt15">

                                <form method="get" class="form" role="form">
                                    <div class="row">
                                        <div class="col-md-3 col-xs-12">
                                            <label>Status</label>
                                            <div class="form-group">
                                                <?php echo form_dropdown('status', ['' => 'Please Select', 'active' => 'Active', 'inactive' => 'Inactive'], set_value('status', $search['status']), 'class="form-control"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 text-right">
                                            <a href="<?php echo base_url('project') ?>" class=" btn btn-md grey-salsa">Reset</a>
                                            <button type="submit" class="btn btn-md green"><i class="icon-magnifier font-white"></i>Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <!-- Begin: life time stats -->
                            <table style="margin-top: 20px;" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="7%" class="text-right">No</th>
                                    <th width="11%">Outlet Name</th>
                                    <th width="11%">Outlet Code</th>
                                    <th width="13%">Outlet Address</th>
                                    <th width="11%">Outlet Type</th>
                                    <th width="11%">Status</th>
                                    <th width="20%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php for ($i = 0; $i < count($projects); $i++) { ?>
                                    <tr role="row" class="heading">
                                        <td class="text-right"><?php echo $i+1 ?></td>
                                        <td class="project_id hidden"><?php echo $projects[$i]['id'] ?></td>
                                        <td class="name"><?php echo $projects[$i]['name']; ?></td>
                                        <td class="code"><?php echo $projects[$i]['code']; ?></td>
                                        <td class="address"><?php echo $projects[$i]['address']; ?></td>
                                        <td class="phone hidden"><?php echo $projects[$i]['phone']; ?></td>
                                        <td class="fax hidden"><?php echo $projects[$i]['fax']; ?></td>
                                        <td class="type hidden"><?php echo $projects[$i]['type']; ?></td>
                                        <td class="secret_key hidden"><?php echo $projects[$i]['secret_key']; ?></td>
                                        <td><?php if ($projects[$i]['type'] == 'outlet') { ?>
                                                Outlet
                                            <?php } elseif ($projects[$i]['type'] == 'operating') { ?>
                                                Operating
                                            <?php } else { echo 'Director';} ?></td>
                                        <td class="status hidden"><?php echo $projects[$i]['status']; ?></td>
                                        <td><?php if ($projects[$i]['status'] == 'active') { ?>
                                                <span class="label label-success">ACTIVE</span>
                                            <?php } else { ?>
                                                <span class="label label-warning">INACTIVE</span>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <a class="project-edit btn btn-md blue"><i class="fa fa-pencil"></i> Edit</a>
                                            <!--<a href="#" class="btn btn-md red project-delete"><i class="fa fa-trash-o"></i> Delete</a>-->
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="project-edit-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="project-modal-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">

                <div id="show_error_message" class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span id="error_message"></span>
                </div>

                <?php echo form_open_multipart(base_url('project/edit'), 'class="form-horizontal" method="post" data-parsley-validate id="project-edit_form"'); ?>

                <div class="form-group">
                    <?php echo form_label('Outlet Name <span style="color:red;" class="required">*</span>', 'name', array('class' => 'col-md-6 col-lg-5 control-label')); ?>

                    <div class="col-md-6 col-lg-7 form-group">
                        <?php echo form_input('name', set_value('name', ''), 'class="form-control" id="modal_name" data-parsley-required="true" data-parsley-required-message="Project Name is required field"'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo form_label('Outlet Code <span style="color:red;" class="required">*</span>', 'code', array('class' => 'col-md-6 col-lg-5 control-label', 'data-toggle' => "tooltip", 'data-original-title' => "Be careful, don't change. Outlet Code will appear in Restaurant Bills or be used to import POS data")); ?>

                    <div class="col-md-6 col-lg-7 form-group">
                        <?php echo form_input('code', set_value('code', ''), 'class="form-control" id="modal_code" data-parsley-required="true" data-parsley-required-message="Project Code is required field"'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo form_label('Secret Key', 'secret_key', array('class' => 'col-md-6 col-lg-5 control-label', 'data-toggle' => "tooltip", 'data-original-title' => "Be careful, don't change. Secret Key will be used to import POS data")); ?>

                    <div class="col-md-6 col-lg-7 form-group">
                        <?php echo form_input('secret_key', set_value('secret_key', ''), 'class="form-control" id="modal_secret_key"'); ?>
                        <button style="float: right;" type="button" class="btn btn-md" id="generate_secret_key">Generate Secret Key</button>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo form_label('Outlet Address <span style="color:red;" class="required">*</span>', 'address', array('class' => 'col-md-6 col-lg-5 control-label')); ?>

                    <div class="col-md-6 col-lg-7 form-group">
                        <?php echo form_input('address', set_value('address', ''), 'class="form-control" id="modal_address" data-parsley-required="true" data-parsley-required-message="Project Address is required field"'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo form_label('Phone', 'phone', array('class' => 'col-md-6 col-lg-5 control-label')); ?>

                    <div class="col-md-6 col-lg-7 form-group">
                        <?php echo form_input('phone', set_value('phone', ''), 'class="form-control" id="modal_phone"'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo form_label('Fax', 'fax', array('class' => 'col-md-6 col-lg-5 control-label')); ?>

                    <div class="col-md-6 col-lg-7 form-group">
                        <?php echo form_input('fax', set_value('fax', ''), 'class="form-control" id="modal_fax"'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-6 col-lg-5 control-label">Status <span style="color:red;" class="required">*</span></label>

                    <div class="col-md-6 col-lg-7 form-group">
                        <?php echo form_dropdown('status', ['' => 'Please Select', 'active' => 'Active', 'inactive' => 'Inactive'], set_value('status', ''), 'id="modal_status" class="form-control" data-parsley-required="true" data-parsley-required-message="Project Status is required field"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-6 col-lg-5 control-label">Outlet Type <span style="color:red;" class="required">*</span></label>

                    <div class="col-md-6 col-lg-7 form-group">
                        <?php echo form_dropdown('type', ['' => 'Please Select', 'outlet' => 'Outlet', 'operating' => 'Operating', 'director' => 'Director'], set_value('type', ''), 'id="modal_type" class="form-control" data-parsley-required="true" data-parsley-required-message="Project Type is required field"'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_id">
                <button  type="button" class="btn btn-md blue" id="project_submit">Submit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<div class="modal" id="project-delete-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Delete Outlet</h4>
            </div>
            <div class="modal-body">

                <p class="text-center">Can you confirm to delete Outlet?</p>

                <?php echo form_open_multipart(base_url('project/delete'), 'class="form-horizontal" method="post" data-parsley-validate'); ?>


            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_delete_id">
                <button  type="submit" class="btn btn-md red">Delete</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {

        $('[data-toggle="tooltip"]').tooltip();

        $('.date-picker').datepicker({
            format: "yyyy-mm-dd"
        });

        $("#project_submit").on("click",function(){

            $('#show_error_message').hide();
            $("#project-edit_form").submit();
        });

        $("#generate_secret_key").on("click",function(){

            var rString = randomString(32, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
            $('#modal_secret_key').val(rString);
        });

        function randomString(length, chars) {
            var result = '';
            for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
            return result;
        }

        $(".project-edit").on("click",function(){
            $("#modal_type").parsley().reset();
            $("#modal_status").parsley().reset();
            $("#modal_name").parsley().reset();
            $("#modal_code").parsley().reset();
            $("#modal_secret_key").parsley().reset();
            $("#modal_address").parsley().reset();

            var el = $(this);
            var name = el.closest('tr').find('.name').html();
            var status = el.closest('tr').find('.status').html();
            var code = el.closest('tr').find('.code').html();
            var secret_key = el.closest('tr').find('.secret_key').html();
            var address = el.closest('tr').find('.address').html();
            var phone = el.closest('tr').find('.phone').html();
            var fax = el.closest('tr').find('.fax').html();
            var type = el.closest('tr').find('.type').html();
            var project_id = el.closest('tr').find('.project_id').html();

            $('#show_error_message').hide();

            $('#modal_name').val(name);
            $('#modal_status').val(status);
            $('#modal_code').val(code);
            $('#modal_secret_key').val(secret_key);
            $('#modal_address').val(address);
            $('#modal_phone').val(phone);
            $('#modal_fax').val(fax);
            $('#modal_type').val(type);
            $('#modal_id').val(project_id);

            $('#project-modal-title').html('Edit Outlet');

            $('#project-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $(".project-delete").on("click",function(){
            var el = $(this);
            var project_id = el.closest('tr').find('.project_id').html();
            $('#modal_delete_id').val(project_id);

            $('#project-delete-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $(".project-add").on("click",function(){
            $("#modal_type").parsley().reset();
            $("#modal_status").parsley().reset();
            $("#modal_name").parsley().reset();
            $("#modal_code").parsley().reset();
            $("#modal_address").parsley().reset();

            $('#modal_name').val('');
            $('#modal_status').val('');
            $('#modal_code').val('');
            $('#modal_type').val('');
            $('#modal_address').val('');
            $('#modal_phone').val('');
            $('#modal_fax').val('');
            $('#modal_id').val('');
            $('#modal_secret_key').val('');
            $('#show_error_message').hide();

            $('#project-modal-title').html('Add Outlet');

            $('#project-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });
    });
</script>
