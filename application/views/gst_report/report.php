<style>
    table { border-collapse: collapse; }
    tr.border-top { border-top: solid thin; }
    tr.border-bottom { border-bottom: solid thin; }
    tr.bold { font-weight: bold; }
    * { font-size: 15px; font-family: Lucida Sans Unicode; }
</style>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/slider-pips/jquery-ui.js"></script>
<button type="button" id="download_excel_file">Download Excel Report</button>
<button type="button" id="download_pdf_file">Print or Download PDF Report</button>
<div id="table_wrapper">
    <table style='width:100%;'>

        <tr>
            <td style='text-align:left;'>
                <table style='color:blue;width:100%;'>
                    <tr>
                        <td style='font-size:20px;text-align:center;'><?php echo $company_name; ?></td>
                    </tr>
                    <tr>
                        <td style='font-size:16px;text-align:center;'><?php echo $starting_month; ?> - <?php echo $ending_month; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style='text-align:center;font-size:18px;'>GST Report [Summary]</td>
        </tr>
    </table>
    <table style='width:100%;'>
        <thead>
            <tr class='border-top'>
                <th colspan="2" style='width:5%;'>Declaration</th>
                <th style='width:5%;  text-align:center;'>Value</th>
            </tr>
        </thead>
        <tbody id="accounts_table">
            <tr>
                <td colspan="2" style='height:40px;'>1, Total Value of Standard-Rated Supplies</td>
                <td class="value_amount" style='text-align:center;'><?php echo $sales_total; ?></td>
            </tr>
            <tr>
                <td colspan="2" style='height:40px;'>2a, Purchases From GST Registered Suppliers</td>
                <td class="value_amount" style='text-align:center;'><?php echo $gst_purchases_total; ?></td>
            </tr>
            <tr>
                <td colspan="2" style='height:40px;'>2b, Import Purchases</td>
                <td class="value_amount" style='text-align:center;'><?php echo $import_purchases_total; ?></td>
            </tr>
            <tr>
                <td colspan="2" style='height:40px;'>2c, Exempt Purchases</td>
                <td class="value_amount" style='text-align:center;'><?php echo $exempt_purchases_total; ?></td>
            </tr>
            <tr>
                <td colspan="2" style='height:40px;'>3, Total Value of Taxable Purchases (2a+2b)</td>
                <td class="value_amount" style='text-align:center;'><?php echo $import_purchases_total+$gst_purchases_total; ?></td>
            </tr>
            <tr>
                <td colspan="2" style='height:40px;'>4, Output Tax Due</td>
                <td class="value_amount" style='text-align:center;'><?php echo $sales_tax_total; ?></td>
            </tr>
            <tr>
                <td colspan="2" style='height:40px;'>5, Less: Input Tax And Refunds Claimed</td>
                <td class="value_amount" style='text-align:center;'><?php echo $purchases_tax_total; ?></td>
            </tr>
            <tr>
                <td colspan="2" style='height:40px;'>6, Equals: Net GST To Be Paid To IRAS</td>
                <td class="value_amount" style='text-align:center;'><?php echo $sales_tax_total >= $purchases_tax_total ? ($sales_tax_total-$purchases_tax_total) : 0; ?></td>
            </tr>
            <tr>
                <td colspan="2" style='height:40px;' id="tax_claimed">&nbsp;&nbsp;&nbsp;Or: Net GST To Be Claimed From IRAS</td>
                <td class="value_amount" style='text-align:center;'><?php echo $sales_tax_total <= $purchases_tax_total ? ($purchases_tax_total-$sales_tax_total) : 0; ?></td>
            </tr>
        </tbody>
    </table>
</div>
<script>

    jQuery(document).ready(function () {

        $('.value_amount').each(function() {
            var el = $(this);

            var html_amount = el.html();

            if (html_amount != '') {
                el.html(numberWithCommas(parseFloat(html_amount).toFixed(2)));
            }
        });

        function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }
    });

    $(document).ready(function() {
        $("#download_excel_file").click(function(e) {
            e.preventDefault();

            //getting data from our table
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('table_wrapper');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');

            var a = document.createElement('a');
            a.href = data_type + ', ' + table_html;
            a.download = 'gst_report_summary.xls';
            a.click();
        });

        $('body').mouseover(function () {
            $("#download_excel_file").show();
            $("#download_pdf_file").show();
        });

        $("#download_pdf_file").click(function () {
            $("#download_excel_file").hide();
            $("#download_pdf_file").hide();
            window.print();
        });
    });
</script>