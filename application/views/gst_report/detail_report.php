<style>
    table { border-collapse: collapse; }
    tr.border-top { border-top: solid thin; }
    tr.border-bottom { border-bottom: solid thin; }
    tr.bold { font-weight: bold; }
    * { font-size: 14px; font-family: Lucida Sans Unicode; }
</style>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/slider-pips/jquery-ui.js"></script>
<button type="button" id="download_excel_file">Download Excel Report</button>
<button type="button" id="download_pdf_file">Print or Download PDF Report</button>
<div id="table_wrapper">
    <table style='width:100%;'>

        <tr>
            <td style='text-align:left;'>
                <table style='color:blue;width:100%;'>
                    <tr>
                        <td style='font-size:20px;text-align:center;'><?php echo $company_name; ?></td>
                    </tr>
                    <tr>
                        <td style='font-size:16px;text-align:center;'><?php echo $starting_month; ?> - <?php echo $ending_month; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style='text-align:center;font-size:18px;'>GST Report [Detail]</td>
        </tr>
    </table>
    <table style='width:100%;'>
        <thead>
            <tr>
                <th colspan="3">Value of Input Tax Amount</th>
            </tr>
            <tr class='border-top'>
                <th style='width:5%;'>Ref No</th>
                <th style='width:5%; text-align:center;'>Date</th>
                <th style='width:5%; text-align:center;'>Supplier Name</th>
                <th style='width:5%; text-align:center;'>Supplier Ref No</th>
                <th style='width:5%; text-align:center;'>Tax Code</th>
                <th style='width:5%; text-align:center;'>Taxable Amount</th>
                <th style='width:5%; text-align:center;'>GST</th>
                <th style='width:5%; text-align:center;'>Zero Rated</th>
                <th style='width:5%; text-align:center;'>Import</th>
                <th style='width:5%; text-align:center;'>Others</th>
                <th style='width:5%; text-align:center;'>Total</th>
            </tr>
        </thead>
        <tbody id="accounts_table">
        <?php foreach ($purchase_trans_data AS $key => $item) { ?>
            <tr>
                <td style='text-align:center;'><?php echo 'PR'.$item['id']; ?></td>
                <td style='text-align:center;'><?php echo $item['document_date']; ?></td>
                <td style='text-align:center;'><?php echo $item['supplier_name']; ?></td>
                <td style='text-align:center;'><?php echo $item['refference_no']; ?></td>
                <td style='text-align:center;'><?php echo $item['tax_type']; ?></td>
                <td style='text-align:center;' class="trans_item taxable"><?php echo $item['tax_type'] == 'TX7' ? ($item['quantity']*$item['unit_cost']*(100-$item['discount_rate'])/100) : '0' ?></td>
                <td style='text-align:center;' class="trans_item gst"><?php echo round((($item['quantity']*$item['unit_cost']*(100-$item['discount_rate'])/100)*$item['tax_rate']/100), 2) ?></td>
                <td style='text-align:center;' class="trans_item zero"><?php echo $item['tax_type'] == 'ZP' ? ($item['quantity']*$item['unit_cost']*(100-$item['discount_rate'])/100) : '0' ?></td>
                <td style='text-align:center;' class="trans_item import1"><?php echo in_array($item['tax_type'], ['IM', 'IM7']) ? ($item['quantity']*$item['unit_cost']*(100-$item['discount_rate'])/100) : '0' ?></td>
                <td style='text-align:center;' class="trans_item others"><?php echo in_array($item['tax_type'], ['NR', 'NT', 'EP', 'OP']) ? ($item['quantity']*$item['unit_cost']*(100-$item['discount_rate'])/100) : '0' ?></td>
                <td style='text-align:center;' class="total_trans"></td>
            </tr>

            <?php if ($purchase_trans_data[$key+1]['supplier_id'] != $item['supplier_id']) {

                foreach ($credit_notes[$item['supplier_id']] AS $credit) { ?>

                    <tr>
                        <td style='text-align:center;'><?php echo 'CN'.$credit['id']; ?></td>
                        <td style='text-align:center;'><?php echo $credit['document_date']; ?></td>
                        <td style='text-align:center;'><?php echo $credit['supplier_name']; ?></td>
                        <td style='text-align:center;'><?php echo $credit['refference_no']; ?></td>
                        <td style='text-align:center;'><?php echo $credit['tax_type']; ?></td>
                        <td style='text-align:center;' class="trans_item taxable"><?php echo $credit['tax_type'] == 'TX7' ? ((-1)*$credit['amount']) : '0' ?></td>
                        <td style='text-align:center;' class="trans_item gst"><?php echo round(((-1)*$credit['tax_amount']), 2) ?></td>
                        <td style='text-align:center;' class="trans_item zero"><?php echo $item['tax_type'] == 'ZP' ? ((-1)*$credit['amount']) : '0' ?></td>
                        <td style='text-align:center;' class="trans_item import1"><?php echo in_array($item['tax_type'], ['IM', 'IM7']) ? ((-1)*$credit['amount']) : '0' ?></td>
                        <td style='text-align:center;' class="trans_item others"><?php echo in_array($item['tax_type'], ['NR', 'NT', 'EP', 'OP']) ? ((-1)*$credit['amount']) : '0' ?></td>
                        <td style='text-align:center;' class="total_trans"></td>
                    </tr>

                <?php }

           } ?>
        <?php } ?>
        </tbody>
        <tbody>
            <tr id="input_tax_total">
                <td colspan="5" style='text-align:right;font-weight: bold;font-size: 15px;' class='border-top'>Total</td>
                <td style='text-align:center;' class="taxable"></td>
                <td style='text-align:center;' class="gst"></td>
                <td style='text-align:center;' class="zero"></td>
                <td style='text-align:center;' class="import1"></td>
                <td style='text-align:center;' class="others"></td>
                <td style='text-align:center;' class="total_trans_total"></td>
            </tr>
        </tbody>
    </table>

    <table style='width:100%; margin-top:50px;'>
        <thead>
        <tr>
            <th colspan="3">Value of Ouput Tax Amount</th>
        </tr>
        <tr class='border-top'>
            <th style='width:5%;'>Ref No</th>
            <th style='width:5%; text-align:center;'>Date</th>
            <th style='width:5%; text-align:center;'>Customer Name</th>
            <th style='width:5%; text-align:center;'>Taxable Amount</th>
            <th style='width:5%; text-align:center;'>GST</th>
            <th style='width:5%; text-align:center;'>Total</th>
        </tr>
        </thead>
        <tbody id="sales_table">
        <?php foreach ($sales_data AS $key => $item) { ?>
            <?php if (!empty($item['taxable_amount']) && !empty($item['tax_amount'])) { ?>
            <tr>
                <td style='text-align:center;'><?php echo 'GE'.$key; ?></td>
                <td style='text-align:center;'><?php echo $item['document_date']; ?></td>
                <td style='text-align:center;'><?php echo $item['description']; ?></td>
                <td style='text-align:center;' class="trans_item taxable"><?php echo $item['taxable_amount']; ?></td>
                <td style='text-align:center;' class="trans_item gst"><?php echo !empty($item['tax_amount']) ? $item['tax_amount'] : 0; ?></td>
                <td style='text-align:center;' class="total_trans"></td>
            </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
        <tbody>
        <tr id="output_tax_total">
            <td colspan="3" style='text-align:right;font-weight: bold;font-size: 15px;' class='border-top'>Total</td>
            <td style='text-align:center;' class="taxable"></td>
            <td style='text-align:center;' class="gst"></td>
            <td style='text-align:center;' class="total_trans_total"></td>
        </tr>
        </tbody>
    </table>
</div>
<script>

    jQuery(document).ready(function () {

        var taxable = 0;
        var gst = 0;
        var zero = 0;
        var import1 = 0;
        var others = 0;
        var total_trans1 = 0;

        $('#accounts_table tr').each(function() {
            var el = $(this);

            var total_trans = 0;
            el.find('.trans_item').each(function() {
                var el2 = $(this);
                total_trans += parseFloat(el2.html());
            });

            el.find('.total_trans').html(total_trans);

            taxable += parseFloat(el.find('.taxable').html());
            gst += parseFloat(el.find('.gst').html());
            zero += parseFloat(el.find('.zero').html());
            import1 += parseFloat(el.find('.import1').html());
            others += parseFloat(el.find('.others').html());
            total_trans1 += total_trans;
        });

        $('#input_tax_total .taxable').html(taxable);
        $('#input_tax_total .gst').html(gst);
        $('#input_tax_total .zero').html(zero);
        $('#input_tax_total .import1').html(import1);
        $('#input_tax_total .others').html(others);
        $('#input_tax_total .total_trans_total').html(total_trans1);

        // sales
        var taxable = 0;
        var gst = 0;
        var total_trans1 = 0;

        $('#sales_table tr').each(function() {
            var el = $(this);

            var total_trans = 0;
            el.find('.trans_item').each(function() {
                var el2 = $(this);
                total_trans += parseFloat(el2.html());
            });

            el.find('.total_trans').html(total_trans);

            taxable += parseFloat(el.find('.taxable').html());
            gst += parseFloat(el.find('.gst').html());
            total_trans1 += total_trans;
        });

        $('#output_tax_total .taxable').html(taxable);
        $('#output_tax_total .gst').html(gst);
        $('#output_tax_total .total_trans_total').html(total_trans1);

        $('.taxable, .gst, .zero, .import1, .others, .total_trans_total, .total_trans, .trans_item').each(function() {
            var el = $(this);

            var html_amount = el.html();

            el.html(numberWithCommas(parseFloat(html_amount).toFixed(2)));
        });

        function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }
    });

    $(document).ready(function() {
        $("#download_excel_file").click(function(e) {
            e.preventDefault();

            //getting data from our table
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('table_wrapper');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');

            var a = document.createElement('a');
            a.href = data_type + ', ' + table_html;
            a.download = 'gst_report_detail.xls';
            a.click();
        });

        $('body').mouseover(function () {
            $("#download_excel_file").show();
            $("#download_pdf_file").show();
        });

        $("#download_pdf_file").click(function () {
            $("#download_excel_file").hide();
            $("#download_pdf_file").hide();
            window.print();
        });
    });
</script>