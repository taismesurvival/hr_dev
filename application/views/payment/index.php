<style>
    .choose_account {cursor: pointer;}
</style>
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Payment
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Payment</a>
        </li>
</div>
<!-- END PAGE HEADER -->
<!-- BEGIN ALERT BOX -->
<?php if (isset($flash_message)) { ?>
    <div class="row margin-bottom-20">
        <div class="col-xs-12">
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END ALERT BOX -->

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Payment</span>
                </div>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-12">

                        <!-- Begin: life time stats -->
                        <div class="table-container">
                            <!-- Begin: life time stats -->
                            <div class="clearfix mt15">

                                <form method="get" class="form" role="form">
                                    <div class="row">
                                        <div class="form-group">
                                            <?php echo form_label('Total Amount', 'description', array('class' => 'col-md-3 col-lg-2 control-label')); ?>

                                            <div class="col-md-3 col-lg-4 form-group">
                                                <input type="text" name="minimum_amount" class="form-control" value="<?php echo $search['minimum_amount'] ?>" placeholder="Minimum Amount">
                                            </div>

                                            <?php echo form_label('~', 'description', array('class' => 'col-md-2 col-lg-1 control-label')); ?>
                                            <div class="col-md-3 col-lg-4 form-group">
                                                <input type="text" name="maximum_amount" class="form-control" value="<?php echo $search['maximum_amount'] ?>" placeholder="Maximum Amount">
                                            </div>

                                        </div>

                                        <div class="col-xs-12">
                                            <div class="row">
                                                <?php echo form_label('Account Code or Name', 'description', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <input type="text" name="account_keyword" class="form-control" value="<?php echo $search['account_keyword'] ?>">
                                                </div>

                                                <?php echo form_label('Keyword', 'keyword', array('class' => 'col-md-2 col-lg-1 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <input type="text" name="keyword" class="form-control" value="<?php echo $search['keyword'] ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="row">
                                                <?php echo form_label('Document Date', 'keyword', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <input type="text" name="document_date_from" class="form-control date-picker" value="<?php echo $search['document_date_from'] ?>" placeholder="Starting Date">
                                                </div>

                                                <?php echo form_label('~', 'description', array('class' => 'col-md-2 col-lg-1 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <input type="text" name="document_date_to" class="form-control date-picker" value="<?php echo $search['document_date_to'] ?>" placeholder="Ending Date">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="row">
                                                <?php echo form_label('Supplier Name', 'supplier_name', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <input type="text" name="supplier_name" class="form-control" value="<?php echo $search['supplier_name'] ?>" placeholder="Supplier Name">
                                                </div>

                                                <?php echo form_label('Status', 'status', array('class' => 'col-md-2 col-lg-1 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <select name="status" class="form-control">
                                                        <option selected value="">Please Select</option>
                                                        <option <?php echo $search['status'] == 'done' ? 'selected' : '' ?> value="done">Approved</option>
                                                        <option <?php echo $search['status'] == 'pending' ? 'selected' : '' ?> value="pending">Pending</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 text-right">
                                            <a href="<?php echo base_url('payment') ?>" class=" btn btn-md grey-salsa">Reset</a>
                                            <button type="submit" class="btn btn-md green"><i class="icon-magnifier font-white"></i>Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <p style="font-size: 14px;font-weight: bold;">Payment Amount: <span id="total_selected">0.00</span></p>
                            <table style="margin-top: 20px;" class="table table-bordered">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="4%" class="text-right">No</th>
                                    <th width="8%">Document Date</th>
                                    <th width="15%">Pay To</th>
                                    <th width="8%">Status</th>
                                    <th width="8%">Amount</th>
                                    <th width="8%">Reference No</th>
                                    <th width="10%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php for ($i = 0; $i < count($payments); $i++) {
                                        ?>
                                    <tr role="row" class="heading">
                                        <td class="text-right">
                                            <input <?php echo $payments[$i]['status'] == 'done' ? 'disabled' : ''; ?> type="checkbox" name="payment_select[]" class="payment_select" value="<?php echo $payments[$i]['id'] ?>">
                                            <?php echo $pagination['offset']+$i ?>
                                        </td>
                                        <td class="payment_id hidden"><?php echo $payments[$i]['id'] ?></td>
                                        <td class="GL_transaction_id hidden"><?php echo $payments[$i]['GL_transaction_id'] ?></td>
                                        <td class="supplier_id hidden"><?php echo $payments[$i]['supplier_id'] ?></td>
                                        <td class="supplier_name hidden"><?php echo $payments[$i]['supplier_name'] ?></td>
                                        <td class="ref_no hidden"><?php echo $payments[$i]['refference_no'] ?></td>
                                        <td class="purchase_entry_id hidden"><?php echo $payments[$i]['purchase_entry_id'] ?></td>
                                        <td class="pay_date"><?php echo $payments[$i]['document_date']; ?></td>
                                        <td class="pay_to"><?php echo $payments[$i]['supplier_name']; ?></td>
                                        <td class="status"><?php echo $payments[$i]['status'] == 'pending' ? '<span class="label label-warning">PENDING</span>' : '<span class="label label-success">APPROVED</span>'; ?></td>
                                        <td class="total_amount"><?php echo number_format($payments[$i]['total_amount'],2); ?></td>
                                        <td class="description"><?php echo $payments[$i]['refference_no']; ?></td>
                                        <td>
                                            <?php if ($payments[$i]['status'] == 'pending' && !in_array($payments[$i]['purchase_entry_id'], $purchase_entry_credit_note)) { ?>
                                                <a class="payment-separate_payment btn btn-md blue"><i class="fa fa-pencil"></i> Separate Payment</a>
                                            <?php } ?>
                                            <?php if (!empty($payments[$i]['separate_father_id']) && $payments[$i]['status'] == 'pending' && !in_array($payments[$i]['separate_father_id'], $separate_father_ids_array)) { ?>
                                                <a class="btn btn-md green" href="<?php echo base_url() ?>payment/undo_separate/<?php echo $payments[$i]['separate_father_id'] ?>"><i class="fa fa-pencil"></i> Undo Separate</a>
                                            <?php } ?>
                                            <?php if ($payments[$i]['status'] == 'done') { ?>
                                                <a href="<?php echo base_url() ?>bank_reconciliation/index/<?php echo $payments[$i]['bank_reconciliation_id'] ?>" class="btn btn-md blue"><i class="fa fa-pencil"></i> View Bank Transaction</a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
                <nav class="pagination-wrap clearfix">
                    <div class="pull-left mt10">
                        <?php if ($pagination['total'] != 0) { ?>
                        <label class="form-control-static"><?php echo $pagination['total'] ?> Items in total, Display
                            <?php echo $pagination['offset'] ?> ~
                            <?php echo $pagination['limit'] ?></label>
                        <?php } ?>
                    </div>
                    <?php if (!empty($pagination) && $pagination['total'] > $pagination['items_per_page']) { ?>
                    <div class="pull-right">
                        <ul class="pagination no-margin">
                            <li <?php if ($pagination['page'] - 1 <= 0) { ?> class="disabled" <?php } ?>>

                            <a href="<?php if ($pagination['page'] - 1 <= 0){ ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] - 1 ?><?php } ?>">
                                Previous
                            </a>
                            </li>
                            <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                            <li>
                                <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=1"> 1 </a>
                            </li>
                            <?php } ?>

                            <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                            <li><a href="javascript:;">...</a></li>
                            <?php } ?>

                            <?php for ($p=($pagination['min_display']); $p <= $pagination['max_display']; $p++) { ?>
                            <?php if ($p > 0) { ?>
                            <li <?php if ($pagination['page'] == $p) { ?> class="active" <?php } ?>>
                            <a href="<?php if ($pagination['page'] != $p) { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $p ?><?php } else { ?>javascript:;<?php } ?>"> <?php echo $p ?> </a>
                            </li>
                            <?php } ?>
                            <?php } ?>

                            <?php if (($pagination['page'] + $pagination['adj']) < $pagination['total_page']) { ?>
                            <li><a href="javascript:;">...</a></li>
                            <?php } ?>

                            <?php if ($pagination['page'] + $pagination['adj'] < $pagination['total_page']) { ?>
                            <li>
                                <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['total_page'] ?>"> <?php echo $pagination['total_page'] ?> </a>
                            </li>
                            <?php } ?>

                            <li <?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> class="disabled" <?php } ?>>
                            <a href="<?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] + 1 ?><?php } ?>">
                                Next
                            </a>
                            </li>
                        </ul>
                    </div>
                    <?php } ?>
                </nav>
                <div class="text-center">
                    <a id="pay_invoices" class="btn btn-md blue"><i class="fa fa-pencil"></i> Pay Invoices</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="warning_select_invoices-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Select Invoice</h4>
            </div>
            <div class="modal-body">

                <p class="text-center">Please select invoice to pay</p>

                <?php echo form_open_multipart(base_url('payment/delete'), 'class="form-horizontal" method="post" data-parsley-validate'); ?>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_delete_id">
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<div class="modal" id="pay_invoices-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg" style="width: 95%;">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="GL_entry-modal-title" class="modal-title">Pay Invoices</h4>
            </div>
            <div class="modal-body">

                <div id="show_error_message_pay_invoices" class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span id="error_message_pay_invoices">Financial Period is updated successfully</span>
                </div>

                <?php echo form_open_multipart(base_url('GL_entry/edit'), 'class="form-horizontal" method="post" id="GL_entry-edit_form"'); ?>

                <div class="form-group">

                    <div style="padding:20px;">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr role="row" class="heading">
                                <th width="8%">Due Date</th>
                                <th width="8%">Pay To</th>
                                <th width="12%">Status</th>
                                <th width="8%">Amount</th>
                                <th width="12%">Reference No</th>
                            </tr>
                            </thead>
                            <tbody id="payment_pre_list">
                            </tbody>
                            <tbody>
                            <tr role="row" class="heading">
                                <td colspan="3" class="text-right">Total Amount</td>
                                <td id="pay_invoices_total_amount" style="border-right: 1px solid #ddd;"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <h4 style="padding: 15px;border-bottom: 1px solid #EFEFEF;" class="modal-title">Payment Mode</h4>

                    <div style="padding: 15px;">
                        <?php echo form_label('Pay From <span style="color:red;" class="required">*</span>', 'payment_method', array('class' => 'col-md-3 col-lg-2 control-label')); ?>

                        <div class="col-md-3 col-lg-4 form-group">
                            <input disabled style="width:60%; display:inline;" id="account_code" class="form-control" type="text">
                            <a style="width:20%; font-size: 25px;" id="account_code_button" class="btn btn-md gray">...</a>
                        </div>

                        <?php echo form_label('Pay Date <span style="color:red;" class="required">*</span>', 'pay_date', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                        <div class="col-md-3 col-lg-4 form-group" style="margin-bottom: 30px;">
                            <div class="input-group date date-picker" data-date-format="yyyy-mm-dd"
                                 data-date-viewmode="years">
                                <input id="pay_invoice_date" value="" name="starting_date" type="text" class="form-control">

                                <div class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </div>
                            </div>
                            <small id="required_pay_date" style="color:red; display:none;">Pay Date is required field</small>
                        </div>

                        <?php echo form_label('', 'payment_method', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                        <div class="col-md-3 col-lg-4 form-group">
                            <input disabled style="width:60%" id="account_name" class="form-control" type="text">
                            <small id="required_account_name" style="color:red; display:none;">Please select Account to pay</small>
                        </div>
                        <div class="hidden" id="bank_account_id">
                        </div>

                        <?php echo form_label('Reference No <span style="color:red;" class="required">*</span>', 'ref_no', array('class' => 'col-md-3 col-lg-2 control-label')); ?>

                        <div class="col-md-3 col-lg-4 form-group">
                            <input class="form-control" id="pay_invoice_ref_no" style="width:60%" type="text">
                            <small id="required_ref_no" style="color:red; display:none;">Reference No is required field</small>
                            <small id="dupplicated_ref_no" style="color:red; display:none;">Reference No is already existed</small>
                        </div>
                    </div>
                    
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_id">
                <button  type="button" class="btn btn-md blue" id="pay_invoices_submit">Submit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<div class="modal" id="select_account-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Select Account</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-3 col-lg-4 form-group">
                    <input type="text" name="keyword" class="form-control" value="<?php echo $search['keyword'] ?>" placeholder="Key Word">
                </div>
                <table style="margin-top: 20px;" class="tree table table-striped table-bordered table-hover">
                    <thead>
                    <tr role="row" class="heading">
                        <th width="25%">Account Name</th>
                        <th width="8%">Account Type</th>
                        <th width="8%">Account Code</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php for ($i = 0; $i < count($accounts); $i++) {
                        ?>
                        <tr role="row" class="choose_account">
                            <td class="modal_account_id hidden"><?php echo $accounts[$i]['id']; ?></td>
                            <td class="modal_account_name"><?php echo $accounts[$i]['name']; ?><?php echo !empty($accounts[$i]['number']) ? ' ('.$accounts[$i]['number'].')' : ''; ?></td>
                            <td><?php echo $accounts[$i]['type']; ?></td>
                            <td class="modal_account_code"><?php echo $accounts[$i]['account_code']; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="payment-separate-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Payment Separation</h4>
            </div>
            <?php echo form_open_multipart(base_url('payment/separate_payment'), 'class="form-horizontal" method="post" data-parsley-validate'); ?>
            <div class="modal-body">

                <div class="row">
                    <div class="col-sm-6">
                        <label>Supplier: </label>
                        <span id="payment_separate_supplier"></span>
                    </div>
                    <div class="col-sm-6">
                        <label>Reference No: </label>
                        <span id="payment_separate_ref_no"></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <label>Document Date: </label>
                        <span id="payment_separate_document_date"></span>
                    </div>
                    <div class="col-sm-6">
                        <label>Amount: </label>
                        <span id="payment_separate_amount"></span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <label>Payment Amount: </label>
                        <input id="payment_separate_payment_amount" name="payment_amount" data-parsley-required="true" data-parsley-required-message="Payment Amount is required field" data-parsley-larger="larger">
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_separate_id">
                <input type="hidden" name="payment_GL_transaction_id" id="separate_payment_GL_transaction_id">
                <button  type="submit" class="btn btn-md blue">Submit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {

        $('input:checkbox.payment_select').on('change', function () {

            var total_amount = 0;
            $('input:checkbox.payment_select').each(function () {
                if (this.checked) {
                    var pay_tr = $(this).closest('tr');
                    var amount = pay_tr.find('.total_amount').html().replace(/,/g, "");

                    total_amount += parseFloat(amount);

                }
            });
            $('#total_selected').html(numberWithCommas(total_amount.toFixed(2)));

            if (this.checked) {
                var checked_tr = $(this).closest('tr');
                checked_tr.css('background-color', 'yellow');

                var checked_supplier_id = checked_tr.find('.supplier_id').html();

                $('input:checkbox.payment_select:enabled').each(function () {

                    var unchecked_tr = $(this).closest('tr');

                    var unchecked_supplier_id = unchecked_tr.find('.supplier_id').html();

                    if (checked_supplier_id != unchecked_supplier_id) {
                        $(this).closest('.checker').addClass('disabled');
                        $(this).prop('disabled', true);
                    }

                });
            } else {
                var item_checked = false;
                $('input:checkbox.payment_select:enabled').each(function () {

                    if (this.checked) {
                        item_checked = true;
                    }

                });

                if (item_checked == false) {
                    $('input:checkbox.payment_select').each(function () {

                        var closest_tr = $(this).closest('tr');
                        closest_tr.css('background-color', 'white');

                        var status_html = closest_tr.find('.status').html();

                        if (status_html == '<span class="label label-warning">PENDING</span>') {
                            $(this).closest('.checker').removeClass('disabled');
                            $(this).prop('disabled', false);
                        }

                    });
                }
            }
        });

        $("#payment_separate_payment_amount").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        window.ParsleyValidator.addValidator('larger',
            function (value, requirement) {

                var amount = $('#payment_separate_amount').html().replace(/,/g, "");
                var error_message = '';

                var response = false;

                if (parseFloat(value) >= parseFloat(amount)) {
                    error_message = 'Payment Amount should be smaller than Total Amount';
                    response = true;
                }

                window.ParsleyValidator.addMessage('en', 'larger', error_message);
                return response == false;
            }, 32
        );

        $('.payment-separate_payment').on('click', function() {

            $('#payment_separate_payment_amount').val('');
            $('#payment_separate_payment_amount').parsley().reset();

            var el = $(this);

            var payment_GL_transaction_id = el.closest('tr').find('.GL_transaction_id').html();
            var purchase_entry_id = el.closest('tr').find('.purchase_entry_id').html();
            var document_date = el.closest('tr').find('.pay_date').html();
            var supplier_name = el.closest('tr').find('.supplier_name').html();
            var ref_no = el.closest('tr').find('.ref_no').html();
            var total_amount = el.closest('tr').find('.total_amount').html();

            $('#payment_separate_supplier').html(supplier_name);
            $('#payment_separate_ref_no').html(ref_no);
            $('#payment_separate_document_date').html(document_date);
            $('#payment_separate_amount').html(total_amount);
            $('#modal_separate_id').val(purchase_entry_id);
            $('#separate_payment_GL_transaction_id').val(payment_GL_transaction_id);



            $('#payment-separate-modal').modal({
                show: true,
                backdrop: 'static'
            });

        });

        $('#pay_invoices').on('click', function() {

            var html = '';
            $('#show_error_message_pay_invoices').hide();

            $('#account_code').val('<?php echo $default_account['account_code']; ?>');
            $('#account_name').val('<?php echo $default_account['name']; ?><?php echo !empty($default_account['number']) ? ' ('.$default_account['number'].')' : ''; ?>');
            $('#bank_account_id').html('<?php echo $default_account['id']; ?>');

            $('#pay_invoices_total_amount').html('');
            $('#pay_invoice_date').val('');
            $('#pay_invoice_ref_no').val('');

            $('#account_code_button').css('color', '#428bca');
            $('#account_code').css('border-color', '#e5e5e5');
            $('#account_name').css('border-color', '#e5e5e5');

            $('#required_account_name').hide();

            $('#pay_invoice_date').css('border-color', '#e5e5e5');

            $('#required_pay_date').hide();

            $('#pay_invoice_ref_no').css('border-color', '#e5e5e5');

            $('#required_ref_no').hide();

            $('#dupplicated_ref_no').hide();

            $('input:checkbox.payment_select').each(function () {
                if (this.checked) {
                    var pay_tr = $(this).closest('tr');

                    html += '<tr role="row" class="heading">';
                    html += '<td class="id hidden">'+pay_tr.find('.payment_id').html()+'</td>';
                    html += '<td class="GL_transaction_id hidden">'+pay_tr.find('.GL_transaction_id').html()+'</td>';
                    html += '<td class="purchase_entry_id hidden">'+pay_tr.find('.purchase_entry_id').html()+'</td>';
                    html += '<td>'+pay_tr.find('.pay_date').html()+'</td>';
                    html += '<td>'+pay_tr.find('.pay_to').html()+'</td>';
                    html += '<td>'+pay_tr.find('.status').html()+'</td>';
                    html += '<td class="invoice_amount">'+pay_tr.find('.total_amount').html()+'</td>';
                    html += '<td>'+pay_tr.find('.description').html()+'</td>';
                    html += '</tr>';
                }
            });

            if (html == '') {
                $('#warning_select_invoices-modal').modal({
                    show: true,
                    backdrop: 'static'
                });
            } else {

                $('#payment_pre_list').html(html);
                calculate_total_invoices();

                $('#pay_invoices-modal').modal({
                    show: true,
                    backdrop: 'static'
                });
            }
        });

        $('.date-picker').datepicker({
            format: "yyyy-mm-dd"
        });

        $("#account_code_button").on("click",function(){
            var el1 = $(this);
            $('#select_account-modal').modal({
                show: true,
                backdrop: 'static'
            });

            var modal_account_code = '';
            var modal_account_name = '';
            var modal_account_id = '';
            $(".choose_account").on("click",function(){
                var el = $(this);
                modal_account_code = el.closest('tr').find('.modal_account_code').html();
                modal_account_name = el.closest('tr').find('.modal_account_name').html();
                modal_account_id = el.closest('tr').find('.modal_account_id').html();
                $('#select_account-modal').modal('hide');

                $('#account_code').val(modal_account_code);
                $('#account_name').val(modal_account_name);
                $('#bank_account_id').html(modal_account_id);

            });
        });

        function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }

        function calculate_total_invoices() {
            var total_amount = 0;

            $('#payment_pre_list tr').each(function() {
                var amount = $(this).find('.invoice_amount').html();

                total_amount += parseFloat(amount.replace(/,/g, ""));
            });

            $("#pay_invoices_total_amount").html(numberWithCommas(total_amount.toFixed(2)));
        }

        $("#pay_invoices_submit").on("click",function(){

            $(this).prop("disabled", true);

            var account_code = $('#account_code').val();
            var bank_account_id = $('#bank_account_id').html();
            var amount = $('#pay_invoices_total_amount').html();
            amount = parseFloat(amount.replace(/,/g, ""));
            var pay_date = $('#pay_invoice_date').val();
            var ref_no = $('#pay_invoice_ref_no').val();

            var error_message = false;

            if (bank_account_id == '') {
                $('#account_code_button').css('color', 'red');
                $('#account_code').css('border-color', 'red');
                $('#account_name').css('border-color', 'red');

                $('#required_account_name').show();
                error_message = true;
            } else {
                $('#account_code_button').css('color', '#428bca');
                $('#account_code').css('border-color', '#e5e5e5');
                $('#account_name').css('border-color', '#e5e5e5');

                $('#required_account_name').hide();
            }

            if (pay_date == '') {
                $('#pay_invoice_date').css('border-color', 'red');
                $('#pay_invoice_date').focus();
                $('#required_pay_date').show();

                error_message = true;
            } else {
                $('#pay_invoice_date').css('border-color', '#e5e5e5');

                $('#required_pay_date').hide();
            }

            if (ref_no == '') {
                $('#pay_invoice_ref_no').css('border-color', 'red');
                $('#pay_invoice_ref_no').focus();
                $('#required_ref_no').show();
                error_message = true;
            } else {
                $('#pay_invoice_ref_no').css('border-color', '#e5e5e5');

                $('#required_ref_no').hide();
            }

            $.ajax({
                async:false,
                method: 'POST',
                url: "<?php echo base_url(); ?>payment/check_ref",
                data: {
                    bank_account_id: bank_account_id,
                    ref_no: ref_no
                },
                dataType: 'json'
            }).done(function(res) {

                if (res.error == true) {
                    $('#pay_invoice_ref_no').css('border-color', 'red');
                    $('#dupplicated_ref_no').show();
                    error_message = true;
                } else {
                    $('#pay_invoice_ref_no').css('border-color', '#e5e5e5');

                    $('#dupplicated_ref_no').hide();
                }
            });

            if (error_message == true) {
                $(this).prop("disabled", false);
                return;
            }

            var payments = [];
            $('#payment_pre_list').find('tr').each(function() {
                var payment = {};
                var el1 = $(this);
                payment.payment_id = el1.find('.id').html();
                payment.GL_transaction_id = el1.find('.GL_transaction_id').html();
                payment.purchase_entry_id = el1.find('.purchase_entry_id').html();
                payment.amount = parseFloat(el1.find('.invoice_amount').html().replace(/,/g, ""));

                payments.push(payment);
            });

            $.ajax({
                method: 'POST',
                url: "<?php echo base_url(); ?>payment/pay_invoices",
                data: {
                    account_code: account_code,
                    bank_account_id: bank_account_id,
                    amount: amount,
                    pay_date: pay_date,
                    ref_no: ref_no,
                    payments: payments
                },
                dataType: 'json'
            }).done(function() {
                if (res.success == true) {
                    window.location.reload();
                }
            });
        });
    });
</script>
