<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Chart of Account
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Chart of Account</a>
        </li>
</div>
<!-- END PAGE HEADER -->
<!-- BEGIN ALERT BOX -->
<?php if (isset($flash_message)) { ?>
    <div class="row margin-bottom-20">
        <div class="col-xs-12">
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END ALERT BOX -->

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Chart of Account</span>
                </div>
                <a href="#" id="change_sales" class="btn btn-md blue pull-right"><i class="fa fa-plus"></i> Change Sales Account</a>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-12">

                        <!-- Begin: life time stats -->
                        <div class="table-container">
                            <!-- Begin: life time stats -->
                            <div class="clearfix mt15">

                                <form method="get" class="form" role="form">
                                    <div class="row">
                                        <div class="form-group">
                                            <?php echo form_label('Keyword', 'description', array('class' => 'col-md-3 col-lg-2 control-label')); ?>

                                            <div class="col-md-3 col-lg-4 form-group">
                                                <input type="text" name="keyword" class="form-control" value="<?php echo $search['keyword'] ?>" placeholder="Key Word">
                                            </div>

                                            <?php echo form_label('Type', 'description', array('class' => 'col-md-2 col-lg-1 control-label')); ?>
                                            <div class="col-md-3 col-lg-4 form-group">
                                                <?php echo form_dropdown('type', [
                                                    '' => 'Please Select',
                                                    'asset' => 'Asset',
                                                    'liability' => 'Liability',
                                                    'equity' => 'Equity',
                                                    'income' => 'Income',
                                                    'cost_of_sales' => 'Cost of Sales',
                                                    'expenses' => 'Expenses'
                                                ], set_value('type', $search['type']), 'class="form-control"'); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 text-right">
                                            <a href="<?php echo base_url('chart_of_account') ?>" class=" btn btn-md grey-salsa">Reset</a>
                                            <button type="submit" class="btn btn-md green"><i class="icon-magnifier font-white"></i>Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <table style="margin-top: 20px;" class="tree table table-striped table-bordered table-hover">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="25%">Account Name</th>
                                    <th width="8%">Account Type</th>
                                    <th width="8%">Account Code</th>
                                    <th width="8%">Status</th>
                                    <th width="5%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php for ($i = 0; $i < count($accounts); $i++) {
                                        ?>
                                    <tr role="row" class="treegrid-<?php echo $accounts[$i]['id'] ?> <?php if (!empty($accounts[$i]['father_id'])) {echo 'treegrid-parent-'.$accounts[$i]['father_id'];} ?> heading">
                                        <td><?php echo $accounts[$i]['name']; ?></td>
                                        <td class="account_name hidden"><?php echo $accounts[$i]['name']; ?></td>
                                        <td class="account_id hidden"><?php echo $accounts[$i]['id']; ?></td>
                                        <td><?php echo $accounts[$i]['type']; ?></td>
                                        <td class="account_code"><?php echo $accounts[$i]['code']; ?></td>
                                        <td class="status"><?php echo $accounts[$i]['status']; ?></td>
                                        <td>
                                            <?php if ($accounts[$i]['code'] >= 50000 || ($accounts[$i]['code'] < 40000 && $accounts[$i]['code'] >= 14100) || $accounts[$i]['code'] <= 12000) { ?>
                                            <a href="#" class="btn btn-md blue master_account_change"><i class="fa fa-pencil"></i> Change Name</a>
                                            <?php  } ?>

                                            <?php if (empty($accounts[$i]['MC_id']) && !($accounts[$i]['code'] >= 14000 && $accounts[$i]['code'] < 14100) && $accounts[$i]['code'] != 40010 && $accounts[$i]['code'] != 40020 && $accounts[$i]['code'] != 40030 && $accounts[$i]['code'] != 40098
                                                                    && $accounts[$i]['code'] != 40110 && $accounts[$i]['code'] != 40210 && $accounts[$i]['code'] != 40220 && $accounts[$i]['code'] != 40510 && $accounts[$i]['code'] != 21401) { ?>
                                                <?php if ($accounts[$i]['status'] == 'inactive') { ?>
                                                <a href="#" class="btn btn-md yellow activate"><i class="fa fa-pencil"></i> Activate</a>
                                                <?php } else { ?>
                                                <a href="#" class="btn btn-md grey-salsa deactivate"><i class="fa fa-pencil"></i> Deactivate</a>
                                            <?php  }} ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="account-edit-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="account-modal-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">

                <?php echo form_open_multipart(base_url('chart_of_account/change_name'), 'class="form-horizontal" method="post" data-parsley-validate id="account-edit_form"'); ?>
                <div class="form-body">

                    <div class="form-group">
                        <?php echo form_label('Account Code <span style="color:red;" class="required">*</span>', 'account_code', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input('account_code', set_value('account_code', ''), 'class="form-control login_id" id="modal_account_code" data-parsley-required="true" data-parsley-required-message="Account Code is required field"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Account Name <span style="color:red;" class="required">*</span>', 'account_name', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input('account_name', set_value('account_name', ''), 'class="form-control" id="modal_account_name" data-parsley-required="true" data-parsley-required-message="Account Name is required field"'); ?>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="account_id" id="modal_account">
                <button  type="button" class="btn btn-md blue" id="account_submit">Submit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<div class="modal" id="sales-edit-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="sales-modal-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">

                <?php echo form_open_multipart(base_url('chart_of_account/change_sales'), 'class="form-horizontal" method="post" data-parsley-validate id="sales-edit_form"'); ?>
                <div class="form-body">

                    <?php $i=0; foreach ($sales_names AS $key => $sale) { $i++; ?>
                    <div class="form-group">
                        <?php echo form_label($key.' <span style="color:red;" class="required">*</span>', 'sales_'.$i, array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input($key, set_value('sales_'.$i, $sale), 'class="form-control" id="modal_sales_'.$i.'" data-parsley-required="true" data-parsley-required-message="Sales Name is required field"'); ?>
                        </div>
                    </div>
                    <?php } ?>
                </div>

            </div>
            <div class="modal-footer">
                <button  type="button" class="btn btn-md blue" id="sales_submit">Submit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {

        $("#account_submit").on("click",function(){

            $('#show_error_message').hide();
            $("#account-edit_form").submit();
        });

        $(".master_account_change").on("click", function () {

            $("#modal_account_code").prop("disabled", true);

            $("#modal_account_name").parsley().reset();

            var el = $(this);
            var account_name = el.closest('tr').find('.account_name').html();
            var account_code = el.closest('tr').find('.account_code').html();
            var account_id = el.closest('tr').find('.account_id').html();

            $('#show_error_message').hide();

            $('#modal_account_name').val(account_name);
            $('#modal_account_code').val(account_code);
            $('#modal_account').val(account_id);

            $('#account-modal-title').html('Change Account Name');

            $('#account-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $("#sales_submit").on("click",function(){

            $('#show_error_message').hide();
            $("#sales-edit_form").submit();
        });

        $("#change_sales").on("click", function () {

            var el = $(this);

            $('#show_error_message').hide();

            $('#sales-modal-title').html('Change Sales Name');

            $('#sales-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $(".activate").on("click", function () {

            var el = $(this);

            var id = el.closest('tr').find('.account_id').html();

            $.ajax({
                method: 'POST',
                url: "<?php echo base_url(); ?>chart_of_account/change_status",
                data: {
                    id: id,
                    status: 'active'
                },
                dataType: 'json'
            }).done(function(res) {
                if (res.success == true) {
                    window.location.reload();
                }
            });
        });

        $(".deactivate").on("click", function () {

            var el = $(this);

            var id = el.closest('tr').find('.account_id').html();

            $.ajax({
                method: 'POST',
                url: "<?php echo base_url(); ?>chart_of_account/change_status",
                data: {
                    id: id,
                    status: 'inactive'
                },
                dataType: 'json'
            }).done(function(res) {
                if (res.success == true) {
                    window.location.reload();
                }
            });
        });
    });
</script>

