<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Supplier
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Supplier</a>
        </li>
</div>
<!-- END PAGE HEADER -->
<!-- BEGIN ALERT BOX -->
<?php if (isset($flash_message)) { ?>
    <div class="row margin-bottom-20">
        <div class="col-xs-12">
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END ALERT BOX -->

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Supplier</span>
                </div>
                <a href="#" class="supplier-add btn btn-md blue pull-right"><i class="fa fa-plus"></i> Add New</a>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-12">

                        <!-- Begin: life time stats -->
                        <div class="table-container">
                            <!-- Begin: life time stats -->
                            <div class="clearfix mt15">

                                <form method="get" class="form" role="form">
                                    <div class="row">
                                        <div class="col-md-3 col-xs-12">
                                            <label>Keyword</label>
                                            <div class="form-group">
                                                <input type="text" name="keyword" class="form-control" value="<?php echo $search['keyword'] ?>" placeholder="Key Word">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 text-right">
                                            <a href="<?php echo base_url('supplier') ?>" class=" btn btn-md grey-salsa">Reset</a>
                                            <button type="submit" class="btn btn-md green"><i class="icon-magnifier font-white"></i>Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <table style="margin-top: 20px;" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="4%" class="text-right">No</th>
                                    <th width="8%">Full Name</th>
                                    <th width="8%">Address</th>
                                    <th width="8%">Email</th>
                                    <th width="8%">Mobile</th>
                                    <th width="25%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php for ($i = 0; $i < count($suppliers); $i++) {
                                        ?>
                                    <tr role="row" class="heading">
                                        <td class="text-right"><?php echo $pagination['offset']+$i ?></td>
                                        <td class="supplier_id hidden"><?php echo $suppliers[$i]['id'] ?></td>
                                        <td class="name"><?php echo $suppliers[$i]['name'] ?></td>
                                        <td class="address"><?php echo $suppliers[$i]['address']; ?></td>
                                        <td class="email"><?php echo $suppliers[$i]['email']; ?></td>
                                        <td class="mobile"><?php echo $suppliers[$i]['mobile']; ?></td>
                                        <td class="default_account_code hidden"><?php echo $suppliers[$i]['default_account_code']; ?></td>
                                        <td class="default_tax_code hidden"><?php echo $suppliers[$i]['default_tax_code']; ?></td>
                                        <td class="default_due_date_number hidden"><?php echo $suppliers[$i]['default_due_date_number']; ?></td>
                                        <td>
                                            <a class="supplier-edit btn btn-md blue"><i class="fa fa-pencil"></i> Edit</a>
                                            <a href="#" class="btn btn-md red supplier-delete"><i class="fa fa-trash-o"></i> Delete</a>
                                            <a href="<?php echo base_url(); ?>purchase_entry?supplier_id=1" class="btn btn-md green"><i class="fa fa-list"></i>Purchases</a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
                <nav class="pagination-wrap clearfix">
                    <div class="pull-left mt10">
                        <?php if ($pagination['total'] != 0) { ?>
                        <label class="form-control-static"><?php echo $pagination['total'] ?> Items in total, Display
                            <?php echo $pagination['offset'] ?> ~
                            <?php echo $pagination['limit'] ?></label>
                        <?php } ?>
                    </div>
                    <?php if (!empty($pagination) && $pagination['total'] > $pagination['items_per_page']) { ?>
                    <div class="pull-right">
                        <ul class="pagination no-margin">
                            <li <?php if ($pagination['page'] - 1 <= 0) { ?> class="disabled" <?php } ?>>

                            <a href="<?php if ($pagination['page'] - 1 <= 0){ ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] - 1 ?><?php } ?>">
                                Previous
                            </a>
                            </li>
                            <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                            <li>
                                <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=1"> 1 </a>
                            </li>
                            <?php } ?>

                            <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                            <li><a href="javascript:;">...</a></li>
                            <?php } ?>

                            <?php for ($p=($pagination['min_display']); $p <= $pagination['max_display']; $p++) { ?>
                            <?php if ($p > 0) { ?>
                            <li <?php if ($pagination['page'] == $p) { ?> class="active" <?php } ?>>
                            <a href="<?php if ($pagination['page'] != $p) { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $p ?><?php } else { ?>javascript:;<?php } ?>"> <?php echo $p ?> </a>
                            </li>
                            <?php } ?>
                            <?php } ?>

                            <?php if (($pagination['page'] + $pagination['adj']) < $pagination['total_page']) { ?>
                            <li><a href="javascript:;">...</a></li>
                            <?php } ?>

                            <?php if ($pagination['page'] + $pagination['adj'] < $pagination['total_page']) { ?>
                            <li>
                                <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['total_page'] ?>"> <?php echo $pagination['total_page'] ?> </a>
                            </li>
                            <?php } ?>

                            <li <?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> class="disabled" <?php } ?>>
                            <a href="<?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] + 1 ?><?php } ?>">
                                Next
                            </a>
                            </li>
                        </ul>
                    </div>
                    <?php } ?>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="supplier-edit-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="supplier-modal-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">

                <div id="show_error_message" class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span id="error_message">Financial Period is updated successfully</span>
                </div>

                <?php echo form_open_multipart(base_url('supplier/edit'), 'class="form-horizontal" method="post" data-parsley-validate id="supplier-edit_form"'); ?>
                <div class="form-body">
                    <div class="form-group">
                        <?php echo form_label('Full Name <span style="color:red;" class="required">*</span>', 'name', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input('name', set_value('name', ''), 'class="form-control" id="modal_name" data-parsley-required="true" data-parsley-required-message="Full Name is required field"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Address', 'address', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input('address', set_value('address', ''), 'class="form-control login_id" id="modal_address"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Email', 'email', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input('email', set_value('email', ''), 'class="form-control" id="modal_email"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Mobile', 'mobile', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input('mobile', set_value('mobile', ''), 'class="form-control" id="modal_mobile"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Default Account Code', 'default_account_code', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input('default_account_code', set_value('default_account_code', ''), 'class="form-control" id="modal_default_account_code" data-parsley-account="account" data-toggle="tooltip" data-original-title="This account code will appear as default for this supplier when making transactions in Purchase Entry"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Default Tax Code', 'default_tax_code', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_dropdown('default_tax_code', ['' => 'Please Select', 'TX7' => 'TX7', 'NR' => 'NR', 'NT' => 'NT', 'ZP' => 'ZP', 'IM' => 'IM', 'IM7' => 'IM7', 'EP' => 'EP', 'OP' => 'OP'], set_select('default_tax_code'), 'class="form-control" id="modal_default_tax_code" data-toggle="tooltip" data-original-title="This tax code will appear as default for this supplier when making transactions in Purchase Entry"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Default Due Date Period (days)', 'default_due_date_number', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input('default_due_date_number', set_value('default_due_date_number', ''), 'class="form-control" id="modal_default_due_date_number" style="width: 40%; display: inline;" data-toggle="tooltip" data-original-title="This is default period for due date in Purchase Entry"'); ?>
                        </div>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_id">
                <button  type="button" class="btn btn-md blue" id="supplier_submit">Submit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<div class="modal" id="supplier-delete-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Delete Supplier</h4>
            </div>
            <div class="modal-body">

                <p class="text-center">Can you confirm to delete supplier?</p>

                <?php echo form_open_multipart(base_url('supplier/delete'), 'class="form-horizontal" method="post" data-parsley-validate'); ?>


            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_delete_id">
                <button  type="submit" class="btn btn-md red">Delete</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {

        $("#modal_default_due_date_number").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $('[data-toggle="tooltip"]').tooltip();

        window.ParsleyValidator.addValidator('account',
            function (value, requirement) {
                var modal_default_account_code = $('#modal_default_account_code').val();
                var account_codes = [];
                account_codes.push(modal_default_account_code);
                var error_message = '';

                var response = false;
                $.ajax({
                    async:false,
                    method: 'POST',
                    url: "<?php echo base_url(); ?>purchase_entry/check_edit",
                    data: {
                        account_codes: account_codes
                    },
                    dataType: 'json'
                }).done(function(res) {

                    response = res.error;
                    error_message = res.message;
                });

                window.ParsleyValidator.addMessage('en', 'account', error_message);
                return response == false;
            }, 32
        );

        var list_accounts = <?php echo $accounts_json; ?>;
        $( "#modal_default_account_code" ).autocomplete({
            source: list_accounts
        });

        $('.ui-autocomplete').css('z-index', '999999');

        $("#supplier_submit").on("click",function(){

            $('#show_error_message').hide();
            $("#supplier-edit_form").submit();
            return;

        });

        $(".supplier-edit").on("click",function(){

            $("#modal_name").parsley().reset();
            $("#modal_email").parsley().reset();

            var el = $(this);
            var name = el.closest('tr').find('.name').html();
            var address = el.closest('tr').find('.address').html();
            var email = el.closest('tr').find('.email').html();
            var mobile = el.closest('tr').find('.mobile').html();
            var default_tax_code = el.closest('tr').find('.default_tax_code').html();
            var default_account_code = el.closest('tr').find('.default_account_code').html();
            var default_due_date_number = el.closest('tr').find('.default_due_date_number').html();
            var id = el.closest('tr').find('.supplier_id').html();

            $('#show_error_message').hide();

            $('#modal_name').val(name);
            $('#modal_address').val(address);
            $('#modal_email').val(email);
            $('#modal_mobile').val(mobile);
            $('#modal_default_tax_code').val(default_tax_code);
            $('#modal_default_account_code').val(default_account_code);
            $('#modal_default_due_date_number').val(default_due_date_number);
            $('#modal_id').val(id);

            $('#supplier-modal-title').html('Edit Supplier');

            $('#supplier-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $(".supplier-delete").on("click",function(){
            var el = $(this);
            var supplier_id = el.closest('tr').find('.supplier_id').html();
            $('#modal_delete_id').val(supplier_id);

            $('#supplier-delete-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $(".supplier-add").on("click",function(){

            $("#modal_name").parsley().reset();
            $("#modal_email").parsley().reset();

            $('#modal_name').val('');
            $('#modal_address').val('');
            $('#modal_email').val('');
            $('#modal_mobile').val('');
            $('#modal_default_tax_code').val('');
            $('#modal_default_account_code').val('');
            $('#show_error_message').hide();
            $('#modal_id').val('');

            $('#supplier-modal-title').html('Add Supplier');

            $('#supplier-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });
    });
</script>
