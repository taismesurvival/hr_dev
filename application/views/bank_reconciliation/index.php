<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Bank Reconciliation
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo base_url('company') ?>">Company Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Bank Reconciliation</a>
        </li>
</div>
<!-- END PAGE HEADER -->
<!-- BEGIN ALERT BOX -->
<?php if (isset($flash_message)) { ?>
    <div class="row margin-bottom-20">
        <div class="col-xs-12">
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END ALERT BOX -->

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Bank Reconciliation</span>
                </div>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-12">

                        <!-- Begin: life time stats -->
                        <div class="table-container">
                            <!-- Begin: life time stats -->
                            <div class="clearfix mt15">

                                <form method="get" class="form" role="form">
                                    <div class="row">
                                        <div class="form-group">

                                            <?php echo form_label('Account Type', 'account_type', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                            <div class="col-md-3 col-lg-4 form-group">
                                                <?php echo form_dropdown('account_type', ['' => 'Please Select', 'bank' => 'Bank Account', 'cash' => 'Petty Cash'], set_value('account_type', $search['account_type']), 'class="form-control"'); ?>
                                            </div>

                                            <?php echo form_label('Keyword', 'key_word', array('class' => 'col-md-2 col-lg-1 control-label')); ?>
                                            <div class="col-md-3 col-lg-4 form-group">
                                                <input type="text" name="key_word" class="form-control" value="<?php echo $search['key_word'] ?>" placeholder="Refference No or Account Name">
                                            </div>

                                            <?php echo form_label('Account', 'bank_account', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                            <div class="col-md-3 col-lg-4 form-group">
                                                <?php echo form_dropdown('bank_account', $bank_accounts, set_value('bank_account', $search['bank_account']), 'class="form-control"'); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="row">
                                                <?php echo form_label('Total Amount', 'description', array('class' => 'col-md-3 col-lg-2 control-label')); ?>

                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <input type="text" name="minimum_amount" class="form-control" value="<?php echo $search['minimum_amount'] ?>" placeholder="Minimum Amount">
                                                </div>

                                                <?php echo form_label('~', 'description', array('class' => 'col-md-2 col-lg-1 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <input type="text" name="maximum_amount" class="form-control" value="<?php echo $search['maximum_amount'] ?>" placeholder="Maximum Amount">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-xs-12">
                                            <div class="row">
                                                <?php echo form_label('Document Date', 'keyword', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <input type="text" name="document_date_from" class="form-control date-picker" value="<?php echo $search['document_date_from'] ?>" placeholder="Starting Date">
                                                </div>

                                                <?php echo form_label('~', 'description', array('class' => 'col-md-2 col-lg-1 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <input type="text" name="document_date_to" class="form-control date-picker" value="<?php echo $search['document_date_to'] ?>" placeholder="Ending Date">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 text-right">
                                            <a href="<?php echo base_url('bank_reconciliation') ?>" class=" btn btn-md grey-salsa">Reset</a>
                                            <button type="submit" class="btn btn-md green"><i class="icon-magnifier font-white"></i>Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <table style="margin-top: 20px;" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="4%" class="text-right">No</th>
                                    <th width="8%">Account</th>
                                    <th width="8%">Type</th>
                                    <th width="8%">Amount</th>
                                    <th width="8%">Reference No</th>
                                    <th width="8%">Pay Date</th>
                                    <th width="8%">Status</th>
                                    <th width="12%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php for ($i = 0; $i < count($bank_reconciliations); $i++) {
                                        ?>
                                    <tr role="row" class="heading">
                                        <td class="text-right"><?php echo $pagination['offset']+$i ?></td>
                                        <td class="id hidden"><?php echo $bank_reconciliations[$i]['id'] ?></td>
                                        <td class="account_name"><?php echo $bank_reconciliations[$i]['name']; ?><?php echo !empty($bank_reconciliations[$i]['number']) ? ' ('.$bank_reconciliations[$i]['number'].')' : '' ?></td>
                                        <td><?php echo $bank_reconciliations[$i]['bank_recons_type'] == 'AR' ? 'Deposit' : 'Withdrawal'; ?></td>
                                        <td class="account_type hidden"><?php echo $bank_reconciliations[$i]['bank_recons_type']; ?></td>
                                        <td class="amount"><?php echo number_format($bank_reconciliations[$i]['amount'],2); ?></td>
                                        <td class="ref_no"><?php echo $bank_reconciliations[$i]['ref_no']; ?></td>
                                        <td class="pay_date"><?php echo $bank_reconciliations[$i]['pay_date']; ?></td>
                                        <td>
                                            <?php echo !empty($bank_reconciliations[$i]['clear_date']) ? '<span class="label label-success">CLEAR</span><br/>' : '<span class="label label-warning">UNCLEAR</span>'; ?>
                                            <?php echo $bank_reconciliations[$i]['clear_date']; ?>
                                        </td>
                                        <td class="clear_date hidden"><?php echo $bank_reconciliations[$i]['clear_date']; ?></td>
                                        <td>
                                            <!--<a class="bank_reconciliation-edit btn btn-md blue"><i class="fa fa-pencil"></i> Edit</a>-->
                                            <?php if (empty($bank_reconciliations[$i]['clear_date']) && !empty($bank_reconciliations[$i]['payment_recons_GL_transaction_id'])
                                                && !empty($bank_reconciliations[$i]['payment_purchase_GL_transaction_id']) && date('Y-m', strtotime($bank_reconciliations[$i]['pay_date'])) > $locked_financial_month
                                                && !empty($bank_reconciliations[$i]['purchase_entry_id'])) { ?>
                                                <a href="<?php echo base_url(); ?>bank_reconciliation/undo_payment/<?php echo $bank_reconciliations[$i]['id'] ?>" class="btn btn-md blue"><i class="fa fa-pencil"></i> Undo Payment</a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
                <nav class="pagination-wrap clearfix">
                    <div class="pull-left mt10">
                        <?php if ($pagination['total'] != 0) { ?>
                        <label class="form-control-static"><?php echo $pagination['total'] ?> Items in total, Display
                            <?php echo $pagination['offset'] ?> ~
                            <?php echo $pagination['limit'] ?></label>
                        <?php } ?>
                    </div>
                    <?php if (!empty($pagination) && $pagination['total'] > $pagination['items_per_page']) { ?>
                    <div class="pull-right">
                        <ul class="pagination no-margin">
                            <li <?php if ($pagination['page'] - 1 <= 0) { ?> class="disabled" <?php } ?>>

                            <a href="<?php if ($pagination['page'] - 1 <= 0){ ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] - 1 ?><?php } ?>">
                                Previous
                            </a>
                            </li>
                            <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                            <li>
                                <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=1"> 1 </a>
                            </li>
                            <?php } ?>

                            <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                            <li><a href="javascript:;">...</a></li>
                            <?php } ?>

                            <?php for ($p=($pagination['min_display']); $p <= $pagination['max_display']; $p++) { ?>
                            <?php if ($p > 0) { ?>
                            <li <?php if ($pagination['page'] == $p) { ?> class="active" <?php } ?>>
                            <a href="<?php if ($pagination['page'] != $p) { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $p ?><?php } else { ?>javascript:;<?php } ?>"> <?php echo $p ?> </a>
                            </li>
                            <?php } ?>
                            <?php } ?>

                            <?php if (($pagination['page'] + $pagination['adj']) < $pagination['total_page']) { ?>
                            <li><a href="javascript:;">...</a></li>
                            <?php } ?>

                            <?php if ($pagination['page'] + $pagination['adj'] < $pagination['total_page']) { ?>
                            <li>
                                <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['total_page'] ?>"> <?php echo $pagination['total_page'] ?> </a>
                            </li>
                            <?php } ?>

                            <li <?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> class="disabled" <?php } ?>>
                            <a href="<?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] + 1 ?><?php } ?>">
                                Next
                            </a>
                            </li>
                        </ul>
                    </div>
                    <?php } ?>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="bank_reconciliation-edit-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="bank_reconciliation-modal-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">

                <div id="show_error_message" class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span id="error_message">Financial Period is updated successfully</span>
                </div>

                <?php echo form_open_multipart(base_url('bank_reconciliation/edit'), 'class="form-horizontal" method="post" data-parsley-validate="" id="bank_reconciliation-edit_form"'); ?>
                <div class="form-body">
                    <div class="form-group">
                        <?php echo form_label('Account Name', 'account_name', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input('account_name', set_value('account_name', ''), 'disabled class="form-control" id="modal_account_name" data-parsley-required="true"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Type', 'account_type', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_dropdown('account_type', ['' => 'Please Select', 'AR' => 'Deposit', 'AP' => 'Withdrawal'], set_select('type'), 'disabled class="form-control" id="modal_account_type"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Amount', 'amount', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input('amount', set_value('amount', ''), 'disabled class="form-control number" id="modal_amount" data-parsley-required="true"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Cheque No <span style="color:red;" class="required">*</span>', 'ref_no', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input('ref_no', set_value('ref_no', ''), 'class="form-control" id="modal_ref_no" data-parsley-required="true" data-parsley-required-message="Cheque No is required field"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Pay Date <span style="color:red;" class="required">*</span>', 'pay_date', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <div class="input-group date date-picker" data-date-format="yyyy-mm-dd"
                                 data-date-viewmode="years">
                                <input id="modal_pay_date" value="" name="pay_date" type="text" class="form-control" data-parsley-required="true" data-parsley-required-message="Pay Date is required field">

                                <div class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Clear Date <span style="color:red;" class="required">*</span>', 'clear_date', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <div class="input-group date date-picker" data-date-format="yyyy-mm-dd"
                                 data-date-viewmode="years">
                                <input id="modal_clear_date" value="" name="clear_date" type="text" class="form-control" data-parsley-required="true" data-parsley-larger="larger" data-parsley-required-message="Clear Date is required field">

                                <div class="input-group-btn">
                                    <button class="btn default" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_id">
                <button  type="button" class="btn btn-md blue" id="bank_reconciliation_submit">Submit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script>

    jQuery(document).ready(function () {

        window.ParsleyValidator.addValidator('larger',
            function (value, requirement) {
                var modal_pay_date = $('#modal_pay_date').val();
                return modal_pay_date <= value;
            }, 32)
            .addMessage('en', 'larger', 'Clear Date should be larger than Pay Date');

        $('.date-picker').datepicker({
            format: "yyyy-mm-dd"
        });

        $("#bank_reconciliation_submit").on("click",function(){

            var modal_ref_no = $('#modal_ref_no').val();
            var modal_pay_date = $('#modal_pay_date').val();
            var modal_clear_date = $('#modal_clear_date').val();

            $('#show_error_message').hide();
            $("#bank_reconciliation-edit_form").submit();
            return;
        });

        $(".bank_reconciliation-edit").on("click",function(){

            $("input[name=pay_date]").parsley().reset();
            $("input[name=ref_no]").parsley().reset();
            $("input[name=clear_date]").parsley().reset();

            var el = $(this);
            var account_name = el.closest('tr').find('.account_name').html();
            var account_type = el.closest('tr').find('.account_type').html() == 'AR' ? 'AR' : 'AP';
            var amount = el.closest('tr').find('.amount').html();
            var ref_no = el.closest('tr').find('.ref_no').html();
            var pay_date = el.closest('tr').find('.pay_date').html();
            var clear_date = el.closest('tr').find('.clear_date').html();

            var id = el.closest('tr').find('.id').html();

            $('#show_error_message').hide();

            $('#modal_account_name').val(account_name);
            $('#modal_account_type').val(account_type);
            $('#modal_amount').val(amount);
            $('#modal_ref_no').val(ref_no);
            $('#modal_pay_date').val(pay_date);
            $('#modal_clear_date').val(clear_date);
            $('#modal_id').val(id);

            $('#bank_reconciliation-modal-title').html('Edit Bank Reconciliation');

            $('#bank_reconciliation-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });
    });
</script>
