<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    <?php echo !empty($title) ? $title : 'Add Group Permission'; ?>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo base_url('company') ?>">Company Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo base_url('group_permission') ?>">Group Permission</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#"><?php echo !empty($title) ? $title : 'Add Group Permission'; ?></a>
        </li>
</div>
<!-- END PAGE HEADER -->
<!-- BEGIN ALERT BOX -->
<?php if (isset($error_message)) { ?>
    <div class="row margin-bottom-20">
        <div class="col-xs-12">
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <?php echo $error_message; ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END ALERT BOX -->

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase"><?php echo !empty($title) ? $title : 'Add Group Permission'; ?></span>
                </div>
            </div>
            <div class="portlet-body">

                <?php echo form_open_multipart(base_url('group_permission/add/'.$group_detail['id']), 'class="form-horizontal" method="post" data-parsley-validate'); ?>
                <div class="form-body">
                    <div class="form-group">
                        <?php echo form_label('Group Name <span style="color:red;" class="required">*</span>', 'name', array('class' => 'col-md-4 col-lg-3 control-label')); ?>

                        <div class="col-md-8 col-lg-9 form-group">
                            <?php echo form_input('name', set_value('name', $group_detail['name']), 'class="form-control" data-parsley-required="true" data-parsley-required-message="Group Name is required field"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Description', 'description', array('class' => 'col-md-4 col-lg-3 control-label')); ?>

                        <div class="col-md-8 col-lg-9 form-group">
                            <?php echo form_textarea('description', set_value('description', $group_detail['description']), 'class="form-control"'); ?>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" value="<?php echo $group_detail['id'] ?>">
                <button  type="submit" class="btn btn-md blue">Submit</button>
                <a  href="<?php echo base_url('group_permission') ?>" class="btn btn-md grey-salsa" data-dismiss="modal">Back</a>
            </div>
            <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
