<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    <?= $title ?>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo base_url("trial_balance_report/view"); ?>">Trial Balance Report</a>
            <i class="fa fa-angle-right"></i>
        </li>
</div>
<!-- END PAGE HEADER-->

<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-money"></i> Trial Balance Report
        </div>

    </div>
    <div class="portlet-body">
        <form role="form" method='post' class='form-horizontal' id="filter_form" data-parsley-validate target="_blank">
            <div class="row form-group">
                <div class="col-md-12">
                    <table class="dataTable table table-bordered table-hover">
                        <tbody>
                            <tr>
                                <td>
                                    Month <span style="color:red;" class="required">*</span>
                                </td>
                                <td>
                                    <div class="input-group date month-picker" data-date-format="yyyy-mm"
                                         data-date-viewmode="years">
                                        <input value="" name="month" type="text" class="form-control" data-parsley-required="true" data-parsley-required-message="Month of Period is required field">

                                        <div class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>

            </div>


            <div class='row form-group'>
                <div class="col-md-12">
                    <input class="btn btn-primary form-control" type='submit' value="Submit">
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    jQuery(document).ready(function () {

        $('.month-picker').datepicker({
            format: "yyyy-mm",
            viewMode: "months",
            minViewMode: "months"
        });

        // run pre selected options
        $('#pre-selected-options').multiSelect();
    });
</script>