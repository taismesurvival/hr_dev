<style>
    table { border-collapse: collapse; }
    tr.border-top { border-top: solid thin; }
    tr.border-bottom { border-bottom: solid thin; }
    tr.bold { font-weight: bold; }
    * { font-size: 14px; font-family: Lucida Sans Unicode; }
</style>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/slider-pips/jquery-ui.js"></script>
<button type="button" id="download_excel_file">Download Excel Report</button>
<button type="button" id="download_pdf_file">Print or Download PDF Report</button>
<div id="table_wrapper">
    <table style='width:100%;'>

        <tr>
            <td style='text-align:left;'>
                <table style='color:blue;width:100%;'>
                    <tr>
                        <td style='font-size:20px;text-align:center;'><?php echo $company_name; ?></td>
                    </tr>
                    <tr>
                        <td style='font-size:16px;text-align:center;'><?php echo $ending_month; ?></td>
                    </tr>
                    <tr>
                        <td style='font-size:16px;text-align:center;'><?php echo $bank_account['name'].' ('.$bank_account['number'].')'; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style='text-align:center;font-size:18px;'>Bank Reconciliation Report</td>
        </tr>
    </table>
    <table style='width:100%;'>
        <thead>
        <tr>
            <th>Reconciled Cheques</th>
        </tr>
        <tr class='border-top'>
            <th style='width:5%;'>Reference No</th>
            <th style='width:5%;'>Description</th>
            <th style='width:5%; text-align:center;'>Pay Date</th>
            <th style='width:5%; text-align:center;'>Clear Date</th>
            <th style='width:5%; text-align:center;'>Deposit</th>
            <th style='width:5%; text-align:center;'>Withdrawal</th>
        </tr>
        </thead>
        <tbody id="reconciled_cheques_table">
        <?php foreach ($reconciled_cheques AS $item) { ?>
            <tr>
                <td style='text-align:center;'><?php echo 'BR'.$item['id']; ?></td>
                <td style='text-align:center;'><?php echo $item['ref_no']; ?></td>
                <td style='text-align:center;'><?php echo $item['pay_date']; ?></td>
                <td style='text-align:center;'><?php echo $item['clear_date']; ?></td>
                <td class="deposit"></td>
                <td class="withdrawal" style='text-align:center;'><?php echo $item['amount']; ?></td>
            </tr>
        <?php } ?>
        </tbody>
        <tbody>
        <tr id="reconciled_cheques_total">
            <td colspan="4" style='text-align:right;font-weight: bold;font-size: 15px;' class='border-top'>Total: </td>
            <td style='text-align:center;' class="deposit"></td>
            <td style='text-align:center;' class="withdrawal"></td>
        </tr>
        </tbody>
    </table>

    <table style='width:100%;'>
        <thead>
        <tr>
            <th>Reconciled Deposits</th>
        </tr>
        <tr class='border-top'>
            <th style='width:5%;'>Reference No</th>
            <th style='width:5%;'>Description</th>
            <th style='width:5%; text-align:center;'>Pay Date</th>
            <th style='width:5%; text-align:center;'>Clear Date</th>
            <th style='width:5%; text-align:center;'>Deposit</th>
            <th style='width:5%; text-align:center;'>Withdrawal</th>
        </tr>
        </thead>
        <tbody id="reconciled_deposits_table">
        <?php foreach ($reconciled_deposits AS $item) { ?>
            <tr>
                <td style='text-align:center;'><?php echo 'BR'.$item['id']; ?></td>
                <td style='text-align:center;'><?php echo $item['ref_no']; ?></td>
                <td style='text-align:center;'><?php echo $item['pay_date']; ?></td>
                <td style='text-align:center;'><?php echo $item['clear_date']; ?></td>
                <td class="deposit" style='text-align:center;'><?php echo $item['amount']; ?></td>
                <td class="withdrawal" style='text-align:center;'></td>
            </tr>
        <?php } ?>
        </tbody>
        <tbody>
        <tr id="reconciled_deposits_total">
            <td colspan="4" style='text-align:right;font-weight: bold;font-size: 15px;' class='border-top'>Total: </td>
            <td style='text-align:center;' class="deposit"></td>
            <td style='text-align:center;' class="withdrawal"></td>
        </tr>
        </tbody>
    </table>

    <table style='width:100%;'>
        <thead>
        <tr>
            <th>Outstanding Cheques</th>
        </tr>
        <tr class='border-top'>
            <th style='width:5%;'>Reference No</th>
            <th style='width:5%;'>Description</th>
            <th style='width:5%; text-align:center;'>Pay Date</th>
            <th style='width:5%; text-align:center;'>Deposit</th>
            <th style='width:5%; text-align:center;'>Withdrawal</th>
        </tr>
        </thead>
        <tbody id="outstanding_cheques_table">
        <?php foreach ($outstanding_cheques AS $item) { ?>
            <tr>
                <td style='text-align:center;'><?php echo 'BR'.$item['id']; ?></td>
                <td style='text-align:center;'><?php echo $item['ref_no']; ?></td>
                <td style='text-align:center;'><?php echo $item['pay_date']; ?></td>
                <td class="deposit" style='text-align:center;'></td>
                <td class="withdrawal" style='text-align:center;'><?php echo $item['amount']; ?></td>
            </tr>
        <?php } ?>
        </tbody>
        <tbody>
        <tr id="outstanding_cheques_total">
            <td colspan="3" style='text-align:right;font-weight: bold;font-size: 15px;' class='border-top'>Total: </td>
            <td style='text-align:center;' class="deposit"></td>
            <td style='text-align:center;' class="withdrawal"></td>
        </tr>
        </tbody>
    </table>

    <table style='width:100%;'>
        <thead>
        <tr>
            <th>Outstanding Deposits</th>
        </tr>
        <tr class='border-top'>
            <th style='width:5%;'>Reference No</th>
            <th style='width:5%;'>Description</th>
            <th style='width:5%; text-align:center;'>Pay Date</th>
            <th style='width:5%; text-align:center;'>Deposit</th>
            <th style='width:5%; text-align:center;'>Withdrawal</th>
        </tr>
        </thead>
        <tbody id="outstanding_deposits_table">
        <?php foreach ($outstanding_deposits AS $item) { ?>
            <tr>
                <td style='text-align:center;'><?php echo 'BR'.$item['id']; ?></td>
                <td style='text-align:center;'><?php echo $item['ref_no']; ?></td>
                <td style='text-align:center;'><?php echo $item['pay_date']; ?></td>
                <td class="deposit" style='text-align:center;'><?php echo $item['amount']; ?></td>
                <td class="withdrawal" style='text-align:center;'></td>
            </tr>
        <?php } ?>
        </tbody>
        <tbody>
        <tr id="outstanding_deposits_total">
            <td colspan="3" style='text-align:right;font-weight: bold;font-size: 15px;' class='border-top'>Total: </td>
            <td style='text-align:center;' class="deposit"></td>
            <td style='text-align:center;' class="withdrawal"></td>
        </tr>
        </tbody>
    </table>

    <table style='width:100%;'>
        <thead>
        <tr class='border-top'>
            <th colspan="2">Reconciliation</th>
        </tr>
        </thead>
        <tbody id="reconciliation_table">
            <tr>
                <td style='height:40px; text-align:right; width: 50%;'>Balance On <?php echo $ending_month; ?>: </td>
                <td id="ending_balance" style='text-align:center;'><?php echo $bank_account_trans_total; ?></td>
            </tr>
            <tr>
                <td style='height:40px; text-align:right; width: 50%;'>Add: Outstanding Cheques: </td>
                <td id="outstanding_cheques" style='text-align:center;'></td>
            </tr>
            <tr>
                <td style='height:40px; text-align:right; width: 50%;'>Deduct: Outstanding Deposits: </td>
                <td id="outstanding_deposits" style='text-align:center;'></td>
            </tr>

            <tr class='border-top'>
                <td style='height:40px; text-align:right; width: 50%;'>Expected Balance On Statement: </td>
                <td id="expected_balance" style='text-align:center;'></td>
            </tr>
        </tbody>
    </table>
</div>
<script>

    jQuery(document).ready(function () {

        var total_reconciled_cheques = 0;
        $('#reconciled_cheques_table tr').each(function() {
            var el = $(this);

            var trans = el.find('.withdrawal').html();

            total_reconciled_cheques += parseFloat(trans);
        });

        $('#reconciled_cheques_total .withdrawal').html((total_reconciled_cheques.toFixed(2)));
        $('#reconciled_cheques_total .deposit').html('0.00');

        var total_reconciled_deposits = 0;
        $('#reconciled_deposits_table tr').each(function() {
            var el = $(this);

            var trans = el.find('.deposit').html();

            total_reconciled_deposits += parseFloat(trans);
        });

        $('#reconciled_deposits_total .withdrawal').html('0.00');
        $('#reconciled_deposits_total .deposit').html((total_reconciled_deposits.toFixed(2)));

        var total_outstanding_cheques = 0;
        $('#outstanding_cheques_table tr').each(function() {
            var el = $(this);

            var trans = el.find('.withdrawal').html();

            total_outstanding_cheques += parseFloat(trans);
        });

        $('#outstanding_cheques_total .withdrawal').html((total_outstanding_cheques.toFixed(2)));
        $('#outstanding_cheques_total .deposit').html('0.00');
        $('#outstanding_cheques').html((total_outstanding_cheques.toFixed(2)));

        var total_outstanding_deposits = 0;
        $('#outstanding_deposits_table tr').each(function() {
            var el = $(this);

            var trans = el.find('.deposit').html();

            total_outstanding_deposits += parseFloat(trans);
        });

        $('#outstanding_deposits_total .withdrawal').html('0.00');
        $('#outstanding_deposits_total .deposit').html((total_outstanding_deposits.toFixed(2)));
        $('#outstanding_deposits').html((total_outstanding_deposits.toFixed(2)));

        var ending_balance = parseFloat($('#ending_balance').html());
        $('#expected_balance').html((ending_balance +total_outstanding_deposits -total_outstanding_cheques).toFixed(2));

        $('.deposit, .withdrawal, #outstanding_deposits, #outstanding_cheques, #ending_balance, #expected_balance').each(function() {
            var el = $(this);

            var html_amount = el.html();

            if (html_amount != '') {
                el.html(numberWithCommas(parseFloat(html_amount).toFixed(2)));
            }
        });

        function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }
    });

    $(document).ready(function() {
        $("#download_excel_file").click(function(e) {
            e.preventDefault();

            //getting data from our table
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('table_wrapper');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');

            var a = document.createElement('a');
            a.href = data_type + ', ' + table_html;
            a.download = 'bank_recons_report.xls';
            a.click();
        });

        $('body').mouseover(function () {
            $("#download_excel_file").show();
            $("#download_pdf_file").show();
        });

        $("#download_pdf_file").click(function () {
            $("#download_excel_file").hide();
            $("#download_pdf_file").hide();
            window.print();
        });
    });
</script>