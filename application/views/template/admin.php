<!doctype html>
<html>
<head>
	<?php echo $head ?>
    <?php if ($hide_bar == TRUE) {?>
        <style>
            @font-face {
                font-family: 'hgw-OpenSansSemibold';
                font-style: normal;
                font-weight: 600;
                src: url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-OpenSans-Semibold.eot');
                /* IE9 Compat Modes */
                src: url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-OpenSans-Semibold.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */ url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-OpenSans-Semibold.woff') format('woff'), /* Modern Browsers */ url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-OpenSans-Semibold.ttf') format('truetype'), /* Safari, Android, iOS */ url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-OpenSans-Semibold.svg#OpenSansSemibold') format('svg');
                /* Legacy iOS */
            }
            .page-content {
                margin-left: 0px !important;
            }

            .nav-tabs_sme {
                border-bottom: 1px solid #f1f1f1;
                background-color: #fafafa;
                *zoom: 1;
            }
            .nav-tabs_sme:before,
            .nav-tabs_sme:after {
                display: table;
                content: "";
                line-height: 0;
            }
            .nav-tabs_sme:after {
                clear: both;
            }
            .nav-tabs_sme li {
                float: left;
                padding-right: 40px;
            }
            .nav-tabs_sme li:last-child {
                padding-right: 0;
            }
            .nav-tabs_sme li a {
                text-transform: uppercase;
                font-size: 12px;
                color: #777777;
                display: block;
                border-bottom: 4px solid transparent;
                padding: 21px 0 18px;
                text-decoration: none !important;
            }
            .nav-tabs_sme li a:hover {
                color: #d11f25;
            }
            .nav-tabs_sme li.active a {
                color: #d11f25;
                font-family: "hgw-OpenSansSemibold", Arial, sans-serif;
                border-bottom-color: #d11f25;
            }

            .nav-tabs_sme-mobile {
                display: none;
                cursor: pointer;
                text-align: center;
                color: #777777;
                background-color: #fafafa;
                line-height: 40px;
                height: 60px;
                padding: 10px 0;
                border-bottom: 1px solid #f1f1f1;
            }
            .nav-tabs_sme-mobile .nvm-text {
                text-transform: uppercase;
            }
            .nav-tabs_sme-mobile .nvm-arrow {
                display: inline-block;
                vertical-align: top;
                margin: 15px 0 0 8px;
                border-left: 6px solid transparent;
                border-right: 6px solid transparent;
                border-top: 6px solid #999999;
            }

            @media screen and (min-width: 641px) {
                .nav-tabs_sme {
                    display: block !important;
                }
            }
            @media screen and (max-width: 640px) {
                .nav-tabs_sme-wrap {
                    position: relative;
                }

                .nav-tabs_sme-mobile {
                    display: block;
                }

                .nav-tabs_sme {
                    background: rgba(255, 255, 255, 0.96);
                    border: none;
                    display: none;
                    position: absolute;
                    top: 100%;
                    left: 0;
                    width: 100%;
                    padding: 0 !important;
                    z-index: 5;
                }

                .nav-tabs_sme li {
                    float: none;
                    width: 100%;
                    padding-right: 0;
                    border-bottom: 1px solid #dddddd;
                    text-align: center;
                }

                .nav-tabs_sme li a {
                    font-size: 14px;
                    border: none;
                    padding: 20px 0;
                }

                .nav-tabs_sme li.active {
                    display: none;
                }
            }



            @font-face {
                font-family: 'hgw-RobotoSlabLight';
                font-style: normal;
                font-weight: 300;
                src: url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-RobotoSlab-Light.eot');
                /* IE9 Compat Modes */
                src: url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-RobotoSlab-Light.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */ url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-RobotoSlab-Light.woff') format('woff'), /* Modern Browsers */ url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-RobotoSlab-Light.ttf') format('truetype'), /* Safari, Android, iOS */ url('https://merchant.hungrygowhere.com/bundles/hgwmerchantss/fonts/hgw-RobotoSlab-Light.svg#RobotoSlabLight') format('svg');
                /* Legacy iOS */
            }

            .hgw_title {
                font-size: 34px;
                font-family: "hgw-RobotoSlabLight", Arial, sans-serif;
            }

            .top-pane {
                min-height: 115px;
                padding: 40px 0 0;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }

            .mss-space {
                padding-left: 50px !important;
                padding-right: 50px !important;
            }

            .mss-line {
                border-bottom: 1px solid #f1f1f1;
            }

            <?php echo $hide_bar == TRUE ? '.page-content-wrapper .page-content{padding: 0px !important;}' : ''; ?>
        </style>

    <?php  } ?>
</head>
	<body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo">
			<?php echo $header; ?>
			<div class="clearfix">
			</div>
			<div class="page-container" <?php echo $hide_bar == TRUE ? 'style="margin-top:0px !important;"' : ''; ?>>
				<?php echo $sidebar ?>
				<!-- BEGIN CONTENT -->
			<div class="page-content-wrapper">
				<div class="page-content">
                    <?php if ($hide_bar == TRUE) { ?>
                        <div class="top-pane mss-space mss-line">
                            <span class="hgw_title">Accounting</span>
                        </div>
                        <div class="nav-tabs_sme-wrap">
                            <div class="nav-tabs_sme-mobile">
                                <?php
                                switch ($check_active) {
                                    case 'PL_report/year_to_date_view':
                                        $selected = 'P&L (year-to-date)';
                                        break;
                                    case 'PL_report/monthly_view':
                                        $selected = 'P&L (monthly)';
                                        break;
                                    case 'balance_sheet_report/year_to_date_view':
                                        $selected = 'BS (year-to-date)';
                                        break;
                                    case 'balance_sheet_report/monthly_view':
                                        $selected = 'BS (monthly)';
                                        break;
                                    case 'aging_report/AP_view':
                                        $selected = 'Aging Report';
                                        break;
                                    case 'gst_report/view':
                                        $selected = 'GST Report';
                                        break;
                                }

                                ?>
                                <span class="nvm-text"><?php echo $selected; ?> <i class="nvm-arrow"></i></span>
                            </div>
                            <ul class="nav-tabs_sme mss-space" style="list-style: none;">
                                <li>
                                    <a href="<?php echo base_url(); ?>salary_import/index">Salary Input</a>
                                </li>
                                <li <?php echo $check_active == 'PL_report/year_to_date_view' ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo base_url(); ?>PL_report/year_to_date_view">P&L (year-to-date)</a>
                                </li>
                                <li <?php echo $check_active == 'PL_report/monthly_view' ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo base_url(); ?>PL_report/monthly_view">P&L (monthly)</a>
                                </li>
                                <li <?php echo $check_active == 'balance_sheet_report/year_to_date_view' ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo base_url(); ?>balance_sheet_report/year_to_date_view">BS (year-to-date)</a>
                                </li>
                                <li <?php echo $check_active == 'balance_sheet_report/monthly_view' ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo base_url(); ?>balance_sheet_report/monthly_view">BS (monthly)</a>
                                </li>
                                <li <?php echo $check_active == 'aging_report/AP_view' ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo base_url(); ?>aging_report/AP_view">Aging Report</a>
                                </li>
                                <li <?php echo $check_active == 'gst_report/view' ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo base_url(); ?>gst_report/view">GST Report</a>
                                </li>
                            </ul>
                        </div>
                    <?php } ?>
					<?php echo $content ?>
				</div>
			</div>
			<!-- END CONTENT -->
			</div>
			<?php echo $footer ?>
	<?php echo $foot ?>
	</body>
</html>

<!--  MODAL -->
<div class="modal" id="home_page_modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×
                </button>
            </div>
            <div class="modal-body" id="home_page_modal_body">
                <iframe style="height:400px" id="iframe_modal" frameborder="0" width="100%" src=""></iframe>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="change_password-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="project-modal-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <?php echo form_open($action, 'class="form-horizontal" method="post" data-parsley-validate'); ?>

                <div class="form-group">
                    <?php echo form_label('Username <span class="required">*</span>', 'username', array('class' => 'control-label col-md-3')); ?>

                    <div class="col-md-4">
                        <div class="input-icon right">
                            <i class="fa"></i>
                            <?php
                            echo form_input('code', set_value('username', ''), 'class="form-control" data-parsley-required="true" data-parsley-type="alphanum" readonly');
                            ?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo form_label('Email <span class="required">*</span>', 'email', array('class' => 'control-label col-md-3')); ?>

                    <div class="col-md-4">
                        <div class="input-icon right">
                            <i class="fa"></i>
                            <?php
                            echo form_input('email', set_value('email', ''), 'class="form-control" data-parsley-required="true"  readonly');
                            ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo form_label('[[NEW_PASSWORD]] <span class="required">*</span>', 'password', array('class' => 'control-label col-md-3')); ?>

                    <div class="col-md-4">
                        <div class="input-icon right">
                            <i class="fa"></i>
                            <?php echo form_password('password', set_value('password'), 'class="form-control" required id="pwd"'); ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo form_label('[[CONFIRM_PASSWORD]] <span class="required">*</span>', 'confirm_passsord', array('class' => 'control-label col-md-3')); ?>

                    <div class="col-md-4">
                        <div class="input-icon right">
                            <i class="fa"></i>
                            <?php echo form_password('confirm_passsord', set_value('password'), 'class="form-control" data-parsley-equalto="#pwd" required data-parsley-error-message="Confirm Password must match Password" '); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_id">
                <button  type="submit" name="btnSubmit" class="btn btn-md blue">[[SUBMIT]]</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">[[CLOSE]]</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<!-- approve END MODAL -->
<script>
    jQuery(document).ready(function () {
        $("#btn_chg_pwd").click(function () {
            $('#change_password-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $(".nav-tabs_sme-mobile").click(function(){
            var d=$(this);
            var e=d.parents(".nav-tabs_sme-wrap");
            if(e.find(".nav-tabs_sme").hasClass("nav-tap-open")) {
                e.find(".nav-tabs_sme").removeClass("nav-tap-open");
                e.find(".nav-tabs_sme").slideUp()
            } else {
                e.find(".nav-tabs_sme").addClass("nav-tap-open");
                e.find(".nav-tabs_sme").slideDown();
            }
        });
        $(document).mouseup(function(g){
            var f=$(".user-info .ui-detail");
            if(!f.is(g.target)&&f.has(g.target).length===0) {
                f.find(".ui-selectmenu-button").removeClass("ui-selectmenu-button-open");
                f.find(".ui-selectmenu-menu").removeClass("ui-front ui-selectmenu-open")
            }
            var d=$(".nav-tabs_sme-wrap");
            var h=$(".nav-tabs_sme-mobile");
            if(h.is(":visible")&&d.length&&!d.is(g.target)&&d.has(g.target).length===0) {
                d.find(".nav-tabs_sme").removeClass("nav-tap-open").slideUp();
            }
        });
    });
</script>