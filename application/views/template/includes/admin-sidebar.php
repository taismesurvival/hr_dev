<!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper" <?php echo $hide_bar == TRUE ? 'style="display:none;"' : ''; ?>>
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <div class="page-sidebar navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU1 -->
            <ul class="page-sidebar-menu page-sidebar-menu-hover-submenu hidden-sm hidden-xs" data-auto-scroll="true" data-slide-speed="200">

                <li>
                    <a href="<?php echo base_url() ?>company">
                        <span class="title">Company</span>
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    </a>
                </li>

                <li class="">
                    <a href="javascript:">
                        <span class="title">GL Journal</span>
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo base_url() ?>GL_entry">GL Entry</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>GL_entry_taxable">GL Entry (taxable)</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>chart_of_account">Chart of Account</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="javascript:">
                        <span class="title">Purchase Entry</span>
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo base_url() ?>purchase_entry">Purchase Entry</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>credit_note?status=pending">Credit Note</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>payment?status=pending">Payment</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>supplier">Supplier</a>
                        </li>
                    </ul>
                </li>

                <li class="">
                    <a href="javascript:">
                        <span class="title">Bank Reconciliation</span>
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo base_url() ?>bank_statement_upload">Bank Statement Upload</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>sales_collection_submit">Sales Collection</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>bank_reconciliation">Bank Transaction</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>clear_bank_submit">Reconcile Bank</a>
                        </li>
                    </ul>
                </li>

                <li class="">
                    <a href="javascript:">
                        <span class="title">Report</span>
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo base_url() ?>PL_report/year_to_date_view">P&L (year-to-date)</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>PL_report/monthly_view">P&L (monthly)</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>balance_sheet_report/year_to_date_view">Balance Sheet (year-to-date)</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>balance_sheet_report/monthly_view">Balance Sheet (monthly)</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>aging_report/AP_view">AP Aging</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>gst_report/view">GST Report</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>gl_report/view">GL Report</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>bank_recons_report/view">Bank Reconciliation Report</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>trial_balance_report/view">Trial Balance Report</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>general_listing_report/view">General Listing Report</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>sales_input/select_outlet">
                        <span class="title">Import Salary & Input Sales</span>
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    </a>
                </li>
                <?php if ($user_type == 'from_pos') { ?>
                <li>
                    <a href="http://smefab.leewenyong.com">
                        <span class="title">POS System</span>
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    </a>
                </li>
                <?php } ?>
                <!-- END 20160318 vernhui Add in Email Link to L12 System -->
			</ul>



           
            <!-- END SIDEBAR MENU1 -->
            <!-- BEGIN RESPONSIVE MENU FOR HORIZONTAL & SIDEBAR MENU -->
            <ul class="page-sidebar-menu visible-sm visible-xs" data-slide-speed="200" data-auto-scroll="true">

                <li>
                    <a href="<?php echo base_url() ?>company">
                        <span class="title">Company</span>
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    </a>
                </li>

                <li class="">
                    <a href="javascript:">
                        <span class="title">GL Journal</span>
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo base_url() ?>GL_entry">GL Entry</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>GL_entry_taxable">GL Entry (taxable)</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>chart_of_account">Chart of Account</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="javascript:">
                        <span class="title">Purchase Entry</span>
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo base_url() ?>purchase_entry">Purchase Entry</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>credit_note?status=pending">Credit Note</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>payment?status=pending">Payment</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>supplier">Supplier</a>
                        </li>
                    </ul>
                </li>

                <li class="">
                    <a href="javascript:">
                        <span class="title">Bank Reconciliation</span>
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo base_url() ?>bank_statement_upload">Bank Statement Upload</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>sales_collection_submit">Sales Collection</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>bank_reconciliation">Bank Transaction</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>clear_bank_submit">Reconcile Bank</a>
                        </li>
                    </ul>
                </li>

                <li class="">
                    <a href="javascript:">
                        <span class="title">Report</span>
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo base_url() ?>PL_report/year_to_date_view">P&L (year-to-date)</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>PL_report/monthly_view">P&L (monthly)</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>balance_sheet_report/year_to_date_view">Balance Sheet (year-to-date)</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>balance_sheet_report/monthly_view">Balance Sheet (monthly)</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>aging_report/AP_view">AP Aging</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>gst_report/view">GST Report</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>gl_report/view">GL Report</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>bank_recons_report/view">Bank Reconciliation Report</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>trial_balance_report/view">Trial Balance Report</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() ?>general_listing_report/view">General Listing Report</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>sales_input/select_outlet">
                        <span class="title">Import Salary & Input Sales</span>
                        <span class="arrow open"></span>
                        <span class="selected"></span>
                    </a>
                </li>
                <?php if ($user_type == 'from_pos') { ?>
                    <li>
                        <a href="http://smefab.leewenyong.com">
                            <span class="title">POS System</span>
                            <span class="arrow open"></span>
                            <span class="selected"></span>
                        </a>
                    </li>
                <?php } ?>
                
            </ul>
            <!-- END RESPONSIVE MENU FOR HORIZONTAL & SIDEBAR MENU -->
        </div>
    </div>
    <!-- END SIDEBAR -->
