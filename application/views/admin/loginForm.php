<!DOCTYPE html>

<html lang="en">

<head>
	<meta charset="utf-8"/>
	<title>Accounting System</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<meta content="" name="description"/>
	<meta content="" name="author"/>


	
	<link href="<?php echo base_url('public/assets/global/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('public/assets/global/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('public/assets/global/plugins/uniform/css/uniform.default.css') ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css"/>

	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/select2/select2.css') ?>"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') ?>"/>
	<!-- END PAGE LEVEL STYLES -->

	<!-- BEGIN THEME STYLES -->
	<link href="<?php echo base_url('public/assets/global/css/components.css') ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('public/assets/global/css/plugins.css') ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('public/assets/admin/layout/css/layout.css') ?>" rel="stylesheet" type="text/css"/>
	<link id="style_color" href="<?php echo base_url('public/assets/admin/layout/css/themes/default.css') ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('public/assets/admin/pages/css/login-soft.css') ?>" rel="stylesheet" type="text/css"/>
	<link href="<?php echo base_url('public/assets/admin/layout/css/custom.css') ?>" rel="stylesheet" type="text/css"/>

	<!-- END THEME STYLES -->
	<link rel="shortcut icon" href="favicon.ico"/>
	<script src="<?php echo base_url('public/assets/global/plugins/jquery-1.11.0.min.js') ?>" type="text/javascript"></script>
	<style>
		@font-face {
    		font-family: raleway;
		    src: url(../public/font/Raleway-Regular.ttf);
		}
		.parsley-errors-list {
			padding-left: 0px;
		}
		.parsley-required {
			list-style: none;
			color: red;
		}
	</style>
</head>

<body style='background:#f1f1f1;'>

<div align='center' style='background:#f1f1f1;margin-top:100px' >

			<div align='center'><img src="<?php echo base_url('') ?>public/images/logo.png" style='height:100px;margin-top:50px;'></div>
			<div style='margin-top:30px;'>
			<form method="POST" id="login_form" data-parsley-validate>
				<table align='center'>
					<?php
						if(isset($msg) && $msg != ""){
					?>
						<tr>
							<td colspan='2' height="50">
								<div class="row">
									<div class="col-xs-12">	
										<span class="alert alert-danger"><?= $msg?></span>
									</div>
								</div>
							</td>
						</tr>
					<?php
						}
					?>
					<tr style='height:40px'>
						<td style='padding:0px 10px'>
							<span style='font-size:20px;font-family:"Arial Rounded MT Bold", "Helvetica Rounded", Arial, sans-serif'>Company Code</span>
						</td>
						<td>	
							<input type="text" name="code" id="code" style='border-radius:100px !important;background:rgba(0,0,0,0);border:solid black 2px;height:35px;' data-parsley-required="true" data-parsley-required-message="Company Code is required field">
						</td>
					</tr>
					<tr style='height:40px'>
						<td style='padding:0px 10px'>
							<span style='font-size:20px;font-family:"Arial Rounded MT Bold", "Helvetica Rounded", Arial, sans-serif'>Username</span>
						</td>
						<td>
							<input type="text" name="user_name" id="user_name" style='border-radius:100px !important;background:rgba(0,0,0,0);border:solid black 2px;height:35px' data-parsley-required="true" data-parsley-required-message="User Name is required field">
						</td>
					</tr>
					<tr style='height:40px'>
						<td style='padding:0px 10px'>
							<span style='font-size:20px;font-family:"Arial Rounded MT Bold", "Helvetica Rounded", Arial, sans-serif'>Password</span>
						</td>
						<td>	
							<input type="password" name="password" id="pwd" style='border-radius:100px !important;background:rgba(0,0,0,0);border:solid black 2px;height:35px;' data-parsley-required="true" data-parsley-required-message="Password is required field">
						</td>
					</tr>
					
					<tr>
						<td colspan='2'>
							<!--
							<input class="btn form-control" style='font-family:raleway;background-color:#FFFA81;margin-top:20px;border-radius:50px !important;font-size:30px;height:50px' type="submit" name="btnLogin" value="Sign in">
							-->
							<button class="btn form-control" style='font-family:"Arial Rounded MT Bold", "Helvetica Rounded", Arial, sans-serif;background-color:#FFFA81;margin-top:20px;border-radius:50px !important;font-size:30px;height:50px' type="button" name="btnLogin" value="Sign in" id="btnLogin">Sign in</button>
						</td>
					</tr>
					<!--
					<tr>
						<td colspan='2' align="right">
							&nbsp;
						</td>
					</tr>
					-->
				</table>
				
				
				
			</form>
			</div>
    
	</div>

	<div align='center' style="display:none;"><span style="font-size:28px;font-family:raleway" id="countnum">logging you in </span></div>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

<script type="text/javascript" src="<?php echo base_url('public/assets/global/plugins/slider-pips/jquery.blockUI.js') ?>"></script>
<script src="<?php echo base_url('public/assets/global/scripts/metronic.js') ?>" type="text/javascript"></script>
<script type="text/javascript">
	
	function count(maks){
		var current = parseInt(maks, 10);
		current = current -1;
		var msg = current / 100;
		var data = 'logging you in '+ msg;
        $("#countnum").html(data);
        if(current !== 0){
            setTimeout(function(){count(current)}, 50);
        }else{
        	$("#login_form").submit();
        }
	}

	$("#btnLogin").on('click',function () {
		//submit_login();
		$("#login_form").submit();
	});
	$("#pwd").on('keyup',function (e) {
		var code = e.which;
		if(code == 13){
			//submit_login();
			$("#login_form").submit();
		}
	});

	function submit_login(){

		$.blockUI({
            boxed: true,
            message: $('#countnum'),
            textOnly: true
        });
		count(20);
	}
</script>
<script type="text/javascript" src="<?php echo base_url('') ?>public/assets/global/plugins/parsley/parsley.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('') ?>public/assets/global/plugins/parsley/parsley.remote.min.js"></script>