<style>
    .choose_account {cursor: pointer;}

    .ms-container .ms-selectable, .ms-container .ms-selection {
        width: 40% !important;
    }
</style>
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Bank Reconciliation
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo base_url('clear_bank_submit') ?>">Bank Reconciliation Submit History</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Bank Reconciliation</a>
        </li>
</div>
<!-- END PAGE HEADER -->
<!-- BEGIN ALERT BOX -->
<?php if (isset($flash_message)) { ?>
    <div class="row margin-bottom-20">
        <div class="col-xs-12">
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END ALERT BOX -->

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Bank Reconciliation</span>
                </div>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-12">

                        <!-- Begin: life time stats -->
                        <div class="table-container">
                            <!-- Begin: life time stats -->
                            <div class="clearfix mt15">

                                <form method="post" class="form" role="form" data-parsley-validate enctype="multipart/form-data">
                                    <div class="row">

                                        <div class="col-xs-12">
                                            <div class="row">
                                                <?php echo form_label('Bank Account', 'description', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <select id="bank_account_id" name="bank_account_id" class="form-control" data-parsley-required="true" data-parsley-required-message="Account is required field">
                                                        <?php echo $bank_accounts_selector; ?>

                                                    </select>
                                                </div>

                                                <?php echo form_label('Month', 'description', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <div class="input-group date month-picker" data-date-format="yyyy-mm"
                                                         data-date-viewmode="years">
                                                        <input id="selected_month" value="" name="selected_month" type="text" class="form-control" data-parsley-required="true" data-parsley-required-message="Month is required">

                                                        <div class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-xs-12 text-right" style="margin-top: 10px;">
                                            <button type="submit" class="btn btn-md green"><i class="icon-magnifier font-white"></i>Show Outstanding Transactions</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <?php if (!empty($payments)) { ?>
                            <table style="margin-top: 20px;" class="table table-bordered">
                                <thead>
                                <tr role="row" class="heading">
                                    <th style="border-bottom: 1px solid #ddd;background-color: white !important;"></th>
                                    <th class="text-center" style="border-bottom: 1px solid #ddd;" colspan="4">Bank Reconciliation</th>
                                    <th style="border-bottom: 1px solid #ddd;background-color: white !important;"></th>
                                    <th class="text-center" style="border-bottom: 1px solid #ddd;" colspan="4">Bank Statement</th>
                                </tr>
                                <tr role="row" class="heading">
                                    <th width="4%" class="text-right">No</th>
                                    <th width="8%">Type</th>
                                    <th width="8%">Amount</th>
                                    <th width="8%">Reference No</th>
                                    <th width="8%">Pay Date</th>
                                    <th width="8%">Match</th>
                                    <th width="8%">Clear Date</th>
                                    <th width="8%">Deposit</th>
                                    <th width="8%">Withdrawal</th>
                                    <th width="8%">Description</th>
                                </tr>
                                </thead>
                                <tbody id="matching_table">
                                    <?php
                                    $total_cash_in_bank = 0;
                                    $total_bank_charge = 0;

                                    for ($i = 0; $i < count($payments); $i++) {

                                        $class = !empty($payments[$i]['id']) ? 'recons_select' : 'statement_select';

                                        ?>
                                    <tr role="row" class="trans">
                                        <td class="text-right match_check">
                                            <?php echo isset($payments[$i]['clear_date']) ? '<a href="javascript:;" class="unmatch">Unmatch</a>' : '<input type="checkbox" class="'.$class.'" >'; ?>
                                        </td>
                                        <td class="hidden recons_type"><?php echo $payments[$i]['type']; ?></td>
                                        <td><?php echo !empty($payments[$i]['type']) ? ($payments[$i]['type'] == 'AR' ? 'Deposit' : 'Withdrawal') : ''; ?></td>
                                        <td class="recons_amount"><?php echo !empty($payments[$i]['amount']) ? number_format($payments[$i]['amount'],2) : ''; ?></td>
                                        <td class="bank_reconciliation_id hidden"><?php echo $payments[$i]['id']; ?></td>
                                        <td><?php echo $payments[$i]['ref_no']; ?></td>
                                        <td><?php echo $payments[$i]['pay_date']; ?></td>
                                        <td class="match_status"><?php echo isset($payments[$i]['clear_date']) ? '<span style="color: green;font-weight: bold; font-size: 14px;">Matched</span>' : ''; ?></td>
                                        <td class="clear_date"><?php echo $payments[$i]['date']; ?></td>
                                        <td class="deposit"><?php echo !empty($payments[$i]['deposit']) ? number_format($payments[$i]['deposit'],2) : ''; ?></td>
                                        <td class="withdrawal"><?php echo !empty($payments[$i]['withdrawal']) ? number_format($payments[$i]['withdrawal'],2) : ''; ?></td>
                                        <td class="description"><?php echo $payments[$i]['description']; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php } ?>

                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
                <?php if (!empty($payments)) { ?>
                <div class="text-center">
                    <small id="required_selected_trans" style="color:red; display:none;">All Transactions from Bank Statement need to be clear</small>
                    <a id="pay_invoices" class="btn btn-md blue"><i class="fa fa-pencil"></i> Submit</a>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {

        $(".statement_select").prop("disabled", true);

        each_row();

        function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }

        $('.month-picker').datepicker({
            format: "yyyy-mm",
            viewMode: "months",
            minViewMode: "months"
        });

        var selected_month = '<?php echo $search['selected_month']; ?>';

        $('#selected_month').val(selected_month);

        var bank_account_id = '<?php echo $search['bank_account_id']; ?>';

        $('#bank_account_id').val(bank_account_id);
        
        function each_row() {

            $('.unmatch').on('click', function () {

                $(".statement_select").prop("checked", false);

                $(".recons_select").prop("checked", false);

                var el = $(this);

                el.closest('tr').find('.match_status').html('');

                var clear_date = el.closest('tr').find('.clear_date').html();

                var deposit = el.closest('tr').find('.deposit').html();

                var withdrawal = el.closest('tr').find('.withdrawal').html();

                var description = el.closest('tr').find('.description').html();

                if (clear_date !== undefined && deposit !== undefined && withdrawal !== undefined && description !== undefined) {

                    el.closest('tr').find('.clear_date').html('');

                    el.closest('tr').find('.deposit').html('');

                    el.closest('tr').find('.withdrawal').html('');

                    el.closest('tr').find('.description').html('');

                    var html = '<tr role="row" class="trans">';
                    html += '<td class="text-right match_check">';
                    html += '<input type="checkbox" class="statement_select">';
                    html += '</td>';
                    html += '<td></td>';
                    html += '<td></td>';
                    html += '<td class="bank_reconciliation_id hidden"></td>';
                    html += '<td></td>';
                    html += '<td></td>';
                    html += '<td class="match_status"></td>';
                    html += '<td class="clear_date">' + clear_date + '</td>';
                    html += '<td class="deposit">' + deposit + '</td>';
                    html += '<td class="withdrawal">' + withdrawal + '</td>';
                    html += '<td class="description">' + description + '</td>';
                    html += '</tr>';

                    $('#matching_table').append(html);

                    $(".statement_select").prop("disabled", true);
                    $(".recons_select").prop("disabled", false);

                    el.closest('tr').find('.match_check').html('<input type="checkbox" class="recons_select">');

                    each_row();
                }

            });

            $('.recons_select').on('click', function () {

                var el = $(this);

                if (el.is(':checked')) {

                    var recons_type = el.closest('.trans').find('.recons_type').html();
                    var recons_amount = el.closest('.trans').find('.recons_amount').html();

                    if (recons_type !== undefined) {

                        $(".statement_select").prop("disabled", false);
                        $(".recons_select").prop("disabled", true);


                        el.prop("disabled", false);

                        var value_match_count = 0;
                        $('.statement_select').each(function () {

                            var el5 = $(this);

                            var deposit = el5.closest('.trans').find('.deposit').html();
                            var withdrawal = el5.closest('.trans').find('.withdrawal').html();

                            if ((recons_type == 'AR' && recons_amount == deposit) || (recons_type == 'AP' && recons_amount == withdrawal)) {

                                el5.closest('.trans').css('background-color', 'yellow');

                                value_match_count++;

                            } else {
                                el5.prop("disabled", true);
                            }

                        });

                        if (value_match_count == 1) {

                            $('.statement_select').each(function () {

                                var el3 = $(this);

                                var clear_date = el3.closest('tr').find('.clear_date').html();

                                var deposit = el3.closest('tr').find('.deposit').html();

                                var withdrawal = el3.closest('tr').find('.withdrawal').html();

                                var description = el3.closest('tr').find('.description').html();


                                if ((recons_type == 'AR' && recons_amount == deposit) || (recons_type == 'AP' && recons_amount == withdrawal)) {

                                    el3.closest('tr').remove();

                                    el.closest('tr').find('.match_status').html('<span style="color: green;font-weight: bold; font-size: 14px;">Matched</span>');


                                    el.closest('tr').find('.clear_date').html(clear_date);

                                    el.closest('tr').find('.deposit').html(deposit);

                                    el.closest('tr').find('.withdrawal').html(withdrawal);

                                    el.closest('tr').find('.description').html(description);

                                    el.closest('tr').find('.match_check').html('<a href="javascript:;" class="unmatch">Unmatch</a>');

                                    each_row();

                                    $(".statement_select").prop("disabled", true);
                                    $(".recons_select").prop("disabled", false);
                                    $('.trans').css('background-color', 'white');
                                }

                            });

                        }

                        $('.statement_select').on('click', function () {

                            var el1 = $(this);

                            $('.trans').css('background-color', 'white');

                            var clear_date = el1.closest('tr').find('.clear_date').html();

                            var deposit = el1.closest('tr').find('.deposit').html();

                            var withdrawal = el1.closest('tr').find('.withdrawal').html();

                            var description = el1.closest('tr').find('.description').html();

                            el1.closest('tr').remove();

                            el.closest('tr').find('.match_status').html('<span style="color: green;font-weight: bold; font-size: 14px;">Matched</span>');


                            el.closest('tr').find('.clear_date').html(clear_date);

                            el.closest('tr').find('.deposit').html(deposit);

                            el.closest('tr').find('.withdrawal').html(withdrawal);

                            el.closest('tr').find('.description').html(description);

                            el.closest('tr').find('.match_check').html('<a href="javascript:;" class="unmatch">Unmatch</a>');

                            each_row();

                            $(".statement_select").prop("disabled", true);
                            $(".recons_select").prop("disabled", false);

                        });
                    }
                } else {

                    $(".statement_select").prop("disabled", true);
                    $(".recons_select").prop("disabled", false);
                    $('.trans').css('background-color', 'white');

                }

            });
        }

        $('#pay_invoices').on('click', function () {

            $(this).prop("disabled", true);

            var bank_account_id = $('#bank_account_id').val();
            var month = $('#selected_month').val();
            var type = $('#type').val();
            var transactions = [];
            var error = false;
            $('#matching_table tr').each(function() {

                var el = $(this);

                if (el.find('.match_status').html() != '') {
                    var transaction = {};
                    transaction.bank_reconciliation_id = el.find('.bank_reconciliation_id').html();
                    transaction.clear_date = el.find('.clear_date').html();

                    transactions.push(transaction);
                }

                if (el.find('.recons_type').html() == '') {
                    error = true;
                }

            });

            if (error != true) {
                $('#required_selected_trans').hide();
            } else {
                $('#required_selected_trans').show();
                $(this).prop("disabled", false);
                return;
            }

            $.ajax({
                method: 'POST',
                url: "<?php echo base_url(); ?>clear_bank_reconciliation/submit",
                data: {
                    bank_account_id: bank_account_id,
                    month: month,
                    type: type,
                    transactions: transactions
                },
                dataType: 'json'
            }).done(function() {

                window.location = "<?php echo base_url(); ?>clear_bank_submit";
            });


        });

    });
</script>
