<style>
    table { border-collapse: collapse; }
    tr.border-top { border-top: solid thin; }
    tr.border-bottom { border-bottom: solid thin; }
    tr.bold { font-weight: bold; }
    * { font-size: 12px; font-family: Lucida Sans Unicode; }
</style>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/slider-pips/jquery-ui.js"></script>
<button type="button" id="download_excel_file">Download Excel Report</button>
<button type="button" id="download_pdf_file">Print or Download PDF Report</button>
<div id="table_wrapper">
    <table style='width:100%;'>
        <tr>
            <td style='font-size:20px;text-align:center;color:blue;'><?php echo $company_name; ?></td>
        </tr>
        <tr>
            <td style='font-size:16px;text-align:center;color:blue;'>Outlet</td>
        </tr>
        <tr>
            <td style='font-size:16px;text-align:center;color:blue;'>
                <?php
                    echo $outlet_name.'<br/>';
                ?>
            </td>
        </tr>
        <tr>
            <td style='text-align:center;font-size:18px;'>Sales Report</td>
        </tr>
        <tr>
            <td style='font-size:16px;text-align:center;'><?php echo $month; ?></td>
        </tr>

    </table>
    <table style='width:100%;'>
        <thead>
        <tr class='border-top'>
            <th style='width:12%;  text-align:center;'>Date</th>
            <th style='width:5%;  text-align:center;'><?php echo $sales_names['40010']; ?></th>
            <th style='width:5%;  text-align:center;'><?php echo $sales_names['40020']; ?></th>
            <th style='width:5%;  text-align:center;'><?php echo $sales_names['40030']; ?></th>
            <th style='width:5%;  text-align:center;'><?php echo $sales_names['40510']; ?></th>
            <th style='width:5%;  text-align:center;'>Service Charge</th>
            <th style='width:5%;  text-align:center;'>Delivery</th>
            <th style='width:5%;  text-align:center;'>GST</th>
            <th style='width:5%;  text-align:center;'>Adjustment</th>
            <th style='width:5%;  text-align:center;'>Discount</th>
            <th style='width:5%;  text-align:center;'><?php echo $debtor_names['14005']; ?></th>
            <th style='width:5%;  text-align:center;'><?php echo $debtor_names['14002']; ?></th>
            <th style='width:5%;  text-align:center;'><?php echo $debtor_names['14004']; ?></th>
            <th style='width:5%;  text-align:center;'><?php echo $debtor_names['14014']; ?></th>
            <th style='width:5%;  text-align:center;'><?php echo $debtor_names['14013']; ?></th>
            <th style='width:5%;  text-align:center;'><?php echo $debtor_names['14003']; ?></th>
            <th style='width:5%;  text-align:center;'><?php echo $debtor_names['14001']; ?></th>
            <th style='width:5%;  text-align:center;'><?php echo $debtor_names['14008']; ?></th>
            <th style='width:5%;  text-align:center;'><?php echo $debtor_names['14019']; ?></th>
            <th style='width:5%;  text-align:center;'><?php echo $debtor_names['14017']; ?></th>
            <th style='width:5%;  text-align:center;'><?php echo $debtor_names['14018']; ?></th>
            <th style='width:5%;  text-align:center;'>Sales Total</th>
        </tr>
        </thead>
        <tbody id="sales_table">
        <?php

        $food_total = 0 ;
        $beverage_total = 0 ;
        $liquor_total = 0 ;
        $others_total = 0 ;
        $service_charge_total = 0 ;
        $delivery_total = 0 ;
        $GST_total = 0 ;
        $adjustment_total = 0 ;
        $discount_total = 0 ;
        $cash_total = 0 ;
        $visa_card_total = 0 ;
        $nets_total = 0 ;
        $internal_voucher_total = 0 ;
        $external_voucher_total = 0 ;
        $master_card_total = 0 ;
        $american_express_total = 0 ;
        $corporate_total = 0 ;
        $foodpanda_total = 0 ;
        $ubereats_total = 0 ;
        $deliveroo_total = 0 ;

        foreach($sales_data AS $item) {

            $food_total += $item['food_amount'] ;
            $beverage_total += $item['beverage_amount'] ;
            $liquor_total += $item['liquor_amount'] ;
            $others_total += $item['others_amount'] ;
            $service_charge_total += $item['service_charge_amount'] ;
            $delivery_total += $item['delivery_amount'] ;
            $GST_total += $item['GST_amount'] ;
            $adjustment_total += $item['adjustment_amount'] ;
            $discount_total += $item['discount_amount'] ;
            $cash_total += $item['cash_amount'] ;
            $visa_card_total += $item['visa_card_amount'] ;
            $nets_total += $item['nets_amount'] ;
            $internal_voucher_total += $item['internal_voucher_amount'] ;
            $external_voucher_total += $item['external_voucher_amount'] ;
            $master_card_total += $item['master_card_amount'] ;
            $american_express_total += $item['american_express_amount'] ;
            $corporate_total += $item['corporate_amount'] ;
            $foodpanda_total += $item['foodpanda_amount'] ;
            $ubereats_total += $item['ubereats_amount'] ;
            $deliveroo_total += $item['deliveroo_amount'] ;

            ?>
            <tr>
                <td><?php echo $item['sales_date']; ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['food_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['beverage_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['liquor_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['others_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['service_charge_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['delivery_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['GST_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['adjustment_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['discount_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['cash_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['visa_card_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['nets_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['internal_voucher_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['external_voucher_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['master_card_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['american_express_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['corporate_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['foodpanda_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['ubereats_amount'],2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($item['deliveroo_amount'],2); ?></td>
                <td style='text-align:center; font-weight: bold;' class="sales_total"></td>
            </tr>
        <?php } ?>

            <tr style='font-weight: bold;'>
                <td>Total</td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($food_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($beverage_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($liquor_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($others_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($service_charge_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($delivery_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($GST_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($adjustment_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($discount_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($cash_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($visa_card_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($nets_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($internal_voucher_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($external_voucher_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($master_card_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($american_express_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($corporate_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($foodpanda_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($ubereats_total,2); ?></td>
                <td style='text-align:center;' class="sales_item"><?php echo number_format($deliveroo_total,2); ?></td>
                <td style='text-align:center; font-weight: bold;' class="sales_total"></td>
            </tr>
        </tbody>
    </table>
</div>
<script>

    jQuery(document).ready(function () {

        $('#sales_table tr').each(function() {
            var el = $(this);

            var sales_total = 0;
            el.find('.sales_item').each(function() {

                var el1 = $(this);

                var sales_item = parseFloat(el1.html().replace(/,/g, ""));

                sales_total += sales_item;


            });

            el.find('.sales_total').html(numberWithCommas((sales_total/2).toFixed(2)));
        });

        function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }
    });

    $(document).ready(function() {
        $("#download_excel_file").click(function(e) {
            e.preventDefault();

            //getting data from our table
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('table_wrapper');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');

            var a = document.createElement('a');
            a.href = data_type + ', ' + table_html;
            a.download = 'sales_report.xls';
            a.click();
        });

        $('body').mouseover(function () {
            $("#download_excel_file").show();
            $("#download_pdf_file").show();
        });

        $("#download_pdf_file").click(function () {
            $("#download_excel_file").hide();
            $("#download_pdf_file").hide();
            window.print();
        });
    });
</script>