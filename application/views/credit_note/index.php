<style>
    .choose_account, .choose_supplier {cursor: pointer;}
    #table_transaction_edit th, #table_select_project td {font-size: 14px;}
    #accounts_list td {font-size: 14px;line-height: 2.8;}
</style>
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Credit Note
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Credit Note</a>
        </li>
</div>
<!-- END PAGE HEADER -->
<!-- BEGIN ALERT BOX -->
<?php if (isset($flash_message)) { ?>
    <div class="row margin-bottom-20">
        <div class="col-xs-12">
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END ALERT BOX -->

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Credit Note</span>
                </div>
                <a href="#" class="purchase_entry-add btn btn-md blue pull-right"><i class="fa fa-plus"></i> Add New</a>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-12">

                        <!-- Begin: life time stats -->
                        <div class="table-container">
                            <!-- Begin: life time stats -->
                            <div class="clearfix mt15">

                                <form method="get" class="form" role="form">
                                    <div class="row">
                                        <div class="form-group">
                                            <?php echo form_label('Total Amount', 'minimum_amount', array('class' => 'col-md-3 col-lg-2 control-label')); ?>

                                            <div class="col-md-3 col-lg-4 form-group">
                                                <input type="text" name="minimum_amount" class="form-control" value="<?php echo $search['minimum_amount'] ?>" placeholder="Minimum Amount">
                                            </div>

                                            <?php echo form_label('~', 'maximum_amount', array('class' => 'col-md-2 col-lg-1 control-label')); ?>
                                            <div class="col-md-3 col-lg-4 form-group">
                                                <input type="text" name="maximum_amount" class="form-control" value="<?php echo $search['maximum_amount'] ?>" placeholder="Maximum Amount">
                                            </div>

                                            <?php echo form_label('Supplier', 'supplier', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                            <div class="col-md-3 col-lg-4 form-group">
                                                <select name="supplier_id" class="form-control">
                                                    <?php echo $supplier_selector; ?>
                                                </select>
                                            </div>

                                            <?php echo form_label('Keyword', 'keyword', array('class' => 'col-md-2 col-lg-1 control-label')); ?>
                                            <div class="col-md-3 col-lg-4 form-group">
                                                <input type="text" name="keyword" class="form-control" value="<?php echo $search['keyword'] ?>">
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="row">
                                                <?php echo form_label('Account Code or Name', 'description', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <input type="text" name="account_keyword" class="form-control" value="<?php echo $search['account_keyword'] ?>">
                                                </div>

                                                <?php echo form_label('Status', 'status', array('class' => 'col-md-2 col-lg-1 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <select name="status" class="form-control">
                                                        <option selected value="">Please Select</option>
                                                        <option <?php echo $search['status'] == 'received' ? 'selected' : '' ?> value="received">Received</option>
                                                        <option <?php echo $search['status'] == 'assigned' ? 'selected' : '' ?> value="assigned">Assigned</option>
                                                        <option <?php echo $search['status'] == 'pending' ? 'selected' : '' ?> value="pending">Pending</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="row">
                                                <?php echo form_label('Document Date', 'keyword', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <input type="text" name="document_date_from" class="form-control date-picker" value="<?php echo $search['document_date_from'] ?>" placeholder="Starting Date">
                                                </div>

                                                <?php echo form_label('~', 'description', array('class' => 'col-md-2 col-lg-1 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <input type="text" name="document_date_to" class="form-control date-picker" value="<?php echo $search['document_date_to'] ?>" placeholder="Ending Date">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 text-right">
                                            <a href="<?php echo base_url('purchase_entry') ?>" class=" btn btn-md grey-salsa">Reset</a>
                                            <button type="submit" class="btn btn-md green"><i class="icon-magnifier font-white"></i>Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <table style="margin-top: 20px;" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="4%" class="text-right">No</th>
                                    <th width="8%">Reference No</th>
                                    <th width="8%">Document Date</th>
                                    <th width="8%">Status</th>
                                    <th width="8%">Amount</th>
                                    <th width="8%">Supplier</th>
                                    <th width="25%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php for ($i = 0; $i < count($purchase_entries); $i++) {
                                    ?>
                                    <tr role="row" class="heading">
                                        <td class="text-right"><?php echo $pagination['offset']+$i ?></td>
                                        <td class="purchase_entry_id hidden"><?php echo $purchase_entries[$i]['id'] ?></td>
                                        <td><?php echo $purchase_entries[$i]['refference_no']; ?></td>
                                        <td class="purchase_entry_document_date"><?php echo $purchase_entries[$i]['document_date']; ?></td>
                                        <td><?php echo $purchase_entries[$i]['status']; ?></td>
                                        <td class="purchase_entry_refference_no hidden"><?php echo $purchase_entries[$i]['refference_no']; ?></td>
                                        <td class="purchase_entry_main_GL_transaction_id hidden"><?php echo $purchase_entries[$i]['main_GL_transaction_id']; ?></td>
                                        <td class="purchase_entry_tax_GL_transaction_id hidden"><?php echo $purchase_entries[$i]['tax_GL_transaction_id']; ?></td>
                                        <td class="purchase_entry_tax_rate hidden"><?php echo $purchase_entries[$i]['tax_rate']; ?></td>
                                        <td class="purchase_entry_tax_type hidden"><?php echo $purchase_entries[$i]['tax_type']; ?></td>
                                        <td class="purchase_entry_description hidden"><?php echo $purchase_entries[$i]['description']; ?></td>
                                        <td class="purchase_entry_total_amount"><?php echo number_format($purchase_entries[$i]['amount']+$purchase_entries[$i]['tax_amount'],2); ?></td>
                                        <td class="purchase_entry_supplier_name"><?php echo $purchase_entries[$i]['supplier_name']; ?></td>
                                        <td class="purchase_entry_supplier_id hidden"><?php echo $purchase_entries[$i]['supplier_id']; ?></td>
                                        <td class="purchase_entry_payable_GL_transaction_id hidden"><?php echo $purchase_entries[$i]['payable_GL_transaction_id']; ?></td>
                                        <td>
                                            <?php if ($purchase_entries[$i]['status'] == 'pending' && date('Y-m', strtotime($purchase_entries[$i]['document_date'])) > $locked_financial_month) { ?>
                                            <a class="purchase_entry-edit btn btn-md blue"><i class="fa fa-pencil"></i> Edit</a>
                                            <a class="purchase_entry-apply_purchase btn btn-md bg-green-seagreen"><i class="fa fa-list"></i> Apply to Purchase</a>
                                            <a class="purchase_entry-bank_in btn btn-md bg-green-seagreen"><i class="fa fa-list"></i> Bank In</a>
                                            <a href="#" class="btn btn-md red purchase_entry-delete"><i class="fa fa-trash-o"></i> Delete</a>
                                            <?php } else { ?>
                                                <a class="purchase_entry-view btn btn-md blue"><i class="fa fa-eye"></i> View</a>
                                            <?php if ($purchase_entries[$i]['status'] == 'received') { ?>
                                                <?php if (empty($purchase_entries[$i]['received_clear_date'])) { ?>
                                                <a class="purchase_entry-undo_bank_in btn btn-md yellow"><i class="fa fa-list"></i> Undo Bank In</a>
                                                <?php } ?>
                                                <a href="<?php echo base_url() ?>bank_reconciliation/index/<?php echo $purchase_entries[$i]['bank_reconciliation']; ?>" class="btn btn-md blue"><i class="fa fa-list"></i> View Bank In</a>
                                            <?php } else if ($purchase_entries[$i]['status'] == 'assigned') { ?>
                                                    <?php if (empty($purchase_entries[$i]['assigned_bank_reconciliation_id'])) { ?>
                                                    <a class="purchase_entry-undo btn btn-md yellow"><i class="fa fa-list"></i> Undo Apply</a>
                                                    <?php } ?>
                                                    <a href="<?php echo base_url() ?>purchase_entry/index/<?php echo $purchase_entries[$i]['purchase_entry_id']; ?>" class="btn btn-md blue"><i class="fa fa-list"></i> View Purchase</a>
                                            <?php } else { ?>
                                                    <a class="purchase_entry-apply_purchase btn btn-md bg-green-seagreen"><i class="fa fa-list"></i> Apply to Purchase</a>
                                                    <a class="purchase_entry-bank_in btn btn-md bg-green-seagreen"><i class="fa fa-list"></i> Bank In</a>
                                            <?php }} ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
                <nav class="pagination-wrap clearfix">
                    <div class="pull-left mt10">
                        <?php if ($pagination['total'] != 0) { ?>
                            <label class="form-control-static"><?php echo $pagination['total'] ?> Items in total, Display
                                <?php echo $pagination['offset'] ?> ~
                                <?php echo $pagination['limit'] ?></label>
                        <?php } ?>
                    </div>
                    <?php if (!empty($pagination) && $pagination['total'] > $pagination['items_per_page']) { ?>
                        <div class="pull-right">
                            <ul class="pagination no-margin">
                                <li <?php if ($pagination['page'] - 1 <= 0) { ?> class="disabled" <?php } ?>>

                                    <a href="<?php if ($pagination['page'] - 1 <= 0){ ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] - 1 ?><?php } ?>">
                                        Previous
                                    </a>
                                </li>
                                <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                                    <li>
                                        <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=1"> 1 </a>
                                    </li>
                                <?php } ?>

                                <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                                    <li><a href="javascript:;">...</a></li>
                                <?php } ?>

                                <?php for ($p=($pagination['min_display']); $p <= $pagination['max_display']; $p++) { ?>
                                    <?php if ($p > 0) { ?>
                                        <li <?php if ($pagination['page'] == $p) { ?> class="active" <?php } ?>>
                                            <a href="<?php if ($pagination['page'] != $p) { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $p ?><?php } else { ?>javascript:;<?php } ?>"> <?php echo $p ?> </a>
                                        </li>
                                    <?php } ?>
                                <?php } ?>

                                <?php if (($pagination['page'] + $pagination['adj']) < $pagination['total_page']) { ?>
                                    <li><a href="javascript:;">...</a></li>
                                <?php } ?>

                                <?php if ($pagination['page'] + $pagination['adj'] < $pagination['total_page']) { ?>
                                    <li>
                                        <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['total_page'] ?>"> <?php echo $pagination['total_page'] ?> </a>
                                    </li>
                                <?php } ?>

                                <li <?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> class="disabled" <?php } ?>>
                                    <a href="<?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] + 1 ?><?php } ?>">
                                        Next
                                    </a>
                                </li>
                            </ul>
                        </div>
                    <?php } ?>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="purchase_entry-edit-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg" style="width: 95%;">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="purchase_entry-modal-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">

                <div id="show_error_message" class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span id="error_message">Financial Period is updated successfully</span>
                </div>

                <?php echo form_open_multipart(base_url('credit_note/edit'), 'class="form-horizontal" method="post" id="purchase_entry-edit_form"'); ?>

                <div style="padding: 15px;" class="form-group">

                    <div class="form-group">
                        <?php echo form_label('Description', 'description', array('class' => 'col-md-3 col-lg-2 control-label')); ?>

                        <div class="col-md-3 col-lg-4 form-group">
                            <textarea name="description" cols="10" rows="3" class="form-control" id="purchase_entry_description"></textarea>
                        </div>

                        <?php echo form_label('Type <span style="color:red;" class="required">*</span>', 'type', array('class' => 'col-md-3 col-lg-2 control-label hidden')); ?>
                        <div class="col-md-3 col-lg-4 form-group hidden">
                            <?php echo form_dropdown('type', ['' => 'Please Select', 'PURC' => 'Invoice', 'PYMT' => 'Direct'], set_value('type', ''), 'id="purchase_entry_type" class="form-control"'); ?>
                            <small id="required_purchase_entry_type" style="color:red; display:none;">Type is required field</small>
                        </div>

                        <?php echo form_label('Supplier', 'supplier_id', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                        <div class="col-md-3 col-lg-4 form-group">
                            <input style="width:75%; display: inline;" type="text" class="form-control" id="purchase_entry_supplier_name">
                            <a style="width:20%; font-size: 25px;height: 35px;" class="supplier_select_button btn btn-md gray">...</a>
                            <div class="hidden" id="purchase_entry_supplier_id"></div>
                        </div>

                        <?php echo form_label('Document Date <span style="color:red;" class="required">*</span>', 'document_date', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                        <div class="col-md-3 col-lg-4 form-group">
                            <input id="purchase_entry_document_date" value="" name="starting_date" type="date" class="form-control">
                            <small id="required_purchase_entry_document_date" style="color:red; display:none;">Document Date is required field</small>
                            <small id="lock_purchase_entry_document_date" style="color:red; display:none;">Document Date is in locked period</small>
                        </div>

                        <?php echo form_label('Due Date', 'due_date', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                        <div class="col-md-3 col-lg-4 form-group">
                            <input id="purchase_entry_due_date" value="" name="starting_date" type="date" class="form-control">
                        </div>

                        <?php echo form_label('Reference No <span style="color:red;" class="required">*</span>', 'refference_no', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                        <div class="col-md-3 col-lg-4 form-group">
                            <input style="width:75%" class="form-control" type="text" id="purchase_entry_refference_no">
                            <small id="required_purchase_entry_refference_no" style="color:red; display:none;">Reference No is required field</small>
                        </div>
                    </div>

                    <table style="margin-top: 20px;" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr role="row" class="heading">
                            <th width="11%">Transaction Account</th>
                            <th width="3%">Amount</th>
                            <th width="2%">Tax</th>
                            <th width="2%">Line Total</th>
                            <th width="7%">Outlets</th>
                            <th width="5%"></th>
                        </tr>
                        </thead>
                        <tbody id="modal-edit-table">
                        </tbody>
                        <tbody>
                        <tr role="row" class="heading">
                            <td style="cursor: pointer;  font-size:14px;" colspan="9" id="purchase_transaction_add" class="text-center"><button type="button" style="color:black;background-color:#f9f9f9" class="btn btn-md"><i class="fa fa-plus"></i> Add New</button></td>
                        </tr>
                        <tr role="row" class="heading hidden">
                            <td style="border-right: none;"><a id="purchase_transaction-mix" class="display-hide btn btn-md blue"><i class="fa fa-pencil"></i></a></td>
                            <td colspan="4" class="text-right" style="font-size:14px;">Total Amount</td>
                            <td style="border-right: 1px solid #ddd; font-size:14px;" id="purchase_transaction_total_tax"></td>
                            <td style="border-right: 1px solid #ddd; font-size:14px;" id="purchase_transaction_total_amount"></td>
                        </tr>
                        </tbody>
                    </table>

                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_id">
                <input type="hidden" name="id" id="payment_GL_transaction_id">
                <button  type="button" class="btn btn-md blue" id="purchase_entry_submit">Submit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<div class="modal" id="purchase_entry_project_select-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Select Outlet</h4>
            </div>
            <div class="modal-body">

                <div id="show_error_message_project" class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span id="error_message_project">Financial Period is updated successfully</span>
                </div>

                <?php echo form_label('Total Transaction Amount', 'description', array('class' => 'col-md-5 col-lg-4 control-label', 'style' => 'font-size:14px;')); ?>

                <div style="font-size: 14px;" id="GL_transaction_total" class="col-md-7 col-lg-8 form-group"></div>

                <table style="margin-top: 20px;" id="table_select_project" class="tree table table-striped table-bordered table-hover">
                    <thead>
                    <tr role="row" class="heading">
                        <th width="15%">Outlet Name</th>
                        <th width="10%">Percentage</th>
                        <th width="10%">Amount</th>
                    </tr>
                    </thead>
                    <tbody id="purchase_project_table">
                    <?php for ($i = 0; $i < count($project_list); $i++) {
                        ?>
                        <tr role="row">
                            <td class="purchase_project_name"><?php echo $project_list[$i]['name'].' ('.$project_list[$i]['description'].')'; ?></td>
                            <td class="trans_id hidden"></td>
                            <td class="tax_id hidden"></td>
                            <td class="project_id hidden"><?php echo $project_list[$i]['id']; ?></td>
                            <td><input style="width:80%; display: inline;" type="text" class="purchase_project_percentage form-control"> % </td>
                            <td><input style="width:80%; display: inline;" type="text" class="purchase_project_amount form-control"></td>
                            <td class="hidden"><input style="width:80%" type="text" class="trans_purchase_project_amount"></td>
                            <td class="hidden"><input style="width:80%" type="text" class="tax_purchase_project_amount"></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                    <tbody>
                    <tr role="row">
                        <td class="text-right">Total</td>
                        <td id="purchase_project_total_percentage"></td>
                        <td id="purchase_project_total_amount"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button  type="button" class="btn btn-md blue" id="purchase_project_submit">Submit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="purchase_transaction_edit-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Edit Credit Note</h4>
            </div>
            <div class="modal-body">

                <div id="show_error_message_transaction" class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span id="error_message_transaction">Financial Period is updated successfully</span>
                </div>

                <table style="margin-top: 20px;" id="table_transaction_edit" class="tree table table-striped table-bordered table-hover">
                    <thead>
                    <tr role="row" class="heading">
                        <th width="15%">Transaction Account <span style="color:red;" class="required">*</span></th>
                        <th width="15%">Description</th>
                        <th width="15%">Amount <span style="color:red;" class="required">*</span></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr role="row">
                        <td class="ui-widget">
                            <input style="width:60%; display:inline;" data-toggle="tooltip" data-original-title="" type="text" class="purchase_transaction_tax_discount_account form-control" id="purchase_transaction_trans_account">
                            <a style="width:20%; font-size:25px;height: 35px;" class="purchase_transaction_account_tax_discount_button btn btn-md gray">...</a>
                            <small id="required_purchase_transaction_trans_account" style="color:red; display:none;">Transaction Account is required field</small>
                            <small id="required_purchase_transaction_account_code" style="color:red; display:none;">Account Code does not exist or cannot be selected</small>
                        </td>
                        <td id="purchase_transaction_id" class="hidden"></td>
                        <td class="hidden">
                            <input style="width:80%" class="form-control" type="text" id="purchase_transaction_quantity" value="1">
                            <small id="required_purchase_transaction_quantity" style="color:red; display:none;">Transaction Quantity is required field</small>
                        </td>
                        <td>
                            <input style="width:80%" class="form-control" type="text" id="purchase_transaction_description">
                        </td>
                        <td>
                            <input style="width:100%" type="text" class="form-control" id="purchase_transaction_unit_cost">
                            <small id="required_purchase_transaction_unit_cost" style="color:red; display:none;">Cost is required field</small>
                        </td>
                        <td class="hidden" id="purchase_transaction_base_amount"></td>
                    </tr>
                    <tr role="row" class="heading">
                        <th width="15%">Tax Account</th>
                        <th width="10%">Tax Rate</th>
                        <th width="10%">Tax Amount</th>
                    </tr>
                    <tr role="row">
                        <td>
                            <input readonly data-toggle="tooltip" data-original-title="" style="width:60%; display:inline;" type="text" class="purchase_transaction_tax_discount_account form-control" id="purchase_transaction_tax_account">
                            <?php if ($tax_account != '60621') { ?>
                            <select style="width: 35%; display:inline;" class="form-control" id="purchase_transaction_tax_type">
                                <option value="TX7">TX7</option>
                                <option value="NR">NR</option>
                                <option value="NT">NT</option>
                                <option value="ZP">ZP</option>
                                <option value="IM">IM</option>
                                <option value="IM7">IM7</option>
                                <option value="EP">EP</option>
                                <option value="OP">OP</option>
                            </select>
                            <?php } ?>
                        </td>
                        <td>
                            <input style="width:80%; display:inline;" type="text" id="purchase_transaction_tax_rate" class="form-control"> %
                            <small id="required_purchase_transaction_tax_rate" style="color:red; display:none;">Tax Rate should be less than 100%</small>
                        </td>
                        <td>
                            <input type="text" id="purchase_transaction_tax_amount" class="form-control">
                        </td>
                    </tr>
                    <tr role="row">
                        <th class="text-right" colspan="2">Line Total</th>
                        <th id="purchase_transaction_item_amount"></th>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button  type="button" class="btn btn-md blue" id="purchase_transaction_edit_add">Add</button>
                <button  type="button" class="btn btn-md blue" id="purchase_transaction_edit_edit">Edit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="purchase_entry_account_select-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Select Account</h4>
            </div>
            <div class="modal-body">
                <table style="margin-top: 20px;" class="tree table table-striped table-bordered table-hover">
                    <thead>
                    <tr role="row" class="heading">
                        <th width="25%">Account Name</th>
                        <th width="8%">Account Type</th>
                        <th width="8%">Account Code</th>
                        <th width="8%">Action</th>
                    </tr>
                    </thead>
                    <tbody id="accounts_list">
                    <?php for ($i = 0; $i < count($accounts); $i++) {
                        ?>
                        <tr role="row" class="treegrid-<?php echo $accounts[$i]['id'] ?> <?php if (!empty($accounts[$i]['father_id'])) {echo 'treegrid-parent-'.$accounts[$i]['father_id'];} ?> heading">
                            <td><?php echo $accounts[$i]['name']; ?></td>
                            <td class="modal_account_name hidden"><?php echo $accounts[$i]['name']; ?></td>
                            <td class="modal_account_type"><?php echo $accounts[$i]['type']; ?></td>
                            <td class="modal_account_code"><?php echo $accounts[$i]['code']; ?></td>
                            <?php if (empty($accounts[$i]['MC_id'])) { ?>
                                <td><button type="button" class="choose_account btn btn-md grey-salsa">Choose This Account</button></td>
                            <?php } else { ?>
                                <td></td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="purchase_entry_select-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Select Purchase Entry</h4>
            </div>
            <div class="modal-body">
                <div id="show_error_message_purchase_entry_select" class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span id="error_message_purchase_entry_select">Financial Period is updated successfully</span>
                </div>
                <div class="form-group">
                    <label for="description" class="col-md-3 col-lg-2 control-label">Supplier</label>
                    <div id="apply_purchase-supplier_name" class="col-md-3 col-lg-4 form-group">

                    </div>
                    <label for="description" class="col-md-3 col-lg-2 control-label">Document Date</label>
                    <div id="apply_purchase-document_date" class="col-md-3 col-lg-4 form-group">

                    </div>
                    <label for="description" class="col-md-3 col-lg-2 control-label">Reference No</label>
                    <div id="apply_purchase-refference_no" class="col-md-3 col-lg-4 form-group">

                    </div>
                    <label for="description" class="col-md-3 col-lg-2 control-label">Amount</label>
                    <div id="apply_purchase-amount" class="col-md-3 col-lg-4 form-group"></div>
                    <div id="apply_purchase-main_GL_transaction_id" class="col-md-3 col-lg-4 form-group hidden"></div>
                    <div id="apply_purchase-tax_GL_transaction_id" class="col-md-3 col-lg-4 form-group hidden"></div>
                    <div id="apply_purchase-tax_rate" class="col-md-3 col-lg-4 form-group hidden"></div>
                    <div id="apply_purchase-tax_type" class="col-md-3 col-lg-4 form-group hidden"></div>
                    <div id="apply_purchase-description" class="col-md-3 col-lg-4 form-group hidden"></div>
                    <div id="apply_purchase-id" class="col-md-3 col-lg-4 form-group hidden"></div>
                    <div id="apply_purchase-payable_GL_transaction_id" class="col-md-3 col-lg-4 form-group hidden"></div>
                </div>

                <table style="margin-top: 20px;" class="tree table table-striped table-bordered table-hover">
                    <thead>
                    <tr role="row" class="heading">
                        <th width="8%">Reference No</th>
                        <th width="8%">Document Date</th>
                        <th width="8%">Total Amount</th>
                        <th width="8%">Supplier</th>
                        <th width="8%">Action</th>
                    </tr>
                    </thead>
                    <tbody id="purchase_entry_list">
                    <?php for ($i = 0; $i < count($purchase_entries_selector); $i++) {
                        if ($purchase_entries_selector[$i]['status'] == 'pending') {
                        ?>
                        <tr role="row" class="heading supplier_<?php echo $purchase_entries_selector[$i]['supplier_id'] ?>">
                            <td class="purchase_entry_id hidden"><?php echo $purchase_entries_selector[$i]['id'] ?></td>
                            <td><?php echo $purchase_entries_selector[$i]['refference_no']; ?></td>
                            <td class="purchase_entry_document_date"><?php echo $purchase_entries_selector[$i]['document_date']; ?></td>
                            <td class="purchase_entry_refference_no hidden"><?php echo $purchase_entries_selector[$i]['refference_no']; ?></td>
                            <td class="purchase_entry_total_tax hidden"><?php echo $purchase_entries_selector[$i]['total_tax']; ?></td>
                            <td class="purchase_entry_total_amount"><?php echo number_format($purchase_entries_selector[$i]['total_amount'],2); ?></td>
                            <td class="purchase_entry_supplier_name"><?php echo $purchase_entries_selector[$i]['supplier_name']; ?></td>
                            <td><button type="button" class="choose_purchase_entry btn btn-md grey-salsa">Choose This Purchase</button></td>
                        </tr>
                    <?php }} ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="purchase_entry_bank_in-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Bank In</h4>
            </div>
            <div class="modal-body">
                <div id="show_error_message_purchase_entry_bank_in" class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span id="error_message_purchase_entry_bank_in">Financial Period is updated successfully</span>
                </div>
                <div class="form-group">
                    <label for="description" class="col-md-3 col-lg-2 control-label">Supplier</label>
                    <div id="bank_in-supplier_name" class="col-md-3 col-lg-4 form-group">

                    </div>
                    <label for="description" class="col-md-3 col-lg-2 control-label">Document Date</label>
                    <div id="bank_in-document_date" class="col-md-3 col-lg-4 form-group">

                    </div>
                    <label for="description" class="col-md-3 col-lg-2 control-label">Reference No</label>
                    <div id="bank_in-refference_no" class="col-md-3 col-lg-4 form-group">

                    </div>
                    <label for="description" class="col-md-3 col-lg-2 control-label">Amount</label>
                    <div id="bank_in-amount" class="col-md-3 col-lg-4 form-group"></div>
                    <div id="bank_in-id" class="col-md-3 col-lg-4 form-group hidden"></div>
                    <div id="bank_in-payable_GL_transaction_id" class="col-md-3 col-lg-4 form-group hidden"></div>
                </div>

                <?php echo form_label('Pay To <span style="color:red;" class="required">*</span>', 'payment_method', array('class' => 'col-md-3 col-lg-2 control-label')); ?>

                <div class="col-md-3 col-lg-4 form-group">
                    <input disabled style="width:60%; display:inline;" id="account_code" class="form-control" type="text">
                    <a style="width:20%; font-size: 25px;" id="account_code_button" class="btn btn-md gray">...</a>
                </div>

                <?php echo form_label('Pay Date <span style="color:red;" class="required">*</span>', 'pay_date', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                <div class="col-md-3 col-lg-4 form-group" style="margin-bottom: 30px;">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd"
                         data-date-viewmode="years">
                        <input id="pay_invoice_date" value="" name="starting_date" type="text" class="form-control">

                        <div class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </div>
                    </div>
                    <small id="required_pay_date" style="color:red; display:none;">Pay Date is required field</small>
                </div>

                <?php echo form_label('', 'payment_method', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                <div class="col-md-3 col-lg-4 form-group">
                    <input disabled style="width:60%" id="account_name" class="form-control" type="text">
                    <small id="required_account_name" style="color:red; display:none;">Please select Account to pay</small>
                </div>
                <div class="hidden" id="bank_account_id"></div>

                <?php echo form_label('Reference No <span style="color:red;" class="required">*</span>', 'ref_no', array('class' => 'col-md-3 col-lg-2 control-label')); ?>

                <div class="col-md-3 col-lg-4 form-group">
                    <input class="form-control" id="pay_invoice_ref_no" style="width:60%" type="text">
                    <small id="required_ref_no" style="color:red; display:none;">Reference No is required field</small>
                </div>

                <table style="margin-top: 20px;" class="tree table table-striped table-bordered table-hover">

                </table>
            </div>

            <div class="modal-footer">
                <button  type="button" class="btn btn-md blue" id="bank_in_submit">Submit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="select_account-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Select Account</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-3 col-lg-4 form-group">
                    <input type="text" name="keyword" class="form-control" value="<?php echo $search['keyword'] ?>" placeholder="Key Word">
                </div>
                <table style="margin-top: 20px;" class="tree table table-striped table-bordered table-hover">
                    <thead>
                    <tr role="row" class="heading">
                        <th width="25%">Account Name</th>
                        <th width="8%">Account Type</th>
                        <th width="8%">Account Code</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php for ($i = 0; $i < count($bank_accounts); $i++) {
                        ?>
                        <tr role="row" class="choose_account">
                            <td class="modal_account_id hidden"><?php echo $bank_accounts[$i]['id']; ?></td>
                            <td class="modal_account_name"><?php echo $bank_accounts[$i]['name']; ?><?php echo !empty($bank_accounts[$i]['number']) ? ' ('.$bank_accounts[$i]['number'].')' : ''; ?></td>
                            <td><?php echo $bank_accounts[$i]['type']; ?></td>
                            <td class="modal_account_code"><?php echo $bank_accounts[$i]['account_code']; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="purchase_entry_supplier_select-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Select Supplier</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-3 col-lg-4 form-group">
                    <input type="text" name="keyword" class="form-control" value="<?php echo $search['keyword'] ?>" placeholder="Key Word">
                </div>
                <table style="margin-top: 20px;" class="tree table table-striped table-bordered table-hover">
                    <thead>
                    <tr role="row" class="heading">
                        <th width="25%">Supplier Name</th>
                        <th width="8%">Supplier Address</th>
                        <th width="8%">Supplier Email</th>
                        <th width="8%">Supplier Mobile</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php for ($i = 0; $i < count($suppliers_list); $i++) {
                        ?>
                        <tr role="row" class="choose_supplier">
                            <td class="modal_supplier_name"><?php echo $suppliers_list[$i]['name']; ?></td>
                            <td class="modal_supplier_id hidden"><?php echo $suppliers_list[$i]['id']; ?></td>
                            <td><?php echo $suppliers_list[$i]['address']; ?></td>
                            <td><?php echo $suppliers_list[$i]['email']; ?></td>
                            <td><?php echo $suppliers_list[$i]['mobile']; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="purchase_entry-delete-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Delete Credit Note</h4>
            </div>
            <div class="modal-body">

                <p class="text-center">Can you confirm to delete Credit Note</p>

                <?php echo form_open_multipart(base_url('credit_note/delete'), 'class="form-horizontal" method="post" data-parsley-validate'); ?>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_delete_id">
                <button  type="submit" class="btn btn-md red">Delete</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<div class="modal" id="purchase_entry-undo-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Undo Credit Note</h4>
            </div>
            <div class="modal-body">

                <p class="text-center">Can you confirm to undo applying purchase?</p>

                <?php echo form_open_multipart(base_url('credit_note/undo'), 'class="form-horizontal" method="post" data-parsley-validate'); ?>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_undo_id">
                <button  type="submit" class="btn btn-md yellow">Undo</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<div class="modal" id="purchase_entry-undo_bank_in-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Undo Credit Note</h4>
            </div>
            <div class="modal-body">

                <p class="text-center">Can you confirm to undo bank in?</p>

                <?php echo form_open_multipart(base_url('credit_note/undo_bank_in'), 'class="form-horizontal" method="post" data-parsley-validate'); ?>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_undo_bank_in_id">
                <button  type="submit" class="btn btn-md yellow">Undo</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<div class="modal" id="confirm_exit_edit-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Close confirm Credit Note</h4>
            </div>
            <div class="modal-body">

                <p class="text-center">Can you confirm to close this Credit Note?</p>

            </div>
            <div class="modal-footer">
                <button id="confirm_exit" type="submit" class="btn btn-md blue" data-dismiss="modal">Confirm</button>
                <button id="cancel_exit"  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {

        var locked_financial_month = '<?php echo $locked_next_financial_month; ?>';

        var default_project_id = '<?php echo $default_project_id; ?>';
        var default_project_name = '<?php echo $default_project_name; ?>';

        var document_date_default = '<?php echo $document_date_default; ?>';

        $("#bank_in_submit").on("click",function(){

            var account_code = $('#account_code').val();
            var bank_account_id = $('#bank_account_id').html();
            var pay_date = $('#pay_invoice_date').val();
            var ref_no = $('#pay_invoice_ref_no').val();

            var credit_note_id = $('#bank_in-id').html();

            var payable_GL_transaction_id = $('#bank_in-payable_GL_transaction_id').html();

            var credit_note_amount = $('#bank_in-amount').html();

            var error_message = false;

            if (bank_account_id == '') {
                $('#account_code_button').css('color', 'red');
                $('#account_code').css('border-color', 'red');
                $('#account_name').css('border-color', 'red');

                $('#required_account_name').show();
                error_message = true;
            } else {
                $('#account_code_button').css('color', '#428bca');
                $('#account_code').css('border-color', '#e5e5e5');
                $('#account_name').css('border-color', '#e5e5e5');

                $('#required_account_name').hide();
            }

            if (pay_date == '') {
                $('#pay_invoice_date').css('border-color', 'red');
                $('#pay_invoice_date').focus();
                $('#required_pay_date').show();

                error_message = true;
            } else {
                $('#pay_invoice_date').css('border-color', '#e5e5e5');

                $('#required_pay_date').hide();
            }

            if (ref_no == '') {
                $('#pay_invoice_ref_no').css('border-color', 'red');
                $('#pay_invoice_ref_no').focus();
                $('#required_ref_no').show();
                error_message = true;
            } else {
                $('#pay_invoice_ref_no').css('border-color', '#e5e5e5');

                $('#required_ref_no').hide();
            }

            if (error_message == true) {
                return;
            }

            $.ajax({
                method: 'POST',
                url: "<?php echo base_url(); ?>credit_note/bank_in",
                data: {
                    account_code: account_code,
                    bank_account_id: bank_account_id,
                    pay_date: pay_date,
                    ref_no: ref_no,
                    credit_note_id: credit_note_id,
                    amount: parseFloat(credit_note_amount.replace(/,/g, "")),
                    payable_GL_transaction_id: payable_GL_transaction_id
                },
                dataType: 'json'
            }).done(function() {
                if (res.success == true) {
                    window.location.reload();
                }
            });
        });

        $("#account_code_button").on("click",function(){
            var el1 = $(this);
            $('#select_account-modal').modal({
                show: true,
                backdrop: 'static'
            });

            var modal_account_code = '';
            var modal_account_name = '';
            var modal_account_id = '';
            $(".choose_account").on("click",function(){
                var el = $(this);
                modal_account_code = el.closest('tr').find('.modal_account_code').html();
                modal_account_name = el.closest('tr').find('.modal_account_name').html();
                modal_account_id = el.closest('tr').find('.modal_account_id').html();
                $('#select_account-modal').modal('hide');

                $('#account_code').val(modal_account_code);
                $('#account_name').val(modal_account_name);
                $('#bank_account_id').html(modal_account_id);

            });
        });


        $('#show_error_message_purchase_entry_select').hide();
        $('#show_error_message_purchase_entry_bank_in').hide();

        $('.purchase_entry-bank_in').on('click', function () {

            $('#account_code').val('');
            $('#account_name').val('');
            $('#bank_account_id').html('');

            $('#pay_invoice_date').val('');
            $('#pay_invoice_ref_no').val('');

            var el = $(this);
            var supplier_id = el.closest('tr').find('.purchase_entry_supplier_id').html();
            var purchase_entry_id = el.closest('tr').find('.purchase_entry_id').html();

            var document_date = el.closest('tr').find('.purchase_entry_document_date').html();
            var refference_no = el.closest('tr').find('.purchase_entry_refference_no').html();
            var amount = el.closest('tr').find('.purchase_entry_total_amount').html();
            var supplier_name = el.closest('tr').find('.purchase_entry_supplier_name').html();

            var payable_GL_transaction_id = el.closest('tr').find('.purchase_entry_payable_GL_transaction_id').html();

            $('#bank_in-supplier_name').html(supplier_name);
            $('#bank_in-id').html(purchase_entry_id);
            $('#bank_in-amount').html(amount);
            $('#bank_in-refference_no').html(refference_no);
            $('#bank_in-document_date').html(document_date);

            $('#bank_in-payable_GL_transaction_id').html(payable_GL_transaction_id);

            $('#account_code_button').css('color', '#428bca');
            $('#account_code').css('border-color', '#e5e5e5');
            $('#account_name').css('border-color', '#e5e5e5');

            $('#required_account_name').hide();

            $('#pay_invoice_date').css('border-color', '#e5e5e5');

            $('#required_pay_date').hide();

            $('#pay_invoice_ref_no').css('border-color', '#e5e5e5');

            $('#required_ref_no').hide();

            $('#purchase_entry_bank_in-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $('.purchase_entry-apply_purchase').off("click");
        $('.purchase_entry-apply_purchase').on('click', function () {

            var el = $(this);
            var supplier_id = el.closest('tr').find('.purchase_entry_supplier_id').html();
            var purchase_entry_id = el.closest('tr').find('.purchase_entry_id').html();

            var document_date = el.closest('tr').find('.purchase_entry_document_date').html();
            var refference_no = el.closest('tr').find('.purchase_entry_refference_no').html();
            var amount = el.closest('tr').find('.purchase_entry_total_amount').html();
            var supplier_name = el.closest('tr').find('.purchase_entry_supplier_name').html();

            var main_GL_transaction_id = el.closest('tr').find('.purchase_entry_main_GL_transaction_id').html();
            var tax_GL_transaction_id = el.closest('tr').find('.purchase_entry_tax_GL_transaction_id').html();
            var tax_rate = el.closest('tr').find('.purchase_entry_tax_rate').html();
            var tax_type = el.closest('tr').find('.purchase_entry_tax_type').html();
            var description = el.closest('tr').find('.purchase_entry_description').html();
            var payable_GL_transaction_id = el.closest('tr').find('.purchase_entry_payable_GL_transaction_id').html();

            $('#apply_purchase-supplier_name').html(supplier_name);
            $('#apply_purchase-id').html(purchase_entry_id);
            $('#apply_purchase-amount').html(amount);
            $('#apply_purchase-refference_no').html(refference_no);
            $('#apply_purchase-document_date').html(document_date);

            $('#apply_purchase-main_GL_transaction_id').html(main_GL_transaction_id);
            $('#apply_purchase-tax_GL_transaction_id').html(tax_GL_transaction_id);
            $('#apply_purchase-tax_rate').html(tax_rate);
            $('#apply_purchase-tax_type').html(tax_type);
            $('#apply_purchase-description').html(description);
            $('#apply_purchase-payable_GL_transaction_id').html(payable_GL_transaction_id);

            $('#purchase_entry_list tr').hide();
            $('#purchase_entry_list .supplier_'+supplier_id).show();

            $('#purchase_entry_select-modal').modal({
                show: true,
                backdrop: 'static'
            });

            $('.choose_purchase_entry').off("click");
            $('.choose_purchase_entry').on('click', function () {

                var el1 = $(this);
                var purchase_entry_id = el1.closest('tr').find('.purchase_entry_id').html();
                var purchase_entry_total_amount = el1.closest('tr').find('.purchase_entry_total_amount').html().replace(/,/g, "");
                var purchase_entry_total_tax = el1.closest('tr').find('.purchase_entry_total_tax').html().replace(/,/g, "");

                var amount = $('#apply_purchase-amount').html();
                var GL_transaction_id = $('#apply_purchase-main_GL_transaction_id').html();
                var tax_GL_transaction_id = $('#apply_purchase-tax_GL_transaction_id').html();
                var tax_rate = $('#apply_purchase-tax_rate').html();
                var tax_type = $('#apply_purchase-tax_type').html();
                var description = $('#apply_purchase-refference_no').html();
                var payable_GL_transaction_id = $('#apply_purchase-payable_GL_transaction_id').html();

                var unit_cost1 = parseFloat(amount.replace(/,/g, ""))*100/(parseFloat(tax_rate)+100);

                var credit_note_id = $('#apply_purchase-id').html();

                var error = false;
                $.ajax({
                    method: 'POST',
                    url: "<?php echo base_url(); ?>credit_note/check_purchase",
                    data: {
                        credit_note_id: credit_note_id,
                        purchase_entry_id: purchase_entry_id
                    },
                    dataType: 'json'
                }).done(function(res) {

                    error = res.error;

                    if (res.error == true) {
                        $('#error_message_purchase_entry_select').html('Credit Note Amount should be smaller or equal to Purchase Entry');
                        $('#show_error_message_purchase_entry_select').show();
                        return;
                    } else {
                        $('#show_error_message_purchase_entry_select').hide();

                        $.ajax({
                            method: 'POST',
                            url: "<?php echo base_url(); ?>credit_note/apply_purchase",
                            data: {
                                quantity: 1,
                                unit_cost: -unit_cost1,
                                amount: -parseFloat(amount.replace(/,/g, "")),
                                purchase_entry_total_amount: parseFloat(purchase_entry_total_amount),
                                purchase_entry_total_tax: parseFloat(purchase_entry_total_tax),
                                purchase_entry_id: purchase_entry_id,
                                GL_transaction_id: GL_transaction_id,
                                tax_GL_transaction_id: tax_GL_transaction_id,
                                tax_rate: tax_rate,
                                tax_type: tax_type,
                                description: 'Apply Credit Note for Ref#'+description,
                                payable_GL_transaction_id: payable_GL_transaction_id,
                                credit_note_id: credit_note_id
                            },
                            dataType: 'json'
                        }).done(function(res) {
                            if (res.success == true) {
                                window.location.reload();
                            }
                        });
                    }
                });

                if (error == true) {
                    return;
                }

            });

        });

        var project_selector = '<?php echo $project_selector; ?>';

        $('[data-toggle="tooltip"]').tooltip();

        $('#purchase_transaction_trans_account').on('change', function () {
            var el = $(this);
            if (el.val() != '') {

                $.ajax({
                    method: 'POST',
                    url: "<?php echo base_url(); ?>GL_entry/get_account_name",
                    data: {
                        account_code: el.val()
                    },
                    dataType: 'json'
                }).done(function(res) {
                    el.attr('data-original-title', res);
                });

            } else {
                el.attr('data-original-title', '');
            }
        });

        $('.date-picker').datepicker({
            format: "yyyy-mm-dd"
        });

        $('.date-picker').on('changeDate', function(ev){
            $(this).datepicker('hide');
        });

        $('.date-picker').on('hide.bs.modal', function(ev){
            ev.stopPropogation();
        });

        $(".supplier_select_button").on("click",function(){
            var el1 = $(this);
            $('#purchase_entry_supplier_select-modal').modal({
                show: true,
                backdrop: 'static'
            });

            var modal_supplier_id = '';
            var modal_supplier_name = '';
            $(".choose_supplier").on("click",function(){
                var el = $(this);
                modal_supplier_id = el.closest('tr').find('.modal_supplier_id').html();
                modal_supplier_name = el.closest('tr').find('.modal_supplier_name').html();
                $('#purchase_entry_supplier_select-modal').modal('hide');

                $('#purchase_entry_supplier_id').html(modal_supplier_id);
                $('#purchase_entry_supplier_name').val(modal_supplier_name);
            });
        });

        function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }

        function each_transaction_row() {

            $("#modal-edit-table td").css('line-height', '50px');
            $("#modal-edit-table td").css('font-size', '14px');
            $('.purchase_transaction_transaction_account').css('font-size', '12px');

            $(".purchase_transaction_project_name").on('change', function () {

                var el = $(this);
                var project_name = el.val();
                var project_id = el.closest('tr').find('.purchase_transaction_project').val();
                var unit_cost = el.closest('tr').find('.purchase_transaction_unit_cost').html();
                unit_cost = parseFloat(unit_cost.replace(/,/g, ""));

                var quantity = el.closest('tr').find('.purchase_transaction_quantity').html();
                quantity = parseFloat(quantity.replace(/,/g, ""));

                var tax_amount = el.closest('tr').find('.purchase_transaction_tax_amount').html();
                tax_amount = tax_amount != '' ? parseFloat(tax_amount.replace(/,/g, "")) : '';

                var trans_amount = unit_cost*quantity;

                if (project_name != '') {

                    var trans_project_html = '';

                    trans_project_html += '<div class="project_line">';
                    trans_project_html += '<div class="id"></div>';
                    trans_project_html += '<div class="project_id">' + project_id + '</div>';
                    trans_project_html += '<div class="project_amount">' + trans_amount + '</div>';
                    trans_project_html += '<div class="project_percentage">100</div>';
                    trans_project_html += '</div>';

                    el.closest('td').find('.project_data_trans').html(trans_project_html);

                    if (tax_amount != '') {
                        var tax_project_html = '';

                        tax_project_html += '<div class="project_line">';
                        tax_project_html += '<div class="id"></div>';
                        tax_project_html += '<div class="project_id">' + project_id + '</div>';
                        tax_project_html += '<div class="project_amount">' + tax_amount + '</div>';
                        tax_project_html += '<div class="project_percentage">100</div>';
                        tax_project_html += '</div>';

                        el.closest('td').find('.project_data_tax').html(tax_project_html);
                    }
                } else {
                    el.closest('td').find('.project_data_trans').html('');
                    el.closest('td').find('.project_data_tax').html('');
                    el.closest('tr').find('.purchase_transaction_project').val('');
                }
            });

            $(".purchase_transaction-edit").on("click",function(){

                clear_transaction_modal();

                $('#purchase_transaction_edit-modal h4.modal-title').html('Edit Credit Note');

                var el = $(this);
                el.closest('tr').addClass('purchase_transaction-edit_tr');
                var purchase_transaction_trans_account_code = el.closest('tr').find('.purchase_transaction_trans_account_code').html();
                var purchase_transaction_tax_account_code = el.closest('tr').find('.purchase_transaction_tax_account_code').html();

                var purchase_transaction_trans_account_name = el.closest('tr').find('.purchase_transaction_trans_account_name').html();
                var purchase_transaction_tax_account_name = el.closest('tr').find('.purchase_transaction_tax_account_name').html();

                var purchase_transaction_tax_rate = el.closest('tr').find('.purchase_transaction_tax_rate').html();
                var purchase_transaction_tax_type = el.closest('tr').find('.purchase_transaction_tax_type').html();

                if (purchase_transaction_tax_type == 'TX7' || purchase_transaction_tax_type == 'IM7') {
                    $("#purchase_transaction_tax_rate").prop('disabled', true);
                    $("#purchase_transaction_tax_amount").prop('disabled', false);
                }

                var purchase_transaction_unit_cost = el.closest('tr').find('.purchase_transaction_unit_cost').html();
                var purchase_transaction_description = el.closest('tr').find('.purchase_transaction_description').html();
                var purchase_transaction_quantity = el.closest('tr').find('.purchase_transaction_quantity').html();
                var purchase_transaction_tax_amount = el.closest('tr').find('.purchase_transaction_tax_amount').html();
                var purchase_transaction_item_amount = el.closest('tr').find('.purchase_transaction_item_amount').html();
                var purchase_transaction_base_amount = parseFloat(purchase_transaction_unit_cost.replace(/,/g, "")) * parseFloat(purchase_transaction_quantity.replace(/,/g, ""));

                $('#purchase_transaction_unit_cost').val(purchase_transaction_unit_cost);
                $('#purchase_transaction_description').val(purchase_transaction_description);
                $('#purchase_transaction_quantity').val(purchase_transaction_quantity);
                $('#purchase_transaction_trans_account').val(purchase_transaction_trans_account_code);
                $('#purchase_transaction_trans_account').attr('data-original-title', purchase_transaction_trans_account_name.replace(/&amp;/g, "&"));

                //$('#purchase_transaction_tax_account').val(purchase_transaction_tax_account_code);
                $('#purchase_transaction_tax_rate').val(purchase_transaction_tax_rate);
                if (purchase_transaction_tax_type != '') {
                    $('#purchase_transaction_tax_type').val(purchase_transaction_tax_type);
                }
                
                $('#purchase_transaction_tax_amount').val(purchase_transaction_tax_amount);
                $('#purchase_transaction_item_amount').html(purchase_transaction_item_amount);
                $('#purchase_transaction_base_amount').html(numberWithCommas(purchase_transaction_base_amount.toFixed(2)));

                $('#purchase_transaction_edit-modal').modal({
                    show: true,
                    backdrop: 'static'
                });
                $('#purchase_transaction_edit_edit').show();
                $('#purchase_transaction_edit_add').hide();

                $("#purchase_transaction_edit_edit").on("click",function(){

                    var error_message = false;

                    if ($('#purchase_transaction_trans_account').val() == '') {

                        $('#purchase_transaction_trans_account').css('border-color', 'red');
                        $('.purchase_transaction_account_tax_discount_button').css('color', 'red');
                        $('#required_purchase_transaction_trans_account').show();
                        $('#purchase_transaction_trans_account').focus();

                        error_message = true;
                    } else {
                        $('#purchase_transaction_trans_account').css('border-color', '#e5e5e5');
                        $('.purchase_transaction_account_tax_discount_button').css('color', '#428bca');
                        $('#required_purchase_transaction_trans_account').hide();
                    }

                    if ($('#purchase_transaction_quantity').val() == '') {

                        $('#purchase_transaction_quantity').css('border-color', 'red');
                        $('#required_purchase_transaction_quantity').show();
                        $('#purchase_transaction_quantity').focus();

                        error_message = true;
                    } else {
                        $('#purchase_transaction_quantity').css('border-color', '#e5e5e5');
                        $('#required_purchase_transaction_quantity').hide();
                    }

                    if ($('#purchase_transaction_unit_cost').val() == '') {

                        $('#purchase_transaction_unit_cost').css('border-color', 'red');
                        $('#required_purchase_transaction_unit_cost').show();
                        $('#purchase_transaction_unit_cost').focus();

                        error_message = true;
                    } else {
                        $('#purchase_transaction_unit_cost').css('border-color', '#e5e5e5');
                        $('#required_purchase_transaction_unit_cost').hide();
                    }

                    if (parseFloat($('#purchase_transaction_tax_rate').val()) > 100) {
                        $('#purchase_transaction_tax_rate').css('border-color', 'red');
                        $('#required_purchase_transaction_tax_rate').show();
                        $('#purchase_transaction_tax_rate').focus();
                        error_message = true;
                    } else {
                        $('#purchase_transaction_tax_rate').css('border-color', '#e5e5e5');
                        $('#required_purchase_transaction_tax_rate').hide();
                    }

                    if (error_message == true) {
                        return;
                    }

                    var account_codes = [];
                    account_codes.push($('#purchase_transaction_trans_account').val());
                    account_codes.push($('#purchase_transaction_tax_account').val());

                    $.ajax({
                        method: 'POST',
                        url: "<?php echo base_url(); ?>credit_note/check_edit",
                        data: {
                            account_codes: account_codes
                        },
                        dataType: 'json'
                    }).done(function(res) {
                        if (res.error != false) {
                            $('#purchase_transaction_trans_account').css('border-color', 'red');
                            $('.purchase_transaction_account_tax_discount_button').css('color', 'red');
                            $('#required_purchase_transaction_account_code').show();

                            error_message = true;
                            return;
                        } else {

                            $('#purchase_transaction_trans_account').css('border-color', '#e5e5e5');
                            $('.purchase_transaction_account_tax_discount_button').css('color', '#428bca');
                            $('#required_purchase_transaction_account_code').hide();

                            $('.purchase_transaction-edit_tr').find('.purchase_transaction_unit_cost').html($('#purchase_transaction_unit_cost').val());
                            $('.purchase_transaction-edit_tr').find('.purchase_transaction_description').html($('#purchase_transaction_description').val());
                            $('.purchase_transaction-edit_tr').find('.purchase_transaction_quantity').html($('#purchase_transaction_quantity').val());
                            $('.purchase_transaction-edit_tr').find('.purchase_transaction_tax_amount').html($('#purchase_transaction_tax_amount').val());
                            $('.purchase_transaction-edit_tr').find('.purchase_transaction_item_amount').html($('#purchase_transaction_item_amount').html());
                            $('.purchase_transaction-edit_tr').find('.purchase_transaction_trans_account_code').html($('#purchase_transaction_trans_account').val());
                            $('.purchase_transaction-edit_tr').find('.purchase_transaction_trans_account_name').html($('#purchase_transaction_trans_account').attr('data-original-title'));

                            $('.purchase_transaction-edit_tr').find('.purchase_transaction_transaction_account').html($('#purchase_transaction_trans_account').attr('data-original-title')+' ('+$('#purchase_transaction_trans_account').val()+')');

                            $('.purchase_transaction-edit_tr').find('.purchase_transaction_tax_account_code').html($('#purchase_transaction_tax_account').val());
                            $('.purchase_transaction-edit_tr').find('.purchase_transaction_tax_account_name').html($('#purchase_transaction_tax_account').attr('data-original-title'));
                            $('.purchase_transaction-edit_tr').find('.purchase_transaction_tax_rate').html($('#purchase_transaction_tax_rate').val());
                            if ($('#purchase_transaction_tax_type').val() !== undefined) {
                                $('.purchase_transaction-edit_tr').find('.purchase_transaction_tax_type').html($('#purchase_transaction_tax_type').val());
                            }

                            $('.purchase_transaction-edit_tr .project_data_trans').find('.project_line').each(function () {

                                var el3 = $(this);
                                var percentage = parseFloat(el3.find('.project_percentage').html());
                                var unit_cost = $('#purchase_transaction_unit_cost').val();
                                var quantity = $('#purchase_transaction_quantity').val();
                                var tax_amount = parseFloat($('#purchase_transaction_tax_amount').val().replace(/,/g, ""));
                                var base_amount = parseFloat(quantity.replace(/,/g, ""))*parseFloat(unit_cost.replace(/,/g, ""));
                                var project_base_amount = base_amount*percentage/100;
                                el3.find('.project_amount').html(project_base_amount.toFixed(2));

                            });

                            if ($('#purchase_transaction_tax_amount').val() != '') {
                                if ($('.purchase_transaction-edit_tr .project_data_tax').html() != '') {
                                    $('.purchase_transaction-edit_tr .project_data_tax').find('.project_line').each(function () {

                                        var el3 = $(this);
                                        var percentage = parseFloat(el3.find('.project_percentage').html());
                                        var tax_amount = parseFloat($('#purchase_transaction_tax_amount').val().replace(/,/g, ""));
                                        var tax_project = tax_amount*percentage/100;
                                        el3.find('.project_amount').html(tax_project.toFixed(2));

                                    });
                                } else {
                                    $('.purchase_transaction-edit_tr .project_data_trans').find('.project_line').each(function () {

                                        var el3 = $(this);
                                        var percentage = parseFloat(el3.find('.project_percentage').html());
                                        var project_id = el3.find('.project_id').html();
                                        var tax_amount = parseFloat($('#purchase_transaction_tax_amount').val().replace(/,/g, ""));
                                        var tax_project = tax_amount*percentage/100;

                                        var html_project_tax = '';
                                        html_project_tax += '<div class="project_line">';
                                        html_project_tax += '<div class="id"></div>';
                                        html_project_tax += '<div class="project_id">'+project_id+'</div>';
                                        html_project_tax += '<div class="project_amount">'+tax_project.toFixed(2)+'</div>';
                                        html_project_tax += '<div class="project_percentage">'+percentage+'</div>';
                                        html_project_tax += '</div>';
                                        $('.purchase_transaction-edit_tr .project_data_tax').append(html_project_tax);

                                    });
                                }
                            } else {
                                $('.purchase_transaction-edit_tr .project_data_tax').html('');
                            }

                            $('#purchase_transaction_edit-modal').modal('hide');

                            calculate_total();
                        }
                    });
                    if (error_message == true) {
                        return;
                    }
                });
            });

            $('#purchase_transaction_edit-modal').on('hidden.bs.modal', function () {
                setTimeout(function(){
                    $('.purchase_transaction-edit_tr').removeClass('purchase_transaction-edit_tr');
                }, 500);
            });

            $(".purchase_transaction_project_split").on("click",function(){
                clear_project_modal();

                var el1 = $(this);

                var project_total_amount = el1.closest('tr').find('.purchase_transaction_item_amount').html();
                var tax_project_total_amount = el1.closest('tr').find('.purchase_transaction_tax_amount').html() == '' ? '0' : el1.closest('tr').find('.purchase_transaction_tax_amount').html();

                $('#GL_transaction_total').html(project_total_amount);

                $('#purchase_entry_project_select-modal').modal({
                    show: true,
                    backdrop: 'static'
                });
                el1.closest('td').addClass('project_chosen_td');

                $('.project_chosen_td .project_data_trans .project_line').each(function () {
                    var el2 = $(this);
                    var project_id = el2.find('.project_id').html();
                    var id = el2.find('.id').html();
                    var project_percentage = el2.find('.project_percentage').html();
                    var trans_amount = el2.find('.project_amount').html();
                    var project_amount = (parseFloat(project_total_amount.replace(/,/g, ""))*parseFloat(project_percentage)/100) - ((parseFloat(project_total_amount.replace(/,/g, "")) - parseFloat(tax_project_total_amount.replace(/,/g, "")))*parseFloat(project_percentage)/100 - parseFloat(trans_amount.replace(/,/g, "")));

                    $('#purchase_project_table tr').each(function () {
                        var el3 = $(this);
                        var box_project_id = el3.find('.project_id').html();

                        if (box_project_id == project_id) {
                            el3.find('.purchase_project_percentage').val(numberWithCommas(project_percentage));
                            el3.find('.purchase_project_amount').val(numberWithCommas(project_amount));
                            el3.find('.trans_id').html(id);
                            el3.find('.trans_purchase_project_amount').val(trans_amount);
                        }
                    });

                });

                $('.project_chosen_td .project_data_tax .project_line').each(function () {
                    var el2 = $(this);
                    var project_id = el2.find('.project_id').html();
                    var id = el2.find('.id').html();
                    var project_amount = el2.find('.project_amount').html();

                    $('#purchase_project_table tr').each(function () {
                        var el3 = $(this);
                        var box_project_id = el3.find('.project_id').html();

                        if (box_project_id == project_id) {
                            el3.find('.tax_purchase_project_amount').val(project_amount);
                            el3.find('.tax_id').html(id);
                        }
                    });
                });

                calculate_total_project();

                $(".purchase_project_percentage").on('change', function () {
                    $('#show_error_message_project').hide();
                    var el2 = $(this);
                    var percent = el2.val();
                    var amount = parseFloat(project_total_amount.replace(/,/g, "")) * parseFloat(percent)/100;
                    var tax_amount = parseFloat(tax_project_total_amount.replace(/,/g, "")) * parseFloat(percent)/100;
                    var trans_amount = amount - tax_amount;

                    if (percent != '') {
                        el2.closest('tr').find('.purchase_project_amount').val(numberWithCommas(amount.toFixed(2)));
                        el2.closest('tr').find('.trans_purchase_project_amount').val(trans_amount.toFixed(2));
                        el2.closest('tr').find('.tax_purchase_project_amount').val(tax_amount.toFixed(2));
                    } else {
                        el2.closest('tr').find('.purchase_project_amount').val('');
                        el2.closest('tr').find('.trans_purchase_project_amount').val('');
                        el2.closest('tr').find('.tax_purchase_project_amount').val('');
                    }

                    calculate_total_project();
                });

                $(".purchase_project_amount").on('change', function () {
                    $('#show_error_message_project').hide();
                    var el3 = $(this);
                    var amount = el3.val().replace(/,/g, "");
                    var percent = parseFloat(amount)/parseFloat(project_total_amount.replace(/,/g, "")) *100;
                    var tax_amount = parseFloat(tax_project_total_amount.replace(/,/g, "")) * parseFloat(percent)/100;
                    var trans_amount = amount - tax_amount;

                    if (el3.val() != '') {
                        el3.closest('tr').find('.purchase_project_percentage').val(numberWithCommas(percent.toFixed(2)));
                        el3.val(numberWithCommas(parseFloat(amount).toFixed(2)));
                        el3.closest('tr').find('.trans_purchase_project_amount').val(trans_amount.toFixed(2));
                        el3.closest('tr').find('.tax_purchase_project_amount').val(tax_amount.toFixed(2));
                    } else {
                        el3.closest('tr').find('.purchase_project_percentage').val('');
                        el3.closest('tr').find('.trans_purchase_project_amount').val('');
                        el3.closest('tr').find('.tax_purchase_project_amount').val('');
                    }

                    calculate_total_project();
                });

                $("#purchase_project_submit").on('click', function () {

                    var trans_amount = $('.project_chosen_td').closest('tr').find('.purchase_transaction_item_amount').html();

                    if (parseFloat($("#purchase_project_total_amount").html().replace(/,/g, "")) == parseFloat(trans_amount.replace(/,/g, ""))) {

                        $('#GL_transaction_total').css('color', 'black');
                        $('#purchase_project_total_amount').css('color', 'black');

                        $('.project_chosen_td .project_data_trans').html('');
                        $('.project_chosen_td .project_data_tax').html('');

                        $('#purchase_project_table tr').each(function () {

                            var el1 = $(this);
                            var percentage = el1.find('.purchase_project_percentage').val();
                            var trans_amount = el1.find('.trans_purchase_project_amount').val();
                            var project_id  = el1.find('.project_id').html();
                            var project_name  = el1.find('.purchase_project_name').html();
                            var trans_id  = el1.find('.trans_id').html();
                            var tax_id  = el1.find('.tax_id').html();
                            var tax_amount = el1.find('.tax_purchase_project_amount').val();
                            var amount = el1.find('.purchase_project_amount').val();

                            if (percentage != '' && amount != '') {

                                if (parseFloat(percentage) == 100) {
                                    $('.project_chosen_td .purchase_transaction_project').val(project_id);
                                    $('.project_chosen_td .purchase_transaction_project_name').val(project_name);
                                } else {
                                    $('.project_chosen_td .purchase_transaction_project').val('');
                                    $('.project_chosen_td .purchase_transaction_project_name').val('');
                                }

                                var trans_project_html = '';

                                trans_project_html += '<div class="project_line">';
                                trans_project_html += '<div class="id">'+trans_id+'</div>';
                                trans_project_html += '<div class="project_id">' + project_id + '</div>';
                                trans_project_html += '<div class="project_amount">' + trans_amount + '</div>';
                                trans_project_html += '<div class="project_percentage">' + percentage.replace(/,/g, "") + '</div>';
                                trans_project_html += '</div>';

                                $('.project_chosen_td .project_data_trans').append(trans_project_html);

                                var tax_project_html = '';

                                if (tax_amount != '0.00') {
                                    tax_project_html += '<div class="project_line">';
                                    tax_project_html += '<div class="id">' + tax_id + '</div>';
                                    tax_project_html += '<div class="project_id">' + project_id + '</div>';
                                    tax_project_html += '<div class="project_amount">' + tax_amount + '</div>';
                                    tax_project_html += '<div class="project_percentage">' + percentage.replace(/,/g, "") + '</div>';
                                    tax_project_html += '</div>';
                                }

                                $('.project_chosen_td .project_data_tax').append(tax_project_html);
                            }

                            $('#purchase_entry_project_select-modal').modal('hide');
                        });

                    } else {
                        $('#error_message_project').html('Total Amount of outlets should be equal to Transaction Amount');
                        $('#GL_transaction_total').css('color', 'red');
                        $('#purchase_project_total_amount').css('color', 'red');
                        $('#show_error_message_project').show();
                        return;
                    }
                });
            });

            $('#purchase_entry_project_select-modal').on('hidden.bs.modal', function () {
                setTimeout(function(){
                    $('.project_chosen_td').removeClass('project_chosen_td');
                }, 400);
            });
        }

        function calculate_total() {
            var total_amount = 0;
            var total_tax = 0;

            $('#modal-edit-table tr').each(function() {
                var item_amount = $(this).find('.purchase_transaction_item_amount').html();
                var item_tax = $(this).find('.purchase_transaction_tax_amount').html();
                if (item_amount != '') {
                    total_amount += parseFloat(item_amount.replace(/,/g, ""));
                }

                if (item_tax != '') {
                    total_tax += parseFloat(item_tax.replace(/,/g, ""));
                }
            });

            $("#purchase_transaction_total_amount").html(numberWithCommas(total_amount.toFixed(2)));
            $("#purchase_transaction_total_tax").html(numberWithCommas(total_tax.toFixed(2)));
        }

        function calculate_total_project() {
            var total_percent = 0;
            var total_amount = 0;

            $('#purchase_project_table tr').each(function() {
                var percent = $(this).find('.purchase_project_percentage').val();
                if (percent != '') {
                    total_percent += parseFloat(percent.replace(/,/g, ""));
                }

                var amount = $(this).find('.purchase_project_amount').val();
                if (amount != '') {
                    total_amount += parseFloat(amount.replace(/,/g, ""));
                }
            });

            $("#purchase_project_total_percentage").html(numberWithCommas(total_percent.toFixed(2))+'%');

            $("#purchase_project_total_amount").html(numberWithCommas(total_amount.toFixed(2)));
        }

        function clear_project_modal() {

            $('#GL_transaction_total').css('color', 'black');
            $('#purchase_project_total_amount').css('color', 'black');

            $('.purchase_project_percentage').val('');
            $('.purchase_project_amount').val('');
            $('#show_error_message_project').hide();

            $('#purchase_project_total_percentage').html('');
            $('#purchase_project_total_amount').html('');
        }

        function calculate_total_transaction_modal() {

            var purchase_transaction_base_amount = $('#purchase_transaction_base_amount').html() == '' ? '0' : $('#purchase_transaction_base_amount').html();
            var purchase_transaction_tax_amount = $('#purchase_transaction_tax_amount').val() == '' ? '0' : $('#purchase_transaction_tax_amount').val();

            var total_amount = parseFloat(purchase_transaction_base_amount.replace(/,/g, "")) + parseFloat(purchase_transaction_tax_amount.replace(/,/g, ""));
            $("#purchase_transaction_item_amount").html(numberWithCommas(total_amount.toFixed(2)));
        }

        function clear_transaction_modal() {

            $('#purchase_transaction_edit-modal input').val('');
            $('#purchase_transaction_quantity').val('1.00');
            $('#show_error_message_transaction').hide();

            $('#purchase_transaction_base_amount').html('');
            $('#purchase_transaction_tax_amount').val('');
            <?php if ($tax_account != '60621') { ?>
            $('#purchase_transaction_tax_type').val('NR');
            $("#purchase_transaction_tax_rate").prop('disabled', true);
            $("#purchase_transaction_tax_amount").prop('disabled', true);
            <?php } ?>
            $('#purchase_transaction_item_amount').html('');

            var tax_account = "<?php echo $tax_account; ?>";
            var tax_account_name = "<?php echo $tax_account_name; ?>";

            $('#purchase_transaction_tax_account').val(tax_account);

            $('#purchase_transaction_tax_account').attr('data-original-title', tax_account_name.replace(/&amp;/g, "&"));

            $('#purchase_transaction_trans_account').css('border-color', '#e5e5e5');
            $('.purchase_transaction_account_tax_discount_button').css('color', '#428bca');
            $('#required_purchase_transaction_trans_account').hide();

            $('#purchase_transaction_quantity').css('border-color', '#e5e5e5');
            $('#required_purchase_transaction_quantity').hide();

            $('#purchase_transaction_unit_cost').css('border-color', '#e5e5e5');
            $('#required_purchase_transaction_unit_cost').hide();

            $('#purchase_transaction_tax_rate').css('border-color', '#e5e5e5');
            $('#required_purchase_transaction_tax_rate').hide();

            $('#required_purchase_transaction_account_code').hide();
        }
        
        function purchase_transaction_edit_box() {
            $(".purchase_transaction_account_tax_discount_button").on("click",function(){
                var el1 = $(this);
                $('#purchase_entry_account_select-modal').modal({
                    show: true,
                    backdrop: 'static'
                });
                el1.closest('td').addClass('account_chosen_td');

                var modal_account_code = '';
                $(".choose_account").on("click",function(){
                    var el = $(this);
                    modal_account_code = el.closest('tr').find('.modal_account_code').html();
                    var modal_account_name = el.closest('tr').find('.modal_account_name').html();
                    $('#purchase_entry_account_select-modal').modal('hide');

                    $('.account_chosen_td').find('.purchase_transaction_tax_discount_account').val(modal_account_code);
                    $('.account_chosen_td').find('.purchase_transaction_tax_discount_account').attr('data-original-title', modal_account_name.replace(/&amp;/g, "&"));
                });
            });

            $('#purchase_entry_account_select-modal').on('hidden.bs.modal', function () {
                setTimeout(function(){
                    $('.account_chosen_td').removeClass('account_chosen_td');
                }, 500);
            });

            $("#purchase_transaction_tax_type").on("change",function(){
                var el = $(this);
                if (el.val() != 'TX7' && el.val() != 'IM7') {
                    $("#purchase_transaction_tax_rate").val('');
                    $("#purchase_transaction_tax_rate").prop('disabled', true);
                    $("#purchase_transaction_tax_amount").val('');
                    $("#purchase_transaction_tax_amount").prop('disabled', true);
                }

                if (el.val() == 'TX7' || el.val() == 'IM7') {
                    var purchase_transaction_unit_cost = $('#purchase_transaction_unit_cost').val();
                    var purchase_transaction_quantity = $('#purchase_transaction_quantity').val();

                    $("#purchase_transaction_tax_rate").val('7.00');
                    $("#purchase_transaction_tax_rate").prop('disabled', true);

                    if (purchase_transaction_unit_cost != '' && purchase_transaction_quantity != '') {
                        var base_amount = parseFloat(purchase_transaction_unit_cost.replace(/,/g, "")) * parseFloat(purchase_transaction_quantity.replace(/,/g, ""));
                        var tax_amount = (base_amount) * 7 / 100;
                        $("#purchase_transaction_tax_amount").val(numberWithCommas(tax_amount.toFixed(2)));
                    }
                    $("#purchase_transaction_tax_amount").prop('disabled', false);
                }

                if (el.val() == 'IM' || el.val() == 'IM7') {
                    $('#purchase_transaction_tax_account').val('21404');
                } else {
                    $('#purchase_transaction_tax_account').val('21403');
                }

                calculate_total_transaction_modal();
            });

            $("#purchase_transaction_unit_cost, #purchase_transaction_quantity, #purchase_transaction_tax_rate, #purchase_transaction_discount_rate").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

            $("#purchase_transaction_unit_cost, #purchase_transaction_quantity").on("change",function(){

                var el = $(this);
                if (el.val() != '') {
                    var value_this = parseFloat(el.val().replace(/,/g, ""));
                }
                $(this).val(numberWithCommas(value_this.toFixed(2)));
                var purchase_transaction_unit_cost = $('#purchase_transaction_unit_cost').val();
                var purchase_transaction_quantity = $('#purchase_transaction_quantity').val();
                var purchase_transaction_tax_rate = $('#purchase_transaction_tax_rate').val();

                if (purchase_transaction_unit_cost != '' && purchase_transaction_quantity != '') {
                    var base_amount = parseFloat(purchase_transaction_unit_cost.replace(/,/g, "")) * parseFloat(purchase_transaction_quantity.replace(/,/g, ""));
                    $('#purchase_transaction_base_amount').html(numberWithCommas(base_amount.toFixed(2)));

                    if (purchase_transaction_tax_rate != '') {
                        var tax_amount = (base_amount) * parseFloat(purchase_transaction_tax_rate) / 100;
                        $('#purchase_transaction_tax_amount').val(numberWithCommas(tax_amount.toFixed(2)));
                    }
                } else {
                    $('#purchase_transaction_base_amount').html('');
                    $('#purchase_transaction_tax_amount').val('');
                }

                calculate_total_transaction_modal();
            });

            $("#purchase_transaction_tax_rate").on("change",function(){

                var purchase_transaction_base_amount = $('#purchase_transaction_base_amount').html();
                var purchase_transaction_tax_rate = $(this).val();

                if (purchase_transaction_base_amount != '' && purchase_transaction_tax_rate != '' && purchase_transaction_tax_rate != '0') {
                    var change_format = parseFloat(purchase_transaction_tax_rate);
                    $(this).val(change_format.toFixed(2));

                    var tax_amount = parseFloat(purchase_transaction_base_amount.replace(/,/g, "")) * parseFloat(purchase_transaction_tax_rate) / 100;
                    $('#purchase_transaction_tax_amount').val(numberWithCommas(tax_amount.toFixed(2)));
                    calculate_total_transaction_modal();
                }

                if (purchase_transaction_tax_rate == '' || purchase_transaction_tax_rate == '0') {
                    $('#purchase_transaction_tax_amount').val('');
                    $(this).val('');
                    calculate_total_transaction_modal();
                }
            });

            $("#purchase_transaction_tax_amount").on("change",function(){

                var purchase_transaction_base_amount = $('#purchase_transaction_base_amount').html();
                var purchase_transaction_tax_amount = $(this).val();

                if (purchase_transaction_base_amount != '' && purchase_transaction_tax_amount != '' && purchase_transaction_tax_amount != '0') {
                    var el = $(this);
                    var amount = parseFloat(el.val().replace(/,/g, ""));
                    el.val(numberWithCommas(amount.toFixed(2)));

                    var tax_rate = parseFloat(purchase_transaction_tax_amount) / (parseFloat(purchase_transaction_base_amount.replace(/,/g, ""))) * 100;
                    $('#purchase_transaction_tax_rate').val(numberWithCommas(tax_rate.toFixed(2)));
                    calculate_total_transaction_modal();
                }

                if (purchase_transaction_tax_amount == '' || purchase_transaction_tax_amount == '0') {
                    $('#purchase_transaction_tax_rate').val('');
                    $(this).val('');
                    calculate_total_transaction_modal();
                }
            });
        }

        $('#purchase_transaction_edit-modal').on('shown', purchase_transaction_edit_box());

        $("#purchase_transaction_add").on('click', function () {
            clear_transaction_modal();

            var supplier_id = $('#purchase_entry_supplier_id').html();
            if (supplier_id != '') {
                $.ajax({
                    method: 'POST',
                    url: "<?php echo base_url(); ?>credit_note/find_supplier",
                    data: {
                        id: supplier_id
                    },
                    dataType: 'json'
                }).done(function(res) {
                    if (res.default_account_code != null) {
                        $('#purchase_transaction_trans_account').val(res.default_account_code);

                        $.ajax({
                            method: 'POST',
                            url: "<?php echo base_url(); ?>GL_entry/get_account_name",
                            data: {
                                account_code: res.default_account_code
                            },
                            dataType: 'json'
                        }).done(function(res) {
                            $('#purchase_transaction_trans_account').attr('data-original-title', res);
                        });
                    }

                    if (res.default_tax_code != '') {
                        $('#purchase_transaction_tax_type').val(res.default_tax_code);

                        if (res.default_tax_code == 'TX7' || res.default_tax_code == 'IM7') {
                            $('#purchase_transaction_tax_rate').val('7.00');
                            $('#purchase_transaction_tax_amount').prop('disabled', false);
                        }

                        if (res.default_tax_code == 'IM' || res.default_tax_code == 'IM7') {
                            $('#purchase_transaction_tax_account').val('21404');
                        } else {
                            $('#purchase_transaction_tax_account').val('21403');
                        }
                    }

                });
            }

            $('#purchase_transaction_edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
            $('#purchase_transaction_edit_add').show();
            $('#purchase_transaction_edit_edit').hide();
            $('#purchase_transaction_edit-modal h4.modal-title').html('Add Credit Note');
        });

        $("#purchase_transaction_edit_add").on("click",function(){

            $('#purchase_transaction_add').closest('tr').hide();

            var purchase_transaction_item_amount = $('#purchase_transaction_item_amount').html();
            var purchase_transaction_tax_amount = $('#purchase_transaction_tax_amount').val();
            var purchase_transaction_unit_cost = $('#purchase_transaction_unit_cost').val();
            var purchase_transaction_description = $('#purchase_transaction_description').val();
            var purchase_transaction_quantity = $('#purchase_transaction_quantity').val();
            var purchase_transaction_trans_account = $('#purchase_transaction_trans_account').val();
            var purchase_transaction_tax_account = $('#purchase_transaction_tax_account').val();
            var purchase_transaction_trans_account_name = $('#purchase_transaction_trans_account').attr('data-original-title');
            var purchase_transaction_tax_account_name = $('#purchase_transaction_tax_account').attr('data-original-title');
            var purchase_transaction_tax_rate = $('#purchase_transaction_tax_rate').val();
            var purchase_transaction_tax_type = $('#purchase_transaction_tax_type').val();
            purchase_transaction_tax_type = purchase_transaction_tax_type !== undefined ? purchase_transaction_tax_type : '';

            var error_message = false;

            if ($('#purchase_transaction_trans_account').val() == '') {

                $('#purchase_transaction_trans_account').css('border-color', 'red');
                $('.purchase_transaction_account_tax_discount_button').css('color', 'red');
                $('#required_purchase_transaction_trans_account').show();
                $('#purchase_transaction_trans_account').focus();

                error_message = true;
            } else {
                $('#purchase_transaction_trans_account').css('border-color', '#e5e5e5');
                $('.purchase_transaction_account_tax_discount_button').css('color', '#428bca');
                $('#required_purchase_transaction_trans_account').hide();
            }

            if ($('#purchase_transaction_quantity').val() == '') {

                $('#purchase_transaction_quantity').css('border-color', 'red');
                $('#required_purchase_transaction_quantity').show();
                $('#purchase_transaction_quantity').focus();

                error_message = true;
            } else {
                $('#purchase_transaction_quantity').css('border-color', '#e5e5e5');
                $('#required_purchase_transaction_quantity').hide();
            }

            if ($('#purchase_transaction_unit_cost').val() == '') {

                $('#purchase_transaction_unit_cost').css('border-color', 'red');
                $('#required_purchase_transaction_unit_cost').show();
                $('#purchase_transaction_unit_cost').focus();

                error_message = true;
            } else {
                $('#purchase_transaction_unit_cost').css('border-color', '#e5e5e5');
                $('#required_purchase_transaction_unit_cost').hide();
            }

            if (parseFloat($('#purchase_transaction_tax_rate').val()) > 100) {
                $('#purchase_transaction_tax_rate').css('border-color', 'red');
                $('#required_purchase_transaction_tax_rate').show();
                $('#purchase_transaction_tax_rate').focus();
                error_message = true;
            } else {
                $('#purchase_transaction_tax_rate').css('border-color', '#e5e5e5');
                $('#required_purchase_transaction_tax_rate').hide();
            }

            if (error_message == true) {
                return;
            }

            var account_codes = [];
            account_codes.push($('#purchase_transaction_trans_account').val());
            account_codes.push($('#purchase_transaction_tax_account').val());
            $.ajax({
                method: 'POST',
                url: "<?php echo base_url(); ?>credit_note/check_edit",
                data: {
                    account_codes: account_codes
                },
                dataType: 'json'
            }).done(function(res) {
                if (res.error != false) {
                    $('#purchase_transaction_trans_account').css('border-color', 'red');
                    $('.purchase_transaction_account_tax_discount_button').css('color', 'red');
                    $('#required_purchase_transaction_account_code').show();
                    error_message = true;
                    return;
                } else {

                    $('#purchase_transaction_trans_account').css('border-color', '#e5e5e5');
                    $('.purchase_transaction_account_tax_discount_button').css('color', '#428bca');
                    $('#required_purchase_transaction_account_code').hide();

                    var html_project_trans = '';
                    var trans_project_id = '';
                    var trans_project_name = '';
                    var html_project_discount = '';
                    var html_project_tax = '';

                    if (default_project_id != '' && default_project_name != '') {

                        trans_project_id = default_project_id;
                        trans_project_name = default_project_name;

                        html_project_trans += '<div class="project_line">';
                        html_project_trans += '<div class="id"></div>';
                        html_project_trans += '<div class="project_id">' + trans_project_id + '</div>';
                        html_project_trans += '<div class="project_amount">' + purchase_transaction_unit_cost.replace(/,/g, "") + '</div>';
                        html_project_trans += '<div class="project_percentage">100</div>';
                        html_project_trans += '</div>';

                        if (purchase_transaction_tax_rate != '0' && purchase_transaction_tax_rate != '') {

                            html_project_tax += '<div class="project_line">';
                            html_project_tax += '<div class="id"></div>';
                            html_project_tax += '<div class="project_id">' + trans_project_id + '</div>';
                            html_project_tax += '<div class="project_amount">' + purchase_transaction_tax_amount.replace(/,/g, "") + '</div>';
                            html_project_tax += '<div class="project_percentage">100</div>';
                            html_project_tax += '</div>';

                        }
                    }

                    var html = '';
                    var number = $('#modal-edit-table tr:last-child').find('.number').html() === undefined ? 1 : parseInt($('#modal-edit-table tr:last-child').find('.number').html()) + 1;

                    html += '<tr role="row" class="heading">';
                    html += '<td class="purchase_transaction_transaction_account">'+purchase_transaction_trans_account_name+' ('+purchase_transaction_trans_account+')</td>';
                    html += '<td class="purchase_transaction_id hidden"></td>';
                    html += '<td class="purchase_transaction_payable_GL_transaction_id hidden"></td>';
                    html += '<td class="purchase_transaction_trans_account_code hidden">'+purchase_transaction_trans_account+'</td>';
                    html += '<td class="purchase_transaction_trans_account_name hidden">'+purchase_transaction_trans_account_name+'</td>';
                    html += '<td class="purchase_transaction_tax_account_code hidden">'+purchase_transaction_tax_account+'</td>';
                    html += '<td class="purchase_transaction_tax_account_name hidden">'+purchase_transaction_tax_account_name+'</td>';
                    html += '<td class="purchase_transaction_tax_rate hidden">'+purchase_transaction_tax_rate+'</td>';
                    html += '<td class="purchase_transaction_tax_type hidden">'+purchase_transaction_tax_type+'</td>';
                    html += '<td class="purchase_transaction_unit_cost">'+purchase_transaction_unit_cost+'</td>';
                    html += '<td class="purchase_transaction_description hidden">'+purchase_transaction_description+'</td>';
                    html += '<td class="purchase_transaction_quantity hidden">'+purchase_transaction_quantity+'</td>';
                    html += '<td class="purchase_transaction_tax_amount">'+purchase_transaction_tax_amount+'</td>';
                    html += '<td class="purchase_transaction_item_amount">'+purchase_transaction_item_amount+'</td>';
                    html += '<td>';
                    html += '<a style="font-size: 25px;height: 35px;" class="purchase_transaction_project_split btn btn-md gray">...</a>' +
                        '<input type="text" style="width:60%; display:inline;" class="purchase_transaction_project_name form-control" value="'+trans_project_name+'">' +
                        '<input type="hidden" class="purchase_transaction_project" value="'+trans_project_id+'">';
                    html += '<div class="project_data_trans hidden">'+html_project_trans;
                    html += '</div>';
                    html += '<div class="project_data_tax hidden">'+html_project_tax;
                    html += '</div>';
                    html += '</td>';
                    html += '<td>';
                    html += '<a href="#" class="btn btn-md red purchase_transaction-delete">';
                    html += '<i class="fa fa-trash-o"></i>';
                    html += '</a>';
                    html += '<a href="#" class="btn btn-md blue purchase_transaction-edit">';
                    html += '<i class="fa fa-pencil"></i>';
                    html += '</a>';
                    html += '</td>';
                    html += '</tr>';

                    $('#modal-edit-table').append(html);

                    var list_projects = <?php echo $projects_json; ?>;
                    $( ".purchase_transaction_project_name" ).autocomplete({
                        source: list_projects,
                        select: function( event, ui) {
                            var el = $(this);
                            el.closest('tr').find('.purchase_transaction_project').val(ui.item.id);
                        }
                    });
                    $('.ui-autocomplete').css('z-index', '999999');

                    calculate_total();

                    $(".purchase_transaction-delete").on('click', function () {
                        var el = $(this);
                        $('#purchase_transaction_add').closest('tr').show();
                        el.closest('tr').remove();

                        calculate_total();
                    });

                    each_transaction_row();

                    $('#purchase_transaction_edit-modal').modal('hide');
                }
            });
        });

        $(".purchase_entry-delete").on("click",function(){
            var el = $(this);
            var purchase_entry_id = el.closest('tr').find('.purchase_entry_id').html();
            $('#modal_delete_id').val(purchase_entry_id);

            $('#purchase_entry-delete-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $(".purchase_entry-undo").on("click",function(){
            var el = $(this);
            var purchase_entry_id = el.closest('tr').find('.purchase_entry_id').html();
            $('#modal_undo_id').val(purchase_entry_id);

            $('#purchase_entry-undo-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $(".purchase_entry-undo_bank_in").on("click",function(){
            var el = $(this);
            var purchase_entry_id = el.closest('tr').find('.purchase_entry_id').html();
            $('#modal_undo_bank_in_id').val(purchase_entry_id);

            $('#purchase_entry-undo_bank_in-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        function clear_edit_modal() {

            $('#purchase_entry_type').val('PURC');
            $('#show_error_message').hide();

            $('#modal-edit-table').html('');
            $('#purchase_transaction_total_amount').html('');
            $('#purchase_transaction_total_tax').html('');

            $('#purchase_entry_description').val('');
            $('#purchase_entry_supplier_id').html('');
            $('#purchase_entry_supplier_name').val('');
            $('#purchase_entry_document_date').val('');
            $('#purchase_entry_due_date').val('');
            $('#purchase_entry_refference_no').val('');

            $('#purchase_entry_refference_no').css('border-color', '#e5e5e5');
            $('#required_purchase_entry_refference_no').hide();

            $('#purchase_entry_document_date').css('border-color', '#e5e5e5');
            $('#required_purchase_entry_document_date').hide();

            $('#purchase_entry_type').css('border-color', '#e5e5e5');
            $('#required_purchase_entry_type').hide();

            $('#purchase_transaction_add').css('color', 'black');
            $('#show_error_message').hide();

            $("#purchase_transaction-mix").hide();
        }

        $('#purchase_entry_document_date, #purchase_entry_supplier_name').on("change",function(){

            var purchase_entry_document_date = $('#purchase_entry_document_date').val();
            var supplier_id = $('#purchase_entry_supplier_id').html();

            if (supplier_id != '' && purchase_entry_document_date != '') {
                $.ajax({
                    method: 'POST',
                    url: "<?php echo base_url(); ?>credit_note/find_supplier",
                    data: {
                        id: supplier_id
                    },
                    dataType: 'json'
                }).done(function (res) {

                    if (res.default_due_date_number != null) {

                        var result = new Date(purchase_entry_document_date);
                        result.setDate(result.getDate() + parseFloat(res.default_due_date_number));
                        $('#purchase_entry_due_date').val($.datepicker.formatDate( "yy-mm-dd", result));
                    }

                });
            }


        });

        $(".purchase_entry-add").on("click",function(){

            $('#purchase_transaction_add').closest('tr').show();
            $('#modal_id').val('');
            $('#purchase_entry-modal-title').html('Add Credit Note');
            clear_edit_modal();

            $("#purchase_entry-edit-modal input, #purchase_entry-edit-modal textarea, #purchase_entry-edit-modal select").prop('disabled', false);

            $("#purchase_entry_project_select-modal input").prop('disabled', false);

            $(".supplier_select_button").show();
            $("#purchase_transaction_add").show();
            $("#purchase_entry_submit").show();
            $(".date-picker button").show();

            $('#purchase_entry_document_date').val(document_date_default);

            $('#purchase_entry-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $(".purchase_entry-edit, .purchase_entry-view").on("click",function(){
            var el = $(this);
            var id = el.closest('tr').find('.purchase_entry_id').html();

            $('#show_error_message').hide();

            $('#modal_id').val(id);

            $('#purchase_entry-modal-title').html('Edit Credit Note');
            clear_edit_modal();

            $.ajax({
                method: 'POST',
                url: "<?php echo base_url(); ?>credit_note/get_detail",
                data: {
                    id: id
                },
                dataType: 'json'
            }).done(function(res) {

                $('#purchase_transaction_add').closest('tr').hide();

                $('#payment_GL_transaction_id').val(res.credit_note.payable_GL_transaction_id);

                $('#purchase_entry_description').val(res.credit_note.description);
                $('#purchase_entry_document_date').val(res.credit_note.document_date);
                $('#purchase_entry_due_date').val(res.credit_note.due_date);
                $('#purchase_entry_refference_no').val(res.credit_note.refference_no);
                $('#purchase_entry_supplier_id').html(res.credit_note.supplier_id);
                $('#purchase_entry_supplier_name').val(res.credit_note.supplier_name);


                var html = '';
                var base_amount = parseFloat(res.credit_note.amount);
                var tax_amount = (base_amount)*parseFloat(res.credit_note.tax_rate)/100;
                var item_amount = (base_amount)+parseFloat(tax_amount);
                var unit_cost = parseFloat(res.credit_note.amount);

                var html_project_trans = '';
                var trans_project_id = '';
                var trans_project_name = '';

                $.each(res.credit_note.trans_project, function (index, item) {

                    if (item.percentage == '100.00') {
                        trans_project_id = item.project_id;
                        trans_project_name = item.project_name+' ('+item.project_code+')';
                    }

                    html_project_trans += '<div class="project_line">';
                    html_project_trans += '<div class="id">'+item.id+'</div>';
                    html_project_trans += '<div class="project_id">'+item.project_id+'</div>';
                    html_project_trans += '<div class="project_amount">'+item.amount+'</div>';
                    html_project_trans += '<div class="project_percentage">'+item.percentage+'</div>';
                    html_project_trans += '</div>';
                });

                var html_project_tax = '';

                if (res.credit_note.tax_rate != '0' && res.credit_note.tax_rate != '' && res.credit_note.tax_project !== undefined) {
                    $.each(res.credit_note.tax_project, function (index, item) {
                        html_project_tax += '<div class="project_line">';
                        html_project_tax += '<div class="id">' + item.id + '</div>';
                        html_project_tax += '<div class="project_id">' + item.project_id + '</div>';
                        html_project_tax += '<div class="project_amount">' + item.amount + '</div>';
                        html_project_tax += '<div class="project_percentage">' + item.percentage + '</div>';
                        html_project_tax += '</div>';
                    });
                }

                var tax_account = res.credit_note.tax_account == undefined ? '': res.credit_note.tax_account;
                var tax_type = res.credit_note.tax_type == undefined ? '' : res.credit_note.tax_type;
                var tax_account_name = res.credit_note.tax_account_name == undefined ? '': res.credit_note.tax_account_name;
                var final_tax_amount = tax_amount == 0 ? '' : numberWithCommas(tax_amount.toFixed(2));

                html += '<tr role="row" class="heading">';
                html += '<td class="purchase_transaction_transaction_account">'+res.credit_note.trans_account_name+' ('+res.credit_note.trans_account+')</td>';
                html += '<td class="purchase_transaction_id hidden">'+res.credit_note.id+'</td>';
                html += '<td class="purchase_transaction_payable_GL_transaction_id hidden">'+res.credit_note.payable_GL_transaction_id+'</td>';
                html += '<td class="purchase_transaction_tax_account_code hidden">'+tax_account+'</td>';
                html += '<td class="purchase_transaction_tax_account_name hidden">'+tax_account_name+'</td>';
                html += '<td class="purchase_transaction_trans_account_code hidden">'+res.credit_note.trans_account+'</td>';
                html += '<td class="purchase_transaction_trans_account_name hidden">'+res.credit_note.trans_account_name+'</td>';
                html += '<td class="purchase_transaction_tax_rate hidden">'+res.credit_note.tax_rate+'</td>';
                html += '<td class="purchase_transaction_tax_type hidden">'+tax_type+'</td>';
                html += '<td class="purchase_transaction_unit_cost">'+numberWithCommas(unit_cost.toFixed(2))+'</td>';
                html += '<td class="purchase_transaction_description hidden"></td>';
                html += '<td class="purchase_transaction_quantity hidden">'+1+'</td>';
                html += '<td class="purchase_transaction_tax_amount">'+final_tax_amount+'</td>';
                html += '<td class="purchase_transaction_item_amount">'+numberWithCommas(item_amount.toFixed(2))+'</td>';
                html += '<td>';
                html += '<a style="font-size: 25px;height: 35px;" class="purchase_transaction_project_split btn btn-md gray">...</a>' +
                    '<input type="text" style="width:60%; display:inline;" class="purchase_transaction_project_name form-control">' +
                    '<input type="hidden" class="purchase_transaction_project">';
                html += '<div class="project_data_trans hidden">';
                html += html_project_trans;
                html += '</div>';
                html += '<div class="project_data_tax hidden">';
                html += html_project_tax;
                html += '</div>';
                html += '</td>';
                html += '<td>';
                html += '<a href="#" class="btn btn-md red purchase_transaction-delete">';
                html += '<i class="fa fa-trash-o"></i>';
                html += '</a>';
                html += '<a href="#" class="btn btn-md blue purchase_transaction-edit">';
                html += '<i class="fa fa-pencil"></i>';
                html += '</a>';
                html += '</td>';
                html += '</tr>';

                $('#modal-edit-table').append(html);

                if (trans_project_id != '') {
                    $('#modal-edit-table tr:last-child .purchase_transaction_project').val(trans_project_id);
                    $('#modal-edit-table tr:last-child .purchase_transaction_project_name').val(trans_project_name);
                }

                var list_projects = <?php echo $projects_json; ?>;
                $( ".purchase_transaction_project_name" ).autocomplete({
                    source: list_projects,
                    select: function( event, ui) {
                        var el = $(this);
                        el.closest('tr').find('.purchase_transaction_project').val(ui.item.id);
                    }
                });
                $('.ui-autocomplete').css('z-index', '999999');

                calculate_total();

                $(".purchase_transaction-delete").on('click', function () {
                    $('#purchase_transaction_add').closest('tr').show();
                    var el = $(this);
                    el.closest('tr').remove();

                    calculate_total();
                });

                each_transaction_row();

                if(el.html() == '<i class="fa fa-eye"></i> View') {
                    $("#purchase_entry-edit-modal input, #purchase_entry-edit-modal textarea, #purchase_entry-edit-modal select").prop('disabled', true);

                    $("#purchase_entry_project_select-modal input").prop('disabled', true);

                    $(".supplier_select_button").hide();
                    $(".purchase_transaction-delete").hide();
                    $(".purchase_transaction-edit").hide();
                    $("#purchase_transaction_add").hide();
                    $("#purchase_entry_submit").hide();
                    $(".date-picker button").hide();

                } else {
                    $("#purchase_entry-edit-modal input, #purchase_entry-edit-modal textarea, #purchase_entry-edit-modal select").prop('disabled', false);

                    $("#purchase_entry_project_select-modal input").prop('disabled', false);

                    $(".supplier_select_button").show();
                    $("#purchase_transaction_add").show();
                    $("#purchase_entry_submit").show();
                    $(".date-picker button").show();
                }
            });

            $('#purchase_entry-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $("#purchase_entry_submit").on("click",function(){

            $(this).prop("disabled", true);

            var purchase_entry_type = $('#purchase_entry_type').val();
            var purchase_entry_description = $('#purchase_entry_description').val();
            var purchase_entry_document_date = $('#purchase_entry_document_date').val();
            var purchase_entry_due_date = $('#purchase_entry_due_date').val();
            var purchase_entry_refference_no = $('#purchase_entry_refference_no').val();
            var purchase_entry_supplier_id = $('#purchase_entry_supplier_id').html();
            var purchase_entry_total_amount = parseFloat($('#purchase_transaction_total_amount').html().replace(/,/g, ""));
            var purchase_entry_total_tax = parseFloat($('#purchase_transaction_total_tax').html() == '' ? 0 : $('#purchase_transaction_total_tax').html().replace(/,/g, ""));
            var modal_id = $('#modal_id').val();
            var payment_GL_transaction_id = $('#payment_GL_transaction_id').val();

            var error_message = false;

            if (purchase_entry_type == '') {

                $('#purchase_entry_type').css('border-color', 'red');
                $('#required_purchase_entry_type').show();
                $('#purchase_entry_type').focus();
                error_message = true;
            } else {
                $('#purchase_entry_type').css('border-color', '#e5e5e5');
                $('#required_purchase_entry_type').hide();
            }

            if (purchase_entry_document_date == '') {

                $('#purchase_entry_document_date').css('border-color', 'red');
                $('#required_purchase_entry_document_date').show();
                $('#purchase_entry_document_date').focus();
                error_message = true;
            } else {
                $('#purchase_entry_document_date').css('border-color', '#e5e5e5');
                $('#required_purchase_entry_document_date').hide();

                if (new Date(locked_financial_month) > new Date(purchase_entry_document_date)) {

                    $('#purchase_entry_document_date').css('border-color', 'red');
                    $('#lock_purchase_entry_document_date').show();
                    error_message = true;
                } else {
                    $('#purchase_entry_document_date').css('border-color', '#e5e5e5');
                    $('#lock_purchase_entry_document_date').hide();
                }
            }

            if (purchase_entry_refference_no == '') {

                $('#purchase_entry_refference_no').css('border-color', 'red');
                $('#required_purchase_entry_refference_no').show();
                $('#purchase_entry_refference_no').focus();
                error_message = true;
            } else {
                $('#purchase_entry_refference_no').css('border-color', '#e5e5e5');
                $('#required_purchase_entry_refference_no').hide();
            }


            if ($('#modal-edit-table').html() == '') {

                $('#purchase_transaction_add').css('color', 'red');
                $('#error_message').html('Credit Note is required');
                $('#show_error_message').show();
                error_message = true;
            } else {
                $('#purchase_transaction_add').css('color', 'black');
                $('#show_error_message').hide();
            }

            var amount = parseFloat($('.purchase_transaction_unit_cost').html().replace(/,/g, ""));
            var tax_rate = $('.purchase_transaction_tax_rate').html();
            var tax_amount = $('.purchase_transaction_tax_amount').html().replace(/,/g, "");
            var tax_type = '';
            if ($('.purchase_transaction_tax_type').html() != '') {
                tax_type = $('.purchase_transaction_tax_type').html();
            }

            var trans_account_code = $('.purchase_transaction_trans_account_code').html();
            var tax_account_code = $('.purchase_transaction_tax_account_code').html();
            var payable_GL_transaction_id = $('.purchase_transaction_payable_GL_transaction_id').html();

            var project_trans = [];
            $('.project_data_trans').find('.project_line').each(function() {
                var project = {};
                var el1 = $(this);
                project.project_id = el1.find('.project_id').html();
                project.id = el1.find('.id').html();
                project.amount = el1.find('.project_amount').html();
                project.percentage = el1.find('.project_percentage').html();

                project_trans.push(project);
            });

            var project_tax = [];
            $('.project_data_tax').find('.project_line').each(function() {
                var project = {};
                var el1 = $(this);
                project.project_id = el1.find('.project_id').html();
                project.id = el1.find('.id').html();
                project.amount = el1.find('.project_amount').html();
                project.percentage = el1.find('.project_percentage').html();

                project_tax.push(project);
            });

            if (project_trans.length == 0) {
                $('#error_message').html('Credit Note Amount is required');
                $('#show_error_message').show();
                $('.purchase_transaction_project_split').css('color', 'red');
                error_message = true;
            } else {

                $('#show_error_message').hide();
                $('.purchase_transaction_project_split').css('color', '#428bca');
            }


            if (error_message == true) {
                $(this).prop("disabled", false);
                return;
            }

            if (purchase_entry_supplier_id != '') {
                var error_message1 = false;
                $.ajax({
                    async: false,
                    method: 'POST',
                    url: "<?php echo base_url(); ?>purchase_entry/check_ref_no",
                    data: {
                        id: modal_id,
                        supplier_id: purchase_entry_supplier_id,
                        refference_no: purchase_entry_refference_no
                    },
                    dataType: 'json'
                }).done(function (res) {

                    if (res.error == true) {
                        $('#purchase_entry_refference_no').css('border-color', 'red');
                        $('#error_message').html('Reference No is existed for this supplier');
                        $('#show_error_message').show();
                        error_message1 = true;
                        $(this).prop("disabled", false);
                        return;
                    } else {
                        $('#purchase_entry_refference_no').css('border-color', '#e5e5e5');
                        $('#show_error_message').hide();
                    }

                });

                if (error_message1 == true) {
                    $(this).prop("disabled", false);
                    return;
                }
            }

            $.ajax({
                method: 'POST',
                url: "<?php echo base_url(); ?>credit_note/edit",
                data: {
                    id: modal_id,
                    description: purchase_entry_description,
                    supplier_id: purchase_entry_supplier_id,
                    refference_no: purchase_entry_refference_no,
                    document_date: purchase_entry_document_date,
                    due_date: purchase_entry_due_date,
                    amount: amount,
                    tax_rate: tax_rate,
                    tax_amount: tax_amount,
                    tax_type: tax_type,
                    project_tax: project_tax,
                    project_trans: project_trans,
                    trans_account_code: trans_account_code,
                    payable_GL_transaction_id: payable_GL_transaction_id,
                    tax_account_code: tax_account_code
                },
                dataType: 'json'
            }).done(function(res) {
                if (res.success == true) {
                    $('#purchase_entry-edit-modal').addClass('submit_purchase_entry');
                    window.location.reload();
                }
            });
        });

        var exit = false;
        $('#purchase_entry-edit-modal').on('hide.bs.modal', function(e){

            $('#confirm_exit_edit-modal').modal('show');

            return exit;
        });

        $('#confirm_exit').on("click",function(){
            exit = true;
            $('#purchase_entry-edit-modal').modal('hide');
        });

        $('#purchase_entry-edit-modal').on('hidden.bs.modal', function(e){
            exit = false;
        });

        $('#accounts_search').on("click",function(){
            var account_keyword = $('#account_keyword').val();
            if (account_keyword != '') {

                account_keyword = account_keyword.toLowerCase();

                $('#accounts_list .choose_account').each(function () {
                    var el = $(this);
                    var modal_account_name = el.find('.modal_account_name').html().toLowerCase();

                    var modal_account_code = el.find('.modal_account_code').html().toLowerCase();

                    if (modal_account_name.indexOf(account_keyword) >= 0
                        || modal_account_code.indexOf(account_keyword) >= 0) {
                        el.show();
                    } else {
                        el.hide();
                    }
                });
            } else {
                $('#accounts_list .choose_account').show();
            }
        });

        var list_accounts = <?php echo $accounts_json; ?>;
        $( "#purchase_transaction_trans_account" ).autocomplete({
            source: list_accounts
        });

        var list_suppliers = <?php echo $suppliers_json; ?>;
        $( "#purchase_entry_supplier_name" ).autocomplete({
            source: list_suppliers,
            select: function( event, ui) {
                $('#purchase_entry_supplier_id').html(ui.item.id);
            }
        });

        $('.ui-autocomplete').css('z-index', '999999');
    });
    window.onbeforeunload = function(e) {

        if ($('#purchase_entry-edit-modal').hasClass('submit_purchase_entry')) {
            return;
        }

        if ($('#purchase_entry-edit-modal').hasClass('in')) {
            var dialogText = 'Dialog text here';
            e.returnValue = dialogText;
            return dialogText;
        }
    };
</script>
