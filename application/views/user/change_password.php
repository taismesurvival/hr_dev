<!DOCTYPE html>



<html lang="en">



<head>

	<meta charset="utf-8"/>

	<title>Accounting System</title>

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>

	<meta http-equiv="Content-type" content="text/html; charset=utf-8">

	<meta content="" name="description"/>

	<meta content="" name="author"/>

	<link href="<?php echo base_url('public/assets/global/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css"/>
	
	<link href="<?php echo base_url('public/assets/global/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css"/>

	<!-- END THEME STYLES -->

	<link rel="shortcut icon" href="<?php echo base_url('favicon.ico'); ?>"/>

	<script src="<?php echo base_url('public/assets/global/plugins/jquery-1.11.0.min.js') ?>" type="text/javascript"></script>

	<style>

		@font-face {

    		font-family: raleway;

		    src: url(../public/font/Raleway-Regular.ttf);

		}
		.required{
			color: #cf0000;
		}

        .frame{
            width: 500px;
            margin: 20px auto;
            border: 1px solid #cccccc;
            padding-right: 20px;
            padding-left: 20px;
            box-shadow: 0px 4px 2px #999999;
            position: relative;
        }
        input, button{
            border-radius: 0px !important;
        }
        .borderless td, .borderless th{
            border-top: none !important;
        }
        .error{
            color: #cf0000;
        }
        .success{
            color: green;
            font-size: 18px;
            text-align: center;
            padding-top: 20px;
        }
        .border-red{
            border-color: #cf0000;
        }
        .title-frame{
            text-align: center;
            font-size: 16px;
            font-weight: bold;
            color: #ffffff;
            background-color: #666666;
            position: absolute;
            top: -11px;
            left: 0;
            width: 100%;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        img{
            height:100px;
            margin-top:50px;
        }
        @media screen and (max-width: 550px)
        {
            body{
                padding: 20px 10px 20px 10px;
            }
            .frame{
                width: 100%;
                padding-right: 7px;
                padding-left: 7px;
            }
            img{
                height: 90px;
            }
        }

	</style>

</head>



<body style='background:white;'>

	<?= isset($message_success) ? '<p class="success">'.$message_success.'</p>' : '' ?>

	<div class="login_page">
		<div class="frame">
			<h4 class="title-frame">Change password</h4>
			<div align='center' style="height: 50px;">
			</div>
			<form action="<?= $action ?>" class="form-horizontal" method="post" data-parsley-validate="" accept-charset="utf-8" novalidate="">
				<table class="table borderless">
					<tr>
						<th>Username <span class="required">*</span></th>
						<td>
							<input type="text" name="code" value="<?= $user_data['username'] ?>" class="form-control" data-parsley-required="true" data-parsley-type="alphanum" readonly="">
							<?= isset($error['code']) ? '<p class="error">'.$error['code'].'</p>' : '' ?>
						</td>
					</tr>
					<tr>
						<th>Email <span class="required">*</span></th>
						<td>
							<input type="text" value="<?= $user_data['email'] ?>" class="form-control" data-parsley-required="true" readonly="">
						</td>
					</tr>
					<tr>
						<th>New password <span class="required">*</span></th>
						<td>
			                <input type="password" name="password" value="" class="form-control" required="" id="pwd" data-parsley-id="3761">
			                <?= isset($error['password']) ? '<p class="error">'.$error['password'].'</p>' : '' ?>
						</td>
					</tr>
					<tr>
						<th>Confirm password <span class="required">*</span></th>
						<td>
			                <input type="password" name="confirm_passsord" value="" class="form-control" data-parsley-equalto="#pwd" required="" data-parsley-id="3961">
			                <?= isset($error['confirm_password']) ? '<p class="error">'.$error['confirm_password'].'</p>' : '' ?>
						</td>
					</tr>
					<tr>
						<th></th>
						<td>
			                <button type="submit" class="btn btn-primary">Save</button>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<div align="center"><a href="<?= base_url() ?>">Back to home page</a></div>
</body>

<!-- END BODY -->

</html>