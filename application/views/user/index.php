<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    User
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo base_url('company') ?>">Company Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">User</a>
        </li>
</div>
<!-- END PAGE HEADER -->
<!-- BEGIN ALERT BOX -->
<?php if (isset($flash_message)) { ?>
    <div class="row margin-bottom-20">
        <div class="col-xs-12">
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END ALERT BOX -->

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">User</span>
                </div>
                <a href="#" class="user-add btn btn-md blue pull-right"><i class="fa fa-plus"></i> Add New</a>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-12">

                        <!-- Begin: life time stats -->
                        <div class="table-container">
                            <!-- Begin: life time stats -->
                            <div class="clearfix mt15">

                                <form method="get" class="form" role="form">
                                    <div class="row">
                                        <div class="col-md-3 col-xs-12">
                                            <label>Login ID</label>
                                            <div class="form-group">
                                                <input type="text" name="username" class="form-control" value="<?php echo $search['username'] ?>" placeholder="Login ID">
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-12">
                                            <label>Status</label>
                                            <div class="form-group">
                                                <?php echo form_dropdown('is_active', ['' => 'Please Select', 'active' => 'Active', 'inactive' => 'Inactive'], set_value('is_active', $search['is_active']), 'class="form-control"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 text-right">
                                            <a href="<?php echo base_url('user') ?>" class=" btn btn-md grey-salsa">Reset</a>
                                            <button type="submit" class="btn btn-md green"><i class="icon-magnifier font-white"></i>Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <table style="margin-top: 20px;" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="4%" class="text-right">No</th>
                                    <th width="8%">User Name</th>
                                    <th width="8%">Login ID</th>
                                    <th width="8%">Role Group</th>
                                    <th width="8%">Mobile</th>
                                    <th width="8%">Email</th>
                                    <th width="8%">Status</th>
                                    <th width="25%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php for ($i = 0; $i < count($users); $i++) {
                                        ?>
                                    <tr role="row" class="heading">
                                        <td class="text-right"><?php echo $pagination['offset']+$i ?></td>
                                        <td class="user_id hidden"><?php echo $users[$i]['id'] ?></td>
                                        <td class="role_id hidden"><?php echo $users[$i]['role_id'] ?></td>
                                        <td class="name"><?php echo $users[$i]['name']; ?></td>
                                        <td class="username"><?php echo $users[$i]['username']; ?></td>
                                        <td><?php echo $users[$i]['role_name']; ?></td>
                                        <td class="mobile"><?php echo $users[$i]['mobile']; ?></td>
                                        <td class="email"><?php echo $users[$i]['email']; ?></td>
                                        <td class="is_active hidden"><?php echo $users[$i]['is_active']; ?></td>
                                        <td class="outlet_id hidden"><?php echo $users[$i]['outlet_id']; ?></td>
                                        <td><?php if ($users[$i]['is_active'] == 'active') { ?>
                                            <span class="label label-success">ACTIVE</span>
                                            <?php } else { ?>
                                            <span class="label label-warning">INACTIVE</span>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <a class="user-edit btn btn-md blue"><i class="fa fa-pencil"></i> Edit</a>
                                            <?php if ($users[$i]['role_id'] != 3) { ?>
                                            <a href="#" class="btn btn-md red user-delete"><i class="fa fa-trash-o"></i> Delete</a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
                <nav class="pagination-wrap clearfix">
                    <div class="pull-left mt10">
                        <?php if ($pagination['total'] != 0) { ?>
                        <label class="form-control-static"><?php echo $pagination['total'] ?> Items in total, Display
                            <?php echo $pagination['offset'] ?> ~
                            <?php echo $pagination['limit'] ?></label>
                        <?php } ?>
                    </div>
                    <?php if (!empty($pagination) && $pagination['total'] > $pagination['items_per_page']) { ?>
                    <div class="pull-right">
                        <ul class="pagination no-margin">
                            <li <?php if ($pagination['page'] - 1 <= 0) { ?> class="disabled" <?php } ?>>

                            <a href="<?php if ($pagination['page'] - 1 <= 0){ ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] - 1 ?><?php } ?>">
                                Previous
                            </a>
                            </li>
                            <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                            <li>
                                <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=1"> 1 </a>
                            </li>
                            <?php } ?>

                            <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                            <li><a href="javascript:;">...</a></li>
                            <?php } ?>

                            <?php for ($p=($pagination['min_display']); $p <= $pagination['max_display']; $p++) { ?>
                            <?php if ($p > 0) { ?>
                            <li <?php if ($pagination['page'] == $p) { ?> class="active" <?php } ?>>
                            <a href="<?php if ($pagination['page'] != $p) { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $p ?><?php } else { ?>javascript:;<?php } ?>"> <?php echo $p ?> </a>
                            </li>
                            <?php } ?>
                            <?php } ?>

                            <?php if (($pagination['page'] + $pagination['adj']) < $pagination['total_page']) { ?>
                            <li><a href="javascript:;">...</a></li>
                            <?php } ?>

                            <?php if ($pagination['page'] + $pagination['adj'] < $pagination['total_page']) { ?>
                            <li>
                                <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['total_page'] ?>"> <?php echo $pagination['total_page'] ?> </a>
                            </li>
                            <?php } ?>

                            <li <?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> class="disabled" <?php } ?>>
                            <a href="<?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] + 1 ?><?php } ?>">
                                Next
                            </a>
                            </li>
                        </ul>
                    </div>
                    <?php } ?>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="user-edit-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="user-modal-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">

                <div id="show_error_message" class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span id="error_message">Financial Period is updated successfully</span>
                </div>

                <?php echo form_open_multipart(base_url('user/edit'), 'class="form-horizontal" method="post" data-parsley-validate id="user-edit_form"'); ?>
                <div class="form-body">
                    <div class="form-group">
                        <?php echo form_label('User Name <span style="color:red;" class="required">*</span>', 'name', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input('name', set_value('name', ''), 'class="form-control" id="modal_name" data-parsley-required="true" data-parsley-required-message="User Name is required field"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Login ID <span style="color:red;" class="required">*</span>', 'username', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input('username', set_value('username', ''), 'class="form-control login_id" id="modal_username" data-parsley-required="true" data-parsley-required-message="Login ID is required field" data-parsley-username="username"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Role Group <span style="color:red;" class="required">*</span>', 'name', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_dropdown('role_id', $role_list, set_select('role_id'), 'class="form-control" id="modal_role_id" data-parsley-required="true" data-parsley-required-message="Role Group is required field"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Mobile', 'mobile', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input('mobile', set_value('mobile', ''), 'class="form-control" id="modal_mobile"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Email', 'email', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_input('email', set_value('email', ''), 'class="form-control" id="modal_email"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Status <span style="color:red;" class="required">*</span>', 'is_active', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_dropdown('is_active', ['' => 'Please Select', 'active' => 'Active', 'inactive' => 'Inactive'], set_select('is_active'), 'class="form-control" id="modal_is_active" data-parsley-required="true" data-parsley-required-message="Status is required field"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php echo form_label('Outlet Permission (only for Sales Input)', 'outlet_id', array('class' => 'col-md-5 col-lg-4 control-label')); ?>

                        <div class="col-md-7 col-lg-8 form-group">
                            <?php echo form_multiselect('outlet_id[]', $project_list, set_select('outlet_id'), 'class="form-control" id="modal_outlet_id"'); ?>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_id">
                <button  type="button" class="btn btn-md blue" id="user_submit">Submit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<div class="modal" id="user-delete-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Delete User</h4>
            </div>
            <div class="modal-body">

                <p class="text-center">Can you confirm to delete User?</p>

                <?php echo form_open_multipart(base_url('user/delete'), 'class="form-horizontal" method="post" data-parsley-validate'); ?>


            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_delete_id">
                <button  type="submit" class="btn btn-md red">Delete</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {

        window.ParsleyValidator.addValidator('username',
            function (value, requirement) {
                var modal_username = $('#modal_username').val();
                var modal_id = $('#modal_id').val();

                var error_message = '';

                var response = false;

                $.ajax({
                    async:false,
                    method: 'POST',
                    url: "<?php echo base_url(); ?>user/check_edit",
                    data: {
                        username: modal_username,
                        id: modal_id
                    },
                    dataType: 'json'
                }).done(function(res) {
                    response = res.error;
                    error_message = res.message;
                });

                window.ParsleyValidator.addMessage('en', 'username', error_message);
                return response == false;
            }, 32
        );

        $('.date-picker').datepicker({
            format: "yyyy-mm-dd"
        });

        $("#user_submit").on("click",function(){

            $('#show_error_message').hide();
            $("#user-edit_form").submit();
        });

        $(".user-edit").on("click",function(){

            $("#modal_username").prop("disabled", true);

            $("#modal_name").parsley().reset();
            $("#modal_username").parsley().reset();
            $("#modal_role_id").parsley().reset();
            $("#modal_is_active").parsley().reset();

            $("#modal_outlet_id option").prop("selected", false);

            var el = $(this);
            var name = el.closest('tr').find('.name').html();
            var username = el.closest('tr').find('.username').html();
            var role_id = el.closest('tr').find('.role_id').html();
            var mobile = el.closest('tr').find('.mobile').html();
            var email = el.closest('tr').find('.email').html();
            var is_active = el.closest('tr').find('.is_active').html();
            var outlet_id = el.closest('tr').find('.outlet_id').html();
            var id = el.closest('tr').find('.user_id').html();

            $('#show_error_message').hide();

            $('#modal_name').val(name);
            $('#modal_username').val(username);
            $('#modal_role_id').val(role_id);
            $('#modal_mobile').val(mobile);
            $('#modal_email').val(email);
            $('#modal_is_active').val(is_active);
            $('#modal_id').val(id);

            if (outlet_id != '') {
                var array = JSON.parse(outlet_id);

                $.each(array, function (i, e) {
                    $("#modal_outlet_id option[value='" + e + "']").prop("selected", true);
                });
            }

            $('#user-modal-title').html('Edit User');

            $('#user-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $(".user-delete").on("click",function(){
            var el = $(this);
            var user_id = el.closest('tr').find('.user_id').html();
            $('#modal_delete_id').val(user_id);

            $('#user-delete-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $(".user-add").on("click",function(){
            $("#modal_username").prop("disabled", false);
            $("#modal_name").parsley().reset();
            $("#modal_username").parsley().reset();
            $("#modal_role_id").parsley().reset();
            $("#modal_is_active").parsley().reset();

            $("#modal_outlet_id option").prop("selected", false);

            $('#modal_name').val('');
            $('#modal_username').val('');
            $('#modal_role_id').val('');
            $('#modal_mobile').val('');
            $('#modal_email').val('');
            $('#modal_is_active').val('');
            $('#show_error_message').hide();
            $('#modal_id').val('');

            $('#user-modal-title').html('Add User');

            $('#user-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });
    });
</script>
