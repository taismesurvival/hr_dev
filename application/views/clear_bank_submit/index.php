<style>
    .choose_account {cursor: pointer;}
    #table_select_project td {font-size: 14px;}
    #accounts_list td {font-size: 14px;line-height: 2.8;}
</style>
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Bank Reconciliation Submit History
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Bank Reconciliation Submit History</a>
        </li>
</div>
<!-- END PAGE HEADER -->
<!-- BEGIN ALERT BOX -->
<?php if (isset($flash_message)) { ?>
    <div class="row margin-bottom-20">
        <div class="col-xs-12">
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END ALERT BOX -->

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Bank Reconciliation Submit History</span>
                </div>
                <a href="<?php echo base_url('clear_bank_reconciliation') ?>" class="btn btn-md blue pull-right"><i class="fa fa-plus"></i> Bank Reconciliation</a>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-12">

                        <!-- Begin: life time stats -->
                        <div class="table-container">
                            <!-- Begin: life time stats -->
                            <div class="clearfix mt15">

                                <form method="get" class="form" role="form">
                                    <div class="row">
                                        <div class="col-md-3 col-xs-12">
                                            <label>Month</label>
                                            <div class="form-group">
                                                <div class="input-group date month-picker" data-date-format="yyyy-mm"
                                                     data-date-viewmode="years">
                                                    <input name="month" type="text" class="form-control" value="<?php echo $search['month'] ?>" placeholder="Month">

                                                    <div class="input-group-btn">
                                                        <button class="btn default" type="button">
                                                            <i class="fa fa-calendar"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-xs-12">
                                            <label>Type</label>
                                            <div class="form-group">
                                                <?php echo form_dropdown('type', ['' => 'Please Select', 'posb' => 'POSB', 'manual' => 'Manual', 'uob' => 'UOB', 'dbs' => 'DBS', 'ocbc' => 'OCBC', 'hsbc' => 'HSBC'], set_value('type', $search['type']), 'class="form-control"'); ?>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 text-right">
                                            <a href="<?php echo base_url('clear_bank_submit') ?>" class=" btn btn-md grey-salsa">Reset</a>
                                            <button type="submit" class="btn btn-md green"><i class="icon-magnifier font-white"></i>Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <table style="margin-top: 20px;" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="4%" class="text-right">No</th>
                                    <th width="8%">Bank Account</th>
                                    <th width="8%">Month</th>
                                    <th width="8%">Type</th>
                                    <th width="8%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php for ($i = 0; $i < count($submits); $i++) {

                                        switch ($submits[$i]['type']) {

                                            case 'uob':
                                                $type = 'UOB';
                                                break;
                                            case 'posb':
                                                $type = 'POSB';
                                                break;
                                            case 'manual':
                                                $type = 'Manual';
                                                break;
                                            case 'dbs':
                                                $type = 'DBS';
                                                break;
                                            case 'ocbc':
                                                $type = 'OCBC';
                                                break;
                                            case 'hsbc':
                                                $type = 'HSBC';
                                                break;

                                        }
                                        ?>
                                    <tr role="row" class="heading">
                                        <td class="text-right"><?php echo $pagination['offset']+$i ?></td>
                                        <td class="id hidden"><?php echo $submits[$i]['id'] ?></td>
                                        <td><?php echo $submits[$i]['name'].' ('.$submits[$i]['number'].')'; ?></td>
                                        <td class="month"><?php echo $submits[$i]['month']; ?></td>
                                        <td class="hidden type"><?php echo $submits[$i]['type']; ?></td>
                                        <td><?php echo $type ?></td>
                                        <td>
                                            <a href="#" class="btn btn-md red GL_entry-delete"><i class="fa fa-trash-o"></i> Delete</a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
                <nav class="pagination-wrap clearfix">
                    <div class="pull-left mt10">
                        <?php if ($pagination['total'] != 0) { ?>
                        <label class="form-control-static"><?php echo $pagination['total'] ?> Items in total, Display
                            <?php echo $pagination['offset'] ?> ~
                            <?php echo $pagination['limit'] ?></label>
                        <?php } ?>
                    </div>
                    <?php if (!empty($pagination) && $pagination['total'] > $pagination['items_per_page']) { ?>
                    <div class="pull-right">
                        <ul class="pagination no-margin">
                            <li <?php if ($pagination['page'] - 1 <= 0) { ?> class="disabled" <?php } ?>>

                            <a href="<?php if ($pagination['page'] - 1 <= 0){ ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] - 1 ?><?php } ?>">
                                Previous
                            </a>
                            </li>
                            <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                            <li>
                                <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=1"> 1 </a>
                            </li>
                            <?php } ?>

                            <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                            <li><a href="javascript:;">...</a></li>
                            <?php } ?>

                            <?php for ($p=($pagination['min_display']); $p <= $pagination['max_display']; $p++) { ?>
                            <?php if ($p > 0) { ?>
                            <li <?php if ($pagination['page'] == $p) { ?> class="active" <?php } ?>>
                            <a href="<?php if ($pagination['page'] != $p) { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $p ?><?php } else { ?>javascript:;<?php } ?>"> <?php echo $p ?> </a>
                            </li>
                            <?php } ?>
                            <?php } ?>

                            <?php if (($pagination['page'] + $pagination['adj']) < $pagination['total_page']) { ?>
                            <li><a href="javascript:;">...</a></li>
                            <?php } ?>

                            <?php if ($pagination['page'] + $pagination['adj'] < $pagination['total_page']) { ?>
                            <li>
                                <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['total_page'] ?>"> <?php echo $pagination['total_page'] ?> </a>
                            </li>
                            <?php } ?>

                            <li <?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> class="disabled" <?php } ?>>
                            <a href="<?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] + 1 ?><?php } ?>">
                                Next
                            </a>
                            </li>
                        </ul>
                    </div>
                    <?php } ?>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="GL_entry-delete-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Delete Submission</h4>
            </div>
            <div class="modal-body">

                <p class="text-center">Can you confirm to delete the Submission</p>

                <?php echo form_open_multipart(base_url('clear_bank_submit/delete'), 'class="form-horizontal" method="post" data-parsley-validate'); ?>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_delete_id">
                <button  type="submit" class="btn btn-md red">Delete</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {

        $('.month-picker').datepicker({
            format: "yyyy-mm",
            viewMode: "months",
            minViewMode: "months"
        });

        $(".GL_entry-delete").on("click",function(){
            var el = $(this);
            var GL_entry_id = el.closest('tr').find('.id').html();
            $('#modal_delete_id').val(GL_entry_id);

            $('#GL_entry-delete-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });
    });
</script>
