<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Financial Periods
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo base_url('company') ?>">Company Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Financial Periods</a>
        </li>
</div>
<!-- END PAGE HEADER -->
<!-- BEGIN ALERT BOX -->
<?php if (isset($flash_message)) { ?>
    <div class="row margin-bottom-20">
        <div class="col-xs-12">
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END ALERT BOX -->

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Financial Periods</span>
                </div>
                <?php if (!empty($locked_financial_month)) { ?>
                <a style="margin-left: 20px;" id="FY-lock_monthly" href="javascript:;" class="btn btn-md blue pull-right"><i class="fa fa-lock"></i> Lock/ Unlock Financial Month</a>
                <?php } ?>
                <a href="#" class="FP-add btn btn-md blue pull-right"><i class="fa fa-plus"></i> Add New</a>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-12">

                        <!-- Begin: life time stats -->
                        <div class="table-container">
                            <!-- Begin: life time stats -->
                            <table style="margin-top: 20px;" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="7%" class="text-right">No</th>
                                    <th width="15%">Starting Date</th>
                                    <th width="15%">Ending Date</th>
                                    <th width="11%">Duration</th>
                                    <th width="25%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php for ($i = 0; $i < count($financial_periods); $i++) {
                                        $starting_date = strtotime($financial_periods[$i]['starting_date']);
                                        $ending_date = strtotime($financial_periods[$i]['ending_date']);
                                        ?>
                                    <tr role="row" class="heading">
                                        <td class="text-right"><?php echo $i+1 ?></td>
                                        <td class="FP_id hidden"><?php echo $financial_periods[$i]['id'] ?></td>
                                        <td class="starting_date"><?php echo date('Y-m-d', $starting_date); ?></td>
                                        <td class="ending_date"><?php echo date('Y-m-d', $ending_date); ?></td>
                                        <td><?php echo floor(($ending_date - $starting_date) / (60 * 60 * 24 * 30)); ?> months</td>
                                        <td>
                                            <a class="FP-edit btn btn-md blue"><i class="fa fa-pencil"></i> Edit</a>
                                            <a href="#" class="btn btn-md red FP-delete"><i class="fa fa-trash-o"></i> Delete</a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="FP-edit-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="FP-modal-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">

                <div id="show_error_message" class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span id="error_message">Financial Period is updated successfully</span>
                </div>

                <?php echo form_open_multipart(base_url('financial_period/edit'), 'class="form-horizontal" method="post" data-parsley-validate id="FP-edit_form"'); ?>

                <div class="form-group">
                    <label class="col-md-6 col-lg-5 control-label">Starting Date <span style="color:red;" class="required">*</span></label>

                    <div class="col-md-6 col-lg-7 form-group">
                        <div class="input-group date date-picker" data-date-format="yyyy-mm-dd"
                             data-date-viewmode="years">
                            <input id="modal_starting_date" value="" name="starting_date" type="text" class="form-control" data-parsley-required="true" data-parsley-required-message="Starting Date is required field">

                            <div class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-6 col-lg-5 control-label">Ending Date <span style="color:red;" class="required">*</span></label>
                    <div class="col-md-6 col-lg-7 form-group">
                        <div class="input-group date date-picker" data-date-format="mm-yyyy"
                             data-date-viewmode="months" data-date-minviewmode="months">
                            <input id="modal_ending_date" value="" name="ending_date" type="text" class="form-control" data-parsley-required="true" data-parsley-required-message="Ending Date is required field" data-parsley-larger="larger">

                            <div class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_id">
                <button  type="button" class="btn btn-md blue" id="FP_submit">Submit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<div class="modal" id="FP-delete-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Delete Financial Period</h4>
            </div>
            <div class="modal-body">

                <p class="text-center">Can you confirm to delete Financial Period?</p>

                <?php echo form_open_multipart(base_url('financial_period/delete'), 'class="form-horizontal" method="post" data-parsley-validate'); ?>


            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_delete_id">
                <button  type="submit" class="btn btn-md red">Delete</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<div class="modal" id="lock_monthly-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Lock/ Unlock Financial Month</h4>
            </div>
            <div class="modal-body">

                <p class="text-center">Last Bank Recons Submit</p>

                <p class="text-center" style="font-weight: bold; font-size: 18px; color:red;"><?php echo 'Bank Account: '.$clear_bank_recons['name'].' ('.$clear_bank_recons['number'].') Month: '.$clear_bank_recons['month']; ?></p>

                <p class="text-center">Locked Financial Month</p>

                <p class="text-center" style="font-weight: bold; font-size: 18px;"><?php echo $locked_financial_month; ?></p>

                <?php echo form_open_multipart(base_url(), 'class="form-horizontal" method="post" data-parsley-validate'); ?>


            </div>
            <div class="modal-footer" style="text-align: center">
                <a <?php echo $disable_ending == TRUE ? 'style="display:none;"' :''; ?> href="<?php echo base_url(); ?>financial_period/lock/<?php echo date('Y-m', strtotime("+1 month", strtotime($locked_financial_month))); ?>" class="btn btn-md grey-salsa pull-right"><i class="fa fa-lock"></i> Lock <?php echo date('Y-m', strtotime("+1 month", strtotime($locked_financial_month))); ?></a>
                <a <?php echo $disable_starting == TRUE ? 'style="display:none;"' :''; ?> href="<?php echo base_url(); ?>financial_period/lock/<?php echo date('Y-m', strtotime("-1 month", strtotime($locked_financial_month))); ?>" class="btn btn-md green pull-left"><i class="fa fa-unlock"></i> Unlock <?php echo $locked_financial_month; ?></a>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {

        $("#FY-lock_monthly").on("click",function(){

            $('#lock_monthly-modal').modal({
                show: true,
                backdrop: 'static'
            });

        });

        window.ParsleyValidator.addValidator('larger',
            function (value, requirement) {
                var modal_starting_date = $('#modal_starting_date').val();
                var modal_ending_date = $('#modal_ending_date').val();
                var modal_id = $('#modal_id').val();
                var error_message = '';

                var response = false;
                $.ajax({
                    async:false,
                    method: 'POST',
                    url: "<?php echo base_url(); ?>financial_period/check_edit",
                    data: {
                        starting_date: modal_starting_date,
                        ending_date: modal_ending_date,
                        id: modal_id
                    },
                    dataType: 'json'
                }).done(function(res) {

                    response = res.error;
                    error_message = res.message;
                });

                window.ParsleyValidator.addMessage('en', 'larger', error_message);
                return response == false;
            }, 32
        );

        $('.date-picker').datepicker({
            format: "yyyy-mm-dd"
        });

        $("#FP_submit").on("click",function(){

            $('#show_error_message').hide();
            $("#FP-edit_form").submit();

        });

        $(".FP-edit").on("click",function(){

            $("#modal_starting_date").parsley().reset();
            $("#modal_ending_date").parsley().reset();

            var el = $(this);
            var starting_date = el.closest('tr').find('.starting_date').html();
            var ending_date = el.closest('tr').find('.ending_date').html();
            var FP_id = el.closest('tr').find('.FP_id').html();

            $('#show_error_message').hide();

            $('#modal_starting_date').val(starting_date);
            $('#modal_ending_date').val(ending_date);
            $('#modal_id').val(FP_id);

            $('#FP-modal-title').html('Edit Financial Period');

            $('#FP-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $(".FP-delete").on("click",function(){
            var el = $(this);
            var FP_id = el.closest('tr').find('.FP_id').html();
            $('#modal_delete_id').val(FP_id);

            $('#FP-delete-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $(".FP-add").on("click",function(){

            $("#modal_starting_date").parsley().reset();
            $("#modal_ending_date").parsley().reset();

            $('#modal_starting_date').val('');
            $('#modal_ending_date').val('');
            $('#modal_id').val('');
            $('#show_error_message').hide();

            $('#FP-modal-title').html('Add Financial Period');

            $('#FP-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });
    });
</script>
