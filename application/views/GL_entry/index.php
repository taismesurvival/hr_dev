<style>
    .choose_account {cursor: pointer;}
    #table_select_project td {font-size: 14px;}
    #accounts_list td {font-size: 14px;line-height: 2.8;}
</style>
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    GL Entry
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">GL Entry</a>
        </li>
</div>
<!-- END PAGE HEADER -->
<!-- BEGIN ALERT BOX -->
<?php if (isset($flash_message)) { ?>
    <div class="row margin-bottom-20">
        <div class="col-xs-12">
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END ALERT BOX -->

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">GL Entry</span>
                </div>
                <a href="#" class="GL_entry-add btn btn-md blue pull-right"><i class="fa fa-plus"></i> Add New</a>
                <?php if ($show_pos == TRUE) { ?>
                <a style="margin-right: 10px;" href="#" class="GL_entry-import_pos btn btn-md blue pull-right"><i class="fa fa-plus"></i> Import POS Data</a>
                <?php } ?>
                <!--<a style="margin-right: 10px;" href="#" class="GL_entry-import_salary btn btn-md blue pull-right"><i class="fa fa-plus"></i> Import Salary Data</a>-->
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-12">

                        <!-- Begin: life time stats -->
                        <div class="table-container">
                            <!-- Begin: life time stats -->
                            <div class="clearfix mt15">

                                <form method="get" class="form" role="form">
                                    <div class="row">
                                        <div class="form-group">
                                            <?php echo form_label('Total Amount', 'description', array('class' => 'col-md-3 col-lg-2 control-label')); ?>

                                            <div class="col-md-3 col-lg-4 form-group">
                                                <input type="number" name="minimum_amount" class="form-control" value="<?php echo $search['minimum_amount'] ?>" placeholder="Minimum Amount">
                                            </div>

                                            <?php echo form_label('~', 'description', array('class' => 'col-md-2 col-lg-1 control-label')); ?>
                                            <div class="col-md-3 col-lg-4 form-group">
                                                <input type="number" name="maximum_amount" class="form-control" value="<?php echo $search['maximum_amount'] ?>" placeholder="Maximum Amount">
                                            </div>

                                            <?php echo form_label('Type', 'description', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                            <div class="col-md-3 col-lg-4 form-group">
                                                <?php echo form_dropdown('type', ['' => 'Please Select', 'GJ' => 'Gl Journal', 'opening' => 'Opening', 'pos' => 'POS', 'salary' => 'Salary'], set_value('type', $search['type']), 'class="form-control"'); ?>
                                            </div>

                                            <?php echo form_label('Keyword', 'keyword', array('class' => 'col-md-2 col-lg-1 control-label')); ?>
                                            <div class="col-md-3 col-lg-4 form-group">
                                                <input type="text" name="keyword" class="form-control" value="<?php echo $search['keyword'] ?>">
                                            </div>

                                        </div>
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <?php echo form_label('Account Code or Name', 'description', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <input type="text" name="account_keyword" class="form-control" value="<?php echo $search['account_keyword'] ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="row">
                                                <?php echo form_label('Document Date', 'keyword', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <input type="text" name="document_date_from" class="form-control date-picker" value="<?php echo $search['document_date_from'] ?>" placeholder="Starting Date">
                                                </div>

                                                <?php echo form_label('~', 'description', array('class' => 'col-md-2 col-lg-1 control-label')); ?>
                                                <div class="col-md-3 col-lg-4 form-group">
                                                    <input type="text" name="document_date_to" class="form-control date-picker" value="<?php echo $search['document_date_to'] ?>" placeholder="Ending Date">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 text-right">
                                            <a href="<?php echo base_url('GL_entry') ?>" class=" btn btn-md grey-salsa">Reset</a>
                                            <button type="submit" class="btn btn-md green"><i class="icon-magnifier font-white"></i>Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <table style="margin-top: 20px;" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="4%" class="text-right">No</th>
                                    <th width="8%">Entry Date</th>
                                    <th width="8%">GL Entry Type</th>
                                    <th width="8%">Total Amount</th>
                                    <th width="8%">Description</th>
                                    <th width="25%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php

                                    $total_page  = 0;
                                    for ($i = 0; $i < count($GL_entries); $i++) {

                                        $total_page += $GL_entries[$i]['total_amount'];
                                        switch ($GL_entries[$i]['type']) {
                                            case 'GJ':
                                                $GL_type = 'GL Journal';
                                                break;
                                            case 'opening':
                                                $GL_type = 'Opening';
                                                break;
                                            case 'pos':
                                                $GL_type = 'POS';
                                                break;
                                            case 'salary':
                                                $GL_type = 'Salary';
                                                break;
                                        }
                                        ?>
                                    <tr role="row" class="heading">
                                        <td class="text-right"><?php echo $pagination['offset']+$i ?></td>
                                        <td class="GL_entry_id hidden"><?php echo $GL_entries[$i]['id'] ?></td>
                                        <td><?php echo $GL_entries[$i]['document_date']; ?></td>
                                        <td><?php echo $GL_type; ?></td>
                                        <td><?php echo number_format($GL_entries[$i]['total_amount'],2); ?></td>
                                        <td><?php echo $GL_entries[$i]['description']; ?></td>
                                        <td>
                                            <a class="GL_entry-edit btn btn-md blue"><i class="fa fa-pencil"></i> Edit</a>
                                            <?php if ($GL_type != 'Salary' && !in_array($GL_entries[$i]['id'], $done_sales_collection) && date('Y-m', strtotime($GL_entries[$i]['document_date'])) > $locked_financial_month) { ?>
                                            <a href="#" class="btn btn-md red GL_entry-delete"><i class="fa fa-trash-o"></i> Delete</a>
                                            <?php } ?>
                                            <?php if ($GL_type == 'Opening' && empty($locked_financial_month)) { ?>
                                                <a  href="<?php echo base_url(); ?>financial_period/lock/<?php echo date('Y-m', strtotime($GL_entries[$i]['document_date'])); ?>" class="btn btn-md grey-salsa pull-right"><i class="fa fa-lock"></i> Lock <?php echo date('Y-m', strtotime($GL_entries[$i]['document_date'])); ?></a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                    <tr role="row" class="heading" style="font-weight: bold;">
                                        <td colspan="3" class="text-right">Total</td>
                                        <td><?php echo number_format($total_page,2); ?></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
                <nav class="pagination-wrap clearfix">
                    <div class="pull-left mt10">
                        <?php if ($pagination['total'] != 0) { ?>
                        <label class="form-control-static"><?php echo $pagination['total'] ?> Items in total, Display
                            <?php echo $pagination['offset'] ?> ~
                            <?php echo $pagination['limit'] ?></label>
                        <?php } ?>
                    </div>
                    <?php if (!empty($pagination) && $pagination['total'] > $pagination['items_per_page']) { ?>
                    <div class="pull-right">
                        <ul class="pagination no-margin">
                            <li <?php if ($pagination['page'] - 1 <= 0) { ?> class="disabled" <?php } ?>>

                            <a href="<?php if ($pagination['page'] - 1 <= 0){ ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] - 1 ?><?php } ?>">
                                Previous
                            </a>
                            </li>
                            <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                            <li>
                                <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=1"> 1 </a>
                            </li>
                            <?php } ?>

                            <?php if ($pagination['page'] - $pagination['adj'] > 1) { ?>
                            <li><a href="javascript:;">...</a></li>
                            <?php } ?>

                            <?php for ($p=($pagination['min_display']); $p <= $pagination['max_display']; $p++) { ?>
                            <?php if ($p > 0) { ?>
                            <li <?php if ($pagination['page'] == $p) { ?> class="active" <?php } ?>>
                            <a href="<?php if ($pagination['page'] != $p) { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $p ?><?php } else { ?>javascript:;<?php } ?>"> <?php echo $p ?> </a>
                            </li>
                            <?php } ?>
                            <?php } ?>

                            <?php if (($pagination['page'] + $pagination['adj']) < $pagination['total_page']) { ?>
                            <li><a href="javascript:;">...</a></li>
                            <?php } ?>

                            <?php if ($pagination['page'] + $pagination['adj'] < $pagination['total_page']) { ?>
                            <li>
                                <a href="?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['total_page'] ?>"> <?php echo $pagination['total_page'] ?> </a>
                            </li>
                            <?php } ?>

                            <li <?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> class="disabled" <?php } ?>>
                            <a href="<?php if (($pagination['page'] + 1) > $pagination['total_page']) { ?> javascript:;<?php } else { ?>?<?php if (!empty($pagination['search'])) { ?><?php echo $pagination['search'] ?>&<?php } ?>p=<?php echo $pagination['page'] + 1 ?><?php } ?>">
                                Next
                            </a>
                            </li>
                        </ul>
                    </div>
                    <?php } ?>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="GL_entry-edit-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg" style="width: 95%;">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="GL_entry-modal-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">

                <div id="show_error_message" class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span id="error_message">Financial Period is updated successfully</span>
                </div>

                <?php echo form_open_multipart(base_url('GL_entry/edit'), 'class="form-horizontal" method="post" id="GL_entry-edit_form"'); ?>

                <div style="padding: 15px;" class="form-group">

                    <div class="form-group">
                        <?php echo form_label('Description', 'description', array('class' => 'col-md-3 col-lg-2 control-label')); ?>

                        <div class="col-md-3 col-lg-4 form-group">
                            <textarea name="description" cols="10" rows="3" class="form-control" id="GL_entry_description"></textarea>
                        </div>

                        <?php echo form_label('Type <span style="color:red;" class="required">*</span>', 'description', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                        <div class="col-md-3 col-lg-4 form-group">
                            <?php echo form_dropdown('type', ['' => 'Please Select', 'GJ' => 'GL Journal', 'opening' => 'Opening', 'pos' => 'POS', 'salary' => 'Salary'], set_value('type', ''), 'id="GL_entry_type" class="form-control"'); ?>
                            <small id="required_GL_entry_type" style="color:red; display:none;">Type is required field</small>
                            <small id="valid_GL_entry_type" style="color:red; display:none;">Type cannot be Salary and POS</small>
                            <small id="dupplicate_GL_opening" style="color:red; display:none;">Opening GL entry is existed in the system</small>
                        </div>

                        <?php echo form_label('Document Date <span style="color:red;" class="required">*</span>', 'document_date', array('class' => 'col-md-3 col-lg-2 control-label')); ?>
                        <div class="col-md-3 col-lg-4 form-group">
                            <input id="GL_entry_document_date" value="" name="starting_date" type="date" class="form-control">
                            <small id="required_GL_entry_document_date" style="color:red; display:none;">Document Date is required field</small>
                            <small id="lock_GL_entry_document_date" style="color:red; display:none;">Document Date is in locked period</small>
                        </div>
                    </div>

                    <table style="margin-top: 20px;" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr role="row" class="heading">
                            <th width="1%" class="text-right">No</th>
                            <th width="7%">Account</th>
                            <th width="14%">Account Name</th>
                            <th width="8%">Description</th>
                            <th width="5%">Debit Amount</th>
                            <th width="5%">Credit Amount</th>
                            <th width="7%">Outlet</th>
                            <th width="1%"></th>
                        </tr>
                        </thead>
                        <tbody id="modal-edit-table">
                        </tbody>
                        <tbody>
                        <tr role="row" class="heading">
                            <td style="cursor: pointer; font-size:14px;" colspan="9" id="GL_transaction_add" class="text-center"><i class="fa fa-plus"></i> Add New</td>
                        </tr>
                        <tr role="row" class="heading">
                            <td colspan="4" class="text-right" style="line-height: 2.8; font-size:14px;" id="total_amount_title">Total Base Amount</td>
                            <td id="GL_transaction_debit_total_amount" style="line-height: 2.8; text-align: center; font-size:14px;"></td>
                            <td style="border-right: 1px solid #ddd; line-height: 2.8; text-align: center; font-size:14px;" id="GL_transaction_credit_total_amount"></td>
                        </tr>
                        </tbody>
                    </table>

                </div>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_id">
                <button  type="button" class="btn btn-md blue" id="GL_entry_submit">Submit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<div class="modal" id="GL_entry_account_select-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Select Account</h4>
            </div>
            <div class="modal-body">
                <table style="margin-top: 20px;" class="tree table table-striped table-bordered table-hover">
                    <thead>
                    <tr role="row" class="heading">
                        <th width="25%">Account Name</th>
                        <th width="8%">Account Type</th>
                        <th width="8%">Account Code</th>
                        <th width="8%">Action</th>
                    </tr>
                    </thead>
                    <tbody id="accounts_list">
                    <?php for ($i = 0; $i < count($accounts); $i++) {
                        ?>
                        <tr role="row" class="treegrid-<?php echo $accounts[$i]['id'] ?> <?php if (!empty($accounts[$i]['father_id'])) {echo 'treegrid-parent-'.$accounts[$i]['father_id'];} ?> heading">
                            <td><?php echo $accounts[$i]['name']; ?></td>
                            <td class="modal_account_name hidden"><?php echo $accounts[$i]['name']; ?></td>
                            <td class="modal_account_type"><?php echo $accounts[$i]['type']; ?></td>
                            <td class="modal_account_code"><?php echo $accounts[$i]['code']; ?></td>
                            <?php if (empty($accounts[$i]['MC_id'])) { ?>
                                <td><button type="button" class="choose_account btn btn-md grey-salsa">Choose This Account</button></td>
                            <?php } else { ?>
                                <td></td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="GL_entry_project_select-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Select Outlet</h4>
            </div>
            <div class="modal-body">

                <div id="show_error_message_project" class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span id="error_message_project">Financial Period is updated successfully</span>
                </div>

                <?php echo form_label('Total Transaction Amount', 'description', array('class' => 'col-md-5 col-lg-4 control-label', 'style' => 'font-size:14px;')); ?>

                <div style="font-size: 14px;" id="GL_transaction_total" class="col-md-7 col-lg-8 form-group"></div>

                <table id="table_select_project" style="margin-top: 20px;" class="tree table table-striped table-bordered table-hover">
                    <thead>
                    <tr role="row" class="heading">
                        <th width="15%">Outlet Name</th>
                        <th width="10%">Percentage</th>
                        <th width="10%">Amount</th>
                    </tr>
                    </thead>
                    <tbody id="GL_project_table">
                    <?php for ($i = 0; $i < count($project_list); $i++) {
                        ?>
                        <tr role="row">
                            <td class="GL_project_name"><?php echo $project_list[$i]['name'].' ('.$project_list[$i]['description'].')'; ?></td>
                            <td class="id hidden"></td>
                            <td class="project_id hidden"><?php echo $project_list[$i]['id']; ?></td>
                            <td><input style="width:80%; display: inline;" type="text" class="GL_project_percentage form-control"> % </td>
                            <td><input style="width:80%; display: inline;" type="text" class="GL_project_amount form-control"></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                    <tbody>
                        <tr role="row">
                            <td class="text-right">Total</td>
                            <td id="GL_project_total_percentage"></td>
                            <td id="GL_project_total_amount"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button  type="button" class="btn btn-md blue" id="GL_project_submit">Submit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="GL_entry-delete-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Delete GL Entry</h4>
            </div>
            <div class="modal-body">

                <p class="text-center">Can you confirm to delete GL Entry</p>

                <?php echo form_open_multipart(base_url('GL_entry/delete'), 'class="form-horizontal" method="post" data-parsley-validate'); ?>

            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_delete_id">
                <button  type="submit" class="btn btn-md red">Delete</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<div class="modal" id="GL_entry-pos_import-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Import POS Data</h4>
            </div>
            <div class="modal-body row">

                <?php echo form_label('Choose date to view report', 'document_date', array('class' => 'col-md-12 col-lg-12 control-label')); ?>
                <div class="col-md-7 col-lg-8 form-group">
                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd"
                         data-date-viewmode="years">
                        <input id="GL_entry_pos_date" value="" name="starting_date" type="text" class="form-control">

                        <div class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </div>
                    </div>
                    <small id="required_POS_date" style="color:red; display:none;">The Report Date is required field</small>
                </div>

            </div>
            <div class="modal-footer">
                <button  type="button" id="view_pos_report" class="btn btn-md blue">View</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="GL_entry-salary_import-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Import Salary Data</h4>
            </div>
            <div class="modal-body row">

                <?php echo form_label('Choose month to view report', 'document_date', array('class' => 'col-md-12 col-lg-12 control-label')); ?>
                <div class="col-md-7 col-lg-8 form-group">
                    <div class="input-group date month-picker" data-date-format="yyyy-mm"
                         data-date-viewmode="years">
                        <input id="GL_entry_salary_date" value="" name="starting_date" type="text" class="form-control">

                        <div class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-calendar"></i>
                            </button>
                        </div>
                    </div>
                    <small id="required_salary_month" style="color:red; display:none;">The Report Month is required field</small>
                </div>

            </div>
            <div class="modal-footer">
                <button  type="button" id="view_salary_report" class="btn btn-md blue">View</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="confirm_exit_edit-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Close confirm GL Entry</h4>
            </div>
            <div class="modal-body">

                <p class="text-center">Can you confirm to close this GL entry?</p>

            </div>
            <div class="modal-footer">
                <button id="confirm_exit" type="submit" class="btn btn-md blue" data-dismiss="modal">Confirm</button>
                <button id="cancel_exit"  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {

        var project_selector = '<?php echo $project_selector; ?>';
        var locked_financial_month = '<?php echo $locked_next_financial_month; ?>';

        var default_project_id = '<?php echo $default_project_id; ?>';
        var default_project_name = '<?php echo $default_project_name; ?>';

        var document_date_default = '<?php echo $document_date_default; ?>';

        $('.date-picker').datepicker({
            format: "yyyy-mm-dd"
        });

        $('.month-picker').datepicker({
            format: "yyyy-mm",
            viewMode: "months",
            minViewMode: "months"
        });

        $('.date-picker').on('changeDate', function(ev){
            $(this).datepicker('hide');
        });

        $('.date-picker').on('hide.bs.modal', function(ev){
            ev.stopPropogation();
        });

        $('.month-picker').on('changeDate', function(ev){
            $(this).datepicker('hide');
            $('#confirm_exit_edit-modal').modal('hide');
        });

        function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }

        function each_transaction_row() {

            $('[data-toggle="tooltip"]').tooltip();

            var list_accounts = <?php echo $accounts_json; ?>;
            $( ".GL_transaction_account_code" ).autocomplete({
                source: list_accounts
            });

            var list_projects = <?php echo $projects_json; ?>;
            $( ".GL_transaction_project_name" ).autocomplete({
                source: list_projects,
                select: function( event, ui) {
                    var el = $(this);
                    el.closest('tr').find('.GL_transaction_project').val(ui.item.id);
                }
            });

            $('.ui-autocomplete').css('z-index', '999999');

            $('#modal-edit-table .GL_transaction_debit_amount, #modal-edit-table .GL_transaction_credit_amount').css('font-size', '14px');

            $(".GL_transaction_debit_amount, .GL_transaction_credit_amount, .GL_project_percentage, .GL_project_amount").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                    // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                    // let it happen, don't do anything
                    return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

            $(".GL_transaction_project_name").on('change', function () {// Still not done

                var el = $(this);
                var project_name = el.val();
                var trans_amount = el.closest('tr').find('.GL_transaction_amount').val();
                var project_id = el.closest('tr').find('.GL_transaction_project').val();
                trans_amount = parseFloat(trans_amount.replace(/,/g, ""));

                if (project_name != '') {
                    var project_html = '';

                    project_html += '<div class="project_line">';
                    project_html += '<div class="id"></div>';
                    project_html += '<div class="project_id">' + project_id + '</div>';
                    project_html += '<div class="project_amount">' + trans_amount + '</div>';
                    project_html += '<div class="project_percentage">100</div>';
                    project_html += '</div>';

                    el.closest('td').find('.project_data').html(project_html);
                } else {
                    el.closest('td').find('.project_data').html('');
                    el.closest('td').find('.GL_transaction_project').val('');
                }
            });

            $('.GL_transaction_account_code').on('change', function () {
                var el = $(this);
                if (el.val() != '') {

                    $.ajax({
                        method: 'POST',
                        url: "<?php echo base_url(); ?>GL_entry/get_account_name",
                        data: {
                            account_code: el.val()
                        },
                        dataType: 'json'
                    }).done(function(res) {
                        el.attr('data-original-title', res);
                        el.closest('tr').find('.GL_transaction_account_name').html(res);
                    });

                    if ($('#GL_transaction_debit_total_amount').html() != '' && $('#GL_transaction_credit_total_amount').html() != '' && el.closest('tr').find('.GL_transaction_credit_amount').val() == '' && el.closest('tr').find('.GL_transaction_debit_amount').val() == '') {
                        var debit_total = parseFloat($('#GL_transaction_debit_total_amount').html().replace(/,/g, ""));
                        var credit_total = parseFloat($('#GL_transaction_credit_total_amount').html().replace(/,/g, ""));

                        var difference = debit_total - credit_total;

                        if (difference >= 0) {

                            el.closest('tr').find('.GL_transaction_credit_amount').val(numberWithCommas(difference.toFixed(2)));
                            el.closest('tr').find('.GL_transaction_debit_amount').val('0.00');
                            el.closest('tr').find(".GL_transaction_type").val('credit');
                            el.closest('tr').find(".GL_transaction_amount").val(numberWithCommas(difference.toFixed(2)));

                        } else {

                            el.closest('tr').find('.GL_transaction_debit_amount').val(numberWithCommas((difference*(-1)).toFixed(2)));
                            el.closest('tr').find('.GL_transaction_credit_amount').val('0.00');
                            el.closest('tr').find(".GL_transaction_type").val('debit');
                            el.closest('tr').find(".GL_transaction_amount").val(numberWithCommas((difference*(-1)).toFixed(2)));
                        }

                        calculate_total();

                        el.closest('tr').find('.GL_transaction_project_split').show();
                        el.closest('tr').find('.GL_transaction_project_name').show();

                        if (default_project_name != '' && default_project_id != '') {
                            el.closest('tr').find('.GL_transaction_project').val(default_project_id);
                            el.closest('tr').find('.GL_transaction_project_name').val(default_project_name);

                            var html_project = '';
                            html_project += '<div class="project_line">';
                            html_project += '<div class="id"></div>';
                            html_project += '<div class="project_id">'+default_project_id+'</div>';
                            html_project += '<div class="project_amount">'+Math.abs(difference.toFixed(2))+'</div>';
                            html_project += '<div class="project_percentage">100</div>';
                            html_project += '</div>';

                            el.closest('tr').find('.project_data').html(html_project);
                        }

                    }

                } else {
                    el.attr('data-original-title', '');
                    el.closest('tr').find('.GL_transaction_account_name').html('');
                }
            });

            $(".GL_transaction_debit_amount").on('change', function () {

                var el = $(this);
                if (parseFloat(el.val()) != 0 && el.val() != '') {
                    var amount = parseFloat(el.val().replace(/,/g, ""));
                    el.val(numberWithCommas(amount.toFixed(2)));
                    el.closest('tr').find('.GL_transaction_project_split').show();
                    el.closest('tr').find('.GL_transaction_project_name').show();

                    if (default_project_name != '' && default_project_id != '' && el.closest('tr').find('.project_data').html() == '') {
                        el.closest('tr').find('.GL_transaction_project').val(default_project_id);
                        el.closest('tr').find('.GL_transaction_project_name').val(default_project_name);

                        var html_project = '';
                        html_project += '<div class="project_line">';
                        html_project += '<div class="id"></div>';
                        html_project += '<div class="project_id">'+default_project_id+'</div>';
                        html_project += '<div class="project_amount">'+amount.toFixed(2)+'</div>';
                        html_project += '<div class="project_percentage">100</div>';
                        html_project += '</div>';

                        el.closest('tr').find('.project_data').html(html_project);
                    }

                    el.closest('tr').find('.project_data').find('.project_line').each(function() {
                        var el2 = $(this);
                        var percentage = parseFloat(el2.find('.project_percentage').html());
                        var amount_each = amount*percentage/100;

                        el2.find('.project_amount').html(amount_each.toFixed(2));
                    });

                    el.closest('tr').find(".GL_transaction_credit_amount").val('0.00');
                    el.closest('tr').find(".GL_transaction_amount").val(numberWithCommas(amount.toFixed(2)));
                    el.closest('tr').find(".GL_transaction_type").val('debit');

                } else {

                    if (el.closest('tr').find(".GL_transaction_credit_amount").val() != ''
                    && parseFloat(el.closest('tr').find(".GL_transaction_credit_amount").val()) != 0) {
                        el.closest('tr').find(".GL_transaction_debit_amount").val('0.00');
                    } else {
                        el.closest('tr').find(".GL_transaction_credit_amount").val('');
                        el.closest('tr').find(".GL_transaction_debit_amount").val('');
                        el.closest('tr').find('.GL_transaction_project_split').hide();
                        el.closest('tr').find('.GL_transaction_project_name').hide();
                        $(".project_line .project_amount").html(0);
                    }
                }

                calculate_total();
            });

            $(".GL_transaction_credit_amount").on('change', function () {

                var el = $(this);
                if (parseFloat(el.val()) != 0 && el.val() != '') {
                    var amount = parseFloat(el.val().replace(/,/g, ""));
                    el.val(numberWithCommas(amount.toFixed(2)));
                    el.closest('tr').find('.GL_transaction_project_split').show();
                    el.closest('tr').find('.GL_transaction_project_name').show();

                    if (default_project_name != '' && default_project_id != '' && el.closest('tr').find('.project_data').html() == '') {
                        el.closest('tr').find('.GL_transaction_project').val(default_project_id);
                        el.closest('tr').find('.GL_transaction_project_name').val(default_project_name);

                        var html_project = '';
                        html_project += '<div class="project_line">';
                        html_project += '<div class="id"></div>';
                        html_project += '<div class="project_id">'+default_project_id+'</div>';
                        html_project += '<div class="project_amount">'+amount.toFixed(2)+'</div>';
                        html_project += '<div class="project_percentage">100</div>';
                        html_project += '</div>';

                        el.closest('tr').find('.project_data').html(html_project);
                    }

                    el.closest('tr').find('.project_data').find('.project_line').each(function() {
                        var el2 = $(this);
                        var percentage = parseFloat(el2.find('.project_percentage').html());
                        var amount_each = amount*percentage/100;

                        el2.find('.project_amount').html(amount_each.toFixed(2));
                    });

                    el.closest('tr').find(".GL_transaction_debit_amount").val('0.00');
                    el.closest('tr').find(".GL_transaction_amount").val(numberWithCommas(amount.toFixed(2)));
                    el.closest('tr').find(".GL_transaction_type").val('credit');

                } else {

                    if (el.closest('tr').find(".GL_transaction_debit_amount").val() != ''
                        && parseFloat(el.closest('tr').find(".GL_transaction_debit_amount").val()) != 0) {
                        el.closest('tr').find(".GL_transaction_credit_amount").val('0.00');
                    } else {
                        el.closest('tr').find(".GL_transaction_credit_amount").val('');
                        el.closest('tr').find(".GL_transaction_debit_amount").val('');
                        el.closest('tr').find('.GL_transaction_project_split').hide();
                        el.closest('tr').find('.GL_transaction_project_name').hide();
                        $(".project_line .project_amount").html(0);
                    }

                }

                calculate_total();
            });

            $(".GL_transaction_account_code_button").on("click",function(){
                var el1 = $(this);
                $('#GL_entry_account_select-modal').modal({
                    show: true,
                    backdrop: 'static'
                });
                el1.closest('td').addClass('account_chosen_td');

                var modal_account_code = '';
                $(".choose_account").on("click",function(){
                    var el = $(this);
                    modal_account_code = el.closest('tr').find('.modal_account_code').html();
                    var modal_account_name = el.closest('tr').find('.modal_account_name').html();
                    $('#GL_entry_account_select-modal').modal('hide');

                    $('.account_chosen_td').find('.GL_transaction_account_code').val(modal_account_code);
                    $('.account_chosen_td').find('.GL_transaction_account_code').attr('data-original-title', modal_account_name);
                    $('.account_chosen_td').closest('tr').find('.GL_transaction_account_name').html(modal_account_name);
                });
            });

            $('#GL_entry_account_select-modal').on('hidden.bs.modal', function () {
                setTimeout(function(){
                    $('.account_chosen_td').removeClass('account_chosen_td');
                }, 500);
            });

            $(".GL_transaction_project_split").on("click",function(){
                clear_project_modal();

                var el1 = $(this);

                var project_total_amount = el1.closest('tr').find('.GL_transaction_amount').val();

                $('#GL_transaction_total').html(project_total_amount);

                $('#GL_entry_project_select-modal').modal({
                    show: true,
                    backdrop: 'static'
                });
                el1.closest('td').addClass('project_chosen_td');

                $('.project_chosen_td .project_data .project_line').each(function () {
                    var el2 = $(this);
                    var project_id = el2.find('.project_id').html();
                    var id = el2.find('.id').html();
                    var project_amount = el2.find('.project_amount').html();
                    var project_percentage = el2.find('.project_percentage').html();

                    $('#GL_project_table tr').each(function () {
                        var el3 = $(this);
                        var box_project_id = el3.find('.project_id').html();

                        if (box_project_id == project_id) {
                            el3.find('.GL_project_percentage').val(numberWithCommas(project_percentage));
                            el3.find('.GL_project_amount').val(numberWithCommas(project_amount));
                            el3.find('.id').html(id);
                        }
                    });

                });
                calculate_total_project();

                $(".GL_project_percentage").on('change', function () {
                    $('#show_error_message_project').hide();
                    var el2 = $(this);
                    var percent = el2.val();
                    var amount = parseFloat(project_total_amount.replace(/,/g, "")) * parseFloat(percent)/100;

                    if (percent != '') {
                        el2.closest('tr').find('.GL_project_amount').val(numberWithCommas(amount.toFixed(2)));
                    } else {
                        el2.closest('tr').find('.GL_project_amount').val('');
                    }

                    calculate_total_project();
                });

                $(".GL_project_amount").on('change', function () {
                    $('#show_error_message_project').hide();
                    var el3 = $(this);
                    var amount = el3.val().replace(/,/g, "");
                    var percent = parseFloat(amount)/parseFloat(project_total_amount.replace(/,/g, "")) *100;

                    if (el3.val() != '') {
                        el3.closest('tr').find('.GL_project_percentage').val(numberWithCommas(percent.toFixed(2)));
                        el3.val(numberWithCommas(parseFloat(amount).toFixed(2)));
                    } else {
                        el3.closest('tr').find('.GL_project_percentage').val('');
                    }

                    calculate_total_project();
                });

                $("#GL_project_submit").on('click', function () {

                    var trans_amount = $('.project_chosen_td').closest('tr').find('.GL_transaction_amount').val();

                    if (parseFloat($("#GL_project_total_amount").html()) == parseFloat(trans_amount)) {

                        $('#GL_transaction_total').css('color', 'black');
                        $('#GL_project_total_amount').css('color', 'black');

                        $('.project_chosen_td .project_data').html('');

                        $('#GL_project_table tr').each(function () {

                            var el1 = $(this);
                            var percentage = el1.find('.GL_project_percentage').val();
                            var amount = el1.find('.GL_project_amount').val();
                            var name = el1.find('.GL_project_name').html();
                            var project_id  = el1.find('.project_id').html();
                            var id  = el1.find('.id').html();

                            if (percentage != '' && amount != '') {

                                if (percentage == 100) {
                                    $('.project_chosen_td .GL_transaction_project').val(project_id);
                                    $('.project_chosen_td .GL_transaction_project_name').val(name);
                                } else {
                                    $('.project_chosen_td .GL_transaction_project_name').val('');
                                    $('.project_chosen_td .GL_transaction_project').val('');
                                }
                                var project_html = '';

                                project_html += '<div class="project_line">';
                                project_html += '<div class="id">'+id+'</div>';
                                project_html += '<div class="project_id">' + project_id + '</div>';
                                project_html += '<div class="project_amount">' + amount.replace(/,/g, "") + '</div>';
                                project_html += '<div class="project_percentage">' + percentage.replace(/,/g, "") + '</div>';
                                project_html += '</div>';
                            }

                            $('.project_chosen_td .project_data').append(project_html);
                            $('#GL_entry_project_select-modal').modal('hide');
                        });

                    } else {
                        $('#error_message_project').html('Total Amount of outlets should be equal to Transaction Amount');
                        $('#GL_transaction_total').css('color', 'red');
                        $('#GL_project_total_amount').css('color', 'red');
                        $('#show_error_message_project').show();
                        return;
                    }
                });
            });

            $('#GL_entry_project_select-modal').on('hidden.bs.modal', function () {
                setTimeout(function(){
                    $('.project_chosen_td').removeClass('project_chosen_td');
                }, 400);
            });
        }

        function calculate_total() {
            var debit_total_amount = 0;
            var credit_total_amount = 0;

            $('#modal-edit-table tr').each(function() {
                var debit_amount = $(this).find('.GL_transaction_debit_amount').val();
                if (debit_amount != '') {
                    debit_total_amount += parseFloat(debit_amount.replace(/,/g, ""));
                }

                var credit_amount = $(this).find('.GL_transaction_credit_amount').val();
                if (credit_amount != '') {
                    credit_total_amount += parseFloat(credit_amount.replace(/,/g, ""));
                }
            });

            $("#GL_transaction_debit_total_amount").html(numberWithCommas(debit_total_amount.toFixed(2)));

            $("#GL_transaction_credit_total_amount").html(numberWithCommas(credit_total_amount.toFixed(2)));
        }

        function calculate_total_project() {
            var total_percent = 0;
            var total_amount = 0;

            $('#GL_project_table tr').each(function() {
                var percent = $(this).find('.GL_project_percentage').val();
                if (percent != '') {
                    total_percent += parseFloat(percent.replace(/,/g, ""));
                }

                var amount = $(this).find('.GL_project_amount').val();
                if (amount != '') {
                    total_amount += parseFloat(amount.replace(/,/g, ""));
                }
            });

            $("#GL_project_total_percentage").html(numberWithCommas(total_percent.toFixed(2))+'%');

            $("#GL_project_total_amount").html(numberWithCommas(total_amount.toFixed(2)));
        }

        function clear_project_modal() {

            $('#GL_transaction_total').css('color', 'black');
            $('#GL_project_total_amount').css('color', 'black');

            $('.GL_project_percentage').val('');
            $('.GL_project_amount').val('');
            $('#show_error_message_project').hide();

            $('#GL_project_total_percentage').html('');
            $('#GL_project_total_amount').html('');
            $('#GL_project_table .id').html('');

        }

        $("#GL_transaction_add").on('click', function () {

            var html = '';

            var number = $('#modal-edit-table tr:last-child').find('.number').html() === undefined ? 1 : parseInt($('#modal-edit-table tr:last-child').find('.number').html()) + 1;

            html += '<tr role="row" class="heading">';
            html += '<td class="number text-right">'+number+'</td>';
            html += '<td class="GL_transaction_id hidden"></td>';
            html += '<td>';
            html += '<input data-toggle="tooltip" data-original-title="" style="width:60%; display:inline"  class="GL_transaction_account_code form-control" type="text" name="GL_transaction_account_code">';
            html += '<a style="width:20%; font-size: 25px;height: 35px;" class="GL_transaction_account_code_button btn btn-md gray">...</a><small class="required_GL_transaction_account_code" style="color:red; display:none;">Account Code is required field</small>';
            html += '<small class="exist_GL_transaction_account_code" style="color:red; display:none;">Account Code does not exist or cannot be selected</small>';
            html += '<small class="prevent_input_GST" style="color:red; display:none;">Input GST cannot be submitted from here</small>';
            html += '</td>';
            html += '<td style="line-height: 2.8; font-size: 12px;" class="GL_transaction_account_name"></td>';
            html += '<td class="hidden">';
            html += '<input style="width:100%" type="text" class="GL_transaction_amount form-control" name="GL_transaction_amount">' +
                '<small class="required_GL_transaction_amount" style="color:red; display:none;">Transaction Amount is required field</small>';
            html += '</td>';
            html += '<td class="hidden">';
            html += '<select name="GL_transaction_type" class="GL_transaction_type form-control">';
            html += '<option selected value="">Please Select</option>';
            html += '<option value="credit">Credit</option>';
            html += '<option value="debit">Debit</option>';
            html += '</select>' +
                '<small class="required_GL_transaction_type" style="color:red; display:none;">Transaction Type is required field</small>';
            html += '</td>';
            html += '<td>';
            html += '<input class="GL_transaction_description form-control" style="width:100%" type="text" name="GL_transaction_description">' +
                '<small class="required_GL_transaction_description" style="color:red; display:none;">Transaction Description is required for Bank Reconciliation</small>';
            html += '</td>';
            html += '<td>' +
                '<input style="width:100%" type="text" class="GL_transaction_debit_amount form-control" name="GL_transaction_amount">';
            html += '</td>';
            html += '<td>' +
                '<input style="width:100%" type="text" class="GL_transaction_credit_amount form-control" name="GL_transaction_amount">';
            html += '</td>';
            html += '<td>';
            html += '<a style="font-size:25px;height: 35px;" class="GL_transaction_project_split btn btn-md gray display-hide">...</a>' +
                '<input type="text" style="width:60%;" class="GL_transaction_project_name form-control display-hide">' +
                '<input type="hidden" class="GL_transaction_project">';
            html += '<div class="project_data hidden">';
            html += '</div>';
            html += '</td>';
            html += '<td>';
            html += '<a href="#" class="btn btn-md red GL_transaction-delete">';
            html += '<i class="fa fa-trash-o"></i>';
            html += '</a>';
            html += '</td>';
            html += '</tr>';

            $('#modal-edit-table').append(html);

            $(".GL_transaction-delete").on('click', function () {
                var el = $(this);
                el.closest('tr').remove();

                calculate_total();
            });

            each_transaction_row();
        });

        $(".GL_entry-delete").on("click",function(){
            var el = $(this);
            var GL_entry_id = el.closest('tr').find('.GL_entry_id').html();
            $('#modal_delete_id').val(GL_entry_id);

            $('#GL_entry-delete-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        function clear_edit_modal() {

            $('#GL_entry_type').val('');
            $('#GL_entry_document_date').val('');
            $('#show_error_message').hide();

            $('#modal-edit-table').html('');
            $('#GL_transaction_credit_total_amount').html('');
            $('#GL_transaction_debit_total_amount').html('');

            $('#GL_entry_description').val('');

            $('#GL_entry_type').css('border-color', '#e5e5e5');
            $('#required_GL_entry_type').hide();
            $('#valid_GL_entry_type').hide();
            $('#dupplicate_GL_opening').hide();

            $('#GL_entry_document_date').css('border-color', '#e5e5e5');
            $('#required_GL_entry_document_date').hide();
            $('#lock_GL_entry_document_date').hide();
            $('#GL_transaction_debit_total_amount').css('color', 'black');
            $('#GL_transaction_credit_total_amount').css('color', 'black');
            $('#total_amount_title').css('color', 'black');

            $('#GL_entry-edit_form input, #GL_entry-edit_form textarea, #GL_entry-edit_form select').prop("disabled", false);
            $('#GL_transaction_add, #GL_entry_submit').show();
        }

        $(".GL_entry-import_pos").on("click",function(){

            $('#required_POS_date').hide();
            $('#GL_entry_pos_date').css('border-color', '#e5e5e5');

            $('#GL_entry-pos_import-modal').modal({
                show: true,
                backdrop: 'static'
            });

            $("#view_pos_report").on("click",function(){
                var pos_date = $('#GL_entry_pos_date').val();

                if (pos_date == '') {
                    $('#required_POS_date').show();
                    $('#GL_entry_pos_date').css('border-color', 'red');
                    return;
                } else {
                    $('#GL_entry-pos_import-modal').modal('hide');

                    $('#GL_entry-modal-title').html('POS Import Data');
                    clear_edit_modal();

                    $('#GL_entry_type').val('GJ');
                    $('#GL_entry_document_date').val(pos_date);
                    $('#GL_entry_description').val('POS entry on date '+pos_date);

                    $.ajax({
                        method: 'POST',
                        url: "<?php echo base_url(); ?>GL_entry/get_pos",
                        data: {
                            pos_date: pos_date
                        },
                        dataType: 'json'
                    }).done(function(res) {
                        var number = 0;
                        $('#modal-edit-table').html('');
                        $.each(res.GL_transactions, function (index, value) {

                            var html_project = '';
                            var trans_project_id = '';
                            var trans_project_name = '';

                            number+=1;

                            $.each(value.project, function (index, item) {

                                if (parseFloat(item.percentage) == 100) {
                                    trans_project_id = item.project_id;
                                    trans_project_name = item.project_name;
                                }

                                html_project += '<div class="project_line">';
                                html_project += '<div class="id"></div>';
                                html_project += '<div class="project_id">'+item.project_id+'</div>';
                                html_project += '<div class="project_amount">'+item.amount+'</div>';
                                html_project += '<div class="project_percentage">'+item.percentage+'</div>';
                                html_project += '</div>';
                            });

                            var html = '';
                            var credit_select = value.type == 'credit' ? 'selected' : '';
                            var debit_select = value.type == 'debit' ? 'selected' : '';
                            var trans_amount = parseFloat(value.amount);

                            html += '<tr role="row" class="heading">';
                            html += '<td class="number text-right">'+number+'</td>';
                            html += '<td class="GL_transaction_id hidden"></td>';
                            html += '<td>';
                            html += '<input data-toggle="tooltip" data-original-title="'+value.account_name+'" style="width:60%; display:inline;" value="'+value.account_code+'" class="GL_transaction_account_code form-control" type="text" name="GL_transaction_account_code">';
                            html += '<a style="width:20%; font-size: 25px;height: 35px;" class="GL_transaction_account_code_button btn btn-md gray">...</a><small class="required_GL_transaction_account_code" style="color:red; display:none;">Account Code is required field</small>';
                            html += '<small class="exist_GL_transaction_account_code" style="color:red; display:none;">Account Code does not exist or cannot be selected</small>';
                            html += '<small class="prevent_input_GST" style="color:red; display:none;">Input GST cannot be submitted from here</small>';
                            html += '</td>';
                            html += '<td style="line-height: 2.8; font-size: 12px;" class="GL_transaction_account_name">'+value.account_name+'</td>';
                            html += '<td class="hidden">';
                            html += '<input style="width:100%" type="text" value="'+numberWithCommas(trans_amount.toFixed(2))+'" class="GL_transaction_amount form-control" name="GL_transaction_amount">' +
                                '<small class="required_GL_transaction_amount" style="color:red; display:none;">Transaction Amount is required field</small>';
                            html += '</td>';
                            html += '<td class="hidden">';
                            html += '<select name="GL_transaction_type" class="GL_transaction_type form-control">';
                            html += '<option selected value="">Please Select</option>';
                            html += '<option '+credit_select+' value="credit">Credit</option>';
                            html += '<option '+debit_select+' value="debit">Debit</option>';
                            html += '</select>' +
                                '<small class="required_GL_transaction_type" style="color:red; display:none;">Transaction Type is required field</small>';
                            html += '</td>';
                            html += '<td>';
                            html += '<input class="GL_transaction_description form-control" style="width:100%" value="'+value.description+'" type="text" name="GL_transaction_description">' +
                                '<small class="required_GL_transaction_description" style="color:red; display:none;">Transaction Description is required for Bank Reconciliation</small>';
                            html += '</td>';
                            html += '<td>' +
                                '<input style="width:100%" type="text" class="GL_transaction_debit_amount form-control" name="GL_transaction_amount">';
                            html += '</td>';
                            html += '<td>' +
                                '<input style="width:100%" type="text" class="GL_transaction_credit_amount form-control" name="GL_transaction_amount">';
                            html += '</td>';
                            html += '<td>';
                            html += '<a style="font-size:25px;height: 35px;" class="GL_transaction_project_split btn btn-md gray">...</a>' +
                                '<input type="text" style="width:60%;" class="GL_transaction_project_name form-control display-hide">' +
                                '<input type="hidden" class="GL_transaction_project">';
                            html += '<div class="project_data hidden">';
                            html += html_project;
                            html += '</div>';
                            html += '</td>';
                            html += '<td>';
                            html += '<a href="#" class="btn btn-md red GL_transaction-delete">';
                            html += '<i class="fa fa-trash-o"></i>';
                            html += '</a>';
                            html += '</td>';
                            html += '</tr>';

                            $('#modal-edit-table').append(html);
                            $('.GL_transaction_project_name').show();

                            if (trans_project_id != '') {
                                $('#modal-edit-table tr:last-child .GL_transaction_project').val(trans_project_id);
                                $('#modal-edit-table tr:last-child .GL_transaction_project_name').val(trans_project_name);
                            }

                            var el = $('#modal-edit-table tr:last-child');
                            var amount = parseFloat(el.find('.GL_transaction_amount').val().replace(/,/g, ""));
                            var type = el.find('.GL_transaction_type').val();

                            if (type == 'debit') {
                                el.find('.GL_transaction_debit_amount').val(numberWithCommas(amount.toFixed(2)));
                                el.find('.GL_transaction_credit_amount').val('0.00');
                            }
                            if (type == 'credit') {
                                el.find('.GL_transaction_debit_amount').val('0.00');
                                el.find('.GL_transaction_credit_amount').val(numberWithCommas(amount.toFixed(2)));
                            }

                        });
                        calculate_total();

                        $(".GL_transaction-delete").on('click', function () {
                            var el = $(this);
                            el.closest('tr').remove();

                            calculate_total();
                        });

                        each_transaction_row();
                    });

                    $('#GL_entry-edit-modal').modal({
                        show: true,
                        backdrop: 'static'
                    });
                }

            });

        });

        $(".GL_entry-import_salary").on("click",function(){

            $('#required_salary_month').hide();
            $('#GL_entry_salary_date').css('border-color', '#e5e5e5');

            $('#GL_entry-salary_import-modal').modal({
                show: true,
                backdrop: 'static'
            });

            $("#view_salary_report").on("click",function(){
                var salary_date = $('#GL_entry_salary_date').val();

                if (salary_date == '') {
                    $('#required_salary_month').show();
                    $('#GL_entry_salary_date').css('border-color', 'red');
                    return;
                } else {
                    $('#required_salary_month').hide();
                    $('#GL_entry-salary_import-modal').modal('hide');

                    $('#GL_entry-modal-title').html('Salary Import Data');
                    clear_edit_modal();

                    $('#GL_entry_type').val('salary');
                    $('#GL_entry_document_date').val(salary_date+'-25');
                    $('#GL_entry_description').val('Salary entry in month '+salary_date);

                    $.ajax({
                        method: 'POST',
                        url: "<?php echo base_url(); ?>GL_entry/get_l12",
                        data: {
                            salary_date: salary_date
                        },
                        dataType: 'json'
                    }).done(function(res) {

                        var number = 0;
                        $.each(res.GL_transactions, function (index, value) {

                            var html_project = '';

                            number+=1;

                            $.each(value.project, function (index, item) {
                                html_project += '<div class="project_line">';
                                html_project += '<div class="id"></div>';
                                html_project += '<div class="project_id">'+item.project_id+'</div>';
                                html_project += '<div class="project_amount">'+item.amount+'</div>';
                                html_project += '<div class="project_percentage">'+item.percentage+'</div>';
                                html_project += '</div>';
                            });

                            var html = '';
                            var credit_select = value.type == 'credit' ? 'selected' : '';
                            var debit_select = value.type == 'debit' ? 'selected' : '';
                            var trans_amount = parseFloat(value.amount);

                            html += '<tr role="row" class="heading">';
                            html += '<td class="number text-right">'+number+'</td>';
                            html += '<td class="GL_transaction_id hidden"></td>';
                            html += '<td>';
                            html += '<input data-toggle="tooltip" data-original-title="'+value.account_name+'" style="width:60%; display:inline;" value="'+value.account_code+'" class="GL_transaction_account_code form-control" type="text" name="GL_transaction_account_code">';
                            html += '<a style="width:20%; font-size: 25px;height: 35px;" class="GL_transaction_account_code_button btn btn-md gray">...</a><small class="required_GL_transaction_account_code" style="color:red; display:none;">Account Code is required field</small>';
                            html += '<small class="exist_GL_transaction_account_code" style="color:red; display:none;">Account Code does not exist or cannot be selected</small>';
                            html += '<small class="prevent_input_GST" style="color:red; display:none;">Input GST cannot be submitted from here</small>';
                            html += '</td>';
                            html += '<td style="line-height: 2.8; font-size: 12px;" class="GL_transaction_account_name">'+value.account_name+'</td>';
                            html += '<td class="hidden">';
                            html += '<input style="width:100%" type="text" value="'+numberWithCommas(trans_amount.toFixed(2))+'" class="GL_transaction_amount form-control" name="GL_transaction_amount">' +
                                '<small class="required_GL_transaction_amount" style="color:red; display:none;">Transaction Amount is required field</small>';
                            html += '</td>';
                            html += '<td class="hidden">';
                            html += '<select name="GL_transaction_type" class="GL_transaction_type form-control">';
                            html += '<option selected value="">Please Select</option>';
                            html += '<option '+credit_select+' value="credit">Credit</option>';
                            html += '<option '+debit_select+' value="debit">Debit</option>';
                            html += '</select>' +
                                '<small class="required_GL_transaction_type" style="color:red; display:none;">Transaction Type is required field</small>';
                            html += '</td>';
                            html += '<td>';
                            html += '<input class="GL_transaction_description form-control" style="width:100%" value="'+value.description+'" type="text" name="GL_transaction_description">' +
                                '<small class="required_GL_transaction_description" style="color:red; display:none;">Transaction Description is required for Bank Reconciliation</small>';
                            html += '</td>';
                            html += '<td>' +
                                '<input style="width:100%" type="text" class="GL_transaction_debit_amount form-control" name="GL_transaction_amount">';
                            html += '</td>';
                            html += '<td>' +
                                '<input style="width:100%" type="text" class="GL_transaction_credit_amount form-control" name="GL_transaction_amount">';
                            html += '</td>';
                            html += '<td>';
                            html += '<a style="font-size:25px;height: 35px;" class="GL_transaction_project_split btn btn-md gray">...</a>' +
                                '<input type="text" style="width:60%;" class="GL_transaction_project_name form-control display-hide">' +
                                '<input type="hidden" class="GL_transaction_project">';
                            html += '<div class="project_data hidden">';
                            html += html_project;
                            html += '</div>';
                            html += '</td>';
                            html += '<td>';
                            html += '<a href="#" class="btn btn-md red GL_transaction-delete">';
                            html += '<i class="fa fa-trash-o"></i>';
                            html += '</a>';
                            html += '</td>';
                            html += '</tr>';

                            $('#modal-edit-table').append(html);

                            var el = $('#modal-edit-table tr:last-child');
                            var amount = parseFloat(el.find('.GL_transaction_amount').val().replace(/,/g, ""));
                            var type = el.find('.GL_transaction_type').val();

                            if (type == 'debit') {
                                el.find('.GL_transaction_debit_amount').val(numberWithCommas(amount.toFixed(2)));
                                el.find('.GL_transaction_credit_amount').val('0.00');
                            }
                            if (type == 'credit') {
                                el.find('.GL_transaction_debit_amount').val('0.00');
                                el.find('.GL_transaction_credit_amount').val(numberWithCommas(amount.toFixed(2)));
                            }

                        });
                        calculate_total();

                        $(".GL_transaction-delete").on('click', function () {
                            var el = $(this);
                            el.closest('tr').remove();

                            calculate_total();
                        });

                        each_transaction_row();
                    });

                    $('#GL_entry-edit-modal').modal({
                        show: true,
                        backdrop: 'static'
                    });
                }

            });

        });

        $(".GL_entry-add").on("click",function(){

            $('#modal_id').val('');
            $('#GL_entry-modal-title').html('Add GL Entry');
            clear_edit_modal();

            $('#GL_entry_document_date').val(document_date_default);

            var html = '';

            html += '<tr role="row" class="heading">';
            html += '<td class="number text-right">1</td>';
            html += '<td class="GL_transaction_id hidden"></td>';
            html += '<td><input data-toggle="tooltip" data-original-title="" style="width:60%; display:inline;"  class="GL_transaction_account_code form-control" type="text" name="GL_transaction_account_code"><a style="width:20%; font-size: 25px;height: 35px;" class="GL_transaction_account_code_button btn btn-md gray">...</a>' +
                '<small class="required_GL_transaction_account_code" style="color:red; display:none;">Account Code is required field</small>';
            html += '<small class="exist_GL_transaction_account_code" style="color:red; display:none;">Account Code does not exist or cannot be selected</small>';
            html += '<small class="prevent_input_GST" style="color:red; display:none;">Input GST cannot be submitted from here</small></td>';
            html += '<td style="line-height: 2.8; font-size: 12px;" class="GL_transaction_account_name"></td>';
            html += '<td class="hidden"><input style="width:100%" type="text" class="GL_transaction_amount form-control" name="GL_transaction_amount"></td>' +
                '<small class="required_GL_transaction_amount" style="color:red; display:none;">Transaction Amount is required field</small>';
            html += '<td class="hidden">';
            html += '<select name="GL_transaction_type" class="GL_transaction_type form-control">';
            html += '<option selected value="">Please Select</option>';
            html += '<option value="credit">Credit</option>';
            html += '<option value="debit">Debit</option>';
            html += '</select>' +
                '<small class="required_GL_transaction_type" style="color:red; display:none;">Transaction Type is required field</small>';
            html += '</td>';
            html += '<td>';
            html += '<input class="GL_transaction_description form-control" style="width:100%" type="text" name="GL_transaction_description">' +
                '<small class="required_GL_transaction_description" style="color:red; display:none;">Transaction Description is required for Bank Reconciliation</small>';
            html += '</td>';
            html += '<td>' +
                '<input style="width:100%" type="text" class="GL_transaction_debit_amount form-control" name="GL_transaction_amount">';
            html += '</td>';
            html += '<td>' +
                '<input style="width:100%" type="text" class="GL_transaction_credit_amount form-control" name="GL_transaction_amount">';
            html += '</td>';
            html += '<td>';
            html += '<a style="font-size:25px;height: 35px;" class="GL_transaction_project_split btn btn-md gray display-hide">...</a>' +
                '<input type="text" style="width:60%;" class="GL_transaction_project_name form-control display-hide">' +
                '<input type="hidden" class="GL_transaction_project">';
            html += '<div class="project_data hidden">';
            html += '</div>';
            html += '</td>';
            html += '<td>';
            html += '<a href="#" class="btn btn-md red GL_transaction-delete">';
            html += '<i class="fa fa-trash-o"></i>';
            html += '</td>';
            html += '</tr>';


            html += '<tr role="row" class="heading">';
            html += '<td class="number text-right">2</td>';
            html += '<td class="GL_transaction_id hidden"></td>';
            html += '<td><input data-toggle="tooltip" data-original-title="" style="width:60%; display:inline;"  class="GL_transaction_account_code form-control" type="text" name="GL_transaction_account_code"><a style="width:20%; font-size: 25px;height: 35px;" class="GL_transaction_account_code_button btn btn-md gray">...</a>' +
                '<small class="required_GL_transaction_account_code" style="color:red; display:none;">Account Code is required field</small>';
            html += '<small class="exist_GL_transaction_account_code" style="color:red; display:none;">Account Code does not exist or cannot be selected</small>';
            html += '<small class="prevent_input_GST" style="color:red; display:none;">Input GST cannot be submitted from here</small></td>';
            html += '<td style="line-height: 2.8; font-size: 12px;" class="GL_transaction_account_name"></td>';
            html += '<td class="hidden"><input style="width:100%" type="text" class="GL_transaction_amount form-control" name="GL_transaction_amount"></td>' +
                '<small class="required_GL_transaction_amount" style="color:red; display:none;">Transaction Amount is required field</small>';
            html += '<td class="hidden">';
            html += '<select name="GL_transaction_type" class="GL_transaction_type form-control">';
            html += '<option selected value="">Please Select</option>';
            html += '<option value="credit">Credit</option>';
            html += '<option value="debit">Debit</option>';
            html += '</select>' +
                '<small class="required_GL_transaction_type" style="color:red; display:none;">Transaction Type is required field</small>';
            html += '</td>';
            html += '<td>';
            html += '<input class="GL_transaction_description form-control" style="width:100%" type="text" name="GL_transaction_description">' +
                '<small class="required_GL_transaction_description" style="color:red; display:none;">Transaction Description is required for Bank Reconciliation</small>';
            html += '</td>';
            html += '<td>' +
                '<input style="width:100%" type="text" class="GL_transaction_debit_amount form-control" name="GL_transaction_amount">';
            html += '</td>';
            html += '<td>' +
                '<input style="width:100%" type="text" class="GL_transaction_credit_amount form-control" name="GL_transaction_amount">';
            html += '</td>';
            html += '<td>';
            html += '<a style="font-size:25px;height: 35px;" class="GL_transaction_project_split btn btn-md gray display-hide">...</a>' +
                '<input type="text" style="width:60%;" class="GL_transaction_project_name form-control display-hide">' +
                '<input type="hidden" class="GL_transaction_project">';
            html += '<div class="project_data hidden">';
            html += '</div>';
            html += '</td>';
            html += '<td>';
            html += '<a href="#" class="btn btn-md red GL_transaction-delete">';
            html += '<i class="fa fa-trash-o"></i>';
            html += '</td>';
            html += '</tr>';

            html += '<tr role="row" class="heading">';
            html += '<td class="number text-right">3</td>';
            html += '<td class="GL_transaction_id hidden"></td>';
            html += '<td><input data-toggle="tooltip" data-original-title="" style="width:60%; display:inline;"  class="GL_transaction_account_code form-control" type="text" name="GL_transaction_account_code"><a style="width:20%; font-size: 25px;height: 35px;" class="GL_transaction_account_code_button btn btn-md gray">...</a>' +
                '<small class="required_GL_transaction_account_code" style="color:red; display:none;">Account Code is required field</small>';
            html += '<small class="exist_GL_transaction_account_code" style="color:red; display:none;">Account Code does not exist or cannot be selected</small>';
            html += '<small class="prevent_input_GST" style="color:red; display:none;">Input GST cannot be submitted from here</small></td>';
            html += '<td style="line-height: 2.8; font-size: 12px;" class="GL_transaction_account_name"></td>';
            html += '<td class="hidden"><input style="width:100%" type="text" class="GL_transaction_amount form-control" name="GL_transaction_amount"></td>' +
                '<small class="required_GL_transaction_amount" style="color:red; display:none;">Transaction Amount is required field</small>';
            html += '<td class="hidden">';
            html += '<select name="GL_transaction_type" class="GL_transaction_type form-control">';
            html += '<option selected value="">Please Select</option>';
            html += '<option value="credit">Credit</option>';
            html += '<option value="debit">Debit</option>';
            html += '</select>' +
                '<small class="required_GL_transaction_type" style="color:red; display:none;">Transaction Type is required field</small>';
            html += '</td>';
            html += '<td>';
            html += '<input class="GL_transaction_description form-control" style="width:100%" type="text" name="GL_transaction_description">' +
                '<small class="required_GL_transaction_description" style="color:red; display:none;">Transaction Description is required for Bank Reconciliation</small>';
            html += '</td>';
            html += '<td>' +
                '<input style="width:100%" type="text" class="GL_transaction_debit_amount form-control" name="GL_transaction_amount">';
            html += '</td>';
            html += '<td>' +
                '<input style="width:100%" type="text" class="GL_transaction_credit_amount form-control" name="GL_transaction_amount">';
            html += '</td>';
            html += '<td>';
            html += '<a style="font-size:25px;height: 35px;" class="GL_transaction_project_split btn btn-md gray display-hide">...</a>' +
                '<input type="text" style="width:60%;" class="GL_transaction_project_name form-control display-hide">' +
                '<input type="hidden" class="GL_transaction_project">';
            html += '<div class="project_data hidden">';
            html += '</div>';
            html += '</td>';
            html += '<td>';
            html += '<a href="#" class="btn btn-md red GL_transaction-delete">';
            html += '<i class="fa fa-trash-o"></i>';
            html += '</td>';
            html += '</tr>';

            $('#modal-edit-table').html(html);

            each_transaction_row();

            $(".GL_transaction-delete").on('click', function () {
                var el = $(this);
                el.closest('tr').remove();

                calculate_total();
            });

            $('#GL_entry-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $(".GL_entry-edit").on("click",function(){
            var el = $(this);
            var id = el.closest('tr').find('.GL_entry_id').html();

            $('#show_error_message').hide();

            $('#modal_id').val(id);

            $('#GL_entry-modal-title').html('Edit GL Entry');
            clear_edit_modal();

            $.ajax({
                method: 'POST',
                url: "<?php echo base_url(); ?>GL_entry/get_detail",
                data: {
                    id: id
                },
                dataType: 'json'
            }).done(function(res) {
                $('#GL_entry_type').val(res.GL_entry.type);
                $('#GL_entry_document_date').val(res.GL_entry.document_date);
                $('#GL_entry_description').val(res.GL_entry.description);

                if (res.GL_entry.type == 'pos' || res.GL_entry.type == 'salary' || new Date(locked_financial_month) > new Date(res.GL_entry.document_date)) {
                    $('#GL_entry-edit_form input, #GL_entry-edit_form textarea, #GL_entry-edit_form select').prop("disabled", true);
                    $('#GL_transaction_add, #GL_entry_submit').hide();

                } else {
                    $('#GL_entry-edit_form input, #GL_entry-edit_form textarea, #GL_entry-edit_form select').prop("disabled", false);
                    $('#GL_transaction_add, #GL_entry_submit').show();

                }

                var number = 0;
                $.each(res.GL_transactions, function (index, value) {

                    var html_project = '';
                    var trans_project_id = '';
                    var trans_project_name = '';

                    number+=1;

                    $.each(value.project, function (index, item) {

                        if (item.percentage == '100.00') {
                            trans_project_id = item.project_id;
                            trans_project_name = item.project_name;
                        }

                        html_project += '<div class="project_line">';
                        html_project += '<div class="id">'+item.id+'</div>';
                        html_project += '<div class="project_id">'+item.project_id+'</div>';
                        html_project += '<div class="project_amount">'+item.amount+'</div>';
                        html_project += '<div class="project_percentage">'+item.percentage+'</div>';
                        html_project += '</div>';
                    });

                    var html = '';
                    var credit_select = value.type == 'credit' ? 'selected' : '';
                    var debit_select = value.type == 'debit' ? 'selected' : '';
                    var trans_amount = parseFloat(value.amount);

                    html += '<tr role="row" class="heading">';
                    html += '<td class="number text-right">'+number+'</td>';
                    html += '<td class="GL_transaction_id hidden">'+value.GL_transaction_id+'</td>';
                    html += '<td>';
                    html += '<input data-toggle="tooltip" data-original-title="'+value.account_name+'" style="width:60%; display:inline;" value="'+value.account_code+'" class="GL_transaction_account_code form-control" type="text" name="GL_transaction_account_code">';
                    html += '<a style="width:20%; font-size: 25px;height: 35px;" class="GL_transaction_account_code_button btn btn-md gray">...</a><small class="required_GL_transaction_account_code" style="color:red; display:none;">Account Code is required field</small>';
                    html += '<small class="exist_GL_transaction_account_code" style="color:red; display:none;">Account Code does not exist or cannot be selected</small>';
                    html += '<small class="prevent_input_GST" style="color:red; display:none;">Input GST cannot be submitted from here</small>';
                    html += '</td>';
                    html += '<td style="line-height: 2.8; font-size: 12px;" class="GL_transaction_account_name">'+value.account_name+'</td>';
                    html += '<td class="hidden">';
                    html += '<input style="width:100%" type="text" value="'+numberWithCommas(trans_amount.toFixed(2))+'" class="GL_transaction_amount form-control" name="GL_transaction_amount">' +
                        '<small class="required_GL_transaction_amount" style="color:red; display:none;">Transaction Amount is required field</small>';
                    html += '</td>';
                    html += '<td class="hidden">';
                    html += '<select name="GL_transaction_type" class="GL_transaction_type form-control">';
                    html += '<option selected value="">Please Select</option>';
                    html += '<option '+credit_select+' value="credit">Credit</option>';
                    html += '<option '+debit_select+' value="debit">Debit</option>';
                    html += '</select>' +
                        '<small class="required_GL_transaction_type" style="color:red; display:none;">Transaction Type is required field</small>';
                    html += '</td>';
                    html += '<td>';
                    html += '<input class="GL_transaction_description form-control" style="width:100%" value="'+value.description+'" type="text" name="GL_transaction_description">' +
                        '<small class="required_GL_transaction_description" style="color:red; display:none;">Transaction Description is required for Bank Reconciliation</small>';
                    html += '</td>';
                    html += '<td>' +
                        '<input style="width:100%" type="text" class="GL_transaction_debit_amount form-control" name="GL_transaction_amount">';
                    html += '</td>';
                    html += '<td>' +
                        '<input style="width:100%" type="text" class="GL_transaction_credit_amount form-control" name="GL_transaction_amount">';
                    html += '</td>';
                    html += '<td>';
                    html += '<a style="font-size:25px;height: 35px;" class="GL_transaction_project_split btn btn-md gray">...</a>' +
                        '<input type="text" style="width:60%;" class="GL_transaction_project_name form-control display-hide">' +
                        '<input type="hidden" class="GL_transaction_project">';
                    html += '<div class="project_data hidden">';
                    html += html_project;
                    html += '</div>';
                    html += '</td>';
                    html += '<td>';
                    html += '<a href="#" class="btn btn-md red GL_transaction-delete">';
                    html += '<i class="fa fa-trash-o"></i>';
                    html += '</a>';
                    html += '</td>';
                    html += '</tr>';

                    $('#modal-edit-table').append(html);
                    $('.GL_transaction_project_name').show();

                    if (trans_project_id != '') {
                        $('#modal-edit-table tr:last-child .GL_transaction_project').val(trans_project_id);
                        $('#modal-edit-table tr:last-child .GL_transaction_project_name').val(trans_project_name);
                    }

                    if (value.clear_date != null || value.sales_collection_id != null) {

                        $('#modal-edit-table tr:last-child input').prop("disabled", true);
                        $('#modal-edit-table tr:last-child .GL_transaction_account_code_button').hide();
                        $('#modal-edit-table tr:last-child .GL_transaction-delete').hide();
                        $('#modal-edit-table tr:last-child .GL_transaction_project_split').hide();
                    }

                    if (res.GL_entry.type == 'pos' || res.GL_entry.type == 'salary' || new Date(locked_financial_month) > new Date(res.GL_entry.document_date)) {
                        $('#modal-edit-table tr:last-child input').prop("disabled", true);
                        $('#modal-edit-table tr:last-child .GL_transaction_account_code_button, #modal-edit-table tr:last-child .GL_transaction_project_split, #modal-edit-table tr:last-child .GL_transaction-delete').hide();

                    }

                    var el = $('#modal-edit-table tr:last-child');
                    var amount = parseFloat(el.find('.GL_transaction_amount').val().replace(/,/g, ""));
                    var type = el.find('.GL_transaction_type').val();

                    if (type == 'debit') {
                        el.find('.GL_transaction_debit_amount').val(numberWithCommas(amount.toFixed(2)));
                        el.find('.GL_transaction_credit_amount').val('0.00');
                    }
                    if (type == 'credit') {
                        el.find('.GL_transaction_debit_amount').val('0.00');
                        el.find('.GL_transaction_credit_amount').val(numberWithCommas(amount.toFixed(2)));
                    }

                });
                calculate_total();

                $(".GL_transaction-delete").on('click', function () {
                    var el = $(this);
                    el.closest('tr').remove();

                    calculate_total();
                });

                each_transaction_row();
            });


            $('#GL_entry-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });

        $("#GL_entry_submit").on("click",function(){

            $(this).prop("disabled", true);

            var GL_entry_type = $('#GL_entry_type').val();
            var GL_entry_document_date = $('#GL_entry_document_date').val();
            var GL_entry_description = $('#GL_entry_description').val();
            var GL_transaction_credit_total_amount = $('#GL_transaction_credit_total_amount').html();
            var GL_transaction_debit_total_amount = $('#GL_transaction_debit_total_amount').html();
            var modal_id = $('#modal_id').val();

            var error_message = false;

            if (GL_entry_type == '') {
                error_message = true;
                $('#GL_entry_type').css('border-color', 'red');
                $('#required_GL_entry_type').show();
                $('#GL_entry_type').focus();
            } else {
                $('#GL_entry_type').css('border-color', '#e5e5e5');
                $('#required_GL_entry_type').hide();

                if (GL_entry_type == 'pos' || GL_entry_type == 'salary') {
                    error_message = true;
                    $('#GL_entry_type').css('border-color', 'red');
                    $('#valid_GL_entry_type').show();
                } else {
                    $('#GL_entry_type').css('border-color', '#e5e5e5');
                    $('#valid_GL_entry_type').hide();
                    
                    if (GL_entry_type == 'opening') {

                        $.ajax({
                            async:false,
                            method: 'POST',
                            url: "<?php echo base_url(); ?>GL_entry/check_opening",
                            dataType: 'json'
                        }).done(function (res) {

                            if(res.id == null || (res.id != null && res.id == modal_id)) {
                                $('#GL_entry_type').css('border-color', '#e5e5e5');
                                $('#dupplicate_GL_opening').hide();
                            } else {
                                error_message = true;
                                $('#GL_entry_type').css('border-color', 'red');
                                $('#dupplicate_GL_opening').show();
                            }

                        });

                    } else {
                        $('#GL_entry_type').css('border-color', '#e5e5e5');
                        $('#dupplicate_GL_opening').hide();
                    }
                }
            }

            if (GL_entry_document_date == '') {
                error_message = true;
                $('#GL_entry_document_date').css('border-color', 'red');
                $('#required_GL_entry_document_date').show();
                $('#GL_entry_document_date').focus();
            } else {
                $('#GL_entry_document_date').css('border-color', '#e5e5e5');
                $('#required_GL_entry_document_date').hide();

                if (new Date(locked_financial_month) > new Date(GL_entry_document_date)) {
                    error_message = true;
                    $('#GL_entry_document_date').css('border-color', 'red');
                    $('#lock_GL_entry_document_date').show();
                } else {
                    $('#GL_entry_document_date').css('border-color', '#e5e5e5');
                    $('#lock_GL_entry_document_date').hide();
                }
            }

            var account_codes = [];
            var transactions = [];

            $('.GL_transaction_account_code').each(function() {

                var el = $(this);

                if (el.val() == '') {
                    el.css('border-color', 'red');
                    el.closest('td').find('.required_GL_transaction_account_code').show();
                    el.closest('td').find('.GL_transaction_account_code_button').css('color', 'red');
                    el.focus();
                    error_message = true;
                } else {
                    el.css('border-color', '#e5e5e5');
                    el.closest('td').find('.required_GL_transaction_account_code').hide();
                    el.closest('td').find('.GL_transaction_account_code_button').css('color', '#428bca');

                    if (GL_entry_type != '') {
                        if ((parseFloat(el.val()) >= 13031 && parseFloat(el.val()) <= 13048 && GL_entry_type != 'opening')
                            || (parseFloat(el.val()) >= 13011 && parseFloat(el.val()) <= 13029 && GL_entry_type != 'opening')) {

                            if (el.closest('tr').find('.GL_transaction_description').val() == '') {
                                el.closest('tr').find('.GL_transaction_description').css('border-color', 'red');
                                el.closest('tr').find('.required_GL_transaction_description').show();
                                error_message = true;

                            } else {
                                el.closest('tr').find('.GL_transaction_description').css('border-color', '#e5e5e5');
                                el.closest('tr').find('.required_GL_transaction_description').hide();
                            }

                        }
                    }

                    if (el.val() == '21403') {
                        el.css('border-color', 'red');
                        el.closest('td').find('.prevent_input_GST').show();
                        el.closest('td').find('.GL_transaction_account_code_button').css('color', 'red');
                        el.focus();
                        error_message = true;
                    } else {
                        el.css('border-color', '#e5e5e5');
                        el.closest('td').find('.prevent_input_GST').hide();
                        el.closest('td').find('.GL_transaction_account_code_button').css('color', '#428bca');
                    }
                }

                if (el.closest('tr').find('.GL_transaction_amount').val() == '') {
                    el.closest('tr').find('.GL_transaction_amount').css('border-color', 'red');
                    el.closest('tr').find('.required_GL_transaction_amount').show();
                    el.closest('tr').find('.GL_transaction_amount').focus();
                    error_message = true;
                } else {
                    el.closest('tr').find('.GL_transaction_amount').css('border-color', '#e5e5e5');
                    el.closest('tr').find('.required_GL_transaction_amount').hide();
                }

                if (el.closest('tr').find('.GL_transaction_type').val() == '') {
                    el.closest('tr').find('.GL_transaction_type').css('border-color', 'red');
                    el.closest('tr').find('.required_GL_transaction_type').show();
                    el.closest('tr').find('.GL_transaction_type').focus();
                    error_message = true;
                } else {
                    el.closest('tr').find('.GL_transaction_type').css('border-color', '#e5e5e5');
                    el.closest('tr').find('.required_GL_transaction_type').hide();
                }

                if (el.val() != '') {

                    account_codes.push(el.val());
                }

                var transaction = {};
                transaction.account_code = el.val();
                transaction.id = el.closest('tr').find('.GL_transaction_id').html();
                transaction.amount = parseFloat(el.closest('tr').find('.GL_transaction_amount').val().replace(/,/g, ""));
                transaction.type = el.closest('tr').find('.GL_transaction_type').val();
                transaction.description = el.closest('tr').find('.GL_transaction_description').val();

                transaction.project = [];
                el.closest('tr').find('.project_data').find('.project_line').each(function() {
                    var project = {};
                    var el1 = $(this);
                    project.project_id = el1.find('.project_id').html();
                    project.id = el1.find('.id').html();
                    project.amount = el1.find('.project_amount').html();
                    project.percentage = el1.find('.project_percentage').html();

                    transaction.project.push(project);
                });

                if (transaction.project.length == 0) {
                    $('#error_message').html('Transaction Project is required');
                    $('#show_error_message').show();
                    el.closest('tr').find('.GL_transaction_project_split').css('color', 'red');
                    error_message = true;
                } else {

                    $('#show_error_message').hide();
                    el.closest('tr').find('.GL_transaction_project_split').css('color', '#428bca');
                }

                transactions.push(transaction);
            });

            if (error_message == true) {
                $(this).prop("disabled", false);
                return;
            }

            if (GL_transaction_credit_total_amount != GL_transaction_debit_total_amount) {
                $('#error_message').html('Total Credit and Debit Amounts should be equal');
                $('#GL_transaction_debit_total_amount').css('color', 'red');
                $('#GL_transaction_credit_total_amount').css('color', 'red');
                $('#total_amount_title').css('color', 'red');
                $('#show_error_message').show();
                $(this).prop("disabled", false);
                return;
            } else {
                $('#GL_transaction_debit_total_amount').css('color', 'black');
                $('#GL_transaction_credit_total_amount').css('color', 'black');
                $('#total_amount_title').css('color', 'black');
            }

            if (GL_transaction_credit_total_amount == '0.00' || GL_transaction_debit_total_amount == '0.00') {
                $('#error_message').html('Transaction Amount should be more than 0');
                $('#GL_transaction_debit_total_amount').css('color', 'red');
                $('#GL_transaction_credit_total_amount').css('color', 'red');
                $('#total_amount_title').css('color', 'red');
                $('#show_error_message').show();
                $(this).prop("disabled", false);
                return;
            } else {
                $('#GL_transaction_debit_total_amount').css('color', 'black');
                $('#GL_transaction_credit_total_amount').css('color', 'black');
                $('#total_amount_title').css('color', 'black');
            }

            $.ajax({
                method: 'POST',
                url: "<?php echo base_url(); ?>GL_entry/check_edit",
                data: {
                    account_codes: account_codes
                },
                dataType: 'json'
            }).done(function(res) {
                if (res.error != false) {

                    $('.GL_transaction_account_code').each(function() {

                        var el = $(this);

                        if (jQuery.inArray( el.val(), res.codes ) >= 0) {
                            el.css('border-color', 'red');
                            el.closest('td').find('.GL_transaction_account_code_button').css('color', 'red');
                            el.closest('td').find('.exist_GL_transaction_account_code').show();
                            el.focus();
                        } else {
                            el.css('border-color', '#e5e5e5');
                            el.closest('td').find('.GL_transaction_account_code_button').css('color', '#428bca');
                            el.closest('td').find('.exist_GL_transaction_account_code').hide();
                        }
                    });

                    error_message = true;
                    $(this).prop("disabled", false);
                    return;
                } else {
                    $.ajax({
                        method: 'POST',
                        url: "<?php echo base_url(); ?>GL_entry/edit",
                        data: {
                            id: modal_id,
                            type: GL_entry_type,
                            document_date: GL_entry_document_date,
                            description: GL_entry_description,
                            total_amount: parseFloat(GL_transaction_credit_total_amount.replace(/,/g, "")),
                            transactions: transactions
                        },
                        dataType: 'json'
                    }).done(function(res) {
                        if (res.success == true) {
                            $('#GL_entry-edit-modal').addClass('submit_GL_entry');
                            window.location.reload();
                        }
                    });
                }
            });
        });

        var exit = false;
        $('#GL_entry-edit-modal').on('hide.bs.modal', function(e){

            $('#confirm_exit_edit-modal').modal('show');

            return exit;
        });

        $('#confirm_exit').on("click",function(){
            exit = true;
            $('#GL_entry-edit-modal').modal('hide');
        });

        $('#GL_entry-edit-modal').on('hidden.bs.modal', function(e){
            exit = false;
        });

        $('#accounts_search').on("click",function(){
            var account_keyword = $('#account_keyword').val();
            if (account_keyword != '') {

                account_keyword = account_keyword.toLowerCase();

                $('#accounts_list .choose_account').each(function () {
                    var el = $(this);
                    var modal_account_name = el.find('.modal_account_name').html().toLowerCase();

                    var modal_account_code = el.find('.modal_account_code').html().toLowerCase();

                    if (modal_account_name.indexOf(account_keyword) >= 0
                        || modal_account_code.indexOf(account_keyword) >= 0) {
                        el.show();
                    } else {
                        el.hide();
                    }
                });
            } else {
                $('#accounts_list .choose_account').show();
            }
        });
    });
    window.onbeforeunload = function(e) {

        if ($('#GL_entry-edit-modal').hasClass('submit_GL_entry')) {
            return;
        }

        if ($('#GL_entry-edit-modal').hasClass('in')) {
            var dialogText = 'Dialog text here';
            e.returnValue = dialogText;
            return dialogText;
        }
    };
</script>
