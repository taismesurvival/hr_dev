<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    Outlet
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url('company') ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo base_url('company') ?>">Company Dashboard</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="#">Payment Method</a>
        </li>
</div>
<!-- END PAGE HEADER -->
<!-- BEGIN ALERT BOX -->
<?php if (isset($flash_message)) { ?>
    <div class="row margin-bottom-20">
        <div class="col-xs-12">
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <?php echo $flash_message; ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- END ALERT BOX -->

<div class="row">
    <div class="col-md-12">

        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-list font-dark"></i>
                    <span class="caption-subject font-dark bold uppercase">Payment Method</span>
                </div>
            </div>
            <div class="portlet-body">

                <div class="row">
                    <div class="col-md-12">

                        <!-- Begin: life time stats -->
                        <div class="table-container">

                            <!-- Begin: life time stats -->
                            <table style="margin-top: 20px;" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="7%" class="text-right">No</th>
                                    <th width="11%">Name</th>
                                    <th width="11%">Default Bank Account</th>
                                    <th width="13%">Default Bank Charge Rate (%)</th>
                                    <th width="10%">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php for ($i = 0; $i < count($payment_methods); $i++) { ?>
                                    <tr role="row" class="heading">
                                        <td class="text-right"><?php echo $i+1 ?></td>
                                        <td class="payment_method_id hidden"><?php echo $payment_methods[$i]['id'] ?></td>
                                        <td class="name"><?php echo $payment_methods[$i]['name']; ?></td>
                                        <td><?php echo !empty($payment_methods[$i]['account_name']) ? $payment_methods[$i]['account_name'].' ('.$payment_methods[$i]['account_number'].')' : ''; ?></td>
                                        <td class="default_bank_account_id hidden"><?php echo $payment_methods[$i]['default_bank_account_id']; ?></td>
                                        <td class="default_bank_charge_rate"><?php echo !empty($payment_methods[$i]['default_bank_charge_rate']) ? number_format($payment_methods[$i]['default_bank_charge_rate'],2) : ''; ?></td>
                                        <td>
                                            <a class="payment_method-edit btn btn-md blue"><i class="fa fa-pencil"></i> Edit</a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>

                        </div>
                        <!-- End: life time stats -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="payment_method-edit-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 id="payment_method-modal-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">

                <div id="show_error_message" class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span id="error_message"></span>
                </div>

                <?php echo form_open_multipart(base_url('payment_method/edit'), 'class="form-horizontal" method="post" data-parsley-validate id="payment_method-edit_form"'); ?>

                <div class="form-group">
                    <?php echo form_label('Payment Method Name', 'name', array('class' => 'col-md-6 col-lg-5 control-label')); ?>

                    <div class="col-md-6 col-lg-7 form-group">
                        <?php echo form_input('name', set_value('name', ''), 'class="form-control" id="modal_name" disabled'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?php echo form_label('Default Bank Charge Rate (%)', 'default_bank_charge_rate', array('class' => 'col-md-6 col-lg-5 control-label')); ?>

                    <div class="col-md-6 col-lg-7 form-group">
                        <?php echo form_input('default_bank_charge_rate', set_value('default_bank_charge_rate', ''), 'class="form-control" id="modal_default_bank_charge_rate"'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-7 col-lg-5 control-label">Default Bank Account</label>

                    <div class="col-md-6 col-lg-7 form-group">
                        <?php echo form_dropdown('default_bank_account_id', $accounts_array, set_value('default_bank_account_id', ''), 'id="modal_default_bank_account_id" class="form-control"'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" id="modal_id">
                <button  type="button" class="btn btn-md blue" id="payment_method_submit">Submit</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script>
    jQuery(document).ready(function () {

        $("#payment_method_submit").on("click",function(){

            $('#show_error_message').hide();
            $("#payment_method-edit_form").submit();
        });

        $(".payment_method-edit").on("click",function(){

            var el = $(this);
            var name = el.closest('tr').find('.name').html();
            var default_bank_charge_rate = el.closest('tr').find('.default_bank_charge_rate').html();
            var default_bank_account_id = el.closest('tr').find('.default_bank_account_id').html();
            var payment_method_id = el.closest('tr').find('.payment_method_id').html();

            $('#show_error_message').hide();

            $('#modal_name').val(name);
            $('#modal_default_bank_charge_rate').val(default_bank_charge_rate);
            $('#modal_default_bank_account_id').val(default_bank_account_id);
            $('#modal_id').val(payment_method_id);

            $('#payment_method-modal-title').html('Edit Payment Method');

            $('#payment_method-edit-modal').modal({
                show: true,
                backdrop: 'static'
            });
        });
    });
</script>
