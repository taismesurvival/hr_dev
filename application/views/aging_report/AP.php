<style>
    table { border-collapse: collapse; }
    tr.border-top { border-top: solid thin; }
    tr.border-bottom { border-bottom: solid thin; }
    tr.bold { font-weight: bold; }
    * { font-size: 12px; font-family: Lucida Sans Unicode; }
</style>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/slider-pips/jquery-ui.js"></script>
<button type="button" id="download_excel_file">Download Excel Report</button>
<button type="button" id="download_pdf_file">Print or Download PDF Report</button>
<div id="table_wrapper">
    <table style='width:100%;'>
        <tr>
            <td style='text-align:left;'>
                <table style='color:blue;width:100%;'>
                    <tr>
                        <td style='font-size:20px;text-align:center;'><?php echo $company_name; ?></td>
                    </tr>
                    <tr>
                        <td style='font-size:16px;text-align:center;'><?php echo $selected_date; ?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style='text-align:center;font-size:18px;'>Aging Report [Account Payables]</td>
        </tr>
    </table>
    <table style='width:100%;'>
        <thead>
            <tr class='border-top'>
                <th style='width:20%;  text-align:center;'>Supplier Reference No</th>
                <th style='width:20%;  text-align:center;'>Reference No</th>
                <th style='width:7%;  text-align:center;'>Date</th>
                <th style='width:7%; text-align:center;'>Total Due</th>
                <th style='width:7%; text-align:center;'>0-30</th>
                <th style='width:7%; text-align:center;'>31-60</th>
                <th style='width:7%; text-align:center;'>61-90</th>
                <th style='width:7%; text-align:center;'>90+</th>
            </tr>
        </thead>
        <tbody id="accounts_table">
            <?php
            $total_amount = [
                'total_amount' => 0,
                '0to30' => 0,
                '31to60' => 0,
                '61to90' => 0,
                '90plus' => 0
            ];

            foreach($purchases_not_done AS $key => $item) {
                $day_difference = (strtotime($selected_date) - strtotime($item['document_date']))/86400;
                ?>
            <?php if ($key == 0 || $purchases_not_done[$key-1]['supplier_id'] != $item['supplier_id']) {
                    $total_amount = [
                        'total_amount' => 0,
                        '0to30' => 0,
                        '31to60' => 0,
                        '61to90' => 0,
                        '90plus' => 0
                    ];
                    ?>
            <tr>
                <td style="font-weight: bold; font-size: 14px;"><?php echo $item['supplier_name']; ?></td>
            </tr>
            <?php }
            $total_amount['total_amount'] += $item['total_amount'];

            $unpaid_amount = $item['total_amount'];

            if ($item['status'] == 'processing') {
                $unpaid_amount = $unpaid_partial[$item['supplier_id']][$item['refference_no']];
            }

                ?>
            <tr>
                <td style="padding-left: 15px;"><?php echo $item['refference_no']; ?></td>
                <td style="text-align: center;"><?php echo $item['total_amount'] >= 0 ? 'PE'.$item['id'] : 'CN'.$item['id']; ?></td>
                <td style="text-align: center;" class="date"><?php echo $item['document_date']; ?></td>
                <td style="text-align: center;" class="total_amount"><?php echo $item['total_amount']; ?></td>
                <td style="text-align: center;" class="0to30"><?php if ($day_difference <= 30) {echo $unpaid_amount; $total_amount['0to30'] += $unpaid_amount;} ?></td>
                <td style="text-align: center;" class="31to60"><?php if ($day_difference > 30 and $day_difference <= 60) {echo $unpaid_amount; $total_amount['31to60'] += $unpaid_amount;} ?></td>
                <td style="text-align: center;" class="61to90"><?php if ($day_difference > 60 and $day_difference <= 90) {echo $unpaid_amount; $total_amount['61to90'] += $unpaid_amount;} ?></td>
                <td style="text-align: center;" class="90plus"><?php if ($day_difference > 90) {echo $unpaid_amount; $total_amount['90plus'] += $unpaid_amount;} ?></td>
            </tr>
                <?php if ($purchases_not_done[$key+1]['supplier_id'] != $item['supplier_id']) {
                    ?>
                <tr class="total_supplier" style="font-weight: bold;">
                    <td colspan="2"></td>
                    <td>Total</td>
                    <td style="text-align: center;" class="total_amount"><?php echo $total_amount['total_amount']; ?></td>
                    <td style="text-align: center;" class="0to30"><?php echo $total_amount['0to30']; ?></td>
                    <td style="text-align: center;" class="31to60"><?php echo $total_amount['31to60']; ?></td>
                    <td style="text-align: center;" class="61to90"><?php echo $total_amount['61to90']; ?></td>
                    <td style="text-align: center;" class="90plus"><?php echo $total_amount['90plus']; ?></td>
                </tr>
                <?php } ?>
            <?php } ?>
            <tr id="grand_total" style="font-weight: bold;">
                <td colspan="2"></td>
                <td>Grand Total</td>
                <td style="text-align: center;" class="total_amount"></td>
                <td style="text-align: center;" class="0to30"></td>
                <td style="text-align: center;" class="31to60"></td>
                <td style="text-align: center;" class="61to90"></td>
                <td style="text-align: center;" class="90plus"></td>
            </tr>
            <tr id="aging_percentage" style="font-weight: bold;">
                <td colspan="2"></td>
                <td>Aging Percentage</td>
                <td style="text-align: center;" class="total_amount"></td>
                <td style="text-align: center;" class="0to30"></td>
                <td style="text-align: center;" class="31to60"></td>
                <td style="text-align: center;" class="61to90"></td>
                <td style="text-align: center;" class="90plus"></td>
            </tr>
        </tbody>
    </table>
</div>
<script>

    jQuery(document).ready(function () {

        var total_amount = 0;
        var to30 = 0;
        var to60 = 0;
        var to90 = 0;
        var plus90 = 0;

        $('#accounts_table .total_supplier').each(function() {
            var el = $(this);

            var total_amount_item = el.find('.total_amount').html();

            total_amount += parseFloat(total_amount_item);

            var to30_item = el.find('.0to30').html();

            to30 += parseFloat(to30_item);

            var to60_item = el.find('.31to60').html();

            to60 += parseFloat(to60_item);

            var to90_item = el.find('.61to90').html();

            to90 += parseFloat(to90_item);

            var plus90_item = el.find('.90plus').html();

            plus90 += parseFloat(plus90_item);

        });

        $('#grand_total .total_amount').html(total_amount);
        $('#grand_total .0to30').html(to30);
        $('#grand_total .31to60').html(to60);
        $('#grand_total .61to90').html(to90);
        $('#grand_total .90plus').html(plus90);

        $('#aging_percentage .0to30').html(to30/total_amount*100);
        $('#aging_percentage .31to60').html(to60/total_amount*100);
        $('#aging_percentage .61to90').html(to90/total_amount*100);
        $('#aging_percentage .90plus').html(plus90/total_amount*100);


        $('.total_amount, .0to30, .31to60, .61to90, .90plus').each(function() {
            var el = $(this);

            var html_amount = el.html();

            if (html_amount != '') {
                el.html(numberWithCommas(parseFloat(html_amount).toFixed(2)));
            }
        });

        function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }
    });

    $(document).ready(function() {
        $("#download_excel_file").click(function(e) {
            e.preventDefault();

            //getting data from our table
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('table_wrapper');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');

            var a = document.createElement('a');
            a.href = data_type + ', ' + table_html;
            a.download = 'AP_aging_report.xls';
            a.click();
        });

        $('body').mouseover(function () {
            $("#download_excel_file").show();
            $("#download_pdf_file").show();
        });

        $("#download_pdf_file").click(function () {
            $("#download_excel_file").hide();
            $("#download_pdf_file").hide();
            window.print();
        });
    });
</script>