<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    <?= $title ?>
</h3>
<div class="page-bar" <?php echo $hide_bar == TRUE ? 'style="display:none;"' : ''; ?>>
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo base_url("aging_report/AP_view"); ?>">Aging Report (Payables)</a>
            <i class="fa fa-angle-right"></i>
        </li>
</div>
<!-- END PAGE HEADER-->

<div class="portlet box <?php echo $hide_bar != TRUE ? 'blue' : ''; ?> ">
    <div class="portlet-title" <?php echo $hide_bar == TRUE ? 'style="display:none;"' : ''; ?>>
        <div class="caption">
            <i class="fa fa-money"></i> Aging Report (Payables)
        </div>

    </div>
    <div class="portlet-body">
        <form role="form" method='post' class='form-horizontal' id="filter_form" data-parsley-validate target="_blank">
            <div class="row form-group">
                <div class="col-md-12">
                    <table class="dataTable table table-bordered table-hover">
                        <tbody>
                            <tr>
                                <td>
                                    Select Date <span style="color:red;" class="required">*</span>
                                </td>
                                <td>
                                    <div class="input-group date date-picker" data-date-format="yyyy-mm-dd"
                                         data-date-viewmode="years">
                                        <input value="" name="selected_date" type="text" class="form-control" data-parsley-required="true" data-parsley-required-message="Date is required field">

                                        <div class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>

            </div>


            <div class='row form-group'>
                <div class="col-md-12">
                    <input class="btn btn-primary form-control" type='submit' value="Submit">
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    jQuery(document).ready(function () {

        $('.date-picker').datepicker({
            format: "yyyy-mm-dd"
        });

        // run pre selected options
        $('#pre-selected-options').multiSelect();
    });
</script>