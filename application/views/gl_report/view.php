<style>
    #accounts_list td {font-size: 14px;line-height: 2.8;}

</style>
<!-- BEGIN PAGE HEADER-->
<h3 class="page-title">
    <?= $title ?>
</h3>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="<?php echo base_url() ?>">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <a href="<?php echo base_url("gl_report/view"); ?>">GL report</a>
            <i class="fa fa-angle-right"></i>
        </li>
</div>
<!-- END PAGE HEADER-->

<div class="portlet box blue ">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-money"></i> GL report
        </div>

    </div>
    <div class="portlet-body">
        <form role="form" method='post' class='form-horizontal' id="filter_form" data-parsley-validate target="_blank">
            <div class="row form-group">
                <div class="col-md-12">
                    <table class="dataTable table table-bordered table-hover">
                        <tbody>
                            <tr>
                                <td>
                                    Period <span style="color:red;" class="required">*</span>
                                </td>
                                <td>
                                    <div class="input-group date date-picker" data-date-format="yyyy-mm"
                                         data-date-viewmode="years">
                                        <input id="PL_month_from" value="" name="starting_month" type="text" class="form-control" data-parsley-required="true" data-parsley-required-message="Starting Date of Period is required field">

                                        <div class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group date date-picker" data-date-format="yyyy-mm"
                                         data-date-viewmode="years">
                                        <input id="PL_month_to" value="" name="ending_month" type="text" class="form-control" data-parsley-required="true" data-parsley-required-message="Ending Date of Period is required field">

                                        <div class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Select Outlets <span style="color:red;" class="required">*</span>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <select name="outlet_list[]" id='pre-selected-options' multiple='multiple' data-parsley-required="true" data-parsley-required-message="Please select outlets to view report">
                                            <?php for ($i = 0; $i < count($project_list); $i++) {
                                            ?>
                                            <option value="<?php echo $project_list[$i]['id']; ?>"><?php echo $project_list[$i]['name'].' ('.$project_list[$i]['description'].')'; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    Select Account to view <span style="color:red;" class="required">*</span>
                                </td>
                                <td colspan="2">
                                    <div>
                                        <a style="width:20%; font-size:25px;height: 35px; float:right;" id="modal_select_account" class="btn btn-md gray">...</a>
                                        <input data-toggle="tooltip" data-original-title="" style="display: inline; width:60%;" type="text" class="form-control" id="select_account" name="account_code" data-parsley-required="true" data-parsley-required-message="Please select account to view report" data-parsley-check_account="check_account">
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>

            </div>


            <div class='row form-group'>
                <div class="col-md-12">
                    <input class="btn btn-primary form-control" type='submit' value="Submit">
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal" id="account_select-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Select Account</h4>
            </div>
            <div class="modal-body">
                <table style="margin-top: 20px;" class="tree table table-striped table-bordered table-hover">
                    <thead>
                    <tr role="row" class="heading">
                        <th width="25%">Account Name</th>
                        <th width="8%">Account Type</th>
                        <th width="8%">Account Code</th>
                        <th width="8%">Action</th>
                    </tr>
                    </thead>
                    <tbody id="accounts_list">
                    <?php for ($i = 0; $i < count($accounts); $i++) {
                        ?>
                        <tr role="row" class="treegrid-<?php echo $accounts[$i]['id'] ?> <?php if (!empty($accounts[$i]['father_id'])) {echo 'treegrid-parent-'.$accounts[$i]['father_id'];} ?> heading">
                            <td><?php echo $accounts[$i]['name']; ?></td>
                            <td class="modal_account_name hidden"><?php echo $accounts[$i]['name']; ?></td>
                            <td class="modal_account_type"><?php echo $accounts[$i]['type']; ?></td>
                            <td class="modal_account_code"><?php echo $accounts[$i]['code']; ?></td>
                            <?php if (empty($accounts[$i]['MC_id'])) { ?>
                            <td><button type="button" class="choose_account btn btn-md grey-salsa">Choose This Account</button></td>
                            <?php } else { ?>
                            <td></td>
                            <?php } ?>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button id="choose_all"  type="button" class="btn btn-md blue" data-dismiss="modal">Choose All Accounts</button>
                <button  type="button" class="btn btn-md grey-salsa" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    jQuery(document).ready(function () {

        $('[data-toggle="tooltip"]').tooltip();

        $('#select_account').on('change', function () {
            var el = $(this);
            if (el.val() != '') {

                $.ajax({
                    method: 'POST',
                    url: "<?php echo base_url(); ?>GL_entry/get_account_name",
                    data: {
                        account_code: el.val()
                    },
                    dataType: 'json'
                }).done(function(res) {
                    el.attr('data-original-title', res);
                });

            } else {
                el.attr('data-original-title', '');
            }
        });

        var list_accounts = <?php echo $accounts_json; ?>;

        function split( val ) {
            return val.split( /,\s*/ );
        }
        function extractLast( term ) {
            return split( term ).pop();
        }

        $( "#select_account" )
        // don't navigate away from the field on tab when selecting an item
            .on( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                    $( this ).autocomplete( "instance" ).menu.active ) {
                    event.preventDefault();
                }
            })
            .autocomplete({
                minLength: 0,
                source: function( request, response ) {
                    // delegate back to autocomplete, but extract the last term
                    response( $.ui.autocomplete.filter(
                        list_accounts, extractLast( request.term ) ) );
                },
                focus: function() {
                    // prevent value inserted on focus
                    return false;
                },
                select: function( event, ui ) {
                    var terms = split( this.value );
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push( ui.item.value );
                    // add placeholder to get the comma-and-space at the end
                    terms.push( "" );
                    this.value = terms.join( ", " );
                    return false;
                }
            });

        $("#choose_all").on("click",function() {
            var all_accounts = '';
            $.each(list_accounts, function (index, value) {
                all_accounts += value.value + ', ';
            });
            $("#select_account").val(all_accounts);
        });

        $("#modal_select_account").on("click",function(){
            $('#account_select-modal').modal({
                show: true,
                backdrop: 'static'
            });

            var modal_account_code = '';
            $(".choose_account").on("click",function(){
                var el = $(this);
                modal_account_code = el.closest('tr').find('.modal_account_code').html();
                var modal_account_name = el.closest('tr').find('.modal_account_name').html();
                $('#account_select-modal').modal('hide');

                $('#select_account').val(modal_account_code);
                $('#select_account').attr('data-original-title', modal_account_name);
            });
        });

        $('.ui-autocomplete').css('z-index', '999999');

        $('.date-picker').datepicker({
            format: "yyyy-mm-dd"
        });

        // run pre selected options
        $('#pre-selected-options').multiSelect();

        window.ParsleyValidator.addValidator('check_account',
            function (value, requirement) {
                var account_code = $('#select_account').val();
                var error_message = '';

                var response = false;
                $.ajax({
                    async:false,
                    method: 'POST',
                    url: "<?php echo base_url(); ?>gl_report/check_account",
                    data: {
                        account_code: account_code
                    },
                    dataType: 'json'
                }).done(function(res) {

                    response = res.error;
                    error_message = res.message;
                });

                window.ParsleyValidator.addMessage('en', 'check_account', error_message);
                return response == false;
            }, 32
        );
    });
</script>