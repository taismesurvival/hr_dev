<style>
    table { border-collapse: collapse; }
    tr.border-top { border-top: solid thin; }
    tr.border-bottom { border-bottom: solid thin; }
    tr.bold { font-weight: bold; }
    * { font-size: 12px; font-family: Lucida Sans Unicode; }
</style>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>/public/assets/global/plugins/slider-pips/jquery-ui.js"></script>
<button type="button" id="download_excel_file">Download Excel Report</button>
<button type="button" id="download_pdf_file">Print or Download PDF Report</button>
<div id="table_wrapper">
    <table style='width:100%;'>

        <tr>
            <td style='font-size:20px;text-align:center;color:blue;'><?php echo $company_name; ?></td>
        </tr>
        <tr>
            <td style='font-size:16px;text-align:center;color:blue;'>Outlets List</td>
        </tr>
        <tr>
            <td style='font-size:16px;text-align:center;color:blue;'>
                <?php foreach($outlets AS $item) {
                    echo $item.'<br/>';
                } ?>
            </td>
        </tr>
        <tr>
            <td style='text-align:center;font-size:18px;'>Selected Period: <?php echo $starting_month; ?> - <?php echo $ending_month; ?></td>
        </tr>
        <tr>
            <td style='text-align:center;font-size:18px;'>GL Report</td>
        </tr>
    </table>
    <?php foreach($code_data AS $code) { ?>
    <?php if (!empty($code['selected_period'])) { ?>
    <table class="code_table" style='width:100%; margin-top: 50px'>
        <thead>
            <tr class='border-top'>
                <td colspan="7" style='text-align:center;font-size:17px;'>Account Code: <?php echo $code['account_infor']['code']; ?></td>
            </tr>
            <tr>
                <td colspan="7" style='text-align:center;font-size:17px;'>Account Name: <?php echo $code['account_infor']['name']; ?></td>
            </tr>
            <tr>
                <th style='text-align:center; width:10%;'>Date</th>
                <th style='text-align:center; width:10%;'>Transaction Category</th>
                <th style='text-align:center; width:20%;'>Reference No</th>
                <th style='text-align:center; width:20%;'>Description</th>
                <th style='text-align:center; width:5%;'>Debit</th>
                <th style='text-align:center; width:5%;'>Credit</th>
                <th style='text-align:center; width:5%;'>Net Activity</th>
                <th style='text-align:center; width:5%;'>Ending Balance</th>
            </tr>
        </thead>
        <tbody class="accounts_table">
            <tr style="font-weight: bold;">
                <td>Beginning Balance:</td>
                <td class="starting_balance"><?php echo $code['starting_balance']; ?></td>
            </tr>

            <?php
            foreach($code['selected_period'] AS $item) {

                ?>
            <tr class="account_item">
                <td style='text-align:center;'><?php echo $item['document_date']; ?></td>
                <td style='text-align:center;'><?php echo $item['transaction_type']; ?></td>
                <td style='text-align:center;'><?php echo 'GL'.$item['id']; ?></td>
                <td style='text-align:center;'><?php echo $item['description']; ?></td>
                <td class="debit_amount" style='text-align:center;'><?php echo $item['type'] == 'debit' ? $item['amount'] : 0; ?></td>
                <td class="credit_amount" style='text-align:center;'><?php echo $item['type'] == 'credit' ? $item['amount'] : 0; ?></td>
                <td></td>
                <td style="text-align: center; font-weight: bold;" class="ending_balance"></td>
            </tr>
            <?php } ?>

            <tr style="font-weight: bold;">
                <td style='text-align:right;' colspan="4">Total Amount</td>
                <td style="display: none;" class="account_type"><?php echo $code['account_type']?></td>
                <td style='text-align:center;' class="debit_total_amount"></td>
                <td style='text-align:center;' class="credit_total_amount"></td>
                <td style='text-align:center;' class="net_activity"></td>
            </tr>
        </tbody>
    </table>
    <?php } ?>
    <?php  } ?>
</div>
<script>

    jQuery(document).ready(function () {

        $('.code_table').each(function() {
            var el1 = $(this);

            var account_type = el1.find('.account_type').html();
            var starting_balance = parseFloat(el1.find('.starting_balance').html());
            var credit_total_amount = 0;
            var debit_total_amount = 0;

            el1.find('.accounts_table').find('.account_item').each(function () {
                var el = $(this);
                var credit_amount = el.find('.credit_amount').html();
                var debit_amount = el.find('.debit_amount').html();

                credit_total_amount += parseFloat(credit_amount);
                debit_total_amount += parseFloat(debit_amount);

                if (account_type == 'credit') {
                    starting_balance = starting_balance + parseFloat(credit_amount) - parseFloat(debit_amount);
                } else {
                    starting_balance = starting_balance - parseFloat(credit_amount) + parseFloat(debit_amount);
                }
                el.find('.ending_balance').html(starting_balance >= 0 ? starting_balance : '(' + Math.abs(starting_balance) + ')');
            });

            el1.find('.credit_total_amount').html(credit_total_amount);
            el1.find('.debit_total_amount').html(debit_total_amount);
            el1.find('.net_activity').html(account_type == 'debit' ? (debit_total_amount - credit_total_amount) : (-debit_total_amount + credit_total_amount));
        });


        $('.credit_amount, .debit_amount, .ending_balance, .starting_balance, .credit_total_amount, .debit_total_amount, .net_activity').each(function() {
            var el = $(this);

            var html_amount = el.html();
            var check_rackets = html_amount.match(/\(([^)]+)\)/);

            if (check_rackets != null) {

                var round_amount = numberWithCommas(parseFloat(check_rackets[1]).toFixed(2));

                el.html('('+round_amount+')');
            } else {
                el.html(numberWithCommas(parseFloat(html_amount).toFixed(2)));
            }
        });

        function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
        }
    });

    $(document).ready(function() {
        $("#download_excel_file").click(function(e) {
            e.preventDefault();

            //getting data from our table
            var data_type = 'data:application/vnd.ms-excel';
            var table_div = document.getElementById('table_wrapper');
            var table_html = table_div.outerHTML.replace(/ /g, '%20');

            var a = document.createElement('a');
            a.href = data_type + ', ' + table_html;
            a.download = 'gl_report.xls';
            a.click();
        });

        $('body').mouseover(function () {
            $("#download_excel_file").show();
            $("#download_pdf_file").show();
        });

        $("#download_pdf_file").click(function () {
            $("#download_excel_file").hide();
            $("#download_pdf_file").hide();
            window.print();
        });
    });
</script>