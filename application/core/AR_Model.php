<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AR_Model extends CI_Model {
    protected $tblName;
    public function __construct(){
        parent::__construct();
      	
      	$master_database = $this->config->item("master_database");
        $selected_db = $this->session->userdata('dbConfig') != "" ? $this->session->userdata('dbConfig') : $master_database;
        
        $config['hostname'] = 'localhost';
        $config['username'] = $this->config->item("master_username");
        $config['password'] = $this->config->item("master_password");
        $config['database'] = $selected_db;
        $config['dbdriver'] = 'mysql';
        $config['dbprefix'] = '';
        $config['pconnect'] = TRUE;
        $config['db_debug'] = FALSE;
        $config['cache_on'] = FALSE;
        $config['cachedir'] = '';
        $config['char_set'] = 'utf8';
        $config['dbcollat'] = 'utf8_unicode_ci';
        $config['swap_pre'] = '';
        $config['autoinit'] = TRUE;
        $config['stricton'] = FALSE;
        $this->load->database($config);
    }
   
	protected function query($query){
        return $this->db->query($query);
    }
    public function executeQuery($query){
        $res =  $this->db->query($query);
        return $res;
    }
   protected function readAllRows($table_name, $additional_criteria = "",$data_status = ""){
        $data = array();

        if($data_status == "")
        {   
            $where = " WHERE deleted_at IS NULL";
        }
        else if($data_status == "only_trash"){
            $where = " WHERE deleted_at IS NOT NULL";
        }
        else if($data_status == "with_trash"){
            $where = " WHERE 1";
        }
        
        $where .= $additional_criteria != "" ? $additional_criteria : "";
        
        $query = "SELECT * FROM " . $table_name . $where;
        
        $rows = $this->executeQuery($query);
        
        foreach ($rows->result_array() as $row) {
            foreach($row as $key=>$val){
                    $row[$key] = $val;
            }
            $data[] = $row;
        }
        
        return $data;
    }
    protected function readRowInfo($table_name, $id, $by = "", $additional_criteria = ""){
        $data = array();
        if ($by == "") $by = "id";
        $where = ($id != "") ? " WHERE $by = '$id'" : "";
        $where .= $additional_criteria != "" ? $additional_criteria : "";

        $query = "SELECT * FROM " . $table_name . $where;
      
        $rows = $this->executeQuery($query);
        
        foreach ($rows->result_array() as $row) {
                $data = $row;
        }
        return $data;
    }
    protected function getInsertId(){
            return $this->db->insert_id();
    }
    protected function insert($tblName,$data,$log = 1){
            foreach($data as $key=>$val){
                if ($this->db->field_exists($key, $this->tblName)){
                    $insert[$key] = $val;
                }   
            }
        $insert["created_at"] = date("Y-m-d H:i:s");
        $this->db->insert($tblName, $insert); 
        $insert_id = $this->db->insert_id();
        $query = $this->db->last_query();
      
        return $insert_id;
    }
    protected function updateRow($data,$tblName,$id,$by,$log = 1){
        
            $allow_update = explode(",", $this->allow_update);
            foreach($data as $key=>$val){
                if ($this->db->field_exists($key, $this->tblName)){
                    $update[$key] = $val;
                }
            }
        $update["updated_at"] = date("Y-m-d H:i:s");
        $this->db->where($by,$id);
        $this->db->update($tblName, $update); 

        $query = $this->db->last_query();

    }

    protected function countRows($table_name, $additional_criteria = "",$data_status = ""){
        if($data_status == "")
        {   
            $where = " WHERE deleted_at IS NULL";
        }
        else if($data_status == "only_trash"){
            $where = " WHERE deleted_at IS NOT NULL";
        }
        else if($data_status == "with_trash"){
            $where = " WHERE 1";
        }
        $where .= $additional_criteria != "" ? $additional_criteria : "";
        $query = "SELECT COUNT(*) as cnt FROM " . $table_name . $where;
        $rows = $this->executeQuery($query);
   
        foreach ($rows->result_array() as $row)
        {
            $count = isset($row['cnt']) ? $row['cnt'] : "";
        }
    
        return $count;
    }
    protected function DeleteRow($tblName,$id,$by,$log = 1){
        $data["deleted_at"] = date("Y-m-d H:i:s");
        $this->db->where($by,$id);
        $this->db->update($tblName, $data);

        $query = $this->db->last_query();
        $data = $this->readRowInfo($tblName,$id);
    }
    protected function restoreRow($tblName,$id,$by,$log = 1){
        $data["deleted_at"] = NULL;
        $this->db->where($by,$id);
        $this->db->update($tblName, $data);
        $query = $this->db->last_query();
        $data = $this->readRowInfo($tblName,$id);

    }
    public function realDeleteRow($tblName,$id,$data = NULL,$log = 1){
        $this->db->where('id', $id);
        $data = $this->readRowInfo($tblName,$id);
        $this->db->delete($tblName);
        $query = $this->db->last_query();


    }
    protected function log($table,$data,$id,$action,$query){
        $ip = $this->input->ip_address();
        $userData = $this->session->userdata("user_data");      // 20160301 vernhui fix the log problem, able to check user id and name
        $userType = $this->session->userdata("user_type");
        if(isset($userData['id'])){
            $user_id = $userData['id'];
            $user_name = isset($userData['username']) ? $userType.'-'.$userData['username'] : $userType.'-'.$userData['code'];
        }else{
            $user_id = 0;
            $user_name = "guest";
        }

        $insert['ip'] = $ip;
        $insert['user_id'] = $user_id;
        $insert['user_name'] = $user_name;
        $insert['affected_table'] = $table;
        $insert['affected_id'] = $id;
        $insert['query'] = $query;
        $insert['action'] = $action;
        $insert['data'] = json_encode($data);
        $insert['wording_log'] = $user_name." ".$action." ".$table;
        $insert["created_at"] = date("Y-m-d H:i:s");
         //$this->db->insert("log", $insert);
    }

    public function add($data)
    {
        return $this->insert($this->tblName,$data);
    }

    public function all($custom_where="",$data_status=""){
        $rows = $this->readAllRows($this->tblName,$custom_where,$data_status);
        return $rows;
    }

    public function find($id,$by='id',$custom_where="") 
    {
        $rows = $this->readRowInfo($this->tblName, $id ,$by,$custom_where);
        return $rows;
    }
    public function count($custom_where="")
    {
        $num = $this->countRows($this->tblName, $custom_where);
        return $num;
    }
    public function update($id,$data)
    {
        //$data["updated_at"] = date("Y-m-d H:i:s");
        
        $this->updateRow($data, $this->tblName,$id,$this->tblId);
        //$this->db->where('id', $id);
        //$this->db->update($this->tblName, $data); 
    }
    public function delete($id){
        $this->deleteRow($this->tblName,$id,$this->tblId);
    }
    public function realDelete($id){
        $this->realDeleteRow($this->tblName,$id,$this->tblId);
    }
    public function soft_delete($id){
        $this->deleteRow($this->tblName,$id,$this->tblId);
    }
    public function restore($id){
        $this->restoreRow($this->tblName,$id,$this->tblId);
    }
}