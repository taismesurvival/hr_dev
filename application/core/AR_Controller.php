<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AR_Controller extends CI_Controller {
	protected $input_data;
	protected $temporary_data;
	protected $data;
	protected $controller_name;
	protected $module_name;
	protected $function_name;

	public function __construct(){
		
		parent::__construct();

		if($_POST){
			$this->input_data['post'] = $this->input->post();
		}	
		else if($_GET){
			$this->input_data['get'] = $this->input->get();
		}
		/*if(count($this->input_data) > 0){
			foreach($this->input_data as $key=>$value){
				$this->input_data[$key] =stripslashes(trim($value));
			}
		}*/

		if (isset($_GET['token']) && isset($_GET['company_code']) && $this->session->userdata("dbConfig") != 'l12com_acc_'.$_GET['company_code']) {
            $pos = 'l12com_pos_'.$_GET['company_code'];
            $acc = 'l12com_acc_'.$_GET['company_code'];

            $sql = "SELECT ".$pos.".user.* FROM ".$acc.".pos_login_token 
        LEFT JOIN ".$pos.".user ON ".$pos.".user.id = ".$acc.".pos_login_token.user_id
        WHERE ".$acc.".pos_login_token.token = '".$_GET['token']."'";

            $token_data = $this->db->query($sql)->result_array();
            if(!empty($token_data)) {
                $this->session->set_userdata("dbConfig",$acc);

                $this->session->set_userdata("user_data",$token_data[0]);
                $this->session->set_userdata("user_type","admin");
                $this->session->set_userdata("login_type","from_pos");
                redirect("");
                return;
            }
        }

        if (isset($_GET['token'])
            && ($this->router->fetch_class() == 'balance_sheet_report' || $this->router->fetch_class() == 'PL_report'
            || $this->router->fetch_class() == 'salary_import' || $this->router->fetch_class() == 'gst_report' || $this->router->fetch_class() == 'aging_report')) {

		    // Call API to valid token
            $call_api = new HungryGoWhere();

            $url = $call_api->server."auth";
            $params = $call_api->token;
            $sig = $call_api->get_signature($url, $params);
            $params['sig'] = $sig;

            $return = $call_api->get_page($url, 'post', $params);
            $data = json_decode($return, true);
            if($data['status'] == 200) {


                $session_token = $data['data']['session_token'];

                $url = $call_api->server."sso_token/".$_GET['token'];
                $params1['session_token'] = $session_token;
                $sig = $call_api->get_signature($url, $params1);
                $params1['sig'] = $sig;

                $return = $call_api->get_page($url, 'get', $params1);
                $data = json_decode($return, true);

                if($data['status'] == 200) {

                    $master_database = $this->config->item("master_database");

                    $restaurant_id = $data['data']['sme_restaurant_id'];
                    $user_name = $data['data']['sme_username'];

                    $sql = "SELECT $master_database.company.`database` FROM $master_database.HGW_restaurant_map 
        LEFT JOIN $master_database.company ON $master_database.company.id = $master_database.HGW_restaurant_map.company_id
        WHERE $master_database.HGW_restaurant_map.HGW_restaurant_id = '".$restaurant_id."'";

                    $database = $this->db->query($sql)->result_array();

                    if (!empty($database)) {

                        $acc_database = $database[0]['database'];

                        $sql = "SELECT ".$acc_database.".user.* FROM ".$acc_database.".user
        WHERE ".$acc_database.".user.username = '".$user_name."' AND ".$acc_database.".user.deleted_at IS NULL";

                        $user_data = $this->db->query($sql)->result_array()[0];

                        if (!empty($user_data) && $user_data['role_id'] == 3) {

                            if ($this->session->userdata("dbConfig") != $database[0]['database']) {
                                $this->session->set_userdata("dbConfig", $acc_database);

                                $this->session->set_userdata("user_data",$user_data);
                                $this->session->set_userdata("user_type","admin");
                                $this->session->set_userdata("login_type",'');
                            }

                            $this->session->set_userdata("login_url", $this->router->fetch_class().'/'.$this->router->fetch_method());
                            redirect($this->router->fetch_class().'/'.$this->router->fetch_method());
                            return;

                        }

                    }

                }


            }
        }

		// For flash message
		if ($this->session->flashdata('__notice') != FALSE) {
            $this->data['flash_message'] = ($this->session->flashdata('__notice'));
        }

        $userdata = $this->session->userdata("user_data");

        if ($userdata['role_id'] == 2 && ($this->router->fetch_method() != 'change_password' && $this->router->fetch_class() != 'salary_import' && $this->router->fetch_class() != 'sales_input' && $this->router->fetch_class() != 'sales_report' && $this->router->fetch_class() != 'AuthController')) {
            redirect('sales_input/select_outlet');
        }

        $login_url = $this->session->userdata("login_url");
        if ($userdata['role_id'] == 3 && $this->router->fetch_class().'/'.$this->router->fetch_method() != $login_url && ($this->router->fetch_class() != 'balance_sheet_report' && $this->router->fetch_class() != 'PL_report'
                && $this->router->fetch_class() != 'salary_import' && $this->router->fetch_class() != 'gst_report' && $this->router->fetch_class() != 'aging_report')) {
            redirect($login_url);
        }
		
		$controller = $this->router->fetch_class();
		$this->load->model("company_model");
		$company_data = $this->company_model->find(1);
        // if(!isset($company_data['id']) && $controller != "company"){
        //     redirect('company/dashboard');
        // }
        $data['company_data'] = 1;
		
		
	}

	public function is_login($type){

	
	
		
		$data = $this->session->userdata("user_data");
		
		if(!isset($data['id'])){
			redirect("AuthController/login");
		}
	}

	protected function __set_flash_message($message)
    {
        $this->load->library("session");
        $this->session->set_flashdata('__notice', $message);
    }
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

class HungryGoWhere
{
    public $token;
    public $session_token;
    protected $token_secret;
    public $server;

    public function __construct(){
        $this->token['access_token'] = '9d5699294ee1ff9c1a71da9a824fa1';
        $this->token['token_secret'] = 'bea0cc8a35';
        $this->token['consumer_key'] = '8dbf55e1b4c9adab177052a28f55b5';
        $this->token['consumer_secret'] = 'feebeb7ffe';
        $this->server = "https://api.menu.test.insing.com/";

        $this->token_secret = $this->token['consumer_secret'];
    }

    function get_signature($url, $params = '')
    {
        $out = parse_url($url);
        $method = str_replace('/', '', $out['path']);
        $parameter = '';
        ksort($params);
        if (is_array($params))
        {
            $parameter = str_replace(array('=','&'),'',http_build_query($params));
        }
        $combine = "{$this->token_secret}{$method}{$parameter}";
        $sig = md5($combine);
        return $sig;
    }

    function get_page($url, $method, $data = '', $json_data = '')
    {
        //echo $url.' '.$method;
        $usecookie  = getcwd().'/cookie.txt';
        $header[0] = "Accept: */*";
        $header[] = "Cache-Control: max-age=0";
        $header[] = "Connection: keep-alive";
        $header[] = "Keep-Alive: 300";
        $header[] = "Accept-Language: en-us";

        $ch = curl_init();
        if ($data)
        {
            $str_data = http_build_query($data);
            if ($method == 'get')
            {
                $header[] = "Content-Type: application/x-www-form-urlencoded";
                $url .= '?'.$str_data;
            } else if ($method == 'post')
            {
                $header[] = "Content-Type: application/x-www-form-urlencoded";
                curl_setopt($ch, CURLOPT_POSTFIELDS, $str_data);
            }  else if ($method == 'post-json')
            {
                $url .= '?'.$str_data;
                $header[] = "Content-Type: application/json";
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
            }else if ($method == 'put')
            {

                $str_data = json_encode($data);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $str_data);
            } else if ($method == 'put-json')
            {
                $url .= '?'.$str_data;
                $header[] = "Content-Type: application/json";
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
            }else  if ($method == 'delete')
            {
                $header[] = "Content-Type: application/x-www-form-urlencoded";
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $str_data);
            }
        }

        //$this->last_url = $url;
        //echo $url."<Br>";
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        //curl_setopt($ch, CURLOPT_VERBOSE, true);

        $user_agent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:14.0) Gecko/20100101 Firefox/14.0.1";
        /*
        if (isset($_SERVER) && $_SERVER['SERVER_NAME'] == 'localhost')
        {
         curl_setopt($ch, CURLOPT_PROXY, "http://singtelproxy.is");
         curl_setopt($ch, CURLOPT_PROXYPORT, 80);
        }
        */
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
        curl_setopt($ch, CURLOPT_TIMEOUT,15);
        if ($usecookie)
        {
            curl_setopt($ch, CURLOPT_COOKIEJAR, $usecookie);
            curl_setopt($ch, CURLOPT_COOKIEFILE, $usecookie);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $result =curl_exec ($ch);
        //echo curl_error ($ch );
        curl_close ($ch);
        return $result;
    }
}