<?php
class api extends AR_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function import_pos_data()
    {
        $input_data = $this->input_data['post'];


        if (!empty($input_data['company_code'])) {

            $company_db = 'l12com_acc_'.strtolower($input_data['company_code']);
            $sql = "SHOW DATABASES LIKE '$company_db'";

            $res = $this->db->query($sql)->result_array();

            if(empty($res)) {

                echo json_encode([
                    'success' => FALSE,
                    'error_message' => 'company_code does not exist, Please contact with SME Survival to use our service'
                ]);
                return;
            }
        } else {
            echo json_encode([
                'success' => FALSE,
                'error_message' => 'company_code is a required field, Please contact with SME Survival to use our service'
            ]);
            return;
        }

        if (!empty($input_data['outlet_code'])) {

            $outlet_code = $input_data['outlet_code'];
            $sql = "SELECT secret_key, name, code, id FROM $company_db.project WHERE $company_db.project.code = '$outlet_code' 
        AND $company_db.project.type = 'outlet' AND $company_db.project.deleted_at is null";

            $outlet = $this->db->query($sql)->result_array();

            if(empty($outlet)) {

                echo json_encode([
                    'success' => FALSE,
                    'error_message' => 'outlet_code does not exist, Please contact with SME Survival to use our service'
                ]);
                return;
            }
        } else {
            echo json_encode([
                'success' => FALSE,
                'error_message' => 'outlet_code is a required field, Please contact with SME Survival to use our service'
            ]);
            return;
        }

        if (!empty($input_data['secret_key'])) {

            if($outlet[0]['secret_key'] != $input_data['secret_key']) {

                echo json_encode([
                    'success' => FALSE,
                    'error_message' => 'secret_key does not match, Please contact with SME Survival to use our service'
                ]);
                return;
            }
        } else {
            echo json_encode([
                'success' => FALSE,
                'error_message' => 'secret_key is a required field, Please contact with SME Survival to use our service'
            ]);
            return;
        }

        if (!empty($input_data['sales_date'])) {

            if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$input_data['sales_date'])) {

                echo json_encode([
                    'success' => FALSE,
                    'error_message' => 'sales_date must be in format YYYY-MM-DD'
                ]);
                return;
            }
        } else {
            echo json_encode([
                'success' => FALSE,
                'error_message' => 'sales_date is a required field'
            ]);
            return;
        }

        $total_sales = $input_data['food_amount'] + $input_data['beverage_amount']
            + $input_data['liquor_amount'] + $input_data['others_amount']
            + $input_data['GST_amount'] + $input_data['service_charge_amount'] + $input_data['delivery_amount'];

        $total_collection = $input_data['discount_amount'] + $input_data['cash_amount']
            + $input_data['visa_card_amount'] + $input_data['nets_amount']
            + $input_data['internal_voucher_amount']+ $input_data['external_voucher_amount'] + $input_data['master_card_amount'] + $input_data['american_express_amount']
            + $input_data['corporate_amount'] + $input_data['ubereats_amount'] + $input_data['deliveroo_amount']
            + $input_data['foodpanda_amount'];

        if ($total_sales == 0 || $total_collection == 0) {

            echo json_encode([
                'success' => FALSE,
                'error_message' => 'Total Sales and Total Collection must not be 0'
            ]);
            return;
        }

        if ($total_sales != $total_collection) {

            echo json_encode([
                'success' => FALSE,
                'error_message' => 'Total Sales and Total Collection must be equal'
            ]);
            return;
        }


        // Add POS data to GL entry
        $outlet_name = $outlet[0]['name'] . ' (' . $outlet[0]['code'] . ')';

        $transactions = [];

        if ($input_data['GST_amount'] > 0) {
            $transactions[] = [
                'account_code' => '21401',
                'amount' => $input_data['GST_amount'],
                'description' => 'GST Total',
                'type' => 'credit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['GST_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if ($input_data['service_charge_amount'] > 0) {
            $transactions[] = [
                'account_code' => '40110',
                'amount' => $input_data['service_charge_amount'],
                'description' => 'Service Charge Total',
                'type' => 'credit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['service_charge_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if ($input_data['delivery_amount'] > 0) {
            $transactions[] = [
                'account_code' => '40098',
                'amount' => $input_data['delivery_amount'],
                'description' => 'Delivery Charge Total',
                'type' => 'credit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['delivery_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if ($input_data['discount_amount'] > 0) {
            $transactions[] = [
                'account_code' => '40210',
                'amount' => $input_data['discount_amount'],
                'description' => 'Overall Discount Total',
                'type' => 'debit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['discount_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if ($input_data['cash_amount'] > 0) {
            $transactions[] = [
                'account_code' => '14005',
                'amount' => $input_data['cash_amount'],
                'description' => 'Cash Total',
                'type' => 'debit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['cash_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if ($input_data['visa_card_amount'] > 0) {
            $transactions[] = [
                'account_code' => '14002',
                'amount' => $input_data['visa_card_amount'],
                'description' => 'Visa Card Total',
                'type' => 'debit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['visa_card_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if ($input_data['nets_amount'] > 0) {
            $transactions[] = [
                'account_code' => '14004',
                'amount' => $input_data['nets_amount'],
                'description' => 'Nets Total',
                'type' => 'debit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['nets_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if ($input_data['internal_voucher_amount'] > 0) {
            $transactions[] = [
                'account_code' => '14014',
                'amount' => $input_data['internal_voucher_amount'],
                'description' => 'Internal Voucher Total',
                'type' => 'debit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['internal_voucher_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if ($input_data['external_voucher_amount'] > 0) {
            $transactions[] = [
                'account_code' => '14013',
                'amount' => $input_data['external_voucher_amount'],
                'description' => 'External Voucher Total',
                'type' => 'debit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['external_voucher_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if ($input_data['master_card_amount'] > 0) {
            $transactions[] = [
                'account_code' => '14003',
                'amount' => $input_data['master_card_amount'],
                'description' => 'Master Card Total',
                'type' => 'debit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['master_card_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if ($input_data['american_express_amount'] > 0) {
            $transactions[] = [
                'account_code' => '14001',
                'amount' => $input_data['american_express_amount'],
                'description' => 'American Express Total',
                'type' => 'debit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['american_express_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if ($input_data['corporate_amount'] > 0) {
            $transactions[] = [
                'account_code' => '14008',
                'amount' => $input_data['corporate_amount'],
                'description' => 'Corporate Total',
                'type' => 'debit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['corporate_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if ($input_data['ubereats_amount'] > 0) {
            $transactions[] = [
                'account_code' => '14017',
                'amount' => $input_data['ubereats_amount'],
                'description' => 'UberEATS Total',
                'type' => 'debit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['ubereats_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if ($input_data['deliveroo_amount'] > 0) {
            $transactions[] = [
                'account_code' => '14018',
                'amount' => $input_data['deliveroo_amount'],
                'description' => 'Deliveroo Total',
                'type' => 'debit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['deliveroo_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if ($input_data['foodpanda_amount'] > 0) {
            $transactions[] = [
                'account_code' => '14019',
                'amount' => $input_data['foodpanda_amount'],
                'description' => 'FoodPanda Total',
                'type' => 'debit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['foodpanda_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if ($input_data['food_amount'] > 0) {
            $transactions[] = [
                'account_code' => '40010',
                'amount' => $input_data['food_amount'],
                'description' => 'Food Sales Total',
                'type' => 'credit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['food_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if ($input_data['beverage_amount'] > 0) {
            $transactions[] = [
                'account_code' => '40020',
                'amount' => $input_data['beverage_amount'],
                'description' => 'Beverage Sales Total',
                'type' => 'credit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['beverage_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if ($input_data['liquor_amount'] > 0) {
            $transactions[] = [
                'account_code' => '40030',
                'amount' => $input_data['liquor_amount'],
                'description' => 'Liquor Sales Total',
                'type' => 'credit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['liquor_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if ($input_data['others_amount'] > 0) {
            $transactions[] = [
                'account_code' => '40510',
                'amount' => $input_data['others_amount'],
                'description' => 'Others Sales Total',
                'type' => 'credit',
                'project' => [0 => [
                    'percentage' => '100',
                    'amount' => $input_data['others_amount'],
                    'project_id' => $outlet[0]['id']
                ]
                ]
            ];
        }

        if (!empty($transactions)) {

            $pos_date = $input_data['sales_date'];

            $GL_entry = [
                'type' => 'POS',
                'document_date' => $pos_date,
                'description' => 'POS entry on date ' . $pos_date . ' for ' . $outlet_name,
                'total_amount' => $total_sales,
                'transactions' => $transactions
            ];

            // Save to GL Entry

            $query = "INSERT INTO " . $company_db . ".GL_entry (type, document_date, description, total_amount) 
            VALUES ('" . $GL_entry['type'] . "', '" . $GL_entry['document_date'] . "', '" . $GL_entry['description'] . "', " . $GL_entry['total_amount'] . ")";
            $this->db->query($query);
            $GL_entry['id'] = $this->db->insert_id();

            foreach ($GL_entry['transactions'] AS $transaction) {

                $query = "INSERT INTO " . $company_db . ".GL_transaction (type, amount, transaction_type, description) 
            VALUES ('" . $transaction['type'] . "', " . $transaction['amount'] . ", 'GL', '" . $transaction['description'] . "')";
                $this->db->query($query);
                $transaction_id = $this->db->insert_id();

                $query = "INSERT INTO " . $company_db . ".GL_mapping (GL_transaction_id, GL_entry_id) 
            VALUES (" . $transaction_id . ", " . $GL_entry['id'] . ")";
                $this->db->query($query);

                $transaction['id'] = $transaction_id;

                foreach ($transaction['project'] AS $project) {
                    $sql = "SELECT " . $company_db . ".account_project.id FROM " . $company_db . ".account_project
    LEFT JOIN " . $company_db . ".master_account ON " . $company_db . ".account_project.account_id = " . $company_db . ".master_account.id
    WHERE " . $company_db . ".account_project.deleted_at IS NULL AND " . $company_db . ".account_project.project_id = " . $project['project_id'] . "
    AND " . $company_db . ".master_account.code = " . $transaction['account_code'];

                    $account_project_id = $this->db->query($sql)->result_array();
                    $project ['account_project_id'] = $account_project_id[0]['id'];
                    $project ['GL_transaction_id'] = $transaction['id'];
                    $project ['document_date'] = $GL_entry['document_date'];

                    $query = "INSERT INTO " . $company_db . ".GL_transaction_project_split (amount, percentage, account_project_id, GL_transaction_id, document_date) 
            VALUES (" . $project['amount'] . ", " . $project['percentage'] . ", " . $project['account_project_id'] . ", " . $project['GL_transaction_id'] . ", '" . $project['document_date'] . "')";
                    $this->db->query($query);
                }
            }

            echo json_encode([
                'success' => TRUE
            ]);
            return;
        }

    }

    public function auto_lock_FM($key_lock = NULL)
    {

        if ($key_lock != 'sdf23923sbfw2983fsnfjw29h2r9ru38') {
            echo 'Restricted Access';
            exit;
        }

        $sql = "SELECT id, `database`
        FROM company
        WHERE deleted_at IS NULL";

        $companies = $this->db->query($sql)->result_array();

        $months_2_ago = strtotime("-2 month");

        foreach ($companies as $company) {

            $database = $company['database'];

            $sql = "SELECT $database.company.locked_financial_month
        FROM $database.company
        WHERE $database.company.id = 1";

            $company_data = $this->db->query($sql)->result_array();

            $locked_financial_month = !empty($company_data[0]['locked_financial_month']) ? strtotime($company_data[0]['locked_financial_month']) : 0;

            if ($months_2_ago > $locked_financial_month) {

                $month_autolock = date('Y-m', $months_2_ago);

                $sql = "UPDATE $database.company SET locked_financial_month = '$month_autolock' WHERE id=1";

                $this->db->query($sql);

            }

        }

        echo 'done';
        return;


    }

    public function auth()
    {

        $input_data = $this->input_data['post'];

        if (!empty($input_data['restaurant_id'])) {

            $master_database = $this->config->item("master_database");

            $sql = "SELECT $master_database.company.`database` FROM $master_database.HGW_restaurant_map 
        LEFT JOIN $master_database.company ON $master_database.company.id = $master_database.HGW_restaurant_map.company_id
        WHERE $master_database.HGW_restaurant_map.HGW_restaurant_id = '".$input_data['restaurant_id']."'";

            $database = $this->db->query($sql)->result_array();

            if (!empty($database)) {

                $acc_database = $database[0]['database'];

                if (!empty($input_data['user_name'])) {

                    $sql = "SELECT " . $acc_database . ".user.* FROM " . $acc_database . ".user
        WHERE " . $acc_database . ".user.username = '" . $input_data['user_name'] . "' AND " . $acc_database . ".user.deleted_at IS NULL";

                    $user_data = $this->db->query($sql)->result_array()[0];

                    if (!empty($input_data['password'])) {

                        $password = md5(strtolower($input_data['user_name']) . $input_data['password']);

                        if(!empty($user_data['password']) && $user_data['password'] == $password
                            && $user_data['role_id'] == 3){

                            $token = random_string('alnum', 32);
                            $expired_time = date("Y-m-d H:i:s", strtotime('+30 minutes'));

                            $sql = "UPDATE $acc_database.user 
                        SET login_token = '$token', token_expired_at = '$expired_time' WHERE id = ". $user_data['id'];

                            $this->db->query($sql);

                            echo json_encode([
                                'success' => TRUE,
                                'data' => [
                                    'token' => $token,
                                    'valid_until' => $expired_time,
                                    'restaurant_id' => $input_data['restaurant_id']
                                ]
                            ]);
                            return;

                        } else {

                            echo json_encode([
                                'success' => FALSE,
                                'error_message' => 'user_name and password is not correct'
                            ]);
                            return;
                        }

                    } else {

                        echo json_encode([
                            'success' => FALSE,
                            'error_message' => 'password is a required field'
                        ]);
                        return;

                    }


                } else {

                    echo json_encode([
                        'success' => FALSE,
                        'error_message' => 'user_name is a required field'
                    ]);
                    return;
                }

            } else {

                echo json_encode([
                    'success' => FALSE,
                    'error_message' => 'restaurant_id is not correct, Please contact with SME Survival to use our service'
                ]);
                return;

            }

        } else {
            echo json_encode([
                'success' => FALSE,
                'error_message' => 'restaurant_id is a required field'
            ]);
            return;
        }
    }

    public function check_token()
    {
        $input_data = $this->input_data['post'];

        if (!empty($input_data['restaurant_id'])) {

            $master_database = $this->config->item("master_database");

            $sql = "SELECT $master_database.company.`database` FROM $master_database.HGW_restaurant_map 
        LEFT JOIN $master_database.company ON $master_database.company.id = $master_database.HGW_restaurant_map.company_id
        WHERE $master_database.HGW_restaurant_map.HGW_restaurant_id = '".$input_data['restaurant_id']."'";

            $database = $this->db->query($sql)->result_array();

            if (!empty($database)) {

                $acc_database = $database[0]['database'];

                if (!empty($input_data['token'])) {

                    $sql = "SELECT " . $acc_database . ".user.* FROM " . $acc_database . ".user
        WHERE " . $acc_database . ".user.login_token = '" . $input_data['token'] . "' AND " . $acc_database . ".user.deleted_at IS NULL";

                    $user_data = $this->db->query($sql)->result_array()[0];

                    if (empty($user_data) || strtotime($user_data['token_expired_at']) <= strtotime('now')) {

                        echo json_encode([
                            'success' => FALSE,
                            'error_message' => 'token is invalid'
                        ]);
                        return;
                    } else {

                        echo json_encode([
                            'success' => TRUE,
                            'data' => [
                                'token' => $input_data['token'],
                                'valid_until' => $user_data['token_expired_at'],
                                'restaurant_id' => $input_data['restaurant_id']
                            ]
                        ]);
                        return;

                    }


                } else {

                    echo json_encode([
                        'success' => FALSE,
                        'error_message' => 'token is a required field'
                    ]);
                    return;
                }

            } else {

                echo json_encode([
                    'success' => FALSE,
                    'error_message' => 'restaurant_id is not correct, Please contact with SME Survival to use our service'
                ]);
                return;

            }

        } else {
            echo json_encode([
                'success' => FALSE,
                'error_message' => 'restaurant_id is a required field'
            ]);
            return;
        }
    }

//    public function create_account($key_lock = NULL)
//    {
//        if ($key_lock != 'sdf23923sbfw2983fsnfjw29h2r9ru38') {
//            echo 'Restricted Access';
//            exit;
//        }
//
//        $master_account = [
//            'name' => 'Trade Debtor - UnionPay',
//            'type' => 'asset',
//            'status' => 'active',
//            'code' => 14026,
//            'level' => '4',
//            'description' => 'Trade Debtor - UnionPay',
//            'father_id' => 167
//        ];
//
//        $sql = "show databases like '%l12com_acc_%'";
//
//        $databases = $this->db->query($sql)->result_array();
//        foreach($databases AS $item) {
//
//            if ($item['Database (%l12com_acc_%)'] != 'l12com_acc_master') {
//                $database_name = $item['Database (%l12com_acc_%)'];
//
//                $sql = "INSERT INTO $database_name.master_account (name, type, status, code, level, description, father_id)
//        VALUES ('" . $master_account['name'] . "', '" . $master_account['type'] . "', '" . $master_account['status'] . "', '" . $master_account['code'] . "', '" . $master_account['level'] . "', '" . $master_account['description'] . "', '" . $master_account['father_id'] . "')";
//
//                $this->db->query($sql);
//                $account_id = $this->db->insert_id();
//
//                $sql = "SELECT $database_name.project.*
//        FROM $database_name.project";
//
//                $projects = $this->db->query($sql)->result_array();
//
//                foreach ($projects AS $project) {
//
//                    $sql = "INSERT INTO $database_name.account_project (account_id, project_id)
//        VALUES ('$account_id', '" . $project['id'] . "')";
//                    $this->db->query($sql);
//
//                }
//            }
//        }
//        echo 'success';
//        return;
//    }

    public function HGW_sales_data($key_lock = NULL)
    {
        if ($key_lock != 'sdf23923sbfw2983fsnfjw29h2r9ru38') {
            echo 'Restricted Access';
            exit;
        }

        $call_api = new HungryGoWhere();

        // Try auth api
        $url = $call_api->server."auth";
        $params = $call_api->token;
        $sig = $call_api->get_signature($url, $params);
        $params['sig'] = $sig;

        $return = $call_api->get_page($url, 'post', $params);
        $data = json_decode($return, true);

        if ($data['status'] == 200) {

            $sql = "SELECT company.company_id, HGW_restaurant_map.HGW_restaurant_id, HGW_restaurant_map.outlet_id,
        company.`database`
        FROM HGW_restaurant_map
        LEFT JOIN company ON company.id = HGW_restaurant_map.company_id
        WHERE HGW_restaurant_map.HGW_restaurant_id IS NOT NULL";

            $HGW_data = $this->db->query($sql)->result_array();

            $sales_date = date('Y-m-d', strtotime('-2 days'));

            foreach ($HGW_data AS $item) {

                $HGW_restaurant_id = $item['HGW_restaurant_id'];

                // Get sales data
                $url = $call_api->server . "pos_transactions";
                $params1 = [];
                $params1['session_token'] = $data['data']['session_token'];
                $params1['filter_restaurant_id'] = $HGW_restaurant_id;
                $params1['filter_complete_date'] = $sales_date;
                $params1['page'] = 1;
                $params1['per_page'] = 20;
                $sig = $call_api->get_signature($url, $params1);
                $params1['sig'] = $sig;

                $sales_return = $call_api->get_page($url, 'get', $params1);
                $total_records = json_decode($sales_return, true);

                $sales_data = [];
                switch ($total_records['status']) {
                    case 200:
                        if ($total_records['total'] <= 20) {
                            $sales_data = $total_records['data'];

                            $json_return1 = addslashes($sales_return);
                            $sql = "INSERT INTO HGW_sales_data (HGW_restaurant_id, date, json_returned, page) VALUES ('$HGW_restaurant_id', '$sales_date', '$json_return1', 1)";
                            $this->db->query($sql);
                        } else {

                            for ($i = 1; $i <= (int)($total_records['total'] / 20) + 1; $i++) {
                                $url = $call_api->server . "pos_transactions";
                                $params2 = [];
                                $params2['session_token'] = $data['data']['session_token'];
                                $params2['filter_restaurant_id'] = $HGW_restaurant_id;
                                $params2['filter_complete_date'] = $sales_date;
                                $params2['page'] = $i;
                                $params2['per_page'] = 20;
                                $sig = $call_api->get_signature($url, $params2);
                                $params2['sig'] = $sig;

                                $sales_return1 = $call_api->get_page($url, 'get', $params2);
                                $sales_data1 = json_decode($sales_return1, true);

                                $json_return = addslashes($sales_return1);

                                $sql = "INSERT INTO HGW_sales_data (HGW_restaurant_id, date, json_returned, page) VALUES ('$HGW_restaurant_id', '$sales_date', '$json_return', $i)";
                                $this->db->query($sql);

                                $sales_data = array_merge($sales_data, $sales_data1['data']);
                            }

                        }
                        break 1;
                    case 201:
                        $sql = "INSERT INTO HGW_sales_data (HGW_restaurant_id, date, json_returned, page) VALUES ('$HGW_restaurant_id', '$sales_date', '$sales_return', 1)";
                        $this->db->query($sql);
                        break 1;
                    default:
                        //Send noti mail
                        $to = 'khanh@smesurvival.com';
                        $title = 'Batch Bug';
                        $content = "<p>Import Sales Data with restaurant_id of " . $HGW_restaurant_id . " for " . $sales_date . " has problems in the get_total_records step </p>";
                        $content .= addslashes($sales_return);
                        $this->send_mail($to, $title, $content);
                        break 1;
                }

                if ($total_records['status'] == 200) {
                    $GST_amount = 0;
                    $discount_amount = 0;
                    $service_charge_amount = 0;
                    $food_amount = 0;
                    $adjustment_amount = 0;

                    $cash_amount = 0;
                    $visa_card_amount = 0;
                    $nets_amount = 0;
                    $master_card_amount = 0;
                    $american_express_amount = 0;
                    $corporate_amount = 0;
                    $internal_voucher_amount = 0;
                    $external_voucher_amount = 0;
                    $ubereats_amount = 0;
                    $deliveroo_amount = 0;
                    $foodpanda_amount = 0;
                    $entertainment_amount = 0;
                    $apple_pay_amount = 0;
                    $android_pay_amount = 0;
                    $alipay_amount = 0;
                    $fave_amount = 0;
                    $unionpay_amount = 0;
                    $jcb_amount = 0;
                    $paynow_amount = 0;

                    $acc_database = $item['database'];
                    $pos_database = 'l12com_pos_' . strtolower($item['company_id']);

                    $sql = "SELECT $acc_database.HGW_payment_mapping.HGW_payment_method_id, $acc_database.HGW_payment_mapping.payment_method_id,
        $pos_database.t_payment_method.name AS payment_method_name
        FROM $acc_database.HGW_payment_mapping
        LEFT JOIN $pos_database.t_payment_method ON $pos_database.t_payment_method.id = $acc_database.HGW_payment_mapping.payment_method_id
        WHERE $acc_database.HGW_payment_mapping.outlet_id = " . $item['outlet_id'];

                    $payment_method_mapping = $this->db->query($sql)->result_array();

                    $payment_method_mapping_array = [];
                    foreach ($payment_method_mapping AS $mapping) {

                        $payment_method_mapping_array[$mapping['HGW_payment_method_id']] = $mapping;

                    }

                    foreach ($sales_data AS $sale) {

                        if ($sale['total_pay'] != 0) {

                            $raw_data = json_decode($sale['raw_data'], true);

                            $total_payments = 0;
                            foreach ($raw_data['receipt']['payments'] AS $payment) {

                                $total_payments += ($payment['amount'] - $payment['change']);

                                switch ($payment_method_mapping_array[$payment['payment_id']]['payment_method_name']) {
                                    case 'Cash':
                                        $cash_amount += ($payment['amount'] - $payment['change']);
                                        break;
                                    case 'Visa Card':
                                        $visa_card_amount += ($payment['amount'] - $payment['change']);
                                        break;
                                    case 'Nets':
                                        $nets_amount += ($payment['amount'] - $payment['change']);
                                        break;
                                    case 'Internal Voucher':
                                        $internal_voucher_amount += ($payment['amount'] - $payment['change']);
                                        break;
                                    case 'External Voucher':
                                        $external_voucher_amount += ($payment['amount'] - $payment['change']);
                                        break;
                                    case 'Master Card':
                                        $master_card_amount += ($payment['amount'] - $payment['change']);
                                        break;
                                    case 'American Express':
                                        $american_express_amount += ($payment['amount'] - $payment['change']);
                                        break;
                                    case 'Corporate':
                                        $corporate_amount += ($payment['amount'] - $payment['change']);
                                        break;
                                    case 'UberEATS':
                                        $ubereats_amount += ($payment['amount'] - $payment['change']);
                                        break;
                                    case 'Deliveroo':
                                        $deliveroo_amount += ($payment['amount'] - $payment['change']);
                                        break;
                                    case 'FoodPanda':
                                        $foodpanda_amount += ($payment['amount'] - $payment['change']);
                                        break;
                                    case 'Entertainment':
                                        $entertainment_amount += ($payment['amount'] - $payment['change']);
                                        break;
                                    case 'Apple Pay':
                                        $apple_pay_amount += ($payment['amount'] - $payment['change']);
                                        break;
                                    case 'Android Pay':
                                        $android_pay_amount += ($payment['amount'] - $payment['change']);
                                        break;
                                    case 'AliPay':
                                        $alipay_amount += ($payment['amount'] - $payment['change']);
                                        break;
                                    case 'PayNow':
                                        $paynow_amount += ($payment['amount'] - $payment['change']);
                                        break;
                                    case 'Fave':
                                        $fave_amount += ($payment['amount'] - $payment['change']);
                                        break;
                                    case 'UnionPay':
                                        $unionpay_amount += ($payment['amount'] - $payment['change']);
                                        break;
                                    case 'JCB':
                                        $jcb_amount += ($payment['amount'] - $payment['change']);
                                        break;

                                }

                            }

                            if ($total_payments != 0) {
                                $GST_amount += $sale['tax'];
                                $discount_amount -= $sale['discount'];
                                $service_charge_amount += $sale['service_charge'];
                                $food_amount += $sale['amount'];
                                $adjustment_amount -= $sale['rounding'];
                            }
                        }


                    }

                    $sql = "SELECT $acc_database.project.name, $acc_database.project.code FROM $acc_database.project WHERE $acc_database.project.id = " . $item['outlet_id'];

                    $outlet = $this->db->query($sql)->result_array();

                    // Add POS data to GL entry
                    $outlet_name = $outlet[0]['name'] . ' (' . $outlet[0]['code'] . ')';

                    $transactions = [];

                    if ($GST_amount != 0) {
                        $transactions[] = [
                            'account_code' => '21401',
                            'amount' => $GST_amount,
                            'description' => 'GST Total',
                            'type' => 'credit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $GST_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($service_charge_amount != 0) {
                        $transactions[] = [
                            'account_code' => '40110',
                            'amount' => $service_charge_amount,
                            'description' => 'Service Charge Total',
                            'type' => 'credit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $service_charge_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($discount_amount != 0) {
                        $transactions[] = [
                            'account_code' => '40210',
                            'amount' => $discount_amount,
                            'description' => 'Overall Discount Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $discount_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($cash_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14005',
                            'amount' => $cash_amount,
                            'description' => 'Cash Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $cash_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($visa_card_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14002',
                            'amount' => $visa_card_amount,
                            'description' => 'Visa Card Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $visa_card_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($nets_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14004',
                            'amount' => $nets_amount,
                            'description' => 'Nets Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $nets_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($internal_voucher_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14014',
                            'amount' => $internal_voucher_amount,
                            'description' => 'Internal Voucher Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $internal_voucher_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($external_voucher_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14013',
                            'amount' => $external_voucher_amount,
                            'description' => 'External Voucher Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $external_voucher_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($master_card_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14003',
                            'amount' => $master_card_amount,
                            'description' => 'Master Card Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $master_card_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($american_express_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14001',
                            'amount' => $american_express_amount,
                            'description' => 'American Express Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $american_express_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($corporate_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14008',
                            'amount' => $corporate_amount,
                            'description' => 'Corporate Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $corporate_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($ubereats_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14017',
                            'amount' => $ubereats_amount,
                            'description' => 'UberEATS Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $ubereats_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($deliveroo_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14018',
                            'amount' => $deliveroo_amount,
                            'description' => 'Deliveroo Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $deliveroo_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($foodpanda_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14019',
                            'amount' => $foodpanda_amount,
                            'description' => 'FoodPanda Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $foodpanda_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($food_amount != 0) {
                        $transactions[] = [
                            'account_code' => '40010',
                            'amount' => $food_amount,
                            'description' => 'Food Sales Total',
                            'type' => 'credit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $food_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($entertainment_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14020',
                            'amount' => $entertainment_amount,
                            'description' => 'Entertainment Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $entertainment_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($apple_pay_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14021',
                            'amount' => $apple_pay_amount,
                            'description' => 'Apple Pay Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $apple_pay_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($android_pay_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14022',
                            'amount' => $android_pay_amount,
                            'description' => 'Android Pay Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $android_pay_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($alipay_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14023',
                            'amount' => $alipay_amount,
                            'description' => 'AliPay Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $alipay_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($paynow_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14024',
                            'amount' => $paynow_amount,
                            'description' => 'PayNow Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $paynow_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($fave_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14025',
                            'amount' => $fave_amount,
                            'description' => 'Fave Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $fave_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($jcb_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14027',
                            'amount' => $jcb_amount,
                            'description' => 'JCB Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $jcb_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($unionpay_amount != 0) {
                        $transactions[] = [
                            'account_code' => '14026',
                            'amount' => $unionpay_amount,
                            'description' => 'UnionPay Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $unionpay_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    if ($adjustment_amount != 0) {
                        $transactions[] = [
                            'account_code' => '40220',
                            'amount' => $adjustment_amount,
                            'description' => 'Adjustment Total',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $adjustment_amount,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];
                    }

                    $total_sales = $food_amount
                        + $GST_amount + $service_charge_amount;

                    $total_collection = $discount_amount + $adjustment_amount +$cash_amount + $visa_card_amount
                        + $nets_amount + $master_card_amount + $american_express_amount + $corporate_amount +
                        $internal_voucher_amount + $external_voucher_amount + $ubereats_amount + $deliveroo_amount
                        + $foodpanda_amount + $entertainment_amount + $apple_pay_amount + $android_pay_amount +
                        $alipay_amount + $fave_amount + $paynow_amount + $unionpay_amount + $jcb_amount;

                    if (round($total_sales, 2) != round($total_collection, 2)) {

                        $diff = $total_sales - $total_collection;

                        $transactions[] = [
                            'account_code' => '40220',
                            'amount' => $diff,
                            'description' => 'System Adjustment',
                            'type' => 'debit',
                            'project' => [0 => [
                                'percentage' => '100',
                                'amount' => $diff,
                                'project_id' => $item['outlet_id']
                            ]
                            ]
                        ];

                        if (abs($diff) > 2) {

                            //Send noti mail
                            $to = 'khanh@smesurvival.com';
                            $title = 'Warning!!! Rounding is too much';
                            $content = "<p>The accounting system rounded $ $diff for restaurant_id: $HGW_restaurant_id on $sales_date</p>";
                            $this->send_mail($to, $title, $content);

                        }

                    }

                    if (!empty($transactions)) {

                        $pos_date = $sales_date;

                        $GL_entry = [
                            'type' => 'POS',
                            'document_date' => $pos_date,
                            'description' => 'POS entry on date ' . $pos_date . ' for ' . $outlet_name,
                            'total_amount' => $total_sales,
                            'transactions' => $transactions
                        ];

                        // Save to GL Entry

                        $query = "INSERT INTO " . $acc_database . ".GL_entry (type, document_date, description, total_amount) 
            VALUES ('" . $GL_entry['type'] . "', '" . $GL_entry['document_date'] . "', '" . $GL_entry['description'] . "', " . $GL_entry['total_amount'] . ")";
                        $this->db->query($query);
                        $GL_entry['id'] = $this->db->insert_id();

                        foreach ($GL_entry['transactions'] AS $transaction) {

                            $query = "INSERT INTO " . $acc_database . ".GL_transaction (type, amount, transaction_type, description) 
            VALUES ('" . $transaction['type'] . "', " . $transaction['amount'] . ", 'GL', '" . $transaction['description'] . "')";
                            $this->db->query($query);
                            $transaction_id = $this->db->insert_id();

                            $query = "INSERT INTO " . $acc_database . ".GL_mapping (GL_transaction_id, GL_entry_id) 
            VALUES (" . $transaction_id . ", " . $GL_entry['id'] . ")";
                            $this->db->query($query);

                            $transaction['id'] = $transaction_id;

                            foreach ($transaction['project'] AS $project) {
                                $sql = "SELECT " . $acc_database . ".account_project.id FROM " . $acc_database . ".account_project
    LEFT JOIN " . $acc_database . ".master_account ON " . $acc_database . ".account_project.account_id = " . $acc_database . ".master_account.id
    WHERE " . $acc_database . ".account_project.deleted_at IS NULL AND " . $acc_database . ".account_project.project_id = " . $project['project_id'] . "
    AND " . $acc_database . ".master_account.code = " . $transaction['account_code'];

                                $account_project_id = $this->db->query($sql)->result_array();
                                $project ['account_project_id'] = $account_project_id[0]['id'];
                                $project ['GL_transaction_id'] = $transaction['id'];
                                $project ['document_date'] = $GL_entry['document_date'];

                                $query = "INSERT INTO " . $acc_database . ".GL_transaction_project_split (amount, percentage, account_project_id, GL_transaction_id, document_date) 
            VALUES (" . $project['amount'] . ", " . $project['percentage'] . ", " . $project['account_project_id'] . ", " . $project['GL_transaction_id'] . ", '" . $project['document_date'] . "')";
                                $this->db->query($query);
                            }
                        }
                    }
                }
            }

        } else {

            //Send noti mail
            $to = 'khanh@smesurvival.com';
            $title = 'Batch Bug';
            $content = "<p>Import Sales Data has problems in auth login step </p>";
            $content .= addslashes($return);
            $this->send_mail($to, $title, $content);
            return;

        }
    }

    private function send_mail($to, $title, $content) {

        $ci =& get_instance();
        $ci->load->library('email');
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://email-smtp.us-east-1.amazonaws.com',
            'smtp_port' => '465',
            'smtp_user' => 'AKIAJQVMBXFJSMFSJWSA',
            'smtp_pass' => 'Aq7GWtvqT+sl6qYdW+SxXHFeR7XPs77rKb/965hEh+J6',
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'wordwrap' => TRUE,
            'newline' => "\r\n"
        );
        $headers = "Content-Type: text/html; charset=UTF-8";
        $ci->email->initialize($config);
        //$ci->email->set_newline("\r\n");
        //mb_convert_encoding($title, "UTF-8");
        //mb_convert_encoding($content, "UTF-8");
        $ci->email->from('receipt.smesurvival@gmail.com', 'SME Survival');
        $ci->email->to($to);
        $ci->email->subject($title);
        $ci->email->message($content);

        $ci->email->send();

    }


}
