<?php
class Financial_period extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';
    }

    public function index()
    {
        $data = $this->data;

        $this->load->model('financial_period_model');
        $this->load->model('company_model');

        $company_data = $this->company_model->find(1);

        $data['locked_financial_month'] = $locked_financial_month = $company_data['locked_financial_month'];

        $a_month_ago = date('Y-m-d', strtotime($locked_financial_month.'-01 +1 month'));
        $sql = "SELECT id FROM financial_period where starting_date = '$a_month_ago' AND deleted_at IS NULL";

        $starting_check1 = $this->db->query($sql)->result_array();

        $aday_ago = date('Y-m-d', strtotime($locked_financial_month.'-01 +1 month -1 day'));
        $sql = "SELECT id FROM financial_period where ending_date = '$aday_ago' AND deleted_at IS NULL";

        $starting_check2 = $this->db->query($sql)->result_array();

        $data['disable_starting'] = !empty($starting_check1) && empty($starting_check2) ? TRUE : FALSE;

        $last_day_of_the_month = date('Y-m-d', strtotime($locked_financial_month.'-01 +1 month -1 day'));
        $sql = "SELECT id FROM financial_period where ending_date = '$last_day_of_the_month' AND deleted_at IS NULL";

        $ending_check1 = $this->db->query($sql)->result_array()[0];

        $tomorrow = date('Y-m-d', strtotime($locked_financial_month.'-01 +1 month'));
        $sql = "SELECT id FROM financial_period where starting_date = '$tomorrow' AND deleted_at IS NULL";

        $ending_check2 = $this->db->query($sql)->result_array()[0];

        $data['disable_ending'] = !empty($ending_check1) && empty($ending_check2) ? TRUE : FALSE;

        $data['financial_periods'] = $this->financial_period_model->all(' order by starting_date desc');

        $sql = "SELECT clear_bank_submit.month, clear_bank_submit.bank_account_id, 
    bank_account.name, bank_account.number, bank_account.account_code
    FROM clear_bank_submit 
    LEFT JOIN bank_account ON bank_account.id = clear_bank_submit.bank_account_id
    where clear_bank_submit.created_at = (select max(created_at) from clear_bank_submit);";

        $data['clear_bank_recons'] = $this->db->query($sql)->result_array()[0];

        return $this->template->loadView("financial_period/index", $data, "admin");
    }

    public function edit()
    {
        if($this->input_data) {
            $this->load->model('financial_period_model');

            $input_data = $this->input_data['post'];

            if (!empty($input_data['id'])) {
                $this->financial_period_model->update($input_data['id'], $input_data);
                $this->__set_flash_message('Financial Period is updated successfully');
                redirect('financial_period');
                return;
            } else {
                $this->financial_period_model->add($input_data);
                $this->__set_flash_message('Financial Period is created successfully');
                redirect('financial_period');
                return;
            }

        }
    }

    public function delete()
    {
        if($this->input_data) {
            $this->load->model('financial_period_model');

            $input_data = $this->input_data['post'];

            $this->financial_period_model->delete($input_data['id']);
            $this->__set_flash_message('Financial Period is deleted successfully');
            redirect('financial_period');
            return;

        }
    }

    public function change_status($id = NULL, $status = NULL)
    {
        $this->load->model('financial_period_model');

        $this->financial_period_model->update($id, [
            'status' => $status
        ]);

        $message = $status == 'locked' ? 'Financial Period is locked successfully' : 'Financial Period is unlocked successfully';
        $this->__set_flash_message($message);
        redirect('financial_period');
        return;
    }

    public function check_edit()
    {
        if($this->input_data) {

            $input_data = $this->input_data['post'];
            $this->load->model('financial_period_model');

            $starting_date = strtotime($input_data['starting_date']);
            $ending_date = strtotime($input_data['ending_date']);

            if ($starting_date >= $ending_date) {
                echo json_encode([
                    'error' => TRUE,
                    'message' => 'Starting Date must be larger than Ending Date'
                ]);
                die;
            }

            if (floor(($ending_date - $starting_date) / (60 * 60 * 24 * 30)) > 12) {
                echo json_encode([
                    'error' => TRUE,
                    'message' => 'Financial Period exceed 12 months'
                ]);
                die;
            }

            $financial_periods = $this->financial_period_model->all();

            foreach ($financial_periods AS $financial_period) {
                if (empty($input_data['id'])) {
                    if (($input_data['starting_date'] >= $financial_period['starting_date'] && $input_data['starting_date'] <= $financial_period['ending_date'])
                        || ($input_data['ending_date'] >= $financial_period['starting_date'] && $input_data['ending_date'] <= $financial_period['ending_date'])
                    ) {
                        echo json_encode([
                            'error' => TRUE,
                            'message' => 'Financial Period is overlapped'
                        ]);
                        die;
                    }
                } else {
                    if (($input_data['starting_date'] >= $financial_period['starting_date'] && $input_data['starting_date'] <= $financial_period['ending_date'] && $input_data['id'] != $financial_period['id'])
                        || ($input_data['ending_date'] >= $financial_period['starting_date'] && $input_data['ending_date'] <= $financial_period['ending_date'] && $input_data['id'] != $financial_period['id'])
                    ) {
                        echo json_encode([
                            'error' => TRUE,
                            'message' => 'Financial Period is overlapped'
                        ]);
                        die;
                    }
                }

            }

            echo json_encode([
                'error' => FALSE
            ]);
            die;
        }
    }

    public function lock($month = null) {

        if (!empty($month)) {

            $this->load->model('company_model');

            $this->company_model->update(1, [
                'locked_financial_month' => $month
            ]);

            $this->__set_flash_message($month.' is locked successfully');
            redirect('financial_period');
            return;

        }

    }
}
