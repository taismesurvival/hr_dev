<?php
class Company extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';
    }

    /**
     * index page, for showing list of data
     */
    public function index(){

        $data = $this->data;
        $this->load->model('company_model');

        $data['company']=$this->company_model->find(1);

        $sql = "SELECT clear_bank_submit.month, clear_bank_submit.bank_account_id, 
    bank_account.name, bank_account.number, bank_account.account_code
    FROM clear_bank_submit 
    LEFT JOIN bank_account ON bank_account.id = clear_bank_submit.bank_account_id
    where clear_bank_submit.created_at = (select max(created_at) from clear_bank_submit);";

        $data['clear_bank_recons'] = $this->db->query($sql)->result_array()[0];

        $this_month = date('Y-m');
        $today = date('Y-m-d');
        $auto_lock = $this_month.'-27';

        if (strtotime($today) >= strtotime($auto_lock)) {

            $data['next_auto_lock'] = date('Y-m-d', strtotime($auto_lock.' +1 month'));

        } else {

            $data['next_auto_lock'] = $auto_lock;

        }
        $data['locked_month'] = date('Y-m', strtotime($data['next_auto_lock'].' -2 months'));

        $pos_database = 'l12com_pos_' . strtolower($data['company']['company_id']);
        $acc_database = 'l12com_acc_' . strtolower($data['company']['company_id']);

        $master_database = $this->config->item("master_database");

        $sql = "SELECT id, name, code FROM project 
        WHERE deleted_at IS NULL AND status =  'active' AND type = 'outlet'";

        $outlets = $this->db->query($sql)->result_array();

        $data['unmaping_payment_methods'] = [];

        foreach ($outlets AS $outlet) {

            $call_api = new HungryGoWhere();

            // Try auth api
            $url = $call_api->server."auth";
            $params = $call_api->token;
            $sig = $call_api->get_signature($url, $params);
            $params['sig'] = $sig;

            $return = $call_api->get_page($url, 'post', $params);
            $data_return = json_decode($return, true);

            if ($data_return['status'] == 200) {

                $sql = "SELECT $master_database.HGW_restaurant_map.HGW_restaurant_id FROM $master_database.HGW_restaurant_map 
        LEFT JOIN $master_database.company ON $master_database.company.id = $master_database.HGW_restaurant_map.company_id
        WHERE $master_database.HGW_restaurant_map.outlet_id = '".$outlet['id']."' AND $master_database.company.`database` = '$acc_database'";

                $HGW_restaurant_id = $this->db->query($sql)->result_array();

                if (!empty($HGW_restaurant_id)) {
                    $url = $call_api->server . $HGW_restaurant_id[0]['HGW_restaurant_id']."/pos/payment";
                    $params1['session_token'] = $data_return['data']['session_token'];
                    $sig = $call_api->get_signature($url, $params1);
                    $params1['sig'] = $sig;

                    $return = $call_api->get_page($url, 'get', $params1);
                    $data_return = json_decode($return, true);
                    if ($data_return['status'] == 200) {
                        foreach ($data_return['data']['body'] AS $item) {

                            if (empty($item['deleted_at'])) {

                                $sql = "SELECT payment_method_id, id 
                        FROM HGW_payment_mapping 
                        WHERE outlet_id = '".$outlet['id']."' AND HGW_payment_method_id = '".$item['id']."' AND payment_method_id IS NOT NULL";

                                $payment_method_data = $this->db->query($sql)->result_array();

                                if (empty($payment_method_data)) {

                                    $data['unmaping_payment_methods'][$outlet['id']]['outlet_name'] = $outlet['name'];
                                    $data['unmaping_payment_methods'][$outlet['id']]['outlet_code'] = $outlet['code'];

                                    $data['unmaping_payment_methods'][$outlet['id']]['HGW_payment_method'][$item['id']] = $item['name'];

                                }
                            }

                        }
                    }


                }
            }
        }

        return $this->template->loadView("company/dashboard", $data, "admin");
    }

    public function profile(){
        // Load model
        $this->load->model('company_model');

        $data['title'] = "[[COMPANY_BASIC_INFORMATION]]";

        $data['company']=$this->company_model->find(1);

        //$data['access_company'] = $this->access_management->check_access("company");

        $data['action']= base_url().'company/profile';

        if ($this->input_data) {
            $input_data = $this->input_data['post'];

            $this->company_model->update(1, $input_data);
            $this->__set_flash_message('Company Information is saved successfully');
            redirect('company');
            return;
        }

        return $this->template->loadView('company/profile', $data, 'admin');
    }
}
