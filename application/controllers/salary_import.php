<?php
class salary_import extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';
    }

    /**
     * index page, for showing list of data
     */
    public function index(){

        $data = $this->data;

        $userdata = $this->session->userdata("user_data");

        $data['hide_bar'] = $userdata['role_id'] == 3 ? TRUE : FALSE;

        $sql = "SELECT id, name, code, type
        FROM project
        WHERE deleted_at IS NULL AND status = 'active'";

        $data['project_list'] = $this->db->query($sql)->result_array();

        $sql = "SELECT id, name, number, account_code FROM bank_account 
        WHERE deleted_at IS NULL AND type = 'bank'";

        $bank_accounts = $this->db->query($sql)->result_array();

        $data['bank_accounts_selector'] = '<option selected value="">Please Select</option>';
        foreach ($bank_accounts AS $item) {
            $data['bank_accounts_selector'] .= '<option value="'.$item['id'].'">'.$item['name'].' ('.$item['number'].')</option>';
        }

        $sql = "SELECT id, name, number, account_code FROM bank_account 
        WHERE deleted_at IS NULL";

        $cash_accounts = $this->db->query($sql)->result_array();
        $data['cash_accounts_selector'] = '<option selected value="">Please Select</option>';
        foreach ($cash_accounts AS $item) {

            if ($item['type'] == 'cash') {
                $data['cash_accounts_selector'] .= '<option value="' . $item['id'] . '">' . $item['name'] . '</option>';
            } else {
                $data['cash_accounts_selector'] .= '<option value="' . $item['id'] . '">' . $item['name'] . ' (' . $item['number'] . ')</option>';
            }
        }


        return $this->template->loadView("salary_import/index", $data);

    }

    public function get_data ()
    {
        $data = [];

        $payroll_month = $this->input_data['post']['payroll_month'];

        $sql2 = "SELECT *
        FROM salary_import
        WHERE payroll_month = '".$payroll_month."' AND deleted_at IS NULL";

        $data['salary_import'] = $this->db->query($sql2)->result_array();

        $sql2 = "SELECT *
        FROM salary_payment
        WHERE payroll_month = '".$payroll_month."' AND deleted_at IS NULL";

        $data['salary_payment'] = $this->db->query($sql2)->result_array();

        echo json_encode($data);
        return;
    }

    public function get_salary ()
    {
        $this->load->model('salary_import_model');
        $this->load->model('salary_payment_model');

        $input_data = $this->input_data['post'];
        $input_data1 = $input_data;

        foreach ($input_data1['non_director'] AS $outlet) {

            if (isset($outlet['net_pay'])) {
                $outlet['payroll_month'] = $input_data1['payroll_month'];

                $this->salary_import_model->add($outlet);
            }

        }

        foreach ($input_data1['payment_modes'] AS $payment) {

            $payment['payroll_month'] = $input_data1['payroll_month'];

            $this->salary_payment_model->add($payment);

        }

        $salary_date = $input_data['payroll_month'].'-27';

        $total_net_pay = 0;
        foreach($input_data['non_director'] AS $item) {

            $key = $item['project_id'];

            $total_amount = 0;
            foreach ($item AS $i) {
                $total_amount += $i;
            }

            // Get outlet name
            $sql2 = "SELECT project.name, project.code, project.type
                FROM project WHERE project.id = $key";

            $outlet = $this->db->query($sql2)->result_array();
            $outlet_name = $outlet[0]['name'] . ' (' . $outlet[0]['code'] . ')';

            $transactions = [];

            switch ($outlet[0]['type']) {
                case 'outlet':
                    $bonus = '50206';
                    $gross_wage = '50201';
                    $net_pay = '21601';
                    $EE_CPF = '21605';
                    $ER_CPF1 = '50202';
                    $ER_CPF2 = '21605';
                    $ethnic = '21605';
                    $SDL1 = '50203';
                    $SDL2 = '21605';
                    $FWL1 = '50204';
                    $FWL2 = '21607';
                    $pay_items = '50211';

                    $salary_amount = '14101';
                    $salary_GST = '21403';
                    $medical_amount = '50212';
                    $medical_GST = '21403';
                    $others_amount = '50211';
                    $others_GST = '21403';
                    break;

                case 'operating':
                    $bonus = '60106';
                    $gross_wage = '60101';
                    $net_pay = '21601';
                    $EE_CPF = '21605';
                    $ER_CPF1 = '60102';
                    $ER_CPF2 = '21605';
                    $ethnic = '21605';
                    $SDL1 = '60103';
                    $SDL2 = '21605';
                    $FWL1 = '60104';
                    $FWL2 = '21607';
                    $pay_items = '60151';

                    $salary_amount = '14101';
                    $salary_GST = '21403';
                    $medical_amount = '60153';
                    $medical_GST = '21403';
                    $others_amount = '60151';
                    $others_GST = '21403';
                    break;

                case 'director':
                    $bonus = '60201';
                    $gross_wage = '60201';
                    $net_pay = '21601';
                    $EE_CPF = '21605';
                    $ER_CPF1 = '60202';
                    $ER_CPF2 = '21605';
                    $ethnic = '21605';
                    $SDL1 = '60202';
                    $SDL2 = '21605';
                    $FWL1 = '60202';
                    $FWL2 = '21607';
                    $pay_items = '60205';

                    $salary_amount = '14101';
                    $salary_GST = '21403';
                    $medical_amount = '60203';
                    $medical_GST = '21403';
                    $others_amount = '60205';
                    $others_GST = '21403';
                    break;
            }

            if ($item['bonus'] > 0) {
                $transactions[] = [
                    'account_code' => $bonus,
                    'amount' => $item['bonus'],
                    'description' => 'Bonus for '.$outlet[0]['name'],
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['bonus'],
                        'project_id' => $key
                    ]
                    ]
                ];
            }

            if ($item['gross_wage'] > 0) {
                $transactions[] = [
                    'account_code' => $gross_wage,
                    'amount' => $item['gross_wage'],
                    'description' => 'Salary for '.$outlet[0]['name'],
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['gross_wage'],
                        'project_id' => $key
                    ]
                    ]
                ];
            }

            if ($item['net_pay'] > 0) {

                $total_net_pay += $item['net_pay'];

                $transactions[] = [
                    'account_code' => $net_pay,
                    'amount' => $item['net_pay'],
                    'description' => 'Salary Accrual for '.$outlet[0]['name'],
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['net_pay'],
                        'project_id' => $key
                    ]
                    ]
                ];
            }

            if ($item['EE_CPF'] > 0) {
                $transactions[] = [
                    'account_code' => $EE_CPF,
                    'amount' => $item['EE_CPF'],
                    'description' => ' EE CPF for '.$outlet[0]['name'],
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['EE_CPF'],
                        'project_id' => $key
                    ]
                    ]
                ];
            }

            if ($item['ER_CPF'] > 0) {
                $transactions[] = [
                    'account_code' => $ER_CPF1,
                    'amount' => $item['ER_CPF'],
                    'description' => 'ER CPF for '.$outlet[0]['name'],
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['ER_CPF'],
                        'project_id' => $key
                    ]
                    ]
                ];

                $transactions[] = [
                    'account_code' => $ER_CPF2,
                    'amount' => $item['ER_CPF'],
                    'description' => 'ER CPF Accrual for '.$outlet[0]['name'],
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['ER_CPF'],
                        'project_id' => $key
                    ]
                    ]
                ];
            }

            if ($item['ethnic'] > 0) {
                $transactions[] = [
                    'account_code' => $ethnic,
                    'amount' => $item['ethnic'],
                    'description' => 'Ethnic Accrual for '.$outlet[0]['name'],
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['ethnic'],
                        'project_id' => $key
                    ]
                    ]
                ];
            }

            if ($item['SDL'] > 0) {
                $transactions[] = [
                    'account_code' => $SDL1,
                    'amount' => $item['SDL'],
                    'description' => 'SDL for '.$outlet[0]['name'],
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['SDL'],
                        'project_id' => $key
                    ]
                    ]
                ];

                $transactions[] = [
                    'account_code' => $SDL2,
                    'amount' => $item['SDL'],
                    'description' => 'SDL Accrual for '.$outlet[0]['name'],
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['SDL'],
                        'project_id' => $key
                    ]
                    ]
                ];
            }

            if ($item['FWL'] > 0) {
                $transactions[] = [
                    'account_code' => $FWL1,
                    'amount' => $item['FWL'],
                    'description' => 'FWL for '.$outlet[0]['name'],
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['FWL'],
                        'project_id' => $key
                    ]
                    ]
                ];

                $transactions[] = [
                    'account_code' => $FWL2,
                    'amount' => $item['FWL'],
                    'description' => 'FWL Accrual for '.$outlet[0]['name'],
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['FWL'],
                        'project_id' => $key
                    ]
                    ]
                ];
            }

            if ($item['pay_items'] > 0) {
                $transactions[] = [
                    'account_code' => $pay_items,
                    'amount' => $item['pay_items'],
                    'description' => 'Pay Items for '.$outlet[0]['name'],
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['pay_items'],
                        'project_id' => $key
                    ]
                    ]
                ];

                $transactions[] = [
                    'account_code' => $pay_items,
                    'amount' => $item['pay_items'],
                    'description' => 'Pay Items for '.$outlet[0]['name'],
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['pay_items'],
                        'project_id' => $key
                    ]
                    ]
                ];
            }

            if ($item['salary_amount'] > 0) {
                $transactions[] = [
                    'account_code' => $salary_amount,
                    'amount' => $item['salary_amount'],
                    'description' => 'Salary Amount for '.$outlet[0]['name'],
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['salary_amount'],
                        'project_id' => $key
                    ]
                    ]
                ];
            }

            if ($item['salary_GST'] > 0) {
                $transactions[] = [
                    'account_code' => $salary_GST,
                    'amount' => $item['salary_GST'],
                    'description' => 'Salary GST for '.$outlet[0]['name'],
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['salary_GST'],
                        'project_id' => $key
                    ]
                    ]
                ];
            }

            if ($item['medical_amount'] > 0) {
                $transactions[] = [
                    'account_code' => $medical_amount,
                    'amount' => $item['medical_amount'],
                    'description' => 'Medical Amount for '.$outlet[0]['name'],
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['medical_amount'],
                        'project_id' => $key
                    ]
                    ]
                ];
            }

            if ($item['medical_GST'] > 0) {
                $transactions[] = [
                    'account_code' => $medical_GST,
                    'amount' => $item['medical_GST'],
                    'description' => 'Medical GST for '.$outlet[0]['name'],
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['medical_GST'],
                        'project_id' => $key
                    ]
                    ]
                ];
            }

            if ($item['others_amount'] > 0) {
                $transactions[] = [
                    'account_code' => $others_amount,
                    'amount' => $item['others_amount'],
                    'description' => 'Others Amount for '.$outlet[0]['name'],
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['others_amount'],
                        'project_id' => $key
                    ]
                    ]
                ];
            }

            if ($item['others_GST'] > 0) {
                $transactions[] = [
                    'account_code' => $others_GST,
                    'amount' => $item['others_GST'],
                    'description' => 'Others GST for '.$outlet[0]['name'],
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['others_GST'],
                        'project_id' => $key
                    ]
                    ]
                ];
            }


            if (!empty($transactions)) {
                $GL_entry = [
                    'type' => 'salary',
                    'document_date' => $salary_date,
                    'description' => 'Salary entry in month ' . $input_data['payroll_month'] . ' for ' . $outlet_name,
                    'total_amount' => ($item['net_pay']+$item['EE_CPF']+$item['ER_CPF']+$item['ethnic']+$item['SDL']+$item['FWL']+$item['pay_items']),
                    'transactions' => $transactions
                ];

                // Save to GL Entry

                $input_data = $GL_entry;

                $query = "INSERT INTO GL_entry (type, document_date, description, total_amount) 
                VALUES ('" . $input_data['type'] . "', '" . $input_data['document_date'] . "', '" . $input_data['description'] . "', " . $input_data['total_amount'] . ")";
                $this->db->query($query);
                $input_data['id'] = $this->db->insert_id();
                //$this->__set_flash_message('Daily Sales from POS is submitted successfully');

                foreach ($input_data['transactions'] AS $transaction) {

                    $query = "INSERT INTO GL_transaction (type, amount, transaction_type, description) 
                VALUES ('" . $transaction['type'] . "', " . $transaction['amount'] . ", 'GL', '" . $transaction['description'] . "')";
                    $this->db->query($query);
                    $transaction_id = $this->db->insert_id();

                    $query = "INSERT INTO GL_mapping (GL_transaction_id, GL_entry_id) 
                VALUES (" . $transaction_id . ", " . $input_data['id'] . ")";
                    $this->db->query($query);

                    $transaction['id'] = $transaction_id;

                    foreach ($transaction['project'] AS $project) {
                        $sql = "SELECT account_project.id FROM account_project
        LEFT JOIN master_account ON account_project.account_id = master_account.id
        WHERE account_project.deleted_at IS NULL AND account_project.project_id = " . $project['project_id'] . "
        AND master_account.code = " . $transaction['account_code'];

                        $account_project_id = $this->db->query($sql)->result_array();
                        $project ['account_project_id'] = $account_project_id[0]['id'];
                        $project ['GL_transaction_id'] = $transaction['id'];
                        $project ['document_date'] = $input_data['document_date'];

                        $query = "INSERT INTO GL_transaction_project_split (amount, percentage, account_project_id, GL_transaction_id, document_date) 
                VALUES (" . $project['amount'] . ", " . $project['percentage'] . ", " . $project['account_project_id'] . ", " . $project['GL_transaction_id'] . ", '" . $project['document_date'] . "')";
                        $this->db->query($query);
                    }
                }
            }

        }

        foreach ($input_data1['payment_modes'] AS $item) {
            $this->load->model('bank_reconciliation_model');
            $this->load->model('GL_transaction_model');
            $this->load->model('GL_transaction_project_split_model');

            $item['type'] = 'AP';
            $bank_recons_id = $this->bank_reconciliation_model->add($item);

            $GL_transaction_id_bank_recon = $this->GL_transaction_model->add([
                'type' => 'credit',
                'amount' => $item['amount'],
                'transaction_type' => 'bank_reconciliation'
            ]);

            $bank_reconciliation_projects = [];
            $payment_GL_trans_ids = [];

            foreach ($input_data1['non_director'] AS $key => $net_pay) {
                if ($net_pay['net_pay'] > 0) {
                    $project_id = $net_pay['project_id'];

                    $amount = round((($net_pay['net_pay'] / $total_net_pay) * $item['amount']), 2);

                    $payment_GL_trans_ids[] = $GL_transaction_id = $this->GL_transaction_model->add([
                        'type' => 'debit',
                        'amount' => $amount,
                        'transaction_type' => 'GL'
                    ]);

                    $sql = "SELECT account_project.id FROM account_project
        INNER JOIN master_account ON account_project.account_id = master_account.id
        WHERE master_account.code = '21601' AND account_project.project_id = '$project_id'";

                    $account_project = $this->db->query($sql)->result_array();

                    $bank_reconciliation_projects[$net_pay['project_id']] += $amount;

                    $project = [];
                    $project['GL_transaction_id'] = $GL_transaction_id;
                    $project ['document_date'] = $item['pay_date'];
                    $project ['amount'] = $amount;
                    $project ['percentage'] = 100;
                    $project ['account_project_id'] = $account_project[0]['id'];
                    $this->GL_transaction_project_split_model->add($project);
                }
            }

            foreach ($bank_reconciliation_projects AS $project_id => $project_amount) {

                $sql = "SELECT account_project.id FROM account_project
    INNER JOIN master_account ON account_project.account_id = master_account.id
    INNER JOIN bank_account ON bank_account.account_code = master_account.code
    WHERE account_project.project_id = " . $project_id . " AND bank_account.id = " . $item['bank_account_id'];

                $account_project_id_bank_recon = $this->db->query($sql)->result_array()[0]['id'];

                $this->GL_transaction_project_split_model->add([
                    'amount' => $project_amount,
                    'percentage' => round($project_amount / $item['amount'] * 100, 2),
                    'account_project_id' => $account_project_id_bank_recon,
                    'GL_transaction_id' => $GL_transaction_id_bank_recon,
                    'document_date' => $item['pay_date']
                ]);
            }

            $this->bank_reconciliation_model->update($bank_recons_id, [
                'payment_recons_GL_transaction_id' => $GL_transaction_id_bank_recon,
                'payment_purchase_GL_transaction_id' => json_encode($payment_GL_trans_ids)
            ]);
        }
        $this->__set_flash_message('Salary Data is submitted successfully');
        echo json_encode([
            'success' => TRUE
        ]);
        return;
    }
}
