<?php
class Bank_account extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';

    }

    public function index()
    {
        $data = $this->data;

        $input_data = $this->input_data['get'];

        $pagination = [
            'page' => (int) $input_data['p'] ? $input_data['p'] : 1,
            'limit' => (int) $input_data['limit'] ? $input_data['p'] : 20
        ];

        $pagination['offset'] = ($pagination['page'] - 1) * $pagination['limit'];

        $data['search'] = $pagination['search'] = $this->input_data['get'];

        $additional_where = '';
        if (!empty($data['search']['key_word'])) {
            $keyword = $data['search']['key_word'];

            $additional_where .= " AND (name LIKE '%$keyword%' OR number LIKE '%$keyword%' OR account_code LIKE '%$keyword%')";
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS id, name, number, account_code, 
        type FROM bank_account 
        WHERE deleted_at IS NULL $additional_where LIMIT ".$pagination['offset'].", ".$pagination['limit'];

        $data['accounts'] = $this->db->query($sql)->result_array();

        $pagination['total'] = $this->db->query('SELECT FOUND_ROWS() AS total;')->result_array()[0]['total'];

        $data['pagination'] = $pagination;

        $sql = "SELECT id, name, number, account_code, type FROM bank_account 
        WHERE is_default = '1'";

        $default_account = $this->db->query($sql)->result_array();

        $data['default_account'] = !empty($default_account) ? $default_account[0]['name'].' '.$default_account[0]['number'].' ('.$default_account[0]['account_code'].')' : '';

        return $this->template->loadView("bank_account/index", $data, "admin");
    }

    public function edit()
    {
        if($this->input_data) {
            $this->load->model('bank_account_model');
            $this->load->model('master_account_model');
            $this->load->model('account_project_model');

            $input_data = $this->input_data['post'];

            if (!empty($input_data['id'])) {
                $sql = "SELECT master_account.id FROM bank_account 
        INNER JOIN master_account  ON bank_account.account_code = master_account.code
        WHERE bank_account.id = ".$input_data['id'];

                $master_account_id = $this->db->query($sql)->result_array()[0]['id'];

                $this->master_account_model->update($master_account_id, [
                    'name' => !empty($input_data['number']) ? $input_data['name'].' ('.$input_data['number'].')' : $input_data['name'],
                    'description' => !empty($input_data['number']) ? $input_data['name'].' ('.$input_data['number'].')' : $input_data['name']
                ]);

                $this->bank_account_model->update($input_data['id'], $input_data);
                $this->__set_flash_message('Account is updated successfully');
                redirect('bank_account');
                return;
            } else {

                $above = $input_data['type'] == 'bank' ? '13030' : '13049';
                $below = $input_data['type'] == 'bank' ? '13010' : '13030';

                $sql = "SELECT max(code) as max_code FROM master_account 
        WHERE deleted_at IS NULL AND code >= $below AND code < $above";

                $max_code = $this->db->query($sql)->result_array()[0]['max_code']+1;

                if (($input_data['type'] == 'bank' && $max_code >= 13030) || ($input_data['type'] == 'cash' && $max_code >= 13049)) {
                    $this->__set_flash_message('Number of Accounts is over the limit');
                    redirect('bank_account');
                    return;
                }

                $father_id = $input_data['type'] == 'bank' ? '158' : '162';

                $account_id = $this->master_account_model->add([
                    'name' => !empty($input_data['number']) ? $input_data['name'].' ('.$input_data['number'].')' : $input_data['name'],
                    'description' => !empty($input_data['number']) ? $input_data['name'].' ('.$input_data['number'].')' : $input_data['name'],
                    'type' => 'asset',
                    'status' => 'active',
                    'code' => $max_code,
                    'level' => 4,
                    'father_id' => $father_id
                ]);

                $sql = "SELECT id FROM project 
        WHERE deleted_at IS NULL";

                $projects = $this->db->query($sql)->result_array();

                foreach ($projects AS $project) {
                    $this->account_project_model->add([
                        'account_id' => $account_id,
                        'project_id' => $project['id']
                    ]);
                }

                $input_data['account_code'] = $max_code;
                $this->bank_account_model->add($input_data);
                $this->__set_flash_message('Account is created successfully');
                redirect('bank_account');
                return;
            }

        }
    }

    public function delete()
    {
        if($this->input_data) {
            $this->load->model('user_model');

            $input_data = $this->input_data['post'];

            $this->user_model->delete($input_data['id']);
            $this->__set_flash_message('User is deleted successfully');
            redirect('user');
            return;

        }
    }

    public function check_edit()
    {
        if($this->input_data) {

            $input_data = $this->input_data['post'];
            $this->load->model('user_model');

            $user = $this->user_model->find($input_data['username'], 'username');

            if (!empty($user) && $input_data['id'] != $user['id']) {
                echo json_encode([
                    'error' => TRUE,
                    'message' => 'User Name has been used'
                ]);
                die;
            }

            echo json_encode([
                'error' => FALSE
            ]);
            die;
        }
    }

    public function set_default ()
    {
        $account_id = $this->input_data['get']['account_id'];

        $this->load->model('bank_account_model');
        $this->bank_account_model->update($account_id, ['is_default' => '1']);

        $sql = "UPDATE bank_account
        SET is_default = '0'
        WHERE id != $account_id";

        $this->db->query($sql);
        redirect('bank_account');

    }
}
