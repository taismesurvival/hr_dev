<?php
class general_listing_report extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';

    }

    /**
     * index page, for showing list of data
     */
    public function view(){

        $data = $this->data;

        $this->load->model('company_model');
        $this->load->model('bank_account_model');
        $company_data = $this->company_model->find(1);

        if($this->input_data) {
            $input_data = $this->input_data['post'];

            $data['company_name'] = $company_data['name'];

            $last_day_of_the_month = date('Y-m-d', strtotime($input_data['month'].'-01 +1 month -1 day'));
            $first_day_of_the_month = $input_data['month'].'-01';

            $data['ending_month'] = date_format(date_create($input_data['month']),"Y F");

            $sql = "SELECT GL_entry.id AS GL_entry_id, GL_entry.type, GL_entry.document_date, GL_entry.description, master_account.name AS account_name,
        master_account.code AS account_code, GL_transaction.type AS GL_type, GL_transaction_project_split.id AS GL_project_id,
        project.name AS project_name, project.code AS project_code, GL_transaction_project_split.amount
        FROM GL_entry 
        LEFT JOIN GL_mapping ON GL_entry.id = GL_mapping.GL_entry_id
        INNER JOIN GL_transaction ON GL_mapping.GL_transaction_id = GL_transaction.id
        LEFT JOIN GL_transaction_project_split ON GL_transaction.id = GL_transaction_project_split.GL_transaction_id
        INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        INNER JOIN master_account ON account_project.account_id = master_account.id
        INNER JOIN project ON account_project.project_id = project.id
        WHERE GL_transaction_project_split.document_date <= '$last_day_of_the_month' AND GL_transaction_project_split.document_date >= '$first_day_of_the_month'
        AND GL_entry.deleted_at IS NULL ORDER BY GL_entry.id ASC";

            $data['GL_entries_data'] = $this->db->query($sql)->result_array();

            $sql = "SELECT purchase_entry.id AS purchase_entry_id, purchase_entry.document_date,
        purchase_entry.total_amount, purchase_entry.description, supplier.name AS supplier_name, 
        purchase_entry.refference_no, purchase_transaction.id AS purchase_transaction_id,
        purchase_transaction.GL_transaction_id, purchase_transaction.tax_GL_transaction_id, purchase_transaction.payable_GL_transaction_id
        FROM purchase_entry 
        
        LEFT JOIN supplier on supplier.id = purchase_entry.supplier_id
        LEFT JOIN purchase_transaction ON purchase_entry.id  = purchase_transaction.purchase_entry_id
        
        WHERE purchase_entry.deleted_at IS NULL AND purchase_entry.type = 'PYMT' AND
         purchase_entry.document_date <= '$last_day_of_the_month' AND purchase_entry.document_date >= '$first_day_of_the_month'
        ORDER BY purchase_entry.id ASC";

            $GL_taxable = $this->db->query($sql)->result_array();

            foreach($GL_taxable AS $item) {

                $additional = $item['GL_transaction_id'].", ".$item['payable_GL_transaction_id'];

                $additional .= !empty($item['tax_GL_transaction_id']) ? ", ".$item['tax_GL_transaction_id'] : "";

                if (!isset($data['GL_taxable_data'][$item['purchase_entry_id']])) {
                    $data['GL_taxable_data'][$item['purchase_entry_id']] = [
                        'document_date' => $item['document_date'],
                        'total_amount' => $item['total_amount'],
                        'description' => $item['description'],
                        'supplier_name' => $item['supplier_name'],
                        'refference_no' => $item['refference_no']
                    ];
                }

                $sql = "SELECT master_account.name AS account_name,
        master_account.code AS account_code, GL_transaction.type AS GL_type, GL_transaction_project_split.id AS GL_project_id,
        project.name AS project_name, project.code AS project_code, GL_transaction_project_split.amount
        FROM GL_transaction_project_split
        INNER JOIN GL_transaction ON GL_transaction_project_split.GL_transaction_id = GL_transaction.id
        INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        INNER JOIN master_account ON account_project.account_id = master_account.id
        INNER JOIN project ON account_project.project_id = project.id
        WHERE GL_transaction_project_split.GL_transaction_id IN ($additional)";
                
                $data['GL_taxable_data'][$item['purchase_entry_id']]['project_split'] = $this->db->query($sql)->result_array();

            }

            $sql = "SELECT purchase_entry.id AS purchase_entry_id, purchase_entry.document_date,
        purchase_entry.total_amount, purchase_entry.description, supplier.name AS supplier_name, 
        purchase_entry.refference_no, purchase_transaction.id AS purchase_transaction_id,
        purchase_transaction.GL_transaction_id, purchase_transaction.tax_GL_transaction_id, purchase_entry.payment_GL_transaction_id,
        purchase_entry.separate_father_id, purchase_transaction.discount_GL_transaction_id, purchase_entry.status,
        purchase_transaction.payable_GL_transaction_id
        FROM purchase_entry 
        
        LEFT JOIN supplier on supplier.id = purchase_entry.supplier_id
        LEFT JOIN purchase_transaction ON purchase_entry.id  = purchase_transaction.purchase_entry_id
        
        WHERE purchase_entry.deleted_at IS NULL AND purchase_entry.type = 'PURC' AND purchase_entry.separate_father_id IS NULL
        AND purchase_entry.document_date <= '$last_day_of_the_month' AND purchase_entry.document_date >= '$first_day_of_the_month'
        ORDER BY purchase_entry.id ASC";

            $purchases = $this->db->query($sql)->result_array();

            foreach($purchases AS $item) {

                $additional = $item['GL_transaction_id'];

                $additional .= !empty($item['tax_GL_transaction_id']) ? ", ".$item['tax_GL_transaction_id'] : "";

                $additional .= !empty($item['payable_GL_transaction_id']) ? ", ".$item['payable_GL_transaction_id'] : "";

                $additional .= !empty($item['discount_GL_transaction_id']) ? ", ".$item['discount_GL_transaction_id'] : "";

                if (!isset($data['purchases_data'][$item['purchase_entry_id']])) {
                    $data['purchases_data'][$item['purchase_entry_id']] = [
                        'document_date' => $item['document_date'],
                        'total_amount' => $item['total_amount'],
                        'description' => $item['description'],
                        'supplier_name' => $item['supplier_name'],
                        'refference_no' => $item['refference_no'],
                        'separate_father_id' => $item['separate_father_id']
                    ];

                    $sql = "SELECT payment_GL_transaction_id FROM purchase_entry WHERE separate_father_id = ". $item['purchase_entry_id'];
                    $children_records = $this->db->query($sql)->result_array();

                    if (!empty($children_records)) {

                        $payment_additional = $children_records[0]['payment_GL_transaction_id'];
                        unset($children_records[0]);

                        foreach ($children_records AS $record) {

                            $payment_additional .= ', '.$record['payment_GL_transaction_id'];

                        }

                    } else {

                        $payment_additional = $item['payment_GL_transaction_id'];

                    }

                    $sql = "SELECT master_account.name AS account_name,
        master_account.code AS account_code, GL_transaction.type AS GL_type, GL_transaction_project_split.id AS GL_project_id,
        project.name AS project_name, project.code AS project_code, GL_transaction_project_split.amount
        FROM GL_transaction_project_split
        INNER JOIN GL_transaction ON GL_transaction_project_split.GL_transaction_id = GL_transaction.id
        INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        INNER JOIN master_account ON account_project.account_id = master_account.id
        INNER JOIN project ON account_project.project_id = project.id
        WHERE GL_transaction_project_split.GL_transaction_id IN ($payment_additional)";

                    $data['purchases_data'][$item['purchase_entry_id']]['project_split'] = $this->db->query($sql)->result_array();

                }

                $sql = "SELECT master_account.name AS account_name,
        master_account.code AS account_code, GL_transaction.type AS GL_type, GL_transaction_project_split.id AS GL_project_id,
        project.name AS project_name, project.code AS project_code, GL_transaction_project_split.amount
        FROM GL_transaction_project_split
        INNER JOIN GL_transaction ON GL_transaction_project_split.GL_transaction_id = GL_transaction.id
        INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        INNER JOIN master_account ON account_project.account_id = master_account.id
        INNER JOIN project ON account_project.project_id = project.id
        WHERE GL_transaction_project_split.GL_transaction_id IN ($additional)";

                $data['purchases_data'][$item['purchase_entry_id']]['project_split'] = array_merge($this->db->query($sql)->result_array(), $data['purchases_data'][$item['purchase_entry_id']]['project_split']);

            }

            $sql = "SELECT credit_note.id AS credit_note_id, credit_note.document_date,
        credit_note.amount, credit_note.description, supplier.name AS supplier_name, 
        credit_note.refference_no,
        credit_note.main_GL_transaction_id, credit_note.tax_GL_transaction_id, credit_note.payable_GL_transaction_id,
        credit_note.received_bank_GL_transaction_id, credit_note.received_payable_GL_transaction_id
        FROM credit_note 
        
        LEFT JOIN supplier on supplier.id = credit_note.supplier_id
        
        WHERE credit_note.deleted_at IS NULL AND credit_note.status != 'assigned' AND
         credit_note.document_date <= '$last_day_of_the_month' AND credit_note.document_date >= '$first_day_of_the_month'
        ORDER BY credit_note.id ASC";

            $credit_notes = $this->db->query($sql)->result_array();

            foreach($credit_notes AS $item) {

                $additional = $item['main_GL_transaction_id'].", ".$item['payable_GL_transaction_id'];

                $additional .= !empty($item['tax_GL_transaction_id']) ? ", ".$item['tax_GL_transaction_id'] : "";

                $additional .= !empty($item['received_bank_GL_transaction_id']) ? ", ".$item['received_bank_GL_transaction_id'] : "";

                $additional .= !empty($item['received_payable_GL_transaction_id']) ? ", ".$item['received_payable_GL_transaction_id'] : "";

                $data['credit_notes_data'][$item['credit_note_id']] = [
                    'document_date' => $item['document_date'],
                    'amount' => $item['amount'],
                    'description' => $item['description'],
                    'supplier_name' => $item['supplier_name'],
                    'refference_no' => $item['refference_no']
                ];

                $sql = "SELECT master_account.name AS account_name,
        master_account.code AS account_code, GL_transaction.type AS GL_type, GL_transaction_project_split.id AS GL_project_id,
        project.name AS project_name, project.code AS project_code, GL_transaction_project_split.amount
        FROM GL_transaction_project_split
        INNER JOIN GL_transaction ON GL_transaction_project_split.GL_transaction_id = GL_transaction.id
        INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        INNER JOIN master_account ON account_project.account_id = master_account.id
        INNER JOIN project ON account_project.project_id = project.id
        WHERE GL_transaction_project_split.GL_transaction_id IN ($additional)";

                $data['credit_notes_data'][$item['credit_note_id']]['project_split'] = $this->db->query($sql)->result_array();

            }

            $sql = "SELECT bank_reconciliation.id AS bank_reconciliation_id, bank_reconciliation.pay_date AS document_date,
        bank_reconciliation.amount,
        bank_reconciliation.ref_no AS refference_no,
        bank_reconciliation.payment_recons_GL_transaction_id, bank_reconciliation.payment_purchase_GL_transaction_id
        FROM bank_reconciliation
        
        WHERE bank_reconciliation.deleted_at IS NULL AND bank_reconciliation.payment_recons_GL_transaction_id IS NOT NULL
        AND bank_reconciliation.payment_purchase_GL_transaction_id IS NOT NULL AND
         bank_reconciliation.pay_date <= '$last_day_of_the_month' AND bank_reconciliation.pay_date >= '$first_day_of_the_month'
        ORDER BY bank_reconciliation.id ASC";

            $bank_reconciliations = $this->db->query($sql)->result_array();

            foreach($bank_reconciliations AS $item) {

                $purchase_payment_GL_transaction_array = json_decode($item['payment_purchase_GL_transaction_id']);

                $additional = $item['payment_recons_GL_transaction_id'];

                foreach ($purchase_payment_GL_transaction_array AS $record) {

                    $additional .= ', ' . $record;
                }

                $data['bank_recons_data'][$item['bank_reconciliation_id']] = [
                    'document_date' => $item['document_date'],
                    'amount' => $item['amount'],
                    'refference_no' => $item['refference_no']
                ];

                $sql = "SELECT master_account.name AS account_name,
        master_account.code AS account_code, GL_transaction.type AS GL_type, GL_transaction_project_split.id AS GL_project_id,
        project.name AS project_name, project.code AS project_code, GL_transaction_project_split.amount
        FROM GL_transaction_project_split
        INNER JOIN GL_transaction ON GL_transaction_project_split.GL_transaction_id = GL_transaction.id
        INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        INNER JOIN master_account ON account_project.account_id = master_account.id
        INNER JOIN project ON account_project.project_id = project.id
        WHERE GL_transaction_project_split.GL_transaction_id IN ($additional)";

                $data['bank_recons_data'][$item['bank_reconciliation_id']]['project_split'] = $this->db->query($sql)->result_array();

            }

            $sql = "SELECT sales_collection_submit.id AS sales_collection_submit_id, sales_collection_submit.document_date,
        sales_collection_submit.payment_method,
        sales_collection_submit.payable_GL_transaction_id1,
        sales_collection_submit.bank_GL_transaction_id, sales_collection_submit.payable_GL_transaction_id2,
        sales_collection_submit.bank_charge_GL_transaction_id
        FROM sales_collection_submit
        
        WHERE sales_collection_submit.deleted_at IS NULL AND
         sales_collection_submit.document_date <= '$last_day_of_the_month' AND sales_collection_submit.document_date >= '$first_day_of_the_month'
        ORDER BY sales_collection_submit.id ASC";

            $sales_collection_submits = $this->db->query($sql)->result_array();

            foreach($sales_collection_submits AS $item) {

                $additional = $item['bank_GL_transaction_id'];

                $additional .= !empty($item['payable_GL_transaction_id1']) ? ", ".$item['payable_GL_transaction_id1'] : "";

                $additional .= !empty($item['bank_charge_GL_transaction_id']) ? ", ".$item['bank_charge_GL_transaction_id'] : "";

                $additional .= !empty($item['payable_GL_transaction_id2']) ? ", ".$item['payable_GL_transaction_id2'] : "";

                $data['sales_collection_submits_data'][$item['sales_collection_submit_id']] = [
                    'document_date' => $item['document_date'],
                    'payment_method' => $item['payment_method']
                ];

                $sql = "SELECT master_account.name AS account_name,
        master_account.code AS account_code, GL_transaction.type AS GL_type, GL_transaction_project_split.id AS GL_project_id,
        project.name AS project_name, project.code AS project_code, GL_transaction_project_split.amount
        FROM GL_transaction_project_split
        INNER JOIN GL_transaction ON GL_transaction_project_split.GL_transaction_id = GL_transaction.id
        INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        INNER JOIN master_account ON account_project.account_id = master_account.id
        INNER JOIN project ON account_project.project_id = project.id
        WHERE GL_transaction_project_split.GL_transaction_id IN ($additional)";

                $data['sales_collection_submits_data'][$item['sales_collection_submit_id']]['project_split'] = $this->db->query($sql)->result_array();

            }
            return $this->template->loadView("general_listing_report/report", $data);

        }

        return $this->template->loadView("general_listing_report/view", $data, "admin");
    }
}
