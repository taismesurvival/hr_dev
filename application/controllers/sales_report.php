<?php
class sales_report extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';
    }

    /**
     * index page, for showing list of data
     */
    public function index($id = null){

        $userdata = $this->session->userdata("user_data");

        if (!empty($id)) {

            $outlet_ids = json_decode($userdata['outlet_id'], true);

            if (!empty($userdata['outlet_id']) && !in_array($id, $outlet_ids)) {
                redirect('sales_input/select_outlet');
                return;
            }

            $data = $this->data;

            $sql = "SELECT description, code
        FROM master_account
        WHERE deleted_at IS NULL AND (code = '40010' OR code = '40020' OR code = '40030' OR code = '40510') AND status = 'active'";

            $sales_names = $this->db->query($sql)->result_array();

            $data['sales_names'] = [];
            foreach ($sales_names AS $name) {

                $data['sales_names'][$name['code']] = $name['description'];

            }

            $sql = "SELECT description, code
        FROM master_account
        WHERE deleted_at IS NULL AND (code = '14005' OR code = '14002' OR code = '14004' OR code = '14014'
        OR code = '14013' OR code = '14003' OR code = '14001' OR code = '14008' OR code = '14017'
         OR code = '14018' OR code = '14019') AND status = 'active'";

            $debtor_names = $this->db->query($sql)->result_array();

            $data['debtor_names'] = [];
            foreach ($debtor_names AS $name) {

                $data['debtor_names'][$name['code']] = str_replace(' from Sales', '', str_replace('Undeposited ', '', str_replace('Trade Debtor - ', '', $name['description'])));

            }

            if ($this->input_data) {
                $month = $this->input_data['post']['month'];

                $sql2 = "SELECT *
            FROM sales_input
            WHERE sales_date LIKE '%".$month."%' AND project_id = $id AND deleted_at IS NULL ORDER BY sales_date asc";

                $data['sales_data'] = $this->db->query($sql2)->result_array();

                $this->load->model('company_model');
                $company_data = $this->company_model->find(1);

                $data['company_name'] = $company_data['name'];

                $sql = "SELECT name, code
            FROM project
            WHERE id = $id";
                $outlet_name = $this->db->query($sql)->result_array()[0];
                $data['outlet_name'] = $outlet_name['name'].' ('.$outlet_name['code'].')';

                $data['month'] = $month;

                return $this->template->loadView("sales_report/report", $data);
            }

            return $this->template->loadView("sales_report/index", $data);
        } else {

            redirect('sales_input/select_outlet');
            return;

        }

    }
}
