<?php
class sales_collection extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->helper("file");
    }

    public function index()
    {
        $data = $this->data;

        $sql = "SELECT id, name, number, account_code FROM bank_account 
        WHERE deleted_at IS NULL AND type = 'bank'";

        $bank_accounts = $this->db->query($sql)->result_array();

        $data['bank_accounts_selector'] = '<option selected value="">Please Select</option>';
        foreach ($bank_accounts AS $item) {

            if (!empty($item['number'])) {
                $data['bank_accounts_selector'] .= '<option value="' . $item['account_code'] . '">' . $item['name'] . ' (' . $item['number'] . ')</option>';
            } else {
                $data['bank_accounts_selector'] .= '<option value="' . $item['account_code'] . '">' . $item['name']. '</option>';
            }
        }

        $data['search'] = $pagination['search'] = $this->input_data['post'];

        $additional_where = '';

        if (!empty($data['search']['outlet_list'])) {
            $oulet_ids = join(',', $data['search']['outlet_list']);

            $data['search']['outlet_string'] = $oulet_ids;

            $additional_where .= " AND account_project.project_id IN ($oulet_ids)";
        }

        if (!empty($data['search']['selected_month'])) {
            $selected_month = $data['search']['selected_month'].'-01';

            $selected_month = date("Y-m-t", strtotime($selected_month));

            $additional_where .= " AND GL_transaction_project_split.document_date <= '$selected_month'";
        }

        if (!empty($data['search']['payment_method'])) {
            $payment_method = $data['search']['payment_method'];

            switch ($payment_method) {
                case 'cash':
                    $account_code = '14005';
                    $pm_name = 'Cash';
                    $bank_statement_code = 'DCA';
                    break;

                case 'visa_and_master':
                    $account_code = '14002, 14003';
                    $pm_name = 'Visa Card';
                    $bank_statement_code = 'DVM';
                    break;

                case 'nets':
                    $account_code = '14004';
                    $pm_name = 'Nets';
                    $bank_statement_code = 'DNE';
                    break;

                case 'american_express':
                    $account_code = '14001';
                    $pm_name = 'American Express';
                    $bank_statement_code = 'DAM';
                    break;
            }
            $additional_where .= " AND master_account.code IN ($account_code)";
        }

        if (!empty($data['search'])) {

            $this->load->model('company_model');
            $company_data = $this->company_model->find(1);

            $company_code = $company_data['company_id'];

            $sql = "SELECT id FROM bank_account
        WHERE account_code = '".$data['search']['bank_account_id']."'";

            $bank_account_id = $this->db->query($sql)->result_array()[0]['id'];

            $sql = "SELECT * FROM bank_statement_upload
                WHERE deleted_at IS NULL AND month = '".$data['search']['selected_month']."' AND bank_account_id = '".$bank_account_id."'";

            $bank_statement_uploaded = $this->db->query($sql)->result_array();

            $file_path = './upload/accounting/'.strtoupper($company_code).'/manual_bank_statement/'.$bank_statement_uploaded[0]['file_name'];

            $this->load->library('php_excel');
            $objPHPExcel = PHPExcel_IOFactory::load($file_path);
            $total_rows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
            $arr_data = array();
            for ($i = 2; $i <= $total_rows; $i++) {

                $arr_data[$i]['date'] = $objPHPExcel->getActiveSheet()->getCell('B' . $i)->getValue();
                $arr_data[$i]['deposit'] = $objPHPExcel->getActiveSheet()->getCell('C' . $i)->getValue();
                $arr_data[$i]['withdrawal'] = $objPHPExcel->getActiveSheet()->getCell('D' . $i)->getValue();
                $arr_data[$i]['description'] = $objPHPExcel->getActiveSheet()->getCell('E' . $i)->getValue();
            }

            $total_bank_statements = 0;
            foreach ($arr_data AS $item) {

                $pieces = explode(" ", $item['description']);

                if ($pieces[0] == $bank_statement_code && !isset($pieces[1])) {
                    $total_bank_statements += $item['deposit'];
                }
            }

            $data['total_bank_statements'] = $total_bank_statements;

        }

        $data['payments'] = [];

        if (!empty($data['search'])) {
            $sql = "SELECT GL_transaction.id, GL_transaction.amount, account_project.project_id, project.name AS project_name, project.code AS project_code,
        GL_transaction_project_split.document_date, GL_transaction.description, master_account.code AS account_code
        FROM GL_transaction 
        INNER JOIN GL_transaction_project_split ON GL_transaction_project_split.GL_transaction_id  = GL_transaction.id
        INNER JOIN account_project ON account_project.id = GL_transaction_project_split.account_project_id
        INNER JOIN project ON account_project.project_id = project.id
        INNER JOIN master_account ON account_project.account_id = master_account.id
        LEFT JOIN sales_collection ON sales_collection.GL_transaction_id = GL_transaction.id
        WHERE sales_collection.id IS NULL AND GL_transaction.deleted_at IS NULL AND GL_transaction.type = 'debit' AND GL_transaction.transaction_type = 'GL' $additional_where";

            $data['payments'] = $this->db->query($sql)->result_array();

            $this->load->model('company_model');
            $company_data = $this->company_model->find(1);
            $pos_database = 'l12com_pos_'.strtolower($company_data['company_id']);

            $sql = "SELECT $pos_database.t_payment_method.default_bank_charge_rate 
        FROM $pos_database.t_payment_method
        WHERE $pos_database.t_payment_method.deleted_at IS NULL AND $pos_database.t_payment_method.name = '$pm_name'";

            $data['default_bank_charge_rate'] = $this->db->query($sql)->result_array()[0]['default_bank_charge_rate'];

        }

        $sql = "SELECT id, name, code AS description,
        type
        FROM project
        WHERE deleted_at IS NULL AND status = 'active' AND type='outlet'";

        $data['project_list'] = $this->db->query($sql)->result_array();

        return $this->template->loadView("sales_collection/index", $data, "admin");
    }

    public function submit ()
    {
        $input_data = $this->input_data['post'];

        $this->load->model('GL_transaction_model');
        $this->load->model('sales_collection_model');
        $this->load->model('sales_collection_submit_model');
        $this->load->model('GL_transaction_project_split_model');
        $this->load->model('bank_reconciliation_model');

        $input_data['bank_GL_transaction_id'] = $this->GL_transaction_model->add([
            'type' =>'debit',
            'amount' => $input_data['bank_in_amount'],
            'transaction_type' => 'bank_reconciliation'
        ]);

        $selected_month = $input_data['document_date'].'-01';

        $input_data['document_date'] = date("Y-m-t", strtotime($selected_month));

        switch ($input_data['payment_method']) {
            case 'cash':
                $bank_statement_code = 'DCA';
                break;

            case 'visa_and_master':
                $bank_statement_code = 'DVM';
                break;

            case 'nets':
                $bank_statement_code = 'DNE';
                break;

            case 'american_express':
                $bank_statement_code = 'DAM';
                break;
        }

        // Add bank reconciliation
        $sql = "SELECT id FROM bank_account
        WHERE account_code = '".$input_data['bank_account_id']."'";

        $bank_account_id = $this->db->query($sql)->result_array()[0]['id'];

        $input_data['bank_reconciliation_id'] = $this->bank_reconciliation_model->add([
            'bank_account_id' => $bank_account_id,
            'amount' => $input_data['bank_in_amount'],
            'ref_no' => $bank_statement_code,
            'type' => 'AR',
            'pay_date' => $input_data['document_date']
        ]);

        $input_data['bank_charge_GL_transaction_id'] = $this->GL_transaction_model->add([
            'type' =>'debit',
            'amount' => $input_data['bank_charge_amount'],
            'transaction_type' => 'bank_reconciliation'
        ]);

        $sales_collection_submit_id = $this->sales_collection_submit_model->add($input_data);

        $total_projects_account_code = [];
        $total_projects = [];

        foreach ($input_data['transactions'] AS $tran) {

            $tran['sales_collection_submit_id'] = $sales_collection_submit_id;

            $this->sales_collection_model->add($tran);

            $total_projects_account_code[$tran['account_code']]['amount'] += $tran['GL_transaction_amount'];

            $total_projects_account_code[$tran['account_code']]['project'][$tran['project_id']] += $tran['GL_transaction_amount'];
            $total_projects[$tran['project_id']] += $tran['GL_transaction_amount'];

        }

        $payable_GL_transaction_ids = [];
        foreach ($total_projects_account_code AS $account_code => $item) {

            $payable_GL_transaction_ids[] = $payable_GL_transaction_id = $this->GL_transaction_model->add([
                'type' =>'credit',
                'amount' => $item['amount'],
                'transaction_type' => 'bank_reconciliation'
            ]);

            foreach ($item['project'] AS $project_id => $amount) {

                $sql = "SELECT account_project.id FROM account_project
        INNER JOIN master_account ON account_project.account_id = master_account.id
        WHERE master_account.code = '$account_code' AND account_project.project_id = " . $project_id;

                $payable_account_project_id = $this->db->query($sql)->result_array()[0]['id'];

                $this->GL_transaction_project_split_model->add([
                    'amount' => $amount,
                    'percentage' => round($amount/$item['amount']*100, 2),
                    'account_project_id' => $payable_account_project_id,
                    'GL_transaction_id' => $payable_GL_transaction_id,
                    'document_date' => $input_data['document_date']
                ]);
            }

        }

        $this->sales_collection_submit_model->update($sales_collection_submit_id, [
            'payable_GL_transaction_id1' => $payable_GL_transaction_ids[0],
            'payable_GL_transaction_id2' => isset($payable_GL_transaction_ids[1]) ? $payable_GL_transaction_ids[1] : NULL
        ]);

        foreach ($total_projects AS $key => $item) {

            $sql = "SELECT account_project.id FROM account_project
        INNER JOIN master_account ON account_project.account_id = master_account.id
        WHERE master_account.code = '".$input_data['bank_account_id']."' AND account_project.project_id = " . $key;

            $bank_account_project_id = $this->db->query($sql)->result_array()[0]['id'];

            $sql = "SELECT account_project.id FROM account_project
        INNER JOIN master_account ON account_project.account_id = master_account.id
        WHERE master_account.code = '60415' AND account_project.project_id = " . $key;

            $bank_charge_account_project_id = $this->db->query($sql)->result_array()[0]['id'];

            $this->GL_transaction_project_split_model->add([
                'amount' => round(($input_data['bank_in_amount']*$item/$input_data['payable_amount']), 2),
                'percentage' => round(($item/$input_data['payable_amount']*100), 2),
                'account_project_id' => $bank_account_project_id,
                'GL_transaction_id' => $input_data['bank_GL_transaction_id'],
                'document_date' => $input_data['document_date']
            ]);

            $this->GL_transaction_project_split_model->add([
                'amount' => round(($input_data['bank_charge_amount']*$item/$input_data['payable_amount']), 2),
                'percentage' => round(($item/$input_data['payable_amount']*100), 2),
                'account_project_id' => $bank_charge_account_project_id,
                'GL_transaction_id' => $input_data['bank_charge_GL_transaction_id'],
                'document_date' => $input_data['document_date']
            ]);
        }

        $this->__set_flash_message('Manual Bank Statement is submitted successfully');
        return;

    }
}
