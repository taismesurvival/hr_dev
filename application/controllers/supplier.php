<?php
class Supplier extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';
    }

    public function index()
    {
        $data = $this->data;

        $sql = "SELECT master_account.father_id, master_account.id, master_account.code, master_account.name, master_account.type, MC.id AS MC_id FROM master_account 
        LEFT JOIN master_account AS MC ON MC.father_id = master_account.id
        WHERE master_account.deleted_at IS NULL AND master_account.status = 'active' GROUP BY master_account.id ORDER BY master_account.code";

        $data['accounts'] = $this->db->query($sql)->result_array();

        $accounts_array = [];
        foreach ($data['accounts'] AS $account) {
            if (empty($account['MC_id'])) {
                $accounts_array[] = [
                    'value' => $account['code'],
                    'label' => $account['code'] . ' - ' . $account['name']
                ];
            }
        }

        $data['accounts_json'] = json_encode($accounts_array);

        $input_data = $this->input_data['get'];

        $pagination = [
            'page' => (int) $input_data['p'] ? $input_data['p'] : 1,
            'limit' => (int) $input_data['limit'] ? $input_data['p'] : 20
        ];

        $pagination['offset'] = ($pagination['page'] - 1) * $pagination['limit'];

        $data['search'] = $pagination['search'] = $this->input_data['get'];

        $additional_where = '';
        if (!empty($data['search']['keyword'])) {
            $keyword = $data['search']['keyword'];

            $additional_where .= " AND ( name LIKE '%$keyword%' OR email LIKE '%$keyword%')";
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS id, name, default_due_date_number,
        mobile, email, address, default_tax_code, default_account_code FROM supplier 
        WHERE deleted_at IS NULL $additional_where LIMIT ".$pagination['offset'].", ".$pagination['limit'];

        $data['suppliers'] = $this->db->query($sql)->result_array();

        $pagination['total'] = $this->db->query('SELECT FOUND_ROWS() AS total;')->result_array()[0]['total'];

        $data['pagination'] = $pagination;

        return $this->template->loadView("supplier/index", $data, "admin");
    }

    public function edit()
    {
        if($this->input_data) {
            $this->load->model('supplier_model');

            $input_data = $this->input_data['post'];

            if (!empty($input_data['id'])) {
                $this->supplier_model->update($input_data['id'], $input_data);
                $this->__set_flash_message('Supplier is updated successfully');
                redirect('supplier');
                return;
            } else {
                $this->supplier_model->add($input_data);
                $this->__set_flash_message('Supplier is created successfully');
                redirect('supplier');
                return;
            }

        }
    }

    public function delete()
    {
        if($this->input_data) {
            $this->load->model('supplier_model');

            $input_data = $this->input_data['post'];

            $this->supplier_model->delete($input_data['id']);
            $this->__set_flash_message('Supplier is deleted successfully');
            redirect('supplier');
            return;

        }
    }
}
