<?php
class credit_note extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
    }

    public function index()
    {

        $data = $this->data;

        $data['document_date_default'] = $this->session->userdata("document_date_default");

        $sql = "SELECT id, name, code AS description
        FROM project
        WHERE deleted_at IS NULL AND status = 'active' AND type = 'outlet'";

        $data_projects = $this->db->query($sql)->result_array();

        if (count($data_projects) == 1) {
            $data['default_project_id'] = $data_projects[0]['id'];
            $data['default_project_name'] = $data_projects[0]['name'].' ('.$data_projects[0]['description'].')';
        }

        $sql = "SELECT id, account_code, name, number, type FROM bank_account 
        WHERE deleted_at IS NULL ORDER BY account_code";

        $data['bank_accounts'] = $this->db->query($sql)->result_array();

        $sql = "SELECT GST_setting FROM company
        WHERE id = 1";

        $GST_setting = $this->db->query($sql)->result_array()[0]['GST_setting'];

        $data['tax_account'] = $GST_setting == 'active' ? '21403' : '60621';

        $data['tax_account_name'] = $GST_setting == 'active' ? 'Input GST' : 'Input GST Paid on Purchases';

        $data['discount_account'] = '50154';

        $data['discount_account_name'] = 'Purchases - Discounts';

        $sql = "SELECT master_account.father_id, master_account.id, master_account.code, master_account.name, master_account.type, MC.id AS MC_id FROM master_account 
        LEFT JOIN master_account AS MC ON MC.father_id = master_account.id
        WHERE master_account.deleted_at IS NULL AND master_account.status = 'active' GROUP BY master_account.id ORDER BY master_account.code";

        $data['accounts'] = $this->db->query($sql)->result_array();

        $accounts_array = [];
        foreach ($data['accounts'] AS $account) {
            if (empty($account['MC_id'])) {
                $accounts_array[] = [
                    'value' => $account['code'],
                    'label' => $account['code'] . ' - ' . $account['name']
                ];
            }
        }

        $data['accounts_json'] = json_encode($accounts_array);

        $this->load->model('supplier_model');

        $this->load->model('company_model');
        $company_data = $this->company_model->find(1);
        $l12_database = 'l12com_'.strtolower($company_data['company_id']);
        $accounting_database = 'l12com_acc_'.strtolower($company_data['company_id']);

        $data['locked_financial_month'] = $company_data['locked_financial_month'];
        $data['locked_next_financial_month'] = date('Y-m', strtotime("+1 month", strtotime($company_data['locked_financial_month'])));

        $sql = "SELECT id, name, code AS description,
        type
        FROM project
        WHERE deleted_at IS NULL AND status = 'active'";

        $data['project_list'] = $this->db->query($sql)->result_array();

        $data['project_selector'] = '<option value="">Please Select</option>';

        foreach ($data['project_list'] AS $item) {
            $data['project_selector'] .= '<option value="'.$item['id'].'">'.$item['name'].' ('.$item['description'].')</option>';
        }

        $projects_array = [];
        foreach ($data['project_list'] AS $project) {
            $projects_array[] = [
                'value' => $project['name'].' ('.$project['description'].')',
                'label' => $project['name'].' ('.$project['description'].')',
                'id' => $project['id']
            ];
        }

        $data['projects_json'] = json_encode($projects_array);

        $input_data = $this->input_data['get'];

        $pagination = [
            'page' => (int) $input_data['p'] ? $input_data['p'] : 1,
            'limit' => (int) $input_data['limit'] ? $input_data['p'] : 20
        ];

        $pagination['offset'] = ($pagination['page'] - 1) * $pagination['limit'];

        $data['search'] = $pagination['search'] = $this->input_data['get'];

        $suppliers = $this->supplier_model->all(' order by name asc');
        $data['suppliers_list'] = $suppliers;

        $suppliers_array = [];
        foreach ($data['suppliers_list'] AS $supplier) {
            $suppliers_array[] = [
                'value' => $supplier['name'],
                'label' => $supplier['name'] . ' - ' . $supplier['address'],
                'id' => $supplier['id']
            ];
        }

        $data['suppliers_json'] = json_encode($suppliers_array);

        $data['supplier_selector'] = '<option selected value="">Please Select</option>';
        foreach ($suppliers AS $supplier) {
            $selected = !empty($data['search']['supplier_id']) && $data['search']['supplier_id'] == $supplier['id'] ? 'selected' : '';
            $data['supplier_selector'] .= '<option '.$selected.' value="'.$supplier['id'].'">'.$supplier['name'].'</option>';
        }

        $additional_where = '';

        if (!empty($data['search']['minimum_amount'])) {
            $minimum_amount = $data['search']['minimum_amount'];

            $additional_where .= " AND credit_note.amount >= $minimum_amount";
        }

        if (!empty($data['search']['maximum_amount'])) {
            $maximum_amount = $data['search']['maximum_amount'];

            $additional_where .= " AND credit_note.amount <= $maximum_amount";
        }

        if (!empty($data['search']['supplier_id'])) {
            $supplier_id = $data['search']['supplier_id'];

            $additional_where .= " AND credit_note.supplier_id = '$supplier_id'";
        }

        if (!empty($data['search']['keyword'])) {
            $keyword = $data['search']['keyword'];

            if (strpos($keyword, 'CN') !== FALSE) {

                $id = explode('CN', $keyword)[1];

                if (!empty($id)) {
                    $additional_where .= " AND credit_note.id = $id";
                } else {
                    $additional_where .= " AND (credit_note.description LIKE '%$keyword%')";
                }

            } else {
                $additional_where .= " AND (credit_note.description LIKE '%$keyword%')";
            }
        }

        if (!empty($data['search']['document_date_from'])) {
            $from = $data['search']['document_date_from'];

            $additional_where .= " AND (credit_note.document_date >= '$from')";
        }

        if (!empty($data['search']['document_date_to'])) {
            $to = $data['search']['document_date_to'];

            $additional_where .= " AND (credit_note.document_date <= '$to')";
        }

        if (!empty($data['search']['status'])) {
            $status = $data['search']['status'];

            $additional_where .= " AND (credit_note.status = '$status')";
        }

        if (!empty($data['search']['account_keyword'])) {
            $account_keyword = $data['search']['account_keyword'];

            $additional_where .= " AND (master_account.code LIKE '%$account_keyword%' OR master_account.name LIKE '%$account_keyword%')";
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS credit_note.id, credit_note.document_date, credit_note.tax_amount,
        credit_note.amount, credit_note.description, credit_note.status, supplier.name AS supplier_name, 
        credit_note.refference_no, credit_note.payable_GL_transaction_id, credit_note.supplier_id, credit_note.main_GL_transaction_id, credit_note.bank_reconciliation,
        credit_note.tax_GL_transaction_id, credit_note.tax_rate, credit_note.tax_type, credit_note.description, purchase_transaction.purchase_entry_id,
        purchase_entry.bank_reconciliation_id AS assigned_bank_reconciliation_id, bank_reconciliation.clear_date AS received_clear_date
        FROM credit_note 
        LEFT JOIN supplier on supplier.id = credit_note.supplier_id
        INNER JOIN GL_transaction_project_split ON credit_note.main_GL_transaction_id  = GL_transaction_project_split.GL_transaction_id
        INNER JOIN account_project ON account_project.id = GL_transaction_project_split.account_project_id
        INNER JOIN master_account ON account_project.account_id = master_account.id
        LEFT JOIN purchase_transaction ON credit_note.purchase_transaction_id = purchase_transaction.id
        LEFT JOIN purchase_entry ON purchase_transaction.purchase_entry_id = purchase_entry.id
        LEFT JOIN bank_reconciliation ON bank_reconciliation.id = credit_note.bank_reconciliation
        WHERE credit_note.deleted_at IS NULL $additional_where GROUP BY credit_note.id LIMIT ".$pagination['offset'].", ".$pagination['limit'];

        $data['purchase_entries'] = $this->db->query($sql)->result_array();

        $pagination['total'] = $this->db->query('SELECT FOUND_ROWS() AS total;')->result_array()[0]['total'];

        $data['pagination'] = $pagination;

        $sql = "SELECT SQL_CALC_FOUND_ROWS purchase_entry.id, purchase_entry.document_date, purchase_entry.type,
        purchase_entry.total_amount, purchase_entry.description, purchase_entry.status, supplier.name AS supplier_name, 
        purchase_entry.refference_no, purchase_entry.payment_GL_transaction_id, purchase_entry.supplier_id, purchase_entry.total_tax
        FROM purchase_entry 
        LEFT JOIN supplier on supplier.id = purchase_entry.supplier_id
        WHERE purchase_entry.deleted_at IS NULL AND purchase_entry.type = 'PURC' AND purchase_entry.separate_father_id IS NULL";

        $data['purchase_entries_selector'] = $this->db->query($sql)->result_array();

        return $this->template->loadView("credit_note/index", $data, "admin");
    }

    public function edit()
    {
        if($this->input_data) {
            $this->load->model('credit_note_model');
            $this->load->model('GL_transaction_model');
            $this->load->model('GL_transaction_project_split_model');

            $input_data = $this->input_data['post'];

            $this->session->set_userdata("document_date_default", $input_data['document_date']);

            if (!empty($input_data['id'])) {
                $credit_note_detail = $this->credit_note_model->find($input_data['id']);

                if (!empty($credit_note_detail['tax_GL_transaction_id'])
                    && $input_data['tax_amount'] > 0) {
                    $this->GL_transaction_model->update($credit_note_detail['tax_GL_transaction_id'],[
                        'type' => 'credit',
                        'amount' => $input_data['tax_amount'],
                        'description' => null
                    ]);
                    $input_data['tax_GL_transaction_id'] = $credit_note_detail['tax_GL_transaction_id'];
                }

                if (empty($credit_note_detail['tax_GL_transaction_id'])
                    && $input_data['tax_amount'] > 0) {
                    $input_data['tax_GL_transaction_id'] = $this->GL_transaction_model->add([
                        'type' => 'credit',
                        'amount' => $input_data['tax_amount'],
                        'transaction_type' => 'purchase',
                        'description' => null
                    ]);
                }

                if (!empty($credit_note_detail['tax_GL_transaction_id'])
                    && $input_data['tax_amount'] == '') {
                    $this->GL_transaction_model->realDelete($credit_note_detail['tax_GL_transaction_id']);
                    $transaction['tax_GL_transaction_id'] = null;
                }

                if (!empty($credit_note_detail['main_GL_transaction_id'])) {
                    $this->GL_transaction_model->update($credit_note_detail['main_GL_transaction_id'],[
                        'type' => 'credit',
                        'amount' => ($input_data['amount']),
                        'description' => null
                    ]);
                    $input_data['main_GL_transaction_id'] = $credit_note_detail['main_GL_transaction_id'];
                } else {
                    $input_data['main_GL_transaction_id'] = $this->GL_transaction_model->add([
                        'type' => 'credit',
                        'amount' => ($input_data['amount']),
                        'transaction_type' => 'purchase',
                        'description' => null
                    ]);
                }

                $this->credit_note_model->update($input_data['id'], $input_data);
                $this->__set_flash_message('The Credit Note is updated successfully');
            } else {
                $input_data['tax_GL_transaction_id'] = null;

                if ($input_data['tax_amount'] > 0) {
                    $input_data['tax_GL_transaction_id'] = $this->GL_transaction_model->add([
                        'type' => 'credit',
                        'amount' => $input_data['tax_amount'],
                        'transaction_type' => 'purchase',
                        'description' => null
                    ]);
                }

                $input_data['main_GL_transaction_id'] = $this->GL_transaction_model->add([
                    'type' => 'credit',
                    'amount' => ($input_data['amount']),
                    'transaction_type' => 'purchase',
                    'description' => null
                ]);
                $input_data['id'] = $this->credit_note_model->add($input_data);
                $this->__set_flash_message('The Credit Note is created successfully');
            }

            $trans_project_ids_array = [];
            $payable_project =[];
            foreach ($input_data['project_trans'] AS $project) {

                $payable_project[$project['project_id']] += $project['amount'];

                $sql = "SELECT account_project.id FROM account_project 
    LEFT JOIN master_account ON account_project.account_id = master_account.id
    WHERE account_project.deleted_at IS NULL AND account_project.project_id = ".$project['project_id']." 
    AND master_account.code = ".$input_data['trans_account_code'];

                $account_project_id = $this->db->query($sql)->result_array();
                $project ['account_project_id'] = $account_project_id[0]['id'];
                $project ['GL_transaction_id'] = $input_data['main_GL_transaction_id'];
                $project ['document_date'] = $input_data['document_date'];

                if (!empty($project['id'])) {
                    $this->GL_transaction_project_split_model->update($project['id'], $project);
                    $trans_project_ids_array[] = $project['id'];
                } else {
                    $project_id = $this->GL_transaction_project_split_model->add($project);
                    $trans_project_ids_array[] = $project_id;
                }
            }

            $split_ids = $this->GL_transaction_project_split_model->all(' AND GL_transaction_id = '.$input_data['main_GL_transaction_id']);

            foreach ($split_ids AS $split_id) {
                if (!in_array($split_id['id'], $trans_project_ids_array)) {
                    $this->GL_transaction_project_split_model->realDelete($split_id['id']);
                }
            }

            $tax_project_ids_array = [];
            if (!empty($input_data['project_tax'])) {
                foreach ($input_data['project_tax'] AS $project) {

                    $payable_project[$project['project_id']] += $project['amount'];

                    $sql = "SELECT account_project.id FROM account_project 
    LEFT JOIN master_account ON account_project.account_id = master_account.id
    WHERE account_project.deleted_at IS NULL AND account_project.project_id = " . $project['project_id'] . " 
    AND master_account.code = " . $input_data['tax_account_code'];

                    $account_project_id = $this->db->query($sql)->result_array();
                    $project ['account_project_id'] = $account_project_id[0]['id'];
                    $project ['GL_transaction_id'] = $input_data['tax_GL_transaction_id'];
                    $project ['document_date'] = $input_data['document_date'];

                    if (!empty($project['id'])) {
                        $this->GL_transaction_project_split_model->update($project['id'], $project);
                        $tax_project_ids_array[] = $project['id'];
                    } else {
                        $project_id = $this->GL_transaction_project_split_model->add($project);
                        $tax_project_ids_array[] = $project_id;
                    }
                }

                $split_ids = $this->GL_transaction_project_split_model->all(' AND GL_transaction_id = ' . $input_data['tax_GL_transaction_id']);

                foreach ($split_ids AS $split_id) {
                    if (!in_array($split_id['id'], $tax_project_ids_array)) {
                        $this->GL_transaction_project_split_model->realDelete($split_id['id']);
                    }
                }
            }

        }


        if (!empty($input_data['payable_GL_transaction_id'])) {
            $this->GL_transaction_model->realDelete($input_data['payable_GL_transaction_id']);

            $sql = "SELECT id FROM GL_transaction_project_split
        WHERE GL_transaction_id = ".$input_data['payable_GL_transaction_id']." AND deleted_at IS NULL";

            $GL_payment_project_split = $this->db->query($sql)->result_array();

            foreach ($GL_payment_project_split AS $project_split) {
                $this->GL_transaction_project_split_model->realDelete($project_split['id']);
            }
        }

        $new_payment_GL_transaction_id = $this->GL_transaction_model->add([
            'type' =>'debit',
            'amount' => $input_data['amount']+$input_data['tax_amount'],
            'transaction_type' => 'payment'
        ]);

        foreach ($payable_project AS $key => $item) {
            $sql = "SELECT account_project.id FROM account_project 
        LEFT JOIN master_account ON account_project.account_id = master_account.id
        WHERE account_project.deleted_at IS NULL AND account_project.project_id = ".$key." 
        AND master_account.code = 21200";

            $account_project_id = $this->db->query($sql)->result_array();

            $this->GL_transaction_project_split_model->add([
                'amount' => $item,
                'percentage' => round($item/($input_data['amount']+$input_data['tax_amount'])*100, 2),
                'account_project_id' => $account_project_id[0]['id'],
                'GL_transaction_id' => $new_payment_GL_transaction_id,
                'document_date' => $input_data['document_date']
            ]);
        }

        $this->credit_note_model->update($input_data['id'], [
            'payable_GL_transaction_id' => $new_payment_GL_transaction_id
        ]);

        echo json_encode([
            'success' => TRUE
        ]);
        return;
    }

    public function delete()
    {
        if($this->input_data) {
            $this->load->model('credit_note_model');
            $this->load->model('GL_transaction_model');
            $this->load->model('GL_transaction_project_split_model');

            $input_data = $this->input_data['post'];

            $credit_note = $this->credit_note_model->find($input_data['id']);

            $this->credit_note_model->delete($input_data['id']);

            $this->GL_transaction_model->realDelete($credit_note['main_GL_transaction_id']);
            $this->GL_transaction_model->realDelete($credit_note['tax_GL_transaction_id']);
            $this->GL_transaction_model->realDelete($credit_note['payable_GL_transaction_id']);

            $additional = $credit_note['main_GL_transaction_id'].', '.$credit_note['payable_GL_transaction_id'];

            if (!empty($credit_note['tax_GL_transaction_id'])) {
                $additional .= ', ' . $credit_note['tax_GL_transaction_id'];
            }

            if (!empty($credit_note['discount_GL_transaction_id'])) {
                $additional .= ', ' . $credit_note['discount_GL_transaction_id'];
            }

            $sql1 = "SELECT id FROM GL_transaction_project_split
    WHERE GL_transaction_id IN ($additional) AND deleted_at IS NULL";

            $GL_transactions_project_split = $this->db->query($sql1)->result_array();

            foreach ($GL_transactions_project_split AS $project_split) {
                $this->GL_transaction_project_split_model->realDelete($project_split['id']);
            }

            $this->__set_flash_message('The Purchase Entry is deleted successfully');
            redirect('credit_note');
            return;

        }
    }

    public function get_detail()
    {
        if($this->input_data) {

            $input_data = $this->input_data['post'];

            $sql1 = "SELECT credit_note.id, credit_note.supplier_id, credit_note.description, credit_note.document_date, credit_note.tax_type,
        IF(credit_note.tax_rate = '0', '', credit_note.tax_rate) AS tax_rate, credit_note.due_date, credit_note.refference_no, credit_note.status, credit_note.amount,
        credit_note.main_GL_transaction_id, credit_note.tax_GL_transaction_id, credit_note.payable_GL_transaction_id, supplier.name AS supplier_name
        FROM credit_note
        LEFT JOIN supplier ON credit_note.supplier_id = supplier.id
        WHERE credit_note.id = ".$input_data['id']." AND credit_note.deleted_at IS NULL";

            $credit_note = $data['credit_note'] = $this->db->query($sql1)->result_array()[0];

            if (!empty($credit_note['main_GL_transaction_id'])) {
                $trans_sql = "SELECT GL_transaction_project_split.id, GL_transaction_project_split.amount, GL_transaction_project_split.percentage,
                master_account.code AS account_code, master_account.name AS account_name, account_project.project_id,
                project.name AS project_name, project.code AS project_code
                FROM GL_transaction_project_split
                INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
                INNER JOIN master_account ON account_project.account_id = master_account.id
                INNER JOIN project ON account_project.project_id = project.id
                WHERE GL_transaction_project_split.GL_transaction_id = ".$credit_note['main_GL_transaction_id']." 
                AND GL_transaction_project_split.deleted_at IS NULL";

                $trans_project = $this->db->query($trans_sql)->result_array();
                $data['credit_note']['trans_project'] = $trans_project;
                $data['credit_note']['trans_account'] = $trans_project[0]['account_code'];
                $data['credit_note']['trans_account_name'] = $trans_project[0]['account_name'];
            }

            if (!empty($credit_note['tax_GL_transaction_id'])) {
                $tax_sql = "SELECT GL_transaction_project_split.id, GL_transaction_project_split.amount, GL_transaction_project_split.percentage,
                master_account.code AS account_code, master_account.name AS account_name, account_project.project_id
                FROM GL_transaction_project_split
                INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
                INNER JOIN master_account ON account_project.account_id = master_account.id
                WHERE GL_transaction_project_split.GL_transaction_id = ".$credit_note['tax_GL_transaction_id']." 
                AND GL_transaction_project_split.deleted_at IS NULL";

                $tax_project = $this->db->query($tax_sql)->result_array();
                $data['credit_note']['tax_project'] = $tax_project;
                $data['credit_note']['tax_account'] = $tax_project[0]['account_code'];
                $data['credit_note']['tax_account_name'] = $tax_project[0]['account_name'];
            }

            echo json_encode($data);
            die;
        }
    }

    public function check_edit()
    {
        if($this->input_data) {

            $input_data = $this->input_data['post'];

            foreach ($input_data['account_codes'] AS $code) {

                $sql = "SELECT master_account.name FROM master_account 
        LEFT JOIN master_account AS MC ON MC.father_id = master_account.id
        WHERE master_account.code = '$code' AND master_account.deleted_at IS NULL AND master_account.status = 'active' AND MC.id IS NULL GROUP BY master_account.id ORDER BY master_account.code";

                $account_name = $this->db->query($sql)->result_array();

                if (empty($account_name)) {
                    echo json_encode([
                        'error' => TRUE,
                        'message' => 'Account Code is not existed'
                    ]);
                    die;
                }
            }

            echo json_encode([
                'error' => FALSE
            ]);
            die;
        }
    }

    public function find_supplier()
    {
        $input_data = $this->input_data['post'];

        $this->load->model('supplier_model');
        $supplier = $this->supplier_model->find($input_data['id']);

        echo json_encode([
            'default_account_code' => $supplier['default_account_code'],
            'default_tax_code' => $supplier['default_tax_code'],
            'default_due_date_number' => $supplier['default_due_date_number']
        ]);
        die;
    }

    public function apply_purchase ()
    {
        if($this->input_data) {
            $input_data = $this->input_data['post'];

            $this->load->model('purchase_transaction_model');
            $this->load->model('credit_note_model');
            $this->load->model('purchase_entry_model');

            $input_data['discount_rate'] = 0;

            $purchase_transaction_id = $this->purchase_transaction_model->add($input_data);

            $this->purchase_entry_model->update($input_data['purchase_entry_id'], [
                'total_amount' => ($input_data['purchase_entry_total_amount'] + $input_data['amount']),
                'total_tax' => ($input_data['purchase_entry_total_tax'] + $input_data['amount'] - $input_data['unit_cost'])
            ]);

            $this->credit_note_model->update($input_data['credit_note_id'], [
                'status' => 'assigned',
                'purchase_transaction_id' => $purchase_transaction_id
            ]);
            $this->__set_flash_message('The Credit Note is assigned to Purchase Entry successfully');
            echo json_encode([
                'success' => TRUE
            ]);
            return;
        }
    }

    public function check_purchase ()
    {

        $input_data = $this->input_data['post'];
        $credit_note_id = $input_data['credit_note_id'];
        $purchase_entry_id = $input_data['purchase_entry_id'];

        $sql = "SELECT GL_transaction_project_split.amount, account_project.project_id FROM GL_transaction_project_split 
        INNER JOIN credit_note ON credit_note.payable_GL_transaction_id = GL_transaction_project_split.GL_transaction_id
        INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        WHERE credit_note.id = $credit_note_id";

        $project_splits = $this->db->query($sql)->result_array();

        foreach($project_splits AS $project) {

            $project_id = $project['project_id'];

            $sql = "SELECT GL_transaction_project_split.amount FROM GL_transaction_project_split 
        INNER JOIN purchase_entry ON purchase_entry.payment_GL_transaction_id = GL_transaction_project_split.GL_transaction_id
        INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        WHERE purchase_entry.id = $purchase_entry_id AND account_project.project_id = $project_id";

            $each_project = $this->db->query($sql)->result_array();
            $total_amount = $each_project[0]['amount'];

            $sql = "SELECT GL_transaction_project_split.amount FROM GL_transaction_project_split 
        INNER JOIN purchase_transaction ON purchase_transaction.payable_GL_transaction_id = GL_transaction_project_split.GL_transaction_id
        INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        WHERE purchase_transaction.purchase_entry_id = $purchase_entry_id AND account_project.project_id = $project_id";

            $credit_note_trans = $this->db->query($sql)->result_array();

            $total_credit_note_trans = 0;
            foreach ($credit_note_trans AS $tran) {
                $total_credit_note_trans += $tran['amount'];
            }


            if ($total_amount-$total_credit_note_trans < $project['amount']) {

                echo json_encode([
                    'error' => TRUE
                ]);
                die;
            }

        }

        echo json_encode([
            'error' => FALSE
        ]);
        die;
    }

    public function undo ()
    {
        if($this->input_data) {
            $input_data = $this->input_data['post'];

            $this->load->model('purchase_transaction_model');
            $this->load->model('credit_note_model');
            $this->load->model('purchase_entry_model');
            $this->load->model('bank_reconciliation_model');

            $credit_note = $this->credit_note_model->find($input_data['id']);
            $purchase_transaction_id = $credit_note['purchase_transaction_id'];

            $sql = "SELECT purchase_transaction.unit_cost, purchase_transaction.purchase_entry_id, purchase_entry.total_tax,
        purchase_entry.total_amount FROM purchase_transaction 
        INNER JOIN purchase_entry ON purchase_entry.id = purchase_transaction.purchase_entry_id
        WHERE purchase_transaction.id = $purchase_transaction_id";

            $purchase_detail = $this->db->query($sql)->result_array();

            $this->purchase_entry_model->update($purchase_detail[0]['purchase_entry_id'], [
                'total_amount' => ($purchase_detail[0]['total_amount'] + $credit_note['amount'] + $credit_note['tax_amount']),
                'total_tax' => ($purchase_detail[0]['total_tax'] + $credit_note['tax_amount'])
            ]);

            $this->purchase_transaction_model->realDelete($credit_note['purchase_transaction_id']);

            $this->credit_note_model->update($credit_note['id'], [
                'status' => 'pending',
                'purchase_transaction_id' => NULL
            ]);
            $this->__set_flash_message('The Credit Note undoes successfully');
            redirect('credit_note');
        }
    }

    public function bank_in()
    {
        if($this->input_data) {
            $input_data = $this->input_data['post'];
            $account_code = $input_data['account_code'];

            $this->load->model('GL_transaction_model');
            $this->load->model('GL_transaction_project_split_model');
            $this->load->model('credit_note_model');
            $this->load->model('bank_reconciliation_model');

            $received_payable_GL_transaction_id = $this->GL_transaction_model->add([
                'amount' => $input_data['amount'],
                'type' => 'credit',
                'transaction_type' => 'payment'
            ]);

            $received_bank_GL_transaction_id = $this->GL_transaction_model->add([
                'amount' => $input_data['amount'],
                'type' => 'debit',
                'transaction_type' => 'bank_reconciliation'
            ]);

            $sql = "SELECT amount, percentage, account_project_id FROM GL_transaction_project_split 
        WHERE GL_transaction_id = " . $input_data['payable_GL_transaction_id'];

            $payable_projects = $this->db->query($sql)->result_array();

            foreach ($payable_projects AS $project) {

                $project['GL_transaction_id'] = $received_payable_GL_transaction_id;
                $project['document_date'] = $input_data['pay_date'];

                $this->GL_transaction_project_split_model->add($project);

                $project['GL_transaction_id'] = $received_bank_GL_transaction_id;
                $project['document_date'] = $input_data['pay_date'];

                $sql = "SELECT acc2.id FROM account_project 
        INNER JOIN account_project AS acc2 ON acc2.project_id = account_project.project_id
        INNER JOIN master_account ON acc2.account_id = master_account.id
        WHERE master_account.code = '$account_code' AND account_project.id = " . $project['account_project_id'];

                $account_project_id = $this->db->query($sql)->result_array()[0]['id'];

                $project['account_project_id'] = $account_project_id;

                $this->GL_transaction_project_split_model->add($project);

            }

            $input_data['type'] = 'AR';
            $bank_reconciliation_id = $this->bank_reconciliation_model->add($input_data);

            $this->credit_note_model->update($input_data['credit_note_id'], [
                'status' => 'received',
                'received_bank_GL_transaction_id' => $received_bank_GL_transaction_id,
                'received_payable_GL_transaction_id' => $received_payable_GL_transaction_id,
                'bank_reconciliation' => $bank_reconciliation_id
            ]);

            $this->__set_flash_message('The Credit Note goes to bank account successfully');
            echo json_encode([
                'success' => TRUE
            ]);
            return;
        }
    }

    public function undo_bank_in()
    {
        if($this->input_data) {
            $input_data = $this->input_data['post'];

            $this->load->model('GL_transaction_model');
            $this->load->model('credit_note_model');
            $this->load->model('GL_transaction_project_split_model');
            $this->load->model('bank_reconciliation_model');

            $credit_note = $this->credit_note_model->find($input_data['id']);

            $this->bank_reconciliation_model->realDelete($credit_note['bank_reconciliation']);
            $this->GL_transaction_model->realDelete($credit_note['received_bank_GL_transaction_id']);
            $this->GL_transaction_model->realDelete($credit_note['received_payable_GL_transaction_id']);

            $additional = $credit_note['received_bank_GL_transaction_id'] . ', ' . $credit_note['received_payable_GL_transaction_id'];

            $sql1 = "SELECT id FROM GL_transaction_project_split
        WHERE GL_transaction_id IN ($additional) AND deleted_at IS NULL";

            $GL_transactions_project_split = $this->db->query($sql1)->result_array();

            foreach ($GL_transactions_project_split AS $project_split) {
                $this->GL_transaction_project_split_model->realDelete($project_split['id']);
            }

            $this->credit_note_model->update($input_data['id'], [
                'status' => 'pending',
                'received_bank_GL_transaction_id' => NULL,
                'received_payable_GL_transaction_id' => NULL,
                'bank_reconciliation' => NULL
            ]);

            $this->__set_flash_message('The Credit Note undoes successfully');
            redirect('credit_note');
        }
    }
}
