<?php
class balance_sheet_report extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';
    }

    /**
     * index page, for showing list of data
     */
    public function year_to_date_view(){

        $data = $this->data;

        $userdata = $this->session->userdata("user_data");

        $data['hide_bar'] = $userdata['role_id'] == 3 ? TRUE : FALSE;

        $data['check_active'] = $this->router->fetch_class().'/'.$this->router->fetch_method();

        $this->load->model('company_model');
        $company_data = $this->company_model->find(1);
        $l12_database = 'l12com_'.strtolower($company_data['company_id']);
        $accounting_database = 'l12com_acc_'.strtolower($company_data['company_id']);

        if($this->input_data) {
            $input_data = $this->input_data['post'];
            $data['company_name'] = $company_data['name'];

            $data['report_type'] = $input_data['report_type'] == 'summary' ? 'hidden' : '';

            $data['starting_month'] = date_format(date_create($input_data['starting_month']),"Y F");
            $data['ending_month'] = date_format(date_create($input_data['ending_month']),"Y F");

            foreach ($input_data['outlet_list'] AS $outlet) {
                $sql = "SELECT name, code
        FROM project
        WHERE id = $outlet";
                $outlet_name = $this->db->query($sql)->result_array()[0];
                $data['outlets'][] = $outlet_name['name'].' ('.$outlet_name['code'].')';
            }

            $starting_date = $input_data['starting_month'].'-01';
            $ending_date = $input_data['ending_month'].'-31';

            // Calculate PL
            $pl_this_year = 0;
            $pl_last_year = 0;

            $sql = "SELECT * 
        FROM financial_period where starting_date <= '$ending_date' and ending_date >= '$ending_date' and deleted_at is null";

            $FY_data = $this->db->query($sql)->result_array();

            if (!empty($FY_data)) {
                $pl_this_year = $this->get_PL($FY_data[0]['starting_date'], $ending_date, $input_data['outlet_list']);

                $last_day_of_last_year = date('Y-m-d', strtotime($FY_data[0]['starting_date'] . ' -1 day'));
                $sql = "SELECT * 
        FROM financial_period where ending_date = '$last_day_of_last_year' and deleted_at is null";

                $FY_last_data = $this->db->query($sql)->result_array();

                if (!empty($FY_last_data)) {
                    $pl_last_year = $this->get_PL($FY_last_data[0]['starting_date'], $FY_last_data[0]['ending_date'], $input_data['outlet_list']);
                }
            }

            $starting_date_1year_ago = date('Y-m-d', strtotime('-1 year', strtotime($starting_date)));
            $ending_date_1year_ago = date('Y-m-d', strtotime('-1 year', strtotime($ending_date)));

            // Calculate PL
            $pl_1year_ago_this_year = 0;
            $pl_1year_ago_last_year = 0;

            $sql = "SELECT * 
        FROM financial_period where starting_date <= '$ending_date_1year_ago' and ending_date >= '$ending_date_1year_ago' and deleted_at is null";

            $FY_data = $this->db->query($sql)->result_array();

            if (!empty($FY_data)) {
                $pl_1year_ago_this_year = $this->get_PL($FY_data[0]['starting_date'], $ending_date_1year_ago, $input_data['outlet_list']);

                $last_day_of_last_year_1year_ago = date('Y-m-d', strtotime($FY_data[0]['starting_date'] . ' -1 day'));
                $sql = "SELECT * 
        FROM financial_period where ending_date = '$last_day_of_last_year_1year_ago' and deleted_at is null";

                $FY_last_data = $this->db->query($sql)->result_array();

                if (!empty($FY_last_data)) {
                    $pl_1year_ago_last_year = $this->get_PL($FY_last_data[0]['starting_date'], $FY_last_data[0]['ending_date'], $input_data['outlet_list']);
                }
            }

            $data['starting_month_1year_ago'] = date_format(date_create($starting_date_1year_ago),"Y F");
            $data['ending_month_1year_ago'] = date_format(date_create($ending_date_1year_ago),"Y F");

            $this->load->model('master_account_model');
            $accounts = $this->master_account_model->all(" AND status = 'active' AND code < 40000 order by code asc");

            $data['accounts'] = [];
            foreach ($accounts AS $account) {

                $data['accounts'][$account['code']]['code'] = $account['code'];
                $data['accounts'][$account['code']]['type'] = $account['type'];
                $data['accounts'][$account['code']]['name'] = $account['name'];
                $data['accounts'][$account['code']]['level'] = $account['level'];
                $data['accounts'][$account['code']]['father_id'] = $account['father_id'];
                $data['accounts'][$account['code']]['id'] = $account['id'];

                $sql_children = "SELECT code FROM master_account WHERE status = 'active' AND father_id = ".$account['id']." order by code asc";

                $children = $this->db->query($sql_children)->result_array();

                if (!empty($children)) {
                    $data['accounts'][$account['code']]['children'] = $this->db->query($sql_children)->result_array();
                }

                $oulet_ids = join(',', $input_data['outlet_list']);

                $sql = "SELECT GL_transaction_project_split.amount, GL_transaction.type
        FROM GL_transaction_project_split 
        LEFT JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        LEFT JOIN GL_transaction ON GL_transaction_project_split.GL_transaction_id = GL_transaction.id
        WHERE account_project.account_id = ".$account['id']." AND account_project.project_id IN ($oulet_ids)";

                $sql_selected_period = $sql." AND GL_transaction_project_split.document_date <= '$ending_date'";
                $sql_year_to_date = $sql." AND GL_transaction_project_split.document_date <= '$ending_date_1year_ago'";

                $data['accounts'][$account['code']]['selected_period'] = $this->db->query($sql_selected_period)->result_array();
                if ($account['code'] == 30510) {
                    $data['accounts'][$account['code']]['selected_period'][] = [
                        'amount' => $pl_this_year,
                        'type' => 'credit'
                    ];
                }
                if ($account['code'] == 30520) {
                    $data['accounts'][$account['code']]['selected_period'][] = [
                        'amount' => $pl_last_year,
                        'type' => 'credit'
                    ];
                }
                $data['accounts'][$account['code']]['year_to_date'] = $this->db->query($sql_year_to_date)->result_array();
                if ($account['code'] == 30510) {
                    $data['accounts'][$account['code']]['year_to_date'][] = [
                        'amount' => $pl_1year_ago_this_year,
                        'type' => 'credit'
                    ];
                }
                if ($account['code'] == 30520) {
                    $data['accounts'][$account['code']]['year_to_date'][] = [
                        'amount' => $pl_1year_ago_last_year,
                        'type' => 'credit'
                    ];
                }

            }

            return $this->template->loadView("balance_sheet_report/year_to_date", $data);
        }

        $sql = "SELECT id, name, code AS description, type
        FROM project
        WHERE project.deleted_at IS NULL AND status = 'active'";

        $data['project_list'] = $this->db->query($sql)->result_array();

        return $this->template->loadView("balance_sheet_report/year_to_date_view", $data, "admin");
    }

    public function monthly_view(){

        $data = $this->data;

        $userdata = $this->session->userdata("user_data");

        $data['hide_bar'] = $userdata['role_id'] == 3 ? TRUE : FALSE;

        $data['check_active'] = $this->router->fetch_class().'/'.$this->router->fetch_method();

        $this->load->model('company_model');
        $company_data = $this->company_model->find(1);
        $l12_database = 'l12com_'.strtolower($company_data['company_id']);
        $accounting_database = 'l12com_acc_'.strtolower($company_data['company_id']);

        if($this->input_data) {
            $input_data = $this->input_data['post'];
            $data['company_name'] = $company_data['name'];

            $data['report_type'] = $input_data['report_type'] == 'summary' ? 'hidden' : '';

            foreach ($input_data['outlet_list'] AS $outlet) {
                $sql = "SELECT name,
        code AS description
        FROM project
        WHERE id = $outlet";
                $outlet_name = $this->db->query($sql)->result_array()[0];
                $data['outlets'][] = $outlet_name['name'].' ('.$outlet_name['description'].')';
            }

            $this->load->model('financial_period_model');
            $financial_period = $this->financial_period_model->find($input_data['financial_period_id']);
            $starting = $financial_period['starting_date'];
            $ending = $financial_period['ending_date'];

            $data['starting_month'] = date_format(date_create($starting),"Y F");
            $data['ending_month'] = date_format(date_create($ending),"Y F");

            $this->load->model('master_account_model');
            $accounts = $this->master_account_model->all(" AND status = 'active' AND code < 40000 order by code asc");

            $data['accounts'] = [];
            foreach ($accounts AS $account) {

                $data['accounts'][$account['code']]['code'] = $account['code'];
                $data['accounts'][$account['code']]['type'] = $account['type'];
                $data['accounts'][$account['code']]['name'] = $account['name'];
                $data['accounts'][$account['code']]['level'] = $account['level'];
                $data['accounts'][$account['code']]['father_id'] = $account['father_id'];
                $data['accounts'][$account['code']]['id'] = $account['id'];

                $sql_children = "SELECT code FROM master_account WHERE status = 'active' AND father_id = ".$account['id']." order by code asc";

                $children = $this->db->query($sql_children)->result_array();

                if (!empty($children)) {
                    $data['accounts'][$account['code']]['children'] = $this->db->query($sql_children)->result_array();
                }

                $oulet_ids = join(',', $input_data['outlet_list']);

                $sql = "SELECT GL_transaction_project_split.amount, GL_transaction.type, date_format(GL_transaction_project_split.document_date,'%c') AS document_month
        FROM GL_transaction_project_split 
        LEFT JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        LEFT JOIN GL_transaction ON GL_transaction_project_split.GL_transaction_id = GL_transaction.id
        WHERE account_project.account_id = ".$account['id']." AND account_project.project_id IN ($oulet_ids)
         AND GL_transaction_project_split.document_date >= '$starting'
         AND GL_transaction_project_split.document_date <= '$ending'";

                $data['accounts'][$account['code']]['monthly_report'] = $this->db->query($sql)->result_array();

            }


            $pl_last_year = 0;

            $last_day_of_last_year = date('Y-m-d', strtotime($starting . ' -1 day'));
            $sql = "SELECT * 
    FROM financial_period where ending_date = '$last_day_of_last_year' and deleted_at is null";

            $FY_last_data = $this->db->query($sql)->result_array();

            if (!empty($FY_last_data)) {
                $pl_last_year = $this->get_PL($FY_last_data[0]['starting_date'], $FY_last_data[0]['ending_date'], $input_data['outlet_list']);
            }

            for($i=0; strtotime($starting.' +'.($i+1).' months') < strtotime($ending.' +1 month'); $i++) {

                $data['accounts'][30520]['monthly_report'][] = [
                    'amount' => $pl_last_year,
                    'type' => 'credit',
                    'document_month' => (int) date('m', strtotime($starting.' +'.($i+1).' months -1 day'))
                ];

                $pl_this_month = $this->get_PL($starting, date('Y-m-d', strtotime($starting.' +'.($i+1).' months -1 day')), $input_data['outlet_list']);

                $data['accounts'][30510]['monthly_report'][] = [
                    'amount' => $pl_this_month,
                    'type' => 'credit',
                    'document_month' => (int) date('m', strtotime($starting.' +'.($i+1).' months -1 day'))
                ];
            }

            return $this->template->loadView("balance_sheet_report/monthly", $data);
        }

        $sql = "SELECT id, name, code AS description,
        type
        FROM project
        WHERE deleted_at IS NULL AND status = 'active'";

        $data['project_list'] = $this->db->query($sql)->result_array();
        $this->load->model('financial_period_model');
        $data['financial_period'] = $this->financial_period_model->all();

        return $this->template->loadView("balance_sheet_report/monthly_view", $data, "admin");
    }

    private function get_PL($starting_date, $ending_date, $outlet_list)
    {

        $this->load->model('master_account_model');
        $accounts = $this->master_account_model->all(" AND status = 'active' AND code >= 40000 order by code asc");

        $total_incomes = 0;
        $total_expenses = 0;

        foreach ($accounts AS $account) {

            $oulet_ids = join(',', $outlet_list);

            $sql = "SELECT GL_transaction_project_split.amount, GL_transaction.type
        FROM GL_transaction_project_split 
        LEFT JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        LEFT JOIN GL_transaction ON GL_transaction_project_split.GL_transaction_id = GL_transaction.id
        WHERE account_project.account_id = ".$account['id']." AND account_project.project_id IN ($oulet_ids)";

            $sql_selected_period = $sql." AND GL_transaction_project_split.document_date >= '$starting_date' AND GL_transaction_project_split.document_date <= '$ending_date'";

            $selected_period_data = $this->db->query($sql_selected_period)->result_array();

            $selected_period = 0;

            foreach ($selected_period_data AS $selected) {
                if ($account['type'] == 'income' OR $account['type'] == 'other_income') {

                    if ($selected['type'] == 'credit') {
                        $selected_period += $selected['amount'];
                    } else {
                        $selected_period -= $selected['amount'];
                    }
                } else {

                    if ($selected['type'] == 'debit') {
                        $selected_period += $selected['amount'];
                    } else {
                        $selected_period -= $selected['amount'];
                    }

                }
            }

            if (($account['code'] >= 40000 && $account['code'] < 50000) || $account['code'] > 70000) {
                $total_incomes += $selected_period;
            } else {
                $total_expenses += $selected_period;

            }

        }
        return ($total_incomes - $total_expenses);
    }
}
