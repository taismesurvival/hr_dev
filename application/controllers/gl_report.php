<?php
class gl_report extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';
    }

    /**
     * index page, for showing list of data
     */
    public function view(){

        $data = $this->data;

        $this->load->model('company_model');
        $this->load->model('master_account_model');
        $company_data = $this->company_model->find(1);
        $l12_database = 'l12com_'.strtolower($company_data['company_id']);
        $accounting_database = 'l12com_acc_'.strtolower($company_data['company_id']);

        if($this->input_data) {
            $input_data = $this->input_data['post'];
            $data['company_name'] = $company_data['name'];

            $data['starting_month'] = date_format(date_create($input_data['starting_month']),"Y F d");
            $data['ending_month'] = date_format(date_create($input_data['ending_month']),"Y F d");

            foreach ($input_data['outlet_list'] AS $outlet) {
                $sql = "SELECT name,
        code AS description
        FROM project
        WHERE id = $outlet";
                $outlet_name = $this->db->query($sql)->result_array()[0];
                $data['outlets'][] = $outlet_name['name'].' ('.$outlet_name['description'].')';
            }

            $starting_date = $input_data['starting_month'];
            $ending_date = $input_data['ending_month'];
            $account_code = $input_data['account_code'];

            $code_array = explode(", ", $account_code);

            $data['code_data'] = [];
            foreach ($code_array AS $item) {

                if (!empty($item)) {
                    $data['code_data'][$item] = [];

                    if (($item >= 10000 && $item < 20000)
                        || ($item >= 50000 && $item < 70000)
                    ) {
                        $data['code_data'][$item]['account_type'] = 'debit';
                    } else {
                        $data['code_data'][$item]['account_type'] = 'credit';
                    }

                    $selected_account = $this->master_account_model->all(" AND status = 'active' AND code = $item order by code asc");

                    $data['code_data'][$item]['account_infor'] = $selected_account[0];

                    $account_id = $selected_account[0]['id'];

                    $oulet_ids = join(',', $input_data['outlet_list']);

                    $sql = "SELECT GL_transaction_project_split.amount, GL_transaction.type, GL_transaction.transaction_type,
        GL_transaction.description, GL_transaction_project_split.document_date, GL_transaction_project_split.id
        FROM GL_transaction_project_split
        LEFT JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        LEFT JOIN GL_transaction ON GL_transaction_project_split.GL_transaction_id = GL_transaction.id
        WHERE account_project.account_id = " . $account_id . " AND account_project.project_id IN ($oulet_ids)";

                    $sql_starting_balance = $sql . " AND GL_transaction_project_split.document_date < '$starting_date'";
                    $sql_selected_period = $sql . " AND GL_transaction_project_split.document_date >= '$starting_date' AND GL_transaction_project_split.document_date <= '$ending_date' ORDER BY GL_transaction_project_split.document_date ASC";

                    $starting_trans = $this->db->query($sql_starting_balance)->result_array();
                    $data['code_data'][$item]['selected_period'] = $this->db->query($sql_selected_period)->result_array();

                    $data['code_data'][$item]['starting_balance'] = 0;
                    foreach ($starting_trans AS $tran) {
                        if ($data['code_data'][$item]['account_type'] == $tran['type']) {
                            $data['code_data'][$item]['starting_balance'] += $tran['amount'];
                        } else {
                            $data['code_data'][$item]['starting_balance'] -= $tran['amount'];
                        }
                    }
                }
            }
            return $this->template->loadView("gl_report/report", $data);
        }

        $sql = "SELECT master_account.father_id, master_account.id, master_account.code, master_account.name, master_account.type, MC.id AS MC_id FROM master_account 
        LEFT JOIN master_account AS MC ON MC.father_id = master_account.id
        WHERE master_account.deleted_at IS NULL AND master_account.status = 'active' GROUP BY master_account.id ORDER BY master_account.code";

        $data['accounts'] = $this->db->query($sql)->result_array();

        $accounts_array = [];
        foreach ($data['accounts'] AS $account) {
            if (empty($account['MC_id'])) {
                $accounts_array[] = [
                    'value' => $account['code'],
                    'label' => $account['code'] . ' - ' . $account['name']
                ];
            }
        }

        $data['accounts_json'] = json_encode($accounts_array);

        $sql = "SELECT id, name, code AS description,
        type
        FROM project
        WHERE deleted_at IS NULL AND status = 'active'";

        $data['project_list'] = $this->db->query($sql)->result_array();

        return $this->template->loadView("gl_report/view", $data, "admin");
    }

    public function check_account()
    {
        if($this->input_data) {

            $input_data = $this->input_data['post'];
            $sql = "SELECT master_account.id
        FROM master_account 
        WHERE master_account.deleted_at IS NULL AND master_account.status = 'active' AND master_account.code = ".$input_data['account_code'];

            $account = $this->db->query($sql)->result_array();

            if (empty($account)) {
                echo json_encode([
                    'error' => TRUE,
                    'message' => 'The Selected Account Code does not exist'
                ]);
                die;
            }

            echo json_encode([
                'error' => FALSE
            ]);
            die;
        }
    }
}
