<?php
class GL_entry_taxable extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
    }

    public function index()
    {

        $data = $this->data;

        $data['document_date_default'] = $this->session->userdata("document_date_default");

        $sql = "SELECT id, name, code AS description
        FROM project
        WHERE deleted_at IS NULL AND status = 'active' AND type = 'outlet'";

        $data_projects = $this->db->query($sql)->result_array();

        if (count($data_projects) == 1) {
            $data['default_project_id'] = $data_projects[0]['id'];
            $data['default_project_name'] = $data_projects[0]['name'].' ('.$data_projects[0]['description'].')';
        }

        $sql = "SELECT GST_setting FROM company
        WHERE id = 1";

        $GST_setting = $this->db->query($sql)->result_array()[0]['GST_setting'];

        $data['tax_account'] = $GST_setting == 'active' ? '21403' : '60621';

        $data['tax_account_name'] = $GST_setting == 'active' ? 'Input GST' : 'Input GST Paid on Purchases';

        $data['discount_account'] = '50154';

        $data['discount_account_name'] = 'Purchases - Discounts';

        $sql = "SELECT master_account.father_id, master_account.id, master_account.code, master_account.name, master_account.type, MC.id AS MC_id FROM master_account 
        LEFT JOIN master_account AS MC ON MC.father_id = master_account.id
        WHERE master_account.deleted_at IS NULL AND master_account.status = 'active' GROUP BY master_account.id ORDER BY master_account.code";

        $data['accounts'] = $this->db->query($sql)->result_array();

        $accounts_array = [];
        foreach ($data['accounts'] AS $account) {
            if (empty($account['MC_id'])) {
                $accounts_array[] = [
                    'value' => $account['code'],
                    'label' => $account['code'] . ' - ' . $account['name']
                ];
            }
        }

        $data['accounts_json'] = json_encode($accounts_array);

        $this->load->model('supplier_model');

        $this->load->model('company_model');
        $company_data = $this->company_model->find(1);
        $l12_database = 'l12com_'.strtolower($company_data['company_id']);
        $accounting_database = 'l12com_acc_'.strtolower($company_data['company_id']);

        $data['locked_financial_month'] = $company_data['locked_financial_month'];
        $data['locked_next_financial_month'] = date('Y-m', strtotime("+1 month", strtotime($company_data['locked_financial_month'])));

        $sql = "SELECT id, name, code AS description,
        type
        FROM project
        WHERE deleted_at IS NULL AND status = 'active'";

        $data['project_list'] = $this->db->query($sql)->result_array();

        $data['project_selector'] = '<option value="">Please Select</option>';

        foreach ($data['project_list'] AS $item) {
            $data['project_selector'] .= '<option value="'.$item['id'].'">'.$item['name'].' ('.$item['description'].')</option>';
        }

        $projects_array = [];
        foreach ($data['project_list'] AS $project) {
            $projects_array[] = [
                'value' => $project['name'].' ('.$project['description'].')',
                'label' => $project['name'].' ('.$project['description'].')',
                'id' => $project['id']
            ];
        }

        $data['projects_json'] = json_encode($projects_array);

        $input_data = $this->input_data['get'];

        $pagination = [
            'page' => (int) $input_data['p'] ? $input_data['p'] : 1,
            'limit' => (int) $input_data['limit'] ? $input_data['p'] : 20
        ];

        $pagination['offset'] = ($pagination['page'] - 1) * $pagination['limit'];

        $data['search'] = $pagination['search'] = $this->input_data['get'];

        $suppliers = $this->supplier_model->all(' order by name asc');
        $data['suppliers_list'] = $suppliers;

        $suppliers_array = [];
        foreach ($data['suppliers_list'] AS $supplier) {
            $suppliers_array[] = [
                'value' => $supplier['name'],
                'label' => $supplier['name'] . ' - ' . $supplier['address'],
                'id' => $supplier['id']
            ];
        }

        $data['suppliers_json'] = json_encode($suppliers_array);

        $data['supplier_selector'] = '<option selected value="">Please Select</option>';
        foreach ($suppliers AS $supplier) {
            $selected = !empty($data['search']['supplier_id']) && $data['search']['supplier_id'] == $supplier['id'] ? 'selected' : '';
            $data['supplier_selector'] .= '<option '.$selected.' value="'.$supplier['id'].'">'.$supplier['name'].'</option>';
        }

        $additional_where = '';

        if (!empty($data['search']['minimum_amount'])) {
            $minimum_amount = $data['search']['minimum_amount'];

            $additional_where .= " AND purchase_entry.total_amount >= $minimum_amount";
        }

        if (!empty($data['search']['maximum_amount'])) {
            $maximum_amount = $data['search']['maximum_amount'];

            $additional_where .= " AND purchase_entry.total_amount <= $maximum_amount";
        }

        if (!empty($data['search']['supplier_id'])) {
            $supplier_id = $data['search']['supplier_id'];

            $additional_where .= " AND purchase_entry.supplier_id = '$supplier_id'";
        }

        if (!empty($data['search']['keyword'])) {
            $keyword = $data['search']['keyword'];

            if(strpos($keyword, 'GT') !== FALSE) {

                $id = explode('GT', $keyword)[1];

                if (!empty($id)) {
                    $additional_where .= " AND purchase_entry.id = $id";
                } else {
                    $additional_where .= " AND (purchase_entry.description LIKE '%$keyword%')";
                }

            } else {
                $additional_where .= " AND (purchase_entry.description LIKE '%$keyword%')";
            }
        }

        if (!empty($data['search']['document_date_from'])) {
            $from = $data['search']['document_date_from'];

            $additional_where .= " AND (purchase_entry.document_date >= '$from')";
        }

        if (!empty($data['search']['document_date_to'])) {
            $to = $data['search']['document_date_to'];

            $additional_where .= " AND (purchase_entry.document_date <= '$to')";
        }

        if (!empty($data['search']['account_keyword'])) {
            $account_keyword = $data['search']['account_keyword'];

            $additional_where .= " AND (master_account.code LIKE '%$account_keyword%' OR master_account.name LIKE '%$account_keyword%')";
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS purchase_entry.id, purchase_entry.document_date, purchase_entry.type,
        purchase_entry.total_amount, purchase_entry.description, purchase_entry.status, supplier.name AS supplier_name, 
        purchase_entry.refference_no, purchase_entry.payment_GL_transaction_id
        FROM purchase_entry 
        LEFT JOIN supplier on supplier.id = purchase_entry.supplier_id
        LEFT JOIN purchase_transaction ON purchase_entry.id  = purchase_transaction.purchase_entry_id
        INNER JOIN GL_transaction_project_split ON purchase_transaction.GL_transaction_id  = GL_transaction_project_split.GL_transaction_id
        INNER JOIN account_project ON account_project.id = GL_transaction_project_split.account_project_id
        INNER JOIN master_account ON account_project.account_id = master_account.id
        WHERE purchase_entry.deleted_at IS NULL AND purchase_entry.type = 'PYMT' $additional_where GROUP BY purchase_entry.id LIMIT ".$pagination['offset'].", ".$pagination['limit'];

        $data['purchase_entries'] = $this->db->query($sql)->result_array();

        $pagination['total'] = $this->db->query('SELECT FOUND_ROWS() AS total;')->result_array()[0]['total'];

        $data['pagination'] = $pagination;

        $sql = "SELECT purchase_transaction.purchase_entry_id FROM purchase_transaction 
        LEFT JOIN GL_transaction AS payable_GL_transaction ON purchase_transaction.payable_GL_transaction_id = payable_GL_transaction.id
        LEFT JOIN bank_reconciliation AS payable_bank_reconciliation ON payable_GL_transaction.bank_reconciliation_id = payable_bank_reconciliation.id
        
        LEFT JOIN GL_transaction AS main_GL_transaction ON purchase_transaction.GL_transaction_id = main_GL_transaction.id
        LEFT JOIN bank_reconciliation AS main_bank_reconciliation ON main_GL_transaction.bank_reconciliation_id = main_bank_reconciliation.id
        
        WHERE purchase_transaction.deleted_at IS NULL AND purchase_transaction.payable_GL_transaction_id IS NOT NULL 
        AND purchase_transaction.unit_cost >= 0 AND (payable_bank_reconciliation.clear_date IS NOT NULL OR main_bank_reconciliation.clear_date IS NOT NULL) 
        group by purchase_transaction.purchase_entry_id";

        $done_taxable_GL = $this->db->query($sql)->result_array();

        $data['done_taxable_GL'] = [];

        foreach ($done_taxable_GL AS $item) {

            $data['done_taxable_GL'][] = $item['purchase_entry_id'];

        }

        return $this->template->loadView("GL_entry_taxable/index", $data, "admin");
    }

    public function edit()
    {
        if($this->input_data) {
            $this->load->model('purchase_entry_model');
            $this->load->model('bank_reconciliation_model');
            $this->load->model('GL_transaction_model');
            $this->load->model('purchase_transaction_model');
            $this->load->model('GL_transaction_project_split_model');

            $input_data = $this->input_data['post'];

            if (!empty($input_data['id'])) {
                $this->purchase_entry_model->update($input_data['id'], $input_data);
                $this->__set_flash_message('The GL Entry (taxable) is updated successfully');
            } else {
                $input_data['id'] = $this->purchase_entry_model->add($input_data);
                $this->__set_flash_message('The GL Entry (taxable) is created successfully');
            }

            $this->session->set_userdata("document_date_default", $input_data['document_date']);

            $trans_ids_array = [];
            $payable_project = [];

            foreach ($input_data['transactions'] AS $transaction) {

                $transaction['purchase_entry_id'] = $input_data['id'];

                if (!empty($transaction['id'])) {
                    $transaction_detail = $this->purchase_transaction_model->find($transaction['id']);

                    if (!empty($transaction_detail['discount_GL_transaction_id'])
                        && $transaction['discount_amount'] > 0) {
                        $this->GL_transaction_model->update($transaction_detail['discount_GL_transaction_id'],[
                            'type' => 'credit',
                            'amount' => $transaction['discount_amount'],
                            'description' => null
                        ]);
                        $transaction['discount_GL_transaction_id'] = $transaction_detail['discount_GL_transaction_id'];
                    }

                    if (empty($transaction_detail['discount_GL_transaction_id'])
                        && $transaction['discount_amount'] > 0) {
                        $transaction['discount_GL_transaction_id'] = $this->GL_transaction_model->add([
                            'type' => 'credit',
                            'amount' => $transaction['discount_amount'],
                            'transaction_type' => 'purchase',
                            'description' => null
                        ]);
                    }

                    if (!empty($transaction_detail['discount_GL_transaction_id'])
                        && $transaction['discount_amount'] == '') {
                        $this->GL_transaction_model->realDelete($transaction_detail['discount_GL_transaction_id']);
                        $transaction['discount_GL_transaction_id'] = null;
                    }

                    if (!empty($transaction_detail['tax_GL_transaction_id'])
                        && $transaction['tax_amount'] > 0) {
                        $this->GL_transaction_model->update($transaction_detail['tax_GL_transaction_id'],[
                            'type' => 'debit',
                            'amount' => $transaction['tax_amount'],
                            'description' => null
                        ]);
                        $transaction['tax_GL_transaction_id'] = $transaction_detail['tax_GL_transaction_id'];
                    }

                    if (empty($transaction_detail['tax_GL_transaction_id'])
                        && $transaction['tax_amount'] > 0) {
                        $transaction['tax_GL_transaction_id'] = $this->GL_transaction_model->add([
                            'type' => 'debit',
                            'amount' => $transaction['tax_amount'],
                            'transaction_type' => 'purchase',
                            'description' => null
                        ]);
                    }

                    if (!empty($transaction_detail['tax_GL_transaction_id'])
                        && $transaction['tax_amount'] == '') {
                        $this->GL_transaction_model->realDelete($transaction_detail['tax_GL_transaction_id']);
                        $transaction['tax_GL_transaction_id'] = null;
                    }

                    $bank_reconciliation_id_trans = $this->GL_transaction_model->find($transaction_detail['GL_transaction_id'])['bank_reconciliation_id'];

                    if (($transaction['trans_account_code'] >= 13031 && $transaction['trans_account_code'] <= 13048)
                        || ($transaction['trans_account_code'] >= 13011 && $transaction['trans_account_code'] <= 13029)) {

                        $bank_reconciliation_id_trans_update = $bank_reconciliation_id_trans;

                        $sql = "SELECT id FROM bank_account 
                WHERE deleted_at IS NULL AND account_code = ".$transaction['trans_account_code'];

                        $bank_account_id = $this->db->query($sql)->result_array()[0]['id'];

                        $bank_recons = [
                            'bank_account_id' => $bank_account_id,
                            'amount' => round($transaction['unit_cost']*$transaction['quantity'], 2),
                            'ref_no' => $transaction['description'],
                            'pay_date' => $input_data['document_date'],
                            'type' => 'AR'
                        ];

                        if (!empty($transaction_detail['GL_transaction_id']) && !empty($bank_reconciliation_id_trans_update)) {

                            $this->bank_reconciliation_model->update($bank_reconciliation_id_trans_update, $bank_recons);

                        } else {
                            $bank_reconciliation_id_trans_update = $this->bank_reconciliation_model->add($bank_recons);
                        }

                    } else {

                        if (!empty($bank_reconciliation_id_trans)) {
                            $this->bank_reconciliation_model->delete($bank_reconciliation_id_trans);
                            $bank_reconciliation_id_trans_update = NULL;
                        }

                    }

                    if (!empty($transaction_detail['GL_transaction_id'])) {
                        $this->GL_transaction_model->update($transaction_detail['GL_transaction_id'],[
                            'type' => 'debit',
                            'amount' => round($transaction['unit_cost']*$transaction['quantity'], 2),
                            'description' => null,
                            'bank_reconciliation_id' => $bank_reconciliation_id_trans_update
                        ]);
                        $transaction['GL_transaction_id'] = $transaction_detail['GL_transaction_id'];
                    } else {
                        $transaction['GL_transaction_id'] = $this->GL_transaction_model->add([
                            'type' => 'debit',
                            'amount' => round($transaction['unit_cost']*$transaction['quantity'], 2),
                            'transaction_type' => 'purchase',
                            'description' => null,
                            'bank_reconciliation_id' => $bank_reconciliation_id_trans_update
                        ]);
                    }

                    $bank_reconciliation_id_payable = $this->GL_transaction_model->find($transaction_detail['payable_GL_transaction_id'])['bank_reconciliation_id'];

                    if (($transaction['payable_account_code'] >= 13031 && $transaction['payable_account_code'] <= 13048)
                        || ($transaction['payable_account_code'] >= 13011 && $transaction['payable_account_code'] <= 13029)) {

                        $bank_reconciliation_id_payable_update = $bank_reconciliation_id_payable;

                        $sql = "SELECT id FROM bank_account 
                WHERE deleted_at IS NULL AND account_code = ".$transaction['payable_account_code'];

                        $bank_account_id = $this->db->query($sql)->result_array()[0]['id'];

                        $bank_recons = [
                            'bank_account_id' => $bank_account_id,
                            'amount' => round(($transaction['unit_cost']*$transaction['quantity'] - $transaction['discount_amount'] + $transaction['tax_amount']), 2),
                            'ref_no' => $transaction['description'],
                            'pay_date' => $input_data['document_date'],
                            'type' => 'AP'
                        ];

                        if (!empty($transaction_detail['payable_GL_transaction_id']) && !empty($bank_reconciliation_id_payable_update)) {

                            $this->bank_reconciliation_model->update($bank_reconciliation_id_payable_update, $bank_recons);

                        } else {
                            $bank_reconciliation_id_payable_update = $this->bank_reconciliation_model->add($bank_recons);
                        }

                    } else {

                        if (!empty($bank_reconciliation_id_payable)) {
                            $this->bank_reconciliation_model->delete($bank_reconciliation_id_payable);
                            $bank_reconciliation_id_payable_update = NULL;
                        }

                    }

                    if (!empty($transaction_detail['payable_GL_transaction_id'])) {
                        $this->GL_transaction_model->update($transaction_detail['payable_GL_transaction_id'],[
                            'type' => 'credit',
                            'amount' => round(($transaction['unit_cost']*$transaction['quantity'] - $transaction['discount_amount'] + $transaction['tax_amount']), 2),
                            'description' => null,
                            'bank_reconciliation_id' => $bank_reconciliation_id_payable_update
                        ]);
                        $transaction['payable_GL_transaction_id'] = $transaction_detail['payable_GL_transaction_id'];
                    } else {
                        $transaction['payable_GL_transaction_id'] = $this->GL_transaction_model->add([
                            'type' => 'credit',
                            'amount' => round(($transaction['unit_cost']*$transaction['quantity'] - $transaction['discount_amount'] + $transaction['tax_amount']), 2),
                            'transaction_type' => 'purchase',
                            'description' => null,
                            'bank_reconciliation_id' => $bank_reconciliation_id_payable_update
                        ]);
                    }

                    $this->purchase_transaction_model->update($transaction['id'], $transaction);
                    $trans_ids_array[] = $transaction['id'];
                } else {
                    $transaction['discount_GL_transaction_id'] = null;
                    $transaction['tax_GL_transaction_id'] = null;

                    if ($transaction['discount_amount'] > 0) {
                        $transaction['discount_GL_transaction_id'] = $this->GL_transaction_model->add([
                            'type' => 'credit',
                            'amount' => $transaction['discount_amount'],
                            'transaction_type' => 'purchase',
                            'description' => null
                        ]);
                    }

                    if ($transaction['tax_amount'] > 0) {
                        $transaction['tax_GL_transaction_id'] = $this->GL_transaction_model->add([
                            'type' => 'debit',
                            'amount' => $transaction['tax_amount'],
                            'transaction_type' => 'purchase',
                            'description' => null
                        ]);
                    }

                    $bank_reconciliation_id_trans_create = NULL;
                    if (($transaction['trans_account_code'] >= 13031 && $transaction['trans_account_code'] <= 13048)
                        || ($transaction['trans_account_code'] >= 13011 && $transaction['trans_account_code'] <= 13029)) {

                        $sql = "SELECT id FROM bank_account 
                WHERE deleted_at IS NULL AND account_code = ".$transaction['trans_account_code'];

                        $bank_account_id = $this->db->query($sql)->result_array()[0]['id'];

                        $bank_recons = [
                            'bank_account_id' => $bank_account_id,
                            'amount' => round($transaction['unit_cost']*$transaction['quantity'], 2),
                            'ref_no' => $transaction['description'],
                            'pay_date' => $input_data['document_date'],
                            'type' => 'AR'
                        ];

                        $bank_reconciliation_id_trans_create = $this->bank_reconciliation_model->add($bank_recons);

                    }

                    $transaction['GL_transaction_id'] = $this->GL_transaction_model->add([
                        'type' => 'debit',
                        'amount' => $transaction['unit_cost']*$transaction['quantity'],
                        'transaction_type' => 'purchase',
                        'description' => null,
                        'bank_reconciliation_id' => $bank_reconciliation_id_trans_create
                    ]);

                    $bank_reconciliation_id_payable_create = NULL;
                    if (($transaction['payable_account_code'] >= 13031 && $transaction['payable_account_code'] <= 13048)
                        || ($transaction['payable_account_code'] >= 13011 && $transaction['payable_account_code'] <= 13029)) {

                        $sql = "SELECT id FROM bank_account 
                WHERE deleted_at IS NULL AND account_code = ".$transaction['payable_account_code'];

                        $bank_account_id = $this->db->query($sql)->result_array()[0]['id'];

                        $bank_recons = [
                            'bank_account_id' => $bank_account_id,
                            'amount' => round(($transaction['unit_cost']*$transaction['quantity'] - $transaction['discount_amount'] + $transaction['tax_amount']), 2),
                            'ref_no' => $transaction['description'],
                            'pay_date' => $input_data['document_date'],
                            'type' => 'AP'
                        ];

                        $bank_reconciliation_id_payable_create = $this->bank_reconciliation_model->add($bank_recons);

                    }

                    $transaction['payable_GL_transaction_id'] = $this->GL_transaction_model->add([
                        'type' => 'credit',
                        'amount' => round(($transaction['unit_cost']*$transaction['quantity'] - $transaction['discount_amount'] + $transaction['tax_amount']), 2),
                        'transaction_type' => 'purchase',
                        'description' => null,
                        'bank_reconciliation_id' => $bank_reconciliation_id_payable_create
                    ]);

                    $transaction_id = $this->purchase_transaction_model->add($transaction);
                    $trans_ids_array[] = $transaction_id;
                    $transaction['id'] = $transaction_id;
                }

                $trans_project_ids_array = [];
                foreach ($transaction['project_trans'] AS $project) {

                    $payable_project[$project['project_id']] += $project['amount'];

                    $sql = "SELECT account_project.id FROM account_project 
        LEFT JOIN master_account ON account_project.account_id = master_account.id
        WHERE account_project.deleted_at IS NULL AND account_project.project_id = ".$project['project_id']." 
        AND master_account.code = ".$transaction['trans_account_code'];

                    $account_project_id = $this->db->query($sql)->result_array();
                    $project ['account_project_id'] = $account_project_id[0]['id'];
                    $project ['GL_transaction_id'] = $transaction['GL_transaction_id'];
                    $project ['document_date'] = $input_data['document_date'];

                    if (!empty($project['id'])) {
                        $this->GL_transaction_project_split_model->update($project['id'], $project);
                        $trans_project_ids_array[] = $project['id'];
                    } else {
                        $project_id = $this->GL_transaction_project_split_model->add($project);
                        $trans_project_ids_array[] = $project_id;
                    }
                }

                $split_ids = $this->GL_transaction_project_split_model->all(' AND GL_transaction_id = '.$transaction['GL_transaction_id']);

                foreach ($split_ids AS $split_id) {
                    if (!in_array($split_id['id'], $trans_project_ids_array)) {
                        $this->GL_transaction_project_split_model->realDelete($split_id['id']);
                    }
                }

                $payable_project_ids_array = [];
                foreach ($transaction['project_payable'] AS $project) {

                    $payable_project[$project['project_id']] += $project['amount'];

                    $sql = "SELECT account_project.id FROM account_project 
        LEFT JOIN master_account ON account_project.account_id = master_account.id
        WHERE account_project.deleted_at IS NULL AND account_project.project_id = ".$project['project_id']." 
        AND master_account.code = ".$transaction['payable_account_code'];

                    $account_project_id = $this->db->query($sql)->result_array();
                    $project ['account_project_id'] = $account_project_id[0]['id'];
                    $project ['GL_transaction_id'] = $transaction['payable_GL_transaction_id'];
                    $project ['document_date'] = $input_data['document_date'];

                    if (!empty($project['id'])) {
                        $this->GL_transaction_project_split_model->update($project['id'], $project);
                        $payable_project_ids_array[] = $project['id'];
                    } else {
                        $project_id = $this->GL_transaction_project_split_model->add($project);
                        $payable_project_ids_array[] = $project_id;
                    }
                }

                $split_ids = $this->GL_transaction_project_split_model->all(' AND GL_transaction_id = '.$transaction['payable_GL_transaction_id']);

                foreach ($split_ids AS $split_id) {
                    if (!in_array($split_id['id'], $payable_project_ids_array)) {
                        $this->GL_transaction_project_split_model->realDelete($split_id['id']);
                    }
                }

                $discount_project_ids_array = [];
                if (!empty($transaction['project_discount'])) {
                    foreach ($transaction['project_discount'] AS $project) {

                        $payable_project[$project['project_id']] -= $project['amount'];

                        $sql = "SELECT account_project.id FROM account_project 
        LEFT JOIN master_account ON account_project.account_id = master_account.id
        WHERE account_project.deleted_at IS NULL AND account_project.project_id = " . $project['project_id'] . " 
        AND master_account.code = " . $transaction['discount_account_code'];

                        $account_project_id = $this->db->query($sql)->result_array();
                        $project ['account_project_id'] = $account_project_id[0]['id'];
                        $project ['GL_transaction_id'] = $transaction['discount_GL_transaction_id'];
                        $project ['document_date'] = $input_data['document_date'];

                        if (!empty($project['id'])) {
                            $this->GL_transaction_project_split_model->update($project['id'], $project);
                            $discount_project_ids_array[] = $project['id'];
                        } else {
                            $project_id = $this->GL_transaction_project_split_model->add($project);
                            $discount_project_ids_array[] = $project_id;
                        }
                    }

                    $split_ids = $this->GL_transaction_project_split_model->all(' AND GL_transaction_id = ' . $transaction['discount_GL_transaction_id']);

                    foreach ($split_ids AS $split_id) {
                        if (!in_array($split_id['id'], $discount_project_ids_array)) {
                            $this->GL_transaction_project_split_model->realDelete($split_id['id']);
                        }
                    }
                }

                $tax_project_ids_array = [];
                if (!empty($transaction['project_tax'])) {
                    foreach ($transaction['project_tax'] AS $project) {

                        $payable_project[$project['project_id']] += $project['amount'];

                        $sql = "SELECT account_project.id FROM account_project 
        LEFT JOIN master_account ON account_project.account_id = master_account.id
        WHERE account_project.deleted_at IS NULL AND account_project.project_id = " . $project['project_id'] . " 
        AND master_account.code = " . $transaction['tax_account_code'];

                        $account_project_id = $this->db->query($sql)->result_array();
                        $project ['account_project_id'] = $account_project_id[0]['id'];
                        $project ['GL_transaction_id'] = $transaction['tax_GL_transaction_id'];
                        $project ['document_date'] = $input_data['document_date'];

                        if (!empty($project['id'])) {
                            $this->GL_transaction_project_split_model->update($project['id'], $project);
                            $tax_project_ids_array[] = $project['id'];
                        } else {
                            $project_id = $this->GL_transaction_project_split_model->add($project);
                            $tax_project_ids_array[] = $project_id;
                        }
                    }

                    $split_ids = $this->GL_transaction_project_split_model->all(' AND GL_transaction_id = ' . $transaction['tax_GL_transaction_id']);

                    foreach ($split_ids AS $split_id) {
                        if (!in_array($split_id['id'], $tax_project_ids_array)) {
                            $this->GL_transaction_project_split_model->realDelete($split_id['id']);
                        }
                    }
                }

            }

            $transaction_ids = $this->purchase_transaction_model->all(' AND purchase_entry_id = '.$input_data['id']);

            foreach ($transaction_ids AS $trans_id) {
                if (!in_array($trans_id['id'], $trans_ids_array)) {
                    $this->purchase_transaction_model->realDelete($trans_id['id']);
                    $this->GL_transaction_model->realDelete($trans_id['GL_transaction_id']);

                    $sql_string = $trans_id['GL_transaction_id'];

                    if (!empty($trans_id['tax_GL_transaction_id'])) {
                        $this->GL_transaction_model->realDelete($trans_id['tax_GL_transaction_id']);
                        $sql_string .= ", ".$trans_id['tax_GL_transaction_id'];
                    }
                    if (!empty($trans_id['discount_GL_transaction_id'])) {
                        $this->GL_transaction_model->realDelete($trans_id['discount_GL_transaction_id']);
                        $sql_string .= ", ".$trans_id['discount_GL_transaction_id'];
                    }

                    $sql1 = "SELECT id FROM GL_transaction_project_split
        WHERE GL_transaction_id IN (".$sql_string.") AND deleted_at IS NULL";

                    $GL_transactions_project_split = $this->db->query($sql1)->result_array();

                    foreach ($GL_transactions_project_split AS $project_split) {
                        $this->GL_transaction_project_split_model->realDelete($project_split['id']);
                    }
                }
            }

            echo json_encode([
                'success' => TRUE
            ]);

            return;
        }
    }

    public function delete()
    {
        if($this->input_data) {
            $this->load->model('purchase_entry_model');
            $this->load->model('bank_reconciliation_model');
            $this->load->model('GL_transaction_model');
            $this->load->model('purchase_transaction_model');
            $this->load->model('GL_transaction_project_split_model');

            $input_data = $this->input_data['post'];

            $this->purchase_entry_model->delete($input_data['id']);

            $sql = "SELECT id, GL_transaction_id, tax_GL_transaction_id, discount_GL_transaction_id, payable_GL_transaction_id
        FROM purchase_transaction
        WHERE purchase_entry_id = ".$input_data['id']." AND deleted_at IS NULL";

            $purchase_transactions_id = $this->db->query($sql)->result_array();

            foreach ($purchase_transactions_id AS $transaction) {
                $this->purchase_transaction_model->realDelete($transaction['id']);

                $bank_reconciliation_id_trans = $this->GL_transaction_model->find($transaction['GL_transaction_id'])['bank_reconciliation_id'];

                if (!empty($bank_reconciliation_id_trans)) {

                    $this->bank_reconciliation_model->delete($bank_reconciliation_id_trans);

                }

                $bank_reconciliation_id_payable = $this->GL_transaction_model->find($transaction['payable_GL_transaction_id'])['bank_reconciliation_id'];

                if (!empty($bank_reconciliation_id_payable)) {

                    $this->bank_reconciliation_model->delete($bank_reconciliation_id_payable);

                }

                $this->GL_transaction_model->realDelete($transaction['GL_transaction_id']);
                $this->GL_transaction_model->realDelete($transaction['tax_GL_transaction_id']);
                $this->GL_transaction_model->realDelete($transaction['discount_GL_transaction_id']);
                $this->GL_transaction_model->realDelete($transaction['payable_GL_transaction_id']);

                $additional = $transaction['GL_transaction_id'].', '.$transaction['payable_GL_transaction_id'];

                if (!empty($transaction['tax_GL_transaction_id'])) {
                    $additional .= ', ' . $transaction['tax_GL_transaction_id'];
                }

                if (!empty($transaction['discount_GL_transaction_id'])) {
                    $additional .= ', ' . $transaction['discount_GL_transaction_id'];
                }

                $sql1 = "SELECT id FROM GL_transaction_project_split
        WHERE GL_transaction_id IN ($additional) AND deleted_at IS NULL";

                $GL_transactions_project_split = $this->db->query($sql1)->result_array();

                foreach ($GL_transactions_project_split AS $project_split) {
                    $this->GL_transaction_project_split_model->realDelete($project_split['id']);
                }
            }

            $this->__set_flash_message('The GL Entry (taxable) is deleted successfully');
            redirect('GL_entry_taxable');
            return;

        }
    }

    public function get_detail()
    {
        if($this->input_data) {

            $input_data = $this->input_data['post'];

            $sql1 = "SELECT purchase_transaction.id, purchase_transaction.quantity, purchase_transaction.description, purchase_transaction.unit_cost, purchase_transaction.tax_type, purchase_transaction.payable_GL_transaction_id,
        IF(purchase_transaction.tax_rate = '0', '', purchase_transaction.tax_rate) AS tax_rate, IF(purchase_transaction.discount_rate = '0', '', purchase_transaction.discount_rate) AS discount_rate, purchase_transaction.description,
        purchase_transaction.GL_transaction_id, purchase_transaction.tax_GL_transaction_id, purchase_transaction.discount_GL_transaction_id
        FROM purchase_transaction
        WHERE purchase_transaction.purchase_entry_id = ".$input_data['id']." AND purchase_transaction.deleted_at IS NULL";

            $data['purchase_transactions'] = $this->db->query($sql1)->result_array();

            $view_mode = false;
            foreach ($data['purchase_transactions'] AS $key => $value) {
                if (!empty($value['GL_transaction_id'])) {

                    $check_clear_sql = "SELECT bank_reconciliation.clear_date FROM GL_transaction 
                    LEFT JOIN bank_reconciliation ON GL_transaction.bank_reconciliation_id = bank_reconciliation.id
                    WHERE GL_transaction.id = ".$value['GL_transaction_id'];
                    $check_clear = $this->db->query($check_clear_sql)->result_array();

                    if (!empty($check_clear[0]['clear_date'])) {
                        $view_mode = true;
                    }

                    $trans_sql = "SELECT GL_transaction_project_split.id, GL_transaction_project_split.amount, GL_transaction_project_split.percentage,
                    master_account.code AS account_code, master_account.name AS account_name, account_project.project_id,
                    project.name AS project_name, project.code AS project_code
                    FROM GL_transaction_project_split
                    INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
                    INNER JOIN master_account ON account_project.account_id = master_account.id
                    INNER JOIN project ON account_project.project_id = project.id
                    WHERE GL_transaction_project_split.GL_transaction_id = ".$value['GL_transaction_id']." 
                    AND GL_transaction_project_split.deleted_at IS NULL";

                    $trans_project = $this->db->query($trans_sql)->result_array();
                    $data['purchase_transactions'][$key]['trans_project'] = $trans_project;
                    $data['purchase_transactions'][$key]['trans_account'] = $trans_project[0]['account_code'];
                    $data['purchase_transactions'][$key]['trans_account_name'] = $trans_project[0]['account_name'];
                }

                if (!empty($value['payable_GL_transaction_id'])) {

                    $check_clear_sql = "SELECT bank_reconciliation.clear_date FROM GL_transaction 
                    LEFT JOIN bank_reconciliation ON GL_transaction.bank_reconciliation_id = bank_reconciliation.id
                    WHERE GL_transaction.id = ".$value['payable_GL_transaction_id'];
                    $check_clear = $this->db->query($check_clear_sql)->result_array();

                    if (!empty($check_clear[0]['clear_date'])) {
                        $view_mode = true;
                    }

                    $payable_sql = "SELECT GL_transaction_project_split.id, GL_transaction_project_split.amount, GL_transaction_project_split.percentage,
                    master_account.code AS account_code, master_account.name AS account_name, account_project.project_id
                    FROM GL_transaction_project_split
                    INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
                    INNER JOIN master_account ON account_project.account_id = master_account.id
                    WHERE GL_transaction_project_split.GL_transaction_id = ".$value['payable_GL_transaction_id']." 
                    AND GL_transaction_project_split.deleted_at IS NULL";

                    $payable_project = $this->db->query($payable_sql)->result_array();
                    $data['purchase_transactions'][$key]['payable_project'] = $payable_project;
                    $data['purchase_transactions'][$key]['payable_account'] = $payable_project[0]['account_code'];
                    $data['purchase_transactions'][$key]['payable_account_name'] = $payable_project[0]['account_name'];
                }

                if (!empty($value['tax_GL_transaction_id'])) {
                    $tax_sql = "SELECT GL_transaction_project_split.id, GL_transaction_project_split.amount, GL_transaction_project_split.percentage,
                    master_account.code AS account_code, master_account.name AS account_name, account_project.project_id
                    FROM GL_transaction_project_split
                    INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
                    INNER JOIN master_account ON account_project.account_id = master_account.id
                    WHERE GL_transaction_project_split.GL_transaction_id = ".$value['tax_GL_transaction_id']." 
                    AND GL_transaction_project_split.deleted_at IS NULL";

                    $tax_project = $this->db->query($tax_sql)->result_array();
                    $data['purchase_transactions'][$key]['tax_project'] = $tax_project;
                    $data['purchase_transactions'][$key]['tax_account'] = $tax_project[0]['account_code'];
                    $data['purchase_transactions'][$key]['tax_account_name'] = $tax_project[0]['account_name'];
                }

                if (!empty($value['discount_GL_transaction_id'])) {
                    $discount_sql = "SELECT GL_transaction_project_split.id, GL_transaction_project_split.amount, GL_transaction_project_split.percentage,
                    master_account.code AS account_code, master_account.name AS account_name, account_project.project_id
                    FROM GL_transaction_project_split
                    INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
                    INNER JOIN master_account ON account_project.account_id = master_account.id
                    WHERE GL_transaction_project_split.GL_transaction_id = ".$value['discount_GL_transaction_id']." 
                    AND GL_transaction_project_split.deleted_at IS NULL";

                    $discount_project = $this->db->query($discount_sql)->result_array();
                    $data['purchase_transactions'][$key]['discount_project'] = $discount_project;
                    $data['purchase_transactions'][$key]['discount_account'] = $discount_project[0]['account_code'];
                    $data['purchase_transactions'][$key]['discount_account_name'] = $discount_project[0]['account_name'];
                }
            }

            $sql2 = "SELECT purchase_entry.type, purchase_entry.refference_no, DATE_FORMAT(purchase_entry.document_date,'%Y-%m-%d') AS document_date,
        purchase_entry.attached_file, purchase_entry.description, purchase_entry.payment_GL_transaction_id,
        purchase_entry.supplier_id, supplier.name AS supplier_name, DATE_FORMAT(purchase_entry.due_date,'%Y-%m-%d') AS due_date
        FROM purchase_entry
        LEFT JOIN supplier ON purchase_entry.supplier_id = supplier.id
        WHERE purchase_entry.id = ".$input_data['id']." AND purchase_entry.deleted_at IS NULL";

            $data['purchase_entry'] = $this->db->query($sql2)->result_array()[0];
            $data['purchase_entry']['view_mode'] = $view_mode;

            echo json_encode($data);
            die;
        }
    }

    public function check_edit()
    {
        if($this->input_data) {

            $input_data = $this->input_data['post'];

            foreach ($input_data['account_codes'] AS $key => $code) {

                $sql = "SELECT master_account.name FROM master_account 
        LEFT JOIN master_account AS MC ON MC.father_id = master_account.id
        WHERE master_account.code = '$code' AND master_account.deleted_at IS NULL AND master_account.status = 'active' AND MC.id IS NULL GROUP BY master_account.id ORDER BY master_account.code";

                $account_name = $this->db->query($sql)->result_array();

                if (empty($account_name)) {
                    echo json_encode([
                        'error' => TRUE,
                        'label' => $key,
                        'message' => 'Account Code is not existed'
                    ]);
                    die;
                }
            }

            echo json_encode([
                'error' => FALSE
            ]);
            die;
        }
    }

    public function find_supplier()
    {
        $input_data = $this->input_data['post'];

        $this->load->model('supplier_model');
        $supplier = $this->supplier_model->find($input_data['id']);

        echo json_encode([
            'default_account_code' => $supplier['default_account_code'],
            'default_tax_code' => $supplier['default_tax_code'],
            'default_due_date_number' => $supplier['default_due_date_number']
        ]);
        die;
    }
}
