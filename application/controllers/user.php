<?php
class User extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';
    }

    public function index()
    {
        $data = $this->data;

        $this->load->model('role_model');
        $role_list = $this->role_model->all(' order by name asc');

        $data['role_list'] = [
            '' => 'Please Select'
        ];
        foreach ($role_list AS $role) {
            $data['role_list'][$role['id']] = $role['name'];
        }

        $this->load->model('project_model');
        $project_list = $this->project_model->all(" AND type = 'outlet' AND status = 'active'");

        $data['project_list'] = [];
        foreach ($project_list AS $item) {
            $data['project_list'][$item['id']] = $item['name'].' ('.$item['code'].')';
        }

        $input_data = $this->input_data['get'];

        $pagination = [
            'page' => (int) $input_data['p'] ? $input_data['p'] : 1,
            'limit' => (int) $input_data['limit'] ? $input_data['p'] : 20
        ];

        $pagination['offset'] = ($pagination['page'] - 1) * $pagination['limit'];

        $data['search'] = $pagination['search'] = $this->input_data['get'];

        $additional_where = '';
        if (!empty($data['search']['username'])) {
            $username_keyword = $data['search']['username'];

            $additional_where .= " AND username LIKE '%$username_keyword%'";
        }

        if (!empty($data['search']['is_active'])) {
            $is_active = $data['search']['is_active'];

            $additional_where .= " AND is_active = '$is_active'";
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS user.id, user.username, user.name, user.is_active, user.outlet_id,
        user.mobile, user.email, user.role_id, role.name AS role_name FROM user 
        LEFT JOIN role ON role.id = user.role_id WHERE user.deleted_at IS NULL $additional_where LIMIT ".$pagination['offset'].", ".$pagination['limit'];

        $data['users'] = $this->db->query($sql)->result_array();

        $pagination['total'] = $this->db->query('SELECT FOUND_ROWS() AS total;')->result_array()[0]['total'];

        $data['pagination'] = $pagination;

        return $this->template->loadView("user/index", $data, "admin");
    }

    public function edit()
    {
        if($this->input_data) {
            $this->load->model('user_model');

            $input_data = $this->input_data['post'];

            if (!empty($input_data['outlet_id'])) {
                $input_data['outlet_id'] = json_encode($input_data['outlet_id']);
            }

            if (!empty($input_data['id'])) {
                $this->user_model->update($input_data['id'], $input_data);
                $this->__set_flash_message('User is updated successfully');
                redirect('user');
                return;
            } else {
                $input_data['password'] = md5(strtolower($input_data['username']).'admin');
                $this->user_model->add($input_data);
                $this->__set_flash_message('User is created successfully');
                redirect('user');
                return;
            }

        }
    }

    public function delete()
    {
        if($this->input_data) {
            $this->load->model('user_model');

            $input_data = $this->input_data['post'];

            $this->user_model->delete($input_data['id']);
            $this->__set_flash_message('User is deleted successfully');
            redirect('user');
            return;

        }
    }

    public function check_edit()
    {
        if($this->input_data) {

            $input_data = $this->input_data['post'];
            $this->load->model('user_model');

            $user = $this->user_model->find($input_data['username'], 'username');

            if (!empty($user) && $input_data['id'] != $user['id'] && empty($user['deleted_at'])) {
                echo json_encode([
                    'error' => TRUE,
                    'message' => 'User Name has been used'
                ]);
                die;
            }

            echo json_encode([
                'error' => FALSE
            ]);
            die;
        }
    }

    public function change_password()
    {

        $user_data = $this->session->userdata("user_data");
        $data['user_data'] = $user_data;
        $crud_data = $this->input->post();

        $error = array();
        if($crud_data != NULL)
        {
            if($crud_data['code'] != $user_data['username'])
            {
                $error['code'] = 'Invalid username';
            }

            if(strlen($crud_data['password']) <= 4)
            {
                $error['password'] = 'Password is too short';
            }
            if($crud_data['password'] == '' || $crud_data['confirm_passsord'] == '')
            {
                $error['password'] = 'Invalid password';
                $error['confirm_password'] = 'Please confirm valid password';
            }
            if($crud_data['password'] != $crud_data['confirm_passsord'])
            {
                $error['confirm_password'] = "Confirm Password must match Password";
            }

            if($error == NULL)
            {
                $update = array();
                $update['password'] = md5(strtolower($user_data['username']).$crud_data['password']);
                $this->db->where('id', $user_data['id']);
                $this->db->where('username', $user_data['username']);
                $this->db->update('user',$update);
                $user_data['password'] = $update['password'];
                $this->session->set_userdata("user_data", $user_data);
                $data['message_success'] = "Change saved";
            }else{
                $data['error'] = $error;
            }
        }

        $data['action'] = base_url('user/change_password');

        return $this->load->view("user/change_password", $data);
    }
}
