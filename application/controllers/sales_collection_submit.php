<?php
class sales_collection_submit extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
    }

    public function index()
    {
        $data = $this->data;

        $input_data = $this->input_data['get'];

        $pagination = [
            'page' => (int) $input_data['p'] ? $input_data['p'] : 1,
            'limit' => (int) $input_data['limit'] ? $input_data['p'] : 20
        ];

        $pagination['offset'] = ($pagination['page'] - 1) * $pagination['limit'];

        $data['search'] = $pagination['search'] = $this->input_data['get'];

        $additional_where = '';

        if (!empty($data['search']['payment_method'])) {
            $payment_method = $data['search']['payment_method'];

            $additional_where .= " AND sales_collection_submit.payment_method = '$payment_method'";
        }

        if (!empty($data['search']['document_date_from'])) {
            $from = $data['search']['document_date_from'];

            $additional_where .= " AND (sales_collection_submit.document_date >= '$from')";
        }

        if (!empty($data['search']['document_date_to'])) {
            $to = $data['search']['document_date_to'];

            $additional_where .= " AND (sales_collection_submit.document_date <= '$to')";
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS sales_collection_submit.*, bank_reconciliation.clear_date FROM sales_collection_submit
        LEFT JOIN bank_reconciliation ON sales_collection_submit.bank_reconciliation_id = bank_reconciliation.id
        WHERE sales_collection_submit.deleted_at IS NULL $additional_where LIMIT ".$pagination['offset'].", ".$pagination['limit'];

        $data['GL_entries'] = $this->db->query($sql)->result_array();

        $pagination['total'] = $this->db->query('SELECT FOUND_ROWS() AS total;')->result_array()[0]['total'];

        $data['pagination'] = $pagination;

        $this->load->model('company_model');
        $company_data = $this->company_model->find(1);

        $data['locked_financial_month'] = $company_data['locked_financial_month'];

        return $this->template->loadView("sales_collection_submit/index", $data, "admin");
    }

    public function delete()
    {
        if($this->input_data) {
            $this->load->model('sales_collection_model');
            $this->load->model('GL_transaction_model');
            $this->load->model('sales_collection_submit_model');
            $this->load->model('GL_transaction_project_split_model');
            $this->load->model('bank_reconciliation_model');

            $input_data = $this->input_data['post'];

            $sales_collection_submit = $this->sales_collection_submit_model->find($input_data['id']);

            $this->sales_collection_submit_model->delete($input_data['id']);

            $additional = $sales_collection_submit['payable_GL_transaction_id1'] . ', ' . $sales_collection_submit['bank_GL_transaction_id'] . ', ' . $sales_collection_submit['bank_charge_GL_transaction_id'];

            $this->GL_transaction_model->realDelete($sales_collection_submit['payable_GL_transaction_id1']);

            $this->GL_transaction_model->realDelete($sales_collection_submit['bank_GL_transaction_id']);

            $this->GL_transaction_model->realDelete($sales_collection_submit['bank_charge_GL_transaction_id']);

            $this->bank_reconciliation_model->realDelete($sales_collection_submit['bank_reconciliation_id']);

            if (!empty($sales_collection_submit['payable_GL_transaction_id2'])) {
                $this->GL_transaction_model->realDelete($sales_collection_submit['payable_GL_transaction_id2']);

                $additional .= ', ' . $sales_collection_submit['payable_GL_transaction_id2'];
            }

            $sql = "SELECT id FROM sales_collection
        WHERE sales_collection_submit_id = ".$input_data['id']." AND deleted_at IS NULL";

            $sales_collection_ids = $this->db->query($sql)->result_array();

            foreach ($sales_collection_ids AS $sales) {
                $this->sales_collection_model->realDelete($sales['id']);
            }

            $sql = "SELECT id FROM GL_transaction_project_split
        WHERE GL_transaction_id IN ($additional) AND deleted_at IS NULL";

            $GL_transactions_project_split = $this->db->query($sql)->result_array();

            foreach ($GL_transactions_project_split AS $project_split) {
                $this->GL_transaction_project_split_model->realDelete($project_split['id']);
            }

            $this->__set_flash_message('The Submission is deleted successfully');
            redirect('sales_collection_submit');
            return;

        }
    }
}
