<?php
class test_iframe extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';

    }

    /**
     * index page, for showing list of data
     */
    public function view(){

        $data = $this->data;

        // Call API to valid token
        $call_api = new HungryGoWhere();

        $url = $call_api->server."auth";
        $params = $call_api->token;
        $sig = $call_api->get_signature($url, $params);
        $params['sig'] = $sig;

        $return = $call_api->get_page($url, 'post', $params);
        $data_auth = json_decode($return, true);
        if($data_auth['status'] == 200) {


            $session_token = $data_auth['data']['session_token'];

            $url = $call_api->server . "sso_token";
            $params1['session_token'] = $session_token;
            $params1['restaurant_id'] = 199447;
            $params1['module_id'] = 7;
            $sig = $call_api->get_signature($url, $params1);
            $params1['sig'] = $sig;

            $return = $call_api->get_page($url, 'post', $params1);
            $data['token'] = json_decode($return, true)['data'];
        }

        return $this->template->loadView("test_iframe/view", $data, "");
    }
}
