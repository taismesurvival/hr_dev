<?php
class trial_balance_report extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';

    }

    /**
     * index page, for showing list of data
     */
    public function view(){

        $data = $this->data;

        $this->load->model('company_model');
        $this->load->model('bank_account_model');
        $company_data = $this->company_model->find(1);

        if($this->input_data) {
            $input_data = $this->input_data['post'];

            $data['company_name'] = $company_data['name'];

            $last_day_of_the_month = date('Y-m-d', strtotime($input_data['month'].'-01 +1 month -1 day'));
            $first_day_of_the_month = $input_data['month'].'-01';

            $data['ending_month'] = date_format(date_create($input_data['month']),"Y F");

            $sql = "SELECT master_account.id, master_account.code, master_account.name FROM master_account 
        LEFT JOIN master_account AS MC ON MC.father_id = master_account.id
        WHERE master_account.deleted_at IS NULL AND master_account.status = 'active'
         AND MC.id IS NULL GROUP BY master_account.id ORDER BY master_account.code";

            $data['accounts'] = $this->db->query($sql)->result_array();
            foreach ($data['accounts'] AS $key => $account) {

                $ending_balance_sql = "SELECT GL_transaction_project_split.amount, GL_transaction.type
    FROM GL_transaction_project_split
    LEFT JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
    LEFT JOIN GL_transaction ON GL_transaction_project_split.GL_transaction_id = GL_transaction.id
    WHERE account_project.account_id = '".$account['id']."'
    AND GL_transaction_project_split.document_date <= '$last_day_of_the_month'";

                $net_activity_sql = $ending_balance_sql." AND GL_transaction_project_split.document_date >= '$first_day_of_the_month'";

                $net_activity = $this->db->query($net_activity_sql)->result_array();
                $ending_balance = $this->db->query($ending_balance_sql)->result_array();

                $data['accounts'][$key]['debit_net_activity'] = 0;
                $data['accounts'][$key]['debit_ending_balance'] = 0;

                if (!empty($net_activity) && !empty($ending_balance)) {

                    foreach ($net_activity AS $act) {

                        if ($act['type'] == 'debit') {

                            $data['accounts'][$key]['debit_net_activity'] += $act['amount'];

                        } else {
                            $data['accounts'][$key]['debit_net_activity'] -= $act['amount'];
                        }

                    }

                    foreach ($ending_balance AS $end) {

                        if ($end['type'] == 'debit') {

                            $data['accounts'][$key]['debit_ending_balance'] += $end['amount'];

                        } else {
                            $data['accounts'][$key]['debit_ending_balance'] -= $end['amount'];
                        }

                    }

                } else {

                    unset($data['accounts'][$key]);
                }

            }

            return $this->template->loadView("trial_balance_report/report", $data);

        }

        return $this->template->loadView("trial_balance_report/view", $data, "admin");
    }
}
