<?php
class Group_permission extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';
        redirect('');
    }

    public function index()
    {
        $data = $this->data;

        $input_data = $this->input_data['get'];

        $pagination = [
            'page' => (int) $input_data['p'] ? $input_data['p'] : 1,
            'limit' => (int) $input_data['limit'] ? $input_data['p'] : 20
        ];

        $pagination['offset'] = ($pagination['page'] - 1) * $pagination['limit'];

        $data['search'] = $pagination['search'] = $this->input_data['get'];

        $additional_where = '';
        if (!empty($data['search'])) {
            $name_keyword = $data['search']['name'];

            $additional_where = " AND name LIKE '%$name_keyword%'";
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS id, name, description FROM role 
        WHERE deleted_at IS NULL $additional_where LIMIT ".$pagination['offset'].", ".$pagination['limit'];

        $data['group_permissions'] = $this->db->query($sql)->result_array();

        $pagination['total'] = $this->db->query('SELECT FOUND_ROWS() AS total;')->result_array()[0]['total'];

        $data['pagination'] = $pagination;

        return $this->template->loadView("group_permission/index", $data, "admin");
    }

    public function add($group_id = null)
    {
        $this->load->model('role_model');
        $data = $this->data;

        if (!empty($group_id)) {
            $data['group_detail'] = $this->role_model->find($group_id);
            $data['title'] = 'Edit Group Permission';
        }

        if($this->input_data) {

            $input_data = $this->input_data['post'];

            if (!empty($group_id)) {
                $this->role_model->update($input_data['id'], $input_data);
                $this->__set_flash_message('Group is updated successfully');
                redirect('group_permission');
                return;
            } else {
                $this->role_model->add($input_data);
                $this->__set_flash_message('Group is created successfully');
                redirect('group_permission');
                return;
            }

        }

        return $this->template->loadView("group_permission/add", $data, "admin");
    }

    public function delete()
    {
        if($this->input_data) {
            $this->load->model('role_model');

            $input_data = $this->input_data['post'];

            $this->role_model->delete($input_data['id']);
            $this->__set_flash_message('Group is deleted successfully');
            redirect('group_permission');
            return;

        }
    }
}
