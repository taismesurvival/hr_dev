<?php
class clear_bank_reconciliation extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->helper("file");
    }

    public function index()
    {
        $data = $this->data;

        $sql = "SELECT id, name, number, account_code FROM bank_account 
        WHERE deleted_at IS NULL AND type = 'bank'";

        $bank_accounts = $this->db->query($sql)->result_array();

        $data['bank_accounts_selector'] = '<option selected value="">Please Select</option>';
        foreach ($bank_accounts AS $item) {

            if (!empty($item['number'])) {
                $data['bank_accounts_selector'] .= '<option value="' . $item['id'] . '">' . $item['name'] . ' (' . $item['number'] . ')</option>';
            } else {
                $data['bank_accounts_selector'] .= '<option value="' . $item['id'] . '">' . $item['name']. '</option>';
            }
        }

        $data['search'] = $pagination['search'] = $this->input_data['post'];

        $additional_where = '';

        if (!empty($data['search']['selected_month'])) {
            $selected_month = $data['search']['selected_month'].'-01';

            $selected_month = date("Y-m-t", strtotime($selected_month));

            $additional_where .= " AND bank_reconciliation.pay_date <= '$selected_month'";
        }

        if (!empty($data['search']['bank_account_id'])) {
            $bank_account_id = $data['search']['bank_account_id'];

            $additional_where .= " AND bank_reconciliation.bank_account_id = '$bank_account_id'";
        }

        $data['payments'] = [];

        if (!empty($data['search'])) {

            $this->load->model('company_model');
            $company_data = $this->company_model->find(1);

            $company_code = $company_data['company_id'];

            $sql = "SELECT * FROM bank_statement_upload
                WHERE deleted_at IS NULL AND month = '".$data['search']['selected_month']."' AND bank_account_id = '".$data['search']['bank_account_id']."'";

            $bank_statement_uploaded = $this->db->query($sql)->result_array();

            if ($bank_statement_uploaded[0]['type'] == 'manual') {
                $file_path = './upload/accounting/' . strtoupper($company_code) . '/manual_bank_statement/' . $bank_statement_uploaded[0]['file_name'];

                $this->load->library('php_excel');
                $objPHPExcel = PHPExcel_IOFactory::load($file_path);
                $total_rows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
                $arr_data = array();
                for ($i = 2; $i <= $total_rows; $i++) {

                    $arr_data[$i]['date'] = $objPHPExcel->getActiveSheet()->getCell('B' . $i)->getValue();
                    $arr_data[$i]['deposit'] = $objPHPExcel->getActiveSheet()->getCell('C' . $i)->getValue();
                    $arr_data[$i]['withdrawal'] = $objPHPExcel->getActiveSheet()->getCell('D' . $i)->getValue();
                    $arr_data[$i]['description'] = $objPHPExcel->getActiveSheet()->getCell('E' . $i)->getValue();
                }


                $sql = "SELECT bank_reconciliation.amount, bank_reconciliation.ref_no, bank_reconciliation.pay_date, bank_reconciliation.type,
        bank_reconciliation.id
        FROM bank_reconciliation 
        WHERE bank_reconciliation.deleted_at IS NULL AND bank_reconciliation.clear_date IS NULL $additional_where";

                $bank_reconciliation = $this->db->query($sql)->result_array();

                $cash_total_sales = 0;
                $card_total_sales = 0;
                $nets_total_sales = 0;
                $american_express_total_sales = 0;

                foreach ($arr_data AS $key => $item) {

                    $pieces = explode(" ", $item['description']);

                    if (!isset($pieces[1]) && $item['deposit'] > 0) {

                        switch ($pieces[0]) {
                            case 'DCA':
                                $cash_total_sales += $item['deposit'];
                                break;

                            case 'DVM':
                                $card_total_sales += $item['deposit'];
                                break;

                            case 'DNE':
                                $nets_total_sales += $item['deposit'];
                                break;

                            case 'DAM':
                                $american_express_total_sales += $item['deposit'];
                                break;
                        }

                        unset($arr_data[$key]);

                    }

                    if (empty($item['deposit']) && empty($item['withdrawal'])) {

                        unset($arr_data[$key]);

                    }
                }

                if ($cash_total_sales > 0) {
                    $arr_data[] = [
                        'date' => $selected_month,
                        'deposit' => $cash_total_sales,
                        'withdrawal' => NULL,
                        'description' => 'DCA'

                    ];
                }

                if ($card_total_sales > 0) {
                    $arr_data[] = [
                        'date' => $selected_month,
                        'deposit' => $card_total_sales,
                        'withdrawal' => NULL,
                        'description' => 'DVM'

                    ];
                }

                if ($nets_total_sales > 0) {
                    $arr_data[] = [
                        'date' => $selected_month,
                        'deposit' => $nets_total_sales,
                        'withdrawal' => NULL,
                        'description' => 'DNE'

                    ];
                }

                if ($american_express_total_sales > 0) {
                    $arr_data[] = [
                        'date' => $selected_month,
                        'deposit' => $american_express_total_sales,
                        'withdrawal' => NULL,
                        'description' => 'DAM'

                    ];
                }

                foreach ($bank_reconciliation AS $key1 => $item) {

                    foreach ($arr_data AS $key2 => $row) {

                        if (($item['type'] == 'AR' && $item['amount'] == $row['deposit'] && $row['description'] == $item['ref_no']) ||
                            ($item['type'] == 'AP' && $item['amount'] == $row['withdrawal'] && $row['description'] == $item['ref_no'])
                        ) {

                            $bank_reconciliation[$key1]['clear_date'] = $row['date'];
                            $bank_reconciliation[$key1]['date'] = $row['date'];
                            $bank_reconciliation[$key1]['deposit'] = $row['deposit'];
                            $bank_reconciliation[$key1]['withdrawal'] = $row['withdrawal'];
                            $bank_reconciliation[$key1]['description'] = $row['description'];
                            $arr_data[$key2]['clear'] = TRUE;

                        }
                    }

                }

                foreach ($arr_data AS $row) {
                    if ($row['clear'] != TRUE) {
                        $bank_reconciliation[] = $row;
                    }
                }

                $data['payments'] = $bank_reconciliation;
            }

        }

        $sql = "SELECT id, name, code AS description,
        type
        FROM project
        WHERE deleted_at IS NULL AND status = 'active' AND type='outlet'";

        $data['project_list'] = $this->db->query($sql)->result_array();

        return $this->template->loadView("clear_bank_reconciliation/index", $data, "admin");
    }

    public function submit ()
    {
        if($this->input_data) {
            $input_data = $this->input_data['post'];

            $this->load->model('bank_reconciliation_model');
            $this->load->model('clear_bank_submit_model');

            $bank_reconciliation_ids = [];
            foreach ($input_data['transactions'] AS $trans) {

                $this->bank_reconciliation_model->update($trans['bank_reconciliation_id'], [
                    'clear_date' => $trans['clear_date']
                ]);
                $bank_reconciliation_ids[] = $trans['bank_reconciliation_id'];
            }

            $this->clear_bank_submit_model->add([
                'bank_recons_ids' => json_encode($bank_reconciliation_ids),
                'type' => 'manual',
                'month' => $input_data['month'],
                'bank_account_id' => $input_data['bank_account_id']

            ]);

            $this->__set_flash_message('Some Bank Reconciliations is clear successfully');
            return;
        }

    }
}
