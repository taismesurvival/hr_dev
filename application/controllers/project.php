<?php
class Project extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';
    }

    public function index()
    {
        $data = $this->data;

        $additional_where = '';

        $data['search'] = $this->input_data['get'];

        if (!empty($data['search']['status'])) {
            $is_active = $data['search']['status'];

            $additional_where .= " AND status = '$is_active'";
        }

        $sql = "SELECT id, name, code, status, type, address, phone, fax, secret_key FROM project 
        WHERE deleted_at IS NULL $additional_where";

        $data['projects'] = $this->db->query($sql)->result_array();

        return $this->template->loadView("project/index", $data, "admin");
    }

    public function edit()
    {
        if($this->input_data) {
            $this->load->model('project_model');
            $this->load->model('account_project_model');

            $input_data = $this->input_data['post'];

            if (!empty($input_data['id'])) {
                $this->project_model->update($input_data['id'], $input_data);
                $this->__set_flash_message('Project is updated successfully');
                redirect('project');
                return;
            } else {
                $project_id = $this->project_model->add($input_data);

                $this->load->model('master_account_model');
                $accounts = $this->master_account_model->all(" AND status = 'active'");

                foreach ($accounts AS $account) {
                    $this->account_project_model->add([
                        'account_id' => $account['id'],
                        'project_id' => $project_id
                    ]);
                }

                $this->__set_flash_message('Project is created successfully');
                redirect('project');
                return;
            }

        }
    }

    public function delete()
    {
        if($this->input_data) {
            $this->load->model('project_model');

            $input_data = $this->input_data['post'];

            $this->project_model->delete($input_data['id']);
            $this->__set_flash_message('Project is deleted successfully');
            redirect('project');
            return;

        }
    }
}
