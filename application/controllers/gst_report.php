<?php
class gst_report extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';

    }

    /**
     * index page, for showing list of data
     */
    public function view(){

        $data = $this->data;

        $userdata = $this->session->userdata("user_data");

        $data['hide_bar'] = $userdata['role_id'] == 3 ? TRUE : FALSE;

        $data['check_active'] = $this->router->fetch_class().'/'.$this->router->fetch_method();

        $this->load->model('company_model');
        $company_data = $this->company_model->find(1);

        if($this->input_data) {
            $input_data = $this->input_data['post'];
            $data['company_name'] = $company_data['name'];

            $data['starting_month'] = date_format(date_create($input_data['starting_month']),"Y F d");
            $data['ending_month'] = date_format(date_create($input_data['ending_month']),"Y F d");

            $starting_date = $input_data['starting_month'];
            $ending_date = $input_data['ending_month'];

            if ($input_data['report_type'] == 'summary') {

                $sql = "SELECT GL_transaction_project_split.amount, GL_transaction.type
        FROM GL_transaction_project_split 
        LEFT JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        LEFT JOIN master_account ON account_project.account_id = master_account.id
        LEFT JOIN GL_transaction ON GL_transaction_project_split.GL_transaction_id = GL_transaction.id
        WHERE master_account.code > 40000 AND  master_account.code < 50000
        AND GL_transaction_project_split.document_date >= '$starting_date' AND GL_transaction_project_split.document_date <= '$ending_date'";

                $sales = $this->db->query($sql)->result_array();
                $data['sales_total'] = 0;
                foreach ($sales AS $sale) {
                    if ($sale['type'] == 'credit') {
                        $data['sales_total'] += $sale['amount'];
                    } else {
                        $data['sales_total'] -= $sale['amount'];
                    }
                }

                $sql = "SELECT GL_transaction_project_split.amount, GL_transaction.type
        FROM GL_transaction_project_split 
        LEFT JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        LEFT JOIN master_account ON account_project.account_id = master_account.id
        LEFT JOIN GL_transaction ON GL_transaction_project_split.GL_transaction_id = GL_transaction.id
        WHERE master_account.code = 21401
        AND GL_transaction_project_split.document_date >= '$starting_date' AND GL_transaction_project_split.document_date <= '$ending_date'";

                $sales_tax = $this->db->query($sql)->result_array();
                $data['sales_tax_total'] = 0;
                foreach ($sales_tax AS $tax) {
                    if ($tax['type'] == 'credit') {
                        $data['sales_tax_total'] += $tax['amount'];
                    } else {
                        $data['sales_tax_total'] -= $tax['amount'];
                    }
                }

                $sql = "SELECT purchase_transaction.quantity, purchase_transaction.unit_cost, purchase_transaction.discount_rate,
        purchase_transaction.tax_rate, purchase_transaction.tax_type
        FROM purchase_transaction 
        LEFT JOIN purchase_entry ON purchase_transaction.purchase_entry_id = purchase_entry.id
        LEFT JOIN credit_note ON purchase_transaction.id = credit_note.purchase_transaction_id
        WHERE purchase_entry.document_date >= '$starting_date' AND purchase_entry.document_date <= '$ending_date'
        AND purchase_entry.deleted_at IS NULL AND credit_note.id IS NULL";

                $purchases_data = $this->db->query($sql)->result_array();
                $data['import_purchases_total'] = 0;
                $data['gst_purchases_total'] = 0;
                $data['exempt_purchases_total'] = 0;
                $data['purchases_tax_total'] = 0;
                foreach ($purchases_data AS $purchase) {
                    switch ($purchase['tax_type']) {
                        case 'IM7':
                            $import_value = ($purchase['quantity'] * $purchase['unit_cost']) * (100 - $purchase['discount_rate']) / 100;
                            $data['import_purchases_total'] += $import_value;
                            $data['purchases_tax_total'] += round($import_value * $purchase['tax_rate'] / 100, 2);
                            break;
                        case 'TX7':
                            $gst_value = ($purchase['quantity'] * $purchase['unit_cost']) * (100 - $purchase['discount_rate']) / 100;
                            $data['gst_purchases_total'] += $gst_value;
                            $data['purchases_tax_total'] += round($gst_value * $purchase['tax_rate'] / 100, 2);
                            break;
                        case 'EP':
                            $exempt_value = ($purchase['quantity'] * $purchase['unit_cost']) * (100 - $purchase['discount_rate']) / 100;
                            $data['exempt_purchases_total'] += $exempt_value;
                            $data['purchases_tax_total'] += round($exempt_value * $purchase['tax_rate'] / 100,2);
                            break;
                    }
                }

                $sql = "SELECT tax_amount, tax_type, amount
        FROM credit_note
        WHERE document_date >= '$starting_date' AND document_date <= '$ending_date' AND deleted_at IS NULL";

                $credit_notes = $this->db->query($sql)->result_array();

                foreach ($credit_notes AS $credit) {
                    switch ($credit['tax_type']) {
                        case 'IM7':
                            $data['import_purchases_total'] -= $credit['amount'];
                            $data['purchases_tax_total'] -= $credit['tax_amount'];
                            break;
                        case 'TX7':
                            $data['gst_purchases_total'] -= $credit['amount'];
                            $data['purchases_tax_total'] -= $credit['tax_amount'];
                            break;
                        case 'EP':
                            $data['exempt_purchases_total'] -= $credit['amount'];
                            $data['purchases_tax_total'] -= $credit['tax_amount'];
                            break;
                    }
                }

                return $this->template->loadView("gst_report/report", $data);
            } else {

                $sql = "SELECT purchase_transaction.id, purchase_transaction.quantity, purchase_transaction.unit_cost, purchase_transaction.discount_rate,
        purchase_transaction.tax_rate, purchase_transaction.tax_type, supplier.name AS supplier_name, purchase_entry.refference_no, purchase_entry.document_date,
        purchase_entry.supplier_id
        FROM purchase_transaction 
        LEFT JOIN purchase_entry ON purchase_transaction.purchase_entry_id = purchase_entry.id
        LEFT JOIN supplier ON purchase_entry.supplier_id = supplier.id
        LEFT JOIN credit_note ON purchase_transaction.id = credit_note.purchase_transaction_id
        WHERE purchase_entry.document_date >= '$starting_date' AND purchase_entry.document_date <= '$ending_date'
        AND purchase_entry.deleted_at IS NULL AND credit_note.id IS NULL ORDER BY purchase_entry.supplier_id ASC";

                $purchase_trans_data = $this->db->query($sql)->result_array();

                $data['purchase_trans_data'] = $purchase_trans_data;

                $sql = "SELECT credit_note.id, credit_note.document_date, credit_note.tax_amount, credit_note.tax_type, credit_note.amount,
        supplier.name AS supplier_name, credit_note.refference_no, credit_note.supplier_id
        FROM credit_note
        JOIN supplier ON credit_note.supplier_id = supplier.id
        WHERE credit_note.document_date >= '$starting_date' AND credit_note.document_date <= '$ending_date' AND credit_note.deleted_at IS NULL";

                $credit_notes_data = $this->db->query($sql)->result_array();
                $data['credit_notes'] = [];
                foreach ($credit_notes_data as $item) {

                    $data['credit_notes'][$item['supplier_id']][] = $item;
                    
                }

                $sql = "SELECT GL_entry.id, GL_entry.description, GL_entry.document_date,
        GL_transaction.amount, GL_transaction.type, master_account.code
        FROM GL_entry 
        LEFT JOIN GL_mapping ON GL_entry.id = GL_mapping.GL_entry_id
        LEFT JOIN GL_transaction ON GL_mapping.GL_transaction_id = GL_transaction.id
        INNER JOIN GL_transaction_project_split ON GL_transaction_project_split.GL_transaction_id = GL_transaction.id
        INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        INNER JOIN master_account ON master_account.id = account_project.account_id
        WHERE GL_entry.document_date >= '$starting_date' AND GL_entry.document_date <= '$ending_date'
        AND GL_entry.deleted_at IS NULL AND ((master_account.code > '40000' AND master_account.code < '50000') OR master_account.code = '21401')";

                $sales_data = $this->db->query($sql)->result_array();

                $data['sales_data'] = [];
                foreach ($sales_data AS $item) {

                    $data['sales_data'][$item['id']]['description'] = $item['description'];
                    $data['sales_data'][$item['id']]['document_date'] = $item['document_date'];

                    if ($item['code'] == 21401 && $item['type'] == 'credit') {

                        $data['sales_data'][$item['id']]['tax_amount'] += $item['amount'];

                    }

                    if ($item['code'] > 40000 && $item['code'] < 50000) {

                        if ($item['type'] == 'credit') {
                            $data['sales_data'][$item['id']]['taxable_amount'] += $item['amount'];
                        } else {
                            $data['sales_data'][$item['id']]['taxable_amount'] -= $item['amount'];
                        }

                    }

                }

                return $this->template->loadView("gst_report/detail_report", $data);

            }
        }

        return $this->template->loadView("gst_report/view", $data, "admin");
    }
}
