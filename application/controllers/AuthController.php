<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AuthController extends AR_Controller {
	public function __construct(){
		parent::__construct();
		
		
	}
	public function login(){

		$userdata = $this->session->userdata("user_data");
		if(isset($userdata['id'])){

			redirect("");
		}

		$data['title'] = 'Login';
		//$data['company_data'] = $this->company_model->find(1);
		
		if(isset($this->input_data['post']['user_name'])){
			$input_data = $this->input_data['post'];
			$user_name = strtoupper($input_data['user_name']);

			$company_data = $this->company_model->find(strtoupper($input_data["code"]),"company_id");
		
			if(isset($company_data['id'])){

				$acc_db = $company_data["database"];


				$query1 = "SELECT $acc_db.user.* FROM $acc_db.user WHERE $acc_db.user.username = '$user_name' AND $acc_db.user.deleted_at IS NULL
            AND $acc_db.user.role_id != 3";

				$res1 = $this->db->query($query1);

				$adminData = $res1->row_array();
				
				$password = md5(strtolower($user_name).$input_data['password']);

				if(isset($adminData['password']) && $adminData['password'] == $password){
				
					$this->session->set_userdata("user_data",$adminData);
					$this->session->set_userdata("user_type","admin");
                    $this->session->set_userdata("login_type",'');

                    $this->session->set_userdata("dbConfig", $company_data["database"]);

					redirect('');
					return;
				} else {

					$data['msg'] = "Incorrect login credentials";
				}

			}else{
				$data['msg'] = "Company Not Found";
			}
				
		}
		//echo $this->db->database;
		$this->load->view("admin/loginForm",$data,"");
	}
	public function logout($type=""){

		$this->session->unset_userdata('user_data');
        $this->session->set_userdata("dbConfig",$this->config->item("master_database"));

			//$this->session->sess_destroy();
		redirect("AuthController/login");
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */