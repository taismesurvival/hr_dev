<?php
class payment extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
    }

    public function index()
    {
        $data = $this->data;

        $sql = "SELECT id, account_code, name, number, type FROM bank_account 
        WHERE deleted_at IS NULL ORDER BY account_code";

        $data['accounts'] = $this->db->query($sql)->result_array();

        $input_data = $this->input_data['get'];

        $pagination = [
            'page' => (int) $input_data['p'] ? $input_data['p'] : 1,
            'limit' => (int) $input_data['limit'] ? $input_data['p'] : 500
        ];

        $pagination['offset'] = ($pagination['page'] - 1) * $pagination['limit'];

        $data['search'] = $pagination['search'] = $this->input_data['get'];

        $additional_where = '';

        if (!empty($data['search']['minimum_amount'])) {
            $minimum_amount = $data['search']['minimum_amount'];

            $additional_where .= " AND purchase_entry.total_amount >= $minimum_amount";
        }

        if (!empty($data['search']['maximum_amount'])) {
            $maximum_amount = $data['search']['maximum_amount'];

            $additional_where .= " AND purchase_entry.total_amount <= $maximum_amount";
        }

        if (!empty($data['search']['keyword'])) {
            $keyword = $data['search']['keyword'];

            $additional_where .= " AND (purchase_entry.description LIKE '%$keyword%')";
        }

        if (!empty($data['search']['supplier_name'])) {
            $supplier_name = $data['search']['supplier_name'];

            $additional_where .= " AND (supplier.name LIKE '%$supplier_name%')";
        }

        if (!empty($data['search']['document_date_from'])) {
            $from = $data['search']['document_date_from'];

            $additional_where .= " AND (purchase_entry.document_date >= '$from')";
        }

        if (!empty($data['search']['document_date_to'])) {
            $to = $data['search']['document_date_to'];

            $additional_where .= " AND (purchase_entry.document_date <= '$to')";
        }

        if (!empty($data['search']['status'])) {
            $status = $data['search']['status'];

            $additional_where .= " AND purchase_entry.status = '$status'";
        }

        if (!empty($data['search']['account_keyword'])) {
            $account_keyword = $data['search']['account_keyword'];

            $additional_where .= " AND (master_account.code LIKE '%$account_keyword%' OR master_account.name LIKE '%$account_keyword%')";
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS purchase_entry.id AS purchase_entry_id, purchase_entry.document_date, purchase_entry.refference_no,
        purchase_entry.total_amount, supplier.name AS supplier_name, purchase_entry.payment_GL_transaction_id, purchase_entry.supplier_id,
        purchase_entry.due_date AS pay_date, purchase_entry.status, purchase_entry.description, purchase_entry.payment_GL_transaction_id AS GL_transaction_id,
        purchase_entry.separate_father_id, purchase_entry.bank_reconciliation_id
        FROM purchase_entry 
        LEFT JOIN supplier on supplier.id = purchase_entry.supplier_id
        LEFT JOIN purchase_transaction ON purchase_entry.id  = purchase_transaction.purchase_entry_id
        LEFT JOIN GL_transaction_project_split ON purchase_transaction.GL_transaction_id  = GL_transaction_project_split.GL_transaction_id
        LEFT JOIN account_project ON account_project.id = GL_transaction_project_split.account_project_id
        LEFT JOIN master_account ON account_project.account_id = master_account.id
        WHERE purchase_entry.total_amount > 0 AND purchase_entry.deleted_at IS NULL AND purchase_entry.type = 'PURC' AND purchase_entry.status != 'processing' $additional_where GROUP BY purchase_entry.id LIMIT ".$pagination['offset'].", ".$pagination['limit'];

        $data['payments'] = $this->db->query($sql)->result_array();

        $pagination['total'] = $this->db->query('SELECT FOUND_ROWS() AS total;')->result_array()[0]['total'];

        $data['pagination'] = $pagination;

        $sql = "SELECT separate_father_id FROM purchase_entry WHERE status = 'done' AND separate_father_id IS NOT NULL AND deleted_at IS NULL";

        $separate_father_ids = $this->db->query($sql)->result_array();

        $data['separate_father_ids_array'] = [];
        foreach ($separate_father_ids AS $id) {
            $data['separate_father_ids_array'][] = $id['separate_father_id'];
        }

        $sql = "SELECT purchase_entry_id FROM purchase_transaction WHERE unit_cost < 0 AND payable_GL_transaction_id IS NOT NULL AND deleted_at IS NULL GROUP BY purchase_entry_id";

        $credit_note_purchase_ids = $this->db->query($sql)->result_array();

        $data['purchase_entry_credit_note'] = [];
        foreach ($credit_note_purchase_ids AS $id) {
            $data['purchase_entry_credit_note'][] = $id['purchase_entry_id'];
        }

        $sql = "SELECT id, name, number, account_code, type FROM bank_account 
        WHERE is_default = '1'";

        $default_account = $this->db->query($sql)->result_array();

        $data['default_account'] = $default_account[0];

        return $this->template->loadView("payment/index", $data, "admin");
    }

    public function pay_invoices()
    {
        if($this->input_data) {
            $this->load->model('bank_reconciliation_model');
            $this->load->model('purchase_entry_model');
            $this->load->model('GL_transaction_model');
            $this->load->model('GL_transaction_project_split_model');

            $input_data = $this->input_data['post'];

            $input_data['type'] = 'AP';
            $bank_reconciliation_id = $this->bank_reconciliation_model->add($input_data);

            $GL_transaction_id_bank_recon = $this->GL_transaction_model->add([
                'type' =>'credit',
                'amount' => $input_data['amount'],
                'transaction_type' => 'bank_reconciliation'
            ]);

            $bank_reconciliation_projects = [];
            $payment_purchase_GL_transaction_id = [];

            foreach ($input_data['payments'] AS $payment) {

                $this->purchase_entry_model->update($payment['purchase_entry_id'], [
                    'status' => 'done',
                    'bank_reconciliation_id' => $bank_reconciliation_id
                ]);

                // Set status done for father
                $purchase_data = $this->purchase_entry_model->find($payment['purchase_entry_id']);
                if (!empty($purchase_data['separate_father_id'])) {

                    $sql = "SELECT *
        FROM purchase_entry
        WHERE separate_father_id = ".$purchase_data['separate_father_id']." AND deleted_at IS NULL AND status = 'pending'";

                    $purchases_pending_for_this = $this->db->query($sql)->result_array();

                    if (empty($purchases_pending_for_this)) {

                        $this->purchase_entry_model->update($purchase_data['separate_father_id'], [
                            'status' => 'done'
                        ]);

                    }

                }


                $payment_amount = $payment['amount'];
                // for credit note purchase transaction
                $sql = "SELECT payable_GL_transaction_id, tax_rate, unit_cost, quantity
        FROM purchase_transaction
        WHERE purchase_entry_id = ".$payment['purchase_entry_id']." AND payable_GL_transaction_id IS NOT NULL";

                $credit_notes = $this->db->query($sql)->result_array();

                if (!empty($credit_notes)) {
                    foreach ($credit_notes AS $credit_note) {

                        if ($credit_note['unit_cost'] <= 0) {
                            $credit_note_GL_transaction_id = $this->GL_transaction_model->add([
                                'type' => 'credit',
                                'amount' => round(((-1)*$credit_note['unit_cost']*$credit_note['quantity']*(100+$credit_note['tax_rate'])/100), 2),
                                'transaction_type' => 'payment'
                            ]);

                            $payment_amount += round(((-1)*$credit_note['unit_cost']*$credit_note['quantity']*(100+$credit_note['tax_rate'])/100), 2);

                            $payment_purchase_GL_transaction_id[] = $credit_note_GL_transaction_id;

                            $sql = "SELECT account_project.project_id, GL_transaction_project_split.amount, GL_transaction_project_split.percentage, GL_transaction_project_split.account_project_id FROM GL_transaction_project_split
        INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        WHERE GL_transaction_project_split.GL_transaction_id = " . $credit_note['payable_GL_transaction_id'];

                            $projects = $this->db->query($sql)->result_array();

                            foreach ($projects AS $project) {

                                $bank_reconciliation_projects[$project['project_id']] -= $project['amount'];

                                $project['GL_transaction_id'] = $credit_note_GL_transaction_id;
                                $project ['document_date'] = $input_data['pay_date'];
                                $this->GL_transaction_project_split_model->add($project);
                            }
                        }
                    }
                }

                // for normal purchase transaction
                $GL_transaction_id = $this->GL_transaction_model->add([
                    'type' =>'debit',
                    'amount' => $payment_amount,
                    'transaction_type' => 'payment'
                ]);

                $payment_purchase_GL_transaction_id[] = $GL_transaction_id;

                $sql = "SELECT account_project.project_id, GL_transaction_project_split.amount, GL_transaction_project_split.percentage, GL_transaction_project_split.account_project_id FROM GL_transaction_project_split
        INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        WHERE GL_transaction_project_split.GL_transaction_id = ".$payment['GL_transaction_id'];

                $projects = $this->db->query($sql)->result_array();

                foreach ($projects AS $project) {

                    $bank_reconciliation_projects[$project['project_id']] += $project['amount'];

                    $project['GL_transaction_id'] = $GL_transaction_id;
                    $project ['document_date'] = $input_data['pay_date'];
                    $this->GL_transaction_project_split_model->add($project);
                }
            }

            foreach ($bank_reconciliation_projects AS $key => $item) {

                $sql = "SELECT account_project.id FROM account_project
        INNER JOIN master_account ON account_project.account_id = master_account.id
        WHERE account_project.project_id = ".$key." AND master_account.code = ".$input_data['account_code'];

                $account_project_id_bank_recon = $this->db->query($sql)->result_array()[0]['id'];

                $this->GL_transaction_project_split_model->add([
                    'amount' => $item,
                    'percentage' => round($item/$input_data['amount']*100, 2),
                    'account_project_id' => $account_project_id_bank_recon,
                    'GL_transaction_id' => $GL_transaction_id_bank_recon,
                    'document_date' => $input_data['pay_date']
                ]);
            }

            $this->bank_reconciliation_model->update($bank_reconciliation_id, [
                'payment_recons_GL_transaction_id' => $GL_transaction_id_bank_recon,
                'payment_purchase_GL_transaction_id' => json_encode($payment_purchase_GL_transaction_id)
            ]);

            $this->__set_flash_message('The Invoices is paid successfully');
            echo json_encode([
                'success' => TRUE
            ]);
            return;
        }
    }

    public function separate_payment ()
    {
        if ($this->input_data) {
            $input_data = $this->input_data['post'];

            $this->load->model('purchase_entry_model');
            $this->load->model('GL_transaction_model');
            $this->load->model('GL_transaction_project_split_model');

            $original_purchase = $new_purchase = $this->purchase_entry_model->find($input_data['id']);

            $sql = "SELECT * FROM GL_transaction_project_split WHERE GL_transaction_id = ".$input_data['payment_GL_transaction_id'];

            $project_splits = $this->db->query($sql)->result_array();

            // Add payment value
            unset($new_purchase['id']);
            $payment_GL_transaction_id = $this->GL_transaction_model->add([
                'type' => 'credit',
                'transaction_type' => 'payment',
                'amount' => $input_data['payment_amount']
            ]);
            $new_purchase['total_amount'] = $input_data['payment_amount'];
            $new_purchase['payment_GL_transaction_id'] = $payment_GL_transaction_id;
            $new_purchase['separate_father_id'] = $input_data['id'];

            $this->purchase_entry_model->add($new_purchase);

            foreach ($project_splits AS $split) {
                $this->GL_transaction_project_split_model->add([
                    'amount' => round(($split['percentage']*$input_data['payment_amount']/100), 2),
                    'percentage' => $split['percentage'],
                    'account_project_id' => $split['account_project_id'],
                    'GL_transaction_id' => $payment_GL_transaction_id,
                    'document_date' => $split['document_date']
                ]);
            }

            // Add the rest
            unset($original_purchase['id']);
            $payment_GL_transaction_id = $this->GL_transaction_model->add([
                'type' => 'credit',
                'transaction_type' => 'payment',
                'amount' => $original_purchase['total_amount'] - $input_data['payment_amount']
            ]);
            $original_purchase['total_amount'] = $original_purchase['total_amount'] - $input_data['payment_amount'];
            $original_purchase['payment_GL_transaction_id'] = $payment_GL_transaction_id;
            $original_purchase['separate_father_id'] = $input_data['id'];

            $this->purchase_entry_model->add($original_purchase);

            foreach ($project_splits AS $split) {
                $this->GL_transaction_project_split_model->add([
                    'amount' => round(($split['percentage']*$original_purchase['total_amount']/100), 2),
                    'percentage' => $split['percentage'],
                    'account_project_id' => $split['account_project_id'],
                    'GL_transaction_id' => $payment_GL_transaction_id,
                    'document_date' => $split['document_date']
                ]);
            }

            // Delete the original one
            $this->purchase_entry_model->update($input_data['id'], [
                'status' => 'processing',
                'payment_GL_transaction_id' => NULL
            ]);
            $this->GL_transaction_model->realDelete($input_data['payment_GL_transaction_id']);

            foreach ($project_splits AS $split) {
                $this->GL_transaction_project_split_model->realDelete($split['id']);
            }
        }

        $this->__set_flash_message('The Invoices is separated successfully');
        redirect('payment');
        return;

    }

    public function undo_separate($id)
    {

        $this->load->model('purchase_entry_model');
        $this->load->model('GL_transaction_model');
        $this->load->model('GL_transaction_project_split_model');

        $original_purchase = $this->purchase_entry_model->find($id);

        $sql = "SELECT * FROM purchase_entry WHERE separate_father_id = ".$id;

        $children_purchase = $this->db->query($sql)->result_array();

        foreach ($children_purchase AS $child) {

            $this->purchase_entry_model->delete($child['id']);

            $sql = "SELECT * FROM GL_transaction_project_split WHERE GL_transaction_id = ".$child['payment_GL_transaction_id'];

            $project_splits = $this->db->query($sql)->result_array();

            $this->GL_transaction_model->realDelete($child['payment_GL_transaction_id']);

            foreach ($project_splits AS $split) {
                $this->GL_transaction_project_split_model->realDelete($split['id']);
            }
        }

        $payment_GL_transaction_id = $this->GL_transaction_model->add([
            'type' => 'credit',
            'transaction_type' => 'payment',
            'amount' => $original_purchase['total_amount']
        ]);

        $this->purchase_entry_model->update($id, [
            'status' => 'pending',
            'payment_GL_transaction_id' => $payment_GL_transaction_id
        ]);

        foreach ($project_splits AS $split) {
            $this->GL_transaction_project_split_model->add([
                'amount' => round(($split['percentage']*$original_purchase['total_amount']/100), 2),
                'percentage' => $split['percentage'],
                'account_project_id' => $split['account_project_id'],
                'GL_transaction_id' => $payment_GL_transaction_id,
                'document_date' => $split['document_date']
            ]);
        }

        $this->__set_flash_message('The Invoices is combined successfully');
        redirect('payment');
        return;
    }

    public function check_ref()
    {
        if ($this->input_data) {
            $input_data = $this->input_data['post'];
            $ref_no = $input_data['ref_no'];

            $sql = "SELECT * FROM bank_reconciliation WHERE bank_account_id = ".$input_data['bank_account_id']." AND ref_no = '$ref_no' AND deleted_at IS NULL";

            $bank_recons = $this->db->query($sql)->result_array();

            if (!empty($bank_recons)) {

                echo json_encode([
                    'error' => TRUE
                ]);
                die;

            } else {

                echo json_encode([
                    'error' => FALSE
                ]);
                die;

            }
        }
    }
}
