<?php
class bank_statement_upload extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->helper("file");
    }

    public function index()
    {
        $data = $this->data;

        $sql = "SELECT id, name, number, account_code FROM bank_account 
        WHERE deleted_at IS NULL AND type = 'bank'";

        $bank_accounts = $this->db->query($sql)->result_array();

        $data['bank_accounts_selector'] = '<option selected value="">Please Select</option>';
        foreach ($bank_accounts AS $item) {

            if (!empty($item['number'])) {
                $data['bank_accounts_selector'] .= '<option value="' . $item['id'] . '">' . $item['name'] . ' (' . $item['number'] . ')</option>';
            } else {
                $data['bank_accounts_selector'] .= '<option value="' . $item['id'] . '">' . $item['name']. '</option>';
            }
        }

        $input_data = $this->input_data['get'];

        $pagination = [
            'page' => (int) $input_data['p'] ? $input_data['p'] : 1,
            'limit' => (int) $input_data['limit'] ? $input_data['p'] : 20
        ];

        $pagination['offset'] = ($pagination['page'] - 1) * $pagination['limit'];

        $data['search'] = $pagination['search'] = $this->input_data['get'];

        $additional_where = '';

        if (!empty($data['search']['month'])) {
            $month = $data['search']['month'];

            $additional_where .= " AND bank_statement_upload.month = '$month'";
        }

        if (!empty($data['search']['type'])) {
            $type = $data['search']['type'];

            $additional_where .= " AND bank_statement_upload.type = '$type'";
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS bank_statement_upload.*, bank_account.name, bank_account.number FROM bank_statement_upload
        INNER JOIN bank_account ON bank_statement_upload.bank_account_id = bank_account.id
        WHERE bank_statement_upload.deleted_at IS NULL $additional_where LIMIT ".$pagination['offset'].", ".$pagination['limit'];

        $data['uploads'] = $this->db->query($sql)->result_array();

        $pagination['total'] = $this->db->query('SELECT FOUND_ROWS() AS total;')->result_array()[0]['total'];

        $data['pagination'] = $pagination;

        $this->load->model('company_model');
        $company_data = $this->company_model->find(1);

        $company_code = $company_data['company_id'];
        $data['url'] = base_url().'/upload/accounting/'.strtoupper($company_code).'/manual_bank_statement/';

        return $this->template->loadView("bank_statement_upload/index", $data, "admin");
    }

    public function edit ()
    {
        if($this->input_data) {
            $this->load->model('bank_statement_upload_model');

            $input_data = $this->input_data['post'];

            $this->load->model('company_model');
            $company_data = $this->company_model->find(1);

            $company_code = $company_data['company_id'];

            $file_name = $_FILES['file_name']['name'];

            $arr_file_name = explode('.', $file_name);
            $file_type = end($arr_file_name);
            unset($arr_file_name[key($arr_file_name)]);
            $file_name = implode('_', $arr_file_name).'.'.$file_type;

            $new_file_name = time().str_replace(" ","",$file_name);

            $config['file_name']            = $new_file_name;

            $config['upload_path']          = './upload/accounting/'.strtoupper($company_code).'/manual_bank_statement/';

            $config['allowed_types']        = '*';

            $config['max_size']             = '102400';

            $config['overwrite']            = TRUE;

            if (!is_dir($config['upload_path'])) {

                mkdir($config['upload_path'], 0777, TRUE);

            }

            //upload image

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file_name')) {

                $input_data['file_name'] = $new_file_name;
                $input_data['uploaded_at'] = date('Y-m-d H:i:s');

                $this->bank_statement_upload_model->add($input_data);
                $this->__set_flash_message('File Upload is created successfully');
                redirect('bank_statement_upload');
                return;


            }

        }

    }

    public function delete()
    {

        if($this->input_data) {
            $this->load->model('bank_statement_upload_model');

            $input_data = $this->input_data['post'];

            $this->load->model('company_model');
            $company_data = $this->company_model->find(1);

            $company_code = $company_data['company_id'];

            $record = $this->bank_statement_upload_model->find($input_data['id']);
            $old_file_name = $record['file_name'];
            // Delete file
            unlink('./upload/accounting/'.strtoupper($company_code).'/manual_bank_statement/'.$old_file_name);

            $this->bank_statement_upload_model->delete($input_data['id']);
            $this->__set_flash_message('File Upload is deleted successfully');
            redirect('bank_statement_upload');
            return;

        }
    }

    public function check_add()
    {
        if($this->input_data) {

            $input_data = $this->input_data['post'];
            $this->load->model('bank_statement_upload_model');

            $sql = "SELECT * FROM bank_statement_upload
            WHERE deleted_at IS NULL AND month = '" . $input_data['month'] . "' AND bank_account_id = '" . $input_data['bank_account_id'] . "'";

            $bank_statement_uploaded = $this->db->query($sql)->result_array();

            if (!empty($bank_statement_uploaded))
            {
                echo json_encode([
                    'error' => TRUE,
                    'message' => 'Month and Bank Account is duplicated'
                ]);
                die;
            } else {

                echo json_encode([
                    'error' => FALSE
                ]);
                die;
            }
        }
    }
}
