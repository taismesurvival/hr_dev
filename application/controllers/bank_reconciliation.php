<?php
class Bank_reconciliation extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';
    }

    public function index($id = null)
    {
        $data = $this->data;

        $input_data = $this->input_data['get'];

        $pagination = [
            'page' => (int) $input_data['p'] ? $input_data['p'] : 1,
            'limit' => (int) $input_data['limit'] ? $input_data['p'] : 20
        ];

        $pagination['offset'] = ($pagination['page'] - 1) * $pagination['limit'];

        $data['search'] = $pagination['search'] = $this->input_data['get'];

        $sql = "SELECT SQL_CALC_FOUND_ROWS id, name, number FROM bank_account 
        WHERE deleted_at IS NULL";

        $bank_accounts = $this->db->query($sql)->result_array();

        $data['bank_accounts'] = [
            '' => 'Please Select'
        ];
        foreach ($bank_accounts AS $account) {
            $data['bank_accounts'][$account['id']] = $account['name'].' ('.$account['number'].')';
        }

        $additional_where = '';
        if (!empty($data['search']['key_word'])) {
            $keyword = $data['search']['key_word'];
            if (strpos($keyword, 'BR') !== FALSE) {

                $id = explode('BR', $keyword)[1];

                if (!empty($id)) {
                    $additional_where .= " AND bank_reconciliation.id = $id";
                } else {
                    $additional_where .= " AND (bank_reconciliation.ref_no LIKE '%$keyword%' OR bank_account.name LIKE '%$keyword%' OR bank_account.number LIKE '%$keyword%')";
                }

            } else {
                $additional_where .= " AND (bank_reconciliation.ref_no LIKE '%$keyword%' OR bank_account.name LIKE '%$keyword%' OR bank_account.number LIKE '%$keyword%')";
            }
        }

        if (!empty($data['search']['account_type'])) {
            $account_type = $data['search']['account_type'];

            $additional_where .= " AND bank_account.type LIKE '%$account_type%'";
        }

        if (!empty($data['search']['bank_account'])) {
            $bank_account_id = $data['search']['bank_account'];

            $additional_where .= " AND bank_reconciliation.bank_account_id = $bank_account_id";
        }

        if (!empty($data['search']['minimum_amount'])) {
            $minimum_amount = $data['search']['minimum_amount'];

            $additional_where .= " AND bank_reconciliation.amount >= $minimum_amount";
        }

        if (!empty($data['search']['maximum_amount'])) {
            $maximum_amount = $data['search']['maximum_amount'];

            $additional_where .= " AND bank_reconciliation.amount <= $maximum_amount";
        }

        if (!empty($data['search']['document_date_from'])) {
            $from = $data['search']['document_date_from'];

            $additional_where .= " AND (bank_reconciliation.pay_date >= '$from')";
        }

        if (!empty($data['search']['document_date_to'])) {
            $to = $data['search']['document_date_to'];

            $additional_where .= " AND (bank_reconciliation.pay_date <= '$to')";
        }

        if (!empty($id)) {

            $additional_where .= " AND bank_reconciliation.id = $id";
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS bank_reconciliation.id, bank_reconciliation.bank_account_id, 
        bank_reconciliation.amount, bank_reconciliation.pay_date, bank_reconciliation.clear_date,
        bank_account.name, bank_account.number, bank_account.type, bank_reconciliation.ref_no, bank_reconciliation.type AS bank_recons_type,
        bank_reconciliation.payment_recons_GL_transaction_id, bank_reconciliation.payment_purchase_GL_transaction_id, purchase_entry.id AS purchase_entry_id
        FROM bank_reconciliation 
        LEFT JOIN bank_account ON bank_reconciliation.bank_account_id = bank_account.id
        LEFT JOIN purchase_entry ON purchase_entry.bank_reconciliation_id = bank_reconciliation.id
        WHERE bank_reconciliation.deleted_at IS NULL $additional_where GROUP BY bank_reconciliation.id LIMIT ".$pagination['offset'].", ".$pagination['limit'];

        $data['bank_reconciliations'] = $this->db->query($sql)->result_array();

        $pagination['total'] = $this->db->query('SELECT FOUND_ROWS() AS total;')->result_array()[0]['total'];

        $data['pagination'] = $pagination;

        $this->load->model('company_model');
        $company_data = $this->company_model->find(1);

        $data['locked_financial_month'] = $company_data['locked_financial_month'];

        return $this->template->loadView("bank_reconciliation/index", $data, "admin");
    }

    public function edit()
    {
        if($this->input_data) {
            $this->load->model('bank_reconciliation_model');

            $input_data = $this->input_data['post'];

            $this->bank_reconciliation_model->update($input_data['id'], $input_data);
            $this->__set_flash_message('The Statement is updated successfully');
            redirect('bank_reconciliation');
            return;

        }
    }

    public function undo_clear($bank_recons_id = null)
    {

        if (!empty($bank_recons_id)) {

            $this->load->model('bank_reconciliation_model');

            $this->bank_reconciliation_model->update($bank_recons_id, [
                'clear_date' => NULL
            ]);

            $this->__set_flash_message('The Statement is not clear anymore');

        }

        redirect('bank_reconciliation');
        return;
    }

    public function undo_payment($bank_recons_id = null)
    {

        if (!empty($bank_recons_id)) {

            $this->load->model('bank_reconciliation_model');
            $this->load->model('GL_transaction_model');
            $this->load->model('GL_transaction_project_split_model');
            $this->load->model('purchase_entry_model');

            $bank_reconciliation = $this->bank_reconciliation_model->find($bank_recons_id);

            $this->bank_reconciliation_model->delete($bank_recons_id);

            $this->GL_transaction_model->realDelete($bank_reconciliation['payment_recons_GL_transaction_id']);

            $purchase_payment_GL_transaction_array = json_decode($bank_reconciliation['payment_purchase_GL_transaction_id']);

            $additional = $bank_reconciliation['payment_recons_GL_transaction_id'];

            foreach ($purchase_payment_GL_transaction_array AS $item) {
                $this->GL_transaction_model->realDelete($item);

                $additional .= ', ' . $item;
            }

            $sql1 = "SELECT id FROM GL_transaction_project_split
        WHERE GL_transaction_id IN ($additional) AND deleted_at IS NULL";

            $GL_transactions_project_split = $this->db->query($sql1)->result_array();

            foreach ($GL_transactions_project_split AS $project_split) {
                $this->GL_transaction_project_split_model->realDelete($project_split['id']);
            }

            $sql2 = "SELECT id FROM purchase_entry
        WHERE bank_reconciliation_id = $bank_recons_id AND deleted_at IS NULL";

            $purchase_entries = $this->db->query($sql2)->result_array();

            foreach ($purchase_entries AS $purchase) {
                $this->purchase_entry_model->update($purchase['id'], [
                    'status' => 'pending',
                    'bank_reconciliation_id' => NULL
                ]);
            }

            $this->__set_flash_message('The Statement is not clear anymore');

        }

        redirect('bank_reconciliation');
        return;
    }
}
