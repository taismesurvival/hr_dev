<?php
class HGW_payment_method_mapping extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';
    }

    public function index()
    {
        $data = $this->data;

        $data['payment_methods'] = [];

        $data['search'] = $this->input_data['get'];

        $this->load->model('company_model');
        $company_data = $this->company_model->find(1);
        $pos_database = 'l12com_pos_' . strtolower($company_data['company_id']);
        $acc_database = 'l12com_acc_' . strtolower($company_data['company_id']);

        $master_database = $this->config->item("master_database");

        if (!empty($data['search']['outlet'])) {

            $call_api = new HungryGoWhere();

            // Try auth api
            $url = $call_api->server."auth";
            $params = $call_api->token;
            $sig = $call_api->get_signature($url, $params);
            $params['sig'] = $sig;

            $return = $call_api->get_page($url, 'post', $params);
            $data_return = json_decode($return, true);

            if ($data_return['status'] == 200) {

                $sql = "SELECT $master_database.HGW_restaurant_map.HGW_restaurant_id FROM $master_database.HGW_restaurant_map 
        LEFT JOIN $master_database.company ON $master_database.company.id = $master_database.HGW_restaurant_map.company_id
        WHERE $master_database.HGW_restaurant_map.outlet_id = '".$data['search']['outlet']."' AND $master_database.company.`database` = '$acc_database'";

                $HGW_restaurant_id = $this->db->query($sql)->result_array();

                if (!empty($HGW_restaurant_id)) {
                    $url = $call_api->server . $HGW_restaurant_id[0]['HGW_restaurant_id']."/pos/payment";
                    $params1['session_token'] = $data_return['data']['session_token'];
                    $sig = $call_api->get_signature($url, $params1);
                    $params1['sig'] = $sig;

                    $return = $call_api->get_page($url, 'get', $params1);
                    $data_return = json_decode($return, true);
                    if ($data_return['status'] == 200) {
                        $i = 0;
                        foreach ($data_return['data']['body'] AS $item) {

                            if (empty($item['deleted_at'])) {
                                $data['payment_methods'][$i]['HGW_payment_method_id'] = $item['id'];
                                $data['payment_methods'][$i]['HGW_payment_method_name'] = $item['name'];

                                $sql = "SELECT $pos_database.t_payment_method.name AS payment_method_name, $acc_database.HGW_payment_mapping.payment_method_id, $acc_database.HGW_payment_mapping.id FROM $acc_database.HGW_payment_mapping 
        LEFT JOIN $pos_database.t_payment_method ON $acc_database.HGW_payment_mapping.payment_method_id = $pos_database.t_payment_method.id
        WHERE $acc_database.HGW_payment_mapping.outlet_id = '".$data['search']['outlet']."' AND $acc_database.HGW_payment_mapping.HGW_payment_method_id = '".$item['id']."'";

                                $payment_method_data = $this->db->query($sql)->result_array();

                                if (!empty($payment_method_data)) {

                                    $data['payment_methods'][$i]['id'] = $payment_method_data[0]['id'];
                                    $data['payment_methods'][$i]['payment_method_name'] = $payment_method_data[0]['payment_method_name'];
                                    $data['payment_methods'][$i]['payment_method_id'] = $payment_method_data[0]['payment_method_id'];

                                }
                            }

                            $i++;

                        }
                    }


                }
            }
        }

        $sql = "SELECT id, name, code AS description, type
        FROM project
        WHERE deleted_at IS NULL AND status = 'active' AND type = 'outlet'";

        $data['project_list'] = $this->db->query($sql)->result_array();

        $data['project_selector'] = '<option value="">Please Select</option>';

        foreach ($data['project_list'] AS $item) {

            $selected = $data['search']['outlet'] == $item['id'] ? 'selected': '';

            $data['project_selector'] .= '<option '.$selected.' value="'.$item['id'].'">'.$item['name'].' ('.$item['description'].')</option>';
        }

        $sql = "SELECT $pos_database.t_payment_method.id, $pos_database.t_payment_method.name
        FROM $pos_database.t_payment_method
        WHERE $pos_database.t_payment_method.deleted_at IS NULL";

        $payments = $this->db->query($sql)->result_array();

        $data['payments'] = '<option value="">Please Select</option>';

        foreach ($payments AS $item) {

            $data['payments'] .= '<option value="'.$item['id'].'">'.$item['name'].'</option>';
        }

        return $this->template->loadView("HGW_payment_method_mapping/index", $data, "admin");
    }

    public function edit()
    {
        if($this->input_data) {

            $this->load->model('hgw_payment_mapping_model');

            $input_data = $this->input_data['post'];
            if (empty($input_data['payment_method_id'])) {
                $input_data['payment_method_id'] = NULL;
            }

            $id = $input_data['id'];
            unset($input_data['id']);

            if (!empty($id)) {

                $this->hgw_payment_mapping_model->update($id, $input_data);


            } else {

                $this->hgw_payment_mapping_model->add($input_data);

            }

            $this->__set_flash_message('HGW Payment Method is mapped successfully');
            redirect('HGW_payment_method_mapping?outlet='.$input_data['outlet_id']);
            return;
        }
    }
}
