<?php
class aging_report extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';
    }

    /**
     * index page, for showing list of data
     */
    public function AP_view(){

        $data = $this->data;

        $userdata = $this->session->userdata("user_data");

        $data['hide_bar'] = $userdata['role_id'] == 3 ? TRUE : FALSE;

        $data['check_active'] = $this->router->fetch_class().'/'.$this->router->fetch_method();

        $this->load->model('company_model');
        $company_data = $this->company_model->find(1);

        if($this->input_data) {
            $input_data = $this->input_data['post'];
            $data['company_name'] = $company_data['name'];

            $data['selected_date'] = $input_data['selected_date'];

            $selected_date = $input_data['selected_date'];

            $sql = "SELECT purchase_entry.id, purchase_entry.refference_no, purchase_entry.total_amount, purchase_entry.document_date,
            purchase_entry.supplier_id, supplier.name AS supplier_name, purchase_entry.status
            FROM purchase_entry
            LEFT JOIN supplier ON purchase_entry.supplier_id = supplier.id
            WHERE purchase_entry.status != 'done' AND purchase_entry.document_date <= '$selected_date' AND purchase_entry.deleted_at IS NULL AND purchase_entry.total_amount > 0
            AND purchase_entry.type = 'PURC' AND purchase_entry.separate_father_id IS NULL
            UNION SELECT credit_note.id, credit_note.refference_no, (credit_note.amount+credit_note.tax_amount)*(-1) AS total_amount, credit_note.document_date,
            credit_note.supplier_id, supplier.name AS supplier_name, credit_note.status
            FROM credit_note
            LEFT JOIN supplier ON credit_note.supplier_id = supplier.id
            WHERE credit_note.status = 'pending' AND credit_note.document_date <= '$selected_date' AND credit_note.deleted_at IS NULL
            ORDER BY supplier_id ASC, document_date ASC
            ";

            $data['purchases_not_done'] = $this->db->query($sql)->result_array();

            $sql = "SELECT total_amount, status, refference_no, supplier_id FROM purchase_entry
            WHERE separate_father_id IS NOT NULL AND deleted_at IS NULL";

            $children_purchase = $this->db->query($sql)->result_array();

            $data['unpaid_partial'] = [];
            foreach ($children_purchase AS $item) {

                if ($item['status'] == 'pending')
                $data['unpaid_partial'][$item['supplier_id']][$item['refference_no']] += $item['total_amount'];

            }

            return $this->template->loadView("aging_report/AP", $data);
        }

        return $this->template->loadView("aging_report/AP_view", $data, "admin");
    }
}
