<?php
class GL_entry extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
    }

    public function index()
    {
        $data = $this->data;

        $data['document_date_default'] = $this->session->userdata("document_date_default");

        $sql = "SELECT master_account.father_id, master_account.id, master_account.code, master_account.name, master_account.type, MC.id AS MC_id FROM master_account 
        LEFT JOIN master_account AS MC ON MC.father_id = master_account.id
        WHERE master_account.deleted_at IS NULL AND master_account.status = 'active' GROUP BY master_account.id ORDER BY master_account.code";

        $data['accounts'] = $this->db->query($sql)->result_array();

        $accounts_array = [];
        foreach ($data['accounts'] AS $account) {
            if (empty($account['MC_id'])) {
                $accounts_array[] = [
                    'value' => $account['code'],
                    'label' => $account['code'] . ' - ' . $account['name']
                ];
            }
        }

        $data['accounts_json'] = json_encode($accounts_array);

        $this->load->model('company_model');
        $company_data = $this->company_model->find(1);
        $l12_database = 'l12com_'.strtolower($company_data['company_id']);
        $accounting_database = 'l12com_acc_'.strtolower($company_data['company_id']);

        $data['locked_financial_month'] = $company_data['locked_financial_month'];
        $data['locked_next_financial_month'] = date('Y-m', strtotime("+1 month", strtotime($company_data['locked_financial_month'])));

        $sql = "SELECT id, name, code AS description, type
        FROM project
        WHERE deleted_at IS NULL AND status = 'active'";

        $data['project_list'] = $this->db->query($sql)->result_array();

        $data['project_selector'] = '<option value="">Please Select</option>';

        foreach ($data['project_list'] AS $item) {
            $data['project_selector'] .= '<option value="'.$item['id'].'">'.$item['name'].' ('.$item['description'].')</option>';
        }

        $sql = "SELECT id, name, code AS description
        FROM project
        WHERE deleted_at IS NULL AND status = 'active' AND type = 'outlet'";

        $data_projects = $this->db->query($sql)->result_array();

        if (count($data_projects) == 1) {
            $data['default_project_id'] = $data_projects[0]['id'];
            $data['default_project_name'] = $data_projects[0]['name'].' ('.$data_projects[0]['description'].')';
        }

        $input_data = $this->input_data['get'];

        $pagination = [
            'page' => (int) $input_data['p'] ? $input_data['p'] : 1,
            'limit' => (int) $input_data['limit'] ? $input_data['p'] : 100
        ];

        $pagination['offset'] = ($pagination['page'] - 1) * $pagination['limit'];

        $data['search'] = $pagination['search'] = $this->input_data['get'];

        $additional_where = '';

        if (!empty($data['search']['minimum_amount'])) {
            $minimum_amount = $data['search']['minimum_amount'];

            $additional_where .= " AND GL_entry.total_amount >= $minimum_amount";
        }

        if (!empty($data['search']['maximum_amount'])) {
            $maximum_amount = $data['search']['maximum_amount'];

            $additional_where .= " AND GL_entry.total_amount <= $maximum_amount";
        }

        if (!empty($data['search']['type'])) {
            $type = $data['search']['type'];

            $additional_where .= " AND GL_entry.type = '$type'";
        }

        if (!empty($data['search']['account_keyword'])) {
            $account_keyword = $data['search']['account_keyword'];

            $additional_where .= " AND (master_account.code LIKE '%$account_keyword%' OR master_account.name LIKE '%$account_keyword%')";
        }

        if (!empty($data['search']['keyword'])) {
            $keyword = $data['search']['keyword'];
            if (strpos($keyword, 'GL') !== FALSE) {

                $id = explode('GL', $keyword)[1];

                if (!empty($id)) {
                    $additional_where .= " AND GL_transaction_project_split.id = $id";
                } else {
                    $additional_where .= " AND (GL_entry.description LIKE '%$keyword%')";
                }

            } else if(strpos($keyword, 'GE') !== FALSE) {

                $id = explode('GE', $keyword)[1];

                if (!empty($id)) {
                    $additional_where .= " AND GL_entry.id = $id";
                } else {
                    $additional_where .= " AND (GL_entry.description LIKE '%$keyword%')";
                }

            } else {
                $additional_where .= " AND (GL_entry.description LIKE '%$keyword%')";
            }

        }

        if (!empty($data['search']['document_date_from'])) {
            $from = $data['search']['document_date_from'];

            $additional_where .= " AND (GL_entry.document_date >= '$from')";
        }

        if (!empty($data['search']['document_date_to'])) {
            $to = $data['search']['document_date_to'];

            $additional_where .= " AND (GL_entry.document_date <= '$to')";
        }

        if (empty($additional_where)) {

            $data['GL_entries'] = [];

            $pagination['total'] = 0;

        } else {
            $sql = "SELECT SQL_CALC_FOUND_ROWS GL_entry.* FROM GL_entry 
        LEFT JOIN GL_mapping ON GL_entry.id = GL_mapping.GL_entry_id
        INNER JOIN GL_transaction_project_split ON GL_mapping.GL_transaction_id = GL_transaction_project_split.GL_transaction_id
        INNER JOIN account_project ON account_project.id = GL_transaction_project_split.account_project_id
        INNER JOIN master_account ON account_project.account_id = master_account.id
        WHERE GL_entry.deleted_at IS NULL $additional_where group by GL_entry.id LIMIT " . $pagination['offset'] . ", " . $pagination['limit'];

            $data['GL_entries'] = $this->db->query($sql)->result_array();

            $pagination['total'] = $this->db->query('SELECT FOUND_ROWS() AS total;')->result_array()[0]['total'];
        }

        $data['pagination'] = $pagination;

        $sql = "SELECT GL_entry.id FROM GL_entry 
        LEFT JOIN GL_mapping ON GL_entry.id = GL_mapping.GL_entry_id
        LEFT JOIN sales_collection ON GL_mapping.GL_transaction_id = sales_collection.GL_transaction_id
        WHERE GL_entry.deleted_at IS NULL AND sales_collection.id IS NOT NULL group by GL_entry.id";

        $done_sales_collection = $this->db->query($sql)->result_array();

        $data['done_sales_collection'] = [];

        foreach ($done_sales_collection AS $item) {

            $data['done_sales_collection'][] = $item['id'];

        }

        $pos_db = 'l12com_pos_'.strtolower($company_data['company_id']);
        $sql = "SHOW DATABASES LIKE '$pos_db'";

        $res = $this->db->query($sql)->result_array();

        $data['show_pos'] = FALSE;
        if (!empty($res)) {
            $data['show_pos'] = TRUE;
        }

        $projects_array = [];
        foreach ($data['project_list'] AS $project) {
            $projects_array[] = [
                'value' => $project['name'].' ('.$project['description'].')',
                'label' => $project['name'].' ('.$project['description'].')',
                'id' => $project['id']
            ];
        }

        $data['projects_json'] = json_encode($projects_array);

        return $this->template->loadView("GL_entry/index", $data, "admin");
    }

    public function edit()
    {
        if($this->input_data) {
            $this->load->model('GL_entry_model');
            $this->load->model('GL_transaction_model');
            $this->load->model('GL_mapping_model');
            $this->load->model('GL_transaction_project_split_model');
            $this->load->model('bank_reconciliation_model');

            $input_data = $this->input_data['post'];

            if (!empty($input_data['id'])) {
                $this->GL_entry_model->update($input_data['id'], $input_data);
                $this->__set_flash_message('The GL entry is updated successfully');
            } else {
                $input_data['id'] = $this->GL_entry_model->add($input_data);
                $this->__set_flash_message('The GL entry is created successfully');
            }

            $this->session->set_userdata("document_date_default", $input_data['document_date']);

            $trans_ids_array = [];
            foreach ($input_data['transactions'] AS $transaction) {

                $transaction ['transaction_type'] = 'GL';

                $bank_reconciliation_id = !empty($transaction['id']) ? $this->GL_transaction_model->find($transaction['id'])['bank_reconciliation_id'] : NULL;

                if (($transaction['account_code'] >= 13031 && $transaction['account_code'] <= 13048 && $input_data['type'] != 'opening')
                    || ($transaction['account_code'] >= 13011 && $transaction['account_code'] <= 13029 && $input_data['type'] != 'opening')) {

                    $transaction['bank_reconciliation_id'] = $bank_reconciliation_id;

                    $sql = "SELECT id FROM bank_account 
                WHERE deleted_at IS NULL AND account_code = ".$transaction['account_code'];

                    $bank_account_id = $this->db->query($sql)->result_array()[0]['id'];

                    $bank_recons = [
                        'bank_account_id' => $bank_account_id,
                        'amount' => $transaction['amount'],
                        'ref_no' => $transaction['description'],
                        'pay_date' => $input_data['document_date'],
                        'type' => $transaction['type'] == 'credit' ? 'AP' : 'AR'
                    ];

                    if (!empty($transaction['id']) && !empty($transaction['bank_reconciliation_id'])) {

                        $this->bank_reconciliation_model->update($transaction['bank_reconciliation_id'], $bank_recons);

                    } else {
                        $transaction['bank_reconciliation_id'] = $this->bank_reconciliation_model->add($bank_recons);
                    }

                } else {

                    if (!empty($bank_reconciliation_id)) {
                        $this->bank_reconciliation_model->delete($bank_reconciliation_id);
                        $transaction['bank_reconciliation_id'] = NULL;
                    }

                }

                if (!empty($transaction['id'])) {
                    $this->GL_transaction_model->update($transaction['id'], $transaction);
                    $trans_ids_array[] = $transaction['id'];
                } else {
                    $transaction_id = $this->GL_transaction_model->add($transaction);
                    $this->GL_mapping_model->add([
                        'GL_transaction_id' => $transaction_id,
                        'GL_entry_id' => $input_data['id']
                    ]);
                    $trans_ids_array[] = $transaction_id;
                    $transaction['id'] = $transaction_id;
                }

                $project_ids_array = [];
                foreach ($transaction['project'] AS $project) {
                    $sql = "SELECT account_project.id FROM account_project 
        LEFT JOIN master_account ON account_project.account_id = master_account.id
        WHERE account_project.deleted_at IS NULL AND project_id = ".$project['project_id']." 
        AND master_account.code = ".$transaction['account_code'];

                    $account_project_id = $this->db->query($sql)->result_array();
                    $project ['account_project_id'] = $account_project_id[0]['id'];
                    $project ['GL_transaction_id'] = $transaction['id'];
                    $project ['document_date'] = $input_data['document_date'];

                    if (!empty($project['id'])) {
                        $this->GL_transaction_project_split_model->update($project['id'], $project);
                        $project_ids_array[] = $project['id'];
                    } else {
                        $project_id = $this->GL_transaction_project_split_model->add($project);
                        $project_ids_array[] = $project_id;
                    }
                }

                $split_ids = $this->GL_transaction_project_split_model->all(' AND GL_transaction_id = '.$transaction['id']);

                foreach ($split_ids AS $split_id) {
                    if (!in_array($split_id['id'], $project_ids_array)) {
                        $this->GL_transaction_project_split_model->realDelete($split_id['id']);
                    }
                }

            }

            $transaction_ids = $this->GL_mapping_model->all(' AND GL_entry_id = '.$input_data['id']);

            foreach ($transaction_ids AS $trans_id) {
                if (!in_array($trans_id['GL_transaction_id'], $trans_ids_array)) {

                    $bank_reconciliation_id = $this->GL_transaction_model->find($trans_id['GL_transaction_id'])['bank_reconciliation_id'];

                    if (!empty($bank_reconciliation_id)) {

                        $this->bank_reconciliation_model->delete($bank_reconciliation_id);

                    }

                    $this->GL_transaction_model->realDelete($trans_id['GL_transaction_id']);
                    $this->GL_mapping_model->realDelete($trans_id['id']);

                    $split_ids = $this->GL_transaction_project_split_model->all(' AND GL_transaction_id = '.$trans_id['GL_transaction_id']);

                    foreach ($split_ids AS $split_id) {
                        $this->GL_transaction_project_split_model->realDelete($split_id['id']);
                    }
                }
            }
            echo json_encode([
                'success' => TRUE
            ]);
            return;
        }
    }

    public function delete()
    {
        if($this->input_data) {
            $this->load->model('GL_entry_model');
            $this->load->model('GL_transaction_model');
            $this->load->model('GL_mapping_model');
            $this->load->model('bank_reconciliation_model');
            $this->load->model('GL_transaction_project_split_model');

            $input_data = $this->input_data['post'];

            $this->GL_entry_model->delete($input_data['id']);

            $sql = "SELECT id, GL_transaction_id FROM GL_mapping
        WHERE GL_entry_id = ".$input_data['id']." AND deleted_at IS NULL";

            $GL_transactions_id = $this->db->query($sql)->result_array();

            foreach ($GL_transactions_id AS $transaction) {

                $bank_reconciliation_id = $this->GL_transaction_model->find($transaction['GL_transaction_id'])['bank_reconciliation_id'];

                if (!empty($bank_reconciliation_id)) {

                    $this->bank_reconciliation_model->delete($bank_reconciliation_id);

                }

                $this->GL_mapping_model->realDelete($transaction['id']);
                $this->GL_transaction_model->realDelete($transaction['GL_transaction_id']);

                $sql1 = "SELECT id FROM GL_transaction_project_split
        WHERE GL_transaction_id = ".$transaction['GL_transaction_id']." AND deleted_at IS NULL";

                $GL_transactions_project_split = $this->db->query($sql1)->result_array();

                foreach ($GL_transactions_project_split AS $project_split) {
                    $this->GL_transaction_project_split_model->realDelete($project_split['id']);
                }
            }

            $this->__set_flash_message('The GL Entry is deleted successfully');
            redirect('GL_entry');
            return;

        }
    }

    public function get_detail()
    {
        if($this->input_data) {

            $input_data = $this->input_data['post'];

            $sql1 = "SELECT GL_mapping.GL_transaction_id, GL_transaction.type, GL_transaction.amount,
        GL_transaction.transaction_type, GL_transaction.description, master_account.code AS account_code,
        master_account.name AS account_name, master_account.type AS account_type, account_project.project_id,
        project.name AS project_name, project.code AS project_code,
        GL_transaction_project_split.amount AS project_amount, GL_transaction_project_split.percentage AS project_percentage,
        GL_transaction_project_split.id AS split_id, bank_reconciliation.clear_date, sales_collection.id AS sales_collection_id
        FROM GL_mapping
        LEFT JOIN sales_collection ON GL_mapping.GL_transaction_id = sales_collection.GL_transaction_id
        INNER JOIN GL_transaction ON GL_mapping.GL_transaction_id = GL_transaction.id
        LEFT JOIN GL_transaction_project_split ON GL_transaction.id = GL_transaction_project_split.GL_transaction_id
        LEFT JOIN bank_reconciliation ON bank_reconciliation.id = GL_transaction.bank_reconciliation_id
        INNER JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
        INNER JOIN project ON account_project.project_id =project.id
        INNER JOIN master_account ON account_project.account_id = master_account.id
        WHERE GL_mapping.GL_entry_id = ".$input_data['id']." AND GL_mapping.deleted_at IS NULL";

            $GL_transactions = $this->db->query($sql1)->result_array();

            $data['GL_transactions'] = [];
            foreach ($GL_transactions AS $GL_transaction) {
                if (!isset($data['GL_transactions'][$GL_transaction['GL_transaction_id']])) {
                    $data['GL_transactions'][$GL_transaction['GL_transaction_id']] = [
                        'GL_transaction_id' => $GL_transaction['GL_transaction_id'],
                        'account_code' => $GL_transaction['account_code'],
                        'account_name' => $GL_transaction['account_name'],
                        'account_type' => $GL_transaction['account_type'],
                        'amount' => $GL_transaction['amount'],
                        'description' => $GL_transaction['description'],
                        'transaction_type' => $GL_transaction['transaction_type'],
                        'type' => $GL_transaction['type'],
                        'clear_date' => $GL_transaction['clear_date'],
                        'sales_collection_id' => $GL_transaction['sales_collection_id']
                    ];
                }

                $data['GL_transactions'][$GL_transaction['GL_transaction_id']]['project'][] = [
                    'id' => $GL_transaction['split_id'],
                    'percentage' => $GL_transaction['project_percentage'],
                    'amount' => $GL_transaction['project_amount'],
                    'project_id' => $GL_transaction['project_id'],
                    'project_name' => $GL_transaction['project_name'].' ('.$GL_transaction['project_code'].')'
                ];
            }

            $sql2 = "SELECT GL_entry.type, GL_entry.document_date,
        GL_entry.attached_file, GL_entry.description, GL_entry.extra_data
        FROM GL_entry
        WHERE GL_entry.id = ".$input_data['id']." AND GL_entry.deleted_at IS NULL";

            $data['GL_entry'] = $this->db->query($sql2)->result_array()[0];

            echo json_encode($data);
            die;
        }
    }

    public function get_pos()
    {
        $this->load->model('company_model');
        $company_data = $this->company_model->find(1);
        $pos_database = 'l12com_pos_'.strtolower($company_data['company_id']);

        $pos_date = $this->input_data['post']['pos_date'];

        $sql = "SELECT description, code
        FROM master_account
        WHERE deleted_at IS NULL AND (code = '40010' OR code = '40020' OR code = '40030' OR code = '40510') AND status = 'active'";

        $sales_names_data = $this->db->query($sql)->result_array();

        $sales_names = [];
        foreach ($sales_names_data AS $name) {

            $sales_names[$name['code']] = $name['description'];

        }

        $sql2 = "SELECT ".$pos_database.".pos_order_service.id, ".$pos_database.".pos_order_service.discount_amount, ".$pos_database.".pos_order_service.tax_amount,
        ".$pos_database.".pos_order_service.outlet_id, ".$pos_database.".pos_order_payment.amount AS payment_amount,
        ".$pos_database.".t_payment_method.name AS payment_name, ".$pos_database.".pos_order_service.service_charge_amount, ".$pos_database.".pos_order_service.delivery_amount
        FROM ".$pos_database.".pos_order_service
        LEFT JOIN ".$pos_database.".pos_order_payment 
        ON ".$pos_database.".pos_order_service.id = ".$pos_database.".pos_order_payment.pos_order_service_id
        INNER JOIN ".$pos_database.".t_payment_method 
        ON ".$pos_database.".pos_order_payment.payment_method_id = ".$pos_database.".t_payment_method.id
        WHERE ".$pos_database.".pos_order_service.paid_at IS NOT NULL 
        AND DATE_FORMAT(".$pos_database.".pos_order_service.created_at,'%Y-%m-%d') = '$pos_date'";

        $orders = $this->db->query($sql2)->result_array();

        $outlets = [];
        foreach ($orders AS $item) {
            $outlets[$item['outlet_id']][$item['id']]['id'] = $item['id'];
            $outlets[$item['outlet_id']][$item['id']]['discount_amount'] = $item['discount_amount'];
            $outlets[$item['outlet_id']][$item['id']]['tax_amount'] = $item['tax_amount'];
            $outlets[$item['outlet_id']][$item['id']]['service_charge_amount'] = $item['service_charge_amount'];
            $outlets[$item['outlet_id']][$item['id']]['delivery_amount'] = $item['delivery_amount'];

            $outlets[$item['outlet_id']][$item['id']]['payment_split'][] = [
                'payment_amount' => $item['payment_amount'],
                'payment_name' => $item['payment_name']
            ];

        }

        $amount_for_oulets = [];
        foreach ($outlets AS $key => $item) {

            $outlet_tax_amount = 0;
            $outlet_discount_amount = 0;
            $outlet_delivery_amount = 0;
            $outlet_service_charge_amount = 0;
            $outlet_cash_amount = 0;
            $outlet_visa_amount = 0;
            $outlet_nets_amount = 0;
            $outlet_master_amount = 0;
            $outlet_american_amount = 0;
            $outlet_corporate_amount = 0;
            $outlet_internal_voucher_amount = 0;
            $outlet_external_voucher_amount = 0;
            $outlet_food_amount = 0;
            $outlet_beverage_amount = 0;
            $outlet_liquor_amount = 0;
            $outlet_others_amount = 0;
            $outlet_ubereats_amount = 0;
            $outlet_deliveroo_amount = 0;
            $outlet_foodpanda_amount = 0;
            $outlet_entertainment_amount = 0;
            $outlet_apple_pay_amount = 0;
            $outlet_android_pay_amount = 0;
            $outlet_alipay_amount = 0;
            $outlet_paynow_amount = 0;
            $outlet_fave_amount = 0;
            $outlet_adjustment_amount = 0;


            foreach ($item AS $key1 => $item1) {
                $outlet_discount_amount += $item1['discount_amount'];
                $outlet_tax_amount += $item1['tax_amount'];
                $outlet_service_charge_amount += $item1['service_charge_amount'];
                $outlet_delivery_amount += $item1['delivery_amount'];

                foreach ($item1['payment_split'] AS $payment) {
                    switch ($payment['payment_name']) {
                        case 'Cash':
                            $outlet_cash_amount += $payment['payment_amount'];
                            break;
                        case 'Visa Card':
                            $outlet_visa_amount += $payment['payment_amount'];
                            break;
                        case 'Nets':
                            $outlet_nets_amount += $payment['payment_amount'];
                            break;
                        case 'Internal Voucher':
                            $outlet_internal_voucher_amount += $payment['payment_amount'];
                            break;
                        case 'External Voucher':
                            $outlet_external_voucher_amount += $payment['payment_amount'];
                            break;
                        case 'Master Card':
                            $outlet_master_amount += $payment['payment_amount'];
                            break;
                        case 'American Express':
                            $outlet_american_amount += $payment['payment_amount'];
                            break;
                        case 'Corporate':
                            $outlet_corporate_amount += $payment['payment_amount'];
                            break;
                        case 'UberEATS':
                            $outlet_ubereats_amount += $payment['payment_amount'];
                            break;
                        case 'Deliveroo':
                            $outlet_deliveroo_amount += $payment['payment_amount'];
                            break;
                        case 'FoodPanda':
                            $outlet_foodpanda_amount += $payment['payment_amount'];
                            break;
                        case 'Entertainment':
                            $outlet_entertainment_amount += $payment['payment_amount'];
                            break;
                        case 'Apple Pay':
                            $outlet_apple_pay_amount += $payment['payment_amount'];
                            break;
                        case 'Android Pay':
                            $outlet_android_pay_amount += $payment['payment_amount'];
                            break;
                        case 'AliPay':
                            $outlet_alipay_amount += $payment['payment_amount'];
                            break;
                        case 'PayNow':
                            $outlet_paynow_amount += $payment['payment_amount'];
                            break;
                        case 'Fave':
                            $outlet_fave_amount += $payment['payment_amount'];
                            break;
                        case 'Adjustment':
                            $outlet_adjustment_amount += $payment['payment_amount'];
                            break;

                    }
                }

                // get items
                $sql = "SELECT ".$pos_database.".pos_order_list_item.id, ".$pos_database.".pos_order_list_item.subprice,
        ".$pos_database.".pos_item_category.name AS category_name
        FROM ".$pos_database.".pos_order_list_item
        LEFT JOIN ".$pos_database.".pos_item
        ON ".$pos_database.".pos_order_list_item.idItem = ".$pos_database.".pos_item.id
        INNER JOIN ".$pos_database.".pos_item_category 
        ON ".$pos_database.".pos_item.category = ".$pos_database.".pos_item_category.id
        WHERE ".$pos_database.".pos_order_list_item.pos_order_service_id =  ".$key1;

                $categories = $this->db->query($sql)->result_array();


                foreach ($categories AS $category) {
                    switch ($category['category_name']) {
                        case $sales_names['40010']:
                            $outlet_food_amount += $category['subprice'];
                            break;
                        case $sales_names['40020']:
                            $outlet_beverage_amount += $category['subprice'];
                            break;
                        case $sales_names['40030']:
                            $outlet_liquor_amount += $category['subprice'];
                            break;
                        case $sales_names['40510']:
                            $outlet_others_amount += $category['subprice'];
                            break;
                    }
                }
            }

            $amount_for_oulets[$key]['outlet_tax_amount'] = $outlet_tax_amount;
            $amount_for_oulets[$key]['outlet_discount_amount'] = $outlet_discount_amount;
            $amount_for_oulets[$key]['outlet_service_charge_amount'] = $outlet_service_charge_amount;
            $amount_for_oulets[$key]['outlet_delivery_amount'] = $outlet_delivery_amount;
            $amount_for_oulets[$key]['outlet_cash_amount'] = $outlet_cash_amount;
            $amount_for_oulets[$key]['outlet_visa_amount'] = $outlet_visa_amount;
            $amount_for_oulets[$key]['outlet_nets_amount'] = $outlet_nets_amount;
            $amount_for_oulets[$key]['outlet_internal_voucher_amount'] = $outlet_internal_voucher_amount;
            $amount_for_oulets[$key]['outlet_external_voucher_amount'] = $outlet_external_voucher_amount;
            $amount_for_oulets[$key]['outlet_master_amount'] = $outlet_master_amount;
            $amount_for_oulets[$key]['outlet_american_amount'] = $outlet_american_amount;
            $amount_for_oulets[$key]['outlet_corporate_amount'] = $outlet_corporate_amount;
            $amount_for_oulets[$key]['outlet_food_amount'] = $outlet_food_amount;
            $amount_for_oulets[$key]['outlet_beverage_amount'] = $outlet_beverage_amount;
            $amount_for_oulets[$key]['outlet_liquor_amount'] = $outlet_liquor_amount;
            $amount_for_oulets[$key]['outlet_others_amount'] = $outlet_others_amount;
            $amount_for_oulets[$key]['outlet_ubereats_amount'] = $outlet_ubereats_amount;
            $amount_for_oulets[$key]['outlet_deliveroo_amount'] = $outlet_deliveroo_amount;
            $amount_for_oulets[$key]['outlet_foodpanda_amount'] = $outlet_foodpanda_amount;
            $amount_for_oulets[$key]['outlet_entertainment_amount'] = $outlet_entertainment_amount;
            $amount_for_oulets[$key]['outlet_apple_pay_amount'] = $outlet_apple_pay_amount;
            $amount_for_oulets[$key]['outlet_android_pay_amount'] = $outlet_android_pay_amount;
            $amount_for_oulets[$key]['outlet_alipay_amount'] = $outlet_alipay_amount;
            $amount_for_oulets[$key]['outlet_paynow_amount'] = $outlet_paynow_amount;
            $amount_for_oulets[$key]['outlet_fave_amount'] = $outlet_fave_amount;
            $amount_for_oulets[$key]['outlet_adjustment_amount'] = $outlet_adjustment_amount;


        }
        foreach ($amount_for_oulets AS $key => $each_outlet) {

            $sql = "SELECT name, code
        FROM project
        WHERE id = $key";

            $project_name = $this->db->query($sql)->result_array();

            if ($each_outlet['outlet_tax_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '21401',
                    'account_name' => 'Output GST',
                    'amount' => $each_outlet['outlet_tax_amount'],
                    'description' => 'GST Total',
                    'transaction_type' => 'POS',
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_tax_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_service_charge_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '40110',
                    'account_name' => 'Sales - Service Charge',
                    'amount' => $each_outlet['outlet_service_charge_amount'],
                    'description' => 'Service Charge Total',
                    'transaction_type' => 'POS',
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_service_charge_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_delivery_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '40098',
                    'account_name' => 'Sales - Delivery Charges',
                    'amount' => $each_outlet['outlet_delivery_amount'],
                    'description' => 'Delivery Charge Total',
                    'transaction_type' => 'POS',
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_delivery_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_discount_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '40210',
                    'account_name' => 'Sales - Discounts',
                    'amount' => $each_outlet['outlet_discount_amount'],
                    'description' => 'Overall Discount Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_discount_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_cash_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '14005',
                    'account_name' => 'Undeposited Cash from Sales',
                    'amount' => $each_outlet['outlet_cash_amount'],
                    'description' => 'Cash Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_cash_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_visa_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '14002',
                    'account_name' => 'Trade Debtor - Visa Cards',
                    'amount' => $each_outlet['outlet_visa_amount'],
                    'description' => 'Visa Card Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_visa_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_nets_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '14004',
                    'account_name' => 'Trade Debtor - NETS',
                    'amount' => $each_outlet['outlet_nets_amount'],
                    'description' => 'Nets Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_nets_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_internal_voucher_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '14014',
                    'account_name' => 'Trade Debtor - Internal Vouchers',
                    'amount' => $each_outlet['outlet_internal_voucher_amount'],
                    'description' => 'Internal Voucher Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_internal_voucher_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_external_voucher_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '14013',
                    'account_name' => 'Trade Debtor - External Vouchers',
                    'amount' => $each_outlet['outlet_external_voucher_amount'],
                    'description' => 'External Voucher Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_external_voucher_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_master_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '14003',
                    'account_name' => 'Trade Debtor - Master Cards',
                    'amount' => $each_outlet['outlet_master_amount'],
                    'description' => 'Master Card Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_master_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_american_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '14001',
                    'account_name' => 'Trade Debtor - American Express',
                    'amount' => $each_outlet['outlet_american_amount'],
                    'description' => 'American Express Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_american_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_corporate_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '14008',
                    'account_name' => 'Trade Debtor - Corporate',
                    'amount' => $each_outlet['outlet_corporate_amount'],
                    'description' => 'Corporate Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_corporate_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_food_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '40010',
                    'account_name' => 'Sales - '.$sales_names['40010'],
                    'amount' => $each_outlet['outlet_food_amount'],
                    'description' => $sales_names['40010'].' Sales Total',
                    'transaction_type' => 'POS',
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_food_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_beverage_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '40020',
                    'account_name' => 'Sales - '.$sales_names['40020'],
                    'amount' => $each_outlet['outlet_beverage_amount'],
                    'description' => $sales_names['40020'].' Sales Total',
                    'transaction_type' => 'POS',
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_beverage_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_liquor_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '40030',
                    'account_name' => 'Sales - '.$sales_names['40030'],
                    'amount' => $each_outlet['outlet_liquor_amount'],
                    'description' => $sales_names['40030'].' Sales Total',
                    'transaction_type' => 'POS',
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_liquor_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_others_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '40510',
                    'account_name' => 'Sales - '.$sales_names['40510'],
                    'amount' => $each_outlet['outlet_others_amount'],
                    'description' => $sales_names['40510'].' Sales Total',
                    'transaction_type' => 'POS',
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_others_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_ubereats_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '14017',
                    'account_name' => 'Trade Debtor - UberEATS',
                    'amount' => $each_outlet['outlet_ubereats_amount'],
                    'description' => 'UberEATS Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_ubereats_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_deliveroo_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '14018',
                    'account_name' => 'Trade Debtor - Deliveroo',
                    'amount' => $each_outlet['outlet_deliveroo_amount'],
                    'description' => 'Deliveroo Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_deliveroo_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_foodpanda_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '14019',
                    'account_name' => 'Trade Debtor - FoodPanda',
                    'amount' => $each_outlet['outlet_foodpanda_amount'],
                    'description' => 'FoodPanda Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_foodpanda_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_entertainment_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '14020',
                    'account_name' => 'Trade Debtor - Entertainment',
                    'amount' => $each_outlet['outlet_entertainment_amount'],
                    'description' => 'Entertainment Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_entertainment_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_apple_pay_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '14021',
                    'account_name' => 'Trade Debtor - Apple Pay',
                    'amount' => $each_outlet['outlet_apple_pay_amount'],
                    'description' => 'Apple Pay Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_apple_pay_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_android_pay_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '14022',
                    'account_name' => 'Trade Debtor - Android Pay',
                    'amount' => $each_outlet['outlet_android_pay_amount'],
                    'description' => 'Android Pay Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_android_pay_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_alipay_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '14023',
                    'account_name' => 'Trade Debtor - AliPay',
                    'amount' => $each_outlet['outlet_alipay_amount'],
                    'description' => 'AliPay Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_alipay_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_paynow_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '14024',
                    'account_name' => 'Trade Debtor - PayNow',
                    'amount' => $each_outlet['outlet_paynow_amount'],
                    'description' => 'PayNow Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_paynow_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_fave_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '14025',
                    'account_name' => 'Trade Debtor - Fave',
                    'amount' => $each_outlet['outlet_fave_amount'],
                    'description' => 'Fave Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_fave_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }

            if ($each_outlet['outlet_adjustment_amount'] > 0) {
                $data['GL_transactions'][] = [
                    'account_code' => '40220',
                    'account_name' => 'Sales - Rounding',
                    'amount' => $each_outlet['outlet_adjustment_amount'],
                    'description' => 'Adjustment Total',
                    'transaction_type' => 'POS',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $each_outlet['outlet_adjustment_amount'],
                        'project_id' => $key,
                        'project_name' => $project_name[0]['name'].' ('.$project_name[0]['code'].')'
                    ]
                    ]
                ];
            }


        }
        echo json_encode($data);
        die;
    }

    public function get_l12()
    {
        $this->load->model('company_model');
        $company_data = $this->company_model->find(1);
        $pos_database = 'l12com_'.strtolower($company_data['company_id']);
        $acc_database = 'l12com_acc_'.strtolower($company_data['company_id']);

        $salary_date = $this->input_data['post']['salary_date'].'-01';

        $sql2 = "SELECT
	a.employee_id, SUM(a.gross_wage) AS gross_wage, SUM(a.contribution_cpf_employer_ordinary) AS contribution_cpf_employer_ordinary,
    SUM(a.contribution_sdl) AS contribution_sdl, SUM(a.contribution_fwl) AS contribution_fwl, emp.code AS employee_code,
    emp.full_name AS employee_name, emp_e.company_structure_1 AS outlet_id, pj.type, i.code AS outlet_code, i.description AS outlet_description
        FROM
    ".$pos_database.".payroll_process_employee a
        LEFT JOIN
    ".$pos_database.".payroll_process pay_p ON a.payroll_process_id = pay_p.id
        LEFT JOIN
    ".$pos_database.".employee emp ON a.employee_id = emp.id
        LEFT JOIN
    ".$pos_database.".employee_employment emp_e ON emp.id = emp_e.id
        LEFT JOIN
    ".$pos_database.".employee_payment_settings emp_ps ON a.employee_id = emp_ps.employee_id
    LEFT JOIN
    ".$acc_database.".project pj ON emp_e.company_structure_1 = pj.id
        LEFT JOIN
    ".$pos_database.".company_structure_1 i ON i.id = emp_e.company_structure_1
        WHERE
    emp.deleted_at IS NULL
    AND emp_ps.deleted_at IS NULL
    AND pj.status = 'active'
    AND (DATE_FORMAT(CONCAT_WS('-', pay_p.year, pay_p.month, '01'),
        '%Y-%m-%d') BETWEEN '$salary_date' AND LAST_DAY('$salary_date'))
        AND IF(emp_e.last_day_work IS NOT NULL,
        emp_e.last_day_work >= '$salary_date',
        emp_e.last_day_work IS NULL)
        GROUP BY emp_e.company_structure_1 , a.id
        ORDER BY emp_e.company_structure_1 , emp.code ASC";

        $salary_list = $this->db->query($sql2)->result_array();

        $GL_trans = [];
        $total_gross_wage_outlet = 0;
        $total_CPF_outlet = 0;
        $total_SDL_outlet = 0;
        $total_FWL_outlet = 0;

        $total_gross_wage_operating = 0;
        $total_CPF_operating = 0;
        $total_SDL_operating = 0;
        $total_FWL_operating = 0;

        foreach ($salary_list AS $item)  {

            if ($item['type'] == 'outlet') {
                $total_gross_wage_outlet += $item['gross_wage'];
                $total_CPF_outlet += $item['contribution_cpf_employer_ordinary'];
                $total_SDL_outlet += $item['contribution_sdl'];
                $total_FWL_outlet += $item['contribution_fwl'];
            } else {
                $total_gross_wage_operating += $item['gross_wage'];
                $total_CPF_operating += $item['contribution_cpf_employer_ordinary'];
                $total_SDL_operating += $item['contribution_sdl'];
                $total_FWL_operating += $item['contribution_fwl'];
            }

            if (!isset($GL_trans[$item['outlet_id']])) {
                $GL_trans[$item['outlet_id']] = [
                    'gross_wage' => $item['gross_wage'],
                    'CPF' => $item['contribution_cpf_employer_ordinary'],
                    'SDL' => $item['contribution_sdl'],
                    'FWL' => $item['contribution_fwl'],
                    'type' => $item['type'],
                    'outlet_code' => $item['outlet_code'],
                    'outlet_description' => $item['outlet_description']
                ];
            } else {
                $GL_trans[$item['outlet_id']]['gross_wage'] += $item['gross_wage'];
                $GL_trans[$item['outlet_id']]['CPF'] += $item['contribution_cpf_employer_ordinary'];
                $GL_trans[$item['outlet_id']]['SDL'] += $item['contribution_sdl'];
                $GL_trans[$item['outlet_id']]['FWL'] += $item['contribution_fwl'];
            }
        }

        if ($total_gross_wage_outlet > 0) {
            $data['GL_transactions']['outlet_gross_wage'] = [
                'account_code' => '50201',
                'account_name' => 'Direct Outlet Staff Salaries & Wages',
                'amount' => $total_gross_wage_outlet,
                'description' => 'Gross Wage Total for Outlets',
                'transaction_type' => 'salary',
                'type' => 'debit',
                'project' => []
            ];
        }

        if ($total_gross_wage_operating > 0) {
            $data['GL_transactions']['operating_gross_wage'] = [
                'account_code' => '60101',
                'account_name' => 'Staff Salary & Wages',
                'amount' => $total_gross_wage_operating,
                'description' => 'Gross Wage Total for Operating',
                'transaction_type' => 'salary',
                'type' => 'debit',
                'project' => []
            ];
        }

        if (($total_gross_wage_operating+$total_gross_wage_outlet) > 0) {
            $data['GL_transactions']['accruals_gross_wage'] = [
                'account_code' => '21601',
                'account_name' => 'Accruals - Salary Wages',
                'amount' => $total_gross_wage_operating+$total_gross_wage_outlet,
                'description' => 'Salary Accruals Total',
                'transaction_type' => 'salary',
                'type' => 'credit',
                'project' => []
            ];
        }

        if ($total_CPF_outlet > 0) {
            $data['GL_transactions']['outlet_CPF'] = [
                'account_code' => '50202',
                'account_name' => 'Direct Outlet Staff CPF',
                'amount' => $total_CPF_outlet,
                'description' => 'CPF Total for Outlets',
                'transaction_type' => 'salary',
                'type' => 'debit',
                'project' => []
            ];
        }

        if ($total_CPF_operating > 0) {
            $data['GL_transactions']['operating_CPF'] = [
                'account_code' => '60102',
                'account_name' => 'Staff CPF',
                'amount' => $total_CPF_operating,
                'description' => 'CPF Total for Operating',
                'transaction_type' => 'salary',
                'type' => 'debit',
                'project' => []
            ];
        }

        if (($total_CPF_operating+$total_CPF_outlet) > 0) {
            $data['GL_transactions']['accruals_CPF'] = [
                'account_code' => '21605',
                'account_name' => 'Accruals - CPF',
                'amount' => $total_CPF_operating+$total_CPF_outlet,
                'description' => 'CPF Accruals Total',
                'transaction_type' => 'salary',
                'type' => 'credit',
                'project' => []
            ];
        }

        if ($total_SDL_outlet > 0) {
            $data['GL_transactions']['outlet_SDL'] = [
                'account_code' => '50203',
                'account_name' => 'Direct Outlet SDL',
                'amount' => $total_SDL_outlet,
                'description' => 'SDL Total for Outlets',
                'transaction_type' => 'salary',
                'type' => 'debit',
                'project' => []
            ];
        }

        if ($total_SDL_operating > 0) {
            $data['GL_transactions']['operating_SDL'] = [
                'account_code' => '60103',
                'account_name' => 'Staff Skill Development Levy (SDL)',
                'amount' => $total_SDL_operating,
                'description' => 'SDL Total for Operating',
                'transaction_type' => 'salary',
                'type' => 'debit',
                'project' => []
            ];
        }

        if (($total_SDL_operating+$total_SDL_outlet) > 0) {

            $data['GL_transactions']['accruals_SDL'] = [
                'account_code' => '21607',
                'account_name' => 'Accruals - FWL SDL',
                'amount' => $total_SDL_operating+$total_SDL_outlet,
                'description' => 'SDL Accruals Total',
                'transaction_type' => 'salary',
                'type' => 'credit',
                'project' => []
            ];
        }

        if ($total_FWL_outlet > 0) {
            $data['GL_transactions']['outlet_FWL'] = [
                'account_code' => '50204',
                'account_name' => 'Direct Outlet FWL',
                'amount' => $total_FWL_outlet,
                'description' => 'FWL Total for Outlets',
                'transaction_type' => 'salary',
                'type' => 'debit',
                'project' => []
            ];
        }

        if ($total_FWL_operating > 0) {
            $data['GL_transactions']['operating_FWL'] = [
                'account_code' => '60104',
                'account_name' => 'Staff Foreign Worker Levy (FWL)',
                'amount' => $total_FWL_operating,
                'description' => 'FWL Total for Operating',
                'transaction_type' => 'salary',
                'type' => 'debit',
                'project' => []
            ];
        }

        if (($total_FWL_operating+$total_FWL_outlet) > 0) {
            $data['GL_transactions']['accruals_FWL'] = [
                'account_code' => '21607',
                'account_name' => 'Accruals - FWL SDL',
                'amount' => $total_FWL_operating+$total_FWL_outlet,
                'description' => 'FWL Accruals Total',
                'transaction_type' => 'salary',
                'type' => 'credit',
                'project' => []
            ];
        }

        foreach ($GL_trans AS $key => $each_outlet) {

            if ($each_outlet['type'] == 'outlet' && $each_outlet['gross_wage'] > 0) {
                $data['GL_transactions']['outlet_gross_wage']['project'][] = [
                    'percentage' => round($each_outlet['gross_wage']/$total_gross_wage_outlet*100, 2),
                    'amount' => $each_outlet['gross_wage'],
                    'project_id' => $key
                ];
            }

            if ($each_outlet['type'] == 'operating' && $each_outlet['gross_wage'] > 0) {
                $data['GL_transactions']['operating_gross_wage']['project'][] = [
                    'percentage' => round($each_outlet['gross_wage']/$total_gross_wage_operating*100, 2),
                    'amount' => $each_outlet['gross_wage'],
                    'project_id' => $key
                ];
            }

            if ($each_outlet['gross_wage'] > 0) {
                $data['GL_transactions']['accruals_gross_wage']['project'][] = [
                    'percentage' => round($each_outlet['gross_wage']/($total_gross_wage_outlet+$total_gross_wage_operating)*100, 2),
                    'amount' => $each_outlet['gross_wage'],
                    'project_id' => $key
                ];
            }

            if ($each_outlet['type'] == 'outlet' && $each_outlet['CPF'] > 0) {
                $data['GL_transactions']['outlet_CPF']['project'][] = [
                    'percentage' => round($each_outlet['CPF']/$total_CPF_outlet*100, 2),
                    'amount' => $each_outlet['CPF'],
                    'project_id' => $key
                ];
            }

            if ($each_outlet['type'] == 'operating' && $each_outlet['CPF'] > 0) {
                $data['GL_transactions']['operating_CPF']['project'][] = [
                    'percentage' => round($each_outlet['CPF']/$total_CPF_operating*100, 2),
                    'amount' => $each_outlet['CPF'],
                    'project_id' => $key
                ];
            }

            if ($each_outlet['CPF'] > 0) {
                $data['GL_transactions']['accruals_CPF']['project'][] = [
                    'percentage' => round($each_outlet['CPF']/($total_CPF_outlet+$total_CPF_operating)*100, 2),
                    'amount' => $each_outlet['CPF'],
                    'project_id' => $key
                ];
            }

            if ($each_outlet['type'] == 'outlet' && $each_outlet['SDL'] > 0) {
                $data['GL_transactions']['outlet_SDL']['project'][] = [
                    'percentage' => round($each_outlet['SDL']/$total_SDL_outlet*100, 2),
                    'amount' => $each_outlet['SDL'],
                    'project_id' => $key
                ];
            }

            if ($each_outlet['type'] == 'operating' && $each_outlet['SDL'] > 0) {
                $data['GL_transactions']['operating_SDL']['project'][] = [
                    'percentage' => round($each_outlet['SDL']/$total_SDL_operating*100, 2),
                    'amount' => $each_outlet['SDL'],
                    'project_id' => $key
                ];
            }

            if ($each_outlet['SDL'] > 0) {
                $data['GL_transactions']['accruals_SDL']['project'][] = [
                    'percentage' => round($each_outlet['SDL']/($total_SDL_outlet+$total_SDL_operating)*100, 2),
                    'amount' => $each_outlet['SDL'],
                    'project_id' => $key
                ];
            }

            if ($each_outlet['type'] == 'outlet' && $each_outlet['FWL'] > 0) {
                $data['GL_transactions']['outlet_FWL']['project'][] = [
                    'percentage' => round($each_outlet['FWL']/$total_FWL_outlet*100, 2),
                    'amount' => $each_outlet['FWL'],
                    'project_id' => $key
                ];
            }

            if ($each_outlet['type'] == 'operating' && $each_outlet['FWL'] > 0) {
                $data['GL_transactions']['operating_FWL']['project'][] = [
                    'percentage' => round($each_outlet['FWL']/$total_FWL_operating*100, 2),
                    'amount' => $each_outlet['FWL'],
                    'project_id' => $key
                ];
            }

            if ($each_outlet['FWL'] > 0) {
                $data['GL_transactions']['accruals_FWL']['project'][] = [
                    'percentage' => round($each_outlet['FWL']/($total_FWL_outlet+$total_FWL_operating)*100, 2),
                    'amount' => $each_outlet['FWL'],
                    'project_id' => $key
                ];
            }

        }

        echo json_encode($data);
        die;
    }

    public function get_account_name()
    {
        if($this->input_data) {

            $input_data = $this->input_data['post'];

            $account_code = $input_data['account_code'];

            $sql = "SELECT master_account.name FROM master_account 
        LEFT JOIN master_account AS MC ON MC.father_id = master_account.id
        WHERE master_account.code = '$account_code' AND master_account.deleted_at IS NULL AND master_account.status = 'active' AND MC.id IS NULL GROUP BY master_account.id ORDER BY master_account.code";

            $account_name = $this->db->query($sql)->result_array();

            if (!empty($account_name)) {
                echo json_encode($account_name[0]['name']);
            } else {
                echo json_encode('This Account Code does not exist or cannot be selected');
            }
            die;
        }
    }

    public function check_edit()
    {
        if($this->input_data) {

            $input_data = $this->input_data['post'];

            $code_not_exist = [];
            foreach ($input_data['account_codes'] AS $code) {

                $sql = "SELECT master_account.name FROM master_account 
        LEFT JOIN master_account AS MC ON MC.father_id = master_account.id
        WHERE master_account.code = '$code' AND master_account.deleted_at IS NULL AND master_account.status = 'active' AND MC.id IS NULL GROUP BY master_account.id ORDER BY master_account.code";

                $account_name = $this->db->query($sql)->result_array();

                if (empty($account_name)) {
                    $code_not_exist[] = $code;
                }
            }
            if (!empty($code_not_exist)) {
                echo json_encode([
                    'error' => TRUE,
                    'codes' => $code_not_exist
                ]);
                die;
            } else {

                echo json_encode([
                    'error' => FALSE
                ]);
                die;
            }
        }
    }

    public function check_opening()
    {
        $sql = "SELECT id FROM GL_entry 
        WHERE type = 'opening' AND deleted_at IS NULL";

        $GL_entry = $this->db->query($sql)->result_array();

        if (!empty($GL_entry)) {
            echo json_encode([
                'id' => $GL_entry[0]['id']
            ]);
            die;
        } else {

            echo json_encode([
                'id' => NULL
            ]);
            die;

        }
    }
}
