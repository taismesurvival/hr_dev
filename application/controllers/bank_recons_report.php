<?php
class bank_recons_report extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';

    }

    /**
     * index page, for showing list of data
     */
    public function view(){

        $data = $this->data;

        $this->load->model('company_model');
        $this->load->model('bank_account_model');
        $company_data = $this->company_model->find(1);

        if($this->input_data) {
            $input_data = $this->input_data['post'];

            $data['company_name'] = $company_data['name'];

            $last_day_of_the_month = date('Y-m-d', strtotime($input_data['month'].'-01 +1 month -1 day'));
            $first_day_of_the_month = $input_data['month'].'-01';

            $data['ending_month'] = date_format(date_create($last_day_of_the_month),"Y F d");

            $data['bank_account'] = $this->bank_account_model->find($input_data['bank_account_id']);

            $sql = "SELECT GL_transaction_project_split.amount, GL_transaction.type
    FROM GL_transaction_project_split
    LEFT JOIN account_project ON GL_transaction_project_split.account_project_id = account_project.id
    LEFT JOIN master_account ON account_project.account_id = master_account.id
    LEFT JOIN GL_transaction ON GL_transaction_project_split.GL_transaction_id = GL_transaction.id
    WHERE master_account.code = '".$data['bank_account']['account_code']."'
    AND GL_transaction_project_split.document_date <= '$last_day_of_the_month'";

            $bank_account_trans = $this->db->query($sql)->result_array();
            $data['bank_account_trans_total'] = 0;
            foreach ($bank_account_trans AS $tran) {
                if ($tran['type'] == 'debit') {
                    $data['bank_account_trans_total'] += $tran['amount'];
                } else {
                    $data['bank_account_trans_total'] -= $tran['amount'];
                }
            }

            $sql = "SELECT amount, ref_no, pay_date,
    clear_date, id
    FROM bank_reconciliation
    WHERE type = 'AP' AND bank_account_id = '".$input_data['bank_account_id']."'
    AND clear_date IS NOT NULL AND deleted_at IS NULL
    AND clear_date <= '$last_day_of_the_month' AND clear_date >= '$first_day_of_the_month'";

            $data['reconciled_cheques'] = $this->db->query($sql)->result_array();

            $sql = "SELECT amount, ref_no, pay_date,
    clear_date, id
    FROM bank_reconciliation
    WHERE type = 'AR' AND bank_account_id = '".$input_data['bank_account_id']."'
    AND clear_date IS NOT NULL AND deleted_at IS NULL
    AND clear_date <= '$last_day_of_the_month' AND clear_date >= '$first_day_of_the_month'";

            $data['reconciled_deposits'] = $this->db->query($sql)->result_array();

            $sql = "SELECT amount, ref_no, pay_date, id
    FROM bank_reconciliation
    WHERE type = 'AP' AND bank_account_id = '".$input_data['bank_account_id']."'
    AND clear_date IS NULL AND deleted_at IS NULL
    AND pay_date <= '$last_day_of_the_month'";

            $data['outstanding_cheques'] = $this->db->query($sql)->result_array();

            $sql = "SELECT amount, ref_no, pay_date, id
    FROM bank_reconciliation
    WHERE type = 'AR' AND bank_account_id = '".$input_data['bank_account_id']."'
    AND clear_date IS NULL AND deleted_at IS NULL
    AND pay_date <= '$last_day_of_the_month'";

            $data['outstanding_deposits'] = $this->db->query($sql)->result_array();

            return $this->template->loadView("bank_recons_report/report", $data);

        }

        $sql = "SELECT id, name, number, account_code FROM bank_account 
        WHERE deleted_at IS NULL AND type = 'bank'";

        $bank_accounts = $this->db->query($sql)->result_array();

        $data['bank_accounts_selector'] = '<option selected value="">Please Select</option>';
        foreach ($bank_accounts AS $item) {

            if (!empty($item['number'])) {
                $data['bank_accounts_selector'] .= '<option value="' . $item['id'] . '">' . $item['name'] . ' (' . $item['number'] . ')</option>';
            } else {
                $data['bank_accounts_selector'] .= '<option value="' . $item['id'] . '">' . $item['name']. '</option>';
            }
        }

        return $this->template->loadView("bank_recons_report/view", $data, "admin");
    }
}
