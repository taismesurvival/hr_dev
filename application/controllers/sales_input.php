<?php
class sales_input extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';
    }

    /**
     * index page, for showing list of data
     */
    public function index($id = null){

        $userdata = $this->session->userdata("user_data");

        if (!empty($id)) {

            $outlet_ids = json_decode($userdata['outlet_id'], true);

            if (!empty($userdata['outlet_id']) && !in_array($id, $outlet_ids)) {
                redirect('sales_input/select_outlet');
                return;
            }

            $data = $this->data;

            $this->load->model('company_model');
            $company_data = $this->company_model->find(1);

            $data['locked_financial_month'] = $company_data['locked_financial_month'];
            $data['locked_next_financial_month'] = date('Y-m', strtotime("+1 month", strtotime($company_data['locked_financial_month'])));

            $data['editable'] = $userdata['role_id'] == 1 ? 'can_edit' : 'cannot';

            $sql = "SELECT id, name, code, type
        FROM project
        WHERE deleted_at IS NULL AND id = $id AND status = 'active'";

            $data['project_list'] = $this->db->query($sql)->result_array();

            $sql = "SELECT description, code
        FROM master_account
        WHERE deleted_at IS NULL AND (code = '40010' OR code = '40020' OR code = '40030' OR code = '40510') AND status = 'active'";

            $sales_names = $this->db->query($sql)->result_array();

            $data['sales_names'] = [];
            foreach ($sales_names AS $name) {

                $data['sales_names'][$name['code']] = $name['description'];

            }

            $sql = "SELECT description, code
        FROM master_account
        WHERE deleted_at IS NULL AND (code = '14005' OR code = '14002' OR code = '14004' OR code = '14014'
        OR code = '14013' OR code = '14003' OR code = '14001' OR code = '14008' OR code = '14017'
         OR code = '14018' OR code = '14019') AND status = 'active'";

            $debtor_names = $this->db->query($sql)->result_array();

            $data['debtor_names'] = [];
            foreach ($debtor_names AS $name) {

                $data['debtor_names'][$name['code']] = str_replace(' from Sales', '', str_replace('Undeposited ', '', str_replace('Trade Debtor - ', '', $name['description'])));

            }

            return $this->template->loadView("sales_input/index", $data);
        } else {

            redirect('sales_input/select_outlet');
            return;

        }

    }

    public function select_outlet ()
    {

        $userdata = $this->session->userdata("user_data");

        if (!empty($userdata['outlet_id']) || $userdata['role_id'] == 1) {
            $additional = '';

            if (!empty($userdata['outlet_id'])) {
                $outlet_ids = json_decode($userdata['outlet_id'], true);
                foreach ($outlet_ids AS $key => $item) {
                    if (isset($outlet_ids[$key+1])) {
                        $additional .= "id = " . $item . " OR ";
                    } else {
                        $additional .= "id = " . $item;
                    }
                }

                $additional =  "AND ($additional)";
            }

            $data = $this->data;

            $sql = "SELECT id, name, code, type
        FROM project
        WHERE deleted_at IS NULL AND status = 'active' AND type = 'outlet' $additional";

            $data['project_list'] = $this->db->query($sql)->result_array();
        } else {
            $data['project_list'] = [];
        }

        return $this->template->loadView("sales_input/select_outlet", $data);
    }

    public function submit_sales ()
    {
        $input_data = $this->input_data['post'];
        $this->load->model('sales_input_model');

        $sql = "SELECT description, code
        FROM master_account
        WHERE deleted_at IS NULL AND (code = '40010' OR code = '40020' OR code = '40030' OR code = '40510') AND status = 'active'";

        $sales_names_data = $this->db->query($sql)->result_array();

        $sales_names = [];
        foreach ($sales_names_data AS $name) {

            $sales_names[$name['code']] = $name['description'];

        }

        $sql = "SELECT description, code
        FROM master_account
        WHERE deleted_at IS NULL AND (code = '14005' OR code = '14002' OR code = '14004' OR code = '14014'
        OR code = '14013' OR code = '14003' OR code = '14001' OR code = '14008' OR code = '14017'
         OR code = '14018' OR code = '14019') AND status = 'active'";

        $debtor_names_data = $this->db->query($sql)->result_array();

        $debtor_names = [];
        foreach ($debtor_names_data AS $name) {

            $debtor_names[$name['code']] = str_replace(' from Sales', '', str_replace('Undeposited ', '', str_replace('Trade Debtor - ', '', $name['description'])));

        }

        foreach ($input_data['outlets'] AS $item) {

            $outlet_id = $item['project_id'];
            $sql = "SELECT secret_key, name, code, id FROM project WHERE id = '$outlet_id' 
        AND deleted_at is null";

            $outlet = $this->db->query($sql)->result_array();

            $sql = "SELECT * FROM sales_input WHERE project_id = '$outlet_id' 
        AND deleted_at is null AND sales_date = '".$input_data['sales_date']."'";

            $sales_data = $this->db->query($sql)->result_array();

            if (!empty($sales_data)) {

                $this->sales_input_model->delete($sales_data[0]['id']);

                // Delete GL entry
                $this->load->model('GL_entry_model');
                $this->load->model('GL_transaction_model');
                $this->load->model('GL_mapping_model');
                $this->load->model('bank_reconciliation_model');
                $this->load->model('GL_transaction_project_split_model');

                $this->GL_entry_model->delete($sales_data[0]['GL_entry_id']);

                $sql = "SELECT id, GL_transaction_id FROM GL_mapping
        WHERE GL_entry_id = ".$sales_data[0]['GL_entry_id']." AND deleted_at IS NULL";

                $GL_transactions_id = $this->db->query($sql)->result_array();

                foreach ($GL_transactions_id AS $transaction) {

                    $bank_reconciliation_id = $this->GL_transaction_model->find($transaction['GL_transaction_id'])['bank_reconciliation_id'];

                    if (!empty($bank_reconciliation_id)) {

                        $this->bank_reconciliation_model->delete($bank_reconciliation_id);

                    }

                    $this->GL_mapping_model->realDelete($transaction['id']);
                    $this->GL_transaction_model->realDelete($transaction['GL_transaction_id']);

                    $sql1 = "SELECT id FROM GL_transaction_project_split
        WHERE GL_transaction_id = ".$transaction['GL_transaction_id']." AND deleted_at IS NULL";

                    $GL_transactions_project_split = $this->db->query($sql1)->result_array();

                    foreach ($GL_transactions_project_split AS $project_split) {
                        $this->GL_transaction_project_split_model->realDelete($project_split['id']);
                    }
                }

            }

            // Add POS data to GL entry
            $outlet_name = $outlet[0]['name'] . ' (' . $outlet[0]['code'] . ')';

            $transactions = [];

            if ($item['GST_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '21401',
                    'amount' => $item['GST_amount'],
                    'description' => 'GST Total',
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['GST_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['service_charge_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '40110',
                    'amount' => $item['service_charge_amount'],
                    'description' => 'Service Charge Total',
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['service_charge_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['delivery_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '40098',
                    'amount' => $item['delivery_amount'],
                    'description' => 'Delivery Charge Total',
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['delivery_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['discount_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '40210',
                    'amount' => $item['discount_amount'],
                    'description' => 'Overall Discount Total',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['discount_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['cash_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '14005',
                    'amount' => $item['cash_amount'],
                    'description' => $debtor_names['14005'].' Total',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['cash_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['visa_card_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '14002',
                    'amount' => $item['visa_card_amount'],
                    'description' => $debtor_names['14002'].' Total',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['visa_card_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['nets_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '14004',
                    'amount' => $item['nets_amount'],
                    'description' => $debtor_names['14004'].' Total',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['nets_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['internal_voucher_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '14014',
                    'amount' => $item['internal_voucher_amount'],
                    'description' => $debtor_names['14014'].' Total',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['internal_voucher_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['external_voucher_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '14013',
                    'amount' => $item['external_voucher_amount'],
                    'description' => $debtor_names['14013'].' Total',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['external_voucher_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['master_card_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '14003',
                    'amount' => $item['master_card_amount'],
                    'description' => $debtor_names['14003'].' Total',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['master_card_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['american_express_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '14001',
                    'amount' => $item['american_express_amount'],
                    'description' => $debtor_names['14001'].' Total',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['american_express_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['corporate_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '14008',
                    'amount' => $item['corporate_amount'],
                    'description' => $debtor_names['14008'].' Total',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['corporate_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['ubereats_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '14017',
                    'amount' => $item['ubereats_amount'],
                    'description' => $debtor_names['14017'].' Total',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['ubereats_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['deliveroo_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '14018',
                    'amount' => $item['deliveroo_amount'],
                    'description' => $debtor_names['14018'].' Total',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['deliveroo_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['foodpanda_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '14019',
                    'amount' => $item['foodpanda_amount'],
                    'description' => $debtor_names['14019'].' Total',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['foodpanda_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['food_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '40010',
                    'amount' => $item['food_amount'],
                    'description' => $sales_names['40010'].' Sales Total',
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['food_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['beverage_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '40020',
                    'amount' => $item['beverage_amount'],
                    'description' => $sales_names['40020'].' Sales Total',
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['beverage_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['liquor_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '40030',
                    'amount' => $item['liquor_amount'],
                    'description' => $sales_names['40030'].' Sales Total',
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['liquor_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['others_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '40510',
                    'amount' => $item['others_amount'],
                    'description' => $sales_names['40510'].' Sales Total',
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['others_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['rounding_amount'] > 0) {
                $transactions[] = [
                    'account_code' => '40220',
                    'amount' => $item['rounding_amount'],
                    'description' => 'Rounding Total',
                    'type' => 'credit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['rounding_amount'],
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            if ($item['rounding_amount'] < 0) {
                $transactions[] = [
                    'account_code' => '40220',
                    'amount' => $item['rounding_amount']* (-1),
                    'description' => 'Rounding Total',
                    'type' => 'debit',
                    'project' => [0 => [
                        'percentage' => '100',
                        'amount' => $item['rounding_amount']* (-1),
                        'project_id' => $outlet[0]['id']
                    ]
                    ]
                ];
            }

            $total_sales = $item['food_amount'] + $item['beverage_amount']
                + $item['liquor_amount'] + $item['others_amount']
                + $item['GST_amount'] + $item['service_charge_amount'] + $item['delivery_amount'] + $item['rounding_amount'];

            $GL_entry_id = NULL;
            if (!empty($transactions)) {

                $pos_date = $input_data['sales_date'];

                $GL_entry = [
                    'type' => 'POS',
                    'document_date' => $pos_date,
                    'description' => 'Sales portal entry on date ' . $pos_date . ' for ' . $outlet_name,
                    'total_amount' => $total_sales,
                    'transactions' => $transactions
                ];

                // Save to GL Entry

                $query = "INSERT INTO GL_entry (type, document_date, description, total_amount) 
            VALUES ('" . $GL_entry['type'] . "', '" . $GL_entry['document_date'] . "', '" . $GL_entry['description'] . "', " . $GL_entry['total_amount'] . ")";
                $this->db->query($query);
                $GL_entry_id = $GL_entry['id'] = $this->db->insert_id();

                foreach ($GL_entry['transactions'] AS $transaction) {

                    $query = "INSERT INTO GL_transaction (type, amount, transaction_type, description) 
            VALUES ('" . $transaction['type'] . "', " . $transaction['amount'] . ", 'GL', '" . $transaction['description'] . "')";
                    $this->db->query($query);
                    $transaction_id = $this->db->insert_id();

                    $query = "INSERT INTO GL_mapping (GL_transaction_id, GL_entry_id) 
            VALUES (" . $transaction_id . ", " . $GL_entry['id'] . ")";
                    $this->db->query($query);

                    $transaction['id'] = $transaction_id;

                    foreach ($transaction['project'] AS $project) {
                        $sql = "SELECT account_project.id FROM account_project
    LEFT JOIN master_account ON account_project.account_id = master_account.id
    WHERE account_project.deleted_at IS NULL AND account_project.project_id = " . $project['project_id'] . "
    AND master_account.code = " . $transaction['account_code'];

                        $account_project_id = $this->db->query($sql)->result_array();
                        $project ['account_project_id'] = $account_project_id[0]['id'];
                        $project ['GL_transaction_id'] = $transaction['id'];
                        $project ['document_date'] = $GL_entry['document_date'];

                        $query = "INSERT INTO GL_transaction_project_split (amount, percentage, account_project_id, GL_transaction_id, document_date) 
            VALUES (" . $project['amount'] . ", " . $project['percentage'] . ", " . $project['account_project_id'] . ", " . $project['GL_transaction_id'] . ", '" . $project['document_date'] . "')";
                        $this->db->query($query);
                    }
                }
            }

            $item['adjustment_amount'] = $item['rounding_amount'];
            $item['GL_entry_id'] = $GL_entry_id;
            $item['sales_date'] = $input_data['sales_date'];

            $this->sales_input_model->add($item);

        }

        $this->__set_flash_message('Sales Data is submitted successfully');

        echo json_encode([
            'success' => TRUE
        ]);
        return;
    }

    public function get_data ()
    {

        $sales_date = $this->input_data['post']['sales_date'];
        $project_id = $this->input_data['post']['project_id'];

        $sql2 = "SELECT *
        FROM sales_input
        WHERE sales_date = '".$sales_date."' AND project_id = $project_id AND deleted_at IS NULL";

        $data = $this->db->query($sql2)->result_array()[0];

        echo json_encode($data);
        return;
    }

    public function check_date ()
    {
        $input_data = $this->input_data['post'];

        $today = date("Y-m-d");
        $twodays_ago = date('Y-m-d', strtotime('-2 days'));

        if (strtotime($input_data['sales_date']) >= strtotime($twodays_ago) && strtotime($input_data['sales_date']) <= strtotime($today)) {
            echo json_encode([
                'error' => FALSE
            ]);
            die;
        } else {
            echo json_encode([
                'error' => TRUE
            ]);
            die;
        }

    }
}
