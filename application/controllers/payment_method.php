<?php
class Payment_method extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
        $this->load->model('company_model');
        $this->controller_name = 'Company';
    }

    public function index()
    {
        $data = $this->data;

        $this->load->model('company_model');
        $company_data = $this->company_model->find(1);
        $pos_database = 'l12com_pos_'.strtolower($company_data['company_id']);
        $acc_database = 'l12com_acc_'.strtolower($company_data['company_id']);

        $sql = "SELECT ".$pos_database.".t_payment_method.id, ".$pos_database.".t_payment_method.name, ".$pos_database.".t_payment_method.default_bank_account_id, ".$pos_database.".t_payment_method.default_bank_charge_rate,
        ".$acc_database.".bank_account.name AS account_name, ".$acc_database.".bank_account.number AS account_number
        FROM ".$pos_database.".t_payment_method
        LEFT JOIN ".$acc_database.".bank_account ON ".$acc_database.".bank_account.id = ".$pos_database.".t_payment_method.default_bank_account_id
        WHERE ".$pos_database.".t_payment_method.deleted_at IS NULL";

        $data['payment_methods'] = $this->db->query($sql)->result_array();

        $accounts_array = ['' => 'Please Select'];
        $sql = "SELECT id, account_code, name, number, type FROM bank_account 
        WHERE deleted_at IS NULL ORDER BY account_code";

        $data['accounts'] = $this->db->query($sql)->result_array();

        foreach ($data['accounts'] AS $account) {
            $accounts_array[$account['id']] = $account['name'];
            $accounts_array[$account['id']] .= !empty($account['number']) ? '('.$account['number'].')' : '';

        }

        $data['accounts_array'] = $accounts_array;

        return $this->template->loadView("payment_method/index", $data, "admin");
    }

    public function edit()
    {
        if($this->input_data) {

            $input_data = $this->input_data['post'];

            $this->load->model('company_model');
            $company_data = $this->company_model->find(1);
            $pos_database = 'l12com_pos_'.strtolower($company_data['company_id']);

            if (!empty($input_data['id'])) {

                $default_bank_charge_rate = $input_data['default_bank_charge_rate'];
                $default_bank_account_id = $input_data['default_bank_account_id'];
                $id = $input_data['id'];

                $sql = "UPDATE ".$pos_database.".t_payment_method
                SET ".$pos_database.".t_payment_method.default_bank_charge_rate = '$default_bank_charge_rate', 
                ".$pos_database.".t_payment_method.default_bank_account_id = '$default_bank_account_id'
                WHERE ".$pos_database.".t_payment_method.id = $id";
                $this->db->query($sql);

                $this->__set_flash_message('Payment Method is updated successfully');
                redirect('payment_method');
                return;
            }
        }
    }
}
