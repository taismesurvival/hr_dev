<?php
class chart_of_account extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
    }

    public function index()
    {
        $data = $this->data;

        $data['search'] = $this->input_data['get'];

        $additional_where = '';

        if (!empty($data['search']['keyword'])) {
            $keyword = $data['search']['keyword'];

            $additional_where .= " AND (master_account.name LIKE '%$keyword%' OR master_account.code LIKE '%$keyword%')";
        }

        if (!empty($data['search']['type'])) {
            $type = $data['search']['type'];

            $additional_where .= " AND master_account.type = '$type'";
        }

        $sql = "SELECT master_account.*, MC.id AS MC_id FROM master_account 
        LEFT JOIN master_account AS MC ON MC.father_id = master_account.id
        WHERE master_account.deleted_at IS NULL $additional_where GROUP BY master_account.id ORDER BY master_account.code";

        $data['accounts'] = $this->db->query($sql)->result_array();

        $sql = "SELECT description, code
        FROM master_account
        WHERE deleted_at IS NULL AND (code = '40010' OR code = '40020' OR code = '40030' OR code = '40510') AND status = 'active' ORDER BY code ASC";

        $sales_names_data = $this->db->query($sql)->result_array();

        $data['sales_names'] = [];
        foreach ($sales_names_data AS $name) {

            $data['sales_names'][$name['code']] = $name['description'];

        }


        return $this->template->loadView("chart_of_account/index", $data, "admin");
    }

    public function change_name()
    {
        if ($this->input_data) {

            $input_data = $this->input_data['post'];

            $this->load->model('master_account_model');

            $this->master_account_model->update($input_data['account_id'], [
                'name' => $input_data['account_name'],
                'description' => $input_data['account_name']
            ]);
            $this->__set_flash_message('Account is updated successfully');
            redirect('chart_of_account');

        }
    }

    public function change_sales()
    {
        if ($this->input_data) {

            $input_data = $this->input_data['post'];

            $this->load->model('company_model');
            $company_data = $this->company_model->find(1);
            $pos_database = 'l12com_pos_'.strtolower($company_data['company_id']);

            $this->db->query('SET SQL_SAFE_UPDATES=0;');
            foreach ($input_data AS $key => $item) {

                $sql = "UPDATE master_account SET description = '$item', name = 'Sales - $item' WHERE code = '$key' AND code IN (40010, 40020, 40030, 40510)";
                $this->db->query($sql);

                $sql = "UPDATE $pos_database.pos_item_category SET $pos_database.pos_item_category.description = '$item', $pos_database.pos_item_category.name = '$item' 
                WHERE $pos_database.pos_item_category.account_code = '$key'";
                $this->db->query($sql);

            }
            $this->db->query('SET SQL_SAFE_UPDATES=1;');

            $this->__set_flash_message('Sales Account is updated successfully');
            redirect('chart_of_account');

        }
    }

    public function change_status()
    {
        if ($this->input_data) {

            $input_data = $this->input_data['post'];

            $sql = "UPDATE master_account SET status = '".$input_data['status']."' WHERE id = '".$input_data['id']."'";
            $this->db->query($sql);

            $this->__set_flash_message('Sales Account is updated successfully');

            echo json_encode([
                'success' => TRUE
            ]);
            return;

        }
    }
}
