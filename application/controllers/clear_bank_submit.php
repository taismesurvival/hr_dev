<?php
class clear_bank_submit extends AR_Controller {

    public function __construct(){
        parent::__construct();
        /*AUTH*/
        $this->is_login("admin");
    }

    public function index()
    {
        $data = $this->data;

        $input_data = $this->input_data['get'];

        $pagination = [
            'page' => (int) $input_data['p'] ? $input_data['p'] : 1,
            'limit' => (int) $input_data['limit'] ? $input_data['p'] : 20
        ];

        $pagination['offset'] = ($pagination['page'] - 1) * $pagination['limit'];

        $data['search'] = $pagination['search'] = $this->input_data['get'];

        $additional_where = '';

        if (!empty($data['search']['month'])) {
            $month = $data['search']['month'];

            $additional_where .= " AND clear_bank_submit.month = '$month'";
        }

        if (!empty($data['search']['type'])) {
            $type = $data['search']['type'];

            $additional_where .= " AND clear_bank_submit.type = '$type'";
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS clear_bank_submit.*, bank_account.name, bank_account.number FROM clear_bank_submit
        INNER JOIN bank_account ON bank_account.id = clear_bank_submit.bank_account_id
        WHERE clear_bank_submit.deleted_at IS NULL $additional_where LIMIT ".$pagination['offset'].", ".$pagination['limit'];

        $data['submits'] = $this->db->query($sql)->result_array();

        $pagination['total'] = $this->db->query('SELECT FOUND_ROWS() AS total;')->result_array()[0]['total'];

        $data['pagination'] = $pagination;

        return $this->template->loadView("clear_bank_submit/index", $data, "admin");
    }

    public function delete()
    {
        if($this->input_data) {
            $this->load->model('clear_bank_submit_model');
            $this->load->model('bank_reconciliation_model');

            $input_data = $this->input_data['post'];

            $clear_bank_submit = $this->clear_bank_submit_model->find($input_data['id']);

            $this->clear_bank_submit_model->delete($input_data['id']);

            $bank_recons_array = json_decode($clear_bank_submit['bank_recons_ids'], TRUE);

            foreach ($bank_recons_array AS $item) {
                $this->bank_reconciliation_model->update($item, ['clear_date' => NULL]);
            }

            $this->__set_flash_message('The Submission is deleted successfully');
            redirect('clear_bank_submit');
            return;

        }
    }
}
