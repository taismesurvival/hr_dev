<?php

/**

 * User: gamalan

 * Date: 3/17/2015

 * Time: 4:55 PM

 */





function action_helper($routename, $id, $allow_edit = 1, $allow_delete = 1, $access_right="")

{

    $button_name='Delete';

    $disabled='';
    $disabled2='';

    $ci =& get_instance();

    $data = '<span class="tooltip-area">';

    switch ($routename) {

        case DatatableConstant::$EMPLOYEE_TYPE_ROUTE:

        case DatatableConstant::$USERGROUP_ROUTE:

            break;

        case DatatableConstant::$USER_ROUTE:

            $data .= '<a href="' . base_url() . $routename . '/editpassword/' . $id . '" class="btn btn-primary btn-inverse btn-sm" title="Set Password"><i class="fa fa-lock"></i> Set Password</a>';

            break;

        case DatatableConstant::$USER_ACCESS_ROUTE:

            $ci->load->model("user_model");

            $user_check = $ci->user_model->find($id, "user_access_group_id");

            if (isset($user_check['id'])) {

                $allow_delete = 0;

            }

            break;

        case DatatableConstant::$EMPLOYEE_ROUTE:

            $data .= '<a href="' . base_url() . $routename . '/dashboard/' . $id . '" class="btn btn-primary btn-inverse btn-sm" title="Set Password"><i class="fa fa-dashboard"></i> Dashboard</a>';

            $allow_delete = 0;

            break;

        case "employee_payable":

            $data .= '<a href="' . base_url() . 'employee/edit_payable/' . $id . '" class="btn btn-primary btn-inverse btn-sm" title="Edit"><i class="fa fa-pencil"></i> Edit</a>';
            
            $ci->load->model("employee_default_payable_model");
            $payable_data = $ci->employee_default_payable_model->find($id);
            
            // $data .= '<a href="' . base_url() . 'employee/destroy_payable/' . $id . '" id="deleteBtn" data="' . $id . '" class="btn btn-warning btn-inverse btn-sm" title="Delete"><i class="fa fa-trash-o"></i> Delete</a>';
            // 20160316 vernhui Change PayItem Recurring Delete Button
            $access_editable = $ci->access_management->check_editable($access_right);     // 20160608 vernhui Allow sub-admin (client HR/Manager) to view/upload employee data
            $data .= '<a href="javascript:;" id="deleteBtn" data="' . $id . '" class="btn btn-warning btn-inverse btn-sm" title="Delete" '.$access_editable.'><i class="fa fa-trash-o"></i> Delete</a>';

            break;

        case "payroll_detail":



            $data .= '<a target="_blank" href="' . base_url() . 'payroll_detail/print_detail/' . $id . '" id="downloadBtn" data="' . $id . '" class="btn btn-warning btn-sm" title="download">Download</a>';



            break;

        case "paylips":
            // 20160122 vernhui Change Download button in payslip tab to View
            $data .= '<a target="_blank" href="' . base_url() . 'payslips/print_detail/' . $id . '" id="downloadBtn" data="' . $id . '" class="btn btn-warning btn-sm" title="download">View</a>';

            break;

        case "leave" :

            $ci->load->model("employee_leave_model");
            $check_leave=$ci->employee_leave_model->find($id);
            $expired_leave_access = $ci->access_management->check_access("expired_leave_cancel");


            /*
             * 20151217 vernhui Cancel Button for the Employee Leave Transaction
             * add in leave status "CANCELLED for the leave transaction
             * allow cancel button to function except "REJECT" && "CANCELLED" status (APPROVED/ PENDING/ REJECT/ CANCELLED)
             */
            $today_date = date("Y-m-d");

            if ( $check_leave['status'] == 'REJECT' || $check_leave['status'] == 'CANCELLED' ) {
                $disabled = 'disabled';
            }
            else if ( $check_leave['status'] == 'APPROVED' && $expired_leave_access['access_type'] != "full" ) {
                $disabled = 'disabled';
            }


            if ( $check_leave['file_leave'] != NULL ) {

                $label  = 'DOWNLOAD FILE';
                $action = base_url() . 'home_page/download_file/' . $id.'/'.$check_leave['file_leave'];
                $target = '_blank';
                $data  .= "<a target='$target' href='$action' id='fileBtn' data='$id' class='btn btn-warning btn-sm' title='Download'>Download</a>";
            } else {
                $data  .= "<a  href='javascript:;' id='btn_upload_file' data='$id' class='btn blue btn-sm btn_upload_file' title='Download'>Upload File</a>";
            }

            $button_name = 'Cancel';
            $data .= "<a  href='javascript:;' id='btn_detail' data='$id' class='btn purple btn-sm btn_detail' title='Detail'>Detail</a>";

            // 20160308 vernhui Enhance Leave Transaction - add in button for resend email to approver 
            $data .= "<a  href='javascript:;' id='btn_resend' data='$id' class='btn green btn-sm btn_resend' title='Resend' $disabled>Resend</a>";

            // 20161026 vernhui Celene - Leave Applicant able to cancel leave after approved
            if ( $check_leave['status'] == 'PENDING' || ( $check_leave['status'] == 'APPROVED' && $check_leave['start_date'] > date('Y-m-d') ) ) {
                $cancel_dis = '';
            } else {
                $cancel_dis = 'disabled';
            }
            $data .= '<a href="#" id="deleteBtn" data="' . $id . '" class="btn btn-warning btn-inverse btn-sm" title="Delete" '.$cancel_dis.'><i class="fa fa-trash-o"></i> '.$button_name.'</a>';


            break;

        case "leave_approval":

            $ci->load->model("employee_leave_model");
            $check_leave=$ci->employee_leave_model->find($id);

            if ($check_leave['file_leave']!=NULL) {
                $label='DOWNLOAD FILE';
                $action=base_url() . 'home_page/download_file/' . $id.'/'.$check_leave['file_leave'];
                $target='_blank';
                $data .= '<a target="'.$target.'" href="' . $action . '" id="fileBtn" data="' . $id . '" class="btn btn-warning btn-sm" title="download">Download</a>'; 
            }else{
                $data .= '<a  href="javascript:;" id="btn_upload_file" data="' . $id . '" class="btn blue btn-sm btn_upload_file" title="download">Upload File</a>';
            }
            
            $data .= '<a  href="javascript:;" id="btn_detail" data="' . $id . '" class="btn purple btn-sm btn_detail" title="detail">Detail</a>';

            break;

        case "claim":

            $ci->load->model("employee_claim_model");
            $check_leave=$ci->employee_claim_model->find($id);

            if ( $check_leave['status'] !='PENDING' && $check_leave['status'] != 'APPROVED' ) {
                $disabled = 'disabled';
            }
            if ( $check_leave['status'] !='PENDING' ) {
                $disabled2 = 'disabled';
            }

            if ( $check_leave['file_path'] != NULL ) {
                $label  = 'DOWNLOAD FILE';
                $action = base_url() . 'home_page/download_file_claim/' . $id.'/'.$check_leave['file_path'];
                $target = '_blank';
                $data  .= "<a target='$target' href='$action' id='fileBtn' data='$id' class='btn btn-warning btn-sm' title=Download'>Download</a>";
            }

            $button_name = 'Cancel';
            $data .= "<a  href='javascript:;' id='btn_detail' data='$id' class='btn purple btn-sm btn_detail' title='detail'>Detail</a>";
            
            // 20160308 vernhui Enhance Leave Transaction - add in button for resend email to approver 
            $data .= "<a  href='javascript:;' id='btn_resend' data='$id' class='btn green btn-sm btn_resend' title='Resend' $disabled2>Resend</a>";
            break;

        case "claim_approval":

            $ci->load->model("employee_claim_model");
            $check_claim=$ci->employee_claim_model->find($id);
            
            if ( $check_claim['file_path'] != NULL ) {
                $label  = 'DOWNLOAD FILE';
                $action = base_url() . 'home_page/download_file_claim/' . $id.'/'.$check_claim['file_path'];
                $target = '_blank';
                $data  .= "<a target='$target' href='$action' id='fileBtn' data='$id' class='btn btn-warning btn-sm' title='Download'>Download</a>";
            }
            $data .= "<a  href='javascript:;' id='btn_detail' data='$id' class='btn purple btn-sm btn_detail' title='Detail'>Detail</a>";

            break;

        case "employee_salary":

            $ci->load->model("employee_salary_settings_model");

            $salary_data = $ci->employee_salary_settings_model->find($id);

            if (time() >= strtotime($salary_data['effective_date'])) {

                //$allow_edit = 0;

                $allow_delete = 0;

                $allow_edit = 1;

            }



            $data .= '<a href="javascript:void(0)" class="btn green  btn-inverse btn-sm btn view_detail" title="view" id="' . $id . '"><i class="fa fa-file"></i> View</a>';

            break;

        case "employee_leave_setting":

                $ci->load->model("employee_leave_setting_model");

                $employee_leave = $ci->employee_leave_setting_model->count(" AND employee_id = $id AND due_date >= CURDATE() ");

                $allow_delete = 0;

                if($employee_leave > 0){

                    $data .= '<a href="javascript:void(0)" class="btn green  btn-inverse btn-sm btn view_detail" title="view" id="' . $id . '"><i class="fa fa-pencil"></i> Edit</a>';

                    $allow_delete = 1;

                }

            break;

        case "employee_claim_setting":

                $ci->load->model("employee_claim_setting_model");

                $claim_count = $ci->employee_claim_setting_model->count(" AND employee_id = $id AND due_date >= CURDATE() ");

                $allow_delete = 0;

                if($claim_count > 0){

                    $data .= '<a href="javascript:void(0)" class="btn green  btn-inverse btn-sm btn view_detail" title="view" id="' . $id . '"><i class="fa fa-pencil"></i> Edit</a>';

                    $allow_delete = 1;

                }

            break;

        case "employee_claim":

            $ci->load->model("employee_claim_model");

            $employee_leave = $ci->employee_claim_model->find($id);

            $allow_delete = 0;

            if($id > 0){

                $data .= '<a href="javascript:void(0)" class="btn green  btn-inverse btn-sm btn view_detail" title="view" id="' . $id . '"><i class="fa fa-file"></i> View</a>';

                $allow_delete = 1;

            }

            break;

        case "settings_levy": {

            $data .= '<a href="' . base_url() . $routename . '/manage/' . $id . '" class="btn btn-primary btn-inverse btn-sm" title="Set Password"><i class="fa fa-dashboard"></i> Manage</a>';

            break;

        }





        case "settings_levy_detail": {

            $data .= '<a href="' . base_url() . 'settings_levy/manage_edit/' . $id . '" class="btn btn-primary btn-inverse btn-sm" title="Edit"><i class="fa fa-pencil"></i> Edit</a>';

            $allow_edit = 0;

            break;

        }

            break;



        case "employee_kpi_target":

            $data .= '<a href="'.base_url('employee_kpi_target/detail/'.$id).'" class="btn green  btn-inverse btn-sm btn view_detail" title="view" id="' . $id . '"><i class="fa fa-pencil"></i> Edit</a>';

            $allow_delete = 0;

            break;

        case "employee_kpi_performance":

            $ci->load->model("employee_kpi_target_model");

            $employee_targets = $ci->employee_kpi_target_model->all(" AND employee_id = $id AND to_date >= CURDATE()");

            $allow_delete = 0;

            if(count($employee_targets)>0){

                foreach($employee_targets as $employee_target) {

                    $ci->load->model('kpi_index_model');

                    $kpi_index = $ci->kpi_index_model->find($employee_target['kpi_index_id']);

                    $data .= '<a href="'.base_url('employee_kpi_performance/edit').'/'.$id.'/'.$employee_target['id'].'" class="btn green  btn-inverse btn-sm btn view_detail" title="view"><i class="fa fa-pencil"></i>Input '.$kpi_index['name'].' Performance</a>';

                }

            }

            break;

        case "employee_kpi_target_list":

            $ci->load->model("employee_kpi_target_model");

            $employee_targets = $ci->employee_kpi_target_model->find($id);

            $data .= '<a href="'.base_url('employee_kpi_target/detail_edit_target/'.$employee_targets['employee_id'].'/'.$id).'" class="btn green  btn-inverse btn-sm btn view_detail" title="view" id="' . $id . '"><i class="fa fa-pencil"></i> Edit</a>';

            $data .= '<a href="'.base_url('employee_kpi_target/detail_edit_detail/'.$employee_targets['employee_id'].'/'.$id).'" class="btn green  btn-inverse btn-sm btn view_detail" title="view" id="' . $id . '"><i class="fa fa-pencil"></i> Input Target</a>';

            $allow_delete = 1;

            $allow_edit = 0;

            break;

        case "home_page_appraisal":
            $ci->load->model("employee_appraise_model");
            $ci->load->model("employee_appraisal_answer_model");
            $employee_appraise_data = $ci->employee_appraise_model->find($id);
            $employee_appraisal_answer_data = $ci->employee_appraisal_answer_model->all(" AND employee_appraise_id = $id ");
            if($employee_appraise_data["status"] == "PENDING"){
                $data .= '<a href="javascript:void(0)" data-employee_appraisal_id="'.$employee_appraise_data["employee_appraisal_id"].'" data-employee_appraise_id="'.$employee_appraise_data["id"].'" class="btn green  btn-inverse btn-sm btn btn_input_score" title="view" id="' . $id . '"><i class="fa fa-pencil"></i> Input Score</a>';
            }
            if(count($employee_appraisal_answer_data) > 0 && $employee_appraise_data["status"] == "PENDING" ){
                $data .= '<a href="javascript:void(0)" data-employee_appraisal_id="'.$employee_appraise_data["employee_appraisal_id"].'" data-employee_appraise_id="'.$employee_appraise_data["id"].'" class="btn btn-primary  btn-inverse btn-sm btn btn_socre_submit" title="view" id="' . $id . '"><i class="fa fa-flag"></i> Submit</a>';
            }
            $allow_delete = 0;
            $allow_edit = 0;
            break;

        case "employee_appraisal":
            $ci->load->model("employee_appraisal_model");
            $ci->load->model("employee_appraise_model");
            $ci->load->model("employee_appraisal_answer_model");

            $employee_appraise_data = $ci->employee_appraise_model->all(" AND employee_appraisal_id = '$id' AND status != 'PENDING' ");
            if(count($employee_appraise_data) > 0) {
                $allow_delete = 0;
                $allow_edit = 0;   
            }

            $data .= '<a href="'.base_url('employee_appraisal/view/'.$id).'" data="'.$id.'" class="btn btn-success btn-inverse btn-sm" title="Edit"><i class="fa fa-list"></i> View</a>';
            break;
        case "timesheet_management":
            $ci->load->model("employee_timesheet_model");
            $date = $ci->employee_timesheet_model->find($id);
            if($date["status"] == "LOCK" || $date["payroll_process_id"] != ""){
                $allow_delete = 0;
                $allow_edit = 0;
            }else{
                //$data .= '<a href="javascript:;" data="'.$id.'" class="btn btn-primary btn-inverse btn-sm btn_edit" title="Edit"><i class="fa fa-pencil"></i> Edit</a>';
                $allow_edit = 0;
            }
            break;
        case "employee_appraisal_approval":

            $ci->load->model("employee_appraise_model");
            $ci->load->model("employee_appraisal_model");

            $employee_appraise_data = $ci->employee_appraise_model->find($id);
            $employee_appraisal_data = $ci->employee_appraisal_model->find($employee_appraise_data['employee_appraisal_id']);

            $userData = $ci->session->userdata("user_data");
            $data .= '<a href="'.base_url('employee_appraisal_approval/view/'.$id).'" data="'.$id.'" class="btn btn-success btn-inverse btn-sm btn_edit" title="view"> VIEW</a>';
            
            if( ($ci->session->userdata("user_type") == "employee" && $userData["id"] == $employee_appraisal_data["employee_id_approval"] && $employee_appraise_data["status"] == "SUBMITTED" ) || $ci->session->userdata("user_type") == "admin" && $employee_appraise_data["status"] == "SUBMITTED" ){
                $data .= '<a href="'.base_url('employee_appraisal_approval/approve/'.$id).'" data="'.$id.'" class="btn btn-primary btn-inverse btn-sm btn_edit" title="approve"> APPROVE</a>';
                $data .= '<a href="'.base_url('employee_appraisal_approval/reject/'.$id).'" data="'.$id.'" class="btn btn-warning btn-inverse btn-sm btn_edit" title="reject"> REJECT</a>';
            }
            
                
            break;
        case "report_writer":
            $data .= '<a href="'.base_url('report_writer/view/'.$id).'" data="'.$id.'" class="btn btn-success btn-inverse btn-sm btn_edit" title="view"target="_blank"> RUN</a>';
            
                
            break;

        default:

            break;

    }



    if ($allow_edit == 1) {
        // $access_editable = $ci->access_management->check_editable("company");     // 20160608 vernhui Allow sub-admin (client HR/Manager) to view/upload employee data
        $data .= '<a href="' . base_url() . $routename . '/edit/' . $id . '" class="btn btn-primary btn-inverse btn-sm" title="Edit" ><i class="fa fa-pencil"></i> Edit</a>';

    }

    // 20161026 vernhui Celene - Leave Applicant able to cancel leave after approved
    if ($allow_delete == 1) {

        if ($routename != "leave") {
            $access_editable = $ci->access_management->check_editable($access_right);     // 20160608 vernhui Allow sub-admin (client HR/Manager) to view/upload employee data
            $data .= '<a href="#" id="deleteBtn" data="' . $id . '" class="btn btn-warning btn-inverse btn-sm" title="Delete" '.$disabled.$access_editable.'><i class="fa fa-trash-o"></i> '.$button_name.'</a>';
        }

    }

    $data .= '</span>';

    return $data;

}



function select_helper($id, $row_num = "")

{

    if ($row_num != "") {

        return "<input type=checkbox id='lab-$id' value=" . $id . " class=_check style=opacity: 0;> <span class='label label-default' for='lab-$id'>$row_num</span>";

    } else {

        return "<input type=checkbox id='lab-$id' value=" . $id . " class=_check style=opacity: 0;> <span class='label label-default' for='lab-$id'></span>";

    }

}

// 20160106 vernhui New Opening Balance
function row_number_helper($id, $row_num = "")
{
    if ($row_num != "") {

        return "<input type='hidden' id='lab-$id' value='" . $id . "' > <span class='label label-default' for='lab-$id'>". $row_num ."</span>";

    } else {

        return "<input type='hidden' id='lab-$id' value='" . $id . "' > <span class='label label-default' for='lab-$id'></span>";

    }
}

// 20160106 vernhui New Opening Balance
function action_button_ob($routename, $id, $year="0")
{
    $data = '<span class="tooltip-area">';
    $data .= '<a href="' . base_url($routename."/form/".$year."/".$id) . '" class="btn btn-primary btn-inverse btn-sm" title="Opening Balance"><i class="fa fa-pencil"></i> Opening Balance</a>';
    $data .= '</span>';

    return $data;
}

// 20160601 vernhui IRAS forms and format specifications
function action_iras($routename, $id, $year="0")
{
    $data = '<span class="tooltip-area">';
    $data .= '<a href="' . base_url( $routename . "/edit/" . $year . "/" . $id) . '" class="btn btn-primary btn-inverse btn-sm" title="Edit"><i class="fa fa-pencil"></i> Edit</a>';
    $data .= '</span>';

    return $data;
}

// 20160317 vernhui Advertisement Newsletter Category
function newsletter_category_helper($routename, $id, $name, $user_type, $access_right) {
    $disabled = "";
    $ci = &get_instance();
    $access_editable = $ci->access_management->check_editable($access_right);     // 20160608 vernhui Allow sub-admin (client HR/Manager) to view/upload employee data
    if ( $name == "Advertisement" && $user_type != "admin" ) 
        $disabled = "disabled";

    $data = '';
    $data .= "<a href='" . base_url() . $routename . '/edit/' . $id . "' class='btn btn-primary btn-inverse btn-sm' title='Edit' $disabled><i class='fa fa-pencil'></i> Edit</a>";
    $data .= "<a href='#' id='deleteBtn' data='$id' class='btn btn-warning btn-inverse btn-sm' title='Delete' $disabled $access_editable><i class='fa fa-trash-o'></i> Delete</a>";

    return $data;
}

function levy_detail_helper($id)

{

    $ci = &get_instance();

    $ci->load->model('tax_fwl_detail_model');

    $detail_list = $ci->tax_fwl_detail_model->all(' AND tax_fwl_id = ' . $id . " ORDER BY effective_date DESC");

    $fwl_date_latest = count($detail_list) > 0 ? $detail_list[0] : null;

    return $fwl_date_latest != null ? view_date($fwl_date_latest['effective_date']) : "";

}



function levy_detail_helper_daily($id)

{

    $ci = &get_instance();

    $ci->load->model('tax_fwl_detail_model');

    $detail_list = $ci->tax_fwl_detail_model->all(' AND tax_fwl_id = ' . $id . " ORDER BY effective_date DESC");

    $fwl_date_latest = count($detail_list) > 0 ? $detail_list[0] : null;

    return $fwl_date_latest != null ? $fwl_date_latest['daily'] : "";

}



function levy_detail_helper_monthly($id)

{

    $ci = &get_instance();

    $ci->load->model('tax_fwl_detail_model');

    $detail_list = $ci->tax_fwl_detail_model->all(' AND tax_fwl_id = ' . $id . " ORDER BY effective_date DESC");

    $fwl_date_latest = count($detail_list) > 0 ? $detail_list[0] : null;

    return $fwl_date_latest != null ? $fwl_date_latest['monthly'] : "";

}



function lengthMenu()

{

    return ' "lengthMenu": [20, 50, 80, 100,500,1000] ,

              "iDisplayLength": 100, ';

}



function resignation_helper($resignation_date)

{

    

    

    if($resignation_date == NULL){

        return "ACTIVE";

    }else{  

    $today = time();

        $date = strtotime($resignation_date);

        return $today > $date ? "TERMINATED" : "ACTIVE";

        

        }

}



function leave_category_helper($id,$i=0, $from_page="admin") {

    $ci = &get_instance();
    $ci->load->model('employee_leave_model');

    // 20160314 vernhui show expired leave entitlement
    if ( $from_page == "admin" )
        $where1 = " AND b.due_date >= DATE_FORMAT(DATE_SUB(curdate(), INTERVAL 11 MONTH), '%Y-%m-01') ";
    else 
        $where1 = " AND b.due_date >= CURDATE() ";

    $data = "";
    $q = " SELECT b.* , b.id as employee_leave_setting_id, CONCAT(a.code) as full_name, IF(c.code IS NULL, c.name, CONCAT(c.code))  as name
            , SUBSTRING(IFNULL(remarks, ''), 1, 10) AS disp_remarks, DATE_FORMAT(due_date, '%d/%m/%Y') AS disp_due_date
            FROM employee a
            LEFT JOIN employee_leave_setting b ON b.employee_id = a.id
            LEFT JOIN employee_leave_category c ON b.leave_category = c.id
            WHERE a.id = '".$id."' $where1
            ORDER BY c.code ASC, b.due_date DESC ; ";

    $q = $ci->db->query($q)->result_array();

    foreach ( $q AS $key => $value ) {
        $content = $value["in_use"]; 

        if ( $i == 0 ) {
            $content = $value["total_leave"];
        }

        if ( $i == 2 ) {
            $content = $value['total_leave'] - $value['in_use'];
        }

        if ( $i == 3 ) {
            $active = $value['active'] == 1?"Yes":"No";
            $content = $active;
        }

        if ( $i == 4 ) {       // 20160101 vernhui Paid Hour Leave (OTHB)  - add in for remarks column
            $content = $value['disp_remarks'];
        }

        if ( $i == 5 ) {       // 20160101 vernhui Paid Hour Leave (OTHB)  - add in for due date column
            $content = $value['disp_due_date'];
        }

        $data .= $value["name"]." : ".$content."  <br>";
    }
    return $data;
}

function claim_category_helper($id,$i=0)

{

    $ci = &get_instance();

    $ci->load->model('employee_leave_model');

    

    $data = "";

    $q = " SELECT b.* , b.id as employee_claim_setting_id, CONCAT(a.code,' - ',a.full_name) as full_name, c.code as code

                FROM employee a

                LEFT JOIN employee_claim_setting b ON b.employee_id = a.id

                LEFT JOIN payroll_pay_item c ON b.payroll_pay_item_id = c.id

                

                WHERE

                b.due_date >= CURDATE()

                AND a.id = ".$id;

    $q = $ci->db->query($q)->result_array();

    foreach ($q as $key => $value) {



        $content = number_format($value["in_use"],2);

        if($i==0){

            $content = number_format($value["amount_limit"],2);

        }

        $data .= $value["code"]." : ".$content."<br>";

    }

    return $data;

}



function leave_helper($id, $row_num = "")

{

    $ci = &get_instance();

    $ci->load->model('employee_leave_model');

    $employee_leave = $ci->employee_leave_model->find($id);

    //if ($employee_leave["status"] == "PENDING") {

            return "<input type=checkbox id='lab-$id' value=" . $id . " class=_check_leave style=opacity: 0;> <span class='label label-default' for='lab-$id'>$row_num</span>";

    /*} else {        

            return "<input disabled type=checkbox id='lab-$id' value=" . $id . " class= style=opacity: 0;> <span class='label label-default' for='lab-$id'></span>";      

    }*/

}

// 20160419 vernhui Change Leave Delete & Cancel Button based on Access Right
function claim_helper($id, $row_num = "") {
    $ci = &get_instance();
    $ci->load->model('employee_claim_model');

    $employee_claim = $ci->employee_claim_model->find($id);
    if ($employee_claim["status"] == "PENDING" || $employee_claim["status"] == "APPROVED" || $employee_claim["status"] == "REJECT" || $employee_claim["status"] == "CANCELLED") {
        return "<input type=checkbox id='lab-$id' value=" . $id . " class=_check_claim style=opacity: 0;> <span class='label label-default' for='lab-$id'></span>";      
    }else{
        return "<input disabled type=checkbox id='lab-$id' value=" . $id . " class= style=opacity: 0;> <span class='label label-default' for='lab-$id'></span>";      
    }
}

// 20160831 vernhui Copy User Access Group Function
function user_access_group_helper($routename, $id, $allow_edit = 1, $allow_delete = 1, $access_right="") {
    $ci =& get_instance();
    $data = '<span class="tooltip-area">';
    $access_editable = $ci->access_management->check_editable($access_right);     // 20160608 vernhui Allow sub-admin (client HR/Manager) to view/upload employee data
    
    if ($allow_edit == 1) {
        $data .= '<a href="' . base_url() . $routename . '/edit/' . $id . '" class="btn btn-primary btn-inverse btn-sm" title="Edit" ><i class="fa fa-pencil"></i> Edit</a>';
    }
    $data .= '<a href="' . base_url() . $routename . '/copy/' . $id . '" class="btn btn-primary btn-inverse btn-sm" title="Copy" ><i class="fa fa-files-o"></i> Copy</a>';
    if ($allow_delete == 1) {    
        $data .= '<a href="#" id="deleteBtn" data="' . $id . '" class="btn btn-warning btn-inverse btn-sm" title="Delete" '.$access_editable.'><i class="fa fa-trash-o"></i> Delete</a>';
    }

    $data .= '</span>';

    return $data;
}
// END 20160831 vernhui Copy User Access Group Function