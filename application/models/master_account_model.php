<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class master_account_model extends AR_Model {
    
    public function __construct() {
        parent::__construct();
        $this->allow_insert = "id,name,type,status,code,description";
        $this->allow_update = "name,type,status,code,description";
        $this->tblName = "master_account";
        $this->tblId = "id";
    }
}
?>