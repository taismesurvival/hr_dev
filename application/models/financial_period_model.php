<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class financial_period_model extends AR_Model {
    
    public function __construct() {
        parent::__construct();
        $this->allow_insert = "id, month_from, year_from, month_to, year_to, status";
        $this->allow_update = "month_from, year_from, month_to, year_to, status";
        $this->tblName = "financial_period";
        $this->tblId = "id";
    }
}
?>