<?php
class User_Model extends AR_Model {

    public function __construct(){
        parent::__construct();
        $this->allow_insert = "username,password,user_access_group_id,employee_id,mobile,email,remarks,active,created_by,update_by";
        $this->allow_update = "password,user_access_group_id,employee_id,mobile,email,remarks,active,update_by";
        $this->tblName = "user";
        $this->tblId = "id";
    }

}
