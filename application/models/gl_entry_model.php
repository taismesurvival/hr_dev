<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class GL_entry_model extends AR_Model {
    
    public function __construct() {
        parent::__construct();
        $this->allow_insert = "id,name,type,status,code,description";
        $this->allow_update = "name,type,status,code,description";
        $this->tblName = "GL_entry";
        $this->tblId = "id";
    }
}
?>