<?php
class Supplier_Model extends AR_Model {

    public function __construct(){
        parent::__construct();
        $this->allow_insert = "id, name, email, addresss, mobile";
        $this->allow_update = "name, email, addresss, mobile";
        $this->tblName = "supplier";
        $this->tblId = "id";
    }

}
