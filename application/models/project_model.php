<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class project_model extends AR_Model {
    
    public function __construct() {
        parent::__construct();
        $this->allow_insert = "id, name, commence_date, completion_date, description, address";
        $this->allow_update = "name, commence_date, completion_date, description, address";
        $this->tblName = "project";
        $this->tblId = "id";
    }
}
?>