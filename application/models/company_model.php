<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class company_model extends AR_Model {
    
    public function __construct() {
        parent::__construct();
        $this->allow_insert = "id,name,company_id,company_address,postal_code,email,phone,fax,contact_person_name,registration_number,registration_date,tax_reference_no,cpf_account_no,cpf_account_no_2,voluntary_cpf_account_no,auth_agent_name,auth_agent_desicnation,pay_run_cut_off_date,started_using, company_logo,font_type,font_size,leave_approval_order,leave_approval_level,leave_cc_level,claim_approval_order,claim_approval_level,leave_entitlement,registration_type,auth_agent_email";
        $this->allow_update = "name,company_id,company_address,postal_code,email,phone,fax,contact_person_name,registration_number,registration_date,tax_reference_no,cpf_account_no_2,cpf_account_no,voluntary_cpf_account_no,auth_agent_name,auth_agent_desicnation,pay_run_cut_off_date,started_using, company_logo,font_type,font_size,leave_approval_order,leave_approval_level,leave_cc_level,claim_approval_order,claim_approval_level,leave_entitlement,registration_type,auth_agent_email";
        $this->tblName = "company";
        $this->tblId = "id";
    }


    // 20160314 vernhui Fix Company Cut off date to month end
    /*
     * Function to get the first payroll date & last payroll date
     * input parameters
     * - $year : year of the payroll process
     * - $month : month of the payroll process
     * return array
     * - first_calculated_date : first payroll date (Y-m-d) 
     * - last_calculated_date : last payroll date (Y-m-d)
    */
    public function get_payroll_first_last_date($year, $month) {
        $this->load->model("company_model");
        $ret 			= array();
        $company_data 	= $this->company_model->find(1);
        $temp_f_date 	= $company_data['pay_run_cut_off_date'];
        $temp_l_date 	= $company_data['pay_run_cut_off_date'];
        $last_month 	= $month - 1;
		$day_first_date = date('t', strtotime($year.'-'.$last_month.'-01'));

        // calculate the first payroll date & last payroll date based on given year & month
        $temp_d = date('t', strtotime($year.'-'.$month.'-01'));
        if ( $temp_d < $temp_l_date )
            $temp_l_date = $temp_d;

        if ( $day_first_date <= $temp_f_date ) {
        	$temp_f_date  = 1;
        	$last_month  += 1;
        } else 
        	$temp_f_date += 1;
    	// end calculation

        $ret['first_calculated_date']	= date("Y-m-d", strtotime($year.'-'.$last_month.'-'.$temp_f_date));
        $ret['last_calculated_date']	= date("Y-m-d", strtotime($year.'-'.$month.'-'.$temp_l_date));
		
		return $ret;
    }

}
?>