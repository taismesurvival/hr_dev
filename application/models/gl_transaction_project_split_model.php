<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class GL_transaction_project_split_model extends AR_Model {
    
    public function __construct() {
        parent::__construct();
        $this->allow_insert = "id,amount,percentage,account_project_id,GL_transaction_id";
        $this->allow_update = "amount,percentage,account_project_id,GL_transaction_id";
        $this->tblName = "GL_transaction_project_split";
        $this->tblId = "id";
    }
}
?>