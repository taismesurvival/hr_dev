<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class role_model extends AR_Model {
    
    public function __construct() {
        parent::__construct();
        $this->allow_insert = "id, username, password, role_id, name, mobile, email, is_active";
        $this->allow_update = "username, password, role_id, name, mobile, email, is_active";
        $this->tblName = "role";
        $this->tblId = "id";
    }
}
?>