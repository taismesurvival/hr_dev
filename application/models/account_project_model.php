<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class account_project_model extends AR_Model {
    
    public function __construct() {
        parent::__construct();
        $this->allow_insert = "id, account_id, project_id";
        $this->allow_update = "account_id, project_id";
        $this->tblName = "account_project";
        $this->tblId = "id";
    }
}
?>