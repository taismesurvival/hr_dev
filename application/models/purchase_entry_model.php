<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class purchase_entry_model extends AR_Model {
    
    public function __construct() {
        parent::__construct();
        $this->allow_insert = "id,amount,percentage,account_project_id,GL_transaction_id";
        $this->allow_update = "amount,percentage,account_project_id,GL_transaction_id";
        $this->tblName = "purchase_entry";
        $this->tblId = "id";
    }
}
?>