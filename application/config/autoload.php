<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$autoload['packages'] = array();

$autoload['libraries'] = array("template","session");

$autoload['helper'] = array("url","AR",'html','form','datatables_helper');

$autoload['config'] = array("dynamic_database");

$autoload['language'] = array();

$autoload['model'] = array("company_model");


/* End of file autoload.php */
/* Location: ./application/config/autoload.php */