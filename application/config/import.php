<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class import extends AR_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->is_login("admin");
        $this->access_management->check_access("import");
        $this->load->model('employee_model');
        $this->load->model('employee_custom_field_model');
        $this->load->model('employee_default_payable_model');
        $this->load->model('employee_employment_model');
        $this->load->model('employee_exclude_module_model');
        $this->load->model('employee_file_model');
        $this->load->model('employee_file_category_model');
        $this->load->model('employee_payment_settings_model');
        $this->load->model('employee_permit_model');
        $this->load->model('employee_residential_model');
        $this->load->model('employee_salary_settings_model');
        $this->load->model('bank_model');
        $this->load->model('company_model');
        $this->load->helper('datatables_helper');
        $this->load->library('selector_builder');
        $this->controller_name = 'import';

        $temp_data = $this->temporary_data;
        $this->data["msg"] = isset($temp_data['msg']) ? $temp_data['msg'] : "";
        $this->data["msg_status"] = isset($temp_data['msg_status']) ? $temp_data['msg_status'] : "";
        $this->data["typeImportDataSelector"] = array(
                                                        base_url().'import/company_data'=>"[[COMPANY_DATA_IMPORT]]",
                                                        base_url().'import/company_bank_account_data'=>"[[COMPANY_BANK_ACCOUNT_DATA_IMPORT]]",
                                                        base_url().'import/company_employee_category_data'=>"[[COMPANY_EMPLOYEE_CATEGORY_DATA_IMPORT]]",
                                                        base_url().'import/company_employee_grade_data'=>"[[COMAPNY_EMPLOYEE_GRADE_DATA_IMPORT]]",
                                                        base_url().'import/company_structure_1_data'=>"[[COMPANY_STRUCTUR_1_DATA_IMPORT]]",
                                                        base_url().'import/employee_data_new'=>"[[EMPLOYEE_DATA_IMPORT]]",
                                                        base_url().'import/salary_data_new'=>"[[EMPLOYEE_SALARY_DATA_IMPORT]]",

                                                        );

    }

    /*  fathan */

    public function index(){
        $this->company_data();
    }

    public function company_data()
    {
        $data = $this->data; //take any data possibly set in construct function
        $data['title'] = "[[COMPANY_DATA_IMPORT]]"; //title to show up in last part of breadcrumb and title of a page
        $data['type'] = Constant::$CREATE;
        $data['action'] = base_url() . 'import/submit_company_data';
        $this->template->loadView('admin/import/form', $data, 'admin');
    }

    public function submit_company_data()
    {
        $this->load->model("bank_model");
        $this->load->model("company_model");
        $this->load->model("company_bank_account_model");

        $company_data = $this->company_model->find(1);

        if(!file_exists("./public/import/")){
                mkdir("./public/import/");
        }
        if(!file_exists("./public/import/".$company_data["company_id"])){
                mkdir("./public/import/".$company_data["company_id"]);
        }

        $config['upload_path'] = './public/import/'.$company_data["company_id"];
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '4096';
        $this->load->library('upload', $config);
        $this->load->library('csvimport');

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            
            $error = $this->upload->display_errors();
            $passData['msg'] = $error;
            $passData['msg_status'] = "danger";
            $this->session->set_userdata("temporary_data", $passData);
            redirect('import/company_data');
        } else {

            $company_data = $this->company_model->find(1);


            $file_data = $this->upload->data();
            $csv_array = $this->csvimport->get_array($file_data['full_path']);
            foreach($csv_array as $data){
                if($data["name"] != ""){
                    $this->company_model->update(1,$data);
                }
                print_r($data);
                echo "<br>";
            }
            $passData['msg'] = "successfully import data";
            $passData['msg_status'] = "success";
            $this->session->set_userdata("temporary_data", $passData);
            redirect('import/company_data');
        }
    }

    public function company_bank_account_data()
    {
        $data = $this->data; //take any data possibly set in construct function
        $data['title'] = "[[COMPANY_BANK_ACCOUNT_DATA_IMPORT]]"; //title to show up in last part of breadcrumb and title of a page
        $data['type'] = Constant::$CREATE;
        $data['action'] = base_url() . 'import/submit_company_bank_account';
        $this->template->loadView('admin/import/form', $data, 'admin');
    }

    public function submit_company_bank_account()
    {
        $this->load->model("bank_model");
        $this->load->model("company_model");
        $this->load->model("company_bank_account_model");

        $company_data = $this->company_model->find(1);

        if(!file_exists("./public/import/")){
                mkdir("./public/import/");
        }
        if(!file_exists("./public/import/".$company_data["company_id"])){
                mkdir("./public/import/".$company_data["company_id"]);
        }

        $config['upload_path'] = './public/import/'.$company_data["company_id"];
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '4096';
        $this->load->library('upload', $config);
        $this->load->library('csvimport');

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            $error = $this->upload->display_errors();
            $passData['msg'] = $error;
            $passData['msg_status'] = "danger";
            $this->session->set_userdata("temporary_data", $passData);
            redirect('import/company_bank_account_data');
        } else {
            $company_data = $this->company_model->find(1);

            //$this->db->query("TRUNCATE `company_bank_account`");

            $file_data = $this->upload->data();
            $csv_array = $this->csvimport->get_array($file_data['full_path']);
            foreach($csv_array as $data){

                $t_bank_data = $this->bank_model->find($data["bank_code"],"GIRO_code");

                $input_data["bank_id"] = $t_bank_data["id"];
                $input_data["account_name"] = $data["account_name"];
                $input_data["account_number"] = $data["account_number"];
                $input_data["branch"] = $data["branch"];
                $input_data["company_code"] = $company_data["company_id"];
                $input_data["active"] = 1;

                $check_data = $this->company_bank_account_model->find($input_data["bank_id"],"bank_id",' AND account_name =  "'.$input_data['account_name'].'" AND account_number =  "'.$input_data['account_number'].'" ');
                if($check_data){
                    $this->company_bank_account_model->update($check_data["id"],$input_data);
                }else{
                    $this->company_bank_account_model->add($input_data);
                }

                print_r($input_data);
                echo "<br>";
            }
            $passData['msg'] = "successfully import data";
            $passData['msg_status'] = "success";
            $this->session->set_userdata("temporary_data", $passData);
            redirect('import/company_bank_account_data');
        }
    }


    public function company_employee_category_data()
    {
        $data = $this->data; //take any data possibly set in construct function
        $data['title'] = "[[COMPANY_EMPLOYEE_CATEGORY_DATA_IMPORT]]"; //title to show up in last part of breadcrumb and title of a page
        $data['type'] = Constant::$CREATE;
        $data['action'] = base_url() . 'import/submit_ccompany_employee_category';
        $this->template->loadView('admin/import/form', $data, 'admin');
    }

    public function submit_ccompany_employee_category()
    {
        $this->load->model("company_employee_category_model");

        $company_data = $this->company_model->find(1);

        if(!file_exists("./public/import/")){
                mkdir("./public/import/");
        }
        if(!file_exists("./public/import/".$company_data["company_id"])){
                mkdir("./public/import/".$company_data["company_id"]);
        }

        $config['upload_path'] = './public/import/'.$company_data["company_id"];
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '4096';
        $this->load->library('upload', $config);
        $this->load->library('csvimport');

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            $error = $this->upload->display_errors();
            $passData['msg'] = $error;
            $passData['msg_status'] = "danger";
            $this->session->set_userdata("temporary_data", $passData);
            redirect('import/company_employee_category_data');

        } else {
            $company_data = $this->company_model->find(1);


            $file_data = $this->upload->data();
            $csv_array = $this->csvimport->get_array($file_data['full_path']);
            $i=1;
            foreach($csv_array as $data){

                $input_data["code"] = strtoupper($data["code"]);
                $input_data["description"] = $data["description"];

                $check_data = $this->company_employee_category_model->find($input_data["code"],"code");
                if($check_data){
                    $this->company_employee_category_model->update($check_data["id"],$input_data);
                }else{
                    $this->company_employee_category_model->add($input_data);
                }

                print_r($input_data)."_".$i++;
                echo "<br>";
            }
            $passData['msg'] = "successfully import data";
            $passData['msg_status'] = "success";
            $this->session->set_userdata("temporary_data", $passData);
            redirect('import/company_employee_category_data');
        }
    }


    public function company_employee_grade_data()
    {
        $data = $this->data; //take any data possibly set in construct function
        $data['title'] = "[[COMAPNY_EMPLOYEE_GRADE_DATA_IMPORT]]"; //title to show up in last part of breadcrumb and title of a page
        $data['type'] = Constant::$CREATE;
        $data['action'] = base_url() . 'import/submit_ccompany_employee_grade';
        $this->template->loadView('admin/import/form', $data, 'admin');
    }

    public function submit_ccompany_employee_grade()
    {
        $this->load->model("company_employee_grade_model");

        $company_data = $this->company_model->find(1);

        if(!file_exists("./public/import/")){
                mkdir("./public/import/");
        }
        if(!file_exists("./public/import/".$company_data["company_id"])){
                mkdir("./public/import/".$company_data["company_id"]);
        }

        $config['upload_path'] = './public/import/'.$company_data["company_id"];
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '4096';
        $this->load->library('upload', $config);
        $this->load->library('csvimport');

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            $error = $this->upload->display_errors();
            $passData['msg'] = $error;
            $passData['msg_status'] = "danger";
            $this->session->set_userdata("temporary_data", $passData);
            redirect('import/company_employee_grade_data');
        } else {
            $company_data = $this->company_model->find(1);


            $file_data = $this->upload->data();
            $csv_array = $this->csvimport->get_array($file_data['full_path']);
            $i=1;
            foreach($csv_array as $data){

                if($data["code"] != ""){
                    $input_data["code"] = strtoupper($data["code"]);
                    $input_data["description"] = $data["description"];

                    $check_data = $this->company_employee_grade_model->find($input_data["code"],"code");
                    if($check_data){
                        $this->company_employee_grade_model->update($check_data["id"],$input_data);
                    }else{
                        $this->company_employee_grade_model->add($input_data);
                    }

                    print_r($input_data)."_".$i++;
                    echo "<br>";
                }
            }
            $passData['msg'] = "successfully import data";
            $passData['msg_status'] = "success";
            $this->session->set_userdata("temporary_data", $passData);
            redirect('import/company_employee_grade_data');
        }
    }


    public function company_employee_position_data()
    {
        $data = $this->data; //take any data possibly set in construct function
        $data['title'] = "[[COMPANY_EMPLOYEE_POSiTION_DATA_IMPORT]]"; //title to show up in last part of breadcrumb and title of a page
        $data['type'] = Constant::$CREATE;
        $data['action'] = base_url() . 'import/submit_ccompany_employee_position';
        $this->template->loadView('admin/import/form', $data, 'admin');
    }

    public function submit_ccompany_employee_position()
    {
        $this->load->model("company_employee_position_model");

        $company_data = $this->company_model->find(1);

        if(!file_exists("./public/import/")){
                mkdir("./public/import/");
        }
        if(!file_exists("./public/import/".$company_data["company_id"])){
                mkdir("./public/import/".$company_data["company_id"]);
        }

        $config['upload_path'] = './public/import/'.$company_data["company_id"];
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '4096';
        $this->load->library('upload', $config);
        $this->load->library('csvimport');

        //$this->db->query("TRUNCATE `company_employee_position`");
        //die();
        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            $error = $this->upload->display_errors();
            $passData['msg'] = $error;
            $passData['msg_status'] = "danger";
            $this->session->set_userdata("temporary_data", $passData);
            redirect('import/company_employee_position_data');
        } else {
            $company_data = $this->company_model->find(1);


            $file_data = $this->upload->data();
            $csv_array = $this->csvimport->get_array($file_data['full_path']);
            $i=1;
            foreach($csv_array as $data){

                $input_data["code"] = strtoupper($data["code"]);
                $input_data["description"] = $data["description"];

                $check_data = $this->company_employee_position_model->find($input_data["code"],"code");
                if($check_data){
                    $this->company_employee_position_model->update($check_data["id"],$input_data);
                    echo "update";
                }else{
                    $this->company_employee_position_model->add($input_data);
                    echo "add";
                }
                echo $i++;
                print_r($input_data)."_";
                echo "<br>";
            }
            $passData['msg'] = "successfully import data";
            $passData['msg_status'] = "success";
            $this->session->set_userdata("temporary_data", $passData);
            redirect('import/company_employee_position_data');
        }
    }


    public function employee_data_new()
    {
        $data = $this->data; //take any data possibly set in construct function
        $data['title'] = "[[EMPLOYEE_DATA_IMPORT]]"; //title to show up in last part of breadcrumb and title of a page
        $data['type'] = Constant::$CREATE;
        $data['action'] = base_url() . 'import/submit_employee_data_new';
        $this->template->loadView('admin/import/form', $data, 'admin');
    }

    public function submit_employee_data_new()
    {   
        
        $this->load->model("employee_model");
        $this->load->model("company_employee_position_model");
        $this->load->model("company_work_day_scheme_model");
        $this->load->model("company_employee_category_model");
        $this->load->model("company_employee_grade_model");
        $this->load->model("company_structure_1_model");
        $this->load->model("company_structure_2_model");
        $this->load->model("company_structure_3_model");
        $this->load->model("employee_employment_model");
        $this->load->model("employee_employment_model");
        $this->load->model("employee_payment_settings_model");
        $this->load->model("employee_residential_model");
        $this->load->model("employee_fingerprint_match_model");

        $company_data = $this->company_model->find(1);

        if(!file_exists("./public/import/")){
                mkdir("./public/import/");
        }
        if(!file_exists("./public/import/".$company_data["company_id"])){
                mkdir("./public/import/".$company_data["company_id"]);
        }

        $config['upload_path'] = './public/import/'.$company_data["company_id"];
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '4096';
        $this->load->library('upload', $config);
        $this->load->library('csvimport');

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            $error = $this->upload->display_errors();
            $passData['msg'] = $error;
            $passData['msg_status'] = "danger";
            $this->session->set_userdata("temporary_data", $passData);
            redirect('import/employee_data_new');
        } else {
            $company_data = $this->company_model->find(1);


            $file_data = $this->upload->data();
            $csv_array = $this->csvimport->get_array($file_data['full_path']);
            $i=1;
            foreach($csv_array as $data){
                // table employee prosess
                    $input_data = array();
                    $input_data["code"] = strtoupper($data["code"]);
                    $input_data["full_name"] = $data["full_name"];
                    $input_data["short_name"] = $data["short_name"];

                    if( strtolower(trim($data["title"])) == "ms" && strtolower(trim($data["marital_status"])) == "married" ){
                        $input_data["title"] = "MRS";
                    }else if( strtolower(trim($data["title"])) == "ms" && strtolower(trim($data["marital_status"])) == "single" ){
                        $input_data["title"] = "MISS";
                    }else{
                        $input_data["title"] = strtoupper($data["title"]);
                    }

                    $input_data["birth_date"] = $data["birth_date"];
                    $input_data["gender"] = strtoupper($data["gender"]);
                    $input_data["race"] = strtoupper($data["race"]);
                    $input_data["religion"] = strtoupper($data["religion"]);
                    $input_data["marital_status"] = strtoupper($data["marital_status"]);
                    $input_data["address1"] = $data["address1"];
                    $input_data["address2"] = $data["address2"];
                    $input_data["address3"] = $data["address3"];
                    $input_data["home_contact"] = $data["home_contact"];
                    $input_data["other_contact"] = $data["other_contact"];
                    $input_data["email"] = $data["email"];
                    $input_data["postal_code"] = $data["postal_code"];
                    $input_data["highest_qualification"] = $data["highest_qualification"];
                    $input_data["allow_login"] = $data["allow_login"];
                    $input_data["payslip_password"] = md5($data["code"].$data['payslip_password']);
                    $input_data["IDType"] = $data["IDType"];
                    $input_data["IDNumber"] = $data["IDNumber"];
                    $input_data["company_cpf"] = $data["company_cpf"];

                    $input_data_employeement = array();
                    $input_data_employeement["employee_status"] =$data["employee_status"];

                    $company_employee_position_data = $this->company_employee_position_model->find(strtoupper($data["employee_position"]),"code");
                    $input_data_employeement["employee_position"] = isset($company_employee_position_data["id"])?$company_employee_position_data["id"]:"";

                    $company_employee_category_data = $this->company_employee_category_model->find(strtoupper($data["employee_category"]),"code");
                    $input_data_employeement["employee_category"] = isset($company_employee_category_data["id"])?$company_employee_category_data["id"]:"";

                    $company_employee_category_data = $this->company_employee_grade_model->find(strtoupper($data["employee_grade"]),"code");
                    $input_data_employeement["employee_grade"] = isset($company_employee_category_data["id"])?$company_employee_category_data["id"]:"";

                    $company_structure_1_data = $this->company_structure_1_model->find(strtoupper($data["company_structure_1"]),"code");
                    $input_data_employeement["company_structure_1"] = isset($company_structure_1_data["id"])?$company_structure_1_data["id"]:"";

                    if($data["company_structure_2"] != ""){
                        $company_structure_2_data = $this->company_structure_2_model->find(strtoupper($data["company_structure_2"]),"code");
                        $input_data_employeement["company_structure_2"] = isset($company_structure_2_data["id"])?$company_structure_2_data["id"]:"";
                    }

                    if($data["company_structure_3"] != ""){
                        $company_structure_3_data = $this->company_structure_3_model->find(strtoupper($data["company_structure_3"]),"code");
                        $input_data_employeement["company_structure_3"] = isset($company_structure_3_data["id"])?$company_structure_3_data["id"]:"";
                    }

                    $input_data_employeement["probation_duration"] = is_numeric($data["probation_duration"]) == true?$data["probation_duration"]:3;
                    $input_data_employeement["probation_duration_unit"] = $data["probation_duration_unit"];
                    $input_data_employeement["employee_confirmed_date"] = $data["employee_confirmed_date"] != ""?$data["employee_confirmed_date"]:NULL;
                    $input_data_employeement["employed_date"] = $data["employed_date"] != ""?$data["employed_date"]:NULL;
                    $input_data_employeement["previous_employed_date"] = $data["previous_employed_date"] != ""?$data["previous_employed_date"]:NULL;
                    
                    $input_data_employeement["work_scheme"] = 1;
                    if($data["work_scheme"] != ""){
                        $work_scheme = $this->company_work_day_scheme_model->find(strtoupper($data["work_scheme"]),"code");
                        if($work_scheme){
                            $input_data_employeement["work_scheme"] = isset($work_scheme["id"])?$work_scheme["id"]:"";
                        }
                    }
                    
                    $input_data_employeement["last_day_work"] = $data["last_day_work"] != ""?$data["last_day_work"]:NULL;
                    $input_data_employeement["contract_end_date"] = $data["contract_end_date"] != ""?$data["contract_end_date"]:NULL;
                    $input_data_employeement["retirement_reason"] = $data["retirement_reason"] != ""?$data["retirement_reason"]:NULL;
                    $input_data_employeement["resignation_date"] = $data["resignation_date"] != ""?$data["resignation_date"]:NULL;
                    
                    
                    //tabel fingerprint
                    $input_data_fingerprint = array();
                    $input_data_fingerprint["fingerprint_id"] = $data["fingerprint id"] != ""?substr($data["fingerprint id"], 3):NULL;

                    //tabel payment_seetings
                    $input_data_payment_settings = array();
                    $input_data_payment_settings["company_bank_account_id"] = $data["company_bank_account_id"] != ""?$data["company_bank_account_id"]:NULL;
                    $input_data_payment_settings["payment_method"] = $data["payment_method"] != ""?strtoupper($data["payment_method"]):NULL;
                    $input_data_payment_settings["giro_account_name"] = $data["giro_account_name"] != ""?$data["giro_account_name"]:$data["full_name"];
                    $input_data_payment_settings["giro_account_no"] = $data["giro_account_no"] != ""?$data["giro_account_no"]:NULL;
                    $input_data_payment_settings["giro_bank_code"] = $data["giro_bank_code"] != ""?$data["giro_bank_code"]:NULL;
                    $input_data_payment_settings["giro_bank_branch"] = $data["giro_bank_branch"] != ""?$data["giro_bank_branch"]:NULL;

                    ////tabel employee_residential 
                    $input_data_residential = array();
                    $input_data_residential["pass_issue_date"] = $data["pass_issue_date"] != ""?$data["pass_issue_date"]:NULL;
                    $input_data_residential["pass_cancel_date"] = $data["pass_cancel_date"] != ""?$data["pass_cancel_date"]:NULL;
                    $input_data_residential["pass_expiry_date"] = $data["pass_expiry_date"] != ""?$data["pass_expiry_date"]:NULL;
                    $input_data_residential["origin_country"] = $data["origin_country"] != ""?strtoupper($data["origin_country"]):NULL;
                    $input_data_residential["cpf_no"] = $data["cpf_no"] != ""?$data["cpf_no"]:NULL;
                    $input_data_residential["income_tax_no"] = $data["income_tax_no"] != ""?$data["income_tax_no"]:NULL;
                    $input_data_residential["passport_no"] = $data["passport_no"] != ""?$data["passport_no"]:NULL;
                    $input_data_residential["passport_issue_date"] = $data["passport_issue_date"] != ""?$data["passport_issue_date"]:NULL;
                    $input_data_residential["passport_expiry_date"] = $data["passport_expiry_date"] != ""?$data["passport_expiry_date"]:NULL;
                    $input_data_residential["residential_status"] = $data["residential_status"] != ""?strtoupper($data["residential_status"]):NULL;
                    $input_data_residential["nationality"] = trim($data["nationality"]) != "SPR"?strtoupper($data["nationality"]):"SINGAPOREAN";
                    $input_data_residential["citizenship"] = trim($data["citizenship"]) != ""?strtoupper($data["citizenship"]):NULL;

                    
                    if(isset($input_data["full_name"]) && $input_data["full_name"] != ""){
                        $check_data = $this->employee_model->find($input_data["code"],"code");
                        if($check_data){
                            $employee_id = $check_data["id"];
                            $this->employee_model->update($check_data["id"],$input_data);

                            $input_data_employeement["id"] = $employee_id;
                            $check_employements = $this->employee_employment_model->find($employee_id);
                            if($check_employements){
                                 $this->employee_employment_model->update($employee_id,$input_data_employeement);
                            }else{
                                 $this->employee_employment_model->add($input_data_employeement);
                            }

                            $input_data_fingerprint["id"] = $employee_id;
                            $check_fingerprint = $this->employee_fingerprint_match_model->find($employee_id);
                            if($check_fingerprint){
                                 $this->employee_fingerprint_match_model->update($employee_id,$input_data_fingerprint);
                            }else{
                                 $this->employee_fingerprint_match_model->add($input_data_fingerprint);
                            }

                            $input_data_payment_settings["id"] = $employee_id;
                            $input_data_payment_settings["employee_id"] = $employee_id;
                            $check_payment_seetings = $this->employee_payment_settings_model->find($employee_id);
                            if($check_payment_seetings){
                                 $this->employee_payment_settings_model->update($employee_id,$input_data_payment_settings);
                            }else{
                                 $this->employee_payment_settings_model->add($input_data_payment_settings);
                            }

                            $input_data_residential["id"] = $employee_id;
                            $check_residential = $this->employee_residential_model->find($employee_id);
                            if($check_residential){
                                 $this->employee_residential_model->update($employee_id,$input_data_residential);
                            }else{
                                 $this->employee_residential_model->add($input_data_residential);
                            }

                        }else{
                            $employee_id = $this->employee_model->add($input_data);

                            $input_data_employeement["id"] = $employee_id;
                            $check_employements = $this->employee_employment_model->find($employee_id);
                            if($check_employements){
                                 $this->employee_employment_model->update($employee_id,$input_data_employeement);
                            }else{
                                 $this->employee_employment_model->add($input_data_employeement);
                            }

                            $input_data_fingerprint["id"] = $employee_id;
                            $check_fingerprint = $this->employee_fingerprint_match_model->find($employee_id);
                            if($check_fingerprint){
                                 $this->employee_fingerprint_match_model->update($employee_id,$input_data_fingerprint);
                            }else{
                                 $this->employee_fingerprint_match_model->add($input_data_fingerprint);
                            }

                            $input_data_payment_settings["id"] = $employee_id;
                            $input_data_payment_settings["employee_id"] = $employee_id;
                            $check_payment_seetings = $this->employee_payment_settings_model->find($employee_id);
                            if($check_payment_seetings){
                                 $this->employee_payment_settings_model->update($employee_id,$input_data_payment_settings);
                            }else{
                                 $this->employee_payment_settings_model->add($input_data_payment_settings);
                            }
                            
                            $input_data_residential["id"] = $employee_id;
                            $check_residential = $this->employee_residential_model->find($employee_id);
                            if($check_residential){
                                 $this->employee_residential_model->update($employee_id,$input_data_residential);
                            }else{
                                 $this->employee_residential_model->add($input_data_residential);
                            }

                        }
                    }


                // end tabel emplopyee proses


            }
            $passData['msg'] = "successfully import data";
            $passData['msg_status'] = "success";
            $this->session->set_userdata("temporary_data", $passData);
            redirect('import/employee_data_new');
        }
    }

    public function salary_data_new()
    {
        $data = $this->data; //take any data possibly set in construct function
        $data['title'] = "[[EMPLOYEE_SALARY_DATA_IMPORT]]"; //title to show up in last part of breadcrumb and title of a page
        $data['type'] = Constant::$CREATE;
        $data['action'] = base_url() . 'import/submit_salary_data_new';
        $this->template->loadView('admin/import/form', $data, 'admin');
    }

    public function submit_salary_data_new()
    {
        $this->load->model("company_employee_position_model");
        $this->load->model("company_work_day_scheme_model");
        $this->load->model("company_employee_position_model");
        $this->load->model("employee_model");

        $company_data = $this->company_model->find(1);

        if(!file_exists("./public/import/")){
                mkdir("./public/import/");
        }
        if(!file_exists("./public/import/".$company_data["company_id"])){
                mkdir("./public/import/".$company_data["company_id"]);
        }

        $config['upload_path'] = './public/import/'.$company_data["company_id"];
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '4096';
        $this->load->library('upload', $config);
        $this->load->library('csvimport');

        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            $error = $this->upload->display_errors();
            $passData['msg'] = $error;
            $passData['msg_status'] = "danger";
            $this->session->set_userdata("temporary_data", $passData);
            redirect('import/salary_data_new');
        } else {
            $company_data = $this->company_model->find(1);


            $file_data = $this->upload->data();
            $csv_array = $this->csvimport->get_array($file_data['full_path']);
            $i=1;
            foreach($csv_array as $data){

                if($data["employee code"] != ""){
                    $employee_data = $this->employee_model->find($data["employee code"],"code");

                    if($employee_data){

                        $text = strtolower($data["basic_salary_amount"]);
                        $text = str_replace('s$', '', $text);
                        $text = str_replace(',', '', $text);
                        $data["basic_salary_amount"] = $text;

                        $input_data["employee_id"] = $employee_data["id"];
                        $input_data["basic_salary_amount"] = $data["basic_salary_amount"];
                        $input_data["effective_date"] = $data["effective_date"];
                        $input_data["overtime_cap"] = $data["overtime_cap"];
                        $input_data["overtime_eligible"] = $data["overtime_eligible"];
                        $input_data["remarks"] = $data["remarks"];



                        $data['employee_employment'] = $this->employee_employment_model->find($employee_data["id"]);

                        if($data['employee_employment']){
                            $data['work_schedule'] = $this->company_work_day_scheme_model->find($data['employee_employment']['work_scheme']);
                        }else{
                            $data['work_schedule'] = array("working_hours_per_week"=>"","working_days_per_week"=>"");
                        }


                        $amt = $input_data["basic_salary_amount"];
                        $wh = $data["work_schedule"]["working_hours_per_week"];
                        $wd = $data["work_schedule"]["working_days_per_week"];
                    
                        $daily_rate = ($amt*12/52)/$wd;
                        //daily_rate = daily_rate.toFixed(2);
                        $hourly_rate = ($amt*12/52)/$wh;

                        $input_data["hourly_rate"] = $hourly_rate;
                        $input_data["daily_rate"] = $daily_rate;

                        $ot_a = 1 * $hourly_rate;
                        $ot_b = 1.5 * $hourly_rate;
                        $ot_c = 2 * $hourly_rate;

                        $input_data["ot_a"] = number_format($ot_a,2);
                        $input_data["ot_b"] = number_format($ot_b,2);
                        $input_data["ot_c"] = number_format($ot_c,2);
                        
                        $check_data = $this->employee_salary_settings_model->find($employee_data["id"],"employee_id"," AND effective_date = '".$input_data["effective_date"]."' ");
                        if($check_data){
                            $this->employee_salary_settings_model->update($check_data["id"],$input_data);
                            echo "update";
                        }else{
                            $this->employee_salary_settings_model->add($input_data);
                            echo "add";
                        }

                    }
                }

                print_r($input_data)."_";
                echo "<br>".$i++."<br>";
            }
            $passData['msg'] = "successfully import data";
            $passData['msg_status'] = "success";
            $this->session->set_userdata("temporary_data", $passData);
            redirect('import/salary_data_new');
        }
    }


    public function company_structure_1_data()
    {
        $data = $this->data; //take any data possibly set in construct function
        $data['title'] = "[[COMPANY_STRUCTUR_1_DATA_IMPORT]]"; //title to show up in last part of breadcrumb and title of a page
        $data['type'] = Constant::$CREATE;
        $data['action'] = base_url() . 'import/submit_company_structure_1';
        $this->template->loadView('admin/import/form', $data, 'admin');
    }

    public function submit_company_structure_1()
    {
        $this->load->model("company_structure_1_model");

        $company_data = $this->company_model->find(1);

        if(!file_exists("./public/import/")){
                mkdir("./public/import/");
        }
        if(!file_exists("./public/import/".$company_data["company_id"])){
                mkdir("./public/import/".$company_data["company_id"]);
        }

        $config['upload_path'] = './public/import/'.$company_data["company_id"];
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '4096';
        $this->load->library('upload', $config);
        $this->load->library('csvimport');

        //$this->db->query("TRUNCATE `company_structure_1`");
        //die();
        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            $error = $this->upload->display_errors();
            $passData['msg'] = $error;
            $passData['msg_status'] = "danger";
            $this->session->set_userdata("temporary_data", $passData);
            redirect('import/company_structure_1_data');
        } else {
            $company_data = $this->company_model->find(1);


            $file_data = $this->upload->data();
            $csv_array = $this->csvimport->get_array($file_data['full_path']);
            $i=1;
            foreach($csv_array as $data){

                $input_data["code"] = strtoupper($data["code"]);
                $input_data["description"] = $data["description"];

                $check_data = $this->company_structure_1_model->find($input_data["code"],"code");
                if($check_data){
                    $this->company_structure_1_model->update($check_data["id"],$input_data);
                    echo "update";
                }else{
                    $this->company_structure_1_model->add($input_data);
                    echo "add";
                }
                echo $i++;
                print_r($input_data)."_";
                echo "<br>";
            }
            $passData['msg'] = "successfully import data";
            $passData['msg_status'] = "success";
            $this->session->set_userdata("temporary_data", $passData);
            redirect('import/company_structure_1_data');
        }
    }


}