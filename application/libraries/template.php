<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//template
//created by : Julius Michael Rinaldo

class Template
{

    public function loadView($bodypath, $data, $type)
    {
        $ci =& get_instance();

        // Load pagination
        if (isset($data['pagination'])) {
            $data['pagination'] = $this->get_pagination($data['pagination']);
        }

        $ci->load->library("session");
        $data['user_type'] = $ci->session->userdata("login_type");

        if ($type == "admin") {
		$ci->load->model("company_model");
		$data['current_company_data'] = $ci->company_model->find(1);
            $data['head'] = $ci->load->view("template/includes/" . $type . "-head", $data, TRUE);
            $data['header'] = $ci->load->view("template/includes/" . $type . "-header", $data, TRUE);
            $data['sidebar'] = $ci->load->view("template/includes/" . $type . "-sidebar", $data, TRUE);
            $data['footer'] = $ci->load->view("template/includes/" . $type . "-footer", $data, TRUE);
            $data['foot'] = $ci->load->view("template/includes/" . $type . "-foot", $data, TRUE);
            $data['content'] = $ci->load->view($bodypath, $data, TRUE);
            $view = $ci->load->view("template/admin.php", $data, TRUE);
        } else if ($type == "public") {

            $data['content_view'] = $ci->load->view($bodypath, $data, TRUE);
            $view = $ci->load->view("template/public.php", $data, TRUE);
        } else {
            $view = $ci->load->view($bodypath, $data, TRUE);
        }
        echo $view;
        
    }

    private function get_pagination($pagination = [])
    {
        $limit = isset($pagination['limit']) ? $pagination['limit'] : 20;
        $page = isset($pagination['page']) ? $pagination['page'] : 1;
        $total = empty($pagination['total']) ? 0 : (int) $pagination['total'];
        $total_page = ceil($total / $limit);
        $adj = 5;
        $offset = ($page - 1) * $limit + 1;

        // Prepare search parameters
        $search = '';

        if (!empty($pagination['search'])) {
            // Remove page parameter
            if (isset($pagination['search']['p'])) {
                unset($pagination['search']['p']);
            }

            $search = http_build_query($pagination['search']);
        }

        return [
            'adj' => $adj,
            'search' => $search,
            'items_per_page' => $limit ,
            'page' => $page,
            'total' => $total,
            'total_page' => $total_page,
            'display' => $total > $limit ? 20 : $total,
            'offset' => $offset,
            'limit' => ($offset + $limit - 1) > $total ? $total : $offset + $limit - 1,
            'min_display' => $page - $adj < 0 ? 1 : $page - $adj,
            'max_display' => $page + $adj > $total_page ? $total_page : $page + $adj
        ];
    }
}
